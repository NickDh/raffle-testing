<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Payment gateway
    |--------------------------------------------------------------------------
    |
    | Define payment getaway which will be used for the payment system
    | Use key for one of defined payment gateways
    |
    */
    'payment_gateway' => 'trustPayment',

    /*
    |--------------------------------------------------------------------------
    | Payment gateways configurations
    |--------------------------------------------------------------------------
    |
    | Here you can update payment gateways options or add additional payment gateways
    |
    | gatewayClass - required field. Added class should implements \App\Payment\Core\PaymentGatewayContract
    |
    */
    'gateways'        => [
        'trustPayment' => [
            'gateway_class' => \App\Payment\TrustPayment\TrustPaymentGateway::class,
            'sandbox_mode'  => env('TRUST_PAYMENT_SANDBOX', 0),
            'jwt'           => [
                'username' => env('TRUST_PAYMENT_JWT_USERNAME', null),
                'secret'   => env('TRUST_PAYMENT_JWT_SECRET', null),
            ],
            'sitereference' => env('TRUST_PAYMENT_SITEREFERENCE', null),
            'js_lib_url'    => 'https://webservices.securetrading.net/js/v3/st.js'
        ],
    ],
];
