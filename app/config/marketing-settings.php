<?php

return [
    'mailchimp' => [
        'title' => 'Mailchimp',
        'desc' => 'All the general settings for application.',
        'icon' => 'glyphicon glyphicon-sunglasses',

        'elements' => [
            [
                'type' => 'textInput', // input fields type
                'data' => 'string', // data type, string, int, boolean
                'key' => 'MC_KEY', // unique name for field
                'label' => 'MAILCHIMP_API_KEY', // you know what label it is
                'rules' => 'min:0|max:50', // validation rule of laravel
                'class' => 'w-auto px-2', // any class for input
                'value' => '' // default value if you want
            ],
            [
                'type' => 'textInput', // input fields type
                'data' => 'string', // data type, string, int, boolean
                'key' => 'MC_AUDIENCE_ID', // unique name for field
                'label' => 'MC_AUDIENCE_ID', // you know what label it is
                'rules' => 'min:0|max:50', // validation rule of laravel
                'class' => 'w-auto px-2', // any class for input
                'value' => '' // default value if you want
            ],
        ]
    ],
    'twilio' => [
        'title' => 'Twilio',
        'desc' => 'twilio setting for app',
        'icon' => 'glyphicon glyphicon-envelope',

        'elements' => [
            [
                'type' => 'textInput', // input fields type
                'data' => 'string', // data type, string, int, boolean
                'key' => 'TWILIO_SID', // unique name for field
                'label' => 'TWILIO_SID', // you know what label it is
                'rules' => 'min:0|max:50', // validation rule of laravel
                'class' => 'w-auto px-2', // any class for input
                'value' => '' // default value if you want
            ],
            [
                'type' => 'textInput', // input fields type
                'data' => 'string', // data type, string, int, boolean
                'key' => 'TWILIO_TOKEN', // unique name for field
                'label' => 'TWILIO_TOKEN', // you know what label it is
                'rules' => 'min:0|max:50', // validation rule of laravel
                'class' => 'w-auto px-2', // any class for input
                'value' => '' // default value if you want
            ],
            [
                'type' => 'textInput', // input fields type
                'data' => 'string', // data type, string, int, boolean
                'key' => 'TWILIO_FROM', // unique name for field
                'label' => 'TWILIO_FROM', // you know what label it is
                'rules' => 'min:0|max:50', // validation rule of laravel
                'class' => 'w-auto px-2', // any class for input
                'value' => '' // default value if you want
            ],
        ]
    ],


];
