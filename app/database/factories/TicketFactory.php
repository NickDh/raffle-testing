<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Tickets\Ticket::class, function (Faker $faker) {

    $user = App\Users\User::where('role_id', 2)->get()->random(1)->first();


    return [
        'status' => $faker->randomElement(\App\Enums\TicketStatusEnum::values()),
        'user_id' => $user->id
    ];
});
