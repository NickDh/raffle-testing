<?php

use App\Enums\CompetitionTypeEnum;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\PromoCodes\PromoCode::class, function (Faker $faker) {
    return [
        'code' => $faker->word,
        'type' => $faker->randomElement(\App\Enums\PromoCodeTypesEnum::values()),
        'value' => $faker->numberBetween(10, 100),
        'max_use' => $faker->numberBetween(1, 10),
        'max_use_per_user' => $faker->numberBetween(1, 10),
        'status' => $faker->randomElement(\App\Enums\PromoCodeStatusTypesEnum::values()),
        'expires_at' => $faker->dateTimeThisYear()
    ];
});
