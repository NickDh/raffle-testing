<?php

use App\Enums\CompetitionTypeEnum;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\PastWinners\PastWinner::class, function (Faker $faker) {
    return [
        'competition' => $faker->words(3, true),
        'username' => $faker->userName,
        'ticket_number' => $faker->numberBetween(1, 100),
        'draw_video' => $faker->url,
        'delivery_video' => $faker->url,
        'main_image'=> 'img_1.png',
    ];
});
