<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->text('short_description')->nullable();
            $table->text('full_description')->nullable();
            $table->float('price')->nullable();
            $table->float('sale_price')->nullable();

            $table->enum('competition_type', \App\Enums\CompetitionTypeEnum::values())->nullable();
            $table->string('tickets_count')->nullable();
            $table->string('tickets_sort_by')->nullable();
            $table->string('letters_to')->nullable();

            $table->dateTime('deadline')->nullable();

            $table->boolean('auto_extend')->nullable();
            $table->integer('hours_extend')->nullable();


            $table->integer('max_tickets_per_user')->nullable();

            $table->string('main_image')->nullable();

            $table->bigInteger('question_id')->unsigned()->nullable();
            $table->foreign('question_id')->references('id')->on('questions');

            $table->enum('status', \App\Enums\CompetitionStatusEnum::values())->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competitions');
    }
}
