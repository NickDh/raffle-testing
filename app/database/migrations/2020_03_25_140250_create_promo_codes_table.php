<?php

use App\Enums\PromoCodeStatusTypesEnum;
use App\Enums\PromoCodeTypesEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_codes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('code');
            $table->enum('type', PromoCodeTypesEnum::values());

            $table->integer('value');
            $table->integer('max_use')->nullable();
            $table->integer('max_use_per_user')->nullable();

            $table->enum('status', PromoCodeStatusTypesEnum::values());

            $table->date('expires_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_codes');
    }
}
