<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTwilioSendedSms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('twilio_sended_sms', function (Blueprint $table) {

            $table->dateTimeTz('date_created')->nullable()->change();
            $table->dateTimeTz('date_sent')->nullable()->change();
            $table->dateTimeTz('date_updated')->nullable()->change();

            $table->string('messaging_service_sid')->nullable()->change();

            $table->string('sid')->nullable()->change();
            $table->string('uri')->nullable()->change();
            $table->string('status',20)->nullable()->change();

            $table->longText('data')->nullable();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
