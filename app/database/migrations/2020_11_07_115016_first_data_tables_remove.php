<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FirstDataTablesRemove extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('payment_methods');
        Schema::dropIfExists('first_data_transactions');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('client_token')->nullable();
            $table->string('public_key_base64')->nullable();
            $table->string('payment_js_nonce')->nullable();
            $table->dateTime('payment_js_authenticated')->nullable();

            $table->string('token')->nullable();
            $table->string('gateway_ref_id')->nullable();
            $table->dateTime('first_data_authenticated')->nullable();

            $table->string('card_masked')->nullable();
            $table->integer('card_last4')->nullable();
            $table->string('card_brand')->nullable();

            $table->bigInteger('client_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('first_data_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('status');

            $table->float('total');
            $table->string('currency');

            $table->string('client_request_id')->nullable();
            $table->string('api_trace_id')->nullable();
            $table->string('ipg_transaction_id')->nullable();
            $table->timestamp('transaction_time')->nullable();
            $table->string('secure_3D_version')->nullable();
            $table->text('method_form')->nullable();
            $table->string('acs_url')->nullable();
            $table->text('creq')->nullable();
            $table->text('session_data')->nullable();
            $table->text('payer_authentication_request')->nullable();
            $table->text('merchant_data')->nullable();
            $table->text('pa_res')->nullable();
            $table->text('md')->nullable();

            $table->text('cres')->nullable();

            $table->text('secure_3D_authentication_request')->nullable();
            $table->text('secure_3D_authentication_response')->nullable();
            $table->text('secure_3D_api_initiation_request')->nullable();
            $table->text('secure_3D_initiation_request')->nullable();
            $table->text('secure_3D_initiation_response')->nullable();
            $table->text('secure_3D_api_initiation2_request')->nullable();
            $table->text('secure_3D_final_request')->nullable();
            $table->text('secure_3D_final_response')->nullable();

            $table->bigInteger('order_id');
            $table->timestamps();
        });
    }
}
