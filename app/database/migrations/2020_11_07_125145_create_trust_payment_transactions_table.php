<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrustPaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trust_payment_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->integer('errorcode');
            $table->string('status');
            $table->string('transactionreference');
            $table->integer('baseamount');
            $table->integer('eci');
            $table->string('currencyiso3a');
            $table->string('errormessage');
            $table->string('enrolled');
            $table->integer('settlestatus');
            $table->text('jwt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trust_payment_transactions');
    }
}
