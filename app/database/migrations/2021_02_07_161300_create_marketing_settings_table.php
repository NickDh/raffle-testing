<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketingSettingsTable extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('marketing_settings')) {
            Schema::create('marketing_settings', function (Blueprint $table) {
                $table->increments('id');
                $table->string('key');
                $table->string('value')->default('');
                $table->string('type');
                $table->string('title');
                $table->timestamps();

                $table->unique(['key']);
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('marketing_settings');
    }
}
