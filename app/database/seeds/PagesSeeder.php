<?php

use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    public function run()
    {
        $page = \App\Pages\Page::where('title', 'How to play')->first();
        if (!isset($page)){
            $this->createPages();
        }

    }

    public function createPages(){
        \App\Pages\Page::create([
            'title' => 'How to play',
            'slug' => 'faq',
            'header_link' => 1,
            'footer_link' => 1,
            'footer_column' => '1',
            'content' => '<h1> How to play - Content</h1>'
        ]);

        \App\Pages\Page::create([
            'title' => 'Privacy Policy',
            'slug' => 'cookie-policy',
            'header_link' => 0,
            'footer_link' => 1,
            'footer_column' => '2',
            'content' => '<h1> Privacy Policy - Content</h1>'
        ]);

        \App\Pages\Page::create([
            'title' => 'Terms & Conditions',
            'slug' => 'terms-and-conditions',
            'header_link' => 0,
            'footer_link' => 1,
            'footer_column' => '2',
            'content' => '<h1> Terms & Conditions - Content</h1>'
        ]);
    }
}