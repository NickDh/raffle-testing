<?php

use App\Users\Roles\Role;
use App\Users\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createRoles();
        $this->createAdmin();

        // something wrong with users
//        if (env('APP_ENV') ==='local'){
//            $this->createUsers();
//        }
    }

    public function createRoles()
    {
        $role_admin = Role::where('name', 'admin')->first();
        if (!isset($role_admin)){
            $role_employee = new Role();
            $role_employee->name = 'Admin';
            $role_employee->description = 'Can use dashboard';
            $role_employee->save();

        }
        $role_user = Role::where('name', 'user')->first();
        if (!isset($role_user)){
            $role_manager = new Role();
            $role_manager->name = 'User';
            $role_manager->description = 'Simple app user';
            $role_manager->save();
        }

    }

    public function createAdmin()
    {
        $role_admin = Role::where('name', 'admin')->first();

        $admin_user = User::where('username', 'rafflelabs')->first();

        if (!isset($admin_user)){
            User::create([
                'username' => 'rafflelabs',
                'first_name' => 'Administrator',
                'last_name' => 'Labs',
                'email' => 'info@rafflelabs.co.uk',
                'password' => 'DNsoq4LW6Nx3',
                'role_id' => $role_admin->id
            ]);
        }

    }

    public function createUsers()
    {
        $role_user = Role::where('name', 'User')->first();

        for($i=1;$i<=100;$i++){
            User::create([
                'username' => 'userSeed_'.$i.'_'.rand(1,999),
                'first_name' => 'Sample User_'.$i.'_'.rand(1,999),
                'last_name' => 'Labs',
                'email' => 'samle_'.$i.'_'.rand(1,999).'@rafflelabs.co.uk',
                'password' => 'DNsoq4LW6Nx3',
                'role_id' => $role_user
            ]);
        }
    }

}
