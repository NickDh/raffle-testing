<?php

use Illuminate\Database\Seeder;
use App\Marketing\MarketingSettings;

class MarketingSettingsSeeder extends Seeder
{
    protected $settings = [
        'TWILIO_SID'=>[
            'key' =>'TWILIO_SID',
            'type'=>'textInput',
            'title'=>'TWILIO_SID',
        ],
        'TWILIO_TOKEN'=>[
            'key' =>'TWILIO_TOKEN',
            'type'=>'textInput',
            'title'=>'TWILIO_TOKEN',
        ],
        'TWILIO_FROM'=>[
            'key' =>'TWILIO_FROM',
            'type'=>'textInput',
            'title'=>'TWILIO_FROM',
        ],
        'MC_KEY'=>[
            'key' =>'MC_KEY',
            'type'=>'textInput',
            'title'=>'MAILCHIMP_API_KEY',
        ],
        'MC_AUDIENCE_ID'=>[
            'key' =>'MC_AUDIENCE_ID',
            'type'=>'textInput',
            'title'=>'MC_AUDIENCE_ID',
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->addTwilioSettings();

    }

    public function addTwilioSettings(){
        foreach ($this->settings as $settingEntity){
            MarketingSettings::firstOrCreate($settingEntity);
        }
    }
}
