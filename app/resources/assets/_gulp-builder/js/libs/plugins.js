//JQuery
import $ from 'jquery';
global.jQuery = $;
global.$ = $;

//old plugin
import './oldFiles/modernizr-3.6.0.min';
import './oldFiles/popper';
import './oldFiles/common_scripts.min';

import '../../node_modules/jquery-countdown/dist/jquery.countdown';
import '../../node_modules/imagesloaded/imagesloaded';
import './oldFiles/isotope';
import './oldFiles/jquery-ui';
import './oldFiles/jquery-ui-touch-punch';
import './oldFiles/waypoints';
import './oldFiles/smoothscroll';
import './spinner';
import './oldFiles/lazyload.min';
import './oldFiles/ResizeSensor.min';

//Wow js plugin
window.WOW = require('../../node_modules/wowjs/dist/wow.min').WOW;
