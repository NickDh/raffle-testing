import {getToken} from "../libs/getToken"

export class Pagination {
    constructor(prevBtn, nextBtn, wrapBlk) {
        this.prevBtn = prevBtn;
        this.nextBtn = nextBtn;
        this.wrapBlk = wrapBlk;
        this.init();
    }

    init() {
        let _this = this;
        let $body = $('body');
        let url;
        let method;
        let token = getToken();
        let currPage = 1;

        /**
         * Get data for the previous
         */
        $body.on('click', _this.prevBtn, function (e) {
            e.preventDefault();
            url = $(this).closest(_this.wrapBlk).attr('data-action');
            method = $(this).closest(_this.wrapBlk).attr('data-method');
            if (!$(this).hasClass('disabled')) {
                currPage = $(this).attr('data-page');
                currPage--;
                sendData(currPage, $(this));
            }
        });

        /**
         * Get data for the next
         */
        $body.on('click', _this.nextBtn, function (e) {
            e.preventDefault();
            url = $(this).closest(_this.wrapBlk).attr('data-action');
            method = $(this).closest(_this.wrapBlk).attr('data-method');
            if (!$(this).hasClass('disabled')) {
                currPage = $(this).attr('data-page');
                currPage++;
                sendData(currPage, $(this));
            }
        });

        /**
         * Send current page
         * @param currPage - number current page
         */
        function sendData(currPage, currBtn){
            $.ajax({
                type: method,
                url: url,
                data: {
                    page: currPage,
                    _token: token
                },
                success: function(data){
                    currBtn.closest(_this.wrapBlk).html(data);
                },
            })
        }
    }
}
