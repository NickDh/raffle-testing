import '../libs/oldFiles/owl.carousel.min';
import { WOW } from '../libs/oldFiles/wow.js';
import '../../node_modules/magnific-popup/dist/jquery.magnific-popup.js';
import '../libs/oldFiles/carousel_with_thumbs';
import { Tikets } from "./selectTickets";
import { Sendform } from "../libs/sendform/sendform2";
import { Cart } from "./_cart";
import '../libs/spinner';
import '../libs/countdown.min';
import '../libs/slick.min';
import {Pagination} from './_pagination';
import '../../node_modules/cleave.js/dist/cleave';
import {TrustPayment} from "./paymnet/trust-payment";

$( document ).ready(function() {
    // let projectApp = new App();
    // projectApp.init();
    global.app = new App();
    global.app.init();
    /**
     * Timer for expire cart
     * @type {jQuery|undefined}
     */
    let $cartTimer = $('.basket-timer');
    let timer2 = $cartTimer.data('time');
    if(timer2){
        let interval = setInterval(function() {

            if(!$cartTimer.parent().is(':visible')){
                clearInterval(interval);
                return;
            }
            let timer = timer2.split(':');
            //by parsing integer, I avoid all extra string processing
            let minutes = parseInt(timer[0], 10);
            let seconds = parseInt(timer[1], 10);
            --seconds;
            minutes = (seconds < 0) ? --minutes : minutes;
            if (minutes < 0) clearInterval(interval);
            seconds = (seconds < 0) ? 59 : seconds;
            seconds = (seconds < 10) ? '0' + seconds : seconds;
            $cartTimer.html(minutes + ':' + seconds);
            timer2 = minutes + ':' + seconds;
            if(minutes == 0 && seconds === '00'){
                clearInterval(interval);
                $.spinnerAdd();
                // location.reload(true);
                window.location.replace("/times-up");
            }
        }, 1000);
    }else{
        $cartTimer.html('0:00');
    }

    /**
     * Class Pagination for page
     */
    new Pagination('.js_btn-prev', '.js_btn-next', '.js_tbl-wrap');

    // sticky header
    let wind = $(window);
    let sticky = $('.header-bar-area');
    wind.on('scroll', function() {
        let scroll = wind.scrollTop();
        if (scroll < 100) {
            sticky.removeClass('sticky');
        } else {
            sticky.addClass('sticky');
        }
    });

    /*=========================
      OwlCarousel START
    ===========================*/
    $('.winner_carousel').owlCarousel({
            center: false,
            autoplay: true,
            autoplayTimeout: 3000,
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            margin: 30,
            loop: true,
            nav: true,
            //margin: 10,
            lazyLoad: true,
            navText: ["<i class='ti-angle-left'></i>","<i class='ti-angle-right'></i>"],
            responsive: {
                0: {
                    nav: true,
                    dots: true,
                    items: 2,
                },
                560: {
                    nav: true,
                    dots :true,
                    items: 3,
                },
                768: {
                    nav: true,
                    dots: true,
                    items: 4,
                },
                1024: {
                    items: 4,
                },
                1200: {
                    items: 4,
                }
            }
    });

    $("#carousel-home .owl-carousel").on("initialized.owl.carousel", function() {
      setTimeout(function() {
        $("#carousel-home .owl-carousel .owl-item.active .owl-slide-animated").addClass("is-transitioned");
        $("section").show();
      }, 200);
    });

    const $owlCarousel = $("#carousel-home .owl-carousel").owlCarousel({
      items: 1,
      loop: true,
      nav: false,
      dots:true,
        responsive:{
            0:{
                 dots:false
            },
            767:{
                dots:false
            },
            768:{
                 dots:true
            }
        }
    });

    $owlCarousel.on("changed.owl.carousel", function(e) {
      $(".owl-slide-animated").removeClass("is-transitioned");
      const $currentOwlItem = $(".owl-item").eq(e.item.index);
      $currentOwlItem.find(".owl-slide-animated").addClass("is-transitioned");
    });

    $owlCarousel.on("resize.owl.carousel", function() {
      setTimeout(function() {
      }, 50);
    });


    $('#arrow-next').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top - 96 }, 500, 'linear');
    });

    /*--------------------------
             Count Down Timer
         ----------------------------*/
    $('[data-countdown]').each(function() {
        let $this = $(this),
            finalDate = $(this).attr('data-countdown');
        $this.countdown(finalDate, function(event) {
            $this.html(event.strftime('<div class="row text-center"><div class="col-3"><span class="cdown day"><span class="time-count">%-D</span> <p>Days</p></span></div><div class="col-3"><span class="cdown hour"><span class="time-count">%-H</span> <p>Hours</p></span></div><div class="col-3"><span class="cdown minutes"><span class="time-count">%M</span> <p>minutes</p></span></div><div class="col-3"><span class="cdown second"><span class="time-count">%S</span> <p>seconds</p></span></div></div>'));
        });
    });

    // Hamburger-menu
    $('.hamburger-menu').on('click', function() {
        $('.hamburger-menu .line-top, .ofcavas-menu').toggleClass('current');
        $('.hamburger-menu .line-center').toggleClass('current');
        $('.hamburger-menu .line-bottom').toggleClass('current');
    });

    $('.ofcavas-menu ul li a').on('click', function() {
        $('.hamburger-menu .line-top, .ofcavas-menu').removeClass('current');
        $('.hamburger-menu .line-center').removeClass('current');
        $('.hamburger-menu .line-bottom').removeClass('current');
    });

        //Footer collapse
    let $headingFooter = $('footer h3');
    $(window).resize(function() {
        if($(window).width() <= 768) {
            $headingFooter.attr("data-toggle","collapse");
        } else {
          $headingFooter.removeAttr("data-toggle","collapse");
        }
    }).resize();
    $headingFooter.on("click", function () {
        $(this).toggleClass('opened');
    });



    $('.progress .progress-bar').css("width",
        function() {
            return $(this).attr("aria-valuenow") + "%";
        }
    )

    /*------------------------------------------
                         Product quantity
        --------------------------------------------*/

    $('.quantity-right-plus').on("click", function(e) {
        e.preventDefault();
        let quantity = parseInt($(this).parent().siblings("input.input-number").val());
        $(this).parent().siblings("input.input-number").val(quantity + 1);
    });
    $('.quantity-left-minus').on("click", function(e) {
        e.preventDefault();
        let quantity = parseInt($(this).parent().siblings("input.input-number").val());
        if (quantity > 0) {
            $(this).parent().siblings("input.input-number").val(quantity - 1);
        }
    });

    $(".timer_wrap").click(function() {
        window.location = $(this).data("location");
        return false;
    });

    // Init animate for elements
    let wow = new WOW({live:false});
    wow.init();
});

class App{
    constructor(){
        /**
         * global variable for Trust Payment
         */
        this.trustPaymentProcessor = new TrustPayment();
    }
    init() {
        /**
         * Init Trust Payment payment gateway processing
         * @type {TrustPayment}
         */
        this.trustPaymentProcessor.init();

        let tickets = new Tikets({
            valTicket: '.js_tickets-val',
            btnTicket: '.js_tickets-btn',
            pickedTickets: '.js_tickets-picked',
            ticketsWrap: '.js_tickets-wrap',
            btnRandom: '.js_tickets-random-btn',
            selectedTicketsWrap: '.js_tickets-selected-wrap',
            answerItem: '.js_tickets-answer',
            btnSubmit: '.js_tickets-submit',
            btnSubmitForRandomTickets: '.js_tickets-random-submit',
            selectedActiveClass: 'selected',
            allTickets: '.js_tickets_all',
        });
        tickets.init();

       let formPromocode = new Sendform('.js_form-promocode', {
            msgError: 'Invalid Promo code',
            success: data => {
               $('.js_cart-total').html(data.response);
               formPromocode.resetField();
               $('#form-status').html('');
            }
        });

        new Sendform('.js_form-reset-password', {
            success: data => {
                $('#form-status').html('Password changed');
                window.location.replace("/user");
            },
            error: error =>{
                const errMsgArr = JSON.parse(error.target.response).error;
                let errMsg = '';
                errMsgArr.forEach((item) =>{
                    errMsg += item;
                });
                $('.js_form-reset-password #form-status').html(errMsg).addClass('with_error');
            }
        });

        new Sendform('.js_form-forgot-password', {
            success: data => {
                $('#form-status').html('A password reset link has been sent to your email');
            }
        });
        new Sendform('.js_form-change-password', {
            success: data => {
                $('#form-status').html('Password changed');
                window.location.replace("/user");
            },
            error: error =>{
                const errMsgArr = JSON.parse(error.target.response).errors.password;
                let errMsg = '';
                if (JSON.parse(error.target.response).errors.current_password){
                    const errMsgArrCurr = JSON.parse(error.target.response).errors.current_password;
                    errMsgArrCurr.forEach((item) =>{
                        errMsg += `<div>${item}</div>`;
                    });
                }
                errMsgArr.forEach((item) =>{
                    errMsg += `<div>${item}</div>`;
                });
                $('.js_form-change-password #form-status').html(errMsg).addClass('with_error');
            }
        });
        new Sendform('.js_form-login', {
            success: data => {
                $('.js_form-login #form-status').html(JSON.parse(data.response).message).removeClass('with_error');
                window.location.replace("/user");
            },
            error: error =>{
                $('.js_form-login #form-status').html(JSON.parse(error.target.response).message).addClass('with_error');
            }
        });
        new Sendform('.js_form-register', {
            success: data => {
                $('.js_form-register #form-status').html(JSON.parse(data.response).message);
                window.location.replace("/user");
            },
            error: error =>{
                const errMsgArr = JSON.parse(error.target.response).error;
                let errMsg = '';
                errMsgArr.forEach((item) =>{
                    errMsg += item;
                });
                $('.js_form-register #form-status').html(errMsg).addClass('with_error');
            }
        });
        new Sendform('.js_form-contacts', {
            success: data => {
                $('.js_form-contacts #form-status').html(JSON.parse(data.response).message);
            },
            error: error =>{
                try {
                    const errMsgArr = JSON.parse(error.target.response).error;
                    let errMsg = '';
                    errMsgArr.forEach((item) =>{
                        errMsg += item;
                    });
                    $('.js_form-contacts #form-status').html(errMsg).addClass('with_error');
                }catch (e) {
                    console.log(e);
                }
            }
        });
        new Sendform('.js_form-change-user-info', {
            success: data => {
                $('.js_form-change-user-info #form-status').html( JSON.parse(data.response).message);
            },
            error: error =>{
                const errMsgArr = JSON.parse(error.target.response).error;
                let errMsg = '';
                errMsgArr.forEach((item) =>{
                    errMsg += item;
                });
                $('.js_form-change-user-info #form-status').html(errMsg).addClass('with_error');
            }
        });
       let changeUserPassword = new Sendform('.js_form-change-user-password', {
            success: data => {
                let response = JSON.parse(data.response);
                if(response.message){
                    $('.js_form-change-user-password #form-status').html(response.message);
                }
               changeUserPassword.resetField();
            },
            error: error =>{
                const errMsgArr = JSON.parse(error.target.response).error;
                let errMsg = '';
                errMsgArr.forEach((item) =>{
                    errMsg += item;
                });
                $('.js_form-change-user-password #form-status').html(errMsg).addClass('with_error');
            }
        });
        const cart = new Cart({
            btnDelete: '.js_cart-delete-item',
            amountInfo: '.js_cart-amount',
            wrapCntCart: '.js_cart-wrap-cnt'
        });
        cart.init();

        //Mask's for format credit card field's
        if($('.js_checkout-date').length) {
            new Cleave('.js_checkout-date', {
                date: true,
                datePattern: ['m', 'Y']
            });
        }
        if($('.js_checkout-card').length) {
            new Cleave('.js_checkout-card', {
                creditCard: true,
                onCreditCardTypeChanged: function (type) {
                }
            });
        }
        if($('.js_checkout-cvv').length){
            new Cleave('.js_checkout-cvv', {
                blocks: [3],
                delimiter: ''
            });
        }
    }
};
