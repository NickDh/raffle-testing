@extends('core.base')

@section('content')
    @include('parts/_main-blk')

    @include('parts/_competitions')

    @include('parts/_how-to-play')

    @include('parts/_past-winners')

    @include('parts/_about')

@endsection
