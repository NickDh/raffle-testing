@extends('core.base')

@section('content')

<div class="top_banner">
    <div class="opacity-mask d-flex align-items-center">
        <div class="container">
            <h1>Reset Password</h1>
        </div>
    </div>
</div>


<div class="container margin_30">
    <!-- /page_header -->
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-8 offset-md-3">
            <div class="box_account">
                <div class="form_container">
                    <h2>Reset Password</h2>
                    <p>Please enter your new password</p>
                    <form action="{{route('password.update')}}" method="post" class="js_form-reset-password">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="email" value="{{$email}}">
                        <input type="hidden" name="token" value="{{$token}}">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>New Password</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Confirm New Password</label>
                                <input type="password" class="form-control"  name="password_confirmation" required>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn_1 full-width">Reset Password</button>
                        </div>
                    </form>
                </div>
                <!-- /form_container -->
            </div>
            <!-- /box_account -->
        </div>
    </div>
</div>
@endsection
