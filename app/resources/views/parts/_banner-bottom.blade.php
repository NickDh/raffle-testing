@if(isset($cart) && $cart->getContent()->count())
<div class="container-fluid fixed-bottom">
    <div class="row align-items-center timer_wrap animate__animated animate__backInUp" data-location="{{route('cart.view')}}">
        <div class="col-md-12">
            Your Entries Will Expire In <span class="basket-timer" data-time="{{$cart->getLifeTimeLeft()}}"></span>
        </div>
    </div>
</div>
@endif
