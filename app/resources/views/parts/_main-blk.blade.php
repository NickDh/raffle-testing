        <div id="carousel-home">
            <div class="owl-carousel owl-theme">
                <!--owl-slide-->
                <div class="owl-slide cover" style="background-image: url({{asset('img/slide_home_1.jpg')}});">
                    <div class="opacity-mask d-flex align-items-center" style="background-color: rgba(0, 0, 0, 0.5);">
                        <div class="container">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-6 static">
                                    <div class="slide-text white">
                                        <h2 class="owl-slide-animated owl-slide-title">EXAMPLE SLIDE 1</h2>
                                        <p class="owl-slide-animated owl-slide-subtitle">
                                           Folly words widow one downs few age every seven.
                                        </p>
                                        <div class="owl-slide-animated owl-slide-cta"><a class="btn_1" href="{{route('live-competitions')}}" role="button">VIEW COMPETITIONS</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/owl-slide-->
                <!--owl-slide-->
                <div class="owl-slide cover" style="background-image: url({{asset('img/slide_home_2.jpg')}});">
                    <div class="opacity-mask d-flex align-items-center" style="background-color: rgba(0, 0, 0, 0.5);">
                        <div class="container">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-6 static">
                                    <div class="slide-text white">
                                        <h2 class="owl-slide-animated owl-slide-title">EXAMPLE SLIDE 2</h2>
                                        <p class="owl-slide-animated owl-slide-subtitle">
                                            Folly words widow one downs few age every seven.
                                        </p>
                                        <div class="owl-slide-animated owl-slide-cta"><a class="btn_1" href="#" role="button">VIEW COMPETITION</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/owl-slide-->
                <!--owl-slide-->
                <div class="owl-slide cover" style="background-image: url({{asset('img/slide_home_3.jpg')}});">
                    <div class="opacity-mask d-flex align-items-center" style="background-color: rgba(0, 0, 0, 0.5);">
                        <div class="container">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-6 static">
                                    <div class="slide-text white">
                                        <h2 class="owl-slide-animated owl-slide-title">EXAMPLE SLIDE 3</h2>
                                        <p class="owl-slide-animated owl-slide-subtitle">
                                            Folly words widow one downs few age every seven.
                                        </p>
                                        <div class="owl-slide-animated owl-slide-cta"><a class="btn_1" href="{{route('live-competitions')}}" role="button">VIEW COMPETITIONS</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/owl-slide-->
            </div>
            <div id="icon_drag_mobile"></div>
        </div>
        <!--/carousel-->
@if($live_draw)
<!-- draw-countdown -->
<div class="draw-section">
    <div class="container">
        <div class="row small-gutters align-items-center pt-2 pb-2">
            <div class="col-md-5 offset-md-1 text-center">
                <h2>Our Next Live Draw Will Be In...</h2>
            </div>
            <div class="col-md-4">
                <div id="live-draw-countdown" data-countdown="{{date('Y/m/d H:i', strtotime($live_draw->date))}}"></div>
            </div>
        </div>
    </div>
</div>
<!-- draw-countdown -->
@endif
