<div class="row">
    <div class="col-6">
    </div>
    <div class="col-6">
        @include('parts/_pagination-ajax', ['paginator' => $orders])
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
          <table class="table">
            <thead>
                <tr>
                  <th scope="col">Order #</th>
                  <th scope="col">Date</th>
                  <th scope="col">Amount</th>
                  <th scope="col">Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                <tr>
                  <th scope="row"><a href="{{route('order.details', $order->id)}}">{{$order->id}}</a></th>
                  <td>{{date('d/m/Y', strtotime($order->created_at))}}</td>
                  <td>£{{$order->present()->total}}</td>
                  <td>{{$order->present()->status}}</td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-lg-8"></div>
    <div class="col-md-6 col-lg-4">
        @include('parts/_pagination-ajax', ['paginator' => $orders])
    </div>
</div>
