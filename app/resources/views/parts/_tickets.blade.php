
<div class="tab-content add_list_content">
<div class="tab-pane" id="lucky-dip">
    <div class="row align-items-center">
        <div class="col-md-4">
            <div class="def-number-input number-input">
                <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
                        class="minus"></button>
                <input class="quantity js_tickets-val" min="0" max="{{$maxRandomValues}}" name="quantity"
                       value="{{$maxRandomValues}}" type="number" disabled>
                <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                        class="plus"></button>
            </div>

            @if(Auth::check() && Auth::user()->availableForRandomizer($competition->max_tickets_per_user, $competition->id) == 0)
                <button type="submit" class="btn_1 js_tickets-btn">Generate</button>
            @endif

            @auth
                <button type="submit" class="btn_1 js_tickets-random-btn"
                        data-action="{{route('cart.random-tickets')}}">Generate
                </button>
            @elseauth
                <button type="submit" class="btn_1 js_tickets-random-btn"
                        data-action="{{route('cart.random-tickets')}}">Generate
                </button>
            @endauth
        </div>
    </div>
</div>
@foreach($competition->grouped_tickets['chunks'] as $group_name => $tickets)
<div class="tab-pane @if($loop->first) active @endif" id="tab{{$loop->iteration}}">
    <ul class="ticket_list mb-0">
        @foreach($tickets as $ticket)
            @if(isset($ticket['status']))
                <li class="@if(\App\Enums\TicketStatusEnum::SOLD_OUT()->is($ticket['status'])) sold @elseif(\App\Enums\TicketStatusEnum::HELD()->is($ticket['status'])) held @else selected js_tickets-btn js_selected-{{$ticket['name']}} @endif">
                    {{$ticket['name']}}
                    @if(\App\Enums\TicketStatusEnum::SOLD_OUT()->is($ticket['status']))
                        <span>sold</span>
                    @elseif(\App\Enums\TicketStatusEnum::HELD()->is($ticket['status']))
                        <span>held</span>
                    @endif
                </li>
            @else
                <li class="js_tickets-btn js_selected-{{$ticket['name']}}">
                    {{$ticket['name']}}
                </li>
            @endif
        @endforeach
    </ul>
</div>
@endforeach
<div class="row mt-4">
    <div class="col-lg-12 numbers-picked mt-10">
        <h5>Your Chosen Numbers: </h5>
        <ul class="ticket_list js_tickets-selected-wrap"
            @auth
            data-amount="{{ Auth::user()->availableTicketsToBuy($competition->max_tickets_per_user, $competition->id)}}"
            @elseauth
            data-amount="{{$competition->max_tickets_per_user}}"
            @endauth
            data-auth="{{Auth::check()}}">
            @foreach($competition->grouped_tickets['picked'] as $ticket)
                <li class="numbers-picked-i selected js_tickets-picked js_selected-{{$ticket}}">
                    {{$ticket}}
                </li>
            @endforeach
        </ul>
    </div>
</div>
</div>
