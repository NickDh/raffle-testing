<div class="container margin_60_35 competition-grid">
    <div class="main_title">
        <h2>Competitions</h2>
        <span>POPULAR</span>
        <p>Just a snapshot of some of our popular competitions</p>
    </div>
    <div class="row small-gutters">
        @foreach($competitions as $competition)
             @include('parts/_competition-card')
        @endforeach
    </div>
    <!-- /row -->
    <div class="row m-2">
        <div class="col-md-12 text-center">
            <a class="btn_1" href="{{route('live-competitions')}}" role="button">VIEW ALL LIVE COMPETITIONS</a>
        </div>
    </div>
</div>
