@if ($paginator->hasPages())

<div class="pagination__wrapper">
    <ul class="pagination">
        <li class="{{ ($paginator->currentPage() == 1) ? '__disable' : '' }}"><a href="{{ $paginator->url($paginator->currentPage()-1) }}" class="prev" title="previous page">❮</a></li>
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <li>
                <a class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}"
                   href="{{ $paginator->url($i) }}">{{ $i }}</a>
            </li>
        @endfor
        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? '__disable' : '' }}"><a href="{{ $paginator->url($paginator->currentPage()+1) }}" class="next" title="next page">❯</a></li>
    </ul>
</div>
@endif
