<div class="container margin_60_35">
    <div class="main_title">
        <h2>Winners</h2>
        <span>Past</span>
    </div>
    <div class="owl-carousel owl-theme winner_carousel">
        @foreach($winners as $winner)
        <div class="item">
                    <img src="{{$winner->present()->main_image}}" alt="EpicWina">
        </div>
        <!-- /item -->
         @endforeach                
    </div>
    <!-- /products_carousel -->
</div>
<!-- /container -->
