<div class="col-md-4 col-xl-4">
    <div class="grid_item">
        <figure class="m-0">
            @if(! \App\Enums\CompetitionStatusEnum::FINISHED()->is($competition->status))
                <a href="{{route('live-competition.show', $competition->slug)}}">
            @else
                <a href="{{route('live-competition.finished', $competition->slug)}}">
            @endif
                <img class="img-fluid lazy" src="{{$competition->present()->main_image}}" alt="{{$competition->title}}">
            </a>
        </figure>
        <div class="row no-gutters bg_dark text-white">
            <div class="@if(! $competition->isFinished())col-4 @else col-12 @endif text-center">
            @if(! $competition->isFinished())
                {{now()->diffInDays($competition->deadline)}} DAYS LEFT
            @else
                Finished {{date('d/m/Y \@ H:i', strtotime($competition->deadline))}}
            @endif
            </div>
            @if(! $competition->isFinished())
            <div class="col-4 text-center">
                @if(\App\Enums\CompetitionStatusEnum::PAUSED()->is($competition->status))
                PAUSED

                @elseif(\App\Enums\CompetitionStatusEnum::FINISHED()->is($competition->status))

                @elseif(\App\Enums\CompetitionStatusEnum::SOLD_OUT()->is($competition->status))
                SOLD OUT
            
                @elseif($competition->sale_price)
                ON SALE
                
                @else
                @endif
            </div>
            <div class="col-4 text-center">
                {{$competition->sold_tickets->count()}} SOLD
            </div>
            @else
            @endif
        </div> 
        @if(! \App\Enums\CompetitionStatusEnum::FINISHED()->is($competition->status))
                <a href="{{route('live-competition.show', $competition->slug)}}">
        @else
                <a href="{{route('live-competition.finished', $competition->slug)}}">
        @endif
            <h3 class="pt-2">{{$competition->title}}</h3>
        </a>
        @if(! $competition->isFinished())
        <div class="row p-2 align-items-center">
            <div class="col-md-12">
                
                <div class="price_box">
                    <span class="new_price"><span>£</span>{{$competition->sale_price ? formatValue($competition->sale_price) : formatValue($competition->price)}}</span>
                    @if($competition->sale_price)
                    <span class="old_price">£{{formatValue($competition->price)}}</span>
                    @endif
                </div>  
            </div>  
        </div>
        @else
        @endif
    </div>
    <!-- /grid_item -->
</div>
