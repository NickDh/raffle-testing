<div class="bg_dark">
    <!-- /container -->
    <div class="container margin_60_35 how-to">
        <div class="main_title">
            <h2>How To Play</h2>
            <span>IT'S EASY</span>
        </div>
        <div class="row text-center step_text">
            <div class="col-md-3">
                <img src="{{asset('img/how-to-1.svg')}}" width="270" height="138">
                <h3>Choose Your Competition</h3>
                <p>Select any competition from our live competitions.</p>
            </div>
            <div class="col-md-3">
                <img src="{{asset('img/how-to-2.svg')}}" width="270" height="138">
                <h3>Select Numbers</h3>
                <p>Pick your own numbers of let our lucky dip pick them for you.</p>
            </div>
            <div class="col-md-3">
                <img src="{{asset('img/how-to-3.svg')}}" width="270" height="138">
                <h3>Answer Question</h3>
                <p>For your entry to be submitted you will need to correctly answer our skill based question.</p>
            </div>
            <div class="col-md-3">
                <img src="{{asset('img/how-to-4.svg')}}" width="270" height="138">
                <h3>Complete Payment</h3>
                <p>Once your payment has been received we will send you confirmation of your entry. Now sit tight for the live draw!</p>
            </div>
        </div>
    </div>
    <!-- /container -->
</div>
