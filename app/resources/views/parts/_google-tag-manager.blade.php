@if(session()->exists('google_tag_manager_event'))
    <script>
        window.dataLayer = window.dataLayer || [];
        dataLayer.push(@json(session()->get('google_tag_manager_event')));
    </script>
@endif