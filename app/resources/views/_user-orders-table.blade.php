
    <table id="past-orders" class="table table-bordered">
        <thead class="thead-light">
        <tr>
            <th>Order #</th>
            <th>Date</th>
            <th>Total</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($user->paid_orders as $order)
            <tr>
                <td>{{$order->id}}</td>
                <td>{{date('M j, Y', strtotime($order->created_at))}}</td>
                <td>£{{formatValue($order->total)}}</td>
                <td>
                    <a href="{{route('order.details', $order->id)}}"
                       class="check-btn sqr-btn ">View</a>
                    <span class="solid-btn">
                        <button data-action="{{route('order.delete', $order->id)}}"
                                class="checkout-btn-submit d-inline-block w-auto p-1 ml-2 text-capitalize js_send-delete"
                                data-toggle="modal" data-target="#confirm-modal" >Delete</button>
                     </span>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

