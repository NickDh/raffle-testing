@extends('emails.order-user')

@section('title', "You've received a new order!")

@section('text', 'Please find the order details below.')

@section('bottom-text', '')