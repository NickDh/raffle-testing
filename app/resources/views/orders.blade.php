@extends('core.base')

@section('content')

<div class="top_banner">
    <div class="opacity-mask d-flex align-items-center">
        <div class="container">
            <h1>Account</h1>
        </div>
    </div>
</div>
<!-- /top_banner -->
<div class="container margin_60_35 account_wrap">
    <div class="row">
        <div class="col-md-3 account_menu">
            <h4 class="d-none d-lg-block">Account Menu</h4>
            <nav class="navbar navbar-expand-lg navbar-light p-0">
                <h4 class="d-lg-none">Account Menu</h4>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#accountnav" aria-controls="accountnav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="accountnav">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                        <a class="nav-link @if(request()->is('entries*')) active @endif" href="{{route('entries')}}"><i class="ti-ticket"></i> Entries</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link @if(request()->is('orders*')) active @endif" href="{{route('orders')}}"><i class="ti-shopping-cart"></i> Orders</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link @if(request()->is('user.info*')) active @endif" href="{{route('user.info')}}"><i class="ti-user"></i> Account Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link @if(request()->is('change-password*')) active @endif" href="{{route('change-password')}}"><i class="ti-lock"></i> Change Password</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{route('logout')}}"><i class="ti-power-off"></i> Log Out</a>
                        </li>
                        @auth
                            @if(Auth::user() && Auth::user()->isAdmin())
                        <li class="nav-item">
                        <a class="nav-link" href="{{route('dashboard::index')}}"><i class="ti-key"></i> Admin Dashboard</a>
                        </li>
                            @endif
                        @endauth
                    </ul>
                </div>
            </nav>
        </div>
        <div class="col-md-8 order_wrap mt-4">
            <h2>Orders</h2>
            <div class="card mb-3">
                <div class="card-body">
                    <div class="js_tbl-wrap" data-method="GET" data-action="{{url()->current()}}">
                        @include('parts/_orders-table')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
