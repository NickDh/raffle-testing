@extends('core.base')

@section('content')

<main>
    <div class="top_banner">
        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
            <div class="container">
                <h1>Terms & Conditions</h1>
            </div>
        </div>
    </div>
    <!-- /top_banner -->
    <div class="container margin_60_35 account_wrap">
        <div class="row">
            <div class="col-md-12">	
	
					<p>Terms and conditions of use</p>



					<p>1. Introduction &amp; Legal Undertaking</p>



					<p>1.1 Company Name (‘Promoter’, ‘our(s)’) operates various competitions resulting in the allocation of prizes in accordance with these terms and conditions on the website (‘Prize’ or ‘Prizes’) www.[Website URL] (the ‘Website’) – (each and all such competitions being referred to herein respectively as the ‘Competition’ or ‘Competitions’).</p>



					<p>1.2 By using our website and entering a Competition the entrant (‘Entrant’, ‘you’, ‘your(s)’ and/or ‘Customer’) will be deemed to have legal capacity to do so, you will have read and understood and accepted these terms and conditions and you will be bound by them and by any other requirements set out in any of the Promoter’s related promotional material.</p>



					<p>1.3 You must be at least 18 years of age to use the website; by using the website or agreeing to these terms and conditions, you warrant and represent to the promoter that you are at least 18 years of age.</p>



					<p>1.4 Competitions are governed by English law and all and/or any matters or disputes relating to the Competition will be dealt with and/or resolved under English Law and the Courts of England shall have exclusive jurisdiction.</p>



					<p>1.4 In the event that you participate in a Competition online via the Website, and by accepting these terms and conditions you hereby confirm that you are not breaching any laws in your country of residence regarding the legality of entering our Competitions. The Promoter will not be held responsible for any Entrant entering any of our Competitions unlawfully. If in any doubt you should immediately leave the Website and check with the relevant authorities in your country.</p>



					<p>1.5 The website uses cookies; by using the website or agreeing to these terms and conditions, you consent to the use of cookies in accordance with the terms of the privacy and cookies policy.</p>



					<p>2. Copyright notice</p>



					<p>2.1 Copyright (c) <em>2020</em> <em>Company Name</em>.</p>



					<p>2.2 Subject to the express provisions of these terms and conditions:</p>



					<p>(a) we, together with our licensors, own and control all the copyright and other intellectual property rights in our website and the material on our website; and</p>



					<p>(b) all the copyright and other intellectual property rights in our website and the material on our website are reserved.</p>



					<p>3. Licence to use website</p>



					<p>3.1 You may:</p>



					<p>(a) view pages from our website in a web browser;</p>



					<p>(b) download pages from our website for caching in a web browser;</p>



					<p>(c) print pages from our website;</p>



					<p>(d) stream audio and video files from our website; and</p>



					<p>(e) use our website services by means of a web browser,</p>



					<p> subject to the other provisions of these terms and conditions.</p>



					<p>3.2 Except as expressly permitted by Section 3.1 or the other provisions of these terms and conditions, you must not download any material from our website or save any such material to your computer.</p>



					<p>3.3 You may only use our website for your own personal and business purposes, and you must not use our website for any other purposes.</p>



					<p>3.4 Except as expressly permitted by these terms and conditions, you must not edit or otherwise modify any material on our website.</p>



					<p>3.5 Unless you own or control the relevant rights in the material, you must not:</p>



					<p>(a) republish material from our website (including republication on another website);</p>



					<p>(b) sell, rent or sub-license material from our website;</p>



					<p>(c) show any material from our website in public;</p>



					<p>(d) exploit material from our website for a commercial purpose; or</p>



					<p>(e) redistribute material from our website.</p>



					<p>3.6 We reserve the right to restrict access to areas of our website, or indeed our whole website, at our discretion; you must not circumvent or bypass, or attempt to circumvent or bypass, any access restriction measures on our website.</p>



					<p>4. Acceptable use</p>



					<p>4.1 You must not:</p>



					<p>(a) use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability or accessibility of the website;</p>



					<p>(b) use our website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;</p>



					<p>(c) use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;</p>



					<p>(d) conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to our website without our express written consent;</p>



					<p>(e) access or otherwise interact with our website using any robot, spider or other automated means, except for the purpose of search engine indexing;</p>



					<p>(f) violate the directives set out in the robots.txt file for our website; or</p>



					<p>(g) use data collected from our website for any direct marketing activity (including without limitation email marketing, SMS marketing, telemarketing and direct mailing).</p>



					<p>4.2 You must not use data collected from our website to contact individuals, companies or other persons or entities.</p>



					<p>4.3 You must ensure that all the information you supply to us through our website, or in relation to our website, is true, accurate, current, complete, and non-misleading.</p>



					<p>5. Registration and accounts</p>



					<p>5.1 You may register for an account with our website by completing and submitting the account registration form on our website.</p>



					<p>5.2 You must not allow any other person to use your account to access the website.</p>



					<p>5.3 You must notify us in writing immediately if you become aware of any unauthorised use of your account.</p>



					<p>5.4 You must not use any other person’s account to access the website, unless you have that person’s express permission to do so.</p>



					<p>6. User login details</p>



					<p>6.1 If you register for an account with our website, you will be asked to choose a username and password.</p>



					<p>6.2 Your username must not be liable to mislead and must comply with the content rules set out in Section 20; you must not use your account or username for or in connection with the impersonation of any person.</p>



					<p>6.3 You must keep your password confidential.</p>



					<p>6.4 You must notify us in writing immediately if you become aware of any disclosure of your password.</p>



					<p>6.5 You are responsible for any activity on our website arising out of any failure to keep your password confidential and may be held liable for any losses arising out of such a failure.</p>



					<p>7. Cancellation and suspension of account</p>



					<p>7.1 We may:</p>



					<p>(a) suspend your account;</p>



					<p>(b) cancel your account; and/or</p>



					<p>(c) edit your account details,</p>



					<p> at any time in our sole discretion without notice or explanation.</p>



					<p>7.2 You may cancel your account on our website by contacting us using the email address you registered with, making the subject heading Account Cancelation Request. This will go to our team who will process your account deletion.</p>



					<p>8. Competition Entry</p>



					<p>8.1 Multiple Competitions may be operated at the same time by the Promoter and each Competition will have a specific Prize.</p>



					<p>8.2  Availability and pricing of Competitions is at the discretion of the Promoter and will be specified at the point of sale on the Website. There is no requirement to pay to enter any Competition, as each Competition has a free entry route available – see rule below for details of how to enter for free. The availability of a free entry route to enter each Competition means that the Competition does not fall within the definition of a lottery under the Gambling Act 2005 and can be operated legally in Great Britain without any need for a licence.</p>



					<p>8.2 In order to enter a Competition, you will need to register an account on the Website (see section 5 Registration &amp; Accounts).</p>



					<p>8.3 When playing a Competition online via the Website, you must follow the on- screen instructions to: (a) select the Competition you wish to enter and when you are ready; confirm and purchase your ticket(s) to the Competition (‘Tickets’), provide your contact and payment details. You will need to check your details carefully and tick the declaration, confirming you have read and understood the Competition terms and conditions; (b) once you have purchased your Tickets, register to play the relevant Competition and when your payment has cleared we will then contact you by email to confirm your entry into the Competition. Please note that when entering online and/or by post you will not be deemed entered into the Competition until we confirm your entry which can be confirmed in your account when you login (and any such entry referred to herein as an ‘Entry’ or ‘Entries’).</p>



					<p>8.4 The Promoter reserves the right to refuse or disqualify any incomplete Entry if it has reasonable grounds for believing that an Entrant has contravened any of these terms and conditions.</p>



					<p>8.5 To the extent permitted by applicable law, all Entries become the Promoter’s property and will not be returned.</p>



					<p>8.6 The Entrant can enter each Competition up to a maximum of 25 times.</p>



					<p>8.6(a) Each account can have unlimited amount of entries, providing they are purchased on that account on behalf of other people.</p>



					<p>8.7 Each Competition closes when the last number is taken, no more Entries after this point will be accepted.</p>



					<p>8.8 All Entrants (including those entering for free) must create an account prior to entering and supply an email address to proceed in the Competition.</p>



					<p>9. Promotion Periods</p>



					<p>9.1 Each Competition will run for a specified period. Please see each Competition for details of start and end times and dates (‘Promotion Period(s)’).</p>



					<p>10.  Competition Judgement</p>



					<p>10.1 Company Name guaranteed 100% random ball spinning machine will determine the winner of each Competition. The number of Entrants who have entered the Competition will determine how many balls are entered into the machine. Each Competition will have a minimum of 49 balls and a maximum of 4000 balls. The result will be live streamed on Facebook Live (or such other live streamed internet channel as the Promoter chooses).</p>



					<p>10.2 Due to the nature of the selection, there will only be one Winner per Competition, unless the Promoter states otherwise.</p>



					<p>10.3 The Promoter will attempt to contact winners of Competitions (referred to herein as ‘Winner(s)’) using the telephone numbers and email address provided at the time of Entry (or as subsequently updated) and held securely in our database. It is the Entrant’s responsibility to ensure that these details are accurate, up to date and complete. If for any reason these details are taken down, inputted and/or submitted and/or recorded in any way by you incorrectly, the Promoter will not be held responsible for any consequences of this of whatever nature and howsoever arising. Entrants must carefully check their contact details have been recorded correctly within their account via the website.</p>



					<p>10.4 If for any reason the Promoter is unable to contact a Winner within 5 working days (which may be extended at the sole discretion of the Promoter) of the end of a Competition, or the Winner fails for whatever reason or cause to confirm acceptance of the Prize and/or the Winner is disqualified as a result of not complying with or contravening any of these terms and conditions, the Winner hereby agrees that it will immediately, irrevocably and automatically forfeit the Prize and the Prize will remain in the possession and ownership of the Promoter.</p>


					<p>10.5 Entrants who specifically consent to marketing communications will be entered onto the Promoter’s database for the purpose of conveying information as to the status of their Competition, as well as any future promotions or Competitions offered by the Promoter.</p>



					<p>10.6 The Promoter also reserves the right at its sole discretion to extend the closing date of any Competition. Each Competition can have the closing time extended by the Promoter up to 4 times. If the Competition is not sold out after the 4th extension of time, then the Prize that will be awarded will be as follows:</p>



					<p> 70% of the value of paid Entries to the Competition. Only the Competition Entrants (including free Entries) will be entered into this draw.</p>



					<p>11. Winner’s Details</p>



					<p>11.1 The Winner will be required to show proof of identification on delivery of the Prize. Any failure to meet this requirement may result in the Winner being disqualified and the Promoter retaining the Prize.</p>



					<p>11.2 All Winners will be asked for their consent by the Promoter to provide photographs and/or pose for photographs and videos and have their personal details (including details of any Prize won by them) included in marketing material. If a Winner consents to the above, the foregoing photographs, videos and marketing material may be used in future marketing and public relations by the Promoter in connection with the Competition and in identifying them as a Winner of a Competition.</p>



					<p>11.3 Following receipt and verification of the details requested above by the Promoter and provided that the Winner has satisfied these terms and conditions, the Winners will be contacted in order to make arrangements for delivery of the Prize.</p>



					<p>12. Competition Prizes</p>



					<p>12.1 The Prizes are determined, selected by all and/or some of the directors of the company and are owned by the Promoter from the date of the Competition going live on the Website to the date that the Winner receives the Prize. Details of each Prize can be found on the Website on the Competitions pages. Company Name take no responsibility for the Prize awarded after delivery has taken place. Once the Winner receives the Prize, the Promoter does not insure the Prize. No insurance comes with the Prizes and the Promoter is not responsible for the Prize once it has been handed over to the Winner.</p>



					<p>12.2 Delivery of the Prize to the Winner’s home address in Great Britain is free. The Promoter has a right to and/or may charge the Winner delivery fees if they require the Prize to be delivered to an address outside Great Britain.</p>



					<p>12.3 All Entrant expenses to collect the Prize are the sole responsibility of the Winner.</p>



					<p>12.4 The Winner hereby agrees that all Prizes are subject to and are conditional on the terms and conditions of the Promoter, Prize provider, manufacturer and/or supplier and/or anyone that is involved in the provision or delivery of the Prize to the Winner.</p>



					<p>12.5 Each Prize must be accepted as awarded and is non-transferable or convertible to other substitutes and cannot be used in conjunction with any other vouchers, offers or discounts, including without limitation any vouchers or offers of the Promoter or other Prize suppliers and/or third parties.</p>



					<p>13. Storage</p>



					<p>13.1 The Promoter can store the chosen Prize free of charge for up to 15 days after notifying the Winner, at the end of which time the Prize will be delivered to the Winner. If the Prize needs to be stored by the Promoter for more than 15 days then this shall be at the entire cost of the Winner where such cost will need to be paid by the Winner to the Promoter before the Winner receives the Prize.</p>



					<p>14. Winners’ Personal Data</p>



					<p>14.1 Subject to the Winner’s consent, the Winner may be asked to have their photo and video taken by the Promoter for promotional purposes (Public Relations and Marketing).</p>



					<p>14.2 When entering a Competition, the Entrant will give consent to use of their name, address, and/or photograph or other likeness, as well as your appearance at publicity events without any additional compensation (save for reasonable travel expenses that are approved in writing in advance by the Promoter) and as required by the Promoter.</p>



					<p>15. Limits of Liability</p>



					<p>15.1 The Promoter makes or gives no representations and/or warranties and/or assurances of whatever nature and howsoever arising (and whether in writing or otherwise) as to the quality, suitability and/or fitness for any particular purpose of any of the goods or services advertised, offered and/or provided as Prizes. Except for liability for death or personal injury caused by the negligence of the Promoter and/or for any fraudulent misrepresentations and/or for any events and/or circumstances to the extent that they cannot be excluded or limited by law. The Promoter shall not be liable for any loss suffered or sustained to person or property including, but not limited to, consequential (including economic) loss by reason of any act or omission by the Promoter, or its servants or agents, in connection with the arrangement for supply, or the supply, of any goods by any person to the Prize Winner(s) and, where applicable, to any family/persons accompanying the Winner(s), or in connection with any of the Competitions promoted by the Promoter.</p>



					<p>15.2 The total maximum aggregate liability of the Promoter to each Winner shall be limited to the total value of each Prize that has been won by the relevant Winner.</p>



					<p>15.3 The total maximum aggregate liability of the Promoter to you shall (if you are not a Winner) be limited to the amount that you have paid to enter Competitions in the first 12 months of you playing any Competition.</p>



					<p>15.4 Nothing in these terms and conditions shall prevent you making claims to the extent that you are exercising your statutory rights.</p>



					<p>16. Electronic Communications</p>



					<p>16.1 No responsibility will be accepted by the Promoter for failed, partial or garbled computer transmissions, for any computer, telephone, cable, network, electronic or internet hardware or software malfunctions, failures, connections, availability, for the acts or omissions of any service provider, internet accessibility or availability or for traffic congestion or unauthorised human act, including any errors or mistakes. The Promoter shall use its reasonable endeavours to award the Prize for a Competition to the correct Entrant. If due to reasons of hardware, software or other computer related failure, or due to human error, the Prize is awarded incorrectly, the Promoter reserves the right to reclaim the Prize and award it to the correct Entrant, at its sole discretion and without admission of liability and the Entrant that has been incorrectly awarded the Prize will immediately at the Entrant’s own cost and expense return it to the Promoter and/or pay the Promoter for that Prize (at the option of the Promoter). The Promoter shall not be liable for any economic and/or other loss and/or consequential loss suffered or sustained to any persons to whom an award has been incorrectly made, and no compensation shall be due to such persons. The Promoter shall use its reasonable endeavours to ensure that the software and Website(s) used to operate its Competitions performs correctly and accurately across the latest versions of popular internet, tablet and mobile browsers. For the avoidance of doubt, only the Ticket recorded in our systems, howsoever displayed or calculated, shall be entered into the relevant Competition and the Promoter shall not be held liable for any Competition Entries that occur as a result of malfunctioning software or other event.</p>



					<p>17. Free Entry Draw</p>



					<p>17.1 To enter any Competition draw each month for free, you must first create an account and then send your name, address, date of birth, e-mail address and contact telephone number on a postcard to the Promoter Marked Free entries and posted to: [Company Address] and must arrive by the Competition closing date. First or second-class postage must be paid. Postal Entries are limited to one Entry per person to each Competition, each entry is to be submitted individually by postcard. The Entrant must specify which Competition they wish to enter. Random number/s will be allocated to each free entry by the Promoter. All free Entries will be treated in the exact same way as a paid Entries. Where applicable, these Competition terms and conditions also apply to free Entries. All free entries are processed on Tuesday’s of each week.</p>



					<p>18. Validation</p>



					<p>18.1 The Promoter hereby reserves the right not give or make a Prize until it is satisfied that(a) the Winner has a validly registered Website account and/or is not in breach of these terms and conditions, (b) any and/or all amounts due or owing by you to the Promoter have been paid in full, (c) the identity of the Winner and his or her entitlement to receive the Prize has been established to the Promoter’s satisfaction (in particular, the Promoter reserves the right to request documentary proof of identity and not to give or make a Prize until satisfied appropriate proof of identity has been provided), and (d) the Promoter may require proof of age to be produced before giving or making a Prize. Prizes will not be given or made to Winners found to be under below the age of 16.</p>



					<p>18.2 Without prejudice to rule 18.1 above, the Promoter reserves the right not to make or give a Prize if it reasonably suspects the occurrence of fraud in relation to a Competition.</p>



					<p>18.3 The Promoter may, at its absolute and sole discretion, give or make a Prize to a person whom it is satisfied is the duly authorised representative of the Winner acting under a lawfully executed power of attorney or other equivalent authorisation.</p>



					<p>19. Your content: licence</p>



					<p>19.1 Any personal data that you supply to the Promoter or authorise the Promoter to obtain from a third party, for example, a credit card company, will be used by the Promoter to administer the Competition and fulfil Prizes where applicable. In order to process, record and use your personal data the Promoter may disclose it to</p>



					<ul><li>any credit card company whose name you give;</li><li>any person to whom the Promoter proposes to transfer any of the Promoter’s rights and/or responsibilities under any agreement the Promoter may have with you;</li><li>any person to whom the Promoter proposes to transfer its business or any part of it;</li><li>comply with any legal or regulatory requirement of the Promoter in any country; and</li><li>prevent, detect or prosecute fraud and other crime. In order to process, use, record and disclose your personal data the Promoter may need to transfer such information outside the United Kingdom, in which event the Promoter is responsible for ensuring that your personal data continues to be adequately protected during the course of such transfer.</li></ul>



					<p>19.2 You may edit your content to the extent permitted using the editing functionality made available on our website.</p>



					<p>19.3 Without prejudice to our other rights under these terms and conditions, if you breach any provision of these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may delete, unpublish or edit any or all of your content.</p>



					<p>20. Your content: rules</p>



					<p>20.1 You warrant and represent that your content will comply with these terms and conditions.</p>



					<p>20.2 Your content must not be illegal or unlawful, must not infringe any person’s legal rights, and must not be capable of giving rise to legal action against any person (in each case in any jurisdiction and under any applicable law).</p>



					<p>20.3 Your content, and the use of your content by us in accordance with these terms and conditions, must not:</p>



					<p>(a) be libellous or maliciously false;</p>



					<p>(b) be obscene or indecent;</p>



					<p>(c) infringe any copyright, moral right, database right, trade mark right, design right, right in passing off, or other intellectual property right;</p>



					<p>(d) infringe any right of confidence, right of privacy or right under data protection legislation;</p>



					<p>(e) constitute negligent advice or contain any negligent statement;</p>



					<p>(f) constitute an incitement to commit a crime, instructions for the commission of a crime or the promotion of criminal activity;</p>



					<p>(g) be in contempt of any court, or in breach of any court order;</p>



					<p>(h) be in breach of racial or religious hatred or discrimination legislation;</p>



					<p>(i) be untrue, false, inaccurate or misleading;</p>



					<p>(j) constitute spam; or</p>



					<p>(k) be offensive, deceptive, fraudulent, threatening, abusive, harassing, anti-social, menacing, hateful, discriminatory or inflammatory;</p>



					<p>20.4 You must not submit to our website any material that is or has ever been the subject of any threatened or actual legal proceedings or other similar complaint.</p>



					<p>21. Report abuse</p>



					<p>21.1 If you learn of any unlawful material or activity on our website, or any material or activity that breaches these terms and conditions then notify the Promoter via email with subject heading Report Abuse.</p>



					<p>22. Indemnity</p>



					<p>22.1 You hereby indemnify us, and undertake to keep us indemnified, against any and all losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by us to a third party in settlement of a claim or dispute) incurred or suffered by us and arising directly or indirectly out of your use of our website or any breach by you of any provision of these terms and conditions.</p>



					<p>23. Breaches of these terms and conditions</p>



					<p>23.1 Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may:</p>



					<p>(a) send you one or more formal warnings;</p>



					<p>(b) temporarily suspend your access to our website;</p>



					<p>(c) permanently prohibit you from accessing our website;</p>



					<p>(d) block computers using your IP address from accessing our website;</p>



					<p>(e) contact any or all of your internet service providers and request that they block your access to our website;</p>



					<p>(f) commence legal action against you, whether for breach of contract or otherwise; and/or</p>



					<p>(g) suspend or delete your account on our website.</p>



					<p>23.2 Where we suspend or prohibit or block your access to our website or a part of our website, you must not take any action to circumvent such suspension or prohibition or blocking (including without limitation creating and/or using a different account).</p>



					<p>24. Third party websites</p>



					<p>24.1 Our website includes hyperlinks to other websites owned and operated by third parties; such hyperlinks are not recommendations.</p>



					<p>24.2 We have no control over third party websites and their contents, and we accept no responsibility for them or for any loss or damage that may arise from your use of them.</p>



					<p>25. Variation</p>



					<p>25.1 We may revise these terms and conditions from time to time.</p>



					<p>25.2 The revised terms and conditions shall apply to the use of our website from the date of publication of the revised terms and conditions on the website, and you hereby waive any right you may otherwise have to be notified of, or to consent to, revisions of these terms and conditions.</p>



					<p>26. Assignment</p>



					<p>26.1 You hereby agree that we may assign, transfer, sub-contract or otherwise deal with our rights and/or obligations under these terms and conditions.</p>



					<p>26.2 You may not without our prior written consent assign, transfer, sub-contract or otherwise deal with any of your rights and/or obligations under these terms and conditions.</p>



					<p>27. Severability</p>



					<p>27.1 If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.</p>



					<p>27.2 If any unlawful and/or unenforceable provision of these terms and conditions would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect.</p>



					<p>30. Third party rights</p>



					<p>30.1 A contract under these terms and conditions is for our benefit and your benefit, and is not intended to benefit or be enforceable by any third party.</p>



					<p>30.2 The exercise of the parties’ rights under a contract under these terms and conditions is not subject to the consent of any third party.</p>



					<p>31. Entire agreement</p>



					<p>31.1 These terms and conditions, together with our privacy and cookies policy, shall constitute the entire agreement between you and us in relation to your use of our website and shall supersede all previous agreements between you and us in relation to your use of our website.</p>



					<p>32. Law and jurisdiction</p>



					<p>32.1 These terms and conditions shall be governed by and construed in accordance with English law.</p>



					<p>32.2 Any disputes relating to these terms and conditions shall be subject to the non-exclusive jurisdiction of the courts of England.</p>



					<p>33. Statutory and regulatory disclosures</p>



					<p>33.1 We will specify on the website or elsewhere in these terms and conditions the different technical steps you must follow to conclude a contract under these terms and conditions, and also the technical means for identifying and correcting input errors prior to the placing of your order.</p>



					<p>33.2 We will not file a copy of these terms and conditions specifically in relation to each user or customer and, if we update these terms and conditions, the version to which you originally agreed will no longer be available on our website. We recommend that you consider saving a copy of these terms and conditions for future reference.</p>



					<p>33.3 These terms and conditions are available in the English language only.</p>



					<p>34. Our details</p>



					<p>34.1 This website is a service platform owned by Raffle Labs Ltd and operated by us Company Name.</p>



					<p>34.2 We are registered in England and Wales under registration number [Company Number] and our registered office is at [Company Address].</p>



					<p>34.3 Our principal place of business is at [Company Address].</p>



					<p>34.4 You can contact us:</p>



					<p>(a) by post, to [Company Address]</p>



					<p>(b) using our website contact form;</p>



					<p>(c) by email, to info@[Website URL]</p>

                </div>
        </div>
    </div>
</main>
<!-- /main -->
@endsection
