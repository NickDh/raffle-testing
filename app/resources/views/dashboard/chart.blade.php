<div class="card">
    <div class="card-header">
        <h3 class="card-title">Sales Snapshot</h3>
    </div><!-- /.card-header -->
    <div class="card-body mb-0 pb-0 mx-0 px-0">
        <div class="chart tab-pane pb-3 px-3">
            <canvas id="statistics-chart-canvas" width="529" height="auto"></canvas>
        </div>
        <div class="row bg-light py-3 px-0 mx-0">
            <div class="col-4 text-center border-right"><div class="text-bold">{{$ordersCount}}</div><div>ORDERS</div></div>
            <div class="col-4 text-center border-right"><div class="text-bold">{{$revenue}}</div><div>SALES</div></div>
            <div class="col-4 text-center"><div class="text-bold">{{$competitionsCount}}</div><div>LIVE COMPETITIONS</div></div>
        </div>
    </div><!-- /.card-body -->
</div>

@push('after-scripts')
    <script>
        $(function () {
            var idCanvasEl = "statistics-chart-canvas";
            new Chart(document.getElementById(idCanvasEl), {
                type: 'line',
                data: {
                    labels: @json($titles),
                    datasets: [
                        {
                            backgroundColor     : 'rgba(60,141,188,0.9)',
                            borderColor         : 'rgba(60,141,188,0.8)',
                            pointRadius         : false,
                            pointColor          : '#3b8bba',
                            pointStrokeColor    : 'rgba(60,141,188,1)',
                            pointHighlightFill  : '#fffff',
                            pointHighlightStroke: 'rgba(60,141,188,1)',
                            data                :  @json($values),
                        },
                    ]
                },
                options: {
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Sales: {{array_first($titles)}} - {{array_last($titles)}}',
                        fontSize: '14',
                    },
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                display: false,
                            },
                            ticks: {
                                stepValue: 10,
                                min: 10,
                                max: {{ count($values) ? max($values) : 100}}
                            }
                        }]
                    }
                }
            });

        })
    </script>
@endpush
