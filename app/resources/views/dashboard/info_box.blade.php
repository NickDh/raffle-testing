<div class="col-lg-4 col-md-6 col-xs-12">
    <!-- small box -->

    <div class="small-box bg-dark">
        <div class="inner">

            <p class="text-uppercase">{{$title}}</p>
            <h3>{{$value}}</h3>
        </div>
        <div class="icon">
            <i class="ion ion-bag"></i>
        </div>
        @isset($text)
            <p class="small-box-footer">{{$text}}</p>
        @endisset
    </div>
</div>
