<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="custom_preloader d-flex align-items-center justify-content-center">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; display: block; shape-rendering: auto;" width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                    <g transform="rotate(0 50 50)">
                        <rect x="49.5" y="31" rx="0" ry="0" width="1" height="10" fill="#0a9447">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.8888888888888888s" repeatCount="indefinite"/>
                        </rect>
                    </g><g transform="rotate(40 50 50)">
                        <rect x="49.5" y="31" rx="0" ry="0" width="1" height="10" fill="#0a9447">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.7777777777777778s" repeatCount="indefinite"/>
                        </rect>
                    </g><g transform="rotate(80 50 50)">
                        <rect x="49.5" y="31" rx="0" ry="0" width="1" height="10" fill="#0a9447">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.6666666666666666s" repeatCount="indefinite"/>
                        </rect>
                    </g><g transform="rotate(120 50 50)">
                        <rect x="49.5" y="31" rx="0" ry="0" width="1" height="10" fill="#0a9447">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5555555555555556s" repeatCount="indefinite"/>
                        </rect>
                    </g><g transform="rotate(160 50 50)">
                        <rect x="49.5" y="31" rx="0" ry="0" width="1" height="10" fill="#0a9447">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.4444444444444444s" repeatCount="indefinite"/>
                        </rect>
                    </g><g transform="rotate(200 50 50)">
                        <rect x="49.5" y="31" rx="0" ry="0" width="1" height="10" fill="#0a9447">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.3333333333333333s" repeatCount="indefinite"/>
                        </rect>
                    </g><g transform="rotate(240 50 50)">
                        <rect x="49.5" y="31" rx="0" ry="0" width="1" height="10" fill="#0a9447">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.2222222222222222s" repeatCount="indefinite"/>
                        </rect>
                    </g><g transform="rotate(280 50 50)">
                        <rect x="49.5" y="31" rx="0" ry="0" width="1" height="10" fill="#0a9447">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.1111111111111111s" repeatCount="indefinite"/>
                        </rect>
                    </g><g transform="rotate(320 50 50)">
                        <rect x="49.5" y="31" rx="0" ry="0" width="1" height="10" fill="#0a9447">
                            <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"/>
                        </rect>
                    </g>
                </svg>
            </div>
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Send SMS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" data-original-title="" title="">
                <label> <span id="users-count"></span></label>
                 <label> <span id="count-message"></span></label>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="send-sms" type="button" class="btn btn-primary">Send SMS</button>
            </div>
        </div>
    </div>
</div>


<div class="card">
    <div class="container">
    <form id="message-form" class="form-inline row d-flex flex-wrap ">

        <div class="col-12 col-md-12 col-lg-12 d-flex align-items-end mr-0">
            <div class="col-12 col-md-12 col-lg-12 form-group form-group-select2-overflow clearfix mr-0" data-original-title="" title="" >
                <label class="d-block w-100 text-nowrap" for="6025ae2b869e7" data-original-title="" title="">Limit by Competition Entries</label>
                <select  id="competitions-ids" name="competitions_ids[]" class="form-control w-100" multiple="multiple">
                    <option value="0">Select all</option>
                </select>
            </div>
        </div>

        <div class="col-12 col-md-12 col-lg-12 d-flex flex-row align-items-end">
            <div class="col-4 col-md-4 col-lg-4 form-group clearfix w-25 flex-fill" data-original-title="" title="" >
                <label class="d-block w-100 text-nowrap" for="6025ae2b869e7" data-original-title="" title="">Limit by Registered</label>
                 <select id="registered-condition" name="registered_condition" class="form-control selectpicker w-100">
                    <option value="and">and Registered </option>
                    <option value="or">or Registered  </option>
                </select>
            </div>
            <div class="col-2 col-md-2 col-lg-2 form-group clearfix w-15 mr-0" data-original-title="" title="" >
                <label class="d-block w-100 text-nowrap" for="6025ae2b869e7" data-original-title="" title="">Days</label>
                <input type="number" min="0" id="registered-for" name="registered_for" class="form-control w-100">
            </div>

            <div class="col-4 col-md-4 col-lg-4 form-group clearfix flex-fill" data-original-title="" title="" >
                <label class="d-block w-100 text-nowrap" for="6025ae2b869e7" data-original-title="" title="">Limit by Active</label>

                <select id="active-condition" name="active_condition" class="form-control selectpicker w-100">
                    <option value="active_and">and Active </option>
                    <option value="active_or">or Active </option>
                    <option value="inactive_and">and Inactive </option>
                    <option value="inactive_or">or Inactive </option>
                </select>
            </div>
            <div class="col-2 col-md-2 col-lg-2 form-group clearfix w-15 mr-0" data-original-title="" title="" >
                <label class="d-block w-100 text-nowrap" for="6025ae2b869e7" data-original-title="" title="">Days</label>
                <input type="number" min="0" id="active-for" name="active_for" class="form-control w-100">
            </div>
        </div>


        <div class="col-12 col-md-12 col-lg-12 d-flex flex-row align-items-end mr-0">
            <div class="col-6 col-md-6 col-lg-6  form-group clearfix flex-fill" data-original-title="" title="" >
                <label class="d-block w-100 text-nowrap" for="6025ae2b869e7" data-original-title="" title="">Limit by Orders</label>
                <select name="order_condition" id="order-condition" class="form-control selectpicker w-100">
                    <option value="All">All</option>
                    <option value="Paid">Order Status Paid</option>
                    <option value="Failed">Order Status Failed</option>
                </select>
            </div>
            <div class="col-3 col-md-3 col-lg-3 form-group clearfix w-25" data-original-title="" title="" >
                <label class="d-block w-100 text-nowrap" for="6025ae2b869e7" data-original-title="" title="">Amount</label>
                <input type="number" min="0" id="order-amount" name="order_amount" class="form-control w-100">
            </div>
            <div class="col-3 col-md-3 col-lg-3  form-group clearfix w-25 mr-0" data-original-title="" title="" >
                <label class="d-block w-100 text-nowrap" for="6025ae2b869e7" data-original-title="" title="">Days</label>
                <input type="number" min="0" id="order-days" name="order_days" class="form-control w-100">
            </div>
        </div>

        <div class="col-12 col-md-12 col-lg-12 d-flex align-items-end mr-0">
            <div class="col-6 col-md-6 col-lg-6 form-group clearfix flex-fill" data-original-title="" title="" >
                <label class="d-block w-100 text-nowrap" for="6025ae2b869e7" data-original-title="" title="">Limit By Ticket Purchases</label>

                <select name="" id="tickets-condition" name="not_buy_tickets_condition" class="form-control selectpicker w-100">
                    <option value="and">and Not purchased Tickets </option>
                    <option value="or">or Not purchased Tickets </option>
                </select>
            </div>
            <div class="col-3 col-md-3 col-lg-3 form-group clearfix w-25 mr-0" data-original-title="" title="" >
                <label class="d-block w-100 text-nowrap" for="6025ae2b869e7" data-original-title="" title="">Days</label>
                <input type="number" min="0" id="tickets-days" name="not_buy_tickets_days" class="form-control w-100">
            </div>

                <input id="exclude-users" name="exclude_users" type="hidden" />
                <input id="include-users" name="include_users" type="hidden" />

            <div class="col-3 col-md-3 col-lg-3 form-group  clearfix d-flex justify-content-end  col-3 col-md-3 col-lg-3 mr-0" data-original-title="" title="" >
                {{--                <button id="send-sms" type="button" class="btn btn-primary">Send SMS</button>--}}
                <button id="clear-filters" type="button" class="btn btn-primary align-items-end " >
                    Clear filters
                </button>
            </div>
        </div>



        <div class="col-12 col-md-12 col-lg-12 d-flex align-items-end mr-0">
            <div class="form-group  clearfix col-9 col-md-9 col-lg-9 mr-0" data-original-title="" title="" >
                <label> <span id="users-count"></span></label>
                <label> <span id="count-message"></span></label>
                <label class="d-block w-100 text-nowrap" for="6025ae2b869e7" data-original-title="" title="">Message
                    <span class="custom-tooltip">
                        <i class="ml-2 fas fa-question-circle" data-original-title="" title=""></i>
                        <span class="custom-tooltip-inner">
                            To personalize messages use variables:<br>
                            first_name<br>
                            last_name<br>
                            username<br>
                        </span>
                    </span>
                </label>


                <input id="text-message" class="form-control w-100" name="text_message" type="text" >

                </input>
            </div>
            <div class="form-group  clearfix d-flex justify-content-end  col-3 col-md-3 col-lg-3 mr-0" data-original-title="" title="" >
{{--                <button id="send-sms" type="button" class="btn btn-primary">Send SMS</button>--}}
                <button id="open-modal-btn" type="button" class="btn btn-primary align-items-start" data-toggle="modal" data-target="#exampleModal">
                    Send sms
                </button>
            </div>

        </div>

    </form>
    </div>

    <div class="container">
        <div class="card-body p-0">
            <div class="col-12 pb-1">
                <table id="users-table" class="display" style="width:100%">
                </table>
            </div>
        </div><!-- /.card-body -->
    </div>
</div>

@push('after-styles')
    <link href="{{ asset('css/select2.css') }}" rel="stylesheet">
@endpush

@push('after-scripts')
    <script src="{{asset('js/datatables.min.js')}}"></script>
{{--    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>--}}
    <script src="{{asset('js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('js/dataTables.select.min.js')}}"></script>

    <script src="{{asset('js/libs/select2.min.js')}}"></script>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.css"/>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css"/>



    <script>
        $(document).ready(function() {
            $('#competitions-ids').select2({
                ajax: {
                    url: '{!! route('dashboard::sms.competitionData')!!}',
                    dataType: 'json',
                    containerCssClass : "",
                    data: function (params) {
                        return {
                            _token: "{{ csrf_token() }}",
                            search: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
            var include_users = $('#include-users');
            let savedRequest =  new URLSearchParams(window.location.search)
                console.log(savedRequest);
                    // надо попробовать обложить в html_special_char() так как запрос не распаршивается обратно
                    // после того как нормально получим данные, то надо подставить в форму
            // console.log(savedRequest.getAll());

            var table = $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching : false,

                ajax:{
                    url:  '{!! route('dashboard::sms.usersData') !!}',
                    data: function (d) {
                        d.competitions_ids = $('#competitions-ids').val();
                        d.active_condition = $('#active-condition').val();
                        d.active_for = $('#active-for').val();
                        d.registered_condition = $('#registered-condition').val();
                        d.registered_for = $('#registered-for').val();

                        d.order_condition = $('#order-condition').val();
                        d.order_amount = $('#order-amount').val();
                        d.order_days = $('#order-days').val();

                        d.not_buy_tickets_condition = $('#tickets-condition').val();
                        d.not_buy_tickets_days = $('#tickets-days').val();

                        }
                    },
                    columnDefs: [ {
                        targets: 0,
                        data: null,
                        defaultContent: '',
                        title: 'Include',
                        orderable: false,
                        className: 'select-checkbox'
                    } ],
                    select: {
                        style:    'multi',
                        selector: 'td:first-child'
                    },
                    columns: [
                        {},
                        { data: 'id', name: 'id', title:'id' },
                        { data: 'first_name', name: 'first_name', title:'First Name' },
                        { data: 'last_name', name: 'last_name', title:'Last Name' },
                        // for test (or style it and test with many competition)
                        // { data: 'competition_title', name: 'competition_title', title:'competition_title' },
                        { data: 'tickets_update', name: 'tickets_update', title:'tickets_update' },


                        { data: 'phone', name: 'phone', title:'Phone' },
                        { data: 'created_at', name: 'created_at', title:'Registered' },
                        { data: 'last_login_at', name: 'last_login_at' , title:'Last Seen' },

                        // {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
                    // initComplete: function () {
                    //     this.api().rows().select();
                    //     // this.api().columns().every(function () {
                    //     //     var column = this;
                    //     //     var input = document.createElement("input");
                    //     //     $(input).appendTo($(column.footer()).empty())
                    //     //         .on('change', function () {
                    //     //             column.search($(this).val()).draw();
                    //     //         });
                    //     // });
                    //     this.api().columns().every(function () {
                    //         var column = this;
                    //         var input = document.createElement("input");
                    //         $(input).appendTo($(column.footer()).empty())
                    //             .on('change', function () {
                    //                 var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    //
                    //                 column.search(val ? val : '', true, false).draw();
                    //             });
                    //     });
                    // }

                });

                table
                    .on( 'select', function ( e, dt, type, indexes ) {
                        let ids = table.rows( { selected: true } ).data().pluck('id').toArray()
                        include_users.val(ids);
                        $('#count-message').text('Send to ' + ids.length + ' users');
                    } )
                    .on( 'deselect', function ( e, dt, type, indexes ) {
                        let ids = table.rows( { selected: true } ).data().pluck('id').toArray()
                        include_users.val(ids);
                        $('#count-message').text('Send to ' + ids.length + ' users');
                    } );

                $('#clear-filters').on('click', function(e) {
                    $('#competitions-ids').val('');
                    $('#active-condition').val('active_and');
                    $('#active-for').val('');
                     $('#registered-condition').val('and');
                    $('#registered-for').val('');

                    $('#order-condition').val('All');
                     $('#order-amount').val('');
                     $('#order-days').val('');

                    $('#tickets-condition').val('and');
                    $('#tickets-days').val('');


                    table.draw();
                    e.preventDefault();
                });
                $('#search-form').on('submit', function(e) {
                    table.draw();
                    e.preventDefault();
                });
                $('#registered-for, #active-for,#order-amount,#order-days, #tickets-days').change( function() {
                    table.draw();
                } );
                $('#active-condition, #registered-condition,#competitions-ids,#order-condition,#tickets-condition').change( function() {
                    table.draw();
                } );
                table.on( 'draw', function () {
                     table.rows().select();

                    let ids = table.rows( { selected: true } ).data().pluck('id').toArray()
                    $('#count-message').text('Send to ' + ids.length + ' users');

                } );

            $('#exampleModal').on('shown.bs.modal', function (evt) {
                evt.preventDefault();
                $('#open-modal-btn').trigger('focus');

            })
            $('#send-sms').click(function (){

                let message = $('#text-message').val();
                let form = JSON.stringify( $('#message-form').serializeArray() );
                //let form = $('#message-form').serialize();
                let users_ids = $('#include-users').val();

                //Надо отправить список пользователей при создании
                console.log(users_ids);
                let data = {
                    "_token": "{{ csrf_token() }}",
                    'text_message' : message,
                    'form' :form,
                    'users_ids':users_ids,
                }
            $.ajax({
                    type: "POST",
                    url: '{!! route('dashboard::sms.createjob')!!}',
                    data: data,
                    beforeSend: function(){
                        $('.custom_preloader').addClass('show');
                    },
                    success: function(response) { //Данные отправлены успешно
                        $('#text-message').val('');
                        $('.custom_preloader').addClass('response').html("<div style='font-size:25px; color:green;'>Sending sms added  to queue</div>");
                        setTimeout(function(){
                            $('#exampleModal .close').trigger('click');
                            $('.custom_preloader').removeClass('show').removeClass('response');
                        }, 3000);
                    },
                    error: function(response) { //Данные отправлены успешно
                        $('.custom_preloader').addClass('response').html("<div style='font-size:25px; color:red;'>Check sending data</div>");
                        setTimeout(function(){
                            $('.custom_preloader').removeClass('show').removeClass('response');
                        }, 3000);

                    },
                });
            })

            } );
    </script>
@endpush
