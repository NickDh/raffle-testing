<div class="admin-info-boxes">

    <form class="form-inline js-submit" action="{{route('dashboard::statistics.index')}}" method="GET" data-replace-blk=".admin-info-boxes" data-success-msg="false" data-status="false" id="form-filter-1">
        <input type="hidden" name="_token" value="{{csrf_token()}}">

        <div class="form-group clearfix" data-original-title="" title="">

            <div id="date_range" class="input-group js_datetime_picker-blk" data-original-title="" title="">
                <input type="text" name="date_from" class="form-control js_datetime_picker" value="{{$date_from ?? ''}}"
                       readonly
                       data-time="true" data-seconds="true" data-date="true" data-single="false" data-ranges="true" data-format="Y/MM/DD H:mm">
                <div class="input-group-addon px-2" data-original-title="" title="">to</div>
                <input type="text" name="date_to" class="form-control js_datetime_picker-end" readonly="" value="{{$date_to ?? ''}}" data-original-title="" title="">
            </div>

        </div>

        <div class="form-group clearfix" data-original-title="" title="">

            <button type="submit" class="btn btn-success float-right ml-2" data-original-title="" title="" data-form="#form-filter-1">
                Search
            </button>
        </div>
    </form>


    <div class="   mt-5" >
        <div class="box">
            <div class="box-body">
                <div class="row">
                    @include('dashboard.info_box', ['title' => 'Total Sales', 'value' => "£ $totalSales"])
                    @include('dashboard.info_box', ['title' => 'Orders', 'value' => "$ordersCount"])
                    @include('dashboard.info_box', ['title' => 'Average Order Value', 'value' => "£ $averageOrders"])
                    @include('dashboard.info_box', ['title' => 'Coupons Used', 'value' => "£ $couponUsed"])
                    @include('dashboard.info_box', ['title' => 'Net Sales', 'value' => "£ $netSales"])
                    @include('dashboard.info_box', ['title' => 'Failed Orders Count', 'value' => $failedOrdersCount])
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->

        </div>


    </div>
</div>
