
<div class="card card-default">
    <div class="card-header with-border">
        <h2 class="card-title">Question editing</h2>
    </div>
    <div class="card-body">
        <form class="js_create-answer-form" action="{{route('dashboard::questions.update', $question->id)}}" method="POST" data-send-all-checkbox="false">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" class="js_delete_id" name="delete_id" value="">

            <div class="form-group clearfix">
                <div class="row">
                    <label for="question" class="col-sm-2 control-label">Question</label>

                    <div class="col-sm-10">
                        <input id="question" class="form-control" type="text"
                               required
                               placeholder=""
                               value="{{$question->question}}" name="question">
                    </div>
                </div>

            </div>
            <div class="form-group clearfix">
                <div class="row">
                    <label  class="col-sm-2 control-label">Answers</label>

                    <div class="col-sm-10">
                        <button type="button"  class="btn btn-flat btn-info js_last-id"
                                data-toggle="modal"
                                data-target="#modal-create-answer">Add Answers</button>
                    </div>
                </div>
            </div>

            <div class="js_wrap-items">

                @php($key = 1)
                @foreach($question->answers as $answer)
                    <div class="form-group clearfix js_answer-item">
                        <div class="row">
                            @if($answer->image)

                                <label class="col-sm-2 control-label">Answer Image</label>

                                <div class="col-sm-10">

                                    <div class="d-flex" style="align-items: flex-start;">
                                        <div class="wfix-200 mr-4">
                                            <div class="card card-default js_ui-file-preview">
                                                <div class="card-body">
                                                    <img class="img-responsive pad js_ui-input-file-preview" src="{{$answer->present()->image}}"
                                                         alt="Photo">
                                                </div>
                                                <div class="card-footer text-center ">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-flat btn-success btn-file">
                                                            <i class="fas fa-edit"></i>
                                                            <input type="file" name="answer-image-{{$key}}" class="js_ui-input-file js_dynamic-field" accept="image/*">
                                                            <input type="hidden" name="answer-image-{{$key}}-id" value="{{$answer->id}}">
                                                        </button>
                                                    </div>
                                                </div>
                                                <!-- /.box-body -->
                                            </div>
                                        </div>

                                        <input type="radio" class="radio" id="radio-answer-image-{{$key}}" name="is_true" value="answer-image-{{$key}}" @if($answer->is_correct) checked @endif>
                                        <label class="radio-lbl" for="radio-answer-image-{{$key}}"> True</label>

                                        <button type="button" class="btn  btn-flat btn-danger ml-4 js_delete-answer" data-id="{{$answer->id}}" data-key="{{$key}}"><i class="fas fa-trash"></i></button>
                                    </div>
                                </div>


                                    @else


                                        <label class="col-sm-2 control-label" for="answer-text-{{$key}}">Answer Text</label>

                                        <div class="col-sm-10">

                                            <div class="d-flex" style="align-items: flex-start;">
                                                <div class="wfix-200 mr-4">
                                                    <div class="form-group clearfix">
                                                        <input id="answer-text-{{$key}}" class="form-control js_dynamic-field"
                                                               type="text"
                                                               value="{{$answer->text}}" name="answer-text-{{$key}}">
                                                        <input type="hidden" name="answer-text-{{$key}}-id" value="{{$answer->id}}">
                                                    </div>
                                                </div>

                                                <input type="radio" class="radio" id="radio-answer-text-{{$key}}" name="is_true" value="answer-text-{{$key}}" @if($answer->is_correct) checked @endif>
                                                <label class="radio-lbl" for="radio-answer-text-{{$key}}"> True</label>

                                                <button type="button" class="btn  btn-flat btn-danger ml-4 js_delete-answer" data-id="{{$answer->id}}" data-key="{{$key}}"><i class="fas fa-trash"></i></button>
                                            </div>

                                        </div>

                                    @endif

                            </div>
                        </div>
                        @php($key++)
                        @endforeach

            </div>

            <div class="clearfix">
                <a href="" class="btn btn-flat btn-danger text-white js_ajax-by-click-btn float-left"
                   data-action="{{route('dashboard::questions.destroy', $question->id)}}" data-method="DELETE"
                   data-confirm="1" data-original-title=""
                    @if($question->competitions->count() > 0) disabled title="This question has connected competitions" @else title="Delete question" @endif>
                    <i class="fas fa-trash" data-original-title="" title=""></i>  Delete  </a>


                <div class="form-group ">
                    <button type="submit" class="btn btn-flat btn-info float-right">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="modal-create-answer" tabindex="-1" role="dialog" >
    <div class="modal-dialog  modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Answer Type</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
               <select class="js-select2 js-type-answer" name="typeAnswer" style="width: 100%;">
                    <option value="text">Text</option>
                    <option value="image">Image</option>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success js_create-answer" >Add</button>
            </div>
        </div>
    </div>
</div>

@push('after-scripts')
    <script>
        var stateForm = {
            lastId: 0,
            nameImgField: 'answer-image',
            nameTextField: 'answer-text'
        };

        var arrIdDelete = [];

        stateForm.lastId =  $('.js_dynamic-field').length;;

        $('body').on('submit', '.js_create-answer-form', function(e) {
            e.preventDefault();
            //console.log('submit')
            var form = this;
            var method = ($(form).attr('method') == undefined ? "POST" : $(form).attr('method'));
            var action = ($(form).attr('action') == undefined ? "" : $(form).attr('action'));

            var $el = $('.alert-section');

            app.bodyEl.on('click', '.js_remove-btn', function () {

                var currentBox = $(this).closest('.box');

                //removes an error alert after closing it
                $(currentBox).slideUp(1000).promise().done(function () {
                    $(currentBox).remove();
                });
            });
            var success = function(request) {
                //redirect
                if (request.redirect !== undefined) {
                    window.location.replace(request.redirect);
                    return;
                }

                var alertHtml = '<div class="card card-success card-solid ">\n                           ' +
                    ' <div class="card-header with-border">\n                                ' +
                    '<i class="fas fa-check margin-r-5"></i>\n                                ' +
                    '<h3 class="card-title"></h3>\n                                ' +
                    '<div class="card-tools float-right ">\n                                    ' +
                    '<button type="button" class="btn btn-box-tool js_remove-btn"><i class="fas fa-times"></i></button>\n                                    ' +
                    '\n                                ' +
                    '</div>\n                            ' +
                    '</div>\n                            ' +
                    '<div class="card-body">Done</div> \n                        ' +
                    '</div>';
                $el.append(alertHtml);

                $('.box-success').slideDown();

                setTimeout(function() {
                    $('.box-success').first().slideUp(500, function() {
                        $(this).remove();
                    });
                }, 3000)
            };
            var error = function(data) {
                var alertHtml = '<div class="card card-danger card-solid ">\n                           ' +
                    ' <div class="card-header with-border">\n                                ' +
                    '<i class="fas fa-check margin-r-5"></i>\n                                ' +
                    '<h3 class="card-title"></h3>\n                                ' +
                    '<div class="card-tools float-right ">\n                                    ' +
                    '<button type="button" class="btn btn-box-tool js_remove-btn"><i class="fas fa-times"></i></button>\n                                    ' +
                    '\n                                ' +
                    '</div>\n                            ' +
                    '</div>\n                            ' +
                    '<div class="card-body">Error</div> \n                        ' +
                    '</div>';
                $el.append(alertHtml);

                $('.box-danger').slideDown();

                setTimeout(function() {
                    $('.box-danger').first().slideUp(500, function() {
                        $(this).remove();
                    });
                }, 3000)

            };
            var files = $(form).find('input[type="file"]');

            //Use when need send files
            var data = new FormData();
            $.each(files, function(i, input){
                if(input.files.length > 0){
                    $.each(input.files, function(i, file){
                        data.append(input.name, file);
                    })
                }
            });

            $.each($(form).find('input:not([type=checkbox], [type=radio], [type=file]), select, textarea'), function(i, el) {
                var id = $(el).attr('name');
                var value = $(el).val();
                data.append(id, value);
            });

            $.each($(form).find('input[type=checkbox]'), function(i, el){
                var id = $(el).attr('name');
                var value = $(el).prop("checked") ? 1 : 0;
                data.append(id, value);
            });
            $.each($(form).find('input[type=radio]'), function(i, el){
                var id = $(el).attr('name');
                if($(el).prop("checked") ) {
                    data.append(id, $(el).val());
                }
            });

            $.ajax({
                url: action,
                method: method,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    success(data);
                },
                error: function(data) {
                    error(data);
                }
            })
        });




        $('body').on('click', '.js_create-answer', function(e){
            var typeAnswer = $('.js-type-answer').val();
            var $contentBlk = $('.js_wrap-items');
            stateForm.lastId += 1;
            var id;
            if($contentBlk.find('.js_answer-item').length){
                id = parseInt($contentBlk.find('.js_answer-item').last().find('.js_delete-answer').attr('data-id')) + 1;
            }
            else{
                id = 1;
            }
            if(typeAnswer === 'image'){
                $contentBlk.append(getImgField(id))
            }else{
                $contentBlk.append(getTextField(id))
            }
            app.initPluginsInWrapper('.js_create-answer-form');
            $('#modal-create-answer').modal('hide');
            $('.js_answer-item').eq(0).find('input[type=radio]').prop('checked', true);
        });
        $('body').on('click', '.js_delete-answer', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            arrIdDelete.push(id);

            //If deleted element was checked like correct answer, it will be set the first element like true
            var imageKey = '#radio-answer-image-' +$(this).attr('data-key');
            var textKey = '#radio-answer-text-' +$(this).attr('data-key');

            var imageSelected = $(imageKey).prop("checked") ? 1 : 0;
            var textSelected = $(textKey).prop("checked") ? 1 : 0;


            $(this).closest('.js_answer-item').remove();
            $('.js_delete_id').val(arrIdDelete);

            console.log(imageSelected);
            console.log(textSelected);
            if(imageSelected || textSelected){
                $('.js_answer-item').eq(0).find('input[type=radio]').prop('checked', true);
            }

        });
        function getImgField(id){
            return  '<div class="form-group clearfix js_answer-item">\n' +
                '                    <div class="row">\n' +
                '                        <label  class="col-sm-2 control-label">Answer Image</label>\n' +
                '\n' +
                '                        <div class="col-sm-10">\n' +
                '\n' +
                '                            <div class="d-flex" style="align-items: flex-start;">\n' +
                '                                <div class="wfix-200 mr-4">\n' +
                '                                    <div class="card card-default js_ui-file-preview"  >\n' +
                '                                        <div class="card-body"  >\n' +
                '                                            <img class="img-responsive pad js_ui-input-file-preview" src="" alt="Photo"\n' +
                '                                                 style="display: none;">\n' +
                '                                            <div class="mailbox-attachment-icon js_ui-input-file-default-img"  >\n' +
                '                                                <i class="fas fa-image"  ></i>\n' +
                '                                            </div>\n' +
                '                                        </div>\n' +
                '                                        <div class="card-footer text-center "  >\n' +
                '                                            <div class="btn-group"  >\n' +
                '                                                <button type="button" class="btn btn-flat btn-success btn-file"  >\n' +
                '                                                    <i class="fas fa-edit"></i>\n' +
                '                                                    <input type="file" name="'+stateForm.nameImgField +'-'+ stateForm.lastId +'" class="js_ui-input-file js_dynamic-field" accept="image/*"  >\n' +
                '                                                </button>\n' +
                '                                            </div>\n' +
                '                                        </div>\n' +
                '                                        <!-- /.box-body -->\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '\n' +
                '                                <input type="radio" class="radio" id="radio-'+stateForm.nameImgField +'-'+ stateForm.lastId +'" name="is_true" value="'+stateForm.nameImgField +'-'+ stateForm.lastId +'">\n' +
                '                                <label class="radio-lbl" for="radio-'+stateForm.nameImgField +'-'+ stateForm.lastId +'"> True</label>\n' +
                '\n' +
                '                                <button type="button" class="btn  btn-flat btn-danger ml-4 js_delete-answer" data-id="'+id+'"><i class="fas fa-trash"></i></button>\n' +
                '                            </div>\n' +
                '\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>'

        }
        function getTextField(id){
            return '<div  class="form-group clearfix js_answer-item">\n' +
                '                    <div class="row">\n' +
                '                        <label  class="col-sm-2 control-label" for="'+stateForm.nameTextField +'-'+ stateForm.lastId +'">Answer Text</label>\n' +
                '\n' +
                '                        <div class="col-sm-10">\n' +
                '\n' +
                '                            <div class="d-flex" style="align-items: flex-start;">\n' +
                '                                <div class="wfix-200 mr-4">\n' +
                '                                    <div class="form-group clearfix">\n' +
                '                                        <input id="'+stateForm.nameTextField +'-'+ stateForm.lastId +'" class="form-control js_dynamic-field" type="text"\n' +
                '                                               value="" name="'+stateForm.nameTextField +'-'+ stateForm.lastId +'">\n' +
                '                                    </div>\n' +
                '                                </div>\n' +
                '\n' +
                '                                <input type="radio" class="radio" id="radio-'+stateForm.nameTextField +'-'+ stateForm.lastId +'" name="is_true" ' +
                'value="'+stateForm.nameTextField +'-'+ stateForm.lastId +'">\n' +
                '                                <label class="radio-lbl" for="radio-'+stateForm.nameTextField +'-'+ stateForm.lastId +'"> True</label>\n' +
                '\n' +
                '                                <button type="button" class="btn  btn-flat btn-danger ml-4 js_delete-answer" data-id="'+id+'"><i class="fas fa-trash"></i></button>\n' +
                '                            </div>\n' +
                '\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>'
        }
    </script>

@endpush
