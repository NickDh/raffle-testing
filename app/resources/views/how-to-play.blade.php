@extends('core.base')

@section('content')

<main>
    <div class="top_banner">
        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
            <div class="container">
                <h1>How To Play</h1>
            </div>
        </div>
    </div>

    <div class="bg_purple how-to">
	    <div class="container margin_60">
	        <div class="row">
	            <div class="col-md-3 text-center">
	                <img src="{{asset('img/how-to-1.svg')}}" width="200" height="144">
	                <h3>Choose a prize</h3>
	                <p>Pick which prize competition you would like to enter</p>
	            </div>
	            <div class="col-md-3 text-center">
	                <img src="{{asset('img/how-to-2.svg')}}" width="200" height="144">
	                <h3>Select your tickets</h3>
	                <p>Pick how many entries you'd like by selecting numbers on the page</p>
	            </div>
	            <div class="col-md-3 text-center">
	                <img src="{{asset('img/how-to-3.svg')}}" width="200" height="144">
	                <h3>Answer the question</h3>
	                <p>Answer the multiple choice question. You must answer correctly for your entry to count.
	            </div>
	            <div class="col-md-3 text-center">
	                <img src="{{asset('img/how-to-4.svg')}}" width="200" height="144">
	                <h3>Watch the live draw</h3>
	                <p>Follow our Facebook Page and watch the winner drawn live!</p>
	            </div>
	        </div>
	    </div><!-- /container -->
	</div>
<!-- /bg_gray -->

    <div class="container margin_60_35 faq">
		<div class="main_title">
			<h2>FAQ</h2>
			<p>Need further information? You may find the answer below.</p>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="question">HOW DO I PURCHASE TICKET NUMBERS?</div>
				<div class="answer">Playing is easy, first you need to create an account by clicking here. Once you aree logged into your account navigate to the competition you wish to enter, select your numbers or use our lucky dip, answer the question and proceed to checkout.</div>
				<div class="question">WHAT HAPPENS IF THE TIMER RUNS OUT AND ALL NUMBERS ARN'T SOLD?</div>
				<div class="answer">Dont worry,  we will always hold a draw for each competition,  we will keep draw date extensions to the minimum, See our <a href="{{route('static.terms-conditions')}}">Terms & Conditions</a> for full details.</div>
				<div class="question">HOW DO YOU DRAW THE WINNER OF THE COMPETITION?</div>
				<div class="answer">We use our specialist random ball machine in order to pick our competition winner. These draws can be watched live via our facebook page.</div>
			</div>
			<div class="col-md-6">
				<div class="question">CAN I BUY MULTIPLE TICKETS FOR A COMPETITION?</div>
				<div class="answer">Our standard maximum entries per user is set to 25 for any one competition, however some competitions may be even more limited. Please check the specific competition statts to view the maximum ticket amount for that specific competition.</div>
				<div class="question">WHEN WILL I RECEIVE MY PRIZE?</div>
				<div class="answer">We aim to ship or deliver all prizes within 7 days of the draw, so including shipping time please allow upto 10 days.</div>
			</div>
		</div>
	</div>
</main>
<!-- /main -->
@endsection
