@extends('core.base')

@section('content')
<div class="top_banner">
    <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
        <div class="container">
            <h1>{{$page->title}}</h1>
        </div>
    </div>
</div>
<!-- /top_banner -->
<div class="container margin_60_35 account_wrap">
    <div class="row">
        {!! $page->content !!}
    </div>
</div>
@endsection
