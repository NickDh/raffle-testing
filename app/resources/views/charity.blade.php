@extends('core.base')

@section('content')

<main>
    <div class="top_banner">
        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
            <div class="container">
                <h1>Charity</h1>
            </div>
        </div>
    </div>

    <div class="container margin_60_35 charity">
		<div class="row align-items-center">
			<div class="col-md-6">
				<h2>Charities need our help more than ever!</h2>
				<p>Gasmonkey Competitions are pledging to donate a percentage of our profits to support our customers nominated charities.</p>
				<p>If you would like to nominate a charity then simpily rgister on our website and email info@gasmonkeycompetitions.co.uk with your name and nominated registered charity.</p>
			</div>
			<div class="col-md-6">
				<img src="{{asset('img/charity.jpg')}}">
			</div>
		</div>
	</div>
</main>
<!-- /main -->
@endsection
