@extends('core.base')

@section('content')

<div class="top_banner">
    <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
        <div class="container">
            <h1>Login</h1>
        </div>
    </div>
</div>
<!-- /top_banner -->
<div class="container margin_30">
<!-- /page_header -->
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-8 offset-md-3">
            <div class="box_account">
                <div class="form_container">
                    <h2>Login</h2>
                    <form action="{{url('login')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group">
                        <label>Username / Email</label>
                         <input type="text" name="login" class="form-control" placeholder="Username / Email" value="{{ old('login') }}" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password" value="{{ old('password') }}" required>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            @if ($errors->has('username'))
                                <div id="form-status" class="with_error">{{ $errors->first('username') }}</div>
                            @endif
                            @if ($errors->has('email'))
                                <div id="form-status" class="with_error">{{ $errors->first('email') }}</div>
                            @endif
                            @if ($errors->has('recaptcha'))
                                <div id="form-status" class="with_error">{{ $errors->first('recaptcha') }}</div>
                            @endif
                        </div>
                    </div>
                    <input type="hidden" name="recaptcha" id="recaptcha">
                    <div class="text-center"><button type="submit" class="btn_1 full-width">Login</button></div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="{{route('password.request')}}">Forgot Password?</a>
                        </div>
                    </div>
                </div>
                <!-- /form_container -->
                <p class="float-right pt-2">No account yet? <a href="{{route('user.register')}}">Create one here.</a></p>
            </div>
            <!-- /box_account -->
        </div>
    </div>
</div>
<!-- /container -->

@endsection
@push('after-scripts')
    @include('parts/_recaptcha')
@endpush
