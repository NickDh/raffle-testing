<a href="{{$link}}" class="brand-link" style="width: 100%;max-width: 240px;margin: 0 auto;">
    <img src="{{asset($icon)}}" alt="{{$text}} Logo" class="brand-image"
         style="opacity: 1;
                display: block;
                float: none;
                margin: 0 auto;
                width: 100%;
                max-height: 100%;">
    <span class="brand-text font-weight-light">{{ $text }}</span>
</a>
