@extends('core.base')

@section('content')

    <div class="breadcrumb-area bg-img">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <h1>Live Draw Results</h1>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 breadcrumb-content">
                    <ul>
                        <li>
                            <a href="{{route('main')}}">Home</a>
                        </li>
                        <li>
                            <a href="{{route('results')}}">Live Draw Results</a>
                        </li>
                        <li class="active">
                            {{$result->title}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="blog-details-area pt-20 pb-40">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="blog-details-wrap">
                        <div class="blog-details-img mb-40">
                            <img src="{{$result->present()->image}}" alt="blog" class="img-fluid">
                        </div>
                        <h3>{{$result->title}}</h3>
                        <div class="solid-btn pt-20 pb-20">
                            <a class="btn-color-white" href="{{$result->link}}" target="_blank">View Live Draw Video</a>
                        </div>
                        {!! $result->content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
