<div class="card-body">
    <h5 class="mb-4">Order #{{$order->id}}</h5>
 @foreach($order->competitionEntries as $competitionEntry)
    <!-- ONE ITEM -->
        <div class="row mb-4 align-items-center">
            <div class="col-md-5 col-lg-3 col-xl-3">
                <div class="mb-3 mb-md-0 text-center">
                    <img class="img-fluid" src="{{$competitionEntry->competition->present()->main_image()}}" alt="{{$competitionEntry->competition_title}}">
                </div>
            </div>
            <div class="col-md-7 col-lg-9 col-xl-9">
                <div>
                    <div class="d-flex justify-content-between">
                        <div>
                            <h5>{{$competitionEntry->competition_title}}</h5>
                            <p class="mb-2 text-muted text-uppercase small">Answer : {{$competitionEntry->answer}}
                                @if($competitionEntry->isAnswerCorrect())
                                    <span class="correct-answer">Correct Answer</span>
                                @else
                                    <span class="wrong-answer">Wrong Answer</span>
                                @endif
                            </p>
                            <p class="mb-3 text-muted text-uppercase small">Ticket Numbers : {{$competitionEntry->present()->ticketNumbers()}}</p>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <p class="mb-0"><span><strong>£{{$competitionEntry->present()->entryPrice()}}</strong></span></p>
                    </div>
                </div>
            </div>
        </div>
        @if(!$loop->last)
            <hr class="mb-4">
        @endif
        <!-- END ONE ITEM -->
    @endforeach
</div>
