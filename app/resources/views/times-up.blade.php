@extends('core.base')

@section('content')
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-md-6">
                <div id="confirm" class="add_top_60 add_bottom_60">
                    <div class="icon icon--order-success svg add_bottom_15">
                        <svg  xmlns="http://www.w3.org/2000/svg" width="72" height="72">
                            <g fill="none" stroke="#E20707" stroke-width="2">
                                <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
                                <path d="M23.6,47.7l25.4-25.4" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;" />
                                <path d="M23.8,22.3l25.4,25.4" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;" />
                            </g>
                        </svg>
                    </div>
                <h2>Sorry You've Run Out OF Time!</h2>
                <p>We can only hold your entries for a limited time. Please try again and navigate straight to checkout.</p>
                <a href="{{route('live-competitions')}}" class="btn_1">View Live Competitions</a>
                </div>
            </div>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
@endsection
