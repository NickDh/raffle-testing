@extends('core.base')

@section('content')

<div class="top_banner">
    <div class="opacity-mask d-flex align-items-center">
        <div class="container">
            <h1>Live Competitions</h1>
        </div>
    </div>
</div>

<div class="container margin_60_35 competition-grid">
    <div class="row small-gutters">
        @foreach($competitions as $competition)
            @include('parts/_competition-card')
        @endforeach
    </div>
    <!-- /row -->
    @include('parts/_pagination', ['paginator' => $competitions])
</div>

@endsection
