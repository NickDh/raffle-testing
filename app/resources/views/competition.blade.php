@extends('core.base')

@section('content')

<div class="container margin_60">
    <div class="row">
        <div class="col-md-6">
            <div class="all">
                @if(\App\Enums\CompetitionStatusEnum::PAUSED()->is($competition->status))
                <span class="ribbon paused">{{$competition->status}}</span>
                @elseif(\App\Enums\CompetitionStatusEnum::SOLD_OUT()->is($competition->status))
                <span class="ribbon sold-out">{{$competition->status}}</span>
                @elseif($competition->sale_price)
                <span class="ribbon sale">ON SALE</span>
                @else
                @endif
                <div class="slider">
                    <div class="owl-carousel owl-theme main">
                        <div style="background-image: url({{$competition->present()->main_image}});background-size:contain;background-repeat: no-repeat;" class="item-box"></div>
                    @foreach($competition->present()->images as $image)
                        <div style="background-image: url({{asset($image)}});background-size:contain;background-repeat: no-repeat;" class="item-box"></div>
                    @endforeach
                    </div>
                    <div class="left nonl"><i class="ti-angle-left"></i></div>
                    <div class="right"><i class="ti-angle-right"></i></div>
                </div>
                <div class="slider-two">
                    <div class="owl-carousel owl-theme thumbs">
                        <div style="background-image: url({{$competition->present()->main_image}});" class="item active"></div>
                    @foreach($competition->present()->images as $image)
                        <div style="background-image: url({{asset($image)}});" class="item"></div>
                    @endforeach
                    </div>
                    <div class="left-t nonl-t"></div>
                    <div class="right-t"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="comp_info">
                <h1>{{$competition->title}}</h1>
                @if($competition->sale_price)
                <div class="price_main"><span class="new_price">£{{formatValue($competition->sale_price)}}</span> <span class="old_price">£{{formatValue($competition->price)}}</span></div>
                @else
                <div class="price_main"><span class="new_price">£{{formatValue($competition->price)}}</span></div>
                @endif
                <div class="description pt-2 ckeditor-cnt">
                    <p>{!! $competition->short_description !!}</p>
                </div>
                <div class="comp-details card">
                    <div class="card-body">
                        @if(!(\App\Enums\CompetitionStatusEnum::LIVE()->is($competition->status)))
                            @if(\App\Enums\CompetitionStatusEnum::PAUSED()->is($competition->status))
                                <div class="text-center"><h3>Deadline {{date('jS F Y \@ H:i', strtotime($competition->realDeadline()))}}</h3></div>
                                <div class="text-center competition-message">
                                    <h4>We Have {{$competition->status}} This Competition</h4>
                                    <p>We are currently facing technical difficulties and will be resuming the competition as soon as they are resolved.</p>
                                </div>
                            @elseif(\App\Enums\CompetitionStatusEnum::SOLD_OUT()->is($competition->status))
                                <div class="text-center"><h3>Deadline {{date('jS F Y \@ H:i', strtotime($competition->realDeadline()))}}</h3></div>
                                <div class="row m-0">
                                    <div class="col-md-8 offset-md-2">
                                        <div id="single-competition" data-countdown="{{date('Y/m/d H:i:s', strtotime($competition->realDeadline()))}}"></div>
                                    </div>
                                </div>
                                <div class="text-center competition-message">
                                    <h4>This Competition Has Now Sold Out</h4>
                                    <p>Once the competition deadline has been reached it will go into a live draw</p>
                                </div>
                            @else
                                THIS COMPETITION IS {{$competition->status}}
                            @endif
                        @else
                        <div class="text-center"><h3>Deadline {{date('jS F Y \@ H:i', strtotime($competition->realDeadline()))}}</h3></div>
                        <div class="row m-0">
                            <div class="col-md-8 offset-md-2">
                                <div id="single-competition" data-countdown="{{date('Y/m/d H:i:s', strtotime($competition->realDeadline()))}}"></div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="prod_options">
                    <div class="row m-0">
                        <div class="col-md-12">
                            <div class="ticket-progress">
                                <div class="progress_count pb-1">
                                    <div class="progress_count_left">
                                        <span>Tickets Sold</span>
                                        <span class="progress-number">{{$competition->sold_tickets->count()}}</span>
                                    </div>
                                    <div class="progress_count_right">
                                        <span>Total Tickets</span>
                                        <span class="progress-number">{{$competition->tickets_count}}</span>
                                    </div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar"
                                         aria-valuemin="0"
                                         aria-valuenow="{{calculatePercentage($competition->tickets_count, $competition->sold_tickets->count())}}"
                                         aria-valuemax="{{$competition->tickets_count}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <div class="alert" role="alert">
                              <small>For all information about our entry methods including our free entry method please view our <a href="{{route('static.terms-conditions')}}">Terms and Conditions</a>.</small>
                            </div>
                        </div>
                    </div>

                    {{--Widthout tickets--}}
                    @if((\App\Enums\CompetitionStatusEnum::LIVE()->is($competition->status)) && config('project.without_tickets'))
                    <div class="row mt-4 enter-competition">
                        <div class="col-md-12">
                            <div class="step_title m-4">
                                <p>Enter Competition</p>
                            </div>
                            <div  @if(!Auth::check()) data-toggle="modal" data-target="#login-modal" @endif>
                                <div class="question-wrapper text-center">
                                    <small>You will need to answer the following question correctly for your entry to be counted.</small>
                                    <p>{{$competition->question->question}}</p>
                                    <ul class="list-inline __tickets-answer">
                                        @foreach($competition->question->answers as $answer)
                                            <li class="list-inline-item js_tickets-answer"
                                                data-id="{{$answer->id}}">
                                                @if($answer->isImage())
                                                    <img src="{{$answer->present()->image}}">
                                                    <h5>Option {{$loop->iteration}}</h5>
                                                @else
                                                    <h5>{{$answer->present()->text}}</h5>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="js_tickets-selected-wrap"
                                     @auth
                                     data-amount="{{ auth()->user()->availableTicketsToBuy($competition->max_tickets_per_user, $competition->id) }}"
                                     @elseauth
                                     data-amount="{{ $competition->max_tickets_per_user }}"
                                     @endauth
                                     data-auth="{{Auth::check()}}">

                                    <div class="step_title m-4">
                                        <p>Choose ticket quantity</p>
                                    </div>

                                    <div class="def-number-input number-input">
                                        <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
                                                class="minus"></button>
                                        <input class="quantity js_tickets-val" min="1" max="{{$maxRandomValuesForNoTicketsFeature}}" name="quantity"
                                               value=@if($maxRandomValuesForNoTicketsFeature  > 0)"1"@else"0"@endif type="number" disabled>
                                        <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                                                class="plus"></button>
                                    </div>
                                </div>
                                @if($maxRandomValuesForNoTicketsFeature <= 0)
                                    <p class="text-center">All available tickets are currently held by other users, please wait until they become available again</p>
                                @endif
                                <div class="btn_add_to_cart" >
                                    <a class="btn_1  my-4 js_tickets-random-submit" data-action="{{route('cart.add-random')}}"><i
                                                class="fas fa-shopping-basket"></i> Add to basket </a>
                                    <div id="_competition-id" class="__hidden">{{$competition->id}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    {{--End widthout tickets--}}
                    @auth
                    @if(Auth::user() && Auth::user()->isAdmin())
                    <div class="row mb-4">
                        <div class="col-md-12">
                            <a class="btn_1" href="{{route('dashboard::competition-entries.find-winner', $competition->id)}}">View Entries</a>
                        </div>
                    </div>
                    @endif
                    @endauth
                </div>
            </div>
            <!-- /comp_info -->
        </div>
    </div>
    <!-- /row -->
</div>
<!-- /container -->

{{--@if(! config('project.without_tickets'))--}}
{{-- competition on--}}
{{--@endif--}}
@if((\App\Enums\CompetitionStatusEnum::LIVE()->is($competition->status)) && ! config('project.without_tickets'))
<div class="enter-competition" @if(!Auth::check()) data-toggle="modal" data-target="#login-modal" @endif>
    <div class="container">
        <div class="main_title">
        <h2>Enter Competition</h2>
    </div>
        <div class="step_title m-4">
            <p><span>1.</span> Select your numbers</p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#lucky-dip">LUCKY DIP</a>
                    </li>
                    @foreach($competition->grouped_tickets['chunks'] as $group_name => $tickets)
                        <li class="nav-item">
                            <a class="nav-link @if($loop->first) @endif" data-toggle="tab"
                               href="#tab{{$loop->iteration}}">{{$group_name}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="tickets_wrap mt-4">
                        <div class="js_tickets-wrap">
                            @include('parts/_tickets')
                        </div>
                </div>
            </div>
        </div>
        <div class="row mt-0" @if(!Auth::check()) data-toggle="modal" data-target="#login-modal" @endif>
            <div class="col-md-12">
                <div class="step_title m-4">
                    <span>2.</span> Answer skill based question
                </div>
                <div class="question-wrapper text-center">
                    <small>You will need to answer the following question correctly for your entry to be counted.</small>
                    <p>{{$competition->question->question}}</p>
                    <ul class="list-inline __tickets-answer">
                        @foreach($competition->question->answers as $answer)
                            <li class="list-inline-item js_tickets-answer"
                                    data-id="{{$answer->id}}">
                        @if($answer->isImage())
                            <img src="{{$answer->present()->image}}">
                            <h5>Option {{$loop->iteration}}</h5>
                        @else
                            <h5>{{$answer->present()->text}}</h5>
                        @endif
                            </li>
                        @endforeach
                    </ul>

                </div>
            </div>
        </div>
        <div class="row mt-0" @if(!Auth::check()) data-toggle="modal" data-target="#login-modal" @endif>
            <div class="col-md-12 text-center">
                <div class="step_title m-4">
                    <span>3.</span> Add to basket
                </div>
                <div class="btn_add_to_cart">
                    <a class="btn_1  my-4 js_tickets-submit" data-action="{{route('cart.add')}}"><i
                                        class="fas fa-shopping-basket"></i> Add to basket (<span
                                        class="js_tickets_all">0</span>)</a>
                    <div id="_competition-id" class="__hidden">{{$competition->id}}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="competition_description bg_gray">
    <div class="container">
        <div class="main_title">
            <h2>Prize Description</h2>
        </div>
        <div class="row">
            <div class="col-md-12 ckeditor-cnt">
                <p>{!! $competition->full_description !!}</p>
            </div>
        </div>
    </div>
</div>

<!-- /tab_content_wrapper -->
<div class="container margin_60_35 competition-grid">
    <div class="main_title">
    <h2>You May Also Like...</h2>
</div>
    <div class="row small-gutters">
        @foreach($competitions as $competition)
            @include('parts/_competition-card',['competition' => $competition])
        @endforeach
    </div>
</div>
<!-- /container -->
@endsection
