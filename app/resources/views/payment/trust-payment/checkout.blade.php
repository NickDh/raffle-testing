@extends('core.base')

@section('content')
{{--  Add TrastPayment library  --}}
<script src="{{$jsLibUrl}}"></script>

<div class="top_banner">
    <div class="opacity-mask d-flex align-items-center">
        <div class="container">
            <h1>Checkout</h1>
        </div>
    </div>
</div>
<div class="content_wrap basket js_trust-payment">
    <div class="container margin_60_35">
        @if($cart->getContent()->count())
            <div class="row">
                <div class="col-lg-8 offset-md-2">
                    @if($cart->getTotal() > 0)
                        {{-- New payment method --}}
                        <div class="payment-method">
                            <div class="pay-top sin-payment">
                                <input id="payment_method_1" class="input-radio" type="radio" value="cheque"
                                       checked="checked" name="payment_method">
                                <label for="payment_method_1"> Credit / Debit Card </label>
                            </div>
                        </div>
                        <div class="card shadow mb-3">
                            <div class="card-body payment-form">
                                <div id="st-notification-frame"></div>
                                <form id="st-form" action="{{route('trust-payment.result')}}" data-cart="{{route('cart.data')}}" method="POST">
                                    {!! csrf_field() !!}

                                    @if(!$useSavedCard)
                                        <div id="st-card-number" class="st-card-number"></div>
                                        <div id="st-expiration-date" class="st-expiration-date"></div>
                                    @endif

                                    <div id="st-security-code" class="st-security-code"></div>
                                    <input id="order" type="hidden" name="order_id">
                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                    <input id="save-payment-method" type="hidden" name="save_payment_method">
                                    <div class="form-check mt-2 mb-2">
                                        <input class="form-check-input js_store-card" name="store-cards" type="checkbox"
                                               id="store-cards" @if(!$useSavedCard) @else checked @endif>
                                        <label class="form-check-label" for="store-cards">
                                            <strong>Store these card details for future purchases.</strong>
                                        </label>
                                    </div>
                                    <button type="submit" id="st-form__submit" class="st-form__submit"
                                            >Pay
                                    </button>
                                </form>
                                <script>
                                    var st = SecureTrading({
                                        jwt: @json($jwtToken),
                                        panIcon: true,
                                        submitOnSuccess: false,
                                        submitOnError: false,
                                        disableNotification: true,
                                        submitCallback: function (data){
                                            app.trustPaymentProcessor.processOrderForm();
                                        },
                                        @if($useSavedCard)
                                        fieldsToSubmit: ['securitycode'],
                                        @endif

                                        livestatus: @json($livePaymentStatus),
                                        styles: {
                                            'font-size-input': '16px',
                                            'color-input': '#495057',
                                            'background-color-input': '#fff',
                                            'border-color-input': '#ced4da',
                                            'border-size-input': '1px',
                                            'border-radius-input': '5px',
                                            'line-height-input': '1.5',
                                            'color-label': '#000',
                                            'font-size-label': '17px',
                                            'line-height-label': '1.7',
                                        }
                                    });
                                    function initTrustPayment() {
                                        st.Components();
                                    }
                                </script>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row basket_wrap">
                <div class="col-lg-8 offset-md-2">
                    <div class="card mb-3 shadow">
                        <form action="{{$orderCreatingUrl}}" method="{{$orderCreatingMethod}}"
                              @if($cart->getTotal() > 0) class="js_order-form" @endif>
                            {!! csrf_field() !!}
                            <div class="card-body">
                                <h5>Basket Total</h5>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                                        Entries <span>£{{ $cart->getSubTotal() }}</span></li>
                                    @if($cart->getDiscount()->isNotEmpty())
                                        <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                                            Coupon Code <span>- £{{$cart->getDiscountValue()}}</span></li>
                                    @endif
                                    <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                                        <div><strong>Total</strong></div>
                                        <span><strong>£{{$cart->getTotal()}}</strong></span>
                                    </li>
                                </ul>
                                <div class="form-check mt-2 mb-2" style="display: none;">
                                    <input class="form-check-input" name="agree" type="checkbox" id="checkbox-agree"
                                           checked  required>
                                </div>
                                @if($cart->getTotal() == 0)
                                    <button type="submit" class="btn_1 full-width">Checkout
                                    </button>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        @else
            <h4>Your cart is empty</h4>
        @endif
        @if(!app()->environment('local'))
            @include('parts/_body-checkout')
        @endif
    </div>
</div>
@endsection
