<?php

namespace App\PastWinners;

use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Presenter\PresentableTrait;
use Webmagic\Core\Presenter\Presenter;

/**
 * App\PastWinners\PastWinner
 *
 * @property int $id
 * @property string|null $competition
 * @property string|null $username
 * @property int|null $ticket_number
 * @property string|null $main_image
 * @property string|null $draw_video
 * @property string|null $delivery_video
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner query()
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner whereCompetition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner whereDeliveryVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner whereDrawVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner whereMainImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner whereTicketNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PastWinner whereUsername($value)
 * @mixin \Eloquent
 */
class PastWinner extends Model
{
    use PresentableTrait;

    /** @var  Presenter class that using for present model */
    protected $presenter = PastWinnerPresenter::class;

    protected $fillable = [
        'competition',
        'username',
        'ticket_number',
        'main_image',
        'draw_video',
        'delivery_video',
    ];
}
