<?php

namespace App\PastWinners;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class PastWinnerDashboardPresenter
{

    /**
     * @param $items
     * @param Dashboard $dashboard
     * @param Request $request
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function getTablePage($items, Dashboard $dashboard, Request $request)
    {
        (new TablePageGenerator($dashboard->page()))
            ->title('Past Winners')
            ->tableTitles('ID', 'Competition', 'Username', 'Ticket Number', 'Actions')
            ->showOnly('id', 'competition', 'username', 'ticket_number')
            ->items($items)
            ->withPagination($items, route('dashboard::past-winners.index',$request->all()))
            ->createLink(route('dashboard::past-winners.create'))
            ->setEditLinkClosure(function (PastWinner $item) {
                return route('dashboard::past-winners.edit', $item);
            })
        ;

        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }


    /**
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function getCreateForm()
    {
        return (new FormPageGenerator())
            ->title('Add New Winner')
            ->action(route('dashboard::past-winners.store'))
            ->ajax(true)
            ->method('POST')
            ->textInput('competition', false, 'Competition', true)
            ->textInput('username', false, 'Winner\'s Name', true)
            ->numberInput('ticket_number', false, 'Ticket #', true, 1, 1 )
            ->imageInput('main_image', '', 'Winner Image')
//            ->textInput('draw_video', '', 'Draw Video')
//            ->textInput('delivery_video', false, 'Delivery Video')
            ->submitButtonTitle('Create');
    }


    /**
     * @param Model $item
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function getEditForm(Model $item)
    {
        $formPageGenerator = (new FormPageGenerator())
            ->title("Past Winner editing" )
            ->action(route('dashboard::past-winners.update', $item))
            ->method('PUT')
            ->textInput('competition', $item, 'Competition', true)
            ->textInput('username', $item, 'Winner\'s Name', true)
            ->numberInput('ticket_number', $item, 'Ticket #', true, 1, 1 )
            ->imageInput('main_image', ['main_image' => $item->main_image ? $item->present()->main_image : ''], 'Winner Image')
//            ->textInput('draw_video', $item, 'Draw Video')
//            ->textInput('delivery_video', $item, 'Delivery Video')
            ->submitButtonTitle('Update');

        $formPageGenerator->getBox()
            ->addElement('box_footer')
            ->linkButton()
            ->icon('fa-trash')
            ->content('Delete')
            ->class('btn-danger js_ajax-by-click-btn pull-left')
            ->dataAttr('action', route('dashboard::past-winners.destroy', $item->id))
            ->dataAttr('method', 'DELETE')
            ->dataAttr('confirm', true);

        return $formPageGenerator;
    }
}
