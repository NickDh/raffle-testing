<?php


namespace App\Users;


use Webmagic\Core\Presenter\Presenter;

class UserPresenter extends Presenter
{
    /**
     * Prepare client full name
     *
     * @return string
     */
    public function prepareFullName()
    {
        return $this->entity->full_name;
    }
}
