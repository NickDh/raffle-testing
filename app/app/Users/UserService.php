<?php

namespace App\Users;



use Exception;
use Illuminate\Http\Request;

class UserService
{
    /**
     * Check request fields and update user data if it was empty
     *
     * @param $user
     * @param Request $request
     * @param UserRepo $userRepo
     * @return bool
     * @throws Exception
     */
    public function updateUserInfoByFilledForm($user, Request $request, UserRepo $userRepo)
    {
        //Prepare data for updating user info
        foreach ($user->toArray() as $name => $value) {
            if (empty($value)) {
                if($request->has($name)){
                    $updated_data[$name] = $request->get($name);
                }
            }
        }

        if(isset($updated_data)){
            if (! $userRepo->update($user->id, $updated_data)) {
                return false;
            }
        }

        return true;
    }
}
