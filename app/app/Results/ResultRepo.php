<?php

namespace App\Results;


use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Webmagic\Core\Entity\EntityRepo;

class ResultRepo extends EntityRepo
{
    protected $entity = Result::class;

    /**
     * Get all with paginate and sorting
     *
     * @param null $per_page
     * @param null $page
     * @param string $sortBy
     * @param string $sort
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws Exception
     */
    public function getByFilter($per_page = null, $page = null, $sortBy = 'created_at', $sort = 'desc')
    {
        $query = $this->query();

        $query->orderBy($sortBy, $sort);

        if (is_null($per_page) && is_null($page)) {
            return $query->get();
        }

        return $query->paginate($per_page, ['*'], 'page', $page);
    }
}
