<?php

namespace App\Jobs;

use App\Marketing\Twilio\SendedSms;

use App\Marketing\Twilio\TwilioMessage;
use App\Marketing\Twilio\SmsTableService;
use App\Marketing\Twilio\SmsRepo;
use App\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class SendTwilioSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The podcast instance.
     *
     * @var \App\Marketing\Twilio\TwilioMessage
     */
    protected $twilio_sms_sended;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(SendedSms $twilio_sms_sended)
    {
        $this->twilio_sms_sended = $twilio_sms_sended;
    }

    /**
     * Execute the job.
     * @param SmsRepo $smsRepo
     * @return void
     */
    public function handle(SmsRepo $smsRepo)
    {

        $twilioMessage = $smsRepo->getByID($this->twilio_sms_sended->message_id);

        $user = User::find($this->twilio_sms_sended->user_id);

        Log::debug('SendTwilioStart',['message'=> $twilioMessage->text, 'message'=> $user->first_name,]);

        $response = $twilioMessage->sendPreparedSms($user);

        $this->twilio_sms_sended->saveResponse($response);

        Log::debug('Response save',['message'=> $twilioMessage->text, 'message'=> $user->first_name,]);


        //$this->twilio_sms_sended->save();

    }
}
