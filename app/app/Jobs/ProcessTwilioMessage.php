<?php

namespace App\Jobs;

use App\Marketing\Twilio\SendedSms;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Marketing\Twilio\TwilioMessage;
use Illuminate\Support\Facades\Log;

class ProcessTwilioMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * The podcast instance.
     *
     * @var \App\Marketing\Twilio\TwilioMessage
     */
    protected $message;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TwilioMessage $message)
    {
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Log::debug('processSending',['id'=>$this->message->id,'text'=>$this->message->text]);


        $users = $this->message->users()->get();

        foreach ($users as $user){
            // prepare message sent
           $sendedSms = SendedSms::create([
                'message_id'=>$this->message->id,
                'user_id'=>$user->id,
               'created_at' => Carbon::now(),
               'updated_at' => Carbon::now(),
            ]);
            SendTwilioSms::dispatch($sendedSms)->onQueue('twiliosms');
        }
    }
}
