<?php

namespace App\Jobs;

use App\Competitions\Competition;
use App\Competitions\CompetitionRepo;
use App\Enums\CompetitionStatusEnum;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FinishCompetitionStatus extends Job
{
    /** @var int  */
    protected $competition_id;

    /**
     * Create a new job instance.
     *
     * @param int $competition_id
     */
    public function __construct(int $competition_id)
    {
        $this->competition_id = $competition_id;
    }

    /**
     * Execute the job.
     *
     * @param CompetitionRepo $competitionRepo
     * @throws Exception
     */
    public function handle(CompetitionRepo $competitionRepo)
    {
        /** @var Competition $competition */
        $competition = $competitionRepo->getByID($this->competition_id);
        if(!$competition){
            logger('Can\'t update status for competition:'. $this->competition_id);
            return;
        }

        logger('check time: ' . $competition->id);
        logger('now:' .Carbon::now()->toDateTimeString());
        logger('deadline:' .$competition->realDeadline()->toDateTimeString());
        logger('Is all tickets sold:' .$competition->isAllTicketsSold() ? 'Yes':'No');

        if ($competition->isDeadlineExpired() && $competition->isAllTicketsSold()) {
            logger('Status set to "Finished"');
            $competitionRepo->update($competition->id, [
                'status' => CompetitionStatusEnum::FINISHED
            ]);
        } else {
            logger('Status was not updated');
        }
    }
}
