<?php

namespace App\Mail;

use App\Settings\SettingsRepo;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactFormMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $message;
    /**
     * @var string
     */
    private $name;

    /**
     * Create a new message instance.
     *
     * @param string $email
     * @param string $message
     * @param string $name
     */
    public function __construct(string $email, string $message, string $name = '')
    {
        $this->email = $email;
        $this->message = $message;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function build()
    {
        $settingsRepo = app()->make(SettingsRepo::class);
        $email = $settingsRepo->getAdminEmail();

        return $this->view('emails.contact')
            ->to($email)
            ->with([
            'name' => $this->name,
            'email' => $this->email,
            'send_message' => $this->message,
        ]);
    }
}
