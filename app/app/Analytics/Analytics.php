<?php

namespace App\Analytics;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Analytics\Analytics
 *
 * @property int $id
 * @property string $head_all
 * @property string $body_all
 * @property string $body_checkout
 * @property string $body_successful_order
 * @property string $body_unsuccessful_order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Analytics newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Analytics newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Analytics query()
 * @method static \Illuminate\Database\Eloquent\Builder|Analytics whereBodyAll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Analytics whereBodyCheckout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Analytics whereBodySuccessfulOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Analytics whereBodyUnsuccessfulOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Analytics whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Analytics whereHeadAll($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Analytics whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Analytics whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Analytics extends Model
{
    protected $table = 'analytics';

    protected $fillable = [
        'head_all',
        'body_all',
        'body_checkout',
        'body_successful_order',
        'body_unsuccessful_order',
    ];
}
