<?php

namespace App\Settings;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Settings\Settings
 *
 * @property int $id
 * @property int $cart_lifetime
 * @property string|null $admin_email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $google_analytics
 * @property int $view_sort_by
 * @property string|null $custom_css
 * @property int $random_generator_value
 * @method static \Illuminate\Database\Eloquent\Builder|Settings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Settings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Settings query()
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereAdminEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereCartLifetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereCustomCss($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereGoogleAnalytics($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereRandomGeneratorValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Settings whereViewSortBy($value)
 * @mixin \Eloquent
 */
class Settings extends Model
{

    protected $table = 'settings';

    protected $fillable = [
        'cart_lifetime',
        'admin_email',
        'google_analytics',
        'view_sort_by',
        'custom_css',
        'random_generator_value'
    ];
}
