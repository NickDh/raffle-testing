<?php

namespace App\PromoCodes;

use App\Enums\PromoCodeStatusTypesEnum;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;

class PromoCodeService
{
    /**
     * @var PromoCodeRepo
     */
    protected $repo;

    /**
     * CompetitionService constructor.
     */
    public function __construct()
    {
        $this->repo = new PromoCodeRepo();
    }

    /**
     * Find all expired promo codes and update their statuses
     *
     * @throws Exception
     */
    public function checkAndUpdateStatuses()
    {
        $expired_promo_codes = $this->repo->getExpiredCodes();
        foreach ($expired_promo_codes as $promo_code) {
            $this->repo->update($promo_code->id, [
                'status' => PromoCodeStatusTypesEnum::EXPIRED(),
            ]);
        }
    }

    /**
     * Check if promo valid by dates and used statuses
     *
     * @param string $code
     * @param int|null $user_id
     * @return bool|Model|string|null
     * @throws Exception
     */
    public function getIfValid(string $code, int $user_id = null)
    {
        $this->checkAndUpdateStatuses();
        $promo_code = $this->repo->getByCode($code);

        if (!$code || is_null($promo_code)) {
            return false;
        }

        if($promo_code->expires_at && today()->greaterThan(Carbon::parse($promo_code->expires_at))){
            return false;
        }

        $orders = $promo_code->paid_orders;

        //Check if used time code valid
        if($promo_code->max_use > 0 && $orders->count() >= $promo_code->max_use){
            return false;
        }

        //Check if used time per person code valid
        if($promo_code->max_use_per_user > 0 && $orders->where('user_id', $user_id)->count() >= $promo_code->max_use_per_user){
            return false;
        }

        return $promo_code;
    }
}
