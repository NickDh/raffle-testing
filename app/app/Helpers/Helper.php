<?php


use Carbon\Carbon;

function convertDateForJsTimer(string $date)
{
    return date('d F Y H:i:s', strtotime($date)) . " GMT+01:00"; //(London = GMT+01:00)
}


function calculatePercentage($total, $x)
{
    if ($total == 0) {
        return 0;
    }
    return ($x * 100) / $total;
}

function checkIfDateGreaterThanToday(string $date)
{
    return Carbon::parse( $date )->gte( Carbon::now()->toDateString());
}

function formatValue($value)
{
    return number_format($value, 2, '.', '');
}

if (! function_exists('marketingSetting')) {

    function marketingSetting($key, $default = null)
    {
        if (is_null($key)) {
            return new \App\Marketing\MarketingSettings();
        }

        if (is_array($key)) {
            return \App\Marketing\MarketingSettings::set($key[0], $key[1]);
        }

        $value = \App\Marketing\MarketingSettings::get($key);

        return is_null($value) ? value($default) : $value;
    }
}
