<?php


namespace App\Pages;


use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;

class PagesRepo extends EntityRepo
{
    /** @var string  */
    protected $entity = Page::class;

    /**
     * @param string $slug
     *
     * @return Model|null
     * @throws Exception
     */
    public function getBySlug(string $slug)
    {
        $query = $this->query()->where('slug', $slug);

        return $this->realGetOne($query);
    }

    /**
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getPagesForFooterLinks()
    {
        $query = $this->query()
            ->where('footer_link', 1);

        return $this->realGetMany($query);
    }

    /**
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getPagesForHeaderLinks()
    {
        $query = $this->query()
            ->where('header_link', 1);

        return $this->realGetMany($query);
    }
}