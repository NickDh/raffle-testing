<?php


namespace App\Pages;


use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\Exceptions\PresenterException;
use Webmagic\Core\Presenter\PresentableTrait;

/**
 * Class Page
 *
 * @method PagePresenter present()
 * @package App\Pages
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property int $header_link
 * @property int $footer_link
 * @property string|null $footer_column
 * @property string|null $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereFooterColumn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereFooterLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereHeaderLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Page extends Model
{
    use PresentableTrait;

    /** @var string  */
    protected $presenter = PagePresenter::class;

    /** @var string[]  */
    protected $fillable = [
      'title',
      'slug',
      'header_link',
      'footer_link',
      'footer_column',
      'content'
    ];

    /**
     * @return bool
     */
    public function showInHeader(): bool
    {
        return $this->header_link == 1;
    }

    /**
     * @return bool
     */
    public function showInFooter(): bool
    {
        return $this->footer_link == 1;
    }

    /**
     * @return bool
     * @throws PresenterException
     */
    public function isCurrent()
    {
        return request()->url() == $this->present()->link();
    }
}