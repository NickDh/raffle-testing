<?php

namespace App\Pages\Dashboard;

use App\Pages\Page;
use Illuminate\Pagination\AbstractPaginator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Elements\Icons\Icon;
use Webmagic\Dashboard\Elements\Links\Link;

class PagesTablePageGenerator extends TablePageGenerator
{
    public function __construct()
    {
        parent::__construct(null);
        $this->prepare();
    }

    /**
     * Prepare basic params
     *
     * @throws NoOneFieldsWereDefined
     */
    public function prepare()
    {
        $this
            ->createLink(route('dashboard::pages.page.create'))
            ->setEditLinkClosure(function (Page $page){
                return route('dashboard::pages.page.edit', $page['id']);
            })
            ->setDestroyLinkClosure(function (Page $page){
                return route('dashboard::pages.page.destroy', $page['id']);
            })
            ->tableTitles('ID', 'Title', 'Slug', 'Header', 'Footer', 'Footer column')
            ->showOnly('id', 'title', 'slug', 'header', 'footer', 'footer_column')
            ->setConfig([
                'title' => function(Page $page){
                    return (new Link())->link($page->present()->link())->content($page->title);
                },
                'header' => function(Page $page){
                    return $page->showInHeader() ? (new Icon())->icon('fa-check') : '';
                },
                'footer' => function(Page $page){
                    return $page->showInFooter() ? (new Icon())->icon('fa-check') : '';
                }
            ])
        ;
    }

    /**
     * @param AbstractPaginator $paginator
     */
    public function itemsWithPagination(AbstractPaginator $paginator)
    {
        $this
            ->items($paginator->items())
            ->withPagination($paginator, route('dashboard::pages.page.index'));
    }
}