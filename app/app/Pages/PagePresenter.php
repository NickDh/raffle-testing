<?php


namespace App\Pages;


use Webmagic\Core\Presenter\Presenter;

class PagePresenter extends Presenter
{
    /**
     * @return string
     */
    public function link()
    {
        return route('page', $this->entity->slug);
    }
}