<?php

namespace App\LiveDraws;

use Illuminate\Database\Eloquent\Model;

/**
 * App\LiveDraws\LiveDraw
 *
 * @property int $id
 * @property string $draw
 * @property string $date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|LiveDraw newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LiveDraw newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LiveDraw query()
 * @method static \Illuminate\Database\Eloquent\Builder|LiveDraw whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LiveDraw whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LiveDraw whereDraw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LiveDraw whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LiveDraw whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LiveDraw extends Model
{
    protected $fillable = [
        'draw',
        'date'
    ];
}
