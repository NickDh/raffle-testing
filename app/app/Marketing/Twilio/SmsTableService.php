<?php

namespace App\Marketing\Twilio;


use App\Jobs\ProcessTwilioMessage;
use App\Jobs\SendTwilioSms;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SmsTableService
{

    /**
     * @param Request $request
     * User filter for sms - request processing
     */
    static public function processQuery(Request $request){

        $users = DB::table('users')->select([
            DB::raw("users.id as id"),
            'users.first_name',
            'users.last_name',
            'users.phone',
            DB::raw('count(competition_entries.user_id) AS count'),
            'users.created_at',
            'users.last_login_at',
            'competition_entries.competition_title',
            'competition_entries.competition_id',
            'orders.status',
            'orders.total',
            'orders.updated_at as orders_update',
            'tickets.updated_at as tickets_update',
            'tickets.number',
            'tickets.status as tickets_status',
        ])->leftJoin('competition_entries','competition_entries.user_id','=','users.id')
            ->leftJoin('orders','orders.user_id','=','users.id')
            ->leftJoin('tickets','tickets.user_id','=','users.id')
            ->groupBy('users.id');

        Log::debug('message-debug',[
            'competitions_ids' => $request->get('competitions_ids'),
            'exclude_users' => $request->get('exclude_users'),
        ]);

        if ($competitions_ids = $request->get('competitions_ids')) {
            $users->whereIn('competition_entries.competition_id', $request->get('competitions_ids'));
        }


        if ($registered_for = $request->get('registered_for')) {
            //check condition
            $date = Carbon::now()->subDays($registered_for);
            if ($request->get('registered_condition') == 'and'){
                $users->whereDate('users.created_at', '>=',  $date);
            }else{
                $users->orWhereDate('users.created_at', '>=',  $date);
            }
        }
        if ($active_for = $request->get('active_for')) {
            $date = Carbon::now()->subDays($request->get('active_for'));
            $active_condition = $request->get('active_condition');
            if ($active_condition == 'active_and') {
                $users->whereDate('users.last_login_at', '>=', $date);
            } elseif($active_condition == 'inactive_and'){
                $users->whereDate('users.last_login_at', '<=', $date);
            }

            elseif($active_condition == 'active_or'){
                $users->where(function($query) use ($date)
                {
                    $query->where('users.subscription',1)
                        ->where('role_id',2)
                        ->orWhereDate('users.last_login_at', '>=', $date);
                });
               // $users->orWhereDate('users.last_login_at', '<=', $date);
            }
            elseif($active_condition == 'inactive_or'){
                $users->where(function($query) use ($date)
                {
                    $query->where('users.subscription',1)
                        ->where('role_id',2)
                        ->orWhereDate('users.last_login_at', '<=', $date);
                });
            }
        }
        if ($exclude_ids = $request->get('exclude_users')) {
            $users->whereNotIn('users.id', $exclude_ids);
        }
        if ($status = $request->get('order_condition')){
            if ($status === 'Paid'){
                $users->where('orders.status', '=',  'Paid');
            }
            if ($status === 'Failed'){
                $users->where('orders.status', '=',  'Failed');
            }
        }
        if ($order_amount = $request->get('order_amount')) {
            if ($orderDays = Carbon::now()->subDays($request->get('order_days'))){
                $users->whereDate('orders.updated_at', '>=',  $orderDays);
            }
            $users->where('orders.total', '>=',  $order_amount);
        }


        if ($request->get('not_buy_tickets_days')){

            $ticketDays = Carbon::now()->subDays($request->get('not_buy_tickets_days'));

            $not_buy_tickets_condition = $request->get('not_buy_tickets_condition');
            if ($not_buy_tickets_condition == 'and') {
                $users->whereDate('tickets.updated_at', '>=',  $ticketDays);
            }else{
                $users->orWhereDate('tickets.updated_at', '>=',  $ticketDays);

            }
        }

        $users->where('users.subscription',1)->where('role_id',2);


        return $users;
    }

}