<?php

namespace App\Marketing\Twilio;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;


class SendedSms extends Model
{
    protected $table = 'twilio_sended_sms';

    # id, message_id  created_at, updated_at   ,
    #// date_created, date_sent, date_updated, messaging_service_sid, error_code, error_message, price, sid, uri, status,


//    /** @var string[]  */
//    protected $fillable = [
//        ''
//    ];
    protected $guarded = [];

    public function saveResponse($response){
        $this->update([
                'status' => $response->__get('status'),
                'uri' => $response->__get('uri'),
                'sid' => $response->__get('sid'),
                'data'=>$response->__toString(),
                'date_created' => $response->__get('dateCreated'),
                'date_sent' => $response->__get('dateSent'),
                'date_updated' => $response->__get('dateUpdated'),
            ]);

    }






}
