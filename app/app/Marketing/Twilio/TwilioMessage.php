<?php

namespace App\Marketing\Twilio;

use App;
use App\Jobs\SendTwilioSms;
use App\Marketing\Twilio\TwilioService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Marketing\MarketingSettings;
use App\Users\User;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class TwilioMessage extends Model
{

    //TWILIO_SID;
    //TWILIO_TOKEN
    //TWILIO_FROM
    //protected $fillable=[''];
    protected $guarded = [];
    protected $table = 'twilio_message';

    protected $from;

    /**

     * @return HasOne
     */
    public function users()
    {
        return $this->belongsToMany('App\Users\User', 'table_twilio_message_users', 'message_id', 'user_id')->withPivot('status');
    }

    /**

     * @return HasOne
     */
    public function sendedSms()
    {
        return $this->hasMany(SendedSms::class,'message_id','id');
    }

    public function getTwilioSettingsByKey($key){
        return MarketingSettings::where('key',$key)->first()->value;
    }

    public function setQuery($query){
        $this->query = serialize($query);
    }

    public function getQuery(){
        return json_decode($this->query);
    }

    public function formatQueryToString(){
        $result = $this->getQuery();

        $str = '';
        foreach ($result as $name =>$value){
            $str .=$value->name . ' ' . $value->value. ' ';
        }

        return $str;
    }

    public function prepareSms(){

        $users = $this->users()->get();

        foreach ($users as $user){
            // prepare message sent

            $this->sendedSms()->create([
                'message_id'=>$this->id,
                'user_id'=>    $user->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'status' => 'created',
            ]);

        }
    }

    /**
     * For use in queue ProcessTwilioMessage
     *
     * @param $user
     * @return mixed
     */
    public function sendPreparedSms($user){
        $this->from = marketingSetting('TWILIO_FROM');
        $callback = route('twillio');

        //Message replace vars;
        $message = str_replace(['first_name','last_name','user_name'],[$user->first_name,$user->last_name,$user->getUserName()],$this->text);

        return $this->messageSendOne($user->phone,$message, $callback);
    }

    public function messageSendOne($phone,$message,$statusCallback = null){

        $twilioService = new TwilioService();


        return $twilioService->sendMessage($phone,$message,$statusCallback);
    }


    /** For sending one message
     *
     * @param $phone
     * @param $message
     * @param null $callback
     * @return mixed
     */
    public function messageSend($user)
    {
        $this->from = $this->marketingSetting('TWILIO_FROM');

        $callback = route('twillio');

        //Message replace vars;
        $message = str_replace(['first_name','last_name','user_name'],[$user->first_name,$user->last_name,$user->getUserName()],$this->text);

        //$this->users()->attach($user->id);

        if (!empty($user->phone)){
            $response = $this->messageSendOne($user->phone,$message, $callback);

            if ( $sid= $response->__get('sid')) {
                return $this->storeResponseToHistory($user,$response);
            }
        }

    }

    public function storeResponseToHistory($user,$response){


        $sendedSms = SendedSms::findOrCreate([
            'user_id' =>$user->id,
            'message_id' => $this->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'status' => $response->__get('status'),
            'uri' => $response->__get('uri'),
            'sid' => $response->__get('sid'),
            'data'=>$response->__toString(),
            'date_created' => $response->__get('dateCreated'),
            'date_sent' => $response->__get('dateSent'),
            'date_updated' => $response->__get('dateUpdated'),
        ]);


         return $sendedSms;
    }
}
