<?php

namespace App\Marketing\Twilio\Dashboard;


use App\Marketing\Twilio\TwilioMessage;

use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\TableFilterGenerator;
use Webmagic\Dashboard\Components\TableGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Elements\Links\LinkButton;
use Webmagic\Dashboard\Pages\BasePage;
use \App\Marketing\Twilio\Dashboard\SmsFilter as SmsFilter;

class SmsHistoryTableFilterGenerator extends TableFilterGenerator
{
    /**
     * SmsHistoryTableFilterGenerator constructor.
     */

    /**
     * Add simple select filed to filter
     *
     * @param string  $key
     * @param array   $options
     * @param Request $request
     * @param string  $label
     *
     * @param bool    $withAny
     *
     * @return $this
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function datePickerInput(
        string $key,
        array $options,
        Request $request,
        string $label = '',
        bool $withAny = true

    ) {
        $name = "filter[$key]";
        $selectedRequestKey = "filter.$key";
        $options = $withAny ? array_prepend($options, 'Any', 'any') : $options;

        $this->datePickerJS($name, $request->input($selectedRequestKey),$label , false,);
        return $this;
    }

}