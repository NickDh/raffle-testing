<?php

namespace App\Marketing\Twilio\Dashboard;


use App\Marketing\Twilio\TwilioMessage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\AbstractPaginator;
use Webmagic\Dashboard\Components\TableFilterGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Elements\Links\LinkButton;
use Webmagic\Dashboard\Pages\BasePage;

use \App\Marketing\Twilio\Dashboard\SmsFilter as SmsFilter;

class SmsHistoryTablePageGenerator extends TablePageGenerator
{
    /**
     * CompetitionsTablePageGenerator constructor.
     */
    public function __construct()
    {
        parent::__construct(null);
    }

    /**
     * @param AbstractPaginator       $paginator
     *
     * @param SmsFilter|null $filter
     *
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function prepare(AbstractPaginator $paginator, SmsFilter $filter = null)
    {
        $this
            ->title('SMS History')
            ->tableTitles('ID', 'text','created_at','success','queued','failed','Users' )
            ->showOnly('id', 'text','created_at','success','queued','failed','usercount' )

            ->setConfig([
                'success' => function (TwilioMessage $item) {
                    $sendedSms = $item->sendedSms();
                    return $sendedSms->where('status','delivered')->count();
                },
                'queued' => function (TwilioMessage $item) {
                    $sendedSms = $item->sendedSms();
                    return $sendedSms->where('status','queued')->count();
                },
                'failed' => function (TwilioMessage $item) {
                    $sendedSms = $item->sendedSms();
                    return $sendedSms->where('status','created')->count();
                },
                'usercount' => function (TwilioMessage $item) {
                    $countUser = $item->users()->count();
                    return $countUser;
                },
            ])
            ->addControls()
            ->items($paginator->all())
            ->withPagination($paginator, request()->url());
            //->addManualSorting();

        $keyword = $filter ? $filter->getSearchPhrase() : request()->get('keyword' ,'');

        $this->addFiltering()
            ->action(request()->url())
            ->method('GET')
            ->textInput('keyword', $keyword, '', false,['placeholder'=>'search by ...'])
           ->datePickerInput('from',[],request(),'',true)
            ->datePickerInput('to',[],request(),'',true)
            ->submitButton('Search');
    }

    /**
     *  Add filtering fot table
     *
     * @return SmsHistoryTableFilterGenerator
     * @throws \Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable
     * @throws \Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined
     */
    public function addFiltering()
    {
        $filterGenerator = (new SmsHistoryTableFilterGenerator($this, '.' . $this->getTableBlockIdentifier()));

        $this->filter = $filterGenerator->getForm();

        // Add form to table
        $this->getTable()
            ->addWrapperClasses(' margin ')
            ->filterArea($this->filter);

        return $filterGenerator;
    }

//    /**
//     * @return $this
//     * @throws FieldUnavailable
//     * @throws NoOneFieldsWereDefined
//     */
//    protected function addManualSorting()
//    {
//        $this->manualSorting(
//            route('dashboard::competitions.position'),
//            // Function for getting ID from items. Item from collection will be put in this function
//            function ($item) {
//                return $item['id'];
//            },
//            'POST'
//        );
//
//        return $this;
//    }

    /**
     * @return $this
     */
    protected function addControls()
    {
        $this->addElementsToToolsCollection(
            function (TwilioMessage $message) {
                return $this->prepareControlButtons($message);
            }
        );

        return $this;
    }

    /**
     * @param TwilioMessage $message
     *
     * @return string
     */
    protected function prepareControlButtons(TwilioMessage $message)
    {
        $html = '<div style="width:300px;">';
        $html .= $this->getEditBtn($message);
        $html .= $this->getDeleteBtn($message);


//        $html .= $this->getPublishStatusBtn($competition);
//        $html .= $this->getEditBtn($competition);
////        $html .= $this->getFindWinnerBtn($competition);
//        $html .= $this->getShowBtn($competition);
//        $html .= $this->getPauseStatusBtn($competition);
        $html .= '</div>';

        return $html;
    }

    /**
     * @param Model $item
     * @return mixed
     */
    protected function getEditBtn(Model $item)
    {
        $btn = (new LinkButton())
            ->icon('fa-comment')
            ->class('btn-success text-white')
            ->js()->tooltip()->regular($item->formatQueryToString());


        return $btn->render();
    }

    /**
     * @param Model $item
     * @return mixed
     */
    protected function getDeleteBtn(Model $item)
    {

        $btn = (new LinkButton())
            ->icon('fa-trash')
            ->class('btn-warning text-white js_ajax-by-click-btn')
            ->dataAttr('action', route('dashboard::sms.destroy', [$item->id,]))
            ->dataAttr('method', 'DELETE')
            ->dataAttr('replace-blk', ".js_item_$item->id")
            ->dataAttr('modal-hide', 'false')
            ->dataAttr('title', "Delete message")
            ->dataAttr('confirm', true);


        return $btn->render();
    }

}
