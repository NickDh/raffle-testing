<?php


namespace App\Marketing\Twilio\Dashboard;


use App\Core\SessionFilter;
use Illuminate\Http\Request;

class SmsFilter extends SessionFilter
{
    /** @var string */
    protected  $searchPhrase = null;

    /**
     * @param Request|null $request
     */
    public function loadParams(Request $request = null)
    {
        $request = $request ?? request();

        $this->searchPhrase = $request->get('keyword', null);

        $this->updateSessionStorage('searchPhrase');
    }

    /**
     * Load params from session
     */
    public function initFromSession()
    {
        $this->loadFromSessionStorage('searchPhrase');
    }

    /**
     * @return bool
     */
    public function isSearchPhraseDefined(): bool
    {
        return $this->searchPhrase !== null;
    }

    /**
     * @return string|null
     */
    public function getSearchPhrase()
    {
        return $this->searchPhrase;
    }
}