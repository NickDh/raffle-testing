<?php
namespace App\Marketing\Twilio;

use Aloha\Twilio\Support\Laravel\Facade as Twilio;
use App\Jobs\SendTwilioSms;
use App\Marketing\MarketingSettings;
use Log;


class TwilioService
{

    protected  $sid;
    protected  $token;
    protected  $from;

    /**
     * SyncService constructor.
     */
    public function __construct()
    {
       $this->sid = MarketingSettings::get('TWILIO_SID');
       $this->token = MarketingSettings::get('TWILIO_TOKEN');
       $this->from = MarketingSettings::get('TWILIO_FROM');

//
//       $this->sid = MarketingSettings::where('TWILIO_SID')->first();
//       $this->token = MarketingSettings::where('TWILIO_TOKEN')->first();
//       $this->from = MarketingSettings::where('TWILIO_FROM')->first();

       $this->setGlobalConfig();
    }

    /**
     * set global Twilio settings
     *  return void
     */
    protected function setGlobalConfig()
    {
        config(['twilio.sid' => $this->sid]);
        config(['twilio.token' => $this->token]);
        config(['twilio.from' => $this->from]);

    }

    public function message($phone, $message, $mediaUrls, $params){
        return Twilio::message($phone, $message, $mediaUrls, $params);
    }

    public function sendMessage($phone,$message,$statusCallback = null){
        Log::debug('TwilioService',[

            "sid" =>$this->sid,
            "from" =>$this->from,
        ]);

        $from = trim($this->from);

        $mediaUrls = [];
        $params = [
            "body" => $message,
            "from" =>$from,
            'statusCallback'=>$statusCallback
        ];

        return $this->message($phone, $message, $mediaUrls, $params);
    }

    //make all Sending Jobs
    public function processSending(TwilioMessage $message){

        //ProcessTwilioMessage::dispatch($twilioMessage)->onQueue('twiliosms');

        $preparedSms = $message->sendedSms()->get();

        foreach ($preparedSms as $sendedSms){
            SendTwilioSms::dispatch($sendedSms)->onQueue('twiliosms');
        }
    }

}