<?php

namespace App\Marketing\Twilio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Dashboard\Elements\Forms\Elements\Checkbox;
use Webmagic\Dashboard\Elements\Forms\Elements\Switcher;
use App\Users\User;
use App\Marketing\Twilio\UserRepo as UserRepo;

use App\Marketing\Twilio\Dashboard\SmsUserFilter as UserFilter;

class UserSmsDashboardPresenter
{

    /**
     * @param $items
     * @param Dashboard $dashboard
     * @param Request $request
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable
     */
    public function getTablePage($items, Dashboard $dashboard, Request $request, UserFilter $filter = null)
    {
        $data['items'] = $request->get('items', 10);
        $data['page'] = $request->get('page', 1);
        $data['keyword'] = $filter ? $filter->getSearchPhrase() : $request->get('keyword', '');

        $tablePageGenerator = (new TablePageGenerator($dashboard->page()))
            ->title('SMS')
            //, 'Last Seen'
            //ID | Username | Name | Phone | Last Seen
            ->tableTitles('ID', 'Include','Username', 'Name', 'Phone', 'Last Seen')
            ->showOnly('id', 'include','username', 'name', 'phone', 'lastSeen')
            ->setConfig([
                'name' => function (User $item) {
                    if($item->first_name || $item->last_name){
                        return $item->present()->prepareFullName;
                    }

                    return '--';
                },
                'lastSeen' =>function (User $user){
                //https://stackoverflow.com/questions/22460066/laravel-last-login-date-and-time-timestamp
                //https://laravel.com/docs/4.2/events
                    return '--';
                },
                'include' => function (User $user) {
                    return (new Checkbox())->checked((bool)$user->subscription);
                },
            ])
            ->items($items)
            ->toolsInModal(true)
            ->withPagination($items, route('dashboard::sms.index', $data))
            ->createLink(route('dashboard::sms.create'))
            //->createLink(route('dashboard::sms.create'))
            ->addToolsLinkButton(route('dashboard::sms.create'),'Create SMS','fas fa-plus',)
//            ->createLink(route('dashboard::users.create'))
//            ->setEditLinkClosure(function (User $item) {
//                return route('dashboard::users.edit', $item);
//            })
        ;

        $tablePageGenerator->addFiltering()
            ->action(route('dashboard::sms.index'))

            ->method('GET')
            ->textInput('keyword', $data['keyword'], 'Entry holders', false)
            ->select('condition_active',['activeOR'=>'OR active ','activeAND'=>'AND active ','inactiveOR'=>'OR inactive ','inactiveAND'=>'AND inactive '], )
            ->textInput('activedays', $data['keyword'], ' FOR ', false)
            ->select('condition_registered',['registeredOR'=>'OR registered ','registeredAND'=>'AND registered '], )
            ->textInput('registereddays', $data['keyword'], ' FOR ', false)


            ->submitButton('Search');

        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }

//Create message


//    /**
//     * @param Dashboard $dashboard
//     * @return mixed
//     */
//    public function getCreateForm(Dashboard $dashboard)
//    {
//        return $dashboard->addContent(view('dashboard.sms_create_form'));
//    }


    /**
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws \Exception
     */
    public function getCreateFormPage()
    {
        //$users = $userRepo->getAll()->toArray();
        $users = User::where('role_id', 2)
            ->where('subscription', 1 )->get()->pluck('first_name','id')->all();

        return (new FormPageGenerator())
            ->title('Add Sms Message')
            ->action(route('dashboard::sms.store'))
            ->ajax(true)
            ->method('POST')
            //->select('user',$users,'','Users',true,true)
            ->selectJS('users_ids', $users, 2, 'Users', true,true)
//            ->selectWithAutocomplete('user',route('dashboard::sms.selectautocomplete'),['1'=>'London'],'1','user',true,true, [])
            //->textInput('phone', false, 'Phone', true)
            ->textInput('text', false, 'Text', true)
            ->submitButtonTitle('Send Message');
    }


    /**
     * @return FormGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws \Exception
     */
    public function getCreateForm()
    {
        return (new FormGenerator())
            ->title('Add Sms Message')
            ->action(route('dashboard::sms.store'))
            ->ajax(true)
            ->method('POST')
            ->selectJS('select', [1 => 'Option 1', 2 => 'Option 2'], 2, 'Select me with JS', false)
            ->selectWithAutocomplete('user',route('dashboard::sms.selectautocomplete'),['1'=>'London'],'1','user',true,true, [])
            //->textInput('phone', false, 'Phone', true)
            ->textInput('text', false, 'Text', true)
            ->submitButtonTitle('Send Message');
    }

//    /**
//     * @param Dashboard $dashboard
//     * @param TwilioMessage|Model $question
//     * @return mixed
//     */
//    public function getEditForm(Dashboard $dashboard, TwilioMessage message)
//    {
//        return $dashboard->addContent(view('dashboard.sms_create_form', compact('message')));
//    }
}
