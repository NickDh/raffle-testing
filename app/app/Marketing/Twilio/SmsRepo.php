<?php

namespace App\Marketing\Twilio;

 use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SmsRepo extends EntityRepo
{
    protected $entity = TwilioMessage::class;


    /**
     * Get all with paginate and filter
     *
     * @param null $keyword
     * @param null $per_page
     * @param null $page
     * @param string $sortBy
     * @param string $sort
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws Exception
     */
    public function getByFilter($keyword = null, $per_page = null, $page = null, $sortBy = 'created_at', $sort = 'desc',$message_id = null,$from = null,$to = null )
    {
        $query = $this->query();

        if (isset($message_id)){
            $query->where('message_id', $message_id);
        }

        if (isset($from) && isset($to)){
            $date_from = Carbon::parse($from)->toDateTimeString();
            $date_to = Carbon::parse($to)->toDateTimeString();
            $query->whereBetween('created_at', [$date_from, $date_to]);
        }

        if ($keyword) {
            $searchQuery = "%$keyword%";

            $query
                ->where('text', 'like', $searchQuery);
        }

        $query->orderBy($sortBy, $sort);

        if (is_null($per_page) && is_null($page)) {
            return $query->get();
        }


        return $query->paginate($per_page, ['*'], 'page', $page);
    }

    /**
     * Get entity by ID
     *
     * @param $id
     *
     * @return Model|null
     * @throws Exception
     */
    public function getByID($id)
    {
        $query = $this->query();
        $query->where('id', $id);


        return $this->realGetOne($query);
    }
}
