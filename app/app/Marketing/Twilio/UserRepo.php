<?php

namespace App\Marketing\Twilio;

use App\Users\User;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;

class UserRepo extends EntityRepo
{
    protected $entity = User::class;

    /**
     * Get all with paginate and filter
     *
     * @param null $keyword
     * @param null $per_page
     * @param null $page
     * @param string $sortBy
     * @param string $sort
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws Exception
     */
    public function getByFilter($keyword = null, $per_page = null, $page = null, $sortBy = 'created_at', $sort = 'desc')
    {
        $query = $this->query();

        $query->where('role_id', 2)
                ->where('subscription', 1 );

        if ($keyword) {
            $searchQuery = "%$keyword%";

            $query
                ->where('username', 'like', $searchQuery)
                ->orWhere('full_name', 'like', $searchQuery)
                ->orWhere('email', 'like', $searchQuery);
        }

        $query->orderBy($sortBy, $sort);

        if (is_null($per_page) && is_null($page)) {
            return $query->get();
        }


        return $query->paginate($per_page, ['*'], 'page', $page);
    }

    /**
     * Get all with paginate and filter
     *
     * @param null $per_page
     * @param null $page
     * @param string $sortBy
     * @param string $sort
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws Exception
     */
    public function getAll( $per_page = null, $page = null, $sortBy = 'created_at', $sort = 'desc')
    {
        $query = $this->query();

        $query->where('role_id', 2)
            ->where('subscription', 1 );

        $query->orderBy($sortBy, $sort);

        if (is_null($per_page) && is_null($page)) {
            return $query->get();
        }

        return $query->paginate($per_page, ['*'], 'page', $page);
    }


}
