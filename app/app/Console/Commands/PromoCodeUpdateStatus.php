<?php

namespace App\Console\Commands;

use App\PromoCodes\PromoCodeService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class PromoCodeUpdateStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'promocode:check-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set expired status to promo code';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param PromoCodeService $codeService
     * @return mixed
     */
    public function handle(PromoCodeService $codeService)
    {
        try {
            $codeService->checkAndUpdateStatuses();
        } catch (Exception $exception){
            Log::error($exception);
        }
    }
}
