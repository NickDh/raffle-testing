<?php

namespace App\Console\Commands;

use App\Ecommerce\Orders\OrderService;
use Illuminate\Console\Command;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;

class CancelOldProcessingOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:cancel-old-processing-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark all orders which are in processing more than max processing time. And return all tickets';

    /**
     * Execute the console command.
     *
     * @param OrderService $orderService
     *
     * @return mixed
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function handle(OrderService $orderService)
    {
        $canceledOrdersCount = $orderService->cancelOldProcessingOrders();

        $this->info("Canceled $canceledOrdersCount orders");
    }
}
