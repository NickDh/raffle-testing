<?php

namespace App\Console\Commands;

use App\Marketing\MarketingSettings;
use App\Users\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use NZTim\Mailchimp\Mailchimp;
use Illuminate\Console\Command;

class AddToMailChimp extends Command
{
    protected $mc;
    protected $mckey;
    protected $mcaudienceid;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending subscribed users to mailchimp';

    /**
     * AddToMailChimp constructor.
     * @throws \NZTim\Mailchimp\Exception\MailchimpException
     */
    public function __construct()
    {

        //$this->mckey = DB::table('marketing_settings')->select(['value'])->where('key','MC_KEY')->first();
        //$this->mcaudienceid = DB::table('marketing_settings')->select(['value'])->where('key','MC_AUDIENCE_ID')->first();

        $this->mckey = marketingSetting('MC_KEY');
        $this->mcaudienceid = marketingSetting('MC_KEY');

        if($this->mckey && $this->mcaudienceid) {
            $this->mc = new Mailchimp($this->mckey);
        }
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->mckey && $this->mcaudienceid) {
            User::whereSubscription(1)->chunkById(100, function($users){
                foreach($users as $user){
                    try {
                        $merge = [
                            'FNAME' => $user->first_name,
                            'LNAME' => $user->last_name,
                        ];
                        $this->mc->subscribe($this->mcaudienceid, $user->email, $merge, true);
                    } catch (\Throwable $e) {
                        report($e);
                    }
                }
            });
        } else {
            Log::info("No api key and audience id");
        }
    }
}
