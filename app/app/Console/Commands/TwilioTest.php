<?php

namespace App\Console\Commands;

use App\Marketing\Twilio\SendedSms;
use App\Users\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Marketing\Twilio\TwilioMessage;
use NZTim\Mailchimp\Mailchimp;
use Twilio\Deserialize;
use Twilio\Rest\Api\V2010\Account\MessageInstance;
use Twilio\Values;


class TwilioTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twilio:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send test message to Twilio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(TwilioMessage $twiliomessage)
    {
        var_dump(route('twillio'));

        $twilioMessage = TwilioMessage::create([
            'text'=> 'This is a test message',
            'created_by'=> '1',
            'created_at'=> Carbon::now(),
            'updated_at'=> Carbon::now(),
        ]);

        $response = $twilioMessage->messageSend('+79829935006','This is a test message',route('twillio'));

        var_dump($response->__get('status'));

        var_dump($response->__get('price'));

    }


}

