<?php

namespace App\Competitions;

use App\Enums\CompetitionStatusEnum;
use App\Enums\CompetitionTypeEnum;
use App\Enums\TicketStatusEnum;
use App\Tickets\Ticket;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laracasts\Presenter\Exceptions\PresenterException;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Dashboard\Elements\Links\LinkButton;
use Webmagic\Dashboard\Elements\Lists\DescriptionList;


class CompetitionDashboardPresenter
{
    /**
     * @param array $questions
     * @return mixed
     * @throws Exception
     */
    public function getCreateForm(array $questions)
    {
        return (new FormPageGenerator())
            ->title('Competition creating')
            ->action(route('dashboard::competitions.store'))
            ->method('POST')
            ->textInput('title', false, 'Competition Title', true)
            ->slugInput('slug', 'title', null, 'URL Slug', true)
            ->visualEditor('short_description', false, 'Short Competition Description', true)
            ->visualEditor('full_description', false, 'Full Competition Description', true)
            ->numberInput('price', false, 'Entry Price', true, '0.01', '0')
            ->numberInput('sale_price', false, 'Sale Price', false, '0.01', '0')
            ->select('competition_type', array_combine(CompetitionTypeEnum::keys(), CompetitionTypeEnum::values()), false,'Competition Type',
                true, false, ['class'=>'form-control js_control-letters js_control-number'])
            ->numberInput('tickets_sort_by', false, 'Sort Tickets In Numbers Of', true, '1', '0')
            ->numberInput('tickets_count', false, 'Number Of Tickets', false, '1', '0', null,
                ['class'=>'form-control js_control-state', 'data-control-state'=>'disable', 'data-control-el'=>'.js_control-number', 'data-control-option'=>'NUMBERS', 'data-state-active-by-empty'=>"0"])
            ->select('letters_to', array_combine(range('A', 'Z'), range('A', 'Z')),false, 'Letters To', false, false,
                ['class'=>'form-control js_control-state', 'data-control-state'=>'disable', 'data-control-el'=>'.js_control-letters', 'data-control-option'=>'LETTERS', 'data-state-active-by-empty'=>"0"])
            ->numberInput('max_tickets_per_user', false, ' Max Tickets Per User', true, '1', '1')
            ->dateTimePickerJS('deadline', false, 'Competition Deadline', true, ['min' => Carbon::now()->toDateString()])
            ->checkbox('auto_extend', false, 'Auto Extend Deadline')
            ->numberInput('hours_extend', false, 'Hours To Extend', false, '1', '0')
            ->imageInput('main_image', '', 'Main Competition Image')
            ->imageArea("photos[]", [], 'Additional Gallery Image')
            ->select('question_id', $questions, false, 'Competition Question', true)
            ->submitButtonTitle('Submit');
    }

    /**
     * @param Competition $competition
     * @param array       $questions
     * @param array       $statuses
     *
     * @return mixed
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws PresenterException
     */
    public function getEditForm(Competition $competition, array $questions, array $statuses)
    {
        $formPageGenerator = (new FormPageGenerator())
            ->title('Edit Competition')
            ->action(route('dashboard::competitions.update', $competition->id))
            ->method('PUT')
            ->textInput('title', $competition, 'Competition Title', true)
            ->slugInput('slug', 'title', $competition, 'URL Slug', true);

        // Special fields for finished competitions
        if($competition->isFinished()){
            $formPageGenerator
                ->textInput('winner_name', $competition, 'Winner name')
                ->textInput('winner_number', $competition, 'Winner number');
        }

        $formPageGenerator
            ->select('status', $statuses, $competition, 'Competition Status', true)
            ->visualEditor('short_description', $competition, 'Short Competition Description', true)
            ->visualEditor('full_description', $competition, 'Full Competition Description', true)
            ->numberInput('price', $competition, 'Entry Price', true, '0.01', '0')
            ->numberInput('sale_price', $competition, 'Sale Price', false, '0.01', '0')
            ->numberInput('max_tickets_per_user', $competition, 'Max Entries Per User', true, '1', '1')
            ->dateTimePickerJS('deadline', $competition->deadline, 'Competition Deadline', true, ['min' => Carbon::now()->toDateTimeString()])
            ->checkbox('auto_extend', $competition, 'Auto Extend Deadline')
            ->numberInput('hours_extend', $competition, 'Hours To Extend', false, '1', '0')
            ->imageInput('main_image',  ['main_image' => $competition->main_image ? $competition->present()->main_image : ''], 'Main Competition Image')
            ->imageArea("photos[]", $competition->present()->images, 'Additional Gallery Image')
            ->select('question_id', $questions, $competition->question_id,'Competition Question', true)
            ->submitButtonTitle('Update');

        $formPageGenerator->getBox()
            ->addElement('box_footer')
            ->linkButton()
            ->icon('fa-trash')
            ->content('Delete')
            ->class('btn-danger text-white js_ajax-by-click-btn pull-left')
            ->dataAttr('action', route('dashboard::competitions.destroy', $competition->id))
            ->dataAttr('method', 'DELETE')
            ->dataAttr('confirm', true);

        return $formPageGenerator;
    }

    /**
     * @param $user
     *
     * @return mixed|string
     */
    public function getUserDescriptionList($user)
    {
        if(is_null($user)){
            return 'No results';
        }

        return (new DescriptionList())
            ->isHorizontal(true)
            ->addData([
                'Name: ' => $user->present()->prepareFullName,
                'Address: ' => $user->address,
                'City: ' => $user->town,
                'Post Code: ' => $user->post_code,
                'Phone: ' => "<a href=\"tel:$user->phone\"> $user->phone</a>",
                'Email: ' => $user->email,
            ])->addClass('my-form-result');
    }

    public function getTicketsTablePage($competition, $tickets, Dashboard $dashboard, Request $request)
    {
        $data['competition'] = $competition->id;
        $data['items'] = $request->get('items', 10);
        $data['page'] = $request->get('page', 1);
        $data['keyword'] = $request->get('keyword', '');

        $tablePageGenerator = (new TablePageGenerator($dashboard->page()))
            ->title('Tickets for ' . $competition->title)
            ->tableTitles('ID', 'Added to cart', 'Number', 'Status', 'User', 'Actions')
            ->showOnly([
                'id',
                'created_at',
                'number',
                'status',
                'user',
            ])
            ->setConfig([
                'user' => function (Ticket $ticket) {
                    return $ticket->user->present()->prepareFullName;
                },
            ])
            ->addElementsToToolsCollection(
                function (Ticket $item) {
                    if (TicketStatusEnum::HELD()->is($item->status)){
                        $btn = (new LinkButton())
                            ->icon('fa-eye')
                            ->class('btn-success js_ajax-by-click-btn')
                            ->dataAttr('action', route('dashboard::competitions.ticket.status', $item->id))
                            ->dataAttr('method', 'POST')
                            ->dataAttr('replace-blk', ".js_item_$item->id")
                            ->dataAttr('modal-hide', 'false')
                            ->dataAttr('title', "Set Live status")
                            ->dataAttr('confirm', true);

                        return $btn->render();
                    }

                }
            )
            ->items($tickets)
            ->withPagination($tickets, route('dashboard::competitions.show', $data))
        ;

        $tablePageGenerator->addFiltering()
            ->action(route('dashboard::competitions.show', $competition))
            ->method('GET')
            ->textInput('keyword', $request->get('keyword' ,''), '', false)
            ->submitButton('Search');

        $tablePageGenerator->getTable();

        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }
}
