<?php


namespace App\Competitions\Dashboard;


use App\app\Competitions\Dashboard\CompetitionsFilter;
use App\app\Competitions\Dashboard\CompetitionsTablePageGenerator;
use App\Competitions\Competition;
use Illuminate\Pagination\AbstractPaginator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Elements\Links\Link;

class FinishedCompetitionTablePageGenerator extends CompetitionsTablePageGenerator
{
    /**
     * @param AbstractPaginator       $paginator
     *
     * @param CompetitionsFilter|null $filter
     *
     * @return $this|void
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function prepare(AbstractPaginator $paginator, CompetitionsFilter $filter = null)
    {
        parent::prepare($paginator);

        $this
            ->title('Finished Competitions')
            ->showOnly([
                'id',
                'title',
                'tickets_sold',
                'tickets_remaining',
                'deadline'
            ])
            ->tableTitles('ID', 'Competition', 'Entries Sold', 'Entries Unsold', 'Deadline', 'Actions')
            ->addToConfig('title', function (Competition $competition){
                $link = (new Link())->content($competition->title)->link(route('dashboard::competition-entries.find-winner', $competition->id));
                return "<div style='min-width:250px; white-space: normal'>$link</div>";
            });

        // Remove create link
        $this->removeCreateBtn();

        return $this;
    }

    /**
     * @return $this
     * @throws NoOneFieldsWereDefined
     */
    protected function removeCreateBtn()
    {
        $this->createLink = null;
        $this->getBox()->boxTools('')->headerAvailable(false);

        $this->updateTable();

        return $this;
    }

    /**
     * @return $this|FinishedCompetitionTablePageGenerator
     */
    protected function addManualSorting()
    {
        return $this;
    }

    /**
     * @param Competition $competition
     *
     * @return string
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareControlButtons(Competition $competition)
    {
        $html = '<div style="width:200px;">';
        $html .= $this->getFindWinnerBtn($competition);
        $html .= $this->getEditBtn($competition);
        $html .= '</div>';

        return $html;
    }
}
