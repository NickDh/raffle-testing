<?php


namespace App\app\Competitions\Dashboard;


use App\Competitions\Competition;
use App\Enums\CompetitionStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\AbstractPaginator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Elements\Links\LinkButton;
use Webmagic\Dashboard\Pages\BasePage;

class CompetitionsTablePageGenerator extends TablePageGenerator
{
    /**
     * CompetitionsTablePageGenerator constructor.
     */
    public function __construct()
    {
        parent::__construct(null);
    }

    /**
     * @param AbstractPaginator       $paginator
     *
     * @param CompetitionsFilter|null $filter
     *
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function prepare(AbstractPaginator $paginator, CompetitionsFilter $filter = null)
    {
        $this
            ->title('Live Competitions')
            ->tableTitles('ID', 'Competition', 'Entries Sold', 'Entries Unsold', 'Deadline', 'Status', 'Actions')
            ->showOnly([
                'id',
                'title',
                'tickets_sold',
                'tickets_remaining',
                'deadline',
                'status'
            ])
            ->setConfig([
                'title' => function (Competition $item) {
                    return '<div style="min-width:250px; white-space: normal">'.$item->title.'</div>' ;
                },
                'deadline' => function (Competition $item) {
                    return date('jS F Y @ H:i', strtotime($item->deadline));
                },
                'tickets_sold' => function (Competition $competition) {
                    return $competition->soldTicketsCount();
                },
                'tickets_remaining' => function (Competition $competition) {
                    return $competition->tickets_count - $competition->soldTicketsCount();
                },
            ])
            ->addControls()
            ->items($paginator->all())
            ->withPagination($paginator, request()->url())
            ->addManualSorting()
            ->createLink(route('dashboard::competitions.create'));

            // Custom create button text
            $this->getBox()->boxTools()->content('Add New Competition');

            $keyword = $filter ? $filter->getSearchPhrase() : request()->get('keyword' ,'');

            $this->addFiltering()
                ->action(request()->url())
                ->method('GET')
                ->textInput('keyword', $keyword, '', false)
                ->submitButton('Search');
    }

    /**
     * @return $this
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function addManualSorting()
    {
        $this->manualSorting(
            route('dashboard::competitions.position'),
            // Function for getting ID from items. Item from collection will be put in this function
            function ($item) {
                return $item['id'];
            },
            'POST'
        );

        return $this;
    }

    /**
     * @return $this
     */
    protected function addControls()
    {
        $this->addElementsToToolsCollection(
            function (Competition $competition) {
                return $this->prepareControlButtons($competition);
            }
        );

        return $this;
    }

    /**
     * @param Competition $competition
     *
     * @return string
     */
    protected function prepareControlButtons(Competition $competition)
    {
        $html = '<div style="width:300px;">';
        $html .= $this->getPublishStatusBtn($competition);
        $html .= $this->getEditBtn($competition);
//        $html .= $this->getFindWinnerBtn($competition);
        $html .= $this->getShowBtn($competition);
        $html .= $this->getPauseStatusBtn($competition);
        $html .= '</div>';

        return $html;
    }

    /**
     * @param Model $item
     * @return mixed
     */
    protected function getEditBtn(Model $item)
    {
        $btn = (new LinkButton())
            ->icon('fa-edit')
            ->link(route('dashboard::competitions.edit', $item))
            ->class('btn-success text-white')
            ->js()->tooltip()->regular('Edit');


        return $btn->render();
    }

    /**
     * @param Model $item
     * @return mixed
     */
    protected function getPublishStatusBtn(Model $item)
    {
        $status = CompetitionStatusEnum::UNPUBLISHED()->is($item->status) ? CompetitionStatusEnum::LIVE() : CompetitionStatusEnum::UNPUBLISHED();

        $btn = (new LinkButton())
            ->icon('fa-traffic-light')
            ->class('btn-danger text-white js_ajax-by-click-btn')
            ->dataAttr('action', route('dashboard::competitions.status', [$item->id, $status]))
            ->dataAttr('method', 'POST')
            ->dataAttr('replace-blk', ".js_item_$item->id")
            ->dataAttr('modal-hide', 'false')
            ->dataAttr('title', "Set $status status")
            ->dataAttr('confirm', true);

        return $btn->render();
    }

    /**
     * @param Model $item
     * @return mixed
     */
    protected function getPauseStatusBtn(Model $item)
    {
        if(CompetitionStatusEnum::PAUSED()->is($item->status)){
            $status = CompetitionStatusEnum::LIVE();
            $icon = 'fa-pause';
        } else{
            $status =  CompetitionStatusEnum::PAUSED();
            $icon = 'fa-play';
        }

        $btn = (new LinkButton())
            ->icon($icon)
            ->class('btn-warning text-white js_ajax-by-click-btn')
            ->dataAttr('action', route('dashboard::competitions.status', [$item->id, $status]))
            ->dataAttr('method', 'POST')
            ->dataAttr('replace-blk', ".js_item_$item->id")
            ->dataAttr('modal-hide', 'false')
            ->dataAttr('title', "Set $status status")
            ->dataAttr('confirm', true);

        return $btn->render();
    }

    /**
     * @param Model $item
     * @return mixed
     */
    protected function getShowBtn(Model $item)
    {
        $btn = (new LinkButton())
            ->icon('fa-ticket-alt')
            ->link(route('dashboard::competitions.show', $item))
            ->class('btn-primary text-white')
            ->js()->tooltip()->regular('Show tickets');


        return $btn->render();
    }

    /**
     * @param Model $item
     * @return string
     * @throws NoOneFieldsWereDefined
     */
    protected function getFindWinnerBtn(Model $item)
    {
        $btn = (new LinkButton())
            ->link(route('dashboard::competition-entries.find-winner', $item->id))
            ->class('btn btn-success text-white')
            ->content('Find Winner');


        return $btn->render();
    }
}
