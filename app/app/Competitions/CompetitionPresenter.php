<?php

namespace App\Competitions;

use Illuminate\Config\Repository;
use Illuminate\Support\Facades\Storage;
use Webmagic\Core\Presenter\Presenter as CorePresenter;

class CompetitionPresenter extends CorePresenter
{
    /**
     * @return Repository|mixed
     */
    public function getConfigPath()
    {
        return config('project.competition_img_path');
    }

    /**
     * Main image
     *
     * @return string
     */
    public function main_image()
    {
        return $this->prepareImageURL($this->entity->main_image);
    }

    /**
     * Prepare url for images
     *
     * @param $file_name
     *
     * @return string
     */
    protected function prepareImageURL($file_name)
    {
        return Storage::disk('public')->url($this->getConfigPath()."/".$file_name);
    }

    /**
     * @param $file_name
     * @return string
     */
    public function getLocalPath($file_name)
    {
        return $this->getConfigPath()."/".$file_name;
    }

    /**
     * Get sale price if it exist or return price
     *
     * @return mixed
     */
    public function actual_price()
    {
        if($this->sale_price){
            return $this->sale_price;
        }

        return $this->price;
    }

    /**
     * @return array|false|string[]
     */
    public function images()
    {
        if($this->photos){
            return explode(';',$this->photos);
        }

        return [];
    }

    /**
     * @return string
     */
    public function url()
    {
        if($this->entity->isFinished()){
            return route('live-competition.finished', $this->entity->slug);
        }

        return route('live-competition.show', $this->entity->slug);
    }

    /**
     * @return false|string
     */
    public function finishDate()
    {
       return date('d/m/Y', strtotime($this->entity->updated_at));
    }

    /**
     * @return false|string
     */
    public function realDeadline()
    {
        return date('d/m/Y', strtotime($this->entity->realDeadline()));
    }
}
