<?php

namespace App\Questions;


use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;

class AnswerRepo extends EntityRepo
{

    /**
     * @var string
     */
    protected $entity = Answer::class;


    /**
     * @param array $ids
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getByIDs(array $ids)
    {
        $query = $this->query();
        $query->whereIn('id', $ids);

        return $this->realGetMany($query);
    }

}
