<?php

namespace App\Questions;


use App\Services\ImagePathManager;
use App\Services\ImagesPrepare;
use Exception;
use Illuminate\Http\Request;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;


class AnswerService
{
    protected  $answerRepo;

    /**
     * AnswerService constructor.
     * @param AnswerRepo $answerRepo
     */
    public function __construct(AnswerRepo $answerRepo)
    {
        $this->answerRepo = $answerRepo;
    }


    /**
     * @param int $question_id
     * @param Request $request
     * @param ImagesPrepare $imagesPrepare
     * @throws Exception
     */
    public function preparingAndCreatingFromRequest(int $question_id, Request $request, ImagesPrepare $imagesPrepare)
    {
        $answers = [];
        foreach ($request->all() as $key => $value){

            if(strpos($key, 'answer-text') !== false && strpos($key, '-id') == false){

                $is_correct = $request->get('is_true') == $key;
                $answers[] = [
                    'text' => $request->get($key),
                    'question_id' => $question_id,
                    'is_correct' => $is_correct,
                    'position' => last(explode('-', $key)),
                    'id' => $request->get($key.'-id', null)
                ];
            }

            if(strpos($key, 'answer-image') !== false){

                $pos = strpos($key, $request->get('is_true'));
                $is_correct = $pos !== false;


                if($request->hasFile($key)){
                    $data = $imagesPrepare->saveFiles(
                        $request->all(),
                        [$key],
                        (new ImagePathManager())->publicPathForPastQuestionsImages(),
                        false
                    );
                    $answers[] = [
                        'image' => $data[$key],
                        'question_id' => $question_id,
                        'is_correct' => $is_correct,
                        'position' => last(explode('-', $key)),
                        'id' => $request->get($key.'-id', null)
                    ];
                } else{
                    $answers[] = [
                        'is_correct' => $is_correct,
                        'id' =>$request->get($key, null)
                    ];
                }


            }

        }
        $answers = collect($answers)->sortBy('position')->toArray();

        foreach ($answers as $answer_data){
            if(is_null($answer_data['id'])){
                $this->answerRepo->create($answer_data);
            } else {

                $answer = $this->answerRepo->getByID($answer_data['id']);
                if(isset($answer_data['image']) && $answer_data['image'] != $answer->image){
                    $imagesPrepare->deleteLocalFile($answer->present()->getLocalPath($answer->image));
                }

                $this->answerRepo->update($answer_data['id'], $answer_data);
            }
        }
    }

    /**
     * Delete answers and clear images from storage
     *
     * @param array $ids
     * @param ImagesPrepare $imagesPrepare
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     * @throws Exception
     */
    public function deleteByIdAndClearImages(array $ids, ImagesPrepare $imagesPrepare)
    {
        $answers = $this->answerRepo->getByIDs($ids);

        foreach ($answers as $answer){
            $imagesPrepare->deleteLocalFile($answer->present()->getLocalPath($answer->image));
        }

        $this->answerRepo->destroy($ids);
    }
}
