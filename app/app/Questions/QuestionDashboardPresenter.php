<?php

namespace App\Questions;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Dashboard;


class QuestionDashboardPresenter
{



    /**
     * @param $items
     * @param Dashboard $dashboard
     * @param Request $request
     * @return mixed|Dashboard
     * @throws \Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined
     */
    public function getTablePage($items, Dashboard $dashboard, Request $request)
    {
        $data['items'] = $request->get('items', 10);
        $data['page'] = $request->get('page', 1);
        $data['keyword'] = $request->get('keyword', '');

        $tablePageGenerator = (new TablePageGenerator($dashboard->page()))
            ->title('Competition Questions')
            ->tableTitles('ID', 'Question', 'Actions')
            ->showOnly([
                'id',
                'question'
            ])
            ->items($items)
            ->withPagination($items, route('dashboard::questions.index', $data))
            ->createLink(route('dashboard::questions.create'))
            ->setEditLinkClosure(function (Question $item) {
                return route('dashboard::questions.edit', $item);
            })
        ;


        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }


    /**
     * @param Dashboard $dashboard
     * @return mixed
     */
    public function getCreateForm(Dashboard $dashboard)
    {
        return $dashboard->addContent(view('dashboard.question_create_form'));
    }

    /**
     * @param Dashboard $dashboard
     * @param Question|Model $question
     * @return mixed
     */
    public function getEditForm(Dashboard $dashboard, Question $question)
    {
        return $dashboard->addContent(view('dashboard.question_edit_form', compact('question')));
    }
}
