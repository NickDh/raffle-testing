<?php

namespace App\Questions;


use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Presenter\PresentableTrait;
use Webmagic\Core\Presenter\Presenter;

/**
 * Class Answer
 *
 * @package App\Questions
 * @method AnswerPresenter present()
 * @property int $id
 * @property string|null $text
 * @property string|null $image
 * @property int|null $question_id
 * @property int $is_correct
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Questions\Question|null $question
 * @method static \Illuminate\Database\Eloquent\Builder|Answer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Answer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Answer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereIsCorrect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Answer extends Model
{
    use PresentableTrait;

    /** @var  Presenter class that using for present model */
    protected $presenter = AnswerPresenter::class;

    protected $fillable = [
        'question_id',
        'text',
        'image',
        'is_correct',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    /**
     * @return bool
     */
    public function isImage(): bool
    {
        return (bool) $this->image;
    }

    /**
     * @return bool
     */
    public function isText(): bool
    {
        return !$this->isImage();
    }
}
