<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImagePathManager
{
    /**
     * @return mixed
     */
    public function publicPathForCompetitionsImages()
    {
        return Storage::disk('public')->path(config('project.competition_img_path'));
    }

    /**
     * @return mixed
     */
    public function publicPathForPastWinnersImages()
    {
        return Storage::disk('public')->path(config('project.past_winner_img_path'));
    }

    /**
     * @return mixed
     */
    public function publicPathForPastQuestionsImages()
    {
        return Storage::disk('public')->path(config('project.question_img_path'));
    }

    /**
     * @return mixed
     */
    public function publicPathForResultsImages()
    {
        return Storage::disk('public')->path(config('project.result_img_path'));
    }
}
