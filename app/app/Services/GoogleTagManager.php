<?php

namespace App\Services;

use App\Ecommerce\Cart\SessionCart;
use App\Tickets\Ticket;
use Exception;
use Illuminate\Support\Facades\App;
use Laracasts\Presenter\Exceptions\PresenterException;

class GoogleTagManager
{
    /**
     * Flash the necessary data to the session that can be
     * used in the JS on the front end to push a new
     * event to Google Tag Manager when a new item
     * is added to the cart.
     *
     * @param  Ticket  $ticket
     * @param  int  $quantity
     * @throws PresenterException
     */
    public function itemAddedToCart(Ticket $ticket, int $quantity): void
    {
        session()->flash('google_tag_manager_event', [
            'event'     => 'add_to_cart',
            'ecommerce' => [
                'items' => [
                    [
                        'item_name' => $ticket->competition->title,
                        'item_id'   => $ticket->competition->id,
                        'price'     => $ticket->competition->present()->actual_price,
                        'quantity'  => $quantity
                    ]
                ],
            ]
        ]);
    }

    /**
     * Flash the necessary data to the session that can be
     * used in the JS on the front end to push a new
     * event to Google Tag Manager when the user
     * initiates the checkout process.
     *
     * @throws Exception
     */
    public function checkoutInitiated(): void
    {
        /** @var SessionCart $cart */
        $cart = App::make(SessionCart::class);
        $cart->key(auth()->id());

        $items = [];

        foreach ($cart->getTickets()->groupBy('competition_id') as $competitionTickets) {
            $items[] = [
                'item_name' => $competitionTickets[0]->competition->title,
                'item_id'   => $competitionTickets[0]->competition->id,
                'price'     => $competitionTickets[0]->competition->present()->actual_price,
                'quantity'  => $competitionTickets->count()
            ];
        }

        session()->flash('google_tag_manager_event', [
            'event'     => 'begin_checkout',
            'ecommerce' => [
                'items' => $items,
            ]
        ]);
    }
}