<?php


namespace App\Payment\Core;


interface TransactionContract
{
    /**
     * @return TransactionStatusEnum
     */
    public function getStatus(): TransactionStatusEnum;

    /**
     * @return bool
     */
    public function isSuccessful(): bool;

    /**
     * @return bool
     */
    public function isFailed(): bool;

    /**
     * @return bool
     */
    public function isFinished(): bool;
}