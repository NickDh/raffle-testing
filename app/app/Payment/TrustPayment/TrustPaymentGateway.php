<?php


namespace App\Payment\TrustPayment;


use App\Ecommerce\Cart\SessionCart;
use App\Ecommerce\Orders\Order;
use App\Payment\Core\PaymentGatewayContract;
use App\Payment\Core\TransactionContract;
use App\Users\User;
use Exception;
use Firebase\JWT\JWT;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;

class TrustPaymentGateway implements PaymentGatewayContract
{
    /** @var TrustPaymentTransaction */
    protected $userLastTransaction;

    /**
     * Check if current user has saved payment method
     *
     * @return bool
     * @throws Exception
     */
    public function isSavedCardExists(): bool
    {
        if(!$transaction = $this->getUserLastTransaction()){
            return false;
        }

        return $transaction->mayBeUsedForNextPayment();
    }

    /**
     * Return last 4 digits for saved card
     *
     * @return string
     * @throws Exception
     */
    public function getSavedCardLast4Digits(): string
    {
       if(!$this->isSavedCardExists()){
           return '';
       }

       return $this->getUserLastTransaction()->cartLast4Digits();
    }


    /**
     * Prepare and return checkout page
     *
     * @param SessionCart $cart
     * @param User        $user
     * @param bool        $useSavedCard
     * @param string      $orderCreatingUrl
     * @param string      $orderCreatingMethod
     *
     * @return string
     * @throws Exception
     */
    public function getCheckoutPage(
        SessionCart $cart,
        User $user,
        bool $useSavedCard,
        string $orderCreatingUrl,
        string $orderCreatingMethod = 'POST'
    ): string {
        $livePaymentStatus = (int)$this->isLivePaymentActivated();
        $jsLibUrl = config('payment.gateways.trustPayment.js_lib_url');

        // Get user last transaction if using saved card is needed
        if($useSavedCard){
            $userLastTransaction = (new TrustPaymentTransactionsRepo())->getUserLastTransaction($user->id);
        }

        if($useSavedCard && $userLastTransaction && $userLastTransaction->mayBeUsedForNextPayment()){
            // Generate JWT token for saved card if needed and possible
            $jwtToken = $this->prepareJWTToken($cart->getTotal(), $userLastTransaction);
        } else {
            // Generate standard JWT if using saved card not needed or not possible
            $jwtToken = $this->prepareJWTToken($cart->getTotal());
            $useSavedCard = false;
        }

        return view('payment.trust-payment.checkout', compact(
            'cart',
            'user',
            'orderCreatingUrl',
            'orderCreatingMethod',
            'jwtToken',
            'livePaymentStatus',
            'jsLibUrl',
            'useSavedCard'
        ));
    }

    /**
     * Return last transaction for by user ID or for current user
     *
     * @param int|null $userId
     *
     * @return Model|null|TrustPaymentTransaction
     * @throws Exception
     */
    protected function getUserLastTransaction(int $userId = null): ?TrustPaymentTransaction
    {
        // Always check load transaction if user ID was set
        if($userId){
            return (new TrustPaymentTransactionsRepo())->getUserLastTransaction($userId);
        }

        // Try to use already loaded transaction
        if(empty($this->userLastTransaction)){
            $this->userLastTransaction = (new TrustPaymentTransactionsRepo())->getUserLastTransaction(auth()->id());
        }

        return $this->userLastTransaction;
    }

    /**
     * @param float                        $amount
     *
     * @param TrustPaymentTransaction|null $referencedTransaction
     *
     * @return string
     */
    protected function prepareJWTToken(float $amount, TrustPaymentTransaction $referencedTransaction = null)
    {
        $jwtSecret = config('payment.gateways.trustPayment.jwt.secret');
        $jwtUsername = config('payment.gateways.trustPayment.jwt.username');

        $user = auth()->user();

        $payload = [
            'iss' => $jwtUsername,
            'iat' => time(),
            'payload' => [
                'accounttypedescription' => 'ECOM',
                'baseamount' => $this->prepareBaseAmount($amount),
                'currencyiso3a' => 'GBP',
                'sitereference' => $this->prepareSitereference(),
                'billingemail' => $user->email,

                'billingfirstname' => $user->first_name,
                'billinglastname' => $user->last_name,
                'billingcounty' => $user->county,
                'billingpostcode' => $user->post_code,
                'billingtelephone' => $user->phone,
                'billingpremise' => $user->address,

                'customerfirstname' => $user->first_name,
                'customerlastname' => $user->last_name,
                'customercounty' => $user->county,
                'customerpostcode' => $user->post_code,
                'customertelephone' => $user->phone,
                'customerpremise' => $user->address,

                "requesttypedescriptions" => ["THREEDQUERY","AUTH"]
            ]
        ];

        // Add reference transaction if needed
        if($referencedTransaction){
            $payload['payload']['parenttransactionreference'] = $referencedTransaction->transactionreference;
        }

        return JWT::encode($payload, $jwtSecret, 'HS256');
    }

    /**
     * Prepare base amount for payment
     *
     * @param float $amount
     *
     * @return int
     */
    protected function prepareBaseAmount(float $amount): int
    {
        return round($amount * 100, 0);
    }

    /**
     * Prepare sitereference
     *
     * @return Repository|Application|mixed|string
     */
    public function prepareSitereference()
    {
        $siteReference = config('payment.gateways.trustPayment.sitereference');

        if($this->isLivePaymentActivated()){
            return $siteReference;
        }

        // Prepare special site reference for testing
        return "test_$siteReference";
    }

    /**
     * @return bool
     */
    public function isLivePaymentActivated(): bool
    {
        return !(bool)config('payment.gateways.trustPayment.sandbox_mode');
    }

    /**
     * Check if payment transaction exists
     *
     * @param Order $order
     *
     * @return bool
     * @throws Exception
     */
    public function hasTransactions(Order $order): bool
    {
        return (new TrustPaymentTransactionsRepo())->getCountForOrderId($order->id) > 0;
    }

    /**
     * Return transaction for payment if exists
     *
     * @param Order $order
     *
     * @return int
     * @throws Exception
     */
    public function getTransactions(Order $order)
    {
        return (new TrustPaymentTransactionsRepo())->getCountForOrderId($order->id);
    }

    /**
     * This function should be used for registering needed routes
     *
     * @return mixed
     */
    public function registerRoutes()
    {
        Route::post('trust-payment/result', "\App\Payment\TrustPayment\TrustPaymentTransactionsProcessor@finalizeOrderProcessing")->name('trust-payment.result');
    }
}
