<?php


namespace App\Payment\TrustPayment;


use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;

class TrustPaymentTransactionsRepo extends EntityRepo
{
    /** @var string  */
    protected $entity = TrustPaymentTransaction::class;

    /**
     * @param int $orderId
     *
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getByOrderId(int $orderId)
    {
        $query = $this->query()->where('order_id', $orderId);

        return $this->realGetMany($query);
    }

    /**
     * @param int $orderId
     *
     * @return int
     * @throws Exception
     */
    public function getCountForOrderId(int $orderId)
    {
        return $this->query()->where('order_id', $orderId)->count();
    }

    /**
     * @param int $userId
     *
     * @return Model|null
     * @throws Exception
     */
    public function getUserLastTransaction(int $userId)
    {
        $query = $this->query()
            ->where('user_id', $userId)
            ->orderBy('created_at', 'desc');

        return $this->realGetOne($query);
    }
}