<?php


namespace App\Payment\TrustPayment;


use App\Ecommerce\Orders\Order;
use Firebase\JWT\JWT;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TrustPaymentTransaction extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'order_id',
        'errorcode',
        'status',
        'transactionreference',
        'baseamount',
        'eci',
        'currencyiso3a',
        'errormessage',
        'enrolled',
        'settlestatus',
        'jwt',
        'user_id',
        'use_for_next_payment'
    ];

    /**
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return object
     */
    public function decodedJWT()
    {
        $jwtSecret = config('payment.gateways.trustPayment.jwt.secret');
        return JWT::decode($this->jwt, $jwtSecret, ['HS256']);
    }

    /**
     * Check if transaction was successful
     *
     * @return bool
     */
    public function isSuccessful(): bool
    {
        if($this->errorcode != 0 || $this->decodedJWT()->payload->response[0]->errorcode != 0){
            return false;
        }

        return in_array($this->decodedJWT()->payload->response[0]->requesttypedescription, ['AUTH', 'THREEDQUERY']);
    }

    /**
     * @return bool
     */
    public function mayBeUsedForNextPayment():bool
    {
        return (bool)$this->use_for_next_payment;
    }

    /**
     * Extract masked card type
     *
     * @return mixed
     */
    public function cardType()
    {
        return $this->decodedJWT()->payload->response[0]->paymenttypedescription;
    }

    /**
     * Extract masked card number
     *
     * @return mixed
     */
    public function cardNumber()
    {
        return $this->decodedJWT()->payload->response[0]->maskedpan;
    }

    /**
     * Return last 4 digits
     *
     * @return false|string
     */
    public function cartLast4Digits()
    {
        return substr($this->cardNumber(), -4);
    }
}