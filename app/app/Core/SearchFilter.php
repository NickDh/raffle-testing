<?php


namespace App\Core;


class SearchFilter extends SessionFilter
{
    /** @var string  */
    protected $searchPhrase = '';

    /**
     * Load params from request
     */
    public function initFromRequest()
    {
        $this->searchPhrase = request()->get('keyword', '');
        $this->updateSessionStorage('searchPhrase');
    }

    /**
     * Load params from session
     */
    public function initFromSession()
    {
        $this->loadFromSessionStorage('searchPhrase');
    }

    /**
     * @return string
     */
    public function getSearchPhrase(): string
    {
        return (string)$this->searchPhrase;
    }
}