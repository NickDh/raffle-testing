<?php

namespace App\Trash;


use App\Competitions\Competition;
use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Dashboard\Elements\Links\LinkButton;

class TrashDashboardPresenter
{
    /**
     * @param $items
     * @param Request $request
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function getTablePage($items, Request $request, Dashboard $dashboard)
    {
        $data['items'] = $request->get('items', 10);
        $data['page'] = $request->get('page', 1);
        $data['keyword'] = $request->get('keyword', '');

        $tablePageGenerator = (new TablePageGenerator())
            ->title('Trashed Competitions')
            ->tableTitles('ID', 'Competition', 'Tickets Sold', 'Tickets Remaining', 'Deadline', 'Status', 'Deleted At', 'Actions')
            ->showOnly([
                'id',
                'title',
                'tickets_sold',
                'tickets_remaining',
                'deadline',
                'status',
                'deleted_at'
            ])
            ->setConfig([
                'title' => function (Competition $item) {
                    return '<div style="min-width:250px; white-space: normal">' . $item->title . '</div>';
                },
                'deadline' => function (Competition $item) {
                    return date('jS F Y @ H:i', strtotime($item->deadline));
                },
                'tickets_sold' => function (Competition $competition) {
                    return $competition->sold_tickets->count();
                },
                'tickets_remaining' => function (Competition $competition) {
                    return $competition->tickets_count - $competition->sold_tickets->count();
                },
                'deleted_at' => function (Competition $item) {
                    return date('jS F Y @ H:i', strtotime($item->deleted_at));
                },
            ])
            ->addElementsToToolsCollection(
                function (Competition $item) {
                    $restoreBtn = (new LinkButton())
                        ->icon('fa-arrow-up')
                        ->class('btn-success js_ajax-by-click-btn')
                        ->dataAttr('action', route('dashboard::trash.restore-competition', $item))
                        ->dataAttr('confirm', true)
                        ->addContent('Restore')->render();

                    $deleteBtn = (new LinkButton())
                        ->icon('fa-trash')
                        ->content('Real Delete')->class('btn-danger js_ajax-by-click-btn')
                        ->dataAttr('action', route('dashboard::trash.destroy-competition', $item))
                        ->dataAttr('method', 'DELETE')
                        ->dataAttr('confirm', true)->render();

                    return '<div style="width:230px;">'.$restoreBtn.' '.$deleteBtn.'</div>';
                }
            )
            ->items($items)
            ->withPagination($items, route('dashboard::trash.index', $data))
        ;

        $tablePageGenerator->addFiltering()
            ->action(route('dashboard::trash.index'))
            ->method('GET')
            ->textInput('keyword', $request->get('keyword', ''), '', false)
            ->submitButton('Search');

        //$tablePageGenerator->getTable()->addClass('table-responsive');

        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }
}
