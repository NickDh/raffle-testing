<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Webmagic\Dashboard\Components\FormGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Pages\LoginPage;
use Carbon\Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Login username to be used by the controller.
     *
     * @var string
     */
    protected $username;


    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return $this->username;
    }


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['guest'])->except('logout');

        $this->username = $this->findUsername();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findUsername()
    {
        $login = request()->input('login');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        request()->merge([$fieldType => $login]);

        return $fieldType;
    }


    /**
     * Show the application's login form.
     *
     * @return LoginPage
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function showLoginForm()
    {
        return (new LoginPage($this->loginForm()));
    }

    /**
     * @return FormGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function loginForm()
    {
        $form = new FormGenerator();
        $form
            ->action(url('login'))
            ->ajax(false)
            ->textInput('login', old(), 'Login', true)
            ->passwordInput('password', null, 'Password', true)
            ->checkbox('remember', false, 'Remember me')
            ->submitButton(trans('dashboard::common.login_page.log_in'));

        return $form;
    }


    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $user->update([
            'last_login_at' => Carbon::now()->toDateTimeString(),
            'last_login_ip' => $request->getClientIp()
        ]);

        if ((int)$user->suspend === 1) {
            Auth::logout();

            return redirect()->back()->withErrors([
                'recaptcha' => 'Your account has been suspended',
            ]);
        } else {
            if($request->ajax()){
                return response()->json(['message'=> 'Success'], 200);
            }
        }
    }

    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function validateLogin(Request $request)
    {
        if (env('APP_ENV') == 'local'){
            $this->validate($request, [
                $this->username() => 'required|string',
                'password' => 'required|string'
                    ]);

        } else{
            $this->validate($request, [
                $this->username() => 'required|string',
                'password' => 'required|string',
                'recaptcha' => 'required|recaptcha',
            ], [
                'recaptcha.recaptcha' => 'Wait for recaptcha to upload',
                'recaptcha.required' => 'Wait for recaptcha to upload',
            ]);
        }

    }
}
