<?php


namespace App\Http\Controllers;


use App\Ecommerce\Orders\OrderProcessor;
use App\Ecommerce\Orders\OrderRepo;
use App\Ecommerce\Payment\FirstData\FirstDataResponseParser;
use App\Ecommerce\Payment\FirstData\PaymentMethod\PaymentMethodService;
use App\Ecommerce\Payment\FirstData\Transactions\Transaction;
use App\Ecommerce\Payment\FirstDataGatewayHandler;
use App\Ecommerce\Payment\PaymentProcessor;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;

class PaymentController extends Controller
{
    /**
     * Authorize session and return data for form
     *
     * @param FirstDataGatewayHandler $paymentJSHandler
     *
     * @param PaymentMethodService    $paymentMethodService
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function authorizeSession(FirstDataGatewayHandler $paymentJSHandler, PaymentMethodService $paymentMethodService)
    {
        Log::channel('payment')->info('User ' . Auth::id() . ' Payment JS authorization started');

        // Authenticate with PaymentJS
        $authorizationData = $paymentJSHandler->authorizeSession();

        // Save new payment method
        $paymentMethodService->addOrUpdate(
            Auth::id(),
            $authorizationData['clientToken'],
            $authorizationData['publicKeyBase64'],
            $authorizationData['nonce']
        );

        Log::channel('payment')->info('User ' . Auth::id() . ' Payment JS authorized with token '. $authorizationData['clientToken']);

        // Return authentication data
        return response()->json($authorizationData);
    }

    /**
     * Store data from FirstData Webhook
     *
     * @param Request              $request
     * @param PaymentMethodService $paymentMethodService
     *
     * @return Application|ResponseFactory|Response
     */
    public function storePaymentMethodData(Request $request, PaymentMethodService $paymentMethodService)
    {
        Log::channel('payment')->info("Received payment details");

        $clientToken = $request->header('client-token');
        Log::channel('payment')->info("Recognized payment details for client_token $clientToken");

        if(!$paymentMethod = $paymentMethodService->getByClientToken($clientToken)){
            Log::channel('payment')->error("Payment method for client_id $clientToken was not recognized");
            abort(500, 'Payment method was not recognized');
        }

        $paymentMethodService->addDetails(
            $clientToken,
            $request->json('card.token'),
            $request->json('gatewayRefId'),
            $request->json('card.masked'),
            $request->json('card.last4'),
            $request->json('card.brand'),
        );

        Log::channel('payment')->info("Stored payment details for client_token '$clientToken'");

        return response('');
    }

    /**
     * Process second step for 3D v1.0 flow
     *
     * @param string           $transactionId
     *
     * @param PaymentProcessor $paymentProcessor
     *
     * @param Request          $request
     *
     * @param OrderProcessor   $orderProcessor
     *
     * @return Application|ResponseFactory|Response
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function final3D1_0Page(string $transactionId, PaymentProcessor $paymentProcessor, Request $request, OrderProcessor $orderProcessor)
    {
        Log::channel('payment')->info("Transaction #$transactionId 3D secure v1.0 final step initiated");

        /** @var Transaction $transaction */
        $transaction = Transaction::find($transactionId);
        if(!$transaction){
            abort(404);
        }
        // Save request details to the transaction
        $transaction = (new FirstDataResponseParser())->parse3DSecure1_0Initiation_request($request, $transaction);
        $transaction = $paymentProcessor->doFirstDataGateway3DSecure1_0FinalRequest($transaction);

        $order = $orderProcessor->processOrderByFinalizedTransaction($transaction->order, $transaction);

        Log::channel('payment')->info("Transaction #$transactionId final step processed");

        return view('payment-result', compact('order'));
    }


    /**
     * Process second step for 3D secure flow
     *
     * @param string           $transactionId
     *
     * @param PaymentProcessor $paymentProcessor
     *
     * @param Request          $request
     *
     * @param OrderProcessor   $orderProcessor
     *
     * @return Application|ResponseFactory|Response
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function process3DSecureStepTwo(
        string $transactionId,
        PaymentProcessor $paymentProcessor,
        Request $request,
        OrderProcessor $orderProcessor
    ) {
        Log::channel('payment')->info("Transaction #$transactionId 3D secure v2.1 second step initiated");

        /** @var Transaction $transaction */
        $transaction = Transaction::find($transactionId);
        if(!$transaction){
            abort(404);
        }

        // Save request details to the transaction
        $transaction->secure_3D_api_initiation_request = $request->getContent();
        $transaction->save();

        $transaction = $paymentProcessor->doFirstDataGateway3DSecure2_1SecondStep($transaction);

        $orderProcessor->processOrderByFinalizedTransaction($transaction->order, $transaction);

        Log::channel('payment')->info("Transaction #$transactionId 3D secure v2.1 second step processed");

        return \response('');
    }

    /**
     * @param string $transactionId
     *
     * @return Application|Factory|View
     */
    public function process3DSecure2_1StepThreeForm(string $transactionId)
    {
        Log::channel('payment')->info("Transaction #$transactionId 3D secure v2.1 third step (form) initiated");

        /** @var Transaction $transaction */
        $transaction = Transaction::find($transactionId);
        if(!$transaction){
            abort(404);
        }

        return view('payment.3DSecure-v2-1-form', compact('transaction'));
    }

    /**
     * @param string $transactionId
     *
     * @return JsonResponse
     */
    public function process3D2_1SecureStatus(string $transactionId)
    {
        /** @var Transaction $transaction */
        $transaction = Transaction::find($transactionId);
        if(!$transaction){
            abort(404);
        }

        // Redirect to result page when order is processed
        if($transaction->isProcessed() && $transaction->order->isProcessed()){
            return \response()->json([
                'status' => 'Ready',
                'redirect' => route('payment.payment-processing.result', $transaction->order_id)
            ]);
        }

        // Redirect to 3D secure v2.1 form to continue auth process
        if($transaction->is3DSecure2_1FormStepThreeRequired()){
            return \response()->json([
                'status' => 'Ready',
                'redirect' => route('process3dSecure2_1-transaction.form', $transaction->id)
            ]);
        }

        return \response()->json([
            'status' => 'Waiting'
        ]);
    }

    /**
     * @param string    $orderId
     * @param OrderRepo $orderRepo
     *
     * @return Application|Factory|View
     * @throws \Exception
     */
    public function paymentProcessingResult(string $orderId, OrderRepo $orderRepo)
    {
        $order = $orderRepo->getByID($orderId);

        if(!$order || $order->user_id != auth()->user()->id){
            abort(404);
        }

        return view('payment-result', compact('order'));
    }

    /**
     * @param string  $transactionId
     * @param Request $request
     *
     * @return Application|Factory|View
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function secure3DFinalRequest(string $transactionId, Request $request)
    {
        Log::channel('payment')->info("Transaction #$transactionId 3D secure v2.1 final step initiated");

        /** @var Transaction $transaction */
        $transaction = Transaction::find($transactionId);
        if(!$transaction){
            abort(404);
        }

        $transaction = (new FirstDataResponseParser())->parse3DSecure2_1SecondInitiation_request($request, $transaction);

        $transaction = (new PaymentProcessor())->doFirstDataGateway3DSecure2_1FinalRequest($transaction);
        $order = (new OrderProcessor())->processOrderByFinalizedTransaction($transaction->order, $transaction);

        Log::channel('payment')->info("Transaction #$transactionId 3D secure v2.1 final step processed");

        return view('payment-result', compact('order'));
    }
}