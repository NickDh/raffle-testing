<?php


namespace App\Http\Controllers\Dashboard;


use App\Ecommerce\CompetitionEntries\CompetitionEntriesExport;
use App\Ecommerce\CompetitionEntries\CompetitionEntriesRepo;
use App\Ecommerce\CompetitionEntries\Dashboard\CompetitionEntriesFilter;
use App\Ecommerce\CompetitionEntries\Dashboard\CompetitionEntriesTablePageGenerator;
use Exception;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Webmagic\Core\Controllers\Controller;
use Webmagic\Dashboard\Elements\Tables\Table;

class CompetitionEntriesController extends Controller
{
    /**
     * @param null                                 $competitionId
     * @param CompetitionEntriesRepo               $competitionEntriesRepo
     * @param CompetitionEntriesTablePageGenerator $pageGenerator
     *
     * @param CompetitionEntriesFilter             $filter
     *
     * @return Table
     * @throws Exception
     */
    public function table(
        $competitionId = null,
        CompetitionEntriesRepo $competitionEntriesRepo,
        CompetitionEntriesTablePageGenerator $pageGenerator,
        CompetitionEntriesFilter $filter
    ) {
        $perPage = 25;

        $filter->initiate();

        if($competitionId){
            $entries = $competitionEntriesRepo->getByCompetitionId($competitionId, $perPage, $filter);
        } else {
            $entries = $competitionEntriesRepo->getAll($perPage, $filter);
        }

        $pageGenerator->prepare($entries, $competitionId);

        return $pageGenerator->getTable();
    }

    /**
     * @param null                     $competitionId
     * @param CompetitionEntriesFilter $filter
     * @param CompetitionEntriesRepo   $competitionEntriesRepo
     *
     * @return BinaryFileResponse
     * @throws Exception
     */
    public function export($competitionId = null, CompetitionEntriesFilter $filter, CompetitionEntriesRepo $competitionEntriesRepo)
    {
        $filter->initiateFromSession();

        if($competitionId){
            $entries = $competitionEntriesRepo->getForExport($competitionId, $filter);
        } else {
            $entries = $competitionEntriesRepo->getForExport(null, $filter);
        }

        $fileName = "Competition_{$competitionId}_entries_". now()->format('d-m-Y').'.csv';

        return Excel::download(new CompetitionEntriesExport($entries), $fileName);
    }
}