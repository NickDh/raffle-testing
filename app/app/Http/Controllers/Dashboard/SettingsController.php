<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Settings\SettingsRepo;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;

class SettingsController extends Controller
{
    use AjaxRedirectTrait;

    /**
     * Settings page
     *
     * @param SettingsRepo $settingsRepo
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function edit(SettingsRepo $settingsRepo)
    {
        $settings = $settingsRepo->getAll()->first();

        return (new FormPageGenerator())
            ->title('Settings page')
            ->method('PUT')
            ->action(route('dashboard::settings.update'))
            ->ajax(true)
            ->textInput('admin_email', $settings['admin_email'], 'Admin Email', true)
            ->numberInput('cart_lifetime', $settings['cart_lifetime'], 'Basket Timeout Duration (Minutes)', true, 1, 1)
//            ->select('view_sort_by', array_combine(config('project.view_sort_by'), config('project.view_sort_by')), $settings['view_sort_by'], 'Competitions Per Page', true)
            ->numberInput('view_sort_by', $settings['view_sort_by'], 'Competitions Per Page', true)
            ->numberInput('random_generator_value', $settings['random_generator_value'], 'Max numbers in random generator', true, 1, 1)
            ->textarea('google_analytics', $settings['google_analytics'], 'Google Analytics')
            ->textarea('custom_css', $settings['custom_css'], 'Custom CSS')
            ->submitButtonTitle('Update settings');
    }

    /**
     * Update settings
     *
     * @param Request $request
     * @param SettingsRepo $settingsRepo
     * @return ResponseFactory|Model|Response|void
     * @throws Exception
     */
    public function update(Request $request, SettingsRepo $settingsRepo)
    {
        $validData = $request->validate([
            'admin_email' => 'required|email',
            'cart_lifetime' => 'required|int',
            'google_analytics' => 'sometimes',
            'view_sort_by' => 'required|int',
            'custom_css' => 'sometimes',
            'random_generator_value' => 'required|int',
        ]);

        $settings = $settingsRepo->getAll()->first();
        if(! $settings){
            return $settingsRepo->create($validData);
        }

        if(! $settingsRepo->update($settings['id'], $validData)){
            return abort(500, 'Cannot store settings!');
        }

        if($request->has('custom_css')){
            $path = public_path('css/custom.css');
            File::put($path, $request->get('custom_css'));
        }

        return response('Ok', 200);
    }


}
