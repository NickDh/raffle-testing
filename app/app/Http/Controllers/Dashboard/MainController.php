<?php

namespace App\Http\Controllers\Dashboard;


use App\Competitions\Competition;
use App\Competitions\CompetitionRepo;
use App\Ecommerce\Orders\OrdersAnalytic;
use App\Enums\CompetitionStatusEnum;
use App\Http\Controllers\Controller;
use App\Ecommerce\Orders\OrderRepo;

use Webmagic\Dashboard\Components\TableGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Dashboard\Elements\Boxes\Box;

class MainController extends Controller
{
    /**
     * @param Dashboard       $dashboard
     * @param CompetitionRepo $competitionRepo
     * @param OrderRepo       $orderRepo
     *
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function index(Dashboard $dashboard, CompetitionRepo $competitionRepo, OrderRepo $orderRepo)
    {
        $competitions = $competitionRepo->getByStatus([CompetitionStatusEnum::LIVE, CompetitionStatusEnum::SOLD_OUT], false);
        $orders = $orderRepo->getAllPaidFromPastDay();

        $table =  (new TableGenerator())
            ->tableTitles('Competition', 'Tickets Sold', 'Tickets Remaining', 'Progress')
            ->showOnly([
                'title',
                'tickets_sold',
                'tickets_remaining',
                'progress'
            ])
            ->setConfig([
                'tickets_sold' => function (Competition $competition) {
                    return $competition->soldTicketsCount();
                },
                'tickets_remaining' => function (Competition $competition) {
                    return $competition->tickets_count - $competition->soldTicketsCount();
                },
                'progress' => function (Competition $competition) {
                    $width = calculatePercentage($competition->tickets_count, $competition->soldTicketsCount());

                    return '<div class="progress progress-xs progress-striped active" style="width:200px">
                      <div class="progress-bar progress-bar-success" style="width: '.$width.'%"></div>
                    </div>';
                },

            ])
            ->items($competitions)
        ;

        $table->getTable()->class('table table-bordered');

        $monthlyEarnings = (new OrdersAnalytic())->getMonthlyAnalytics();

        $chart = view('dashboard.chart', [
            'titles' => array_keys($monthlyEarnings),
            'values' => array_values($monthlyEarnings),
            'ordersCount' => $orders->count(),
            'revenue' => '£ '.formatValue($orders->sum('total')),
            'competitionsCount' => $competitions->where('status', CompetitionStatusEnum::LIVE)->count()
        ]);

        $title= '<div class="col-lg-9 col-xs-9 h3" style="padding-left:0">Live Competitions</div>';
        $box = new Box();
        $box->addContent($table);
        $box->addBoxHeaderContent($title);

        $dashboard->addContent($chart);
        $dashboard->addContent($box);
        $dashboard->page()->addTitle('Dashboard');
        return $dashboard;
    }
}
