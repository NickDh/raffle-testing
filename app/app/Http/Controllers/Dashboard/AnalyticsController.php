<?php

namespace App\Http\Controllers\Dashboard;

use App\Analytics\AnalyticsRepo;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;

class AnalyticsController extends Controller
{
    use AjaxRedirectTrait;

    /**
     * Analytics page
     *
     * @param AnalyticsRepo $analyticsRepo
     * @return FormPageGenerator
     * @throws NoOneFieldsWereDefined
     * @throws FieldUnavailable
     * @throws Exception
     */
    public function edit(AnalyticsRepo $analyticsRepo)
    {
        $analytics = $analyticsRepo->getAll()->first();

        return (new FormPageGenerator())
            ->title('Tracking Code Integration')
            ->method('PUT')
            ->action(route('dashboard::analytics.update'))
            ->ajax(true)
            ->textarea('head_all', $analytics['head_all'], 'Head (All Pages)')
            ->textarea('body_all', $analytics['body_all'], 'Body (All Pages)')
            ->textarea('body_checkout', $analytics['body_all'], 'Body (Checkout)')
            ->textarea('body_successful_order', $analytics['body_successful_order'], 'Body (Successful Order)')
            ->textarea('body_unsuccessful_order', $analytics['body_unsuccessful_order'], 'Body (Unsuccessful Order)')
            ->submitButtonTitle('Update Tracking');
    }

    /**
     * Update analytics
     *
     * @param Request $request
     * @param AnalyticsRepo $analyticsRepo
     * @return Application|ResponseFactory|Model|Response|void
     * @throws Exception
     */
    public function update(Request $request, AnalyticsRepo $analyticsRepo)
    {
        $validData = $request->validate([
            'head_all'  =>  'string|nullable',
            'body_all'  =>  'string|nullable',
            'body_checkout' =>  'string|nullable',
            'body_successful_order' =>  'string|nullable',
            'body_unsuccessful_order'   =>  'string|nullable',
        ]);

        $validData = [
            'head_all'  =>  $request->get('head_all') ?? '',
            'body_all'  =>  $request->get('body_all') ?? '',
            'body_checkout' =>  $request->get('body_checkout') ?? '',
            'body_successful_order' => $request->get('body_successful_order') ?? '',
            'body_unsuccessful_order'   =>  $request->get('body_unsuccessful_order') ?? '',
        ];

        $analytics = $analyticsRepo->getAll()->first();
        if(! $analytics){
            return $analyticsRepo->create($validData);
        }

        if(! $analyticsRepo->update($analytics['id'], $validData)){
            return abort(500, 'Cannot store analytics!');
        }
        return response('Ok', 200);
    }
}
