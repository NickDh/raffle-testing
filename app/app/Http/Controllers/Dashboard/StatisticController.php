<?php

namespace App\Http\Controllers\Dashboard;

use App\Ecommerce\Orders\OrderRepo;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class StatisticController extends Controller
{
    use AjaxRedirectTrait;

    /**
     * @param Dashboard $dashboard
     * @param OrderRepo $orderRepo
     * @param Request $request
     * @return Application|Factory|View|Dashboard
     * @throws NoOneFieldsWereDefined
     */
    public function index(Dashboard $dashboard, OrderRepo $orderRepo, Request $request)
    {
        $date_from = $request->get('date_from', null);
        $date_to = $request->get('date_to', null);


        $totalSales = formatValue($orderRepo->countSumByPeriod('total', $date_from, $date_to));
        $ordersCount = formatValue($orderRepo->paidOrdersCountByPeriod($date_from, $date_to));
        $averageOrders = $ordersCount > 0 ? formatValue($totalSales / $ordersCount) : 0;
        $couponUsed = formatValue($orderRepo->countSumByPeriod('discount_value', $date_from, $date_to));
        $failedOrdersCount = $orderRepo->failedOrdersCount($date_from, $date_to);

        $netSales = $totalSales - $couponUsed;

        $data = compact('totalSales', 'ordersCount', 'averageOrders', 'couponUsed', 'netSales', 'date_from', 'date_to', 'failedOrdersCount');

        if ($request->ajax()) {
            return view('dashboard.statistic', $data);
        }

        $dashboard->page()
            ->addContentHeader("<h1>Statistics</h1>")
            ->addContent(
                view('dashboard.statistic', $data)
            );

        return $dashboard;
    }
}
