<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Questions\AnswerService;
use App\Questions\QuestionDashboardPresenter;
use App\Questions\QuestionRepo;
use App\Services\ImagesPrepare;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class QuestionController extends Controller
{
    use AjaxRedirectTrait;


    /**
     * @param QuestionRepo $questionRepo
     * @param Dashboard $dashboard
     * @param Request $request
     * @param QuestionDashboardPresenter $dashboardPresenter
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function index(
        QuestionRepo $questionRepo,
        Dashboard $dashboard,
        Request $request,
        QuestionDashboardPresenter $dashboardPresenter
    )
    {
        $questions = $questionRepo->getByFilter(
            $request->get('items', 10),
            $request->get('page', 1)
        );

        return $dashboardPresenter->getTablePage($questions, $dashboard, $request);
    }

    /**
     * Get creating form
     *
     * @param Dashboard $dashboard
     * @param QuestionDashboardPresenter $dashboardPresenter
     * @return mixed
     */
    public function create(Dashboard $dashboard, QuestionDashboardPresenter $dashboardPresenter)
    {
        return $dashboardPresenter->getCreateForm($dashboard);
    }


    /**
     * Create question and answers
     *
     * @param Request $request
     * @param QuestionRepo $questionRepo
     * @param ImagesPrepare $imagesPrepare
     * @param AnswerService $answerService
     * @return Application|ResponseFactory|JsonResponse|RedirectResponse|Response|Redirector
     * @throws Exception
     */
    public function store(
        Request $request,
        QuestionRepo $questionRepo,
        ImagesPrepare $imagesPrepare,
        AnswerService $answerService
    ){
        $request->validate([
            'question' => 'required|string',
            'is_true' => 'required',
        ]);


        if ( !$question = $questionRepo->create($request->all())) {
            return response('Error on creating', 500);
        }

        try {
            $answerService->preparingAndCreatingFromRequest($question->id, $request, $imagesPrepare);
        } catch (Exception $e) {
//            logger("can't save answer for question: $question->id");
        }


        return $this->redirect(route('dashboard::questions.index'));
    }


    /**
     * @param int $item_id
     * @param QuestionRepo $questionRepo
     * @param QuestionDashboardPresenter $dashboardPresenter
     * @param Dashboard $dashboard
     * @return mixed|void
     * @throws Exception
     */
    public function edit(
        int $item_id,
        QuestionRepo $questionRepo,
        QuestionDashboardPresenter $dashboardPresenter,
        Dashboard $dashboard
    ){
        if (!$question = $questionRepo->getByID($item_id)) {
            return abort(404);
        }

        return $dashboardPresenter->getEditForm($dashboard, $question);
    }

    /**
     * Update question and create, update or delete answers by request data
     *
     * @param int $item_id
     * @param QuestionRepo $questionRepo
     * @param Request $request
     * @param ImagesPrepare $imagesPrepare
     * @param AnswerService $answerService
     * @throws Exception
     */
    public function update(
        int $item_id,
        QuestionRepo $questionRepo,
        Request $request,
        ImagesPrepare $imagesPrepare,
        AnswerService $answerService
    ){
        $request->validate([
            'is_true' => 'required',
        ]);

        if (!$question = $questionRepo->getByID($item_id)) {
            return abort(404);
        }

        try {

            //delete answers
            if ($request->has('delete_id')) {
                $ids = explode(',', $request->get('delete_id'));
                $answerService->deleteByIdAndClearImages($ids, $imagesPrepare);
            }

            //create or update answers
            $answerService->preparingAndCreatingFromRequest($question->id, $request, $imagesPrepare);

        } catch (Exception $e) {
//            logger("error on question updating: $question->id");
        }
    }

    /**
     * Delete question and their answers with images clearing
     *
     * @param int $item_id
     * @param QuestionRepo $questionRepo
     * @param AnswerService $answerService
     * @param ImagesPrepare $imagesPrepare
     * @return JsonResponse|RedirectResponse|Redirector
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     * @throws Exception
     */
    public function destroy(int $item_id, QuestionRepo $questionRepo, AnswerService $answerService, ImagesPrepare $imagesPrepare)
    {
        if (!$question = $questionRepo->getByID($item_id)) {
            abort(404);
        };

        $ids = $question->answers->pluck('id')->toArray();
        $answerService->deleteByIdAndClearImages($ids, $imagesPrepare);

        if (!$questionRepo->destroy($item_id)) {
            abort(500, 'Error on destroying');
        }

        return $this->redirect(route('dashboard::questions.index'));
    }
}
