<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Results\ResultDashboardPresenter;
use App\Results\ResultRepo;
use App\Services\ImagePathManager;
use App\Services\ImagesPrepare;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class ResultController extends Controller
{
    use AjaxRedirectTrait;


    /**
     * @param ResultRepo $resultRepo
     * @param Dashboard $dashboard
     * @param Request $request
     * @param ResultDashboardPresenter $dashboardPresenter
     * @return mixed|Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function index(
        ResultRepo $resultRepo,
        Dashboard $dashboard,
        Request $request,
        ResultDashboardPresenter $dashboardPresenter
    )
    {
        $items = $resultRepo->getByFilter(
            $request->get('items', 10),
            $request->get('page', 1)
        );

        return $dashboardPresenter->getTablePage($items, $dashboard, $request);
    }

    /**
     * @param ResultDashboardPresenter $dashboardPresenter
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function create(ResultDashboardPresenter $dashboardPresenter)
    {
        return $dashboardPresenter->getCreateForm();
    }

    /**
     * @param Request $request
     * @param ResultRepo $resultRepo
     * @param ImagesPrepare $imagesPrepare
     * @return ResponseFactory|JsonResponse|RedirectResponse|Response|Redirector
     * @throws Exception
     */
    public function store(Request $request, ResultRepo $resultRepo, ImagesPrepare $imagesPrepare)
    {
        $request->validate([
            'image' => 'image:jpeg,png'
        ]);

        $data = $imagesPrepare->saveFiles(
            $request->all(),
            ['image'],
            (new ImagePathManager())->publicPathForResultsImages()
        );

        if (!$resultRepo->create($data)) {
            return response('Error on creating', 500);
        }

        return $this->redirect(route('dashboard::results.index'));
    }

    /**
     * @param int $item_id
     * @param ResultRepo $resultRepo
     * @param ResultDashboardPresenter $dashboardPresenter
     * @return void|FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function edit(int $item_id, ResultRepo $resultRepo, ResultDashboardPresenter $dashboardPresenter)
    {
        if (!$item = $resultRepo->getByID($item_id)) {
            return abort(404);
        }

        return $dashboardPresenter->getEditForm($item);
    }

    /**
     * @param int $item_id
     * @param ResultRepo $resultRepo
     * @param Request $request
     * @param ImagesPrepare $imagesPrepare
     * @return ResponseFactory|Response|void
     * @throws Exception
     */
    public function update(int $item_id, ResultRepo $resultRepo, Request $request, ImagesPrepare $imagesPrepare)
    {
        $request->validate([
            'image' => 'image:jpeg,png'
        ]);

        $data = $imagesPrepare->saveFiles(
            $request->all(),
            ['image'],
            (new ImagePathManager())->publicPathForResultsImages()
        );

        if (!$item = $resultRepo->getByID($item_id)) {
            return abort(404);
        }

        if(isset($data['image']) && $data['image'] != $item->image){
            $imagesPrepare->deleteLocalFile($item->present()->getLocalPath($item->image));
        }

        if (!$resultRepo->update($item->id, $data)) {
            return response('Error on updating', 500);
        }
    }

    /**
     * @param int $item_id
     * @param ResultRepo $resultRepo
     * @param ImagesPrepare $imagesPrepare
     * @return JsonResponse|RedirectResponse|Redirector
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     * @throws Exception
     */
    public function destroy(int $item_id, ResultRepo $resultRepo, ImagesPrepare $imagesPrepare)
    {
        if (!$result = $resultRepo->getByID($item_id)) {
            abort(404);
        };

        $imagesPrepare->deleteLocalFile($result->present()->getLocalPath($result->image));

        if (!$resultRepo->destroy($item_id)) {
            abort(500, 'Error on destroying');
        }

        return $this->redirect(route('dashboard::results.index'));
    }
}
