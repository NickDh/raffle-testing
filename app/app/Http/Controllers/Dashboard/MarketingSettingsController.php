<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Marketing\MarketingSettings;
use App\Settings\SettingsRepo;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;

class MarketingSettingsController extends Controller
{
    use AjaxRedirectTrait;

    /**
     * Settings page
     *
     * @param MarketingSettings $settingsRepo
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function edit(MarketingSettings $settingsRepo)
    {
        $settings = $settingsRepo->getAllSettings();


        $formPageGenerator =  new FormPageGenerator();
        $formPageGenerator->title('Marketing Settings page')
            ->method('PUT')
            ->action(route('dashboard::marketing.settings.update'))
            ->ajax(true);

        foreach ($settings as $settingEntity){
            switch ($settingEntity->type) {
                case 'textInput':
                    $formPageGenerator->textInput($settingEntity->key,$settingEntity->value,$settingEntity->title);
                    break;
                case 'textarea':
                    $formPageGenerator->textarea($settingEntity->key,$settingEntity->value,$settingEntity->title);
                    break;
                case 'numberInput':
                    $formPageGenerator->numberInput($settingEntity->key,$settingEntity->value,$settingEntity->title);
                    break;
            }
        }
        $formPageGenerator->submitButtonTitle('Update settings');

        return $formPageGenerator;
    }

    public function update(Request $request,MarketingSettings $settings)
    {
        $rules = MarketingSettings::getValidationRules();
        $data = $this->validate($request, $rules);

        $validSettings = array_keys($rules);

        foreach ($data as $key => $val) {
            if (in_array($key, $validSettings)) {
                MarketingSettings::add($key, $val);
            }
        }

        return response('Ok', 200);
    }


}
