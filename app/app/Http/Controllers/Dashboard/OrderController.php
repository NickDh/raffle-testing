<?php

namespace App\Http\Controllers\Dashboard;

use App\Competitions\CompetitionService;
use App\Ecommerce\Orders\OrderFilter;
use App\Ecommerce\Orders\OrderService;
use App\Enums\OrderStatusEnum;
use App\Http\Controllers\Controller;
use App\Ecommerce\Orders\OrderDashboardPresenter;
use App\Ecommerce\Orders\OrderRepo;
use App\Tickets\TicketRepo;
use Exception;
use Illuminate\Http\Request;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class OrderController extends Controller
{
    use AjaxRedirectTrait;

    /**
     * @param OrderRepo $orderRepo
     * @param Dashboard $dashboard
     * @param Request $request
     * @param OrderDashboardPresenter $dashboardPresenter
     * @return mixed|Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function index(
        OrderRepo $orderRepo,
        Dashboard $dashboard,
        Request $request,
        OrderDashboardPresenter $dashboardPresenter,
        OrderFilter $filter
    )
    {
        if($request->ajax()){
            $filter->initFromRequest();
        } else {
            $filter->initFromSession();
        }

        $items = $orderRepo->getByFilter(
            $filter->getSearchPhrase(),
            $request->get('items', 10),
            $request->get('page', 1),
            'created_at',
            'desc',
            true,
            true
        );


        return $dashboardPresenter->getTablePage($items, $dashboard, $request, $filter);
    }

    /**
     * @param int $item_id
     * @param OrderRepo $orderRepo
     * @param Dashboard $dashboard
     * @param OrderDashboardPresenter $dashboardPresenter
     * @return void|Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function show(int $item_id, OrderRepo $orderRepo, Dashboard $dashboard, OrderDashboardPresenter $dashboardPresenter)
    {
        if (!$order = $orderRepo->getByID($item_id, true)) {
            return abort(404);
        }

        $dashboardPresenter->getDescriptionList($order, $dashboard);

        return $dashboardPresenter->getOrderItemsTable($order, $dashboard);
    }

    /**
     * @param int $item_id
     * @param OrderRepo $orderRepo
     * @param TicketRepo $ticketRepo
     * @param CompetitionService $competitionService
     * @return mixed
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     * @throws Exception
     */
    public function cancel(int $item_id, OrderRepo $orderRepo, TicketRepo $ticketRepo, CompetitionService $competitionService)
    {
        if (!$order = $orderRepo->getByID($item_id, true)) {
            abort(404);
        };

        $order->status = OrderStatusEnum::CANCELLED();
        $order->save();
        //if (! $orderRepo->update($item_id, ['status' => OrderStatusEnum::CANCELLED()])){
        //    return abort(500, 'Error on updating');
        //}

        foreach ($order->cart_data as $competition_id => $item) {
            $ticketsIds = explode(';',$item['ticket_id']);
            $ticketRepo->destroy($ticketsIds);

            $competitionService->setLiveStatusIfCan($competition_id);
        }

        return response()->json(['message'=> 'Success'], 200);
    }

}
