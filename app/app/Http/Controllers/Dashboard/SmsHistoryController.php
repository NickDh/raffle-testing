<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Marketing\Twilio\Dashboard\SmsFilter;
use App\Marketing\Twilio\SmsRepo;
use App\Marketing\Twilio\Dashboard\SmsHistoryTablePageGenerator;


use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

class SmsHistoryController extends Controller
{
    use AjaxRedirectTrait;

    /**
     * @param SmsRepo $smsRepo,
     * @param SmsHistoryTablePageGenerator $pageGenerator,
     * @param Request       $request
     * @param SmsFilter     $filter
     *
     * @return mixed|Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function index(
        SmsRepo $smsRepo,
        SmsHistoryTablePageGenerator $pageGenerator,
        Request $request,
        SmsFilter $filter
    ) {
        $perPage = 100;

        if($request->ajax()){
            $filter->loadParams();
        } else {
            $filter->initFromSession();
        }
        $datefilter = $request->get('filter', null);

        $messages = $smsRepo->getByFilter(
            $filter->getSearchPhrase(),
            $request->get('items', 10),
            $request->get('page', 1),
            'created_at',
            'desc',
            null,
            $datefilter['from'],
            $datefilter['to'],
        );

        $pageGenerator->prepare($messages, $filter);

        if ($request->ajax()) {
            return $pageGenerator->getTable();
        }

        return $pageGenerator;
    }


    /**
     * @param int $item_id
     * @param SmsRepo $smsRepo
     * @return JsonResponse|RedirectResponse|Redirector
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     * @throws Exception
     */
    public function destroy(int $item_id, SmsRepo $smsRepo)
    {
        if (!$sms = $smsRepo->getByID($item_id)) {
            abort(404);
        };

        //$sms->SendedSms->delete();

        if (!$smsRepo->destroy($item_id)) {
            abort(500, 'Error on destroying');
        }

        return $this->redirect(route('dashboard::sms.history.index'));
    }


}
