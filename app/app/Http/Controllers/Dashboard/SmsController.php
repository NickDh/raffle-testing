<?php

namespace App\Http\Controllers\Dashboard;

use App;
use App\Jobs\ProcessTwilioMessage;
use App\Marketing\Twilio\SmsTableService;
use App\Marketing\Twilio\TwilioService;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Marketing\Twilio\SendedSms;
use App\Marketing\Twilio\Dashboard\UserSmsDashboardPresenter;
use Illuminate\Support\Facades\Auth;

use App\Marketing\Twilio\SmsRepo;
use Carbon\Carbon;
use Exception;

use Illuminate\Contracts\Routing\ResponseFactory;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Log;
use Webmagic\Core\Controllers\AjaxRedirectTrait;

use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;

use Webmagic\Dashboard\Elements\Boxes\Box;

use App\Users\User;
use App\Marketing\Twilio\TwilioMessage;
use App\Marketing\Twilio\Dashboard\SmsUserFilter as UserFilter;
use Yajra\DataTables\DataTables;

class SmsController extends Controller
{
    use AjaxRedirectTrait;


    /**
     * @param Dashboard $dashboard
     * @return mixed
     */
    public function index(Dashboard $dashboard)
    {

        $box = new Box();

        $title= '<div class="col-lg-9 col-xs-9 h3" style="padding-left:0">Send SMS</div>';
        $box->addBoxHeaderContent($title);

        $box->addContent(view('dashboard.sms_create_form'));

//        $box->addBoxHeaderContent('Form');
        $dashboard->page()->addTitle('Dashboard');
//        $dashboard->addFooter('');

        $dashboard->addContent($box);
        // dd($dashboard);

        return $dashboard;
        //return $dashboard->addContent(view('dashboard.sms_create_form'));
    }

    /**
     * @param UserSmsDashboardPresenter $dashboardPresenter
     * @param Dashboard $dashboard,
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function create(UserSmsDashboardPresenter $dashboardPresenter,Dashboard $dashboard)
    {
        return $dashboardPresenter->getCreateFormPage($dashboard);
    }

    /**Функция должна создать и сохранить сообщение.
     * Сохраняем и вызываем Джобу (или на ивент подпишем)
     *
     *
     * @param Request $request
     * @param TwilioMessage $twilioMessage
     * @return ResponseFactory|JsonResponse|RedirectResponse|Response|Redirector
     * @throws Exception
     */
    public function store(Request $request)
    {

        $request->validate([
            'text' => 'required'
        ]);

        $admin_user_id = $id = Auth::id();

        $text = $request->input('text');
        try {
            $twilioMessage = TwilioMessage::create([
                'text'=> $text,
                'created_by'=> $admin_user_id,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ]);
        }catch (Exception $exception){
            return response($exception->getMessage(), $exception->getCode());
        }


        $users_ids = $request->input('users_ids');
        if (empty($users_ids)){
            return response('Empty users', 401);
        }

        if (is_numeric($users_ids)){
            $user = User::find($users_ids);
            try{
                $twilioMessage->messageSend($user);
            }catch(Exception $exception){
                return response('Not saved', 200);
            }
        } elseif(is_array($users_ids)){
            //Этот вариант будет отправлять в очередь
            foreach ($users_ids as $users_id ){
                $user = User::find($users_id);
                $twilioMessage->messageSend($user);
            }
        }

        return response('Message created', 200);
    }

    /**
     * @param Request $request
     */
    public  function  saveToHistory(Request $request){
        Log::debug('Callback from twilio received',compact($request));

        $sended = SendedSms::query()
            ->where('sid', $request->get('SmsSid'))
            ->update([
                'status' => $request->get('SmsStatus'),
            ]);
        Log::debug('Message status updated',compact($sended));

    }

    /**
     * @param Request $request
     */
    public  function  makeQueueFromMessage(Request $request){
        $request->validate([
            'text_message' => 'required',
            'form' => 'required',
            'users_ids' => 'required',
        ]);

        $admin_user_id = $id = Auth::id();

        $text = $request->get('text_message');
        $form = $request->get('form');
        $users_ids = $request->get('users_ids');


        Log::debug('TwilioMessage',['text'=>$text]);

        try {
            $twilioMessage = TwilioMessage::create([
                'text'=> $text,
                'created_by'=> $admin_user_id,
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'query' =>$form
            ]);
        }catch (Exception $exception){
            return response($exception->getMessage(), $exception->getCode());
        }

        Log::debug('create TwilioMessage',['id'=>$twilioMessage->id,'text'=>$twilioMessage->text,'users_id'=>$users_ids]);


        if (is_numeric($users_ids)){
            $twilioMessage->users()->attach($users_ids);
        } elseif (is_string($users_ids)) {
            $users_id_array = explode(',',$users_ids);
            foreach ($users_id_array as $users_id ){
                $twilioMessage->users()->attach($users_id);
            }
        }elseif(is_array($users_ids)){
            foreach ($users_ids as $users_id ){
                $twilioMessage->users()->attach($users_id);
            }
        }
        $twilioMessage->prepareSms();

        $twilioService = new TwilioService();

        $twilioService->processSending($twilioMessage);

        return response('Job created', 200);
    }


    public function usersDataSearchData(Request $request)
    {
        $users = SmsTableService::processQuery($request);

        $datatables =  Datatables::of($users);

        // Global search function
//        if ($keyword = $request->get('search')['value']) {
//            // override users.name global search
////            $datatables->filterColumn('users.first_name', 'where', 'like', "$keyword%");
////            $datatables->filterColumn('users.last_name', 'where', 'like', "$keyword%");
////            $datatables->filterColumn('users.phone', 'where', 'like', "$keyword%");
////            $datatables->filterColumn('phone', function($query, $keyword) {
////                $query->where('phone', 'like', ["%{$keyword}%"]);
////            });
//
//            $datatables->filterColumn('user_id', function($query, $keyword) {
//                $query->whereRaw("users.id like ?", ["%{$keyword}%"]);
//            });
//            // override users.id global search - demo for concat
////            $datatables->filterColumn('users.id', 'where', "like", ["%$keyword%"]);
//        }

        return $datatables->make(true);
    }

}
