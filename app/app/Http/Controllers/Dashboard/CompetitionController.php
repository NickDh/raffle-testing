<?php

namespace App\Http\Controllers\Dashboard;

use App\app\Competitions\Dashboard\CompetitionsFilter;
use App\app\Competitions\Dashboard\CompetitionsTablePageGenerator;
use App\Competitions\Competition;
use App\Competitions\CompetitionDashboardPresenter;
use App\Competitions\CompetitionRepo;
use App\Competitions\CompetitionService;
use App\Competitions\Dashboard\FindWinnerPageGenerator;
use App\Competitions\Dashboard\FinishedCompetitionTablePageGenerator;
use App\Ecommerce\Cart\CartStorageRepo;
use App\Ecommerce\CompetitionEntries\CompetitionEntriesRepo;
use App\Ecommerce\CompetitionEntries\Dashboard\CompetitionEntriesFilter;
use App\Ecommerce\CompetitionEntries\Dashboard\CompetitionEntriesTablePageGenerator;
use App\Enums\CompetitionStatusEnum;
use App\Enums\CompetitionTypeEnum;
use App\Enums\TicketStatusEnum;
use App\Questions\QuestionRepo;
use App\Services\ImagePathManager;
use App\Services\ImagesPrepare;
use App\Tickets\TicketRepo;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Webmagic\Core\Controllers\AjaxRedirectTrait;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;
use Webmagic\Dashboard\Components\FormPageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Dashboard\Elements\Tables\Observers\ManualSorting\ManualSortingObserver;
use Webmagic\Dashboard\Elements\Tables\Table;

class CompetitionController
{
    use AjaxRedirectTrait;

    /**
     * @param CompetitionRepo                $competitionRepo
     * @param CompetitionsTablePageGenerator $pageGenerator
     * @param Request                        $request
     * @param CompetitionsFilter             $filter
     *
     * @return mixed|Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function index(
        CompetitionRepo $competitionRepo,
        CompetitionsTablePageGenerator $pageGenerator,
        Request $request,
        CompetitionsFilter $filter
    ) {
        $perPage = 25;

        if($request->ajax()){
            $filter->loadParams();
        } else {
            $filter->initFromSession();
        }

        $competitions = $competitionRepo->getUnfinishedCompetitions($perPage, $filter);

        $pageGenerator->prepare($competitions, $filter);

        if ($request->ajax()) {
            return $pageGenerator->getTable();
        }

        return $pageGenerator;
    }

    /**
     * @param CompetitionRepo                      $competitionRepo
     * @param FinishedCompetitionTablePageGenerator $pageGenerator
     * @param Request                              $request
     * @param CompetitionsFilter                   $filter
     *
     * @return FinishedCompetitionTablePageGenerator|Table
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function finishedCompetitions(
        CompetitionRepo $competitionRepo,
        FinishedCompetitionTablePageGenerator $pageGenerator,
        Request $request,
        CompetitionsFilter $filter
    )
    {
        $perPage = 25;

        if($request->ajax()){
            $filter->loadParams();
        } else {
            $filter->initFromSession();
        }

        $competitions = $competitionRepo->getFinishedCompetitions($perPage, $filter);

        $pageGenerator->prepare($competitions, $filter);

        if ($request->ajax()) {
            return $pageGenerator->getTable();
        }

        return $pageGenerator;
    }

    /**
     * Update position
     *
     * @param ManualSortingObserver $manualSorter
     * @param CompetitionRepo $competitionRepo
     * @throws Exception
     */
    public function positionUpdate(ManualSortingObserver $manualSorter, CompetitionRepo $competitionRepo)
    {
        $competition = $this->getCompetitionOrAbort($manualSorter->itemId());

        if (!$referenceCompetition = $competitionRepo->getByID($manualSorter->referenceItemId())) {
            return abort(404);
        }


        if ($manualSorter->isItemSetAfterReference()) {
            $competition->moveAfter($referenceCompetition);
        }

        if ($manualSorter->isItemSetBeforeReference()) {
            $competition->moveBefore($referenceCompetition);
        }
    }

    /**
     * @param int $item_id
     * @param $statusEnum
     * @param CompetitionRepo $competitionRepo
     * @return string|void
     * @throws Exception
     */
    public function changeStatus(int $item_id, $statusEnum, CompetitionRepo $competitionRepo)
    {
        if (! CompetitionStatusEnum::isValid($statusEnum)) {
            return abort(404);
        }

        if (! $competitionRepo->update($item_id, ['status' => $statusEnum])){
            return abort(500, 'Error on updating');
        }

        return  $this->redirect(route('dashboard::competitions.index'));
    }

    /**
     * @param CompetitionDashboardPresenter $dashboardPresenter
     * @param QuestionRepo $questionRepo
     * @return mixed
     * @throws Exception
     */
    public function create(CompetitionDashboardPresenter $dashboardPresenter, QuestionRepo $questionRepo)
    {
        $questions = $questionRepo->getForSelect('question', 'id');

        return $dashboardPresenter->getCreateForm($questions);
    }

    /**
     * @param Request $request
     * @param CompetitionRepo $competitionRepo
     * @param ImagesPrepare $imagesPrepare
     * @param CompetitionService $service
     * @return ResponseFactory|JsonResponse|RedirectResponse|Response|Redirector
     * @throws Exception
     */
    public function store(Request $request, CompetitionRepo $competitionRepo, ImagesPrepare $imagesPrepare, CompetitionService $service)
    {
        $request->validate([
            'main_image' => 'required|image:jpeg,png',
            'slug' => 'required|unique:competitions,slug'
        ]);

        $data = $imagesPrepare->saveFiles(
            $request->all(),
            ['main_image'],
            (new ImagePathManager())->publicPathForCompetitionsImages()
        );

        if (CompetitionTypeEnum::LETTERS()->getKey() == $data['competition_type']){
            $key = array_search($data['letters_to'], range('A', 'Z'));
            $index = ++$key;
            $data['tickets_count'] = $index * $data['tickets_sort_by'];
        }

        if ($request->get('photos')){
            $data['photos'] = implode(';', $request->get('photos'));
        }

        $first_competition = $competitionRepo->getFirstByPosition();

        if (!$competition = $competitionRepo->create($data)) {
            return response('Error on creating', 500);
        }

        if ($first_competition){
            $competition->moveBefore($first_competition);
        }

        $service->addJobTorFinishCompetition($competition);

        return $this->redirect(route('dashboard::competitions.index'));
    }

    /**
     * @param int $item_id
     * @param CompetitionRepo $competitionRepo
     * @param CompetitionDashboardPresenter $dashboardPresenter
     * @param QuestionRepo $questionRepo
     * @return mixed|void
     * @throws Exception
     */
    public function edit(int $item_id, CompetitionRepo $competitionRepo, CompetitionDashboardPresenter $dashboardPresenter, QuestionRepo $questionRepo)
    {
        $competition = $this->getCompetitionOrAbort($item_id);

        $questions = $questionRepo->getForSelect('question', 'id');
        $statuses = array_combine(CompetitionStatusEnum::values(), CompetitionStatusEnum::values());

        return $dashboardPresenter->getEditForm($competition, $questions, $statuses);
    }

    /**
     * @param int $item_id
     * @param CompetitionRepo $competitionRepo
     * @param Request $request
     * @param ImagesPrepare $imagesPrepare
     * @param CompetitionService $service
     * @return ResponseFactory|Response|void
     * @throws Exception
     */
    public function update(
        int $item_id,
        CompetitionRepo $competitionRepo,
        Request $request,
        ImagesPrepare $imagesPrepare,
        CompetitionService $service
    ){
        $request->validate([
            'main_image' => 'image:jpeg,png',
            'slug' => 'required|unique:competitions,slug,'.$item_id
        ]);

        $competition = $this->getCompetitionOrAbort($item_id);

        $data = $imagesPrepare->saveFiles(
            $request->all(),
            ['main_image'],
            (new ImagePathManager())->publicPathForCompetitionsImages()
        );

        if(isset($data['main_image']) && $data['main_image'] != $competition->main_image){
            $imagesPrepare->deleteLocalFile($competition->present()->getLocalPath($competition->main_image));
        }

        $data['auto_extend'] = $request->get('auto_extend', false);

        if ($request->get('photos')){
            $data['photos'] = implode(';', $request->get('photos'));
        }

        if (!$competitionRepo->update($item_id, $data)) {
            return response('Error on updating', 500);
        }

        if (!$updatedCompetition = $competitionRepo->getByID($item_id)) {
            return abort(404);
        }

        $service->addJobTorFinishCompetition($updatedCompetition);
    }

    /**
     * @param int $item_id
     * @param CompetitionRepo $competitionRepo
     * @return JsonResponse|RedirectResponse|Redirector
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     * @throws Exception
     */
    public function destroy(int $item_id, CompetitionRepo $competitionRepo)
    {
        $this->getCompetitionOrAbort($item_id);

        if (!$competitionRepo->destroy($item_id)) {
            abort(500, 'Error on destroying');
        }

        return $this->redirect(route('dashboard::competitions.index'));
    }

    /**
     * @param int                                  $item_id
     * @param FindWinnerPageGenerator              $pageGenerator
     *
     * @param CompetitionEntriesRepo               $competitionEntriesRepo
     *
     * @param CompetitionEntriesTablePageGenerator $entriesTablePageGenerator
     *
     * @param CompetitionEntriesFilter             $filter
     *
     * @return FormPageGenerator
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function findWinnerPage(
        int $item_id,
        FindWinnerPageGenerator $pageGenerator,
        CompetitionEntriesRepo $competitionEntriesRepo,
        CompetitionEntriesTablePageGenerator $entriesTablePageGenerator,
        CompetitionEntriesFilter $filter
    ) {
        $competition = $this->getCompetitionOrAbort($item_id);
        $pageGenerator->prepare($competition);

        // Initiate from session to save previous state
        $filter->initiateFromSession();

        $perPage = 25;
        $entries = $competitionEntriesRepo->getByCompetitionId($competition->id, $perPage, $filter);
        $entriesTablePageGenerator->prepare($entries, $competition->id, $filter);

        $pageGenerator->addEntriesTable($entriesTablePageGenerator);

        return $pageGenerator;
    }

    /**
     * @param int                           $item_id
     * @param Request                       $request
     * @param CompetitionDashboardPresenter $dashboardPresenter
     *
     * @return mixed|string
     * @throws Exception
     */
    public function findWinner
    (
        int $item_id,
        Request $request,
        CompetitionDashboardPresenter $dashboardPresenter
    ){
        $validData = $request->validate([
            'number' => 'required'
        ]);

        $competition = $this->getCompetitionOrAbort($item_id);

        $user = null;
        if ($ticket = $competition->findSoldTicket($validData['number'])){
            $user = $ticket->user;
        }

        return $dashboardPresenter->getUserDescriptionList($user);
    }

    /**
     * @param int $competitionId
     *
     * @return Model|null|Competition
     * @throws Exception
     */
    protected function getCompetitionOrAbort(int $competitionId)
    {
        if (!$competition = (new CompetitionRepo())->getByID($competitionId)) {
            abort(404);
        }

        return $competition;
    }

    /**
     * @param int                           $item_id
     * @param Request                       $request
     * @param CompetitionRepo               $competitionRepo
     * @param CompetitionDashboardPresenter $dashboardPresenter
     * @param Dashboard                     $dashboard
     * @param TicketRepo                    $ticketRepo
     *
     * @return mixed|Dashboard
     * @throws Exception
     */
    public function show
    (
        int $item_id,
        Request $request,
        CompetitionRepo $competitionRepo,
        CompetitionDashboardPresenter $dashboardPresenter,
        Dashboard $dashboard,
        TicketRepo $ticketRepo
    ) {

        if (! $competition = $competitionRepo->getByID($item_id)) {
            abort(404);
        }

        $tickets  = $ticketRepo->getByFilter(
            $request->get('keyword', ''),
            $request->get('items', 10),
            $request->get('page', 1),
            $competition->id
        );


        return $dashboardPresenter->getTicketsTablePage($competition, $tickets, $dashboard, $request);
    }

    /**
     * @param int $item_id
     * @param TicketRepo $ticketRepo
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function changeTicketStatusToLive(int $item_id, TicketRepo $ticketRepo)
    {
        if (! $ticket = $ticketRepo->getByID($item_id)) {
            abort(404);
        }

        if (! TicketStatusEnum::HELD()->is($ticket->status)) {
            abort(500, 'Unable to change status for this ticket');
        }

        $ticketRepo->destroy($item_id);
    }

    public function getCompetitionJson2(CompetitionRepo $competitionRepo){
        $result = new Collection(['results' => $competitionRepo->getAll()->pluck('title','id',),'pagination'=>['more'=>false]]);
        return $result->toJson();
    }

    public function getCompetitionJson(Request $request){

        $search = $request->search;

        if($search == ''){
            $employees = Competition::orderby('title','asc')->select('id','title')->limit(5)->get();
        }else{
            $employees = Competition::orderby('title','asc')->select('id','title')->where('title', 'like', '%' .$search . '%')->limit(5)->get();
        }

        $response = array();
        foreach($employees as $employee){
            $response[] = array(
                "id"=>$employee->id,
                "text"=>$employee->title
            );
        }

        echo json_encode($response);
        exit;
    }
}
