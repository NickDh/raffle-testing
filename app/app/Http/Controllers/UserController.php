<?php

namespace App\Http\Controllers;


use App\Rules\CheckOldPassword;
use App\Users\UserRepo;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * @return Factory|View
     */
    public function getMainPage()
    {
        $user = Auth::user();
        return view('user', compact('user'));
    }

    /**
     * @param UserRepo $userRepo
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function updateUserInfo(UserRepo $userRepo, Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'first_name'   => 'required',
            'last_name'    => 'required',
            'email'        => 'required|email|unique:users,email,' . $user->id,
            'phone'        => 'required',
            'address'      => 'required',
            'town'         => 'required',
            'post_code'    => 'required',
            'subscription' => ['sometimes', 'in:on,off'],
        ]);
        if (!$validator->passes()) {
            return response()->json(['error'=>$validator->errors()->all()], 422);
        }

        $validData = $validator->validated();
        $validData['subscription'] = $request->get('subscription', 'off') === 'on' ? 1 : 0;

        if (!$userRepo->update($user->getKey(), $validData)) {
            return response()->json(['message' => 'Error'], 500);
        }

        return response()->json(['message' => 'Successfully saved'], 200);
    }

    /**
     * Update current user password
     *
     * @param Request $request
     */
    public function updatePassword(Request $request)
    {
        $user = Auth::user();

        $validData = $request->validate([
            'current_password' => ['required', new CheckOldPassword],
            'password' => 'required|confirmed|min:6',
        ]);

        $user->password = $validData['password'];
        $user->save();
    }
}
