<?php

namespace App\Http\Controllers;

use App\Ecommerce\Orders\Exceptions\CartOrTickersDeletedException;
use App\Ecommerce\Orders\Exceptions\NoUsablePaymentMethodException;
use App\Ecommerce\Orders\Exceptions\NumberAreTakenException;
use App\Ecommerce\Orders\OrderProcessor;
use App\Ecommerce\Orders\OrderRepo;
use App\Ecommerce\Orders\OrderService;
use App\Ecommerce\Payment\FirstData\Exceptions\FirstDataGatewayAuthErrorException;
use App\Ecommerce\Payment\FirstData\PaymentMethod\PaymentMethodService;
use App\Ecommerce\Payment\PaymentProcessor;
use App\Payment\Core\PaymentGatewayContract;
use App\Services\GoogleTagManager;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Throwable;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;

class OrderController extends CartController
{
    /**
     * Return checkout page
     *
     * @param  PaymentGatewayContract  $paymentGateway
     *
     * @param  Request  $request
     * @param  GoogleTagManager  $googleTagManager
     * @return string
     * @throws Exception
     */
    public function checkout(PaymentGatewayContract $paymentGateway, Request $request, GoogleTagManager $googleTagManager)
    {
        $useSavedCard = (bool)$request->get('use_saved_card', false);

        $googleTagManager->checkoutInitiated();

        return $paymentGateway->getCheckoutPage($this->prepareCart(Auth::id()), Auth::user(), $useSavedCard, route('order.create'));
    }

    /**
     * Process checkout and create new order
     *
     * @param Request        $request
     * @param OrderProcessor $orderProcessor
     *
     * @return Application|JsonResponse|RedirectResponse|Redirector
     * @throws Throwable
     */
    public function create(
        Request $request,
        OrderProcessor $orderProcessor
    ) {
        $request->validate([
           'agree' => ['required', 'in:on']
        ]);

        $user = Auth::user();
        $cart = $this->prepareCart();

        // Place new order and clear cart
        try {
            $order = $orderProcessor->placeNewOrderAndClearCart($user, $cart);
        } catch (NoUsablePaymentMethodException $exception){
            abort(500, $exception->getMessage());
        } catch (CartOrTickersDeletedException $exception){
            return response()->json(['error' => $exception->getMessage()], 500);
        } catch (NumberAreTakenException $exception){
            return response()->json(['error' => $exception->getMessage()], 500);
        }

        // Process free orders
        if(!$order->shouldBePaid()){
            $order = $orderProcessor->processFreeOrder($order);
            $redirectUrl = route('order.payment-result', ['order_id' => $order->id]);

            if(\request()->ajax()){
                return response()->json(['redirect' => $redirectUrl]);
            }

            return redirect($redirectUrl);
        }

        return response()->json(['orderId' => $order->id]);
    }

    /**
     * Show details order page if user is owner or admin
     *
     * @param int $order_id
     * @param OrderRepo $orderRepo
     * @return Application|Factory|View
     * @throws Exception
     */
    public function detailsPage(int $order_id, OrderRepo $orderRepo)
    {
        if (! $order = $orderRepo->getByID($order_id)) {
            abort(404);
        }

        $user = Auth::user();
        if ($order->user_id != $user->id && ! $user->isAdmin()) {
            abort(404);
        }

        return view('order-details', compact('order'));
    }

    /**
     * @param int $order_id
     * @param OrderRepo $orderRepo
     * @return Application|Factory|View
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function delete(int $order_id, OrderRepo $orderRepo)
    {
        if (! $order = $orderRepo->getByID($order_id)) {
            abort(404);
        }

        $user = Auth::user();
        if ($order->user_id != $user->id && ! $user->isAdmin()) {
            abort(404);
        }

        $orderRepo->destroy($order_id);

        return view('_user-orders-table', compact('user'));
    }

    /**
     * @param OrderRepo $orderRepo
     * @param Request   $request
     *
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index(OrderRepo $orderRepo, Request $request)
    {
        $perPage = 10;
        $orders = $orderRepo->getByUserID(auth()->user()->id, $perPage);

        if($request->ajax()){
            return view('parts/_orders-table', compact('orders'));
        }

        return view('orders', compact('orders'));
    }

    /**
     * @param string    $orderId
     * @param OrderRepo $orderRepo
     *
     * @return Application|Factory|View
     * @throws Exception
     */
    public function paymentResult(string $orderId, OrderRepo $orderRepo)
    {
        if(!$order = $orderRepo->getByID($orderId)){
            abort(404);
        }

        return view('payment-result', compact('order'));
    }
}

