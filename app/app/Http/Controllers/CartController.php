<?php

namespace App\Http\Controllers;


use App\Competitions\CompetitionRepo;
use App\Ecommerce\Cart\SessionCart as Cart;
use App\Ecommerce\Orders\OrderRepo;
use App\Enums\TicketStatusEnum;
use App\Payment\Core\PaymentGatewayContract;
use App\PromoCodes\PromoCodeService;
use App\Questions\Answer;
use App\Questions\AnswerRepo;
use App\Services\GoogleTagManager;
use App\Settings\SettingsRepo;
use App\Tickets\Ticket;
use App\Tickets\TicketRepo;
use App\Tickets\TicketService;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;

class CartController extends Controller
{
    protected $cart;

    /**
     * CartController constructor.
     *
     * @param Cart $cart
     */
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * Prepare session cart for user
     *
     * @param null $user_id
     *
     * @return Cart
     */
    protected function prepareCart($user_id = null)
    {
        if (is_null($user_id)) {
            $user_id = Auth::user()->id;
        }

        $this->cart->key($user_id);

        return $this->cart;
    }

    /**
     * Return cart page
     *
     * @param PaymentGatewayContract $paymentGateway
     *
     * @return Factory|View
     */
    public function getCartPage(PaymentGatewayContract $paymentGateway)
    {
        if (!Auth::check()) {
            return view('cart');
        }

        $cart = $this->prepareCart();
        $isSavedCardExists = $paymentGateway->isSavedCardExists();
        $savedCard4digits = $paymentGateway->getSavedCardLast4Digits();

        return view('cart', compact('cart', 'isSavedCardExists', 'savedCard4digits'));
    }

    /**
     * No tickets processing method
     *
     * @param Request         $request
     * @param CompetitionRepo $competitionRepo
     * @param TicketService   $ticketService
     *
     * @return Application|ResponseFactory|JsonResponse|Response
     * @throws Exception
     */
    public function addRandomItems(Request $request, CompetitionRepo $competitionRepo, TicketService $ticketService)
    {
        // Check of feature activated
        if(!config('project.without_tickets')){
            abort(401, 'Unauthorized');
        }

        $validData = $request->validate([
            'tickets_count'  => ['required', 'integer'],
            'competition_id' => ['required', 'exists:competitions,id'],
            'answer_id'      => ['required', 'integer'],
        ]);

        if (!$competition = $competitionRepo->getByID($validData['competition_id'])) {
            abort(404);
        }

        // If user required to many tickets
        $availableToBuy = Auth::user()->availableTicketsToBuy($competition->max_tickets_per_user, $competition->id);
        if($availableToBuy <= 0){
            return \response()->json(['message' => 'Sorry, you have selected or exceeded the maximum tickets for this competition'], 500);
        } elseif ($availableToBuy < $validData['tickets_count']) {
            return $this->tryLessTicketsResponse();
        }

        // Get random tickets
        try {
            $preparedTickets = $ticketService->prepareGroupedTicketsForCompetition($competition, $validData['tickets_count']);
        } catch (Exception $exception){
            return $this->tryLessTicketsResponse();
        }

        $tickets = $preparedTickets['picked'];

        // Add tickets to cart
        $cart = $this->prepareCart();
        $answer = (new AnswerRepo())->getByID($validData['answer_id']);
        $ticketRepo = new TicketRepo();
        // FIX | Fix for be sure that all tickets saved without no issues (Using transaction)
        \DB::beginTransaction();
        try {
            foreach ($tickets as $ticket_number) {
                $ticket = $ticketRepo->create([
                    'number'         => $ticket_number,
                    'user_id'        => Auth::user()->id,
                    'competition_id' => $competition->id,
                    'status'         => TicketStatusEnum::HELD,
                ]);

                $cart->add($ticket, $answer);
            }
        } catch (Exception $e) {
            \DB::rollBack();
            // Adding tickets issued because of coalition or because of taken tickets
            return $this->tryLessTicketsResponse();
        }

        \DB::commit();

        return \response('Tickets added');
    }

    /**
     * @return JsonResponse
     */
    public function getData()
    {
        $cart = $this->prepareCart();

        return \response()->json($cart->getContent()->toArray());
    }

    /**
     * @return JsonResponse
     */
    protected function tryLessTicketsResponse()
    {
        return \response()->json(['message' => 'Please, choose less tickets to buy'], 500);
    }

    /**
     * Add items to cart
     *
     * @param  Request  $request
     * @param  TicketRepo  $ticketRepo
     * @param  CompetitionRepo  $competitionRepo
     * @param  GoogleTagManager  $googleTagManager
     * @return JsonResponse
     * @throws Exception
     */
    public function addItems(Request $request, TicketRepo $ticketRepo, CompetitionRepo $competitionRepo, GoogleTagManager $googleTagManager)
    {
        $validData = $request->validate([
            'tickets'        => ['required', 'array'],
            'competition_id' => ['required', 'exists:competitions,id'],
            'answer_id'      => ['required', 'int'],
        ]);

        $competitionId = $validData['competition_id'];

        // Check if tickets already created
        $taken_tickets = $ticketRepo->getByNumber($validData['tickets'], $competitionId);
        if ($taken_tickets->count()) {
            return $this->ticketsAlreadyTakenResponse($taken_tickets);
        }

        // Check if tickets taken in any active orders
        $ticketsForChecking = [];
        foreach ($request->tickets as $number) {
            $ticketsForChecking[] = new Ticket(['number' => $number, 'competitionId' => $competitionId]);
        }

        $isNumbersTakenInActiveOrders = (new OrderRepo())->isNumbersTaken(null, ... $ticketsForChecking);
        if ($isNumbersTakenInActiveOrders) {
            return $this->ticketsAlreadyTakenResponse($taken_tickets);
        }

        $competition = $competitionRepo->getByID($competitionId);

        // Add tickets to cart
        $cart = $this->prepareCart();
        $answer = (new AnswerRepo())->getByID($validData['answer_id']);
        // FIX | Fix for be sure that all tickets saved without no issues (Using transaction)
        \DB::beginTransaction();
        try {
            foreach ($validData['tickets'] as $ticket_number) {
                $ticket = $ticketRepo->create([
                    'number'         => $ticket_number,
                    'user_id'        => Auth::user()->id,
                    'competition_id' => $competition->id,
                    'status'         => TicketStatusEnum::HELD,
                ]);

                $cart->add($ticket, $answer);
            }
        } catch (Exception $e) {
            \DB::rollBack();
            return $this->ticketsAlreadyTakenResponse($taken_tickets);
        }

        \DB::commit();

        $googleTagManager->itemAddedToCart($ticket, count($request->tickets));
    }

    /**
     * Generate response for error when tickets already taken
     *
     * @param Collection $taken_tickets
     *
     * @return JsonResponse
     */
    protected function ticketsAlreadyTakenResponse(Collection $taken_tickets)
    {
        return response()->json([
            'message' => 'Tickets has already taken: ' . implode(', ', $taken_tickets->pluck('number')->toArray()),
        ], 500);
    }

    /**
     * Delete all tickets from cart by competition id
     *
     * @param            $item_id
     * @param TicketRepo $ticketRepo
     *
     * @return Application|Factory|View
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function deleteItems($item_id, TicketRepo $ticketRepo)
    {
        $cart = $this->prepareCart();

        $competition = $cart->getItem($item_id);
        $tickets = explode(';', $competition['ticket_id']);
        $ticketRepo->destroy($tickets);

        $cart->remove($item_id);

        return view('parts._cart-total', compact('cart'));
    }


    /**
     * Check promo code and add to cart if valid
     *
     * @param Request                $request
     * @param PromoCodeService       $promoCodeService
     * @param PaymentGatewayContract $paymentGateway
     *
     * @return ResponseFactory|Factory|Response|View
     * @throws Exception
     */
    public function checkCouponCode(
        Request $request,
        PromoCodeService $promoCodeService,
        PaymentGatewayContract $paymentGateway
    ) {
        $request->validate([
            'name' => 'required',
        ]);

        $promoCode = $promoCodeService->getIfValid($request->name, Auth::user()->id);

        if (!$promoCode) {
            return response('Coupon is not valid', 500);
        }

        $cart = $this->prepareCart();
        $cart->addDiscount($promoCode);

        $isSavedCardExists = $paymentGateway->isSavedCardExists();
        $savedCard4digits = $paymentGateway->getSavedCardLast4Digits();

        return view('parts._cart-total', compact('cart', 'isSavedCardExists', 'savedCard4digits'));
    }


    /**
     * Prepare view with render tickets and hold random tickets by amount
     *
     * @param Request         $request
     * @param TicketService   $ticketService
     * @param CompetitionRepo $competitionRepo
     *
     * @return Application|Factory|JsonResponse|View
     * @throws Exception
     */
    public function getRandomTickets(Request $request, TicketService $ticketService, CompetitionRepo $competitionRepo)
    {
        $request->validate([
            'competition_id' => 'required',
            'amount'         => 'required',
        ]);

        if (!$competition = $competitionRepo->getByID($request->competition_id)) {
            abort(404);
        }

        try {
            $competition->grouped_tickets = $ticketService->prepareGroupedTicketsForCompetition($competition,
                $request->amount);
        } catch (Exception $exception) {
            return response()->json([
                'message' => 'We were unable to randomly select that many numbers due them being sold out or already reserved, please try choosing a lower amount.',
            ], 500);
        }

        $maxRandomValues = (new SettingsRepo())->getSettings()->random_generator_value;
        if ($maxRandomValues > $competition->max_tickets_per_user) {
            $maxRandomValues = $competition->max_tickets_per_user;
        }

        return view('parts._tickets', compact('competition', 'maxRandomValues'));
    }
}
