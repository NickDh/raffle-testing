<?php


namespace App\Http\ViewComposers;


use App\Pages\Page;
use App\Pages\PagesRepo;
use Illuminate\View\View;

class FooterPagesViewComposer
{
    /**
     * @param View $view
     *
     * @throws \Exception
     */
    public function compose(View $view)
    {
        $footerLinkPages = (new PagesRepo())->getPagesForFooterLinks();

        $footerLinkPages = $footerLinkPages->groupBy(function (Page $page){
            return $page->footer_column;
        });

        $view->with(compact('footerLinkPages'));
    }
}