<?php


namespace App\Providers;


use App\Payment\Core\PaymentGatewayContract;
use Exception;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Throwable;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     * @throws Throwable
     */
    public function register()
    {
        // Register concrete payment gateway class
        $this->app->bind(PaymentGatewayContract::class, $this->getDefinedPaymentGateway());
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Register routes
        app(PaymentGatewayContract::class)->registerRoutes();
    }

    /**
     * Check end return concrete payment gateway class
     *
     * @return Repository|Application|mixed
     * @throws Throwable
     */
    protected function getDefinedPaymentGateway()
    {
        $definedAlias = config('payment.payment_gateway');

        $definedClass = config("payment.gateways.$definedAlias.gateway_class");

        throw_if(!$definedClass, 'Payment gateway with name '.$definedAlias.' not defined or gatewayClass option is not defined. Please, check payment.php config');
        throw_if(! app($definedClass) instanceof PaymentGatewayContract, "$definedClass should implements \App\Payment\Core\PaymentGatewayContract");

        return $definedClass;
    }
}