<?php

namespace App\Providers;

 use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Schema::defaultStringLength(191);
        $this->bodyClassShare();

        Validator::extend('recaptcha', 'App\\Rules\\CheckReCaptcha@passes');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerServices();
        $this->registerViewComposers();
    }

    /**
     * Prepare and share body class
     */
    protected function bodyClassShare()
    {
        $request = request();

        if($request->path() === '/'){
            $body_class = 'page-index';
        } else {
            $body_class = 'page-' . str_replace('/', '-', $request->path());
        }

        view()->share('body_class', $body_class);
    }

    /**
     * Registering app services
     */
    public function registerServices()
    {
        /**
         * Conditionally Loading Service Providers
         *
         * local - load all tech service providers
         * dev-server - load iseed and migrate generator service providers
         */
        if($this->app->environment() === 'local'){
            $this->app->register('Barryvdh\Debugbar\ServiceProvider');
            $this->app->register(DuskServiceProvider::class);
        }

        if($this->app->environment() === 'local' || $this->app->environment() === 'dev-server'){
            $this->app->register('Orangehill\Iseed\IseedServiceProvider');
        }
    }


    /**
     * App View Composers registration
     */
    protected function registerViewComposers()
    {

        View::composer(['parts._footer',], '\App\Http\ViewComposers\FooterPagesViewComposer');
        View::composer(['parts._header',], '\App\Http\ViewComposers\HeaderPagesViewComposer');

        View::composer(['parts._header', 'parts._banner-bottom', 'checkout'], '\App\Http\ViewComposers\CartViewComposer');
        View::composer(['parts._counters'], '\App\Http\ViewComposers\CountersViewComposer');
        View::composer(
            [
                'parts._head-all',
                'parts._body-all',
                'parts._body-checkout',
                'parts._body-successful-order',
                'parts._body-successful-order',
            ], '\App\Http\ViewComposers\AnalyticsViewComposer');
    }
}
