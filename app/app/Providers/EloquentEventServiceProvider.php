<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use App\Users\User;
use NZTim\Mailchimp\Mailchimp;

class EloquentEventServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function sendMc ($user)
    {
        //$mckey = DB::table('marketing_settings')->select(['value'])->where('key','MC_KEY')->first()->value;
        //$mcaudienceid = DB::table('marketing_settings')->select(['value'])->where('key','MC_AUDIENCE_ID')->first()->value;

        $mckey = marketingSetting('MC_KEY');
        $mcaudienceid = marketingSetting('MC_AUDIENCE_ID');


        if($mckey && $mcaudienceid) {
            $mc = new Mailchimp($mckey);
            if($user->subscription) {
                $merge = [
                    'FNAME' => $user->first_name,
                    'LNAME' => $user->last_name,
                ];
                Log::info('Subscribed');
                $mc->subscribe($mcaudienceid, $user->email, $merge, false);
            } else {
                Log::info('Unsubscribed');
                $mc->unsubscribe($mcaudienceid, $user->email);
            }
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        User::updated(function ($user) {
            $this->sendMc($user);
        });

        User::created(function ($user) {
            $this->sendMc($user);
        });
    }
}
