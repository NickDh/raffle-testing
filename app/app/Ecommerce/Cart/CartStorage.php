<?php

namespace App\Ecommerce\Cart;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Ecommerce\Cart\CartStorage
 *
 * @property int $id
 * @property string $cart_data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CartStorage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CartStorage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CartStorage query()
 * @method static \Illuminate\Database\Eloquent\Builder|CartStorage whereCartData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CartStorage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CartStorage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CartStorage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CartStorage extends Model
{
    protected $table = 'cart_storage';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'cart_data',
    ];

    public function setCartDataAttribute($value)
    {
        $this->attributes['cart_data'] = serialize($value);
    }

    public function getCartDataAttribute($value)
    {
        return unserialize($value);
    }
}