<?php

namespace App\Ecommerce\Cart;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


class CartDiscountResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Model $promoCode
     * @return Collection
     */
    public static function collect(Model $promoCode)
    {
        return collect([
            'id'      => $promoCode->id,
            'type'    => $promoCode->type,
            'value'   => $promoCode->value,
            'code'    => $promoCode->code,
        ]);
    }
}