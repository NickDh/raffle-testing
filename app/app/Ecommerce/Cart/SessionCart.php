<?php

namespace App\Ecommerce\Cart;

use App\Enums\PromoCodeTypesEnum;
use App\Jobs\ClearCartJob;
use App\Questions\Answer;
use App\Settings\SettingsRepo;
use App\Tickets\Ticket;
use App\Tickets\TicketRepo;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Model;
use App\Ecommerce\Cart\CartStorageRepo as Session;
use Illuminate\Support\Collection;
use Laracasts\Presenter\Exceptions\PresenterException;

class SessionCart
{
    /**
     * the item storage
     *
     * @var
     */
    public $storage;

    /**
     * @var string
     */
    private $storageKey;

    /**
     * @var string
     */
    private $storageKeyCartItems;
    /**
     * @var string
     */
    private $storageKeyCartDiscount;
    /**
     * @var int
     */
    private $cartLifeTime;


    /**
     * Cart constructor.
     *
     * @param Session $storage
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function __construct(Session $storage)
    {
        $settingsRepo = app()->make(SettingsRepo::class);

        $this->storage = $storage;
        $this->cartLifeTime = $settingsRepo->getCartLifetime();
    }

    /**
     * Add special key prefix for different storage user
     *
     * @param $key
     * @return $this
     */
    public function key($key)
    {
        $this->storageKey = $key;
        $this->storageKeyCartItems = $this->storageKey . '_cart_items';
        $this->storageKeyCartDiscount = $this->storageKey . '_cart_discount';

        return $this;
    }

    /**
     * Save cart in storage by key
     *
     * @param $cart
     * @throws Exception
     */
    protected function save($cart)
    {
        $this->storage->put($this->storageKeyCartItems, $cart);
    }

    /**
     * Destroy cart form storage
     *
     * @return bool
     */
    public function clear()
    {
        $this->storage->remove($this->storageKeyCartItems);
        $this->storage->remove($this->storageKeyCartDiscount);

        return true;
    }

    /**
     * Get the cart items
     *
     * @return Collection
     */
    public function getContent()
    {
        return (new Collection($this->storage->get($this->storageKeyCartItems)));
    }


    /**
     * Add item to cart or update if it already exist
     *
     * @param Model  $ticket
     * @param Answer $answer
     *
     * @return bool|int|string
     * @throws Exception
     */
    public function add(Model $ticket, Answer $answer)
    {
        if(! $this->getItem($ticket->competition_id)){
            $this->addItem($ticket, $answer);
        } else {
            $this->updateItem($ticket, $answer);
        }

        $this->addJobForCartClear();
    }

    /**
     * Add new job for clear cart after some time
     */
    protected function addJobForCartClear()
    {
        dispatch(new ClearCartJob($this->storageKeyCartItems, $this->storageKeyCartDiscount, $this->cartLifeTime))
            ->delay(Carbon::now()->addMinutes($this->cartLifeTime))
            ->onQueue('clear-cart');
    }


    /**
     * Get cart item by unique id
     *
     * @param $cart_item_id
     * @return mixed
     */
    public function getItem($cart_item_id)
    {
        return $this->getContent()->get($cart_item_id);
    }


    /**
     * Add item to cart with id like competition id
     *
     * @param Ticket $ticket
     * @param Answer $answer
     *
     * @return string
     * @throws PresenterException
     */
    public function addItem(Ticket $ticket, Answer $answer)
    {
        $cart = $this->getContent();

        $cart->put($ticket->competition_id, CartItemResource::collect($ticket, $answer));

        $this->save($cart);
    }

    /**
     * Update item price and ticket numbers value
     *
     * @param Model  $ticket
     * @param Answer $answer
     *
     * @throws Exception
     */
    public function updateItem(Model $ticket, Answer $answer)
    {
        $cart = $this->getContent();

        $cartItem = $this->getItem($ticket->competition_id);
        $cartItem['ticket_number'] .= ', '.$ticket->number;
        $cartItem['ticket_id'] .= ';'.$ticket->id;
        $cartItem['price'] += $ticket->competition->present()->actual_price;
        $cartItem['qty'] += 1;
        $cartItem['answer'] = $answer->present()->stringAnswer();
        $cartItem['answer_id'] = $answer->id;
        $cartItem['is_answer_correct'] = $answer->is_correct;

        $cart->put($ticket->competition_id, $cartItem);

        $this->save($cart);
    }

    /**
     * Remove cart item by id
     * and update cart in storage
     *
     * @param $cart_item_id
     *
     * @throws Exception
     */
    public function remove($cart_item_id)
    {
        $cart = $this->getContent();
        $cart->forget($cart_item_id);
        $this->save($cart);

        if($this->getItemsQuantity() == 0){
            $this->clear();
        }
    }

    /**
     * Save promo code for cart in storage
     *
     * @param Model $promoCode
     */
    public function addDiscount(Model $promoCode)
    {
        $this->storage->put($this->storageKeyCartDiscount, CartDiscountResource::collect($promoCode));
    }

    /**
     * Return cart promo
     *
     * @return Collection
     */
    public function getDiscount()
    {
        return new Collection($this->storage->get($this->storageKeyCartDiscount));
    }

    /**
     * Return promo code if exist
     *
     * @return mixed|string
     */
    public function getDiscountCode()
    {
        return $this->getDiscount()['code'] ?? '';
    }

    /**
     * Count difference between total nad subtotal
     *
     * @return string
     */
    public function getDiscountValue()
    {
        return formatValue($this->getSubTotal() - $this->getTotal());
    }

    /**
     * Count cart subtotal
     *
     * @return string
     */
    public function getSubTotal()
    {
        $cart = $this->getContent();

        $sum = $cart->sum(function ($item) {
            return $item['price'];
        });

        return formatValue($sum);
    }

    /**
     * Count total with discount if exist
     *
     * @return string
     */
    public function getTotal()
    {
        $subTotal = $this->getSubTotal();

        $total = $subTotal;

        $discount = $this->getDiscount();
        if($discount->isNotEmpty()){
            if(PromoCodeTypesEnum::PERCENT()->is($discount['type'])){

                $discountValue = $subTotal * ($discount['value'] / 100);
                $total = floatval($subTotal - $discountValue);

            } else{
                $total = floatval($subTotal - $discount['value']);
            }

            if($total < 0){
                $total = 0;
            }
        }
        return formatValue($total);
    }

    /**
     * Count total items quantity in cart
     *
     * @return int
     */
    public function getItemsQuantity()
    {
        $items = $this->getContent();

        if ($items->isEmpty()){
            return 0;
        }

        $count = $items->sum(function ($item) {
            return $item['qty'];
        });

        return $count;
    }

    /**
     * @return int|null
     */
    public function getLifeTimeLeft()
    {
        return  $this->storage->getTimeLeft($this->storageKeyCartItems, $this->cartLifeTime);
    }

    /**
     * @return mixed
     */
    public function getAllTicketsIds()
    {
       $competitions = $this->getContent();
        $data = [];
       foreach ($competitions as $competition){
           $tickets = explode(';', $competition['ticket_id']);
           foreach ($tickets as $ticket){
               $data[] = (int)$ticket;
           }
       }
      return $data;
    }

    /**
     * Return all tickets from cart
     *
     * @return \Illuminate\Database\Eloquent\Collection
     * @throws Exception
     */
    public function getTickets()
    {
        $ids = $this->getAllTicketsIds();

        return (new TicketRepo())->getByIDs(...$ids);
    }
}
