<?php


namespace App\Ecommerce\Orders\Exceptions;


use Throwable;

class NoUsablePaymentMethodException extends \Exception
{
    /** @var string */
    protected $message = 'User has no usable payment method';
}