<?php


namespace App\Ecommerce\Orders\Exceptions;


class NumberAreTakenException extends \Exception
{
    /** @var string  */
    protected $message = 'One or more tickets already taken';
}