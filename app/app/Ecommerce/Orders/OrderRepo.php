<?php

namespace App\Ecommerce\Orders;

use App\Enums\OrderStatusEnum;
use App\Tickets\Ticket;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;

class OrderRepo extends EntityRepo
{
	protected $entity = Order::class;

    /**
     * Default ordering
     *
     * @param Builder $query
     *
     * @return Builder
     */
    protected function addOrdering(Builder $query): Builder
    {
        $query->orderBy('orders.created_at', 'desc');

        return parent::addOrdering($query);
    }


    /**
     * Check if tickets taken for different
     *
     * @param int|null $excludeOrderId
     * @param Ticket   ...$tickets
     *
     * @return bool
     * @throws Exception
     */
    public function isNumbersTaken(int $excludeOrderId = null, Ticket ... $tickets)
    {
        if(!count($tickets)){
            return false;
        }

        $query = $this->query()
            ->leftJoin('competition_entries', 'competition_entries.order_id', '=', 'orders.id')
            ->leftJoin('selected_tickets', 'selected_tickets.competition_entry_id', '=', 'competition_entries.id')
            ->where('competition_entries.is_answer_right', 1)
            ->whereIn('orders.status', [OrderStatusEnum::PAID, OrderStatusEnum::PROCESSING, OrderStatusEnum::COLLISION]);

        if($excludeOrderId){
            $query->where('orders.id', '<>', $excludeOrderId);
        }

        $query->where(function($firstWhere) use ($tickets) {
            $firstTicket = array_first($tickets);

            $firstWhere->where(function ($secondQuery) use ($firstTicket) {
                $secondQuery->where('selected_tickets.ticket', $firstTicket->number)
                    ->where('competition_entries.competition_id', $firstTicket->competition_id);
            });

            foreach ($tickets as $ticket){
                $firstWhere->orWhere(function ($secondQuery) use ($ticket) {
                    $secondQuery->where('selected_tickets.ticket', $ticket->number)
                        ->where('competition_entries.competition_id', $ticket->competition_id);
                });
            }
        });

        $activeOrdersWithNumbers = $this->realGetMany($query);

        return count($activeOrdersWithNumbers) > 0;
    }

    /**
     * Return orders which were not processed during max order processing time
     *
     * @return Builder[]|Collection
     * @throws Exception
     */
    public function getOldUnprocessedOrders()
    {
        $maxOrderProcessingTime = config('project.max_order_processing_time');

        return $this->query()
            ->where('status', OrderStatusEnum::PROCESSING)
            ->where('created_at', '<', now()->subMinutes($maxOrderProcessingTime))
            ->get();
    }

    /**
     * Get all with paginate and filter
     *
     * @param null $keyword
     * @param null $per_page
     * @param null $page
     * @param string $sortBy
     * @param string $sort
     * @param bool $with_users
     * @param bool $with_trashed
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws Exception
     */
    public function getByFilter(
        $keyword = null,
        $per_page = null,
        $page = null,
        $sortBy = 'created_at',
        $sort = 'desc',
        $with_users = false,
        $with_trashed = false
    ) {
        $query = $this->query();

        if ($keyword) {
            $query->orWhere('id', 'LIKE', '%'.$keyword.'%');
            $query->orWhere('customer_email', 'LIKE', '%'.$keyword.'%');
            $query->orWhere('customer_name', 'LIKE', '%'.$keyword.'%');
            $query->orWhere('created_at', 'LIKE', '%'.$keyword.'%');
        }

        if($with_users){
            $query->with('user');
        }

        if ($with_trashed){
            $query->withTrashed();
        }

        $query->orderBy($sortBy, $sort);

        if (is_null($per_page) && is_null($page)) {
            return $query->get();
        }

        return $query->paginate($per_page, ['*'], 'page', $page);
    }

    /**
     * Get entity by ID
     *
     * @param $id
     *
     * @return Model|null
     * @throws Exception
     */
    public function getByID($id, $with_trashed = false)
    {
        $query = $this->query();
        $query->where('id', $id);

        if ($with_trashed){
            $query->withTrashed();
        }

        return $this->realGetOne($query);
    }

    /**
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getAllPaidFromPastDay()
    {
        $query = $this->query();
        $query->where("created_at",">",Carbon::now()->startOfDay());
        $query->where('status', OrderStatusEnum::PAID);

        return $this->realGetMany($query);
    }

    /**
     * @param int $id
     * @return Builder[]|Collection
     * @throws Exception
     */
    public function getByCompetitionId(int $id)
    {
        $query = $this->query();

        $query->where('status', OrderStatusEnum::PAID);
        $query->where("cart_data", "like", "%\"competition_id\";i:$id;%");

        return $query->get();
    }

    /**
     * @param Builder $query
     * @param $date_from
     * @param $date_to
     * @return Builder
     */
    public function addFilterByPeriod(Builder $query, $date_from, $date_to)
    {
        if ($date_from && $date_to){
            $date_from = Carbon::parse($date_from)->toDateTimeString();
            $date_to = Carbon::parse($date_to)->toDateTimeString();

            $query->whereBetween('created_at', [$date_from, $date_to]);
        }

        return $query;
    }

    /**
     * @param $column_table
     * @param $date_from
     * @param $date_to
     * @return int|mixed
     * @throws Exception
     */
    public function countSumByPeriod($column_table, $date_from, $date_to)
    {
        $query = $this->query();
        $query = $this->paidFilter($query);

        if ($date_from && $date_to){
            $this->addFilterByPeriod($query, $date_from, $date_to);
        }

        return $query->sum($column_table);
    }

    /**
     * @param $date_from
     * @param $date_to
     * @return int
     * @throws Exception
     */
    public function paidOrdersCountByPeriod($date_from, $date_to)
    {
        $query = $this->query();
        $query = $this->paidFilter($query);

        //todo check if it works correctly with one date only
        if ($date_from && $date_to){
            $this->addFilterByPeriod($query, $date_from, $date_to);
        }

        return $query->count();
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    protected function paidFilter(Builder $query)
    {
        return $query->where('status', OrderStatusEnum::PAID());
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    protected function failedFilter(Builder $query)
    {
        return $query->where('status', OrderStatusEnum::FAILED());
    }

    /**
     * @param $date_from
     * @param $date_to
     *
     * @return int
     * @throws Exception
     */
    public function failedOrdersCount($date_from, $date_to)
    {
        $query = $this->query();
        $query = $this->failedFilter($query);

        //todo check if it works correctly with one date only
        if ($date_from && $date_to){
            $this->addFilterByPeriod($query, $date_from, $date_to);
        }

        return $query->count();
    }

    /**
     * @param array $data
     *
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws Exception
     */
    public function getByUserID(int $userId, int $perPage = null)
    {
        $query = $this->query()
            ->where('user_id', $userId);

        return $this->realGetMany($query, $perPage);
    }
}
