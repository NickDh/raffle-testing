<?php

namespace App\Ecommerce\Orders;


use App\Competitions\CompetitionService;
use App\Ecommerce\Cart\SessionCart;
use App\Ecommerce\Orders\Exceptions\OrdersEmailsSendingFailed;
use App\Enums\OrderStatusEnum;
use App\Enums\TicketStatusEnum;
use App\Jobs\CancelOldProcessingOrdersJob;
use App\Jobs\ReturnHeldTicketsJob;
use App\Mail\NewOrderForAdmin;
use App\Mail\NewOrderForCustomer;
use App\Tickets\TicketRepo;
use App\Tickets\TicketService;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException;
use Webmagic\Core\Entity\Exceptions\ModelNotDefinedException;

class OrderService
{
    /**
     * @var OrderRepo
     */
    private $orderRepo;

    /**
     * OrderService constructor.
     *
     * @param OrderRepo $orderRepo
     */
    public function __construct(OrderRepo $orderRepo)
    {
        $this->orderRepo = $orderRepo;
    }

    /**
     * @param Model       $order
     * @param string|null $xref
     *
     * @return Model
     * @throws Exception
     */
    public function executeSuccessOrderScenario(Order $order, string $xref = null)
    {
        // Additional logging for errors
        try {
            $order->fill([
                'status' => OrderStatusEnum::PAID,
                'xref' => $xref
            ]);
            $order->save();

            // Mark tickets with right answer as Sold Out
            (new TicketRepo())->setStatus($order->ticketIDsWithCorrectAnswer(), TicketStatusEnum::SOLD_OUT);

            // Unlock tickets with wrong answer
            $this->unlockTickets(... $order->ticketIDsWithWrongAnswer());

            (new CompetitionService())->checkAndSetSoldOutStatus($order->cart_data->keys()->toArray());
        } catch (Exception $exception){
            Log::channel('payment')->error("User $order->user_id order $order->id processing failed | " . $exception->getMessage());
            throw new Exception($exception->getMessage(), $exception->getCode(), $exception);
        }

        // Errors with emails will be only logged
        try{
//            Mail::queue(new NewOrderForAdmin($order));
            Mail::send(new NewOrderForCustomer($order));
        } catch (Exception $e){
            Log::error($e->getMessage(). $e->getCode() . ':' .  $e->getMessage() . PHP_EOL . $e->getTraceAsString());
        }

        return $order;
    }

    /**
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     * @throws Exception
     *
     * @return int
     */
    public function cancelOldProcessingOrders()
    {
        //Get unprocessed orders
        $oldUnprocessedOrders = (new OrderRepo())->getOldUnprocessedOrders();

        //Mark orders filed and return tickets
        foreach ($oldUnprocessedOrders as $order){
            logger("Order $order->id was not confirmed from Paytriot");
            $this->executeFailOrderScenario($order);
            logger("Order $order->id marked as 'Failed'. All tickets unblocked. Processed in " . get_class($this));
        }

        return $oldUnprocessedOrders->count();
    }

    /**
     * @param Model $order
     *
     * @return Model
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function executeFailOrderScenario(Model $order)
    {
        $this->orderRepo->update($order->id, ['status' => OrderStatusEnum::FAILED]);

        $this->clearOrderTickets($order);

        return $order;
    }

    /**
     * Clear cart and tickets from db
     *
     * @param SessionCart $cart
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function clearCartWithTickets(SessionCart $cart)
    {
        $ids = $cart->getAllTicketsIds();
        if (count($ids)){
            (new TicketRepo())->destroy($ids);
        }
        $cart->clear();
    }

    /**
     * Clear tickets from order if cart already does not exist
     *
     * @param Model $order
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function clearOrderTickets(Model $order)
    {
        $ids = $order->ticketsIDs();

        $this->unlockTickets(...$ids);
    }

    /**
     * Unlock tickets
     *
     * @param mixed ...$ids
     *
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    protected function unlockTickets(... $ids)
    {
        if (count($ids)){
            (new TicketRepo())->destroy($ids);
        }
    }

    /**
     * Prepare order data and create in db
     *
     * @param $user
     * @param SessionCart $cart
     * @return Model
     * @throws Exception
     */
    public function createOrder($user, SessionCart $cart)
    {
        //Prepare order data for db
        $order_data = [
            'user_id' => $user->id,
            'cart_data' => $cart->getContent(),
            'subtotal' => $cart->getSubTotal(),
            'total' => $cart->getTotal(),
            'discount_code' => $cart->getDiscountCode(),
            'discount_value' => $cart->getDiscountValue(),
            'promo_code_id' => $cart->getDiscount()['id'] ?? null,
            'customer_name' => $user->first_name . ' ' . $user->last_name,
            'customer_email' => $user->email,
            'customer_phone' => $user->phone,
            'customer_address' => $user->address,
            'customer_county' => $user->county,
            'customer_city' => $user->town,
            'customer_post_code' => $user->post_code,
            'status' => OrderStatusEnum::PROCESSING,
            'viewed_notification' => false
        ];

        $order = $this->orderRepo->create($order_data);

        $tickets = $cart->getTickets()->keyBy('id');
        $tickets = $tickets->map(function ($ticket) {
            return collect($ticket->toArray())
                ->only(['number', 'competition_id'])
                ->all();
        });

        $order->tickets()->sync($tickets);

        return $order;
    }

    /**
     * Check items in cart, if there doesn't exist need to clear cart
     *
     * @param SessionCart $cart
     *
     * @return bool
     * @throws EntityNotExtendsModelException
     * @throws ModelNotDefinedException
     */
    public function checkCartStatement(SessionCart $cart)
    {
        if ($cart->getItemsQuantity() == 0) {
            return false;
        }

        $items = $cart->getAllTicketsIds();
        $ticketService = new TicketService();

        if (! $ticketService->checkIfTicketsExist($items)) {
            $this->clearCartWithTickets($cart);

            return false;
        }

        return  true;
    }
}
