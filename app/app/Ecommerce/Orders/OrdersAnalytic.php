<?php


namespace App\Ecommerce\Orders;


use App\Enums\OrderStatusEnum;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class OrdersAnalytic
{
    /**
     * @param Carbon|null $from
     *
     * @return mixed
     */
    public function getMonthlyAnalytics(Carbon $from = null)
    {
        $from = $from ?? now()->subYear();

        /** @var Collection|Order[] $query */
        $query = Order::where('status', OrderStatusEnum::PAID)
            ->where('created_at', '>', $from)
            ->where('created_at', '<=', now())
            ->select(DB::raw("DATE_FORMAT(created_at, '%Y-%m') as month"), DB::raw('sum(total) as sum'))
            ->groupBy('month');

        $orders = $query->get();

        $preparedArray = [];

        foreach ($orders as $order){
            $key = Carbon::createFromFormat('Y-m', $order->month)->format('F (Y)');
            $preparedArray[$key] = round($order->sum, 0);
        }

        return  $preparedArray;
    }
}