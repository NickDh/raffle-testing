<?php

namespace App\Ecommerce\Orders;


use App\Ecommerce\CompetitionEntries\CompetitionEntry;
use App\Enums\OrderStatusEnum;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Webmagic\Dashboard\Components\TableGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Dashboard\Elements\Lists\DescriptionList;


class OrderDashboardPresenter
{

    /**
     * @param $items
     * @param Dashboard $dashboard
     * @param Request $request
     * @return mixed|Dashboard
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function getTablePage($items, Dashboard $dashboard, Request $request, OrderFilter $filter)
    {
        $data['items'] = $request->get('items', 10);
        $data['page'] = $request->get('page', 1);
        $data['keyword'] = $filter ? $filter->getSearchPhrase() : $request->get('keyword', '');

        $tablePageGenerator = (new TablePageGenerator($dashboard->page()))
            ->title('Orders')
            ->tableTitles('Order #', 'User', 'Date', 'Status', 'Total', 'Actions')
            ->showOnly([
                'id',
                'user',
                'date',
                'status',
                'total'
            ])
            ->setConfig([
                'date' => function (Order $item) {
                    return date('d/n/Y @ H:i', strtotime($item->created_at));
                },
                'user' => function (Order $item) {
                    return $item->customer_name ?: '--';
                },
                'total' => function (Order $item) {
                    return '£'.formatValue($item->total);
                },

            ])
            ->setShowLinkClosure(
                function (Order $item) {
                    return route('dashboard::orders.show', $item->id);
                }
            )
            ->items($items)
            ->withPagination($items, route('dashboard::orders.index', $data))
        ;

        $tablePageGenerator->addFiltering()
            ->action(route('dashboard::orders.index'))
            ->method('GET')
            ->textInput('keyword', $data['keyword'], '', false)
            ->submitButton('Search');



        if ($request->ajax()) {
            return $dashboard->page()->content()->toArray()['box_body'];
        }

        return $dashboard;
    }

    /**
     * @param Model $order
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function getDescriptionList(Model $order, Dashboard $dashboard)
    {
        $page = $dashboard->page();
        $page->setPageTitle("Order # $order->id",)
            ->addElement()
            ->box()->boxTitle('Billed To:')->footerAvailable(false)
            ->addElement('box_tools')->linkButton()
            ->content('Cancel')
            ->class( OrderStatusEnum::CANCELLED()->is($order->status) ? 'btn-danger js_ajax-by-click-btn disabled' : 'btn-danger js_ajax-by-click-btn')
            ->dataAttr('action', route('dashboard::orders.cancel', $order->id))
            ->dataAttr('method', 'POST')
            ->dataAttr('confirm', true)->parent()
            ->addToolsLinkButton(route('dashboard::orders.index'), 'Back To Orders')

            ->element()->descriptionList(
                [
                    'Name: ' => $order->customer_name,
                    'Address: ' => $order->customer_address ?? ' - ',
                    'City: ' => $order->customer_city ? $order->customer_city : ' - ',
                    'Post Code: ' => $order->customer_post_code ?  $order->customer_post_code : ' - ',
                    'Phone: ' => $order->customer_phone ? $order->customer_phone : ' - ',
                    'Email: ' => $order->customer_email,
                    'Order Date: ' => $order->present()->prettyCreatedDateTime()
                ]
            )->isHorizontal(true)
            ->parent('page');

        return $dashboard;
    }

    /**
     * @param Model $order
     * @param Dashboard $dashboard
     * @return Dashboard
     * @throws NoOneFieldsWereDefined
     * @throws Exception
     */
    public function getOrderItemsTable(Model $order, Dashboard $dashboard)
    {
        $table = (new TableGenerator())
            ->tableTitles('Competition', 'Price', 'Quantity', 'Tickets', 'Answer',  'Total')
            ->showOnly('competition', 'price', 'qty', 'tickets', 'answer', 'total')
            ->setConfig([
                'competition' => function(CompetitionEntry $entry){
                    return $entry->competition->title;
                },
                'qty' => function(CompetitionEntry $entry){
                    return count($entry->selectedTickets);
                },
                'price' => function (CompetitionEntry $item) {
                    return '£'.$item->present()->entryPrice();
                },
                'tickets' => function (CompetitionEntry $item) {
                    return $item->present()->ticketNumbers();
                },
                'answer' => function(CompetitionEntry $entry){
                    return $entry->answer . '<br><small>' . $entry->present()->answerStatus().'</small>';
                },
                'total' => function (CompetitionEntry $item) {
                    return '£'.$item->present()->entryTotal();
                },
            ])
            ->items($order->competitionEntries);


        $total_part = (new DescriptionList())->addData([
            'Subtotal: ' => '£'.formatValue($order->subtotal),
            'Discount Code: ' => $order->discount_code ?: '-',
            'Discount Value: ' => $order->discount_value > 0 ? '£'.formatValue($order->discount_value) : 0,
            'Total: ' => '£'.formatValue($order->total)
        ])->isHorizontal(true);
        $total_part = "<div class='pull-right'> $total_part </div>";


        $dashboard->page()->addElement()
            ->box()->boxTitle('Order Summary')
            ->content($table)->addBoxFooter($total_part);


        return $dashboard;
    }
}
