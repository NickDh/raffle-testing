<?php


namespace App\Ecommerce\CompetitionEntries\Dashboard;


use App\Ecommerce\CompetitionEntries\CompetitionEntry;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Core\Content\Exceptions\FieldUnavailable;
use Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined;

class CompetitionEntriesTablePageGenerator extends TablePageGenerator
{
    /**
     * @param LengthAwarePaginator          $paginator
     *
     *
     * @param int|null                      $competitionId
     *
     * @param CompetitionEntriesFilter|null $filter
     *
     * @return $this
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    public function prepare($paginator, int $competitionId = null, CompetitionEntriesFilter $filter = null)
    {


        $this
            ->tableTitles('Order #', 'User', 'Email', 'Tickets` numbers', 'Answer', 'Answer status', 'Date')
            ->showOnly('order_id', 'user', 'email', 'tickets', 'answer', 'answer_status', 'created_at')
            ->setConfig([
                'user' => function(CompetitionEntry $entry){
                    return $entry->user->present()->prepareFullName() . $entry->order->status;
                },
                'email' => function(CompetitionEntry $entry){
                    return $entry->user->email;
                },
                'tickets' => function(CompetitionEntry $entry){
                    return $entry->present()->ticketNumbers();
                },
                'answer_status' => function(CompetitionEntry $entry){
                    return $entry->present()->answerStatus();
                },
                'created_at' => function(CompetitionEntry $entry){
                    return $entry->present()->date();
                }
            ])
            ->items($paginator->items())

            ->withPagination($paginator, route('dashboard::competition-entries.table', $competitionId));

        $this->addExportBtn($competitionId);
        $this->prepareFiltering($competitionId, $filter);

        $this->getBox()->boxTitle('Competition entries');

        return $this;
    }

    /**
     * @param int|null                      $competitionId
     *
     * @param CompetitionEntriesFilter|null $filter
     *
     * @throws FieldUnavailable
     * @throws NoOneFieldsWereDefined
     */
    protected function prepareFiltering(int $competitionId = null, CompetitionEntriesFilter $filter = null)
    {
       $request = request();
       $keyword = $filter ? $filter->getSearchPhrase() : $request->get('keyword' ,'');
       $answerStatus = $filter ? $filter->getAnswerStatus() : $request->get('answer_status' ,'Any');

        $this
            ->addFiltering()
            ->method('GET')
            ->action(route('dashboard::competition-entries.table', $competitionId))
            ->textInput('keyword', $keyword, 'Search by')
            ->selectJS('answer_status', ['Any' => 'Any', 'Correct' => 'Correct', 'Incorrect' => 'Incorrect'], $answerStatus, 'Answer')
            ->submitButton('Filter');
    }

    /**
     * @param int|null $competitionId
     *
     * @return $this
     * @throws NoOneFieldsWereDefined
     */
    protected function addExportBtn(int $competitionId = null)
    {
        $exportLink = route('dashboard::competition-entries.export',  $competitionId);

        $this->addToolsLinkButton($exportLink, 'Export to CSV');

        return $this;
    }
}
