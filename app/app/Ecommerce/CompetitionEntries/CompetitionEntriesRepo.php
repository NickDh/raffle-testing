<?php


namespace App\Ecommerce\CompetitionEntries;


use App\Ecommerce\Cart\SessionCart;
use App\Ecommerce\CompetitionEntries\Dashboard\CompetitionEntriesFilter;
use App\Ecommerce\Orders\Order;
use App\Enums\CompetitionStatusEnum;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Webmagic\Core\Entity\EntityRepo;

class CompetitionEntriesRepo extends EntityRepo
{
    /** @var string  */
    protected $entity = CompetitionEntry::class;



    /**
     * @param int      $userId
     * @param int|null $perPage
     *
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getLiveEntriesWithCorrectAnswerForUser(int $userId, int $perPage = null)
    {
        $query = $this->query()
            ->where('competition_entries.user_id', $userId)
            ->where('competition_entries.is_answer_right', 1)
            ->whereOrderPaid();

        $query
            ->leftJoin('competitions', 'competition_entries.competition_id', '=', 'competitions.id')
            ->where('competitions.status', '<>', CompetitionStatusEnum::FINISHED());

        $query->select('competition_entries.*');

        return $this->realGetMany($query, $perPage);
    }

    /**
     * @param int      $userId
     * @param int|null $perPage
     *
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getFinishedEntriesWithCorrectAnswerForUser(int $userId, int $perPage = null)
    {
        $query = $this->query()
            ->where('competition_entries.user_id', $userId)
            ->where('competition_entries.is_answer_right', 1)
            ->whereOrderPaid();

        $query
            ->leftJoin('competitions', 'competition_entries.competition_id', '=', 'competitions.id')
            ->where('competitions.status', '=', CompetitionStatusEnum::FINISHED());

        $query->select('competition_entries.*');

        return $this->realGetMany($query, $perPage);
    }

    /**
     * @param int                           $competitionId
     * @param int|null                      $perPage
     *
     * @param CompetitionEntriesFilter|null $filter
     *
     * @param bool                          $paidOrdersOnly

     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getByCompetitionId(int $competitionId, int $perPage = null, CompetitionEntriesFilter $filter = null, bool $paidOrdersOnly = true)
    {
        $query = $this->query();

        $query->where('competition_entries.competition_id', $competitionId);

        if($filter){
            $this->applyFilter($filter, $query);
        }

        if($paidOrdersOnly){
            $query->whereOrderPaid();
        }

        $query->select('competition_entries.*');

        return $this->realGetMany($query, $perPage);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    protected function addOrdering(Builder $query): Builder
    {
        $query->orderBy('competition_entries.created_at', 'desc');

        return parent::addOrdering($query);
    }


    /**
     * @param int|null                      $competitionId
     * @param CompetitionEntriesFilter|null $filter
     *
     * @return Builder[]|Collection
     * @throws Exception
     */
    public function getForExport(int $competitionId = null, CompetitionEntriesFilter $filter = null)
    {
        $query = $this->query();

        $query->where('competition_id', $competitionId);

        $query->whereOrderPaid();

        if($filter){
            $this->applyFilter($filter, $query);
        }

        $query->join('selected_tickets', 'selected_tickets.competition_entry_id', '=', 'competition_entries.id');

        $query->select('competition_entries.*', 'selected_tickets.ticket as number');

        return $query->get();
    }

    /**
     * @param null                          $perPage
     * @param CompetitionEntriesFilter|null $filter
     *
     * @return LengthAwarePaginator|Collection|\Illuminate\Pagination\LengthAwarePaginator
     * @throws Exception
     */
    public function getAll($perPage = null, CompetitionEntriesFilter $filter = null)
    {
        $query = $this->query();

        if($filter){
            $this->applyFilter($filter, $query);
        }

        return $this->realGetMany($query, $perPage);
    }


    /**
     * @param CompetitionEntriesFilter $filter
     * @param Builder                  $builder
     */
    protected function applyFilter(CompetitionEntriesFilter $filter, Builder $builder)
    {
        if($filter->isAnswerStatusOptionActivated()){
            $builder->where('is_answer_right', $filter->showOnlyCorrectAnswers());
        }

        if($filter->hasSearchPhrase()){
            $searchQuery = "%{$filter->getSearchPhrase()}%";
            $builder
                ->leftJoin('users', 'competition_entries.user_id', '=','users.id');

            $builder
                ->where('competition_entries.order_id', 'like', $searchQuery)
                ->orWhere('competition_entries.answer', 'like', $searchQuery)
                ->orWhere('users.full_name', 'like', $searchQuery)
                ->orWhere('users.email', 'like', $searchQuery);
        }
    }

    /**
     * Create CompetitionEntry and Selected Tickets based on Cart And Order
     *
     * @param SessionCart $cart
     * @param Order       $order
     *
     * @throws Exception
     */
    public function createBasedOnOrder(SessionCart $cart, Order $order)
    {
        $data = $cart->getContent();

        foreach ($data as $competitionDetails){
            /** @var CompetitionEntry $competitionEntry */
            $competitionEntry = $this->create([
                'competition_title' => $competitionDetails['competition'],
                'competition_id' => $competitionDetails['competition_id'],
                'user_id' => $order->user_id,
                'question' => '',
                'question_id' => 0,
                'answer' => $competitionDetails['answer'],
                'answer_id' => $competitionDetails['answer_id'],
                'is_answer_right' => $competitionDetails['is_answer_correct'],
                'order_id' => $order->id,
                'entry_price' => $competitionDetails['item_price']
            ]);

            $tickets = explode(', ', $competitionDetails['ticket_number']);
            $selectedTickets = [];
            foreach ($tickets as $ticket){
                $selectedTickets[] = [
                    'ticket' => $ticket
                ];
            }

            $competitionEntry->selectedTickets()->createMany($selectedTickets);
        }

    }
}