<?php


namespace App\app\Ecommerce\CompetitionEntries;


use Illuminate\Support\Carbon;
use Webmagic\Core\Presenter\Presenter;

class CompetitionEntryPresenter extends Presenter
{
    /**
     * @return string
     */
    public function ticketNumbers()
    {
        return $this->entity->selectedTickets->implode('ticket', ', ');
    }

    /**
     * @return string
     */
    public function entryPrice()
    {
        return number_format($this->entity->entry_price, 2, '.', '');
    }

    /**
     * @return string
     */
    public function entryTotal()
    {
        return number_format($this->entity->entry_price * $this->entity->selectedTickets->count(), 2, '.', '');
    }

    /**
     * @return string
     */
    public function answerStatus()
    {
        return $this->entity->isAnswerCorrect() ? 'Correct' : 'Incorrect';
    }

    /**
     * @return string
     */
    public function date()
    {
        return Carbon::parse($this->entity->created_at)->format('d.m.y');
    }

    /**
     * @return string
     */
    public function prettyDate()
    {
        return Carbon::parse($this->entity->created_at)->format('d/m/Y');
    }
}