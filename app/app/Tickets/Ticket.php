<?php

namespace App\Tickets;

use App\Competitions\Competition;
use App\Ecommerce\Orders\Order;
use App\Enums\OrderStatusEnum;
use App\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Tickets\Ticket
 *
 * @property int $id
 * @property string|null $status
 * @property string $number
 * @property int|null $user_id
 * @property int|null $competition_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Order[] $activeOrders
 * @property-read int|null $active_orders_count
 * @property-read Competition|null $competition
 * @property-read User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereCompetitionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereUserId($value)
 * @mixin \Eloquent
 */
class Ticket extends Model
{
    protected $fillable = [
        'id',
        'number',
        'status',
        'competition_id',
        'user_id',
        'created_at',
        'updated_at'
    ];

    /**
     * Return active orders for ticket
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Illuminate\Database\Query\Builder
     */
    public function activeOrders()
    {
        return $this->belongsToMany(Order::class, 'orders_tickets', 'ticket_id','order_id',  'id')
            ->whereNotIn('status', [OrderStatusEnum::FAILED, OrderStatusEnum::PROCESSING]);
    }

    /**
     * @return BelongsTo
     */
    public function competition()
    {
        return $this->belongsTo(Competition::class);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
