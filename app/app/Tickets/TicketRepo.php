<?php

namespace App\Tickets;


use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Webmagic\Core\Entity\EntityRepo;

class TicketRepo extends EntityRepo
{
    protected $entity = Ticket::class;

    /**
     * Return tickets based on IDs
     *
     * @param mixed ...$ids
     *
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getByIDs(... $ids)
    {
        $query = $this->query();
        $query->whereIn('id', $ids);

        return $this->realGetMany($query);
    }

    /**
     * Return tickets which has active orders
     *
     * @param mixed ...$ids
     *
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getByIdsWithActiveOrders(... $ids)
    {
        $query = $this->query();
        $query->whereIn('id', $ids)
            ->has('activeOrders');

        return $this->realGetMany($query);
    }

    /**
     * Get by competition id and ticket number or numbers
     *
     * @param $number
     * @param int $competition_id
     * @return LengthAwarePaginator|Collection
     * @throws Exception
     */
    public function getByNumber($number, int $competition_id)
    {
        $query = $this->query();

        $query->where('competition_id', $competition_id);

        if(is_array($number)){
            $query->whereIn('number', $number);
        } else{
            $query->where('number', $number);
        }

        return $this->realGetMany($query);
    }

    /**
     * Update status for one or many tickets by id
     *
     * @param $id
     * @param string $statusEnum
     * @return int
     * @throws Exception
     */
    public function setStatus($id, string $statusEnum)
    {
        $query = $this->query();

        if(is_array($id)){
            $query->whereIn('id', $id);
        } else{
            $query->where('id', $id);
        }

        return $query->update(['status' => $statusEnum]);
    }

    /**
     * @param null $keyword
     * @param null $per_page
     * @param null $page
     * @param null $competition_id
     * @param string $sortBy
     * @param string $sort
     * @param bool $with_users
     * @return LengthAwarePaginator|Builder[]|Collection
     * @throws Exception
     */
    public function getByFilter($keyword = null, $per_page = null, $page = null, $competition_id = null, $sortBy = 'created_at', $sort = 'desc', $with_users = true)
    {
        $query = $this->query();

        if ($keyword) {
            $query->where('number', 'LIKE', '%'.$keyword.'%');
        }

        $query->orderBy($sortBy, $sort);


        if ($competition_id){
            $query->where('competition_id', '=', $competition_id);
        }


        if ($with_users){
            $query->with('user');
        }


        if (is_null($per_page) && is_null($page)) {
            return $query->get();
        }


        return $query->paginate($per_page, ['*'], 'page', $page);
    }
}
