<?php

namespace App\Tickets;


use App\Enums\CompetitionTypeEnum;
use Exception;
use Illuminate\Database\Eloquent\Model;

class TicketService
{
    /**
     * Get array of number type tickets
     *
     * @param Model $competition
     * @return array
     */
    protected function getAllNumbersTickets(Model $competition)
    {
        $tickets = [];

        for ($i = 1; $i <= $competition->tickets_count; $i++) {
            $tickets[$i] = ['name' => $i];
        }

        return $tickets;
    }

    /**
     * Get array of letters type tickets
     *
     * @param Model $competition
     * @return array
     */
    protected function getAllLettersTickets(Model $competition)
    {
        $alphas = range('A', $competition->letters_to);

        $all_tickets_array = [];

        foreach ($alphas as $alpha) {
            $tickets = [];

            for ($i = 1; $i <= $competition->tickets_sort_by; $i++) {
                $tickets[$alpha . $i] = ['name' => $alpha . $i];;
            }

            $all_tickets_array[] = $tickets;
        }

        return array_merge(...$all_tickets_array);
    }

    /**
     * @param array $tickets
     * @param int $group_by
     * @param bool $for_letters
     * @return array
     */
    protected function chunkTicketsByGroup(array $tickets, int $group_by, bool $for_letters = false)
    {
        $chunks = [];
        foreach (array_chunk($tickets, $group_by, true) as $chunk) {

            if ($for_letters) {
                $name = array_first($chunk)['name'][0];
            } else {
                $name = array_first($chunk)['name'] . ' - ' . array_last($chunk)['name'];
            }
            $chunks[$name] = $chunk;
        }

        return $chunks;
    }

    /**
     * Add special statuses for hold and sold tickets
     *
     * @param array $tickets
     * @param Model $competition
     * @return array
     */
    protected function addBusyStatusesToTickets(array $tickets, Model $competition)
    {
        foreach ($competition->tickets as $hold_ticket){
            $tickets[$hold_ticket->number]['status'] = $hold_ticket->status;
        }

        return $tickets;
    }

    /**
     * Prepare special array of chunks with tickets and their statuses and add random picked tickets if need
     *
     * @param Model $competition
     * @param int $randomTicketsCount
     * @return array
     * @throws Exception
     */
    public function prepareGroupedTicketsForCompetition(Model $competition, int $randomTicketsCount = 0)
    {
        if (CompetitionTypeEnum::NUMBERS()->is($competition->competition_type)) {

            $tickets = $this->getAllNumbersTickets($competition);
            $tickets = $this->addBusyStatusesToTickets($tickets, $competition);

            if($randomTicketsCount > 0){
                $randomPickedTickets = $this->getRandomTickets($tickets, $randomTicketsCount);
                $tickets = $this->addSelectedStatusToTickets($tickets, $randomPickedTickets);
            }

            $chunks = $this->chunkTicketsByGroup($tickets, $competition->tickets_sort_by);

            $chunks['All Available'] = array_filter($tickets, function ($ticket) {
                if (!isset($ticket['status']) || $ticket['status'] == 'selected') {
                    return $ticket;
                }
            });
        } else {
            $tickets = $this->getAllLettersTickets($competition);
            $tickets = $this->addBusyStatusesToTickets($tickets, $competition);

            if($randomTicketsCount > 0){
                $randomPickedTickets = $this->getRandomTickets($tickets, $randomTicketsCount);
                $tickets = $this->addSelectedStatusToTickets($tickets, $randomPickedTickets);
            }

            $chunks = $this->chunkTicketsByGroup($tickets, $competition->tickets_sort_by, true);

        }

        return [
            'chunks' => $chunks,
            'picked' => $randomPickedTickets ?? []
        ];
    }

    /**
     * Get all unpicked tickets and find random free tickets
     *
     * @param array $tickets
     * @param int $randomTickets
     * @return array|mixed
     * @throws Exception
     */
    protected function getRandomTickets(array $tickets, int $randomTickets)
    {
        $unpickedTickets = array_filter($tickets, function ($item){
            return !isset($item['status']);
        });

        $availableCount = count($unpickedTickets);

        if($randomTickets > $availableCount){
            throw new Exception(
                "Only $availableCount tickets available"
            );
        }
        $result = array_rand($unpickedTickets, $randomTickets);

        if(is_array($result)){
            return $result;
        }

        return array($result);
    }


    /**
     * Add special status for picked tickets
     *
     * @param array $tickets
     * @param array $randomPickedTickets
     * @return array
     */
    protected function addSelectedStatusToTickets(array $tickets, array $randomPickedTickets)
    {
        foreach ($randomPickedTickets as $ticket){
            $tickets[$ticket]['status'] = 'selected';
        }

        return $tickets;
    }

    /**
     * @param array $ids
     * @return bool
     * @throws Exception
     */
    public function checkIfTicketsExist(array $ids)
    {
        $ticketRepo = new TicketRepo();

        foreach ($ids as $id){
            if (! $ticket = $ticketRepo->getByID($id)){
                return false;
            }
        }

        return true;
    }
}
