<?php

/* Dashboard routes */
include 'dashboard.php';

/**
 * Pages
 */
Route::get('/', [
    'as' => 'main',
    'uses' => 'MainController@main',
]);
Route::post('live-draw/next', [
    'as' => 'live-draw.next',
    'uses' => 'MainController@getNextAvailableLiveDraw',
]);
Route::post('contact', [
    'as' => 'contact.send',
    'uses' => 'MainController@contactForm',
]);
Route::get('live-competitions', [
    'as' => 'live-competitions',
    'uses' => 'MainController@competitions',
]);
Route::get('live-competitions/{slug}', [
    'as' => 'live-competition.show',
    'uses' => 'MainController@competition',
]);
Route::post('live-competitions/{id}/deadline', [
    'as' => 'live-competition.deadline',
    'uses' => 'MainController@checkCompetitionDeadline',
]);
Route::get('winners', [
    'as' => 'winners',
    'uses' => 'MainController@winners',
]);
Route::get('results', [
    'as' => 'results',
    'uses' => 'MainController@results',
]);
Route::get('results/{id?}', [
    'as' => 'result-details',
    'uses' => 'MainController@resultDetails',
]);
Route::get('finished-competitions', [
    'as' => 'finished-competitions',
    'uses' => 'MainController@finishedCompetitions',
]);
Route::get('finished-competition/{slug}', [
    'as' => 'live-competition.finished',
    'uses' => 'MainController@finishedCompetition',
]);
Route::get('page/{slug}', [
    'as' => 'page',
    'uses' => 'MainController@page'
]);

/**
 * User
 */
Route::group([
    'as' => 'user.',
    'prefix' => 'user',
    'middleware' => 'auth',
], function () {
    Route::get('', [
        'as' => 'info',
        'uses' => 'UserController@getMainPage',
    ]);

    Route::post('', [
        'as' => 'update-info',
        'uses' => 'UserController@updateUserInfo',
    ]);

    Route::post('update-password', [
        'as' => 'update-password',
        'uses' => 'UserController@updatePassword',
    ]);
});

/**
 * Static pages
 */
Route::group([
    'as' => 'static.',
], function () {
    Route::view('contacts', 'contacts')->name('contacts');
});

Route::group([
    'as' => 'static.',
], function () {
    Route::view('terms-conditions', 'terms-conditions')->name('terms-conditions');
});

Route::group([
    'as' => 'static.',
], function () {
    Route::view('privacy-policy', 'privacy-policy')->name('privacy-policy');
});

Route::group([
    'as' => 'static.',
], function () {
    Route::view('how-to-play', 'how-to-play')->name('how-to-play');
});

Route::group([
    'as' => 'static.',
], function () {
    Route::view('charity', 'charity')->name('charity');
});

/**
 * Auth
 */
Route::group([
], function () {
    Auth::routes();
    Route::view('user/login', 'login')->name('user.login')->middleware('guest');
    Route::view('user/register', 'register')->name('user.register')->middleware('guest');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    Route::view('reset-password', 'reset-password')->name('reset-password');
    Route::view('forgot-password', 'forgot-password')->name('forgot-password');
});

/**
 * Cart
 */
Route::group([
    'as' => 'cart.',
    'prefix' => 'cart',
    'middleware' => 'auth',
], function () {
    Route::post('add', 'CartController@addItems')->name('add');
    Route::post('delete/{item_id}', 'CartController@deleteItems')->name('delete');
    Route::post('coupon', 'CartController@checkCouponCode')->name('check-coupon');
    Route::post('random', 'CartController@getRandomTickets')->name('random-tickets');
    Route::post('add-random', 'CartController@addRandomItems')->name('add-random');
    Route::get('data','CartController@getData')->name('data');
});
Route::get('cart', 'CartController@getCartPage')->name('cart.view');

/**
 * Order
 */
Route::group([
    'as' => 'order.',
    'prefix' => 'order',
    'middleware' => 'auth',
], function () {
    Route::post('create', 'OrderController@create')->name('create');
    Route::post('delete/{order}', 'OrderController@delete')->name('delete');
    Route::get('details/{order_id}', 'OrderController@detailsPage')->name('details');
    Route::get('checkout', 'OrderController@checkout')->name('checkout');
    Route::get('payment-result/{order_id}', 'OrderController@paymentResult')->name('payment-result')->where(['order_id' => '[0-9]*']);
});

Route::get('orders','OrderController@index')->name('orders')->middleware('auth');

/**
 * E-commerce
 */
Route::view('times-up', 'times-up')->name('times-up');
Route::match(['get', 'post'],'entries',[
    'middleware' => 'auth',
    'as' => 'entries',
    'uses' => 'MainController@entries'
]);

Route::view('change-password', 'change-password')->name('change-password');
