#!/usr/bin/env bash

# Prepare config
cp .env.example .env
composer install
php artisan key:generate
php artisan migrate --seed
chmod 777 -R storage/ bootstrap/cache public/css/custom.css