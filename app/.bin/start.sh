#!/usr/bin/env bash

## CRON
service cron start
## Install Cron tasks
/bin/bash .cron/add_cron_tasks.sh

# Supervisor
service supervisor start
# Install workers
/bin/bash .supervisor/install_supervisor_workers.sh

# To have no issues with permissions
chown -R www-data:www-data /srv/app/storage
chown -R www-data:www-data /srv/app/bootstrap/cache

find /srv/app/storage -type f -exec chmod 644 {} \;
find /srv/app/storage -type d -exec chmod 755 {} \;

find /srv/app/bootstrap/cache -type f -exec chmod 644 {} \;
find /srv/app/bootstrap/cache -type d -exec chmod 755 {} \;

# Custom CSS permissions updating
chown -R www-data:www-data /srv/app/public/css/custom.css
chmod 644 /srv/app/public/css/custom.css

# Run PHP-FPM
php-fpm
