/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/js/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js/libs.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/libs.js":
/*!********************!*\
  !*** ./js/libs.js ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./libs/plugins.js */ "./js/libs/plugins.js");

/***/ }),

/***/ "./js/libs/oldFiles/ResizeSensor.min.js":
/*!**********************************************!*\
  !*** ./js/libs/oldFiles/ResizeSensor.min.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {!function () {
  var e = function e(t, i) {
    function s() {
      this.q = [], this.add = function (e) {
        this.q.push(e);
      };
      var e, t;

      this.call = function () {
        for (e = 0, t = this.q.length; e < t; e++) {
          this.q[e].call();
        }
      };
    }

    function o(e, t) {
      return e.currentStyle ? e.currentStyle[t] : window.getComputedStyle ? window.getComputedStyle(e, null).getPropertyValue(t) : e.style[t];
    }

    function n(e, t) {
      if (e.resizedAttached) {
        if (e.resizedAttached) return void e.resizedAttached.add(t);
      } else e.resizedAttached = new s(), e.resizedAttached.add(t);

      e.resizeSensor = document.createElement("div"), e.resizeSensor.className = "resize-sensor";
      var i = "position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;",
          n = "position: absolute; left: 0; top: 0; transition: 0s;";
      e.resizeSensor.style.cssText = i, e.resizeSensor.innerHTML = '<div class="resize-sensor-expand" style="' + i + '"><div style="' + n + '"></div></div><div class="resize-sensor-shrink" style="' + i + '"><div style="' + n + ' width: 200%; height: 200%"></div></div>', e.appendChild(e.resizeSensor), {
        fixed: 1,
        absolute: 1
      }[o(e, "position")] || (e.style.position = "relative");
      var d,
          r,
          l = e.resizeSensor.childNodes[0],
          c = l.childNodes[0],
          h = e.resizeSensor.childNodes[1],
          a = (h.childNodes[0], function () {
        c.style.width = l.offsetWidth + 10 + "px", c.style.height = l.offsetHeight + 10 + "px", l.scrollLeft = l.scrollWidth, l.scrollTop = l.scrollHeight, h.scrollLeft = h.scrollWidth, h.scrollTop = h.scrollHeight, d = e.offsetWidth, r = e.offsetHeight;
      });
      a();

      var f = function f() {
        e.resizedAttached && e.resizedAttached.call();
      },
          u = function u(e, t, i) {
        e.attachEvent ? e.attachEvent("on" + t, i) : e.addEventListener(t, i);
      },
          p = function p() {
        e.offsetWidth == d && e.offsetHeight == r || f(), a();
      };

      u(l, "scroll", p), u(h, "scroll", p);
    }

    var d = Object.prototype.toString.call(t),
        r = "[object Array]" === d || "[object NodeList]" === d || "[object HTMLCollection]" === d || "undefined" != typeof jQuery && t instanceof jQuery || "undefined" != typeof Elements && t instanceof Elements;
    if (r) for (var l = 0, c = t.length; l < c; l++) {
      n(t[l], i);
    } else n(t, i);

    this.detach = function () {
      if (r) for (var i = 0, s = t.length; i < s; i++) {
        e.detach(t[i]);
      } else e.detach(t);
    };
  };

  e.detach = function (e) {
    e.resizeSensor && (e.removeChild(e.resizeSensor), delete e.resizeSensor, delete e.resizedAttached);
  },  true && "undefined" != typeof module.exports ? module.exports = e : window.ResizeSensor = e;
}();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/libs/oldFiles/common_scripts.min.js":
/*!************************************************!*\
  !*** ./js/libs/oldFiles/common_scripts.min.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module, __webpack_provided_window_dot_jQuery, jQuery, global) {var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_0__;var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof2(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _extends() {
  return (_extends = Object.assign || function (t) {
    for (var e = 1; e < arguments.length; e++) {
      var n = arguments[e];

      for (var i in n) {
        Object.prototype.hasOwnProperty.call(n, i) && (t[i] = n[i]);
      }
    }

    return t;
  }).apply(this, arguments);
}

function _typeof(t) {
  return (_typeof = "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator) ? function (t) {
    return _typeof2(t);
  } : function (t) {
    return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : _typeof2(t);
  })(t);
}

!function (t, e) {
  "use strict";

  "object" == ( false ? undefined : _typeof2(module)) && "object" == _typeof2(module.exports) ? module.exports = t.document ? e(t, !0) : function (t) {
    if (!t.document) throw new Error("jQuery requires a window with a document");
    return e(t);
  } : e(t);
}("undefined" != typeof window ? window : this, function (x, t) {
  "use strict";

  function g(t) {
    return null != t && t === t.window;
  }

  var e = [],
      i = Object.getPrototypeOf,
      a = e.slice,
      m = e.flat ? function (t) {
    return e.flat.call(t);
  } : function (t) {
    return e.concat.apply([], t);
  },
      l = e.push,
      o = e.indexOf,
      n = {},
      r = n.toString,
      v = n.hasOwnProperty,
      s = v.toString,
      c = s.call(Object),
      y = {},
      _ = function _(t) {
    return "function" == typeof t && "number" != typeof t.nodeType;
  },
      E = x.document,
      u = {
    type: !0,
    src: !0,
    nonce: !0,
    noModule: !0
  };

  function b(t, e, n) {
    var i,
        o,
        r = (n = n || E).createElement("script");
    if (r.text = t, e) for (i in u) {
      (o = e[i] || e.getAttribute && e.getAttribute(i)) && r.setAttribute(i, o);
    }
    n.head.appendChild(r).parentNode.removeChild(r);
  }

  function w(t) {
    return null == t ? t + "" : "object" == _typeof2(t) || "function" == typeof t ? n[r.call(t)] || "object" : _typeof2(t);
  }

  var h = "3.5.1",
      T = function T(t, e) {
    return new T.fn.init(t, e);
  };

  function d(t) {
    var e = !!t && "length" in t && t.length,
        n = w(t);
    return !_(t) && !g(t) && ("array" === n || 0 === e || "number" == typeof e && 0 < e && e - 1 in t);
  }

  T.fn = T.prototype = {
    jquery: h,
    constructor: T,
    length: 0,
    toArray: function toArray() {
      return a.call(this);
    },
    get: function get(t) {
      return null == t ? a.call(this) : t < 0 ? this[t + this.length] : this[t];
    },
    pushStack: function pushStack(t) {
      var e = T.merge(this.constructor(), t);
      return e.prevObject = this, e;
    },
    each: function each(t) {
      return T.each(this, t);
    },
    map: function map(n) {
      return this.pushStack(T.map(this, function (t, e) {
        return n.call(t, e, t);
      }));
    },
    slice: function slice() {
      return this.pushStack(a.apply(this, arguments));
    },
    first: function first() {
      return this.eq(0);
    },
    last: function last() {
      return this.eq(-1);
    },
    even: function even() {
      return this.pushStack(T.grep(this, function (t, e) {
        return (e + 1) % 2;
      }));
    },
    odd: function odd() {
      return this.pushStack(T.grep(this, function (t, e) {
        return e % 2;
      }));
    },
    eq: function eq(t) {
      var e = this.length,
          n = +t + (t < 0 ? e : 0);
      return this.pushStack(0 <= n && n < e ? [this[n]] : []);
    },
    end: function end() {
      return this.prevObject || this.constructor();
    },
    push: l,
    sort: e.sort,
    splice: e.splice
  }, T.extend = T.fn.extend = function () {
    var t,
        e,
        n,
        i,
        o,
        r,
        s = arguments[0] || {},
        a = 1,
        l = arguments.length,
        c = !1;

    for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == _typeof2(s) || _(s) || (s = {}), a === l && (s = this, a--); a < l; a++) {
      if (null != (t = arguments[a])) for (e in t) {
        i = t[e], "__proto__" !== e && s !== i && (c && i && (T.isPlainObject(i) || (o = Array.isArray(i))) ? (n = s[e], r = o && !Array.isArray(n) ? [] : o || T.isPlainObject(n) ? n : {}, o = !1, s[e] = T.extend(c, r, i)) : void 0 !== i && (s[e] = i));
      }
    }

    return s;
  }, T.extend({
    expando: "jQuery" + (h + Math.random()).replace(/\D/g, ""),
    isReady: !0,
    error: function error(t) {
      throw new Error(t);
    },
    noop: function noop() {},
    isPlainObject: function isPlainObject(t) {
      var e, n;
      return !(!t || "[object Object]" !== r.call(t) || (e = i(t)) && ("function" != typeof (n = v.call(e, "constructor") && e.constructor) || s.call(n) !== c));
    },
    isEmptyObject: function isEmptyObject(t) {
      var e;

      for (e in t) {
        return !1;
      }

      return !0;
    },
    globalEval: function globalEval(t, e, n) {
      b(t, {
        nonce: e && e.nonce
      }, n);
    },
    each: function each(t, e) {
      var n,
          i = 0;
      if (d(t)) for (n = t.length; i < n && !1 !== e.call(t[i], i, t[i]); i++) {
        ;
      } else for (i in t) {
        if (!1 === e.call(t[i], i, t[i])) break;
      }
      return t;
    },
    makeArray: function makeArray(t, e) {
      var n = e || [];
      return null != t && (d(Object(t)) ? T.merge(n, "string" == typeof t ? [t] : t) : l.call(n, t)), n;
    },
    inArray: function inArray(t, e, n) {
      return null == e ? -1 : o.call(e, t, n);
    },
    merge: function merge(t, e) {
      for (var n = +e.length, i = 0, o = t.length; i < n; i++) {
        t[o++] = e[i];
      }

      return t.length = o, t;
    },
    grep: function grep(t, e, n) {
      for (var i = [], o = 0, r = t.length, s = !n; o < r; o++) {
        !e(t[o], o) != s && i.push(t[o]);
      }

      return i;
    },
    map: function map(t, e, n) {
      var i,
          o,
          r = 0,
          s = [];
      if (d(t)) for (i = t.length; r < i; r++) {
        null != (o = e(t[r], r, n)) && s.push(o);
      } else for (r in t) {
        null != (o = e(t[r], r, n)) && s.push(o);
      }
      return m(s);
    },
    guid: 1,
    support: y
  }), "function" == typeof Symbol && (T.fn[Symbol.iterator] = e[Symbol.iterator]), T.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (t, e) {
    n["[object " + e + "]"] = e.toLowerCase();
  });

  var f = function (n) {
    function h(t, e) {
      var n = "0x" + t.slice(1) - 65536;
      return e || (n < 0 ? String.fromCharCode(65536 + n) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320));
    }

    function o() {
      C();
    }

    var t,
        f,
        b,
        r,
        s,
        p,
        d,
        g,
        w,
        l,
        c,
        C,
        x,
        a,
        E,
        m,
        u,
        v,
        y,
        T = "sizzle" + +new Date(),
        _ = n.document,
        S = 0,
        i = 0,
        A = lt(),
        k = lt(),
        D = lt(),
        I = lt(),
        N = function N(t, e) {
      return t === e && (c = !0), 0;
    },
        O = {}.hasOwnProperty,
        e = [],
        P = e.pop,
        L = e.push,
        j = e.push,
        M = e.slice,
        H = function H(t, e) {
      for (var n = 0, i = t.length; n < i; n++) {
        if (t[n] === e) return n;
      }

      return -1;
    },
        $ = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
        q = "[\\x20\\t\\r\\n\\f]",
        z = "(?:\\\\[\\da-fA-F]{1,6}" + q + "?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",
        R = "\\[" + q + "*(" + z + ")(?:" + q + "*([*^$|!~]?=)" + q + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + z + "))|)" + q + "*\\]",
        F = ":(" + z + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + R + ")*)|.*)\\)|)",
        W = new RegExp(q + "+", "g"),
        B = new RegExp("^" + q + "+|((?:^|[^\\\\])(?:\\\\.)*)" + q + "+$", "g"),
        U = new RegExp("^" + q + "*," + q + "*"),
        Q = new RegExp("^" + q + "*([>+~]|" + q + ")" + q + "*"),
        K = new RegExp(q + "|>"),
        V = new RegExp(F),
        Y = new RegExp("^" + z + "$"),
        X = {
      ID: new RegExp("^#(" + z + ")"),
      CLASS: new RegExp("^\\.(" + z + ")"),
      TAG: new RegExp("^(" + z + "|[*])"),
      ATTR: new RegExp("^" + R),
      PSEUDO: new RegExp("^" + F),
      CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + q + "*(even|odd|(([+-]|)(\\d*)n|)" + q + "*(?:([+-]|)" + q + "*(\\d+)|))" + q + "*\\)|)", "i"),
      bool: new RegExp("^(?:" + $ + ")$", "i"),
      needsContext: new RegExp("^" + q + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + q + "*((?:-\\d)?\\d*)" + q + "*\\)|)(?=[^-]|$)", "i")
    },
        Z = /HTML$/i,
        G = /^(?:input|select|textarea|button)$/i,
        J = /^h\d$/i,
        tt = /^[^{]+\{\s*\[native \w/,
        et = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
        nt = /[+~]/,
        it = new RegExp("\\\\[\\da-fA-F]{1,6}" + q + "?|\\\\([^\\r\\n\\f])", "g"),
        ot = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
        rt = function rt(t, e) {
      return e ? "\0" === t ? "�" : t.slice(0, -1) + "\\" + t.charCodeAt(t.length - 1).toString(16) + " " : "\\" + t;
    },
        st = yt(function (t) {
      return !0 === t.disabled && "fieldset" === t.nodeName.toLowerCase();
    }, {
      dir: "parentNode",
      next: "legend"
    });

    try {
      j.apply(e = M.call(_.childNodes), _.childNodes), e[_.childNodes.length].nodeType;
    } catch (t) {
      j = {
        apply: e.length ? function (t, e) {
          L.apply(t, M.call(e));
        } : function (t, e) {
          for (var n = t.length, i = 0; t[n++] = e[i++];) {
            ;
          }

          t.length = n - 1;
        }
      };
    }

    function at(t, e, n, i) {
      var o,
          r,
          s,
          a,
          l,
          c,
          u,
          h = e && e.ownerDocument,
          d = e ? e.nodeType : 9;
      if (n = n || [], "string" != typeof t || !t || 1 !== d && 9 !== d && 11 !== d) return n;

      if (!i && (C(e), e = e || x, E)) {
        if (11 !== d && (l = et.exec(t))) if (o = l[1]) {
          if (9 === d) {
            if (!(s = e.getElementById(o))) return n;
            if (s.id === o) return n.push(s), n;
          } else if (h && (s = h.getElementById(o)) && y(e, s) && s.id === o) return n.push(s), n;
        } else {
          if (l[2]) return j.apply(n, e.getElementsByTagName(t)), n;
          if ((o = l[3]) && f.getElementsByClassName && e.getElementsByClassName) return j.apply(n, e.getElementsByClassName(o)), n;
        }

        if (f.qsa && !I[t + " "] && (!m || !m.test(t)) && (1 !== d || "object" !== e.nodeName.toLowerCase())) {
          if (u = t, h = e, 1 === d && (K.test(t) || Q.test(t))) {
            for ((h = nt.test(t) && gt(e.parentNode) || e) === e && f.scope || ((a = e.getAttribute("id")) ? a = a.replace(ot, rt) : e.setAttribute("id", a = T)), r = (c = p(t)).length; r--;) {
              c[r] = (a ? "#" + a : ":scope") + " " + vt(c[r]);
            }

            u = c.join(",");
          }

          try {
            return j.apply(n, h.querySelectorAll(u)), n;
          } catch (e) {
            I(t, !0);
          } finally {
            a === T && e.removeAttribute("id");
          }
        }
      }

      return g(t.replace(B, "$1"), e, n, i);
    }

    function lt() {
      var i = [];
      return function t(e, n) {
        return i.push(e + " ") > b.cacheLength && delete t[i.shift()], t[e + " "] = n;
      };
    }

    function ct(t) {
      return t[T] = !0, t;
    }

    function ut(t) {
      var e = x.createElement("fieldset");

      try {
        return !!t(e);
      } catch (t) {
        return !1;
      } finally {
        e.parentNode && e.parentNode.removeChild(e), e = null;
      }
    }

    function ht(t, e) {
      for (var n = t.split("|"), i = n.length; i--;) {
        b.attrHandle[n[i]] = e;
      }
    }

    function dt(t, e) {
      var n = e && t,
          i = n && 1 === t.nodeType && 1 === e.nodeType && t.sourceIndex - e.sourceIndex;
      if (i) return i;
      if (n) for (; n = n.nextSibling;) {
        if (n === e) return -1;
      }
      return t ? 1 : -1;
    }

    function ft(e) {
      return function (t) {
        return "form" in t ? t.parentNode && !1 === t.disabled ? "label" in t ? "label" in t.parentNode ? t.parentNode.disabled === e : t.disabled === e : t.isDisabled === e || t.isDisabled !== !e && st(t) === e : t.disabled === e : "label" in t && t.disabled === e;
      };
    }

    function pt(s) {
      return ct(function (r) {
        return r = +r, ct(function (t, e) {
          for (var n, i = s([], t.length, r), o = i.length; o--;) {
            t[n = i[o]] && (t[n] = !(e[n] = t[n]));
          }
        });
      });
    }

    function gt(t) {
      return t && void 0 !== t.getElementsByTagName && t;
    }

    for (t in f = at.support = {}, s = at.isXML = function (t) {
      var e = t.namespaceURI,
          n = (t.ownerDocument || t).documentElement;
      return !Z.test(e || n && n.nodeName || "HTML");
    }, C = at.setDocument = function (t) {
      var e,
          n,
          i = t ? t.ownerDocument || t : _;
      return i != x && 9 === i.nodeType && i.documentElement && (a = (x = i).documentElement, E = !s(x), _ != x && (n = x.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", o, !1) : n.attachEvent && n.attachEvent("onunload", o)), f.scope = ut(function (t) {
        return a.appendChild(t).appendChild(x.createElement("div")), void 0 !== t.querySelectorAll && !t.querySelectorAll(":scope fieldset div").length;
      }), f.attributes = ut(function (t) {
        return t.className = "i", !t.getAttribute("className");
      }), f.getElementsByTagName = ut(function (t) {
        return t.appendChild(x.createComment("")), !t.getElementsByTagName("*").length;
      }), f.getElementsByClassName = tt.test(x.getElementsByClassName), f.getById = ut(function (t) {
        return a.appendChild(t).id = T, !x.getElementsByName || !x.getElementsByName(T).length;
      }), f.getById ? (b.filter.ID = function (t) {
        var e = t.replace(it, h);
        return function (t) {
          return t.getAttribute("id") === e;
        };
      }, b.find.ID = function (t, e) {
        if (void 0 !== e.getElementById && E) {
          var n = e.getElementById(t);
          return n ? [n] : [];
        }
      }) : (b.filter.ID = function (t) {
        var n = t.replace(it, h);
        return function (t) {
          var e = void 0 !== t.getAttributeNode && t.getAttributeNode("id");
          return e && e.value === n;
        };
      }, b.find.ID = function (t, e) {
        if (void 0 !== e.getElementById && E) {
          var n,
              i,
              o,
              r = e.getElementById(t);

          if (r) {
            if ((n = r.getAttributeNode("id")) && n.value === t) return [r];

            for (o = e.getElementsByName(t), i = 0; r = o[i++];) {
              if ((n = r.getAttributeNode("id")) && n.value === t) return [r];
            }
          }

          return [];
        }
      }), b.find.TAG = f.getElementsByTagName ? function (t, e) {
        return void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t) : f.qsa ? e.querySelectorAll(t) : void 0;
      } : function (t, e) {
        var n,
            i = [],
            o = 0,
            r = e.getElementsByTagName(t);
        if ("*" !== t) return r;

        for (; n = r[o++];) {
          1 === n.nodeType && i.push(n);
        }

        return i;
      }, b.find.CLASS = f.getElementsByClassName && function (t, e) {
        if (void 0 !== e.getElementsByClassName && E) return e.getElementsByClassName(t);
      }, u = [], m = [], (f.qsa = tt.test(x.querySelectorAll)) && (ut(function (t) {
        var e;
        a.appendChild(t).innerHTML = "<a id='" + T + "'></a><select id='" + T + "-\r\\' msallowcapture=''><option selected=''></option></select>", t.querySelectorAll("[msallowcapture^='']").length && m.push("[*^$]=" + q + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || m.push("\\[" + q + "*(?:value|" + $ + ")"), t.querySelectorAll("[id~=" + T + "-]").length || m.push("~="), (e = x.createElement("input")).setAttribute("name", ""), t.appendChild(e), t.querySelectorAll("[name='']").length || m.push("\\[" + q + "*name" + q + "*=" + q + "*(?:''|\"\")"), t.querySelectorAll(":checked").length || m.push(":checked"), t.querySelectorAll("a#" + T + "+*").length || m.push(".#.+[+~]"), t.querySelectorAll("\\\f"), m.push("[\\r\\n\\f]");
      }), ut(function (t) {
        t.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
        var e = x.createElement("input");
        e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && m.push("name" + q + "*[*^$|!~]?="), 2 !== t.querySelectorAll(":enabled").length && m.push(":enabled", ":disabled"), a.appendChild(t).disabled = !0, 2 !== t.querySelectorAll(":disabled").length && m.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), m.push(",.*:");
      })), (f.matchesSelector = tt.test(v = a.matches || a.webkitMatchesSelector || a.mozMatchesSelector || a.oMatchesSelector || a.msMatchesSelector)) && ut(function (t) {
        f.disconnectedMatch = v.call(t, "*"), v.call(t, "[s!='']:x"), u.push("!=", F);
      }), m = m.length && new RegExp(m.join("|")), u = u.length && new RegExp(u.join("|")), e = tt.test(a.compareDocumentPosition), y = e || tt.test(a.contains) ? function (t, e) {
        var n = 9 === t.nodeType ? t.documentElement : t,
            i = e && e.parentNode;
        return t === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(i)));
      } : function (t, e) {
        if (e) for (; e = e.parentNode;) {
          if (e === t) return !0;
        }
        return !1;
      }, N = e ? function (t, e) {
        if (t === e) return c = !0, 0;
        var n = !t.compareDocumentPosition - !e.compareDocumentPosition;
        return n || (1 & (n = (t.ownerDocument || t) == (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1) || !f.sortDetached && e.compareDocumentPosition(t) === n ? t == x || t.ownerDocument == _ && y(_, t) ? -1 : e == x || e.ownerDocument == _ && y(_, e) ? 1 : l ? H(l, t) - H(l, e) : 0 : 4 & n ? -1 : 1);
      } : function (t, e) {
        if (t === e) return c = !0, 0;
        var n,
            i = 0,
            o = t.parentNode,
            r = e.parentNode,
            s = [t],
            a = [e];
        if (!o || !r) return t == x ? -1 : e == x ? 1 : o ? -1 : r ? 1 : l ? H(l, t) - H(l, e) : 0;
        if (o === r) return dt(t, e);

        for (n = t; n = n.parentNode;) {
          s.unshift(n);
        }

        for (n = e; n = n.parentNode;) {
          a.unshift(n);
        }

        for (; s[i] === a[i];) {
          i++;
        }

        return i ? dt(s[i], a[i]) : s[i] == _ ? -1 : a[i] == _ ? 1 : 0;
      }), x;
    }, at.matches = function (t, e) {
      return at(t, null, null, e);
    }, at.matchesSelector = function (t, e) {
      if (C(t), f.matchesSelector && E && !I[e + " "] && (!u || !u.test(e)) && (!m || !m.test(e))) try {
        var n = v.call(t, e);
        if (n || f.disconnectedMatch || t.document && 11 !== t.document.nodeType) return n;
      } catch (t) {
        I(e, !0);
      }
      return 0 < at(e, x, null, [t]).length;
    }, at.contains = function (t, e) {
      return (t.ownerDocument || t) != x && C(t), y(t, e);
    }, at.attr = function (t, e) {
      (t.ownerDocument || t) != x && C(t);
      var n = b.attrHandle[e.toLowerCase()],
          i = n && O.call(b.attrHandle, e.toLowerCase()) ? n(t, e, !E) : void 0;
      return void 0 !== i ? i : f.attributes || !E ? t.getAttribute(e) : (i = t.getAttributeNode(e)) && i.specified ? i.value : null;
    }, at.escape = function (t) {
      return (t + "").replace(ot, rt);
    }, at.error = function (t) {
      throw new Error("Syntax error, unrecognized expression: " + t);
    }, at.uniqueSort = function (t) {
      var e,
          n = [],
          i = 0,
          o = 0;

      if (c = !f.detectDuplicates, l = !f.sortStable && t.slice(0), t.sort(N), c) {
        for (; e = t[o++];) {
          e === t[o] && (i = n.push(o));
        }

        for (; i--;) {
          t.splice(n[i], 1);
        }
      }

      return l = null, t;
    }, r = at.getText = function (t) {
      var e,
          n = "",
          i = 0,
          o = t.nodeType;

      if (o) {
        if (1 === o || 9 === o || 11 === o) {
          if ("string" == typeof t.textContent) return t.textContent;

          for (t = t.firstChild; t; t = t.nextSibling) {
            n += r(t);
          }
        } else if (3 === o || 4 === o) return t.nodeValue;
      } else for (; e = t[i++];) {
        n += r(e);
      }

      return n;
    }, (b = at.selectors = {
      cacheLength: 50,
      createPseudo: ct,
      match: X,
      attrHandle: {},
      find: {},
      relative: {
        ">": {
          dir: "parentNode",
          first: !0
        },
        " ": {
          dir: "parentNode"
        },
        "+": {
          dir: "previousSibling",
          first: !0
        },
        "~": {
          dir: "previousSibling"
        }
      },
      preFilter: {
        ATTR: function ATTR(t) {
          return t[1] = t[1].replace(it, h), t[3] = (t[3] || t[4] || t[5] || "").replace(it, h), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4);
        },
        CHILD: function CHILD(t) {
          return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || at.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && at.error(t[0]), t;
        },
        PSEUDO: function PSEUDO(t) {
          var e,
              n = !t[6] && t[2];
          return X.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : n && V.test(n) && (e = p(n, !0)) && (e = n.indexOf(")", n.length - e) - n.length) && (t[0] = t[0].slice(0, e), t[2] = n.slice(0, e)), t.slice(0, 3));
        }
      },
      filter: {
        TAG: function TAG(t) {
          var e = t.replace(it, h).toLowerCase();
          return "*" === t ? function () {
            return !0;
          } : function (t) {
            return t.nodeName && t.nodeName.toLowerCase() === e;
          };
        },
        CLASS: function CLASS(t) {
          var e = A[t + " "];
          return e || (e = new RegExp("(^|" + q + ")" + t + "(" + q + "|$)")) && A(t, function (t) {
            return e.test("string" == typeof t.className && t.className || void 0 !== t.getAttribute && t.getAttribute("class") || "");
          });
        },
        ATTR: function ATTR(n, i, o) {
          return function (t) {
            var e = at.attr(t, n);
            return null == e ? "!=" === i : !i || (e += "", "=" === i ? e === o : "!=" === i ? e !== o : "^=" === i ? o && 0 === e.indexOf(o) : "*=" === i ? o && -1 < e.indexOf(o) : "$=" === i ? o && e.slice(-o.length) === o : "~=" === i ? -1 < (" " + e.replace(W, " ") + " ").indexOf(o) : "|=" === i && (e === o || e.slice(0, o.length + 1) === o + "-"));
          };
        },
        CHILD: function CHILD(p, t, e, g, m) {
          var v = "nth" !== p.slice(0, 3),
              y = "last" !== p.slice(-4),
              _ = "of-type" === t;

          return 1 === g && 0 === m ? function (t) {
            return !!t.parentNode;
          } : function (t, e, n) {
            var i,
                o,
                r,
                s,
                a,
                l,
                c = v != y ? "nextSibling" : "previousSibling",
                u = t.parentNode,
                h = _ && t.nodeName.toLowerCase(),
                d = !n && !_,
                f = !1;

            if (u) {
              if (v) {
                for (; c;) {
                  for (s = t; s = s[c];) {
                    if (_ ? s.nodeName.toLowerCase() === h : 1 === s.nodeType) return !1;
                  }

                  l = c = "only" === p && !l && "nextSibling";
                }

                return !0;
              }

              if (l = [y ? u.firstChild : u.lastChild], y && d) {
                for (f = (a = (i = (o = (r = (s = u)[T] || (s[T] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[p] || [])[0] === S && i[1]) && i[2], s = a && u.childNodes[a]; s = ++a && s && s[c] || (f = a = 0) || l.pop();) {
                  if (1 === s.nodeType && ++f && s === t) {
                    o[p] = [S, a, f];
                    break;
                  }
                }
              } else if (d && (f = a = (i = (o = (r = (s = t)[T] || (s[T] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[p] || [])[0] === S && i[1]), !1 === f) for (; (s = ++a && s && s[c] || (f = a = 0) || l.pop()) && ((_ ? s.nodeName.toLowerCase() !== h : 1 !== s.nodeType) || !++f || (d && ((o = (r = s[T] || (s[T] = {}))[s.uniqueID] || (r[s.uniqueID] = {}))[p] = [S, f]), s !== t));) {
                ;
              }

              return (f -= m) === g || f % g == 0 && 0 <= f / g;
            }
          };
        },
        PSEUDO: function PSEUDO(t, r) {
          var e,
              s = b.pseudos[t] || b.setFilters[t.toLowerCase()] || at.error("unsupported pseudo: " + t);
          return s[T] ? s(r) : 1 < s.length ? (e = [t, t, "", r], b.setFilters.hasOwnProperty(t.toLowerCase()) ? ct(function (t, e) {
            for (var n, i = s(t, r), o = i.length; o--;) {
              t[n = H(t, i[o])] = !(e[n] = i[o]);
            }
          }) : function (t) {
            return s(t, 0, e);
          }) : s;
        }
      },
      pseudos: {
        not: ct(function (t) {
          var i = [],
              o = [],
              a = d(t.replace(B, "$1"));
          return a[T] ? ct(function (t, e, n, i) {
            for (var o, r = a(t, null, i, []), s = t.length; s--;) {
              (o = r[s]) && (t[s] = !(e[s] = o));
            }
          }) : function (t, e, n) {
            return i[0] = t, a(i, null, n, o), i[0] = null, !o.pop();
          };
        }),
        has: ct(function (e) {
          return function (t) {
            return 0 < at(e, t).length;
          };
        }),
        contains: ct(function (e) {
          return e = e.replace(it, h), function (t) {
            return -1 < (t.textContent || r(t)).indexOf(e);
          };
        }),
        lang: ct(function (n) {
          return Y.test(n || "") || at.error("unsupported lang: " + n), n = n.replace(it, h).toLowerCase(), function (t) {
            var e;

            do {
              if (e = E ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (e = e.toLowerCase()) === n || 0 === e.indexOf(n + "-");
            } while ((t = t.parentNode) && 1 === t.nodeType);

            return !1;
          };
        }),
        target: function target(t) {
          var e = n.location && n.location.hash;
          return e && e.slice(1) === t.id;
        },
        root: function root(t) {
          return t === a;
        },
        focus: function focus(t) {
          return t === x.activeElement && (!x.hasFocus || x.hasFocus()) && !!(t.type || t.href || ~t.tabIndex);
        },
        enabled: ft(!1),
        disabled: ft(!0),
        checked: function checked(t) {
          var e = t.nodeName.toLowerCase();
          return "input" === e && !!t.checked || "option" === e && !!t.selected;
        },
        selected: function selected(t) {
          return t.parentNode && t.parentNode.selectedIndex, !0 === t.selected;
        },
        empty: function empty(t) {
          for (t = t.firstChild; t; t = t.nextSibling) {
            if (t.nodeType < 6) return !1;
          }

          return !0;
        },
        parent: function parent(t) {
          return !b.pseudos.empty(t);
        },
        header: function header(t) {
          return J.test(t.nodeName);
        },
        input: function input(t) {
          return G.test(t.nodeName);
        },
        button: function button(t) {
          var e = t.nodeName.toLowerCase();
          return "input" === e && "button" === t.type || "button" === e;
        },
        text: function text(t) {
          var e;
          return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase());
        },
        first: pt(function () {
          return [0];
        }),
        last: pt(function (t, e) {
          return [e - 1];
        }),
        eq: pt(function (t, e, n) {
          return [n < 0 ? n + e : n];
        }),
        even: pt(function (t, e) {
          for (var n = 0; n < e; n += 2) {
            t.push(n);
          }

          return t;
        }),
        odd: pt(function (t, e) {
          for (var n = 1; n < e; n += 2) {
            t.push(n);
          }

          return t;
        }),
        lt: pt(function (t, e, n) {
          for (var i = n < 0 ? n + e : e < n ? e : n; 0 <= --i;) {
            t.push(i);
          }

          return t;
        }),
        gt: pt(function (t, e, n) {
          for (var i = n < 0 ? n + e : n; ++i < e;) {
            t.push(i);
          }

          return t;
        })
      }
    }).pseudos.nth = b.pseudos.eq, {
      radio: !0,
      checkbox: !0,
      file: !0,
      password: !0,
      image: !0
    }) {
      b.pseudos[t] = function (e) {
        return function (t) {
          return "input" === t.nodeName.toLowerCase() && t.type === e;
        };
      }(t);
    }

    for (t in {
      submit: !0,
      reset: !0
    }) {
      b.pseudos[t] = function (n) {
        return function (t) {
          var e = t.nodeName.toLowerCase();
          return ("input" === e || "button" === e) && t.type === n;
        };
      }(t);
    }

    function mt() {}

    function vt(t) {
      for (var e = 0, n = t.length, i = ""; e < n; e++) {
        i += t[e].value;
      }

      return i;
    }

    function yt(a, t, e) {
      var l = t.dir,
          c = t.next,
          u = c || l,
          h = e && "parentNode" === u,
          d = i++;
      return t.first ? function (t, e, n) {
        for (; t = t[l];) {
          if (1 === t.nodeType || h) return a(t, e, n);
        }

        return !1;
      } : function (t, e, n) {
        var i,
            o,
            r,
            s = [S, d];

        if (n) {
          for (; t = t[l];) {
            if ((1 === t.nodeType || h) && a(t, e, n)) return !0;
          }
        } else for (; t = t[l];) {
          if (1 === t.nodeType || h) if (o = (r = t[T] || (t[T] = {}))[t.uniqueID] || (r[t.uniqueID] = {}), c && c === t.nodeName.toLowerCase()) t = t[l] || t;else {
            if ((i = o[u]) && i[0] === S && i[1] === d) return s[2] = i[2];
            if ((o[u] = s)[2] = a(t, e, n)) return !0;
          }
        }

        return !1;
      };
    }

    function _t(o) {
      return 1 < o.length ? function (t, e, n) {
        for (var i = o.length; i--;) {
          if (!o[i](t, e, n)) return !1;
        }

        return !0;
      } : o[0];
    }

    function bt(t, e, n, i, o) {
      for (var r, s = [], a = 0, l = t.length, c = null != e; a < l; a++) {
        (r = t[a]) && (n && !n(r, i, o) || (s.push(r), c && e.push(a)));
      }

      return s;
    }

    function wt(t) {
      for (var o, e, n, i = t.length, r = b.relative[t[0].type], s = r || b.relative[" "], a = r ? 1 : 0, l = yt(function (t) {
        return t === o;
      }, s, !0), c = yt(function (t) {
        return -1 < H(o, t);
      }, s, !0), u = [function (t, e, n) {
        var i = !r && (n || e !== w) || ((o = e).nodeType ? l : c)(t, e, n);
        return o = null, i;
      }]; a < i; a++) {
        if (e = b.relative[t[a].type]) u = [yt(_t(u), e)];else {
          if ((e = b.filter[t[a].type].apply(null, t[a].matches))[T]) {
            for (n = ++a; n < i && !b.relative[t[n].type]; n++) {
              ;
            }

            return function t(f, p, g, m, v, e) {
              return m && !m[T] && (m = t(m)), v && !v[T] && (v = t(v, e)), ct(function (t, e, n, i) {
                var o,
                    r,
                    s,
                    a = [],
                    l = [],
                    c = e.length,
                    u = t || function (t, e, n) {
                  for (var i = 0, o = e.length; i < o; i++) {
                    at(t, e[i], n);
                  }

                  return n;
                }(p || "*", n.nodeType ? [n] : n, []),
                    h = !f || !t && p ? u : bt(u, a, f, n, i),
                    d = g ? v || (t ? f : c || m) ? [] : e : h;

                if (g && g(h, d, n, i), m) for (o = bt(d, l), m(o, [], n, i), r = o.length; r--;) {
                  (s = o[r]) && (d[l[r]] = !(h[l[r]] = s));
                }

                if (t) {
                  if (v || f) {
                    if (v) {
                      for (o = [], r = d.length; r--;) {
                        (s = d[r]) && o.push(h[r] = s);
                      }

                      v(null, d = [], o, i);
                    }

                    for (r = d.length; r--;) {
                      (s = d[r]) && -1 < (o = v ? H(t, s) : a[r]) && (t[o] = !(e[o] = s));
                    }
                  }
                } else d = bt(d === e ? d.splice(c, d.length) : d), v ? v(null, e, d, i) : j.apply(e, d);
              });
            }(1 < a && _t(u), 1 < a && vt(t.slice(0, a - 1).concat({
              value: " " === t[a - 2].type ? "*" : ""
            })).replace(B, "$1"), e, a < n && wt(t.slice(a, n)), n < i && wt(t = t.slice(n)), n < i && vt(t));
          }

          u.push(e);
        }
      }

      return _t(u);
    }

    return mt.prototype = b.filters = b.pseudos, b.setFilters = new mt(), p = at.tokenize = function (t, e) {
      var n,
          i,
          o,
          r,
          s,
          a,
          l,
          c = k[t + " "];
      if (c) return e ? 0 : c.slice(0);

      for (s = t, a = [], l = b.preFilter; s;) {
        for (r in n && !(i = U.exec(s)) || (i && (s = s.slice(i[0].length) || s), a.push(o = [])), n = !1, (i = Q.exec(s)) && (n = i.shift(), o.push({
          value: n,
          type: i[0].replace(B, " ")
        }), s = s.slice(n.length)), b.filter) {
          !(i = X[r].exec(s)) || l[r] && !(i = l[r](i)) || (n = i.shift(), o.push({
            value: n,
            type: r,
            matches: i
          }), s = s.slice(n.length));
        }

        if (!n) break;
      }

      return e ? s.length : s ? at.error(t) : k(t, a).slice(0);
    }, d = at.compile = function (t, e) {
      var n,
          m,
          v,
          y,
          _,
          i,
          o = [],
          r = [],
          s = D[t + " "];

      if (!s) {
        for (n = (e = e || p(t)).length; n--;) {
          (s = wt(e[n]))[T] ? o.push(s) : r.push(s);
        }

        (s = D(t, (m = r, y = 0 < (v = o).length, _ = 0 < m.length, i = function i(t, e, n, _i2, o) {
          var r,
              s,
              a,
              l = 0,
              c = "0",
              u = t && [],
              h = [],
              d = w,
              f = t || _ && b.find.TAG("*", o),
              p = S += null == d ? 1 : Math.random() || .1,
              g = f.length;

          for (o && (w = e == x || e || o); c !== g && null != (r = f[c]); c++) {
            if (_ && r) {
              for (s = 0, e || r.ownerDocument == x || (C(r), n = !E); a = m[s++];) {
                if (a(r, e || x, n)) {
                  _i2.push(r);

                  break;
                }
              }

              o && (S = p);
            }

            y && ((r = !a && r) && l--, t && u.push(r));
          }

          if (l += c, y && c !== l) {
            for (s = 0; a = v[s++];) {
              a(u, h, e, n);
            }

            if (t) {
              if (0 < l) for (; c--;) {
                u[c] || h[c] || (h[c] = P.call(_i2));
              }
              h = bt(h);
            }

            j.apply(_i2, h), o && !t && 0 < h.length && 1 < l + v.length && at.uniqueSort(_i2);
          }

          return o && (S = p, w = d), u;
        }, y ? ct(i) : i))).selector = t;
      }

      return s;
    }, g = at.select = function (t, e, n, i) {
      var o,
          r,
          s,
          a,
          l,
          c = "function" == typeof t && t,
          u = !i && p(t = c.selector || t);

      if (n = n || [], 1 === u.length) {
        if (2 < (r = u[0] = u[0].slice(0)).length && "ID" === (s = r[0]).type && 9 === e.nodeType && E && b.relative[r[1].type]) {
          if (!(e = (b.find.ID(s.matches[0].replace(it, h), e) || [])[0])) return n;
          c && (e = e.parentNode), t = t.slice(r.shift().value.length);
        }

        for (o = X.needsContext.test(t) ? 0 : r.length; o-- && (s = r[o], !b.relative[a = s.type]);) {
          if ((l = b.find[a]) && (i = l(s.matches[0].replace(it, h), nt.test(r[0].type) && gt(e.parentNode) || e))) {
            if (r.splice(o, 1), !(t = i.length && vt(r))) return j.apply(n, i), n;
            break;
          }
        }
      }

      return (c || d(t, u))(i, e, !E, n, !e || nt.test(t) && gt(e.parentNode) || e), n;
    }, f.sortStable = T.split("").sort(N).join("") === T, f.detectDuplicates = !!c, C(), f.sortDetached = ut(function (t) {
      return 1 & t.compareDocumentPosition(x.createElement("fieldset"));
    }), ut(function (t) {
      return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href");
    }) || ht("type|href|height|width", function (t, e, n) {
      if (!n) return t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2);
    }), f.attributes && ut(function (t) {
      return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value");
    }) || ht("value", function (t, e, n) {
      if (!n && "input" === t.nodeName.toLowerCase()) return t.defaultValue;
    }), ut(function (t) {
      return null == t.getAttribute("disabled");
    }) || ht($, function (t, e, n) {
      var i;
      if (!n) return !0 === t[e] ? e.toLowerCase() : (i = t.getAttributeNode(e)) && i.specified ? i.value : null;
    }), at;
  }(x);

  T.find = f, T.expr = f.selectors, T.expr[":"] = T.expr.pseudos, T.uniqueSort = T.unique = f.uniqueSort, T.text = f.getText, T.isXMLDoc = f.isXML, T.contains = f.contains, T.escapeSelector = f.escape;

  function p(t, e, n) {
    for (var i = [], o = void 0 !== n; (t = t[e]) && 9 !== t.nodeType;) {
      if (1 === t.nodeType) {
        if (o && T(t).is(n)) break;
        i.push(t);
      }
    }

    return i;
  }

  function C(t, e) {
    for (var n = []; t; t = t.nextSibling) {
      1 === t.nodeType && t !== e && n.push(t);
    }

    return n;
  }

  var S = T.expr.match.needsContext;

  function A(t, e) {
    return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase();
  }

  var k = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

  function D(t, n, i) {
    return _(n) ? T.grep(t, function (t, e) {
      return !!n.call(t, e, t) !== i;
    }) : n.nodeType ? T.grep(t, function (t) {
      return t === n !== i;
    }) : "string" != typeof n ? T.grep(t, function (t) {
      return -1 < o.call(n, t) !== i;
    }) : T.filter(n, t, i);
  }

  T.filter = function (t, e, n) {
    var i = e[0];
    return n && (t = ":not(" + t + ")"), 1 === e.length && 1 === i.nodeType ? T.find.matchesSelector(i, t) ? [i] : [] : T.find.matches(t, T.grep(e, function (t) {
      return 1 === t.nodeType;
    }));
  }, T.fn.extend({
    find: function find(t) {
      var e,
          n,
          i = this.length,
          o = this;
      if ("string" != typeof t) return this.pushStack(T(t).filter(function () {
        for (e = 0; e < i; e++) {
          if (T.contains(o[e], this)) return !0;
        }
      }));

      for (n = this.pushStack([]), e = 0; e < i; e++) {
        T.find(t, o[e], n);
      }

      return 1 < i ? T.uniqueSort(n) : n;
    },
    filter: function filter(t) {
      return this.pushStack(D(this, t || [], !1));
    },
    not: function not(t) {
      return this.pushStack(D(this, t || [], !0));
    },
    is: function is(t) {
      return !!D(this, "string" == typeof t && S.test(t) ? T(t) : t || [], !1).length;
    }
  });
  var I,
      N = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
  (T.fn.init = function (t, e, n) {
    var i, o;
    if (!t) return this;
    if (n = n || I, "string" != typeof t) return t.nodeType ? (this[0] = t, this.length = 1, this) : _(t) ? void 0 !== n.ready ? n.ready(t) : t(T) : T.makeArray(t, this);
    if (!(i = "<" === t[0] && ">" === t[t.length - 1] && 3 <= t.length ? [null, t, null] : N.exec(t)) || !i[1] && e) return !e || e.jquery ? (e || n).find(t) : this.constructor(e).find(t);

    if (i[1]) {
      if (e = e instanceof T ? e[0] : e, T.merge(this, T.parseHTML(i[1], e && e.nodeType ? e.ownerDocument || e : E, !0)), k.test(i[1]) && T.isPlainObject(e)) for (i in e) {
        _(this[i]) ? this[i](e[i]) : this.attr(i, e[i]);
      }
      return this;
    }

    return (o = E.getElementById(i[2])) && (this[0] = o, this.length = 1), this;
  }).prototype = T.fn, I = T(E);
  var O = /^(?:parents|prev(?:Until|All))/,
      P = {
    children: !0,
    contents: !0,
    next: !0,
    prev: !0
  };

  function L(t, e) {
    for (; (t = t[e]) && 1 !== t.nodeType;) {
      ;
    }

    return t;
  }

  T.fn.extend({
    has: function has(t) {
      var e = T(t, this),
          n = e.length;
      return this.filter(function () {
        for (var t = 0; t < n; t++) {
          if (T.contains(this, e[t])) return !0;
        }
      });
    },
    closest: function closest(t, e) {
      var n,
          i = 0,
          o = this.length,
          r = [],
          s = "string" != typeof t && T(t);
      if (!S.test(t)) for (; i < o; i++) {
        for (n = this[i]; n && n !== e; n = n.parentNode) {
          if (n.nodeType < 11 && (s ? -1 < s.index(n) : 1 === n.nodeType && T.find.matchesSelector(n, t))) {
            r.push(n);
            break;
          }
        }
      }
      return this.pushStack(1 < r.length ? T.uniqueSort(r) : r);
    },
    index: function index(t) {
      return t ? "string" == typeof t ? o.call(T(t), this[0]) : o.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
    },
    add: function add(t, e) {
      return this.pushStack(T.uniqueSort(T.merge(this.get(), T(t, e))));
    },
    addBack: function addBack(t) {
      return this.add(null == t ? this.prevObject : this.prevObject.filter(t));
    }
  }), T.each({
    parent: function parent(t) {
      var e = t.parentNode;
      return e && 11 !== e.nodeType ? e : null;
    },
    parents: function parents(t) {
      return p(t, "parentNode");
    },
    parentsUntil: function parentsUntil(t, e, n) {
      return p(t, "parentNode", n);
    },
    next: function next(t) {
      return L(t, "nextSibling");
    },
    prev: function prev(t) {
      return L(t, "previousSibling");
    },
    nextAll: function nextAll(t) {
      return p(t, "nextSibling");
    },
    prevAll: function prevAll(t) {
      return p(t, "previousSibling");
    },
    nextUntil: function nextUntil(t, e, n) {
      return p(t, "nextSibling", n);
    },
    prevUntil: function prevUntil(t, e, n) {
      return p(t, "previousSibling", n);
    },
    siblings: function siblings(t) {
      return C((t.parentNode || {}).firstChild, t);
    },
    children: function children(t) {
      return C(t.firstChild);
    },
    contents: function contents(t) {
      return null != t.contentDocument && i(t.contentDocument) ? t.contentDocument : (A(t, "template") && (t = t.content || t), T.merge([], t.childNodes));
    }
  }, function (i, o) {
    T.fn[i] = function (t, e) {
      var n = T.map(this, o, t);
      return "Until" !== i.slice(-5) && (e = t), e && "string" == typeof e && (n = T.filter(e, n)), 1 < this.length && (P[i] || T.uniqueSort(n), O.test(i) && n.reverse()), this.pushStack(n);
    };
  });
  var j = /[^\x20\t\r\n\f]+/g;

  function M(t) {
    return t;
  }

  function H(t) {
    throw t;
  }

  function $(t, e, n, i) {
    var o;

    try {
      t && _(o = t.promise) ? o.call(t).done(e).fail(n) : t && _(o = t.then) ? o.call(t, e, n) : e.apply(void 0, [t].slice(i));
    } catch (t) {
      n.apply(void 0, [t]);
    }
  }

  T.Callbacks = function (i) {
    var n;
    i = "string" == typeof i ? (n = {}, T.each(i.match(j) || [], function (t, e) {
      n[e] = !0;
    }), n) : T.extend({}, i);

    function o() {
      for (s = s || i.once, e = r = !0; l.length; c = -1) {
        for (t = l.shift(); ++c < a.length;) {
          !1 === a[c].apply(t[0], t[1]) && i.stopOnFalse && (c = a.length, t = !1);
        }
      }

      i.memory || (t = !1), r = !1, s && (a = t ? [] : "");
    }

    var r,
        t,
        e,
        s,
        a = [],
        l = [],
        c = -1,
        u = {
      add: function add() {
        return a && (t && !r && (c = a.length - 1, l.push(t)), function n(t) {
          T.each(t, function (t, e) {
            _(e) ? i.unique && u.has(e) || a.push(e) : e && e.length && "string" !== w(e) && n(e);
          });
        }(arguments), t && !r && o()), this;
      },
      remove: function remove() {
        return T.each(arguments, function (t, e) {
          for (var n; -1 < (n = T.inArray(e, a, n));) {
            a.splice(n, 1), n <= c && c--;
          }
        }), this;
      },
      has: function has(t) {
        return t ? -1 < T.inArray(t, a) : 0 < a.length;
      },
      empty: function empty() {
        return a = a && [], this;
      },
      disable: function disable() {
        return s = l = [], a = t = "", this;
      },
      disabled: function disabled() {
        return !a;
      },
      lock: function lock() {
        return s = l = [], t || r || (a = t = ""), this;
      },
      locked: function locked() {
        return !!s;
      },
      fireWith: function fireWith(t, e) {
        return s || (e = [t, (e = e || []).slice ? e.slice() : e], l.push(e), r || o()), this;
      },
      fire: function fire() {
        return u.fireWith(this, arguments), this;
      },
      fired: function fired() {
        return !!e;
      }
    };
    return u;
  }, T.extend({
    Deferred: function Deferred(t) {
      var r = [["notify", "progress", T.Callbacks("memory"), T.Callbacks("memory"), 2], ["resolve", "done", T.Callbacks("once memory"), T.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", T.Callbacks("once memory"), T.Callbacks("once memory"), 1, "rejected"]],
          o = "pending",
          s = {
        state: function state() {
          return o;
        },
        always: function always() {
          return a.done(arguments).fail(arguments), this;
        },
        catch: function _catch(t) {
          return s.then(null, t);
        },
        pipe: function pipe() {
          var o = arguments;
          return T.Deferred(function (i) {
            T.each(r, function (t, e) {
              var n = _(o[e[4]]) && o[e[4]];
              a[e[1]](function () {
                var t = n && n.apply(this, arguments);
                t && _(t.promise) ? t.promise().progress(i.notify).done(i.resolve).fail(i.reject) : i[e[0] + "With"](this, n ? [t] : arguments);
              });
            }), o = null;
          }).promise();
        },
        then: function then(e, n, i) {
          var l = 0;

          function c(o, r, s, a) {
            return function () {
              function t() {
                var t, e;

                if (!(o < l)) {
                  if ((t = s.apply(n, i)) === r.promise()) throw new TypeError("Thenable self-resolution");
                  e = t && ("object" == _typeof2(t) || "function" == typeof t) && t.then, _(e) ? a ? e.call(t, c(l, r, M, a), c(l, r, H, a)) : (l++, e.call(t, c(l, r, M, a), c(l, r, H, a), c(l, r, M, r.notifyWith))) : (s !== M && (n = void 0, i = [t]), (a || r.resolveWith)(n, i));
                }
              }

              var n = this,
                  i = arguments,
                  e = a ? t : function () {
                try {
                  t();
                } catch (t) {
                  T.Deferred.exceptionHook && T.Deferred.exceptionHook(t, e.stackTrace), l <= o + 1 && (s !== H && (n = void 0, i = [t]), r.rejectWith(n, i));
                }
              };
              o ? e() : (T.Deferred.getStackHook && (e.stackTrace = T.Deferred.getStackHook()), x.setTimeout(e));
            };
          }

          return T.Deferred(function (t) {
            r[0][3].add(c(0, t, _(i) ? i : M, t.notifyWith)), r[1][3].add(c(0, t, _(e) ? e : M)), r[2][3].add(c(0, t, _(n) ? n : H));
          }).promise();
        },
        promise: function promise(t) {
          return null != t ? T.extend(t, s) : s;
        }
      },
          a = {};
      return T.each(r, function (t, e) {
        var n = e[2],
            i = e[5];
        s[e[1]] = n.add, i && n.add(function () {
          o = i;
        }, r[3 - t][2].disable, r[3 - t][3].disable, r[0][2].lock, r[0][3].lock), n.add(e[3].fire), a[e[0]] = function () {
          return a[e[0] + "With"](this === a ? void 0 : this, arguments), this;
        }, a[e[0] + "With"] = n.fireWith;
      }), s.promise(a), t && t.call(a, a), a;
    },
    when: function when(t) {
      function e(e) {
        return function (t) {
          o[e] = this, r[e] = 1 < arguments.length ? a.call(arguments) : t, --n || s.resolveWith(o, r);
        };
      }

      var n = arguments.length,
          i = n,
          o = Array(i),
          r = a.call(arguments),
          s = T.Deferred();
      if (n <= 1 && ($(t, s.done(e(i)).resolve, s.reject, !n), "pending" === s.state() || _(r[i] && r[i].then))) return s.then();

      for (; i--;) {
        $(r[i], e(i), s.reject);
      }

      return s.promise();
    }
  });
  var q = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
  T.Deferred.exceptionHook = function (t, e) {
    x.console && x.console.warn && t && q.test(t.name) && x.console.warn("jQuery.Deferred exception: " + t.message, t.stack, e);
  }, T.readyException = function (t) {
    x.setTimeout(function () {
      throw t;
    });
  };
  var z = T.Deferred();

  function R() {
    E.removeEventListener("DOMContentLoaded", R), x.removeEventListener("load", R), T.ready();
  }

  T.fn.ready = function (t) {
    return z.then(t).catch(function (t) {
      T.readyException(t);
    }), this;
  }, T.extend({
    isReady: !1,
    readyWait: 1,
    ready: function ready(t) {
      (!0 === t ? --T.readyWait : T.isReady) || (T.isReady = !0) !== t && 0 < --T.readyWait || z.resolveWith(E, [T]);
    }
  }), T.ready.then = z.then, "complete" === E.readyState || "loading" !== E.readyState && !E.documentElement.doScroll ? x.setTimeout(T.ready) : (E.addEventListener("DOMContentLoaded", R), x.addEventListener("load", R));

  var F = function F(t, e, n, i, o, r, s) {
    var a = 0,
        l = t.length,
        c = null == n;
    if ("object" === w(n)) for (a in o = !0, n) {
      F(t, e, a, n[a], !0, r, s);
    } else if (void 0 !== i && (o = !0, _(i) || (s = !0), c && (e = s ? (e.call(t, i), null) : (c = e, function (t, e, n) {
      return c.call(T(t), n);
    })), e)) for (; a < l; a++) {
      e(t[a], n, s ? i : i.call(t[a], a, e(t[a], n)));
    }
    return o ? t : c ? e.call(t) : l ? e(t[0], n) : r;
  },
      W = /^-ms-/,
      B = /-([a-z])/g;

  function U(t, e) {
    return e.toUpperCase();
  }

  function Q(t) {
    return t.replace(W, "ms-").replace(B, U);
  }

  function K(t) {
    return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType;
  }

  function V() {
    this.expando = T.expando + V.uid++;
  }

  V.uid = 1, V.prototype = {
    cache: function cache(t) {
      var e = t[this.expando];
      return e || (e = {}, K(t) && (t.nodeType ? t[this.expando] = e : Object.defineProperty(t, this.expando, {
        value: e,
        configurable: !0
      }))), e;
    },
    set: function set(t, e, n) {
      var i,
          o = this.cache(t);
      if ("string" == typeof e) o[Q(e)] = n;else for (i in e) {
        o[Q(i)] = e[i];
      }
      return o;
    },
    get: function get(t, e) {
      return void 0 === e ? this.cache(t) : t[this.expando] && t[this.expando][Q(e)];
    },
    access: function access(t, e, n) {
      return void 0 === e || e && "string" == typeof e && void 0 === n ? this.get(t, e) : (this.set(t, e, n), void 0 !== n ? n : e);
    },
    remove: function remove(t, e) {
      var n,
          i = t[this.expando];

      if (void 0 !== i) {
        if (void 0 !== e) {
          n = (e = Array.isArray(e) ? e.map(Q) : (e = Q(e)) in i ? [e] : e.match(j) || []).length;

          for (; n--;) {
            delete i[e[n]];
          }
        }

        void 0 !== e && !T.isEmptyObject(i) || (t.nodeType ? t[this.expando] = void 0 : delete t[this.expando]);
      }
    },
    hasData: function hasData(t) {
      var e = t[this.expando];
      return void 0 !== e && !T.isEmptyObject(e);
    }
  };
  var Y = new V(),
      X = new V(),
      Z = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
      G = /[A-Z]/g;

  function J(t, e, n) {
    var i, o;
    if (void 0 === n && 1 === t.nodeType) if (i = "data-" + e.replace(G, "-$&").toLowerCase(), "string" == typeof (n = t.getAttribute(i))) {
      try {
        n = "true" === (o = n) || "false" !== o && ("null" === o ? null : o === +o + "" ? +o : Z.test(o) ? JSON.parse(o) : o);
      } catch (t) {}

      X.set(t, e, n);
    } else n = void 0;
    return n;
  }

  T.extend({
    hasData: function hasData(t) {
      return X.hasData(t) || Y.hasData(t);
    },
    data: function data(t, e, n) {
      return X.access(t, e, n);
    },
    removeData: function removeData(t, e) {
      X.remove(t, e);
    },
    _data: function _data(t, e, n) {
      return Y.access(t, e, n);
    },
    _removeData: function _removeData(t, e) {
      Y.remove(t, e);
    }
  }), T.fn.extend({
    data: function data(n, t) {
      var e,
          i,
          o,
          r = this[0],
          s = r && r.attributes;
      if (void 0 !== n) return "object" == _typeof2(n) ? this.each(function () {
        X.set(this, n);
      }) : F(this, function (t) {
        var e;
        return r && void 0 === t ? void 0 !== (e = X.get(r, n)) || void 0 !== (e = J(r, n)) ? e : void 0 : void this.each(function () {
          X.set(this, n, t);
        });
      }, null, t, 1 < arguments.length, null, !0);

      if (this.length && (o = X.get(r), 1 === r.nodeType && !Y.get(r, "hasDataAttrs"))) {
        for (e = s.length; e--;) {
          s[e] && 0 === (i = s[e].name).indexOf("data-") && (i = Q(i.slice(5)), J(r, i, o[i]));
        }

        Y.set(r, "hasDataAttrs", !0);
      }

      return o;
    },
    removeData: function removeData(t) {
      return this.each(function () {
        X.remove(this, t);
      });
    }
  }), T.extend({
    queue: function queue(t, e, n) {
      var i;
      if (t) return e = (e || "fx") + "queue", i = Y.get(t, e), n && (!i || Array.isArray(n) ? i = Y.access(t, e, T.makeArray(n)) : i.push(n)), i || [];
    },
    dequeue: function dequeue(t, e) {
      e = e || "fx";

      var n = T.queue(t, e),
          i = n.length,
          o = n.shift(),
          r = T._queueHooks(t, e);

      "inprogress" === o && (o = n.shift(), i--), o && ("fx" === e && n.unshift("inprogress"), delete r.stop, o.call(t, function () {
        T.dequeue(t, e);
      }, r)), !i && r && r.empty.fire();
    },
    _queueHooks: function _queueHooks(t, e) {
      var n = e + "queueHooks";
      return Y.get(t, n) || Y.access(t, n, {
        empty: T.Callbacks("once memory").add(function () {
          Y.remove(t, [e + "queue", n]);
        })
      });
    }
  }), T.fn.extend({
    queue: function queue(e, n) {
      var t = 2;
      return "string" != typeof e && (n = e, e = "fx", t--), arguments.length < t ? T.queue(this[0], e) : void 0 === n ? this : this.each(function () {
        var t = T.queue(this, e, n);
        T._queueHooks(this, e), "fx" === e && "inprogress" !== t[0] && T.dequeue(this, e);
      });
    },
    dequeue: function dequeue(t) {
      return this.each(function () {
        T.dequeue(this, t);
      });
    },
    clearQueue: function clearQueue(t) {
      return this.queue(t || "fx", []);
    },
    promise: function promise(t, e) {
      function n() {
        --o || r.resolveWith(s, [s]);
      }

      var i,
          o = 1,
          r = T.Deferred(),
          s = this,
          a = this.length;

      for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; a--;) {
        (i = Y.get(s[a], t + "queueHooks")) && i.empty && (o++, i.empty.add(n));
      }

      return n(), r.promise(e);
    }
  });

  var tt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
      et = new RegExp("^(?:([+-])=|)(" + tt + ")([a-z%]*)$", "i"),
      nt = ["Top", "Right", "Bottom", "Left"],
      it = E.documentElement,
      ot = function ot(t) {
    return T.contains(t.ownerDocument, t);
  },
      rt = {
    composed: !0
  };

  it.getRootNode && (ot = function ot(t) {
    return T.contains(t.ownerDocument, t) || t.getRootNode(rt) === t.ownerDocument;
  });

  function st(t, e) {
    return "none" === (t = e || t).style.display || "" === t.style.display && ot(t) && "none" === T.css(t, "display");
  }

  function at(t, e, n, i) {
    var o,
        r,
        s = 20,
        a = i ? function () {
      return i.cur();
    } : function () {
      return T.css(t, e, "");
    },
        l = a(),
        c = n && n[3] || (T.cssNumber[e] ? "" : "px"),
        u = t.nodeType && (T.cssNumber[e] || "px" !== c && +l) && et.exec(T.css(t, e));

    if (u && u[3] !== c) {
      for (l /= 2, c = c || u[3], u = +l || 1; s--;) {
        T.style(t, e, u + c), (1 - r) * (1 - (r = a() / l || .5)) <= 0 && (s = 0), u /= r;
      }

      u *= 2, T.style(t, e, u + c), n = n || [];
    }

    return n && (u = +u || +l || 0, o = n[1] ? u + (n[1] + 1) * n[2] : +n[2], i && (i.unit = c, i.start = u, i.end = o)), o;
  }

  var lt = {};

  function ct(t, e) {
    for (var n, i, o, r, s, a, l = [], c = 0, u = t.length; c < u; c++) {
      (i = t[c]).style && (n = i.style.display, e ? ("none" === n && (l[c] = Y.get(i, "display") || null, l[c] || (i.style.display = "")), "" === i.style.display && st(i) && (l[c] = (a = r = o = void 0, r = i.ownerDocument, s = i.nodeName, (a = lt[s]) || (o = r.body.appendChild(r.createElement(s)), a = T.css(o, "display"), o.parentNode.removeChild(o), "none" === a && (a = "block"), lt[s] = a)))) : "none" !== n && (l[c] = "none", Y.set(i, "display", n)));
    }

    for (c = 0; c < u; c++) {
      null != l[c] && (t[c].style.display = l[c]);
    }

    return t;
  }

  T.fn.extend({
    show: function show() {
      return ct(this, !0);
    },
    hide: function hide() {
      return ct(this);
    },
    toggle: function toggle(t) {
      return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function () {
        st(this) ? T(this).show() : T(this).hide();
      });
    }
  });
  var ut,
      ht = /^(?:checkbox|radio)$/i,
      dt = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i,
      ft = /^$|^module$|\/(?:java|ecma)script/i,
      pt = E.createDocumentFragment().appendChild(E.createElement("div"));
  (ut = E.createElement("input")).setAttribute("type", "radio"), ut.setAttribute("checked", "checked"), ut.setAttribute("name", "t"), pt.appendChild(ut), y.checkClone = pt.cloneNode(!0).cloneNode(!0).lastChild.checked, pt.innerHTML = "<textarea>x</textarea>", y.noCloneChecked = !!pt.cloneNode(!0).lastChild.defaultValue, pt.innerHTML = "<option></option>", y.option = !!pt.lastChild;
  var gt = {
    thead: [1, "<table>", "</table>"],
    col: [2, "<table><colgroup>", "</colgroup></table>"],
    tr: [2, "<table><tbody>", "</tbody></table>"],
    td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
    _default: [0, "", ""]
  };

  function mt(t, e) {
    var n = void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e || "*") : void 0 !== t.querySelectorAll ? t.querySelectorAll(e || "*") : [];
    return void 0 === e || e && A(t, e) ? T.merge([t], n) : n;
  }

  function vt(t, e) {
    for (var n = 0, i = t.length; n < i; n++) {
      Y.set(t[n], "globalEval", !e || Y.get(e[n], "globalEval"));
    }
  }

  gt.tbody = gt.tfoot = gt.colgroup = gt.caption = gt.thead, gt.th = gt.td, y.option || (gt.optgroup = gt.option = [1, "<select multiple='multiple'>", "</select>"]);
  var yt = /<|&#?\w+;/;

  function _t(t, e, n, i, o) {
    for (var r, s, a, l, c, u, h = e.createDocumentFragment(), d = [], f = 0, p = t.length; f < p; f++) {
      if ((r = t[f]) || 0 === r) if ("object" === w(r)) T.merge(d, r.nodeType ? [r] : r);else if (yt.test(r)) {
        for (s = s || h.appendChild(e.createElement("div")), a = (dt.exec(r) || ["", ""])[1].toLowerCase(), l = gt[a] || gt._default, s.innerHTML = l[1] + T.htmlPrefilter(r) + l[2], u = l[0]; u--;) {
          s = s.lastChild;
        }

        T.merge(d, s.childNodes), (s = h.firstChild).textContent = "";
      } else d.push(e.createTextNode(r));
    }

    for (h.textContent = "", f = 0; r = d[f++];) {
      if (i && -1 < T.inArray(r, i)) o && o.push(r);else if (c = ot(r), s = mt(h.appendChild(r), "script"), c && vt(s), n) for (u = 0; r = s[u++];) {
        ft.test(r.type || "") && n.push(r);
      }
    }

    return h;
  }

  var bt = /^key/,
      wt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
      Ct = /^([^.]*)(?:\.(.+)|)/;

  function xt() {
    return !0;
  }

  function Et() {
    return !1;
  }

  function Tt(t, e) {
    return t === function () {
      try {
        return E.activeElement;
      } catch (t) {}
    }() == ("focus" === e);
  }

  function St(t, e, n, i, o, r) {
    var s, a;

    if ("object" == _typeof2(e)) {
      for (a in "string" != typeof n && (i = i || n, n = void 0), e) {
        St(t, a, n, i, e[a], r);
      }

      return t;
    }

    if (null == i && null == o ? (o = n, i = n = void 0) : null == o && ("string" == typeof n ? (o = i, i = void 0) : (o = i, i = n, n = void 0)), !1 === o) o = Et;else if (!o) return t;
    return 1 === r && (s = o, (o = function o(t) {
      return T().off(t), s.apply(this, arguments);
    }).guid = s.guid || (s.guid = T.guid++)), t.each(function () {
      T.event.add(this, e, o, i, n);
    });
  }

  function At(t, o, r) {
    r ? (Y.set(t, o, !1), T.event.add(t, o, {
      namespace: !1,
      handler: function handler(t) {
        var e,
            n,
            i = Y.get(this, o);

        if (1 & t.isTrigger && this[o]) {
          if (i.length) (T.event.special[o] || {}).delegateType && t.stopPropagation();else if (i = a.call(arguments), Y.set(this, o, i), e = r(this, o), this[o](), i !== (n = Y.get(this, o)) || e ? Y.set(this, o, !1) : n = {}, i !== n) return t.stopImmediatePropagation(), t.preventDefault(), n.value;
        } else i.length && (Y.set(this, o, {
          value: T.event.trigger(T.extend(i[0], T.Event.prototype), i.slice(1), this)
        }), t.stopImmediatePropagation());
      }
    })) : void 0 === Y.get(t, o) && T.event.add(t, o, xt);
  }

  T.event = {
    global: {},
    add: function add(e, t, n, i, o) {
      var r,
          s,
          a,
          l,
          c,
          u,
          h,
          d,
          f,
          p,
          g,
          m = Y.get(e);
      if (K(e)) for (n.handler && (n = (r = n).handler, o = r.selector), o && T.find.matchesSelector(it, o), n.guid || (n.guid = T.guid++), (l = m.events) || (l = m.events = Object.create(null)), (s = m.handle) || (s = m.handle = function (t) {
        return void 0 !== T && T.event.triggered !== t.type ? T.event.dispatch.apply(e, arguments) : void 0;
      }), c = (t = (t || "").match(j) || [""]).length; c--;) {
        f = g = (a = Ct.exec(t[c]) || [])[1], p = (a[2] || "").split(".").sort(), f && (h = T.event.special[f] || {}, f = (o ? h.delegateType : h.bindType) || f, h = T.event.special[f] || {}, u = T.extend({
          type: f,
          origType: g,
          data: i,
          handler: n,
          guid: n.guid,
          selector: o,
          needsContext: o && T.expr.match.needsContext.test(o),
          namespace: p.join(".")
        }, r), (d = l[f]) || ((d = l[f] = []).delegateCount = 0, h.setup && !1 !== h.setup.call(e, i, p, s) || e.addEventListener && e.addEventListener(f, s)), h.add && (h.add.call(e, u), u.handler.guid || (u.handler.guid = n.guid)), o ? d.splice(d.delegateCount++, 0, u) : d.push(u), T.event.global[f] = !0);
      }
    },
    remove: function remove(t, e, n, i, o) {
      var r,
          s,
          a,
          l,
          c,
          u,
          h,
          d,
          f,
          p,
          g,
          m = Y.hasData(t) && Y.get(t);

      if (m && (l = m.events)) {
        for (c = (e = (e || "").match(j) || [""]).length; c--;) {
          if (f = g = (a = Ct.exec(e[c]) || [])[1], p = (a[2] || "").split(".").sort(), f) {
            for (h = T.event.special[f] || {}, d = l[f = (i ? h.delegateType : h.bindType) || f] || [], a = a[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = r = d.length; r--;) {
              u = d[r], !o && g !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || i && i !== u.selector && ("**" !== i || !u.selector) || (d.splice(r, 1), u.selector && d.delegateCount--, h.remove && h.remove.call(t, u));
            }

            s && !d.length && (h.teardown && !1 !== h.teardown.call(t, p, m.handle) || T.removeEvent(t, f, m.handle), delete l[f]);
          } else for (f in l) {
            T.event.remove(t, f + e[c], n, i, !0);
          }
        }

        T.isEmptyObject(l) && Y.remove(t, "handle events");
      }
    },
    dispatch: function dispatch(t) {
      var e,
          n,
          i,
          o,
          r,
          s,
          a = new Array(arguments.length),
          l = T.event.fix(t),
          c = (Y.get(this, "events") || Object.create(null))[l.type] || [],
          u = T.event.special[l.type] || {};

      for (a[0] = l, e = 1; e < arguments.length; e++) {
        a[e] = arguments[e];
      }

      if (l.delegateTarget = this, !u.preDispatch || !1 !== u.preDispatch.call(this, l)) {
        for (s = T.event.handlers.call(this, l, c), e = 0; (o = s[e++]) && !l.isPropagationStopped();) {
          for (l.currentTarget = o.elem, n = 0; (r = o.handlers[n++]) && !l.isImmediatePropagationStopped();) {
            l.rnamespace && !1 !== r.namespace && !l.rnamespace.test(r.namespace) || (l.handleObj = r, l.data = r.data, void 0 !== (i = ((T.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, a)) && !1 === (l.result = i) && (l.preventDefault(), l.stopPropagation()));
          }
        }

        return u.postDispatch && u.postDispatch.call(this, l), l.result;
      }
    },
    handlers: function handlers(t, e) {
      var n,
          i,
          o,
          r,
          s,
          a = [],
          l = e.delegateCount,
          c = t.target;
      if (l && c.nodeType && !("click" === t.type && 1 <= t.button)) for (; c !== this; c = c.parentNode || this) {
        if (1 === c.nodeType && ("click" !== t.type || !0 !== c.disabled)) {
          for (r = [], s = {}, n = 0; n < l; n++) {
            void 0 === s[o = (i = e[n]).selector + " "] && (s[o] = i.needsContext ? -1 < T(o, this).index(c) : T.find(o, this, null, [c]).length), s[o] && r.push(i);
          }

          r.length && a.push({
            elem: c,
            handlers: r
          });
        }
      }
      return c = this, l < e.length && a.push({
        elem: c,
        handlers: e.slice(l)
      }), a;
    },
    addProp: function addProp(e, t) {
      Object.defineProperty(T.Event.prototype, e, {
        enumerable: !0,
        configurable: !0,
        get: _(t) ? function () {
          if (this.originalEvent) return t(this.originalEvent);
        } : function () {
          if (this.originalEvent) return this.originalEvent[e];
        },
        set: function set(t) {
          Object.defineProperty(this, e, {
            enumerable: !0,
            configurable: !0,
            writable: !0,
            value: t
          });
        }
      });
    },
    fix: function fix(t) {
      return t[T.expando] ? t : new T.Event(t);
    },
    special: {
      load: {
        noBubble: !0
      },
      click: {
        setup: function setup(t) {
          var e = this || t;
          return ht.test(e.type) && e.click && A(e, "input") && At(e, "click", xt), !1;
        },
        trigger: function trigger(t) {
          var e = this || t;
          return ht.test(e.type) && e.click && A(e, "input") && At(e, "click"), !0;
        },
        _default: function _default(t) {
          var e = t.target;
          return ht.test(e.type) && e.click && A(e, "input") && Y.get(e, "click") || A(e, "a");
        }
      },
      beforeunload: {
        postDispatch: function postDispatch(t) {
          void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result);
        }
      }
    }
  }, T.removeEvent = function (t, e, n) {
    t.removeEventListener && t.removeEventListener(e, n);
  }, T.Event = function (t, e) {
    if (!(this instanceof T.Event)) return new T.Event(t, e);
    t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && !1 === t.returnValue ? xt : Et, this.target = t.target && 3 === t.target.nodeType ? t.target.parentNode : t.target, this.currentTarget = t.currentTarget, this.relatedTarget = t.relatedTarget) : this.type = t, e && T.extend(this, e), this.timeStamp = t && t.timeStamp || Date.now(), this[T.expando] = !0;
  }, T.Event.prototype = {
    constructor: T.Event,
    isDefaultPrevented: Et,
    isPropagationStopped: Et,
    isImmediatePropagationStopped: Et,
    isSimulated: !1,
    preventDefault: function preventDefault() {
      var t = this.originalEvent;
      this.isDefaultPrevented = xt, t && !this.isSimulated && t.preventDefault();
    },
    stopPropagation: function stopPropagation() {
      var t = this.originalEvent;
      this.isPropagationStopped = xt, t && !this.isSimulated && t.stopPropagation();
    },
    stopImmediatePropagation: function stopImmediatePropagation() {
      var t = this.originalEvent;
      this.isImmediatePropagationStopped = xt, t && !this.isSimulated && t.stopImmediatePropagation(), this.stopPropagation();
    }
  }, T.each({
    altKey: !0,
    bubbles: !0,
    cancelable: !0,
    changedTouches: !0,
    ctrlKey: !0,
    detail: !0,
    eventPhase: !0,
    metaKey: !0,
    pageX: !0,
    pageY: !0,
    shiftKey: !0,
    view: !0,
    char: !0,
    code: !0,
    charCode: !0,
    key: !0,
    keyCode: !0,
    button: !0,
    buttons: !0,
    clientX: !0,
    clientY: !0,
    offsetX: !0,
    offsetY: !0,
    pointerId: !0,
    pointerType: !0,
    screenX: !0,
    screenY: !0,
    targetTouches: !0,
    toElement: !0,
    touches: !0,
    which: function which(t) {
      var e = t.button;
      return null == t.which && bt.test(t.type) ? null != t.charCode ? t.charCode : t.keyCode : !t.which && void 0 !== e && wt.test(t.type) ? 1 & e ? 1 : 2 & e ? 3 : 4 & e ? 2 : 0 : t.which;
    }
  }, T.event.addProp), T.each({
    focus: "focusin",
    blur: "focusout"
  }, function (t, e) {
    T.event.special[t] = {
      setup: function setup() {
        return At(this, t, Tt), !1;
      },
      trigger: function trigger() {
        return At(this, t), !0;
      },
      delegateType: e
    };
  }), T.each({
    mouseenter: "mouseover",
    mouseleave: "mouseout",
    pointerenter: "pointerover",
    pointerleave: "pointerout"
  }, function (t, o) {
    T.event.special[t] = {
      delegateType: o,
      bindType: o,
      handle: function handle(t) {
        var e,
            n = t.relatedTarget,
            i = t.handleObj;
        return n && (n === this || T.contains(this, n)) || (t.type = i.origType, e = i.handler.apply(this, arguments), t.type = o), e;
      }
    };
  }), T.fn.extend({
    on: function on(t, e, n, i) {
      return St(this, t, e, n, i);
    },
    one: function one(t, e, n, i) {
      return St(this, t, e, n, i, 1);
    },
    off: function off(t, e, n) {
      var i, o;
      if (t && t.preventDefault && t.handleObj) return i = t.handleObj, T(t.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
      if ("object" != _typeof2(t)) return !1 !== e && "function" != typeof e || (n = e, e = void 0), !1 === n && (n = Et), this.each(function () {
        T.event.remove(this, t, n, e);
      });

      for (o in t) {
        this.off(o, e, t[o]);
      }

      return this;
    }
  });
  var kt = /<script|<style|<link/i,
      Dt = /checked\s*(?:[^=]|=\s*.checked.)/i,
      It = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

  function Nt(t, e) {
    return A(t, "table") && A(11 !== e.nodeType ? e : e.firstChild, "tr") && T(t).children("tbody")[0] || t;
  }

  function Ot(t) {
    return t.type = (null !== t.getAttribute("type")) + "/" + t.type, t;
  }

  function Pt(t) {
    return "true/" === (t.type || "").slice(0, 5) ? t.type = t.type.slice(5) : t.removeAttribute("type"), t;
  }

  function Lt(t, e) {
    var n, i, o, r, s, a;

    if (1 === e.nodeType) {
      if (Y.hasData(t) && (a = Y.get(t).events)) for (o in Y.remove(e, "handle events"), a) {
        for (n = 0, i = a[o].length; n < i; n++) {
          T.event.add(e, o, a[o][n]);
        }
      }
      X.hasData(t) && (r = X.access(t), s = T.extend({}, r), X.set(e, s));
    }
  }

  function jt(n, i, o, r) {
    i = m(i);

    var t,
        e,
        s,
        a,
        l,
        c,
        u = 0,
        h = n.length,
        d = h - 1,
        f = i[0],
        p = _(f);

    if (p || 1 < h && "string" == typeof f && !y.checkClone && Dt.test(f)) return n.each(function (t) {
      var e = n.eq(t);
      p && (i[0] = f.call(this, t, e.html())), jt(e, i, o, r);
    });

    if (h && (e = (t = _t(i, n[0].ownerDocument, !1, n, r)).firstChild, 1 === t.childNodes.length && (t = e), e || r)) {
      for (a = (s = T.map(mt(t, "script"), Ot)).length; u < h; u++) {
        l = t, u !== d && (l = T.clone(l, !0, !0), a && T.merge(s, mt(l, "script"))), o.call(n[u], l, u);
      }

      if (a) for (c = s[s.length - 1].ownerDocument, T.map(s, Pt), u = 0; u < a; u++) {
        l = s[u], ft.test(l.type || "") && !Y.access(l, "globalEval") && T.contains(c, l) && (l.src && "module" !== (l.type || "").toLowerCase() ? T._evalUrl && !l.noModule && T._evalUrl(l.src, {
          nonce: l.nonce || l.getAttribute("nonce")
        }, c) : b(l.textContent.replace(It, ""), l, c));
      }
    }

    return n;
  }

  function Mt(t, e, n) {
    for (var i, o = e ? T.filter(e, t) : t, r = 0; null != (i = o[r]); r++) {
      n || 1 !== i.nodeType || T.cleanData(mt(i)), i.parentNode && (n && ot(i) && vt(mt(i, "script")), i.parentNode.removeChild(i));
    }

    return t;
  }

  T.extend({
    htmlPrefilter: function htmlPrefilter(t) {
      return t;
    },
    clone: function clone(t, e, n) {
      var i,
          o,
          r,
          s,
          a,
          l,
          c,
          u = t.cloneNode(!0),
          h = ot(t);
      if (!(y.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || T.isXMLDoc(t))) for (s = mt(u), i = 0, o = (r = mt(t)).length; i < o; i++) {
        a = r[i], "input" === (c = (l = s[i]).nodeName.toLowerCase()) && ht.test(a.type) ? l.checked = a.checked : "input" !== c && "textarea" !== c || (l.defaultValue = a.defaultValue);
      }
      if (e) if (n) for (r = r || mt(t), s = s || mt(u), i = 0, o = r.length; i < o; i++) {
        Lt(r[i], s[i]);
      } else Lt(t, u);
      return 0 < (s = mt(u, "script")).length && vt(s, !h && mt(t, "script")), u;
    },
    cleanData: function cleanData(t) {
      for (var e, n, i, o = T.event.special, r = 0; void 0 !== (n = t[r]); r++) {
        if (K(n)) {
          if (e = n[Y.expando]) {
            if (e.events) for (i in e.events) {
              o[i] ? T.event.remove(n, i) : T.removeEvent(n, i, e.handle);
            }
            n[Y.expando] = void 0;
          }

          n[X.expando] && (n[X.expando] = void 0);
        }
      }
    }
  }), T.fn.extend({
    detach: function detach(t) {
      return Mt(this, t, !0);
    },
    remove: function remove(t) {
      return Mt(this, t);
    },
    text: function text(t) {
      return F(this, function (t) {
        return void 0 === t ? T.text(this) : this.empty().each(function () {
          1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = t);
        });
      }, null, t, arguments.length);
    },
    append: function append() {
      return jt(this, arguments, function (t) {
        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || Nt(this, t).appendChild(t);
      });
    },
    prepend: function prepend() {
      return jt(this, arguments, function (t) {
        var e;
        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (e = Nt(this, t)).insertBefore(t, e.firstChild);
      });
    },
    before: function before() {
      return jt(this, arguments, function (t) {
        this.parentNode && this.parentNode.insertBefore(t, this);
      });
    },
    after: function after() {
      return jt(this, arguments, function (t) {
        this.parentNode && this.parentNode.insertBefore(t, this.nextSibling);
      });
    },
    empty: function empty() {
      for (var t, e = 0; null != (t = this[e]); e++) {
        1 === t.nodeType && (T.cleanData(mt(t, !1)), t.textContent = "");
      }

      return this;
    },
    clone: function clone(t, e) {
      return t = null != t && t, e = null == e ? t : e, this.map(function () {
        return T.clone(this, t, e);
      });
    },
    html: function html(t) {
      return F(this, function (t) {
        var e = this[0] || {},
            n = 0,
            i = this.length;
        if (void 0 === t && 1 === e.nodeType) return e.innerHTML;

        if ("string" == typeof t && !kt.test(t) && !gt[(dt.exec(t) || ["", ""])[1].toLowerCase()]) {
          t = T.htmlPrefilter(t);

          try {
            for (; n < i; n++) {
              1 === (e = this[n] || {}).nodeType && (T.cleanData(mt(e, !1)), e.innerHTML = t);
            }

            e = 0;
          } catch (t) {}
        }

        e && this.empty().append(t);
      }, null, t, arguments.length);
    },
    replaceWith: function replaceWith() {
      var n = [];
      return jt(this, arguments, function (t) {
        var e = this.parentNode;
        T.inArray(this, n) < 0 && (T.cleanData(mt(this)), e && e.replaceChild(t, this));
      }, n);
    }
  }), T.each({
    appendTo: "append",
    prependTo: "prepend",
    insertBefore: "before",
    insertAfter: "after",
    replaceAll: "replaceWith"
  }, function (t, s) {
    T.fn[t] = function (t) {
      for (var e, n = [], i = T(t), o = i.length - 1, r = 0; r <= o; r++) {
        e = r === o ? this : this.clone(!0), T(i[r])[s](e), l.apply(n, e.get());
      }

      return this.pushStack(n);
    };
  });

  function Ht(t, e, n) {
    var i,
        o,
        r = {};

    for (o in e) {
      r[o] = t.style[o], t.style[o] = e[o];
    }

    for (o in i = n.call(t), e) {
      t.style[o] = r[o];
    }

    return i;
  }

  var $t,
      qt,
      zt,
      Rt,
      Ft,
      Wt,
      Bt,
      Ut,
      Qt = new RegExp("^(" + tt + ")(?!px)[a-z%]+$", "i"),
      Kt = function Kt(t) {
    var e = t.ownerDocument.defaultView;
    return e && e.opener || (e = x), e.getComputedStyle(t);
  },
      Vt = new RegExp(nt.join("|"), "i");

  function Yt(t, e, n) {
    var i,
        o,
        r,
        s,
        a = t.style;
    return (n = n || Kt(t)) && ("" !== (s = n.getPropertyValue(e) || n[e]) || ot(t) || (s = T.style(t, e)), !y.pixelBoxStyles() && Qt.test(s) && Vt.test(e) && (i = a.width, o = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = o, a.maxWidth = r)), void 0 !== s ? s + "" : s;
  }

  function Xt(t, e) {
    return {
      get: function get() {
        if (!t()) return (this.get = e).apply(this, arguments);
        delete this.get;
      }
    };
  }

  function Zt() {
    var t;
    Ut && (Bt.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", Ut.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", it.appendChild(Bt).appendChild(Ut), t = x.getComputedStyle(Ut), $t = "1%" !== t.top, Wt = 12 === Gt(t.marginLeft), Ut.style.right = "60%", Rt = 36 === Gt(t.right), qt = 36 === Gt(t.width), Ut.style.position = "absolute", zt = 12 === Gt(Ut.offsetWidth / 3), it.removeChild(Bt), Ut = null);
  }

  function Gt(t) {
    return Math.round(parseFloat(t));
  }

  Bt = E.createElement("div"), (Ut = E.createElement("div")).style && (Ut.style.backgroundClip = "content-box", Ut.cloneNode(!0).style.backgroundClip = "", y.clearCloneStyle = "content-box" === Ut.style.backgroundClip, T.extend(y, {
    boxSizingReliable: function boxSizingReliable() {
      return Zt(), qt;
    },
    pixelBoxStyles: function pixelBoxStyles() {
      return Zt(), Rt;
    },
    pixelPosition: function pixelPosition() {
      return Zt(), $t;
    },
    reliableMarginLeft: function reliableMarginLeft() {
      return Zt(), Wt;
    },
    scrollboxSize: function scrollboxSize() {
      return Zt(), zt;
    },
    reliableTrDimensions: function reliableTrDimensions() {
      var t, e, n, i;
      return null == Ft && (t = E.createElement("table"), e = E.createElement("tr"), n = E.createElement("div"), t.style.cssText = "position:absolute;left:-11111px", e.style.height = "1px", n.style.height = "9px", it.appendChild(t).appendChild(e).appendChild(n), i = x.getComputedStyle(e), Ft = 3 < parseInt(i.height), it.removeChild(t)), Ft;
    }
  }));
  var Jt = ["Webkit", "Moz", "ms"],
      te = E.createElement("div").style,
      ee = {};

  function ne(t) {
    return T.cssProps[t] || ee[t] || (t in te ? t : ee[t] = function (t) {
      for (var e = t[0].toUpperCase() + t.slice(1), n = Jt.length; n--;) {
        if ((t = Jt[n] + e) in te) return t;
      }
    }(t) || t);
  }

  var ie = /^(none|table(?!-c[ea]).+)/,
      oe = /^--/,
      re = {
    position: "absolute",
    visibility: "hidden",
    display: "block"
  },
      se = {
    letterSpacing: "0",
    fontWeight: "400"
  };

  function ae(t, e, n) {
    var i = et.exec(e);
    return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : e;
  }

  function le(t, e, n, i, o, r) {
    var s = "width" === e ? 1 : 0,
        a = 0,
        l = 0;
    if (n === (i ? "border" : "content")) return 0;

    for (; s < 4; s += 2) {
      "margin" === n && (l += T.css(t, n + nt[s], !0, o)), i ? ("content" === n && (l -= T.css(t, "padding" + nt[s], !0, o)), "margin" !== n && (l -= T.css(t, "border" + nt[s] + "Width", !0, o))) : (l += T.css(t, "padding" + nt[s], !0, o), "padding" !== n ? l += T.css(t, "border" + nt[s] + "Width", !0, o) : a += T.css(t, "border" + nt[s] + "Width", !0, o));
    }

    return !i && 0 <= r && (l += Math.max(0, Math.ceil(t["offset" + e[0].toUpperCase() + e.slice(1)] - r - l - a - .5)) || 0), l;
  }

  function ce(t, e, n) {
    var i = Kt(t),
        o = (!y.boxSizingReliable() || n) && "border-box" === T.css(t, "boxSizing", !1, i),
        r = o,
        s = Yt(t, e, i),
        a = "offset" + e[0].toUpperCase() + e.slice(1);

    if (Qt.test(s)) {
      if (!n) return s;
      s = "auto";
    }

    return (!y.boxSizingReliable() && o || !y.reliableTrDimensions() && A(t, "tr") || "auto" === s || !parseFloat(s) && "inline" === T.css(t, "display", !1, i)) && t.getClientRects().length && (o = "border-box" === T.css(t, "boxSizing", !1, i), (r = a in t) && (s = t[a])), (s = parseFloat(s) || 0) + le(t, e, n || (o ? "border" : "content"), r, i, s) + "px";
  }

  function ue(t, e, n, i, o) {
    return new ue.prototype.init(t, e, n, i, o);
  }

  T.extend({
    cssHooks: {
      opacity: {
        get: function get(t, e) {
          if (e) {
            var n = Yt(t, "opacity");
            return "" === n ? "1" : n;
          }
        }
      }
    },
    cssNumber: {
      animationIterationCount: !0,
      columnCount: !0,
      fillOpacity: !0,
      flexGrow: !0,
      flexShrink: !0,
      fontWeight: !0,
      gridArea: !0,
      gridColumn: !0,
      gridColumnEnd: !0,
      gridColumnStart: !0,
      gridRow: !0,
      gridRowEnd: !0,
      gridRowStart: !0,
      lineHeight: !0,
      opacity: !0,
      order: !0,
      orphans: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0
    },
    cssProps: {},
    style: function style(t, e, n, i) {
      if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
        var o,
            r,
            s,
            a = Q(e),
            l = oe.test(e),
            c = t.style;
        if (l || (e = ne(a)), s = T.cssHooks[e] || T.cssHooks[a], void 0 === n) return s && "get" in s && void 0 !== (o = s.get(t, !1, i)) ? o : c[e];
        "string" == (r = _typeof2(n)) && (o = et.exec(n)) && o[1] && (n = at(t, e, o), r = "number"), null != n && n == n && ("number" !== r || l || (n += o && o[3] || (T.cssNumber[a] ? "" : "px")), y.clearCloneStyle || "" !== n || 0 !== e.indexOf("background") || (c[e] = "inherit"), s && "set" in s && void 0 === (n = s.set(t, n, i)) || (l ? c.setProperty(e, n) : c[e] = n));
      }
    },
    css: function css(t, e, n, i) {
      var o,
          r,
          s,
          a = Q(e);
      return oe.test(e) || (e = ne(a)), (s = T.cssHooks[e] || T.cssHooks[a]) && "get" in s && (o = s.get(t, !0, n)), void 0 === o && (o = Yt(t, e, i)), "normal" === o && e in se && (o = se[e]), "" === n || n ? (r = parseFloat(o), !0 === n || isFinite(r) ? r || 0 : o) : o;
    }
  }), T.each(["height", "width"], function (t, l) {
    T.cssHooks[l] = {
      get: function get(t, e, n) {
        if (e) return !ie.test(T.css(t, "display")) || t.getClientRects().length && t.getBoundingClientRect().width ? ce(t, l, n) : Ht(t, re, function () {
          return ce(t, l, n);
        });
      },
      set: function set(t, e, n) {
        var i,
            o = Kt(t),
            r = !y.scrollboxSize() && "absolute" === o.position,
            s = (r || n) && "border-box" === T.css(t, "boxSizing", !1, o),
            a = n ? le(t, l, n, s, o) : 0;
        return s && r && (a -= Math.ceil(t["offset" + l[0].toUpperCase() + l.slice(1)] - parseFloat(o[l]) - le(t, l, "border", !1, o) - .5)), a && (i = et.exec(e)) && "px" !== (i[3] || "px") && (t.style[l] = e, e = T.css(t, l)), ae(0, e, a);
      }
    };
  }), T.cssHooks.marginLeft = Xt(y.reliableMarginLeft, function (t, e) {
    if (e) return (parseFloat(Yt(t, "marginLeft")) || t.getBoundingClientRect().left - Ht(t, {
      marginLeft: 0
    }, function () {
      return t.getBoundingClientRect().left;
    })) + "px";
  }), T.each({
    margin: "",
    padding: "",
    border: "Width"
  }, function (o, r) {
    T.cssHooks[o + r] = {
      expand: function expand(t) {
        for (var e = 0, n = {}, i = "string" == typeof t ? t.split(" ") : [t]; e < 4; e++) {
          n[o + nt[e] + r] = i[e] || i[e - 2] || i[0];
        }

        return n;
      }
    }, "margin" !== o && (T.cssHooks[o + r].set = ae);
  }), T.fn.extend({
    css: function css(t, e) {
      return F(this, function (t, e, n) {
        var i,
            o,
            r = {},
            s = 0;

        if (Array.isArray(e)) {
          for (i = Kt(t), o = e.length; s < o; s++) {
            r[e[s]] = T.css(t, e[s], !1, i);
          }

          return r;
        }

        return void 0 !== n ? T.style(t, e, n) : T.css(t, e);
      }, t, e, 1 < arguments.length);
    }
  }), ((T.Tween = ue).prototype = {
    constructor: ue,
    init: function init(t, e, n, i, o, r) {
      this.elem = t, this.prop = n, this.easing = o || T.easing._default, this.options = e, this.start = this.now = this.cur(), this.end = i, this.unit = r || (T.cssNumber[n] ? "" : "px");
    },
    cur: function cur() {
      var t = ue.propHooks[this.prop];
      return t && t.get ? t.get(this) : ue.propHooks._default.get(this);
    },
    run: function run(t) {
      var e,
          n = ue.propHooks[this.prop];
      return this.options.duration ? this.pos = e = T.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : ue.propHooks._default.set(this), this;
    }
  }).init.prototype = ue.prototype, (ue.propHooks = {
    _default: {
      get: function get(t) {
        var e;
        return 1 !== t.elem.nodeType || null != t.elem[t.prop] && null == t.elem.style[t.prop] ? t.elem[t.prop] : (e = T.css(t.elem, t.prop, "")) && "auto" !== e ? e : 0;
      },
      set: function set(t) {
        T.fx.step[t.prop] ? T.fx.step[t.prop](t) : 1 !== t.elem.nodeType || !T.cssHooks[t.prop] && null == t.elem.style[ne(t.prop)] ? t.elem[t.prop] = t.now : T.style(t.elem, t.prop, t.now + t.unit);
      }
    }
  }).scrollTop = ue.propHooks.scrollLeft = {
    set: function set(t) {
      t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now);
    }
  }, T.easing = {
    linear: function linear(t) {
      return t;
    },
    swing: function swing(t) {
      return .5 - Math.cos(t * Math.PI) / 2;
    },
    _default: "swing"
  }, T.fx = ue.prototype.init, T.fx.step = {};
  var he,
      de,
      fe,
      pe,
      ge = /^(?:toggle|show|hide)$/,
      me = /queueHooks$/;

  function ve() {
    de && (!1 === E.hidden && x.requestAnimationFrame ? x.requestAnimationFrame(ve) : x.setTimeout(ve, T.fx.interval), T.fx.tick());
  }

  function ye() {
    return x.setTimeout(function () {
      he = void 0;
    }), he = Date.now();
  }

  function _e(t, e) {
    var n,
        i = 0,
        o = {
      height: t
    };

    for (e = e ? 1 : 0; i < 4; i += 2 - e) {
      o["margin" + (n = nt[i])] = o["padding" + n] = t;
    }

    return e && (o.opacity = o.width = t), o;
  }

  function be(t, e, n) {
    for (var i, o = (we.tweeners[e] || []).concat(we.tweeners["*"]), r = 0, s = o.length; r < s; r++) {
      if (i = o[r].call(n, e, t)) return i;
    }
  }

  function we(r, t, e) {
    var n,
        s,
        i = 0,
        o = we.prefilters.length,
        a = T.Deferred().always(function () {
      delete l.elem;
    }),
        l = function l() {
      if (s) return !1;

      for (var t = he || ye(), e = Math.max(0, c.startTime + c.duration - t), n = 1 - (e / c.duration || 0), i = 0, o = c.tweens.length; i < o; i++) {
        c.tweens[i].run(n);
      }

      return a.notifyWith(r, [c, n, e]), n < 1 && o ? e : (o || a.notifyWith(r, [c, 1, 0]), a.resolveWith(r, [c]), !1);
    },
        c = a.promise({
      elem: r,
      props: T.extend({}, t),
      opts: T.extend(!0, {
        specialEasing: {},
        easing: T.easing._default
      }, e),
      originalProperties: t,
      originalOptions: e,
      startTime: he || ye(),
      duration: e.duration,
      tweens: [],
      createTween: function createTween(t, e) {
        var n = T.Tween(r, c.opts, t, e, c.opts.specialEasing[t] || c.opts.easing);
        return c.tweens.push(n), n;
      },
      stop: function stop(t) {
        var e = 0,
            n = t ? c.tweens.length : 0;
        if (s) return this;

        for (s = !0; e < n; e++) {
          c.tweens[e].run(1);
        }

        return t ? (a.notifyWith(r, [c, 1, 0]), a.resolveWith(r, [c, t])) : a.rejectWith(r, [c, t]), this;
      }
    }),
        u = c.props;

    for (function (t, e) {
      var n, i, o, r, s;

      for (n in t) {
        if (o = e[i = Q(n)], r = t[n], Array.isArray(r) && (o = r[1], r = t[n] = r[0]), n !== i && (t[i] = r, delete t[n]), (s = T.cssHooks[i]) && ("expand" in s)) for (n in r = s.expand(r), delete t[i], r) {
          (n in t) || (t[n] = r[n], e[n] = o);
        } else e[i] = o;
      }
    }(u, c.opts.specialEasing); i < o; i++) {
      if (n = we.prefilters[i].call(c, r, u, c.opts)) return _(n.stop) && (T._queueHooks(c.elem, c.opts.queue).stop = n.stop.bind(n)), n;
    }

    return T.map(u, be, c), _(c.opts.start) && c.opts.start.call(r, c), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always), T.fx.timer(T.extend(l, {
      elem: r,
      anim: c,
      queue: c.opts.queue
    })), c;
  }

  T.Animation = T.extend(we, {
    tweeners: {
      "*": [function (t, e) {
        var n = this.createTween(t, e);
        return at(n.elem, t, et.exec(e), n), n;
      }]
    },
    tweener: function tweener(t, e) {
      for (var n, i = 0, o = (t = _(t) ? (e = t, ["*"]) : t.match(j)).length; i < o; i++) {
        n = t[i], we.tweeners[n] = we.tweeners[n] || [], we.tweeners[n].unshift(e);
      }
    },
    prefilters: [function (t, e, n) {
      var i,
          o,
          r,
          s,
          a,
          l,
          c,
          u,
          h = "width" in e || "height" in e,
          d = this,
          f = {},
          p = t.style,
          g = t.nodeType && st(t),
          m = Y.get(t, "fxshow");

      for (i in n.queue || (null == (s = T._queueHooks(t, "fx")).unqueued && (s.unqueued = 0, a = s.empty.fire, s.empty.fire = function () {
        s.unqueued || a();
      }), s.unqueued++, d.always(function () {
        d.always(function () {
          s.unqueued--, T.queue(t, "fx").length || s.empty.fire();
        });
      })), e) {
        if (o = e[i], ge.test(o)) {
          if (delete e[i], r = r || "toggle" === o, o === (g ? "hide" : "show")) {
            if ("show" !== o || !m || void 0 === m[i]) continue;
            g = !0;
          }

          f[i] = m && m[i] || T.style(t, i);
        }
      }

      if ((l = !T.isEmptyObject(e)) || !T.isEmptyObject(f)) for (i in h && 1 === t.nodeType && (n.overflow = [p.overflow, p.overflowX, p.overflowY], null == (c = m && m.display) && (c = Y.get(t, "display")), "none" === (u = T.css(t, "display")) && (c ? u = c : (ct([t], !0), c = t.style.display || c, u = T.css(t, "display"), ct([t]))), ("inline" === u || "inline-block" === u && null != c) && "none" === T.css(t, "float") && (l || (d.done(function () {
        p.display = c;
      }), null == c && (u = p.display, c = "none" === u ? "" : u)), p.display = "inline-block")), n.overflow && (p.overflow = "hidden", d.always(function () {
        p.overflow = n.overflow[0], p.overflowX = n.overflow[1], p.overflowY = n.overflow[2];
      })), l = !1, f) {
        l || (m ? "hidden" in m && (g = m.hidden) : m = Y.access(t, "fxshow", {
          display: c
        }), r && (m.hidden = !g), g && ct([t], !0), d.done(function () {
          for (i in g || ct([t]), Y.remove(t, "fxshow"), f) {
            T.style(t, i, f[i]);
          }
        })), l = be(g ? m[i] : 0, i, d), i in m || (m[i] = l.start, g && (l.end = l.start, l.start = 0));
      }
    }],
    prefilter: function prefilter(t, e) {
      e ? we.prefilters.unshift(t) : we.prefilters.push(t);
    }
  }), T.speed = function (t, e, n) {
    var i = t && "object" == _typeof2(t) ? T.extend({}, t) : {
      complete: n || !n && e || _(t) && t,
      duration: t,
      easing: n && e || e && !_(e) && e
    };
    return T.fx.off ? i.duration = 0 : "number" != typeof i.duration && (i.duration in T.fx.speeds ? i.duration = T.fx.speeds[i.duration] : i.duration = T.fx.speeds._default), null != i.queue && !0 !== i.queue || (i.queue = "fx"), i.old = i.complete, i.complete = function () {
      _(i.old) && i.old.call(this), i.queue && T.dequeue(this, i.queue);
    }, i;
  }, T.fn.extend({
    fadeTo: function fadeTo(t, e, n, i) {
      return this.filter(st).css("opacity", 0).show().end().animate({
        opacity: e
      }, t, n, i);
    },
    animate: function animate(e, t, n, i) {
      function o() {
        var t = we(this, T.extend({}, e), s);
        (r || Y.get(this, "finish")) && t.stop(!0);
      }

      var r = T.isEmptyObject(e),
          s = T.speed(t, n, i);
      return o.finish = o, r || !1 === s.queue ? this.each(o) : this.queue(s.queue, o);
    },
    stop: function stop(o, t, r) {
      function s(t) {
        var e = t.stop;
        delete t.stop, e(r);
      }

      return "string" != typeof o && (r = t, t = o, o = void 0), t && this.queue(o || "fx", []), this.each(function () {
        var t = !0,
            e = null != o && o + "queueHooks",
            n = T.timers,
            i = Y.get(this);
        if (e) i[e] && i[e].stop && s(i[e]);else for (e in i) {
          i[e] && i[e].stop && me.test(e) && s(i[e]);
        }

        for (e = n.length; e--;) {
          n[e].elem !== this || null != o && n[e].queue !== o || (n[e].anim.stop(r), t = !1, n.splice(e, 1));
        }

        !t && r || T.dequeue(this, o);
      });
    },
    finish: function finish(s) {
      return !1 !== s && (s = s || "fx"), this.each(function () {
        var t,
            e = Y.get(this),
            n = e[s + "queue"],
            i = e[s + "queueHooks"],
            o = T.timers,
            r = n ? n.length : 0;

        for (e.finish = !0, T.queue(this, s, []), i && i.stop && i.stop.call(this, !0), t = o.length; t--;) {
          o[t].elem === this && o[t].queue === s && (o[t].anim.stop(!0), o.splice(t, 1));
        }

        for (t = 0; t < r; t++) {
          n[t] && n[t].finish && n[t].finish.call(this);
        }

        delete e.finish;
      });
    }
  }), T.each(["toggle", "show", "hide"], function (t, i) {
    var o = T.fn[i];

    T.fn[i] = function (t, e, n) {
      return null == t || "boolean" == typeof t ? o.apply(this, arguments) : this.animate(_e(i, !0), t, e, n);
    };
  }), T.each({
    slideDown: _e("show"),
    slideUp: _e("hide"),
    slideToggle: _e("toggle"),
    fadeIn: {
      opacity: "show"
    },
    fadeOut: {
      opacity: "hide"
    },
    fadeToggle: {
      opacity: "toggle"
    }
  }, function (t, i) {
    T.fn[t] = function (t, e, n) {
      return this.animate(i, t, e, n);
    };
  }), T.timers = [], T.fx.tick = function () {
    var t,
        e = 0,
        n = T.timers;

    for (he = Date.now(); e < n.length; e++) {
      (t = n[e])() || n[e] !== t || n.splice(e--, 1);
    }

    n.length || T.fx.stop(), he = void 0;
  }, T.fx.timer = function (t) {
    T.timers.push(t), T.fx.start();
  }, T.fx.interval = 13, T.fx.start = function () {
    de || (de = !0, ve());
  }, T.fx.stop = function () {
    de = null;
  }, T.fx.speeds = {
    slow: 600,
    fast: 200,
    _default: 400
  }, T.fn.delay = function (i, t) {
    return i = T.fx && T.fx.speeds[i] || i, t = t || "fx", this.queue(t, function (t, e) {
      var n = x.setTimeout(t, i);

      e.stop = function () {
        x.clearTimeout(n);
      };
    });
  }, fe = E.createElement("input"), pe = E.createElement("select").appendChild(E.createElement("option")), fe.type = "checkbox", y.checkOn = "" !== fe.value, y.optSelected = pe.selected, (fe = E.createElement("input")).value = "t", fe.type = "radio", y.radioValue = "t" === fe.value;
  var Ce,
      xe = T.expr.attrHandle;
  T.fn.extend({
    attr: function attr(t, e) {
      return F(this, T.attr, t, e, 1 < arguments.length);
    },
    removeAttr: function removeAttr(t) {
      return this.each(function () {
        T.removeAttr(this, t);
      });
    }
  }), T.extend({
    attr: function attr(t, e, n) {
      var i,
          o,
          r = t.nodeType;
      if (3 !== r && 8 !== r && 2 !== r) return void 0 === t.getAttribute ? T.prop(t, e, n) : (1 === r && T.isXMLDoc(t) || (o = T.attrHooks[e.toLowerCase()] || (T.expr.match.bool.test(e) ? Ce : void 0)), void 0 !== n ? null === n ? void T.removeAttr(t, e) : o && "set" in o && void 0 !== (i = o.set(t, n, e)) ? i : (t.setAttribute(e, n + ""), n) : !(o && "get" in o && null !== (i = o.get(t, e))) && null == (i = T.find.attr(t, e)) ? void 0 : i);
    },
    attrHooks: {
      type: {
        set: function set(t, e) {
          if (!y.radioValue && "radio" === e && A(t, "input")) {
            var n = t.value;
            return t.setAttribute("type", e), n && (t.value = n), e;
          }
        }
      }
    },
    removeAttr: function removeAttr(t, e) {
      var n,
          i = 0,
          o = e && e.match(j);
      if (o && 1 === t.nodeType) for (; n = o[i++];) {
        t.removeAttribute(n);
      }
    }
  }), Ce = {
    set: function set(t, e, n) {
      return !1 === e ? T.removeAttr(t, n) : t.setAttribute(n, n), n;
    }
  }, T.each(T.expr.match.bool.source.match(/\w+/g), function (t, e) {
    var s = xe[e] || T.find.attr;

    xe[e] = function (t, e, n) {
      var i,
          o,
          r = e.toLowerCase();
      return n || (o = xe[r], xe[r] = i, i = null != s(t, e, n) ? r : null, xe[r] = o), i;
    };
  });
  var Ee = /^(?:input|select|textarea|button)$/i,
      Te = /^(?:a|area)$/i;

  function Se(t) {
    return (t.match(j) || []).join(" ");
  }

  function Ae(t) {
    return t.getAttribute && t.getAttribute("class") || "";
  }

  function ke(t) {
    return Array.isArray(t) ? t : "string" == typeof t && t.match(j) || [];
  }

  T.fn.extend({
    prop: function prop(t, e) {
      return F(this, T.prop, t, e, 1 < arguments.length);
    },
    removeProp: function removeProp(t) {
      return this.each(function () {
        delete this[T.propFix[t] || t];
      });
    }
  }), T.extend({
    prop: function prop(t, e, n) {
      var i,
          o,
          r = t.nodeType;
      if (3 !== r && 8 !== r && 2 !== r) return 1 === r && T.isXMLDoc(t) || (e = T.propFix[e] || e, o = T.propHooks[e]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(t, n, e)) ? i : t[e] = n : o && "get" in o && null !== (i = o.get(t, e)) ? i : t[e];
    },
    propHooks: {
      tabIndex: {
        get: function get(t) {
          var e = T.find.attr(t, "tabindex");
          return e ? parseInt(e, 10) : Ee.test(t.nodeName) || Te.test(t.nodeName) && t.href ? 0 : -1;
        }
      }
    },
    propFix: {
      for: "htmlFor",
      class: "className"
    }
  }), y.optSelected || (T.propHooks.selected = {
    get: function get(t) {
      var e = t.parentNode;
      return e && e.parentNode && e.parentNode.selectedIndex, null;
    },
    set: function set(t) {
      var e = t.parentNode;
      e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex);
    }
  }), T.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
    T.propFix[this.toLowerCase()] = this;
  }), T.fn.extend({
    addClass: function addClass(e) {
      var t,
          n,
          i,
          o,
          r,
          s,
          a,
          l = 0;
      if (_(e)) return this.each(function (t) {
        T(this).addClass(e.call(this, t, Ae(this)));
      });
      if ((t = ke(e)).length) for (; n = this[l++];) {
        if (o = Ae(n), i = 1 === n.nodeType && " " + Se(o) + " ") {
          for (s = 0; r = t[s++];) {
            i.indexOf(" " + r + " ") < 0 && (i += r + " ");
          }

          o !== (a = Se(i)) && n.setAttribute("class", a);
        }
      }
      return this;
    },
    removeClass: function removeClass(e) {
      var t,
          n,
          i,
          o,
          r,
          s,
          a,
          l = 0;
      if (_(e)) return this.each(function (t) {
        T(this).removeClass(e.call(this, t, Ae(this)));
      });
      if (!arguments.length) return this.attr("class", "");
      if ((t = ke(e)).length) for (; n = this[l++];) {
        if (o = Ae(n), i = 1 === n.nodeType && " " + Se(o) + " ") {
          for (s = 0; r = t[s++];) {
            for (; -1 < i.indexOf(" " + r + " ");) {
              i = i.replace(" " + r + " ", " ");
            }
          }

          o !== (a = Se(i)) && n.setAttribute("class", a);
        }
      }
      return this;
    },
    toggleClass: function toggleClass(o, e) {
      var r = _typeof2(o),
          s = "string" == r || Array.isArray(o);

      return "boolean" == typeof e && s ? e ? this.addClass(o) : this.removeClass(o) : _(o) ? this.each(function (t) {
        T(this).toggleClass(o.call(this, t, Ae(this), e), e);
      }) : this.each(function () {
        var t, e, n, i;
        if (s) for (e = 0, n = T(this), i = ke(o); t = i[e++];) {
          n.hasClass(t) ? n.removeClass(t) : n.addClass(t);
        } else void 0 !== o && "boolean" != r || ((t = Ae(this)) && Y.set(this, "__className__", t), this.setAttribute && this.setAttribute("class", !t && !1 !== o && Y.get(this, "__className__") || ""));
      });
    },
    hasClass: function hasClass(t) {
      for (var e, n = 0, i = " " + t + " "; e = this[n++];) {
        if (1 === e.nodeType && -1 < (" " + Se(Ae(e)) + " ").indexOf(i)) return !0;
      }

      return !1;
    }
  });
  var De = /\r/g;
  T.fn.extend({
    val: function val(n) {
      var i,
          t,
          o,
          e = this[0];
      return arguments.length ? (o = _(n), this.each(function (t) {
        var e;
        1 === this.nodeType && (null == (e = o ? n.call(this, t, T(this).val()) : n) ? e = "" : "number" == typeof e ? e += "" : Array.isArray(e) && (e = T.map(e, function (t) {
          return null == t ? "" : t + "";
        })), (i = T.valHooks[this.type] || T.valHooks[this.nodeName.toLowerCase()]) && "set" in i && void 0 !== i.set(this, e, "value") || (this.value = e));
      })) : e ? (i = T.valHooks[e.type] || T.valHooks[e.nodeName.toLowerCase()]) && "get" in i && void 0 !== (t = i.get(e, "value")) ? t : "string" == typeof (t = e.value) ? t.replace(De, "") : null == t ? "" : t : void 0;
    }
  }), T.extend({
    valHooks: {
      option: {
        get: function get(t) {
          var e = T.find.attr(t, "value");
          return null != e ? e : Se(T.text(t));
        }
      },
      select: {
        get: function get(t) {
          for (var e, n, i = t.options, o = t.selectedIndex, r = "select-one" === t.type, s = r ? null : [], a = r ? o + 1 : i.length, l = o < 0 ? a : r ? o : 0; l < a; l++) {
            if (((n = i[l]).selected || l === o) && !n.disabled && (!n.parentNode.disabled || !A(n.parentNode, "optgroup"))) {
              if (e = T(n).val(), r) return e;
              s.push(e);
            }
          }

          return s;
        },
        set: function set(t, e) {
          for (var n, i, o = t.options, r = T.makeArray(e), s = o.length; s--;) {
            ((i = o[s]).selected = -1 < T.inArray(T.valHooks.option.get(i), r)) && (n = !0);
          }

          return n || (t.selectedIndex = -1), r;
        }
      }
    }
  }), T.each(["radio", "checkbox"], function () {
    T.valHooks[this] = {
      set: function set(t, e) {
        if (Array.isArray(e)) return t.checked = -1 < T.inArray(T(t).val(), e);
      }
    }, y.checkOn || (T.valHooks[this].get = function (t) {
      return null === t.getAttribute("value") ? "on" : t.value;
    });
  }), y.focusin = "onfocusin" in x;

  function Ie(t) {
    t.stopPropagation();
  }

  var Ne = /^(?:focusinfocus|focusoutblur)$/;
  T.extend(T.event, {
    trigger: function trigger(t, e, n, i) {
      var o,
          r,
          s,
          a,
          l,
          c,
          u,
          h = [n || E],
          d = v.call(t, "type") ? t.type : t,
          f = v.call(t, "namespace") ? t.namespace.split(".") : [],
          p = u = r = n = n || E;

      if (3 !== n.nodeType && 8 !== n.nodeType && !Ne.test(d + T.event.triggered) && (-1 < d.indexOf(".") && (d = (f = d.split(".")).shift(), f.sort()), a = d.indexOf(":") < 0 && "on" + d, (t = t[T.expando] ? t : new T.Event(d, "object" == _typeof2(t) && t)).isTrigger = i ? 2 : 3, t.namespace = f.join("."), t.rnamespace = t.namespace ? new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = n), e = null == e ? [t] : T.makeArray(e, [t]), c = T.event.special[d] || {}, i || !c.trigger || !1 !== c.trigger.apply(n, e))) {
        if (!i && !c.noBubble && !g(n)) {
          for (s = c.delegateType || d, Ne.test(s + d) || (p = p.parentNode); p; p = p.parentNode) {
            h.push(p), r = p;
          }

          r === (n.ownerDocument || E) && h.push(r.defaultView || r.parentWindow || x);
        }

        for (o = 0; (p = h[o++]) && !t.isPropagationStopped();) {
          u = p, t.type = 1 < o ? s : c.bindType || d, (l = (Y.get(p, "events") || Object.create(null))[t.type] && Y.get(p, "handle")) && l.apply(p, e), (l = a && p[a]) && l.apply && K(p) && (t.result = l.apply(p, e), !1 === t.result && t.preventDefault());
        }

        return t.type = d, i || t.isDefaultPrevented() || c._default && !1 !== c._default.apply(h.pop(), e) || !K(n) || a && _(n[d]) && !g(n) && ((r = n[a]) && (n[a] = null), T.event.triggered = d, t.isPropagationStopped() && u.addEventListener(d, Ie), n[d](), t.isPropagationStopped() && u.removeEventListener(d, Ie), T.event.triggered = void 0, r && (n[a] = r)), t.result;
      }
    },
    simulate: function simulate(t, e, n) {
      var i = T.extend(new T.Event(), n, {
        type: t,
        isSimulated: !0
      });
      T.event.trigger(i, null, e);
    }
  }), T.fn.extend({
    trigger: function trigger(t, e) {
      return this.each(function () {
        T.event.trigger(t, e, this);
      });
    },
    triggerHandler: function triggerHandler(t, e) {
      var n = this[0];
      if (n) return T.event.trigger(t, e, n, !0);
    }
  }), y.focusin || T.each({
    focus: "focusin",
    blur: "focusout"
  }, function (n, i) {
    function o(t) {
      T.event.simulate(i, t.target, T.event.fix(t));
    }

    T.event.special[i] = {
      setup: function setup() {
        var t = this.ownerDocument || this.document || this,
            e = Y.access(t, i);
        e || t.addEventListener(n, o, !0), Y.access(t, i, (e || 0) + 1);
      },
      teardown: function teardown() {
        var t = this.ownerDocument || this.document || this,
            e = Y.access(t, i) - 1;
        e ? Y.access(t, i, e) : (t.removeEventListener(n, o, !0), Y.remove(t, i));
      }
    };
  });
  var Oe = x.location,
      Pe = {
    guid: Date.now()
  },
      Le = /\?/;

  T.parseXML = function (t) {
    var e;
    if (!t || "string" != typeof t) return null;

    try {
      e = new x.DOMParser().parseFromString(t, "text/xml");
    } catch (t) {
      e = void 0;
    }

    return e && !e.getElementsByTagName("parsererror").length || T.error("Invalid XML: " + t), e;
  };

  var je = /\[\]$/,
      Me = /\r?\n/g,
      He = /^(?:submit|button|image|reset|file)$/i,
      $e = /^(?:input|select|textarea|keygen)/i;
  T.param = function (t, e) {
    function n(t, e) {
      var n = _(e) ? e() : e;
      o[o.length] = encodeURIComponent(t) + "=" + encodeURIComponent(null == n ? "" : n);
    }

    var i,
        o = [];
    if (null == t) return "";
    if (Array.isArray(t) || t.jquery && !T.isPlainObject(t)) T.each(t, function () {
      n(this.name, this.value);
    });else for (i in t) {
      !function n(i, t, o, r) {
        var e;
        if (Array.isArray(t)) T.each(t, function (t, e) {
          o || je.test(i) ? r(i, e) : n(i + "[" + ("object" == _typeof2(e) && null != e ? t : "") + "]", e, o, r);
        });else if (o || "object" !== w(t)) r(i, t);else for (e in t) {
          n(i + "[" + e + "]", t[e], o, r);
        }
      }(i, t[i], e, n);
    }
    return o.join("&");
  }, T.fn.extend({
    serialize: function serialize() {
      return T.param(this.serializeArray());
    },
    serializeArray: function serializeArray() {
      return this.map(function () {
        var t = T.prop(this, "elements");
        return t ? T.makeArray(t) : this;
      }).filter(function () {
        var t = this.type;
        return this.name && !T(this).is(":disabled") && $e.test(this.nodeName) && !He.test(t) && (this.checked || !ht.test(t));
      }).map(function (t, e) {
        var n = T(this).val();
        return null == n ? null : Array.isArray(n) ? T.map(n, function (t) {
          return {
            name: e.name,
            value: t.replace(Me, "\r\n")
          };
        }) : {
          name: e.name,
          value: n.replace(Me, "\r\n")
        };
      }).get();
    }
  });
  var qe = /%20/g,
      ze = /#.*$/,
      Re = /([?&])_=[^&]*/,
      Fe = /^(.*?):[ \t]*([^\r\n]*)$/gm,
      We = /^(?:GET|HEAD)$/,
      Be = /^\/\//,
      Ue = {},
      Qe = {},
      Ke = "*/".concat("*"),
      Ve = E.createElement("a");

  function Ye(r) {
    return function (t, e) {
      "string" != typeof t && (e = t, t = "*");
      var n,
          i = 0,
          o = t.toLowerCase().match(j) || [];
      if (_(e)) for (; n = o[i++];) {
        "+" === n[0] ? (n = n.slice(1) || "*", (r[n] = r[n] || []).unshift(e)) : (r[n] = r[n] || []).push(e);
      }
    };
  }

  function Xe(e, o, r, s) {
    var a = {},
        l = e === Qe;

    function c(t) {
      var i;
      return a[t] = !0, T.each(e[t] || [], function (t, e) {
        var n = e(o, r, s);
        return "string" != typeof n || l || a[n] ? l ? !(i = n) : void 0 : (o.dataTypes.unshift(n), c(n), !1);
      }), i;
    }

    return c(o.dataTypes[0]) || !a["*"] && c("*");
  }

  function Ze(t, e) {
    var n,
        i,
        o = T.ajaxSettings.flatOptions || {};

    for (n in e) {
      void 0 !== e[n] && ((o[n] ? t : i = i || {})[n] = e[n]);
    }

    return i && T.extend(!0, t, i), t;
  }

  Ve.href = Oe.href, T.extend({
    active: 0,
    lastModified: {},
    etag: {},
    ajaxSettings: {
      url: Oe.href,
      type: "GET",
      isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Oe.protocol),
      global: !0,
      processData: !0,
      async: !0,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      accepts: {
        "*": Ke,
        text: "text/plain",
        html: "text/html",
        xml: "application/xml, text/xml",
        json: "application/json, text/javascript"
      },
      contents: {
        xml: /\bxml\b/,
        html: /\bhtml/,
        json: /\bjson\b/
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText",
        json: "responseJSON"
      },
      converters: {
        "* text": String,
        "text html": !0,
        "text json": JSON.parse,
        "text xml": T.parseXML
      },
      flatOptions: {
        url: !0,
        context: !0
      }
    },
    ajaxSetup: function ajaxSetup(t, e) {
      return e ? Ze(Ze(t, T.ajaxSettings), e) : Ze(T.ajaxSettings, t);
    },
    ajaxPrefilter: Ye(Ue),
    ajaxTransport: Ye(Qe),
    ajax: function ajax(t, e) {
      "object" == _typeof2(t) && (e = t, t = void 0), e = e || {};

      var u,
          h,
          d,
          n,
          f,
          i,
          p,
          g,
          o,
          r,
          m = T.ajaxSetup({}, e),
          v = m.context || m,
          y = m.context && (v.nodeType || v.jquery) ? T(v) : T.event,
          _ = T.Deferred(),
          b = T.Callbacks("once memory"),
          w = m.statusCode || {},
          s = {},
          a = {},
          l = "canceled",
          C = {
        readyState: 0,
        getResponseHeader: function getResponseHeader(t) {
          var e;

          if (p) {
            if (!n) for (n = {}; e = Fe.exec(d);) {
              n[e[1].toLowerCase() + " "] = (n[e[1].toLowerCase() + " "] || []).concat(e[2]);
            }
            e = n[t.toLowerCase() + " "];
          }

          return null == e ? null : e.join(", ");
        },
        getAllResponseHeaders: function getAllResponseHeaders() {
          return p ? d : null;
        },
        setRequestHeader: function setRequestHeader(t, e) {
          return null == p && (t = a[t.toLowerCase()] = a[t.toLowerCase()] || t, s[t] = e), this;
        },
        overrideMimeType: function overrideMimeType(t) {
          return null == p && (m.mimeType = t), this;
        },
        statusCode: function statusCode(t) {
          var e;
          if (t) if (p) C.always(t[C.status]);else for (e in t) {
            w[e] = [w[e], t[e]];
          }
          return this;
        },
        abort: function abort(t) {
          var e = t || l;
          return u && u.abort(e), c(0, e), this;
        }
      };

      if (_.promise(C), m.url = ((t || m.url || Oe.href) + "").replace(Be, Oe.protocol + "//"), m.type = e.method || e.type || m.method || m.type, m.dataTypes = (m.dataType || "*").toLowerCase().match(j) || [""], null == m.crossDomain) {
        i = E.createElement("a");

        try {
          i.href = m.url, i.href, m.crossDomain = Ve.protocol + "//" + Ve.host != i.protocol + "//" + i.host;
        } catch (t) {
          m.crossDomain = !0;
        }
      }

      if (m.data && m.processData && "string" != typeof m.data && (m.data = T.param(m.data, m.traditional)), Xe(Ue, m, e, C), p) return C;

      for (o in (g = T.event && m.global) && 0 == T.active++ && T.event.trigger("ajaxStart"), m.type = m.type.toUpperCase(), m.hasContent = !We.test(m.type), h = m.url.replace(ze, ""), m.hasContent ? m.data && m.processData && 0 === (m.contentType || "").indexOf("application/x-www-form-urlencoded") && (m.data = m.data.replace(qe, "+")) : (r = m.url.slice(h.length), m.data && (m.processData || "string" == typeof m.data) && (h += (Le.test(h) ? "&" : "?") + m.data, delete m.data), !1 === m.cache && (h = h.replace(Re, "$1"), r = (Le.test(h) ? "&" : "?") + "_=" + Pe.guid++ + r), m.url = h + r), m.ifModified && (T.lastModified[h] && C.setRequestHeader("If-Modified-Since", T.lastModified[h]), T.etag[h] && C.setRequestHeader("If-None-Match", T.etag[h])), (m.data && m.hasContent && !1 !== m.contentType || e.contentType) && C.setRequestHeader("Content-Type", m.contentType), C.setRequestHeader("Accept", m.dataTypes[0] && m.accepts[m.dataTypes[0]] ? m.accepts[m.dataTypes[0]] + ("*" !== m.dataTypes[0] ? ", " + Ke + "; q=0.01" : "") : m.accepts["*"]), m.headers) {
        C.setRequestHeader(o, m.headers[o]);
      }

      if (m.beforeSend && (!1 === m.beforeSend.call(v, C, m) || p)) return C.abort();

      if (l = "abort", b.add(m.complete), C.done(m.success), C.fail(m.error), u = Xe(Qe, m, e, C)) {
        if (C.readyState = 1, g && y.trigger("ajaxSend", [C, m]), p) return C;
        m.async && 0 < m.timeout && (f = x.setTimeout(function () {
          C.abort("timeout");
        }, m.timeout));

        try {
          p = !1, u.send(s, c);
        } catch (t) {
          if (p) throw t;
          c(-1, t);
        }
      } else c(-1, "No Transport");

      function c(t, e, n, i) {
        var o,
            r,
            s,
            a,
            l,
            c = e;
        p || (p = !0, f && x.clearTimeout(f), u = void 0, d = i || "", C.readyState = 0 < t ? 4 : 0, o = 200 <= t && t < 300 || 304 === t, n && (a = function (t, e, n) {
          for (var i, o, r, s, a = t.contents, l = t.dataTypes; "*" === l[0];) {
            l.shift(), void 0 === i && (i = t.mimeType || e.getResponseHeader("Content-Type"));
          }

          if (i) for (o in a) {
            if (a[o] && a[o].test(i)) {
              l.unshift(o);
              break;
            }
          }
          if (l[0] in n) r = l[0];else {
            for (o in n) {
              if (!l[0] || t.converters[o + " " + l[0]]) {
                r = o;
                break;
              }

              s = s || o;
            }

            r = r || s;
          }
          if (r) return r !== l[0] && l.unshift(r), n[r];
        }(m, C, n)), !o && -1 < T.inArray("script", m.dataTypes) && (m.converters["text script"] = function () {}), a = function (t, e, n, i) {
          var o,
              r,
              s,
              a,
              l,
              c = {},
              u = t.dataTypes.slice();
          if (u[1]) for (s in t.converters) {
            c[s.toLowerCase()] = t.converters[s];
          }

          for (r = u.shift(); r;) {
            if (t.responseFields[r] && (n[t.responseFields[r]] = e), !l && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = r, r = u.shift()) if ("*" === r) r = l;else if ("*" !== l && l !== r) {
              if (!(s = c[l + " " + r] || c["* " + r])) for (o in c) {
                if ((a = o.split(" "))[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                  !0 === s ? s = c[o] : !0 !== c[o] && (r = a[0], u.unshift(a[1]));
                  break;
                }
              }
              if (!0 !== s) if (s && t.throws) e = s(e);else try {
                e = s(e);
              } catch (t) {
                return {
                  state: "parsererror",
                  error: s ? t : "No conversion from " + l + " to " + r
                };
              }
            }
          }

          return {
            state: "success",
            data: e
          };
        }(m, a, C, o), o ? (m.ifModified && ((l = C.getResponseHeader("Last-Modified")) && (T.lastModified[h] = l), (l = C.getResponseHeader("etag")) && (T.etag[h] = l)), 204 === t || "HEAD" === m.type ? c = "nocontent" : 304 === t ? c = "notmodified" : (c = a.state, r = a.data, o = !(s = a.error))) : (s = c, !t && c || (c = "error", t < 0 && (t = 0))), C.status = t, C.statusText = (e || c) + "", o ? _.resolveWith(v, [r, c, C]) : _.rejectWith(v, [C, c, s]), C.statusCode(w), w = void 0, g && y.trigger(o ? "ajaxSuccess" : "ajaxError", [C, m, o ? r : s]), b.fireWith(v, [C, c]), g && (y.trigger("ajaxComplete", [C, m]), --T.active || T.event.trigger("ajaxStop")));
      }

      return C;
    },
    getJSON: function getJSON(t, e, n) {
      return T.get(t, e, n, "json");
    },
    getScript: function getScript(t, e) {
      return T.get(t, void 0, e, "script");
    }
  }), T.each(["get", "post"], function (t, o) {
    T[o] = function (t, e, n, i) {
      return _(e) && (i = i || n, n = e, e = void 0), T.ajax(T.extend({
        url: t,
        type: o,
        dataType: i,
        data: e,
        success: n
      }, T.isPlainObject(t) && t));
    };
  }), T.ajaxPrefilter(function (t) {
    var e;

    for (e in t.headers) {
      "content-type" === e.toLowerCase() && (t.contentType = t.headers[e] || "");
    }
  }), T._evalUrl = function (t, e, n) {
    return T.ajax({
      url: t,
      type: "GET",
      dataType: "script",
      cache: !0,
      async: !1,
      global: !1,
      converters: {
        "text script": function textScript() {}
      },
      dataFilter: function dataFilter(t) {
        T.globalEval(t, e, n);
      }
    });
  }, T.fn.extend({
    wrapAll: function wrapAll(t) {
      var e;
      return this[0] && (_(t) && (t = t.call(this[0])), e = T(t, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && e.insertBefore(this[0]), e.map(function () {
        for (var t = this; t.firstElementChild;) {
          t = t.firstElementChild;
        }

        return t;
      }).append(this)), this;
    },
    wrapInner: function wrapInner(n) {
      return _(n) ? this.each(function (t) {
        T(this).wrapInner(n.call(this, t));
      }) : this.each(function () {
        var t = T(this),
            e = t.contents();
        e.length ? e.wrapAll(n) : t.append(n);
      });
    },
    wrap: function wrap(e) {
      var n = _(e);

      return this.each(function (t) {
        T(this).wrapAll(n ? e.call(this, t) : e);
      });
    },
    unwrap: function unwrap(t) {
      return this.parent(t).not("body").each(function () {
        T(this).replaceWith(this.childNodes);
      }), this;
    }
  }), T.expr.pseudos.hidden = function (t) {
    return !T.expr.pseudos.visible(t);
  }, T.expr.pseudos.visible = function (t) {
    return !!(t.offsetWidth || t.offsetHeight || t.getClientRects().length);
  }, T.ajaxSettings.xhr = function () {
    try {
      return new x.XMLHttpRequest();
    } catch (t) {}
  };
  var Ge = {
    0: 200,
    1223: 204
  },
      Je = T.ajaxSettings.xhr();
  y.cors = !!Je && "withCredentials" in Je, y.ajax = Je = !!Je, T.ajaxTransport(function (o) {
    var _r, s;

    if (y.cors || Je && !o.crossDomain) return {
      send: function send(t, e) {
        var n,
            i = o.xhr();
        if (i.open(o.type, o.url, o.async, o.username, o.password), o.xhrFields) for (n in o.xhrFields) {
          i[n] = o.xhrFields[n];
        }

        for (n in o.mimeType && i.overrideMimeType && i.overrideMimeType(o.mimeType), o.crossDomain || t["X-Requested-With"] || (t["X-Requested-With"] = "XMLHttpRequest"), t) {
          i.setRequestHeader(n, t[n]);
        }

        _r = function r(t) {
          return function () {
            _r && (_r = s = i.onload = i.onerror = i.onabort = i.ontimeout = i.onreadystatechange = null, "abort" === t ? i.abort() : "error" === t ? "number" != typeof i.status ? e(0, "error") : e(i.status, i.statusText) : e(Ge[i.status] || i.status, i.statusText, "text" !== (i.responseType || "text") || "string" != typeof i.responseText ? {
              binary: i.response
            } : {
              text: i.responseText
            }, i.getAllResponseHeaders()));
          };
        }, i.onload = _r(), s = i.onerror = i.ontimeout = _r("error"), void 0 !== i.onabort ? i.onabort = s : i.onreadystatechange = function () {
          4 === i.readyState && x.setTimeout(function () {
            _r && s();
          });
        }, _r = _r("abort");

        try {
          i.send(o.hasContent && o.data || null);
        } catch (t) {
          if (_r) throw t;
        }
      },
      abort: function abort() {
        _r && _r();
      }
    };
  }), T.ajaxPrefilter(function (t) {
    t.crossDomain && (t.contents.script = !1);
  }), T.ajaxSetup({
    accepts: {
      script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
    },
    contents: {
      script: /\b(?:java|ecma)script\b/
    },
    converters: {
      "text script": function textScript(t) {
        return T.globalEval(t), t;
      }
    }
  }), T.ajaxPrefilter("script", function (t) {
    void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET");
  }), T.ajaxTransport("script", function (n) {
    var i, _o;

    if (n.crossDomain || n.scriptAttrs) return {
      send: function send(t, e) {
        i = T("<script>").attr(n.scriptAttrs || {}).prop({
          charset: n.scriptCharset,
          src: n.url
        }).on("load error", _o = function o(t) {
          i.remove(), _o = null, t && e("error" === t.type ? 404 : 200, t.type);
        }), E.head.appendChild(i[0]);
      },
      abort: function abort() {
        _o && _o();
      }
    };
  });
  var tn,
      en = [],
      nn = /(=)\?(?=&|$)|\?\?/;
  T.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function jsonpCallback() {
      var t = en.pop() || T.expando + "_" + Pe.guid++;
      return this[t] = !0, t;
    }
  }), T.ajaxPrefilter("json jsonp", function (t, e, n) {
    var i,
        o,
        r,
        s = !1 !== t.jsonp && (nn.test(t.url) ? "url" : "string" == typeof t.data && 0 === (t.contentType || "").indexOf("application/x-www-form-urlencoded") && nn.test(t.data) && "data");
    if (s || "jsonp" === t.dataTypes[0]) return i = t.jsonpCallback = _(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(nn, "$1" + i) : !1 !== t.jsonp && (t.url += (Le.test(t.url) ? "&" : "?") + t.jsonp + "=" + i), t.converters["script json"] = function () {
      return r || T.error(i + " was not called"), r[0];
    }, t.dataTypes[0] = "json", o = x[i], x[i] = function () {
      r = arguments;
    }, n.always(function () {
      void 0 === o ? T(x).removeProp(i) : x[i] = o, t[i] && (t.jsonpCallback = e.jsonpCallback, en.push(i)), r && _(o) && o(r[0]), r = o = void 0;
    }), "script";
  }), y.createHTMLDocument = ((tn = E.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === tn.childNodes.length), T.parseHTML = function (t, e, n) {
    return "string" != typeof t ? [] : ("boolean" == typeof e && (n = e, e = !1), e || (y.createHTMLDocument ? ((i = (e = E.implementation.createHTMLDocument("")).createElement("base")).href = E.location.href, e.head.appendChild(i)) : e = E), r = !n && [], (o = k.exec(t)) ? [e.createElement(o[1])] : (o = _t([t], e, r), r && r.length && T(r).remove(), T.merge([], o.childNodes)));
    var i, o, r;
  }, T.fn.load = function (t, e, n) {
    var i,
        o,
        r,
        s = this,
        a = t.indexOf(" ");
    return -1 < a && (i = Se(t.slice(a)), t = t.slice(0, a)), _(e) ? (n = e, e = void 0) : e && "object" == _typeof2(e) && (o = "POST"), 0 < s.length && T.ajax({
      url: t,
      type: o || "GET",
      dataType: "html",
      data: e
    }).done(function (t) {
      r = arguments, s.html(i ? T("<div>").append(T.parseHTML(t)).find(i) : t);
    }).always(n && function (t, e) {
      s.each(function () {
        n.apply(this, r || [t.responseText, e, t]);
      });
    }), this;
  }, T.expr.pseudos.animated = function (e) {
    return T.grep(T.timers, function (t) {
      return e === t.elem;
    }).length;
  }, T.offset = {
    setOffset: function setOffset(t, e, n) {
      var i,
          o,
          r,
          s,
          a,
          l,
          c = T.css(t, "position"),
          u = T(t),
          h = {};
      "static" === c && (t.style.position = "relative"), a = u.offset(), r = T.css(t, "top"), l = T.css(t, "left"), o = ("absolute" === c || "fixed" === c) && -1 < (r + l).indexOf("auto") ? (s = (i = u.position()).top, i.left) : (s = parseFloat(r) || 0, parseFloat(l) || 0), _(e) && (e = e.call(t, n, T.extend({}, a))), null != e.top && (h.top = e.top - a.top + s), null != e.left && (h.left = e.left - a.left + o), "using" in e ? e.using.call(t, h) : ("number" == typeof h.top && (h.top += "px"), "number" == typeof h.left && (h.left += "px"), u.css(h));
    }
  }, T.fn.extend({
    offset: function offset(e) {
      if (arguments.length) return void 0 === e ? this : this.each(function (t) {
        T.offset.setOffset(this, e, t);
      });
      var t,
          n,
          i = this[0];
      return i ? i.getClientRects().length ? (t = i.getBoundingClientRect(), n = i.ownerDocument.defaultView, {
        top: t.top + n.pageYOffset,
        left: t.left + n.pageXOffset
      }) : {
        top: 0,
        left: 0
      } : void 0;
    },
    position: function position() {
      if (this[0]) {
        var t,
            e,
            n,
            i = this[0],
            o = {
          top: 0,
          left: 0
        };
        if ("fixed" === T.css(i, "position")) e = i.getBoundingClientRect();else {
          for (e = this.offset(), n = i.ownerDocument, t = i.offsetParent || n.documentElement; t && (t === n.body || t === n.documentElement) && "static" === T.css(t, "position");) {
            t = t.parentNode;
          }

          t && t !== i && 1 === t.nodeType && ((o = T(t).offset()).top += T.css(t, "borderTopWidth", !0), o.left += T.css(t, "borderLeftWidth", !0));
        }
        return {
          top: e.top - o.top - T.css(i, "marginTop", !0),
          left: e.left - o.left - T.css(i, "marginLeft", !0)
        };
      }
    },
    offsetParent: function offsetParent() {
      return this.map(function () {
        for (var t = this.offsetParent; t && "static" === T.css(t, "position");) {
          t = t.offsetParent;
        }

        return t || it;
      });
    }
  }), T.each({
    scrollLeft: "pageXOffset",
    scrollTop: "pageYOffset"
  }, function (e, o) {
    var r = "pageYOffset" === o;

    T.fn[e] = function (t) {
      return F(this, function (t, e, n) {
        var i;
        return g(t) ? i = t : 9 === t.nodeType && (i = t.defaultView), void 0 === n ? i ? i[o] : t[e] : void (i ? i.scrollTo(r ? i.pageXOffset : n, r ? n : i.pageYOffset) : t[e] = n);
      }, e, t, arguments.length);
    };
  }), T.each(["top", "left"], function (t, n) {
    T.cssHooks[n] = Xt(y.pixelPosition, function (t, e) {
      if (e) return e = Yt(t, n), Qt.test(e) ? T(t).position()[n] + "px" : e;
    });
  }), T.each({
    Height: "height",
    Width: "width"
  }, function (s, a) {
    T.each({
      padding: "inner" + s,
      content: a,
      "": "outer" + s
    }, function (i, r) {
      T.fn[r] = function (t, e) {
        var n = arguments.length && (i || "boolean" != typeof t),
            o = i || (!0 === t || !0 === e ? "margin" : "border");
        return F(this, function (t, e, n) {
          var i;
          return g(t) ? 0 === r.indexOf("outer") ? t["inner" + s] : t.document.documentElement["client" + s] : 9 === t.nodeType ? (i = t.documentElement, Math.max(t.body["scroll" + s], i["scroll" + s], t.body["offset" + s], i["offset" + s], i["client" + s])) : void 0 === n ? T.css(t, e, o) : T.style(t, e, n, o);
        }, a, n ? t : void 0, n);
      };
    });
  }), T.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (t, e) {
    T.fn[e] = function (t) {
      return this.on(e, t);
    };
  }), T.fn.extend({
    bind: function bind(t, e, n) {
      return this.on(t, null, e, n);
    },
    unbind: function unbind(t, e) {
      return this.off(t, null, e);
    },
    delegate: function delegate(t, e, n, i) {
      return this.on(e, t, n, i);
    },
    undelegate: function undelegate(t, e, n) {
      return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n);
    },
    hover: function hover(t, e) {
      return this.mouseenter(t).mouseleave(e || t);
    }
  }), T.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (t, n) {
    T.fn[n] = function (t, e) {
      return 0 < arguments.length ? this.on(n, null, t, e) : this.trigger(n);
    };
  });
  var on = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
  T.proxy = function (t, e) {
    var n, i, o;
    if ("string" == typeof e && (n = t[e], e = t, t = n), _(t)) return i = a.call(arguments, 2), (o = function o() {
      return t.apply(e || this, i.concat(a.call(arguments)));
    }).guid = t.guid = t.guid || T.guid++, o;
  }, T.holdReady = function (t) {
    t ? T.readyWait++ : T.ready(!0);
  }, T.isArray = Array.isArray, T.parseJSON = JSON.parse, T.nodeName = A, T.isFunction = _, T.isWindow = g, T.camelCase = Q, T.type = w, T.now = Date.now, T.isNumeric = function (t) {
    var e = T.type(t);
    return ("number" === e || "string" === e) && !isNaN(t - parseFloat(t));
  }, T.trim = function (t) {
    return null == t ? "" : (t + "").replace(on, "");
  },  true && !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_LOCAL_MODULE_0__ = ((function () {
    return T;
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)));
  var rn = x.jQuery,
      sn = x.$;
  return T.noConflict = function (t) {
    return x.$ === T && (x.$ = sn), t && x.jQuery === T && (x.jQuery = rn), T;
  }, void 0 === t && (x.jQuery = x.$ = T), T;
}), function (l, n, o, a) {
  function c(t, e) {
    this.settings = null, this.options = l.extend({}, c.Defaults, e), this.$element = l(t), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
      time: null,
      target: null,
      pointer: null,
      stage: {
        start: null,
        current: null
      },
      direction: null
    }, this._states = {
      current: {},
      tags: {
        initializing: ["busy"],
        animating: ["busy"],
        dragging: ["interacting"]
      }
    }, l.each(["onResize", "onThrottledResize"], l.proxy(function (t, e) {
      this._handlers[e] = l.proxy(this[e], this);
    }, this)), l.each(c.Plugins, l.proxy(function (t, e) {
      this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this);
    }, this)), l.each(c.Workers, l.proxy(function (t, e) {
      this._pipe.push({
        filter: e.filter,
        run: l.proxy(e.run, this)
      });
    }, this)), this.setup(), this.initialize();
  }

  c.Defaults = {
    items: 3,
    loop: !1,
    center: !1,
    rewind: !1,
    checkVisibility: !0,
    mouseDrag: !0,
    touchDrag: !0,
    pullDrag: !0,
    freeDrag: !1,
    margin: 0,
    stagePadding: 0,
    merge: !1,
    mergeFit: !0,
    autoWidth: !1,
    startPosition: 0,
    rtl: !1,
    smartSpeed: 250,
    fluidSpeed: !1,
    dragEndSpeed: !1,
    responsive: {},
    responsiveRefreshRate: 200,
    responsiveBaseElement: n,
    fallbackEasing: "swing",
    slideTransition: "",
    info: !1,
    nestedItemSelector: !1,
    itemElement: "div",
    stageElement: "div",
    refreshClass: "owl-refresh",
    loadedClass: "owl-loaded",
    loadingClass: "owl-loading",
    rtlClass: "owl-rtl",
    responsiveClass: "owl-responsive",
    dragClass: "owl-drag",
    itemClass: "owl-item",
    stageClass: "owl-stage",
    stageOuterClass: "owl-stage-outer",
    grabClass: "owl-grab"
  }, c.Width = {
    Default: "default",
    Inner: "inner",
    Outer: "outer"
  }, c.Type = {
    Event: "event",
    State: "state"
  }, c.Plugins = {}, c.Workers = [{
    filter: ["width", "settings"],
    run: function run() {
      this._width = this.$element.width();
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run(t) {
      t.current = this._items && this._items[this.relative(this._current)];
    }
  }, {
    filter: ["items", "settings"],
    run: function run() {
      this.$stage.children(".cloned").remove();
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run(t) {
      var e = this.settings.margin || "",
          n = !this.settings.autoWidth,
          i = this.settings.rtl,
          o = {
        width: "auto",
        "margin-left": i ? e : "",
        "margin-right": i ? "" : e
      };
      n || this.$stage.children().css(o), t.css = o;
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run(t) {
      var e = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
          n = null,
          i = this._items.length,
          o = !this.settings.autoWidth,
          r = [];

      for (t.items = {
        merge: !1,
        width: e
      }; i--;) {
        n = this._mergers[i], n = this.settings.mergeFit && Math.min(n, this.settings.items) || n, t.items.merge = 1 < n || t.items.merge, r[i] = o ? e * n : this._items[i].width();
      }

      this._widths = r;
    }
  }, {
    filter: ["items", "settings"],
    run: function run() {
      var t = [],
          e = this._items,
          n = this.settings,
          i = Math.max(2 * n.items, 4),
          o = 2 * Math.ceil(e.length / 2),
          r = n.loop && e.length ? n.rewind ? i : Math.max(i, o) : 0,
          s = "",
          a = "";

      for (r /= 2; 0 < r;) {
        t.push(this.normalize(t.length / 2, !0)), s += e[t[t.length - 1]][0].outerHTML, t.push(this.normalize(e.length - 1 - (t.length - 1) / 2, !0)), a = e[t[t.length - 1]][0].outerHTML + a, --r;
      }

      this._clones = t, l(s).addClass("cloned").appendTo(this.$stage), l(a).addClass("cloned").prependTo(this.$stage);
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run() {
      for (var t = this.settings.rtl ? 1 : -1, e = this._clones.length + this._items.length, n = -1, i = 0, o = 0, r = []; ++n < e;) {
        i = r[n - 1] || 0, o = this._widths[this.relative(n)] + this.settings.margin, r.push(i + o * t);
      }

      this._coordinates = r;
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run() {
      var t = this.settings.stagePadding,
          e = this._coordinates,
          n = {
        width: Math.ceil(Math.abs(e[e.length - 1])) + 2 * t,
        "padding-left": t || "",
        "padding-right": t || ""
      };
      this.$stage.css(n);
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run(t) {
      var e = this._coordinates.length,
          n = !this.settings.autoWidth,
          i = this.$stage.children();
      if (n && t.items.merge) for (; e--;) {
        t.css.width = this._widths[this.relative(e)], i.eq(e).css(t.css);
      } else n && (t.css.width = t.items.width, i.css(t.css));
    }
  }, {
    filter: ["items"],
    run: function run() {
      this._coordinates.length < 1 && this.$stage.removeAttr("style");
    }
  }, {
    filter: ["width", "items", "settings"],
    run: function run(t) {
      t.current = t.current ? this.$stage.children().index(t.current) : 0, t.current = Math.max(this.minimum(), Math.min(this.maximum(), t.current)), this.reset(t.current);
    }
  }, {
    filter: ["position"],
    run: function run() {
      this.animate(this.coordinates(this._current));
    }
  }, {
    filter: ["width", "position", "items", "settings"],
    run: function run() {
      for (var t, e, n = this.settings.rtl ? 1 : -1, i = 2 * this.settings.stagePadding, o = this.coordinates(this.current()) + i, r = o + this.width() * n, s = [], a = 0, l = this._coordinates.length; a < l; a++) {
        t = this._coordinates[a - 1] || 0, e = Math.abs(this._coordinates[a]) + i * n, (this.op(t, "<=", o) && this.op(t, ">", r) || this.op(e, "<", o) && this.op(e, ">", r)) && s.push(a);
      }

      this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + s.join("), :eq(") + ")").addClass("active"), this.$stage.children(".center").removeClass("center"), this.settings.center && this.$stage.children().eq(this.current()).addClass("center");
    }
  }], c.prototype.initializeStage = function () {
    this.$stage = this.$element.find("." + this.settings.stageClass), this.$stage.length || (this.$element.addClass(this.options.loadingClass), this.$stage = l("<" + this.settings.stageElement + ">", {
      class: this.settings.stageClass
    }).wrap(l("<div/>", {
      class: this.settings.stageOuterClass
    })), this.$element.append(this.$stage.parent()));
  }, c.prototype.initializeItems = function () {
    var t = this.$element.find(".owl-item");
    if (t.length) return this._items = t.get().map(function (t) {
      return l(t);
    }), this._mergers = this._items.map(function () {
      return 1;
    }), void this.refresh();
    this.replace(this.$element.children().not(this.$stage.parent())), this.isVisible() ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass);
  }, c.prototype.initialize = function () {
    var t, e, n;
    this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading") && (t = this.$element.find("img"), e = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : a, n = this.$element.children(e).width(), t.length && n <= 0 && this.preloadAutoWidthImages(t)), this.initializeStage(), this.initializeItems(), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized");
  }, c.prototype.isVisible = function () {
    return !this.settings.checkVisibility || this.$element.is(":visible");
  }, c.prototype.setup = function () {
    var e = this.viewport(),
        t = this.options.responsive,
        n = -1,
        i = null;
    t ? (l.each(t, function (t) {
      t <= e && n < t && (n = Number(t));
    }), "function" == typeof (i = l.extend({}, this.options, t[n])).stagePadding && (i.stagePadding = i.stagePadding()), delete i.responsive, i.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + n))) : i = l.extend({}, this.options), this.trigger("change", {
      property: {
        name: "settings",
        value: i
      }
    }), this._breakpoint = n, this.settings = i, this.invalidate("settings"), this.trigger("changed", {
      property: {
        name: "settings",
        value: this.settings
      }
    });
  }, c.prototype.optionsLogic = function () {
    this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1);
  }, c.prototype.prepare = function (t) {
    var e = this.trigger("prepare", {
      content: t
    });
    return e.data || (e.data = l("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(t)), this.trigger("prepared", {
      content: e.data
    }), e.data;
  }, c.prototype.update = function () {
    for (var t = 0, e = this._pipe.length, n = l.proxy(function (t) {
      return this[t];
    }, this._invalidated), i = {}; t < e;) {
      (this._invalidated.all || 0 < l.grep(this._pipe[t].filter, n).length) && this._pipe[t].run(i), t++;
    }

    this._invalidated = {}, this.is("valid") || this.enter("valid");
  }, c.prototype.width = function (t) {
    switch (t = t || c.Width.Default) {
      case c.Width.Inner:
      case c.Width.Outer:
        return this._width;

      default:
        return this._width - 2 * this.settings.stagePadding + this.settings.margin;
    }
  }, c.prototype.refresh = function () {
    this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed");
  }, c.prototype.onThrottledResize = function () {
    n.clearTimeout(this.resizeTimer), this.resizeTimer = n.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate);
  }, c.prototype.onResize = function () {
    return !!this._items.length && this._width !== this.$element.width() && !!this.isVisible() && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")));
  }, c.prototype.registerEventHandlers = function () {
    l.support.transition && this.$stage.on(l.support.transition.end + ".owl.core", l.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(n, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", l.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function () {
      return !1;
    })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", l.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", l.proxy(this.onDragEnd, this)));
  }, c.prototype.onDragStart = function (t) {
    var e = null;
    3 !== t.which && (e = l.support.transform ? {
      x: (e = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","))[16 === e.length ? 12 : 4],
      y: e[16 === e.length ? 13 : 5]
    } : (e = this.$stage.position(), {
      x: this.settings.rtl ? e.left + this.$stage.width() - this.width() + this.settings.margin : e.left,
      y: e.top
    }), this.is("animating") && (l.support.transform ? this.animate(e.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === t.type), this.speed(0), this._drag.time = new Date().getTime(), this._drag.target = l(t.target), this._drag.stage.start = e, this._drag.stage.current = e, this._drag.pointer = this.pointer(t), l(o).on("mouseup.owl.core touchend.owl.core", l.proxy(this.onDragEnd, this)), l(o).one("mousemove.owl.core touchmove.owl.core", l.proxy(function (t) {
      var e = this.difference(this._drag.pointer, this.pointer(t));
      l(o).on("mousemove.owl.core touchmove.owl.core", l.proxy(this.onDragMove, this)), Math.abs(e.x) < Math.abs(e.y) && this.is("valid") || (t.preventDefault(), this.enter("dragging"), this.trigger("drag"));
    }, this)));
  }, c.prototype.onDragMove = function (t) {
    var e = null,
        n = null,
        i = null,
        o = this.difference(this._drag.pointer, this.pointer(t)),
        r = this.difference(this._drag.stage.start, o);
    this.is("dragging") && (t.preventDefault(), this.settings.loop ? (e = this.coordinates(this.minimum()), n = this.coordinates(this.maximum() + 1) - e, r.x = ((r.x - e) % n + n) % n + e) : (e = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), n = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), i = this.settings.pullDrag ? -1 * o.x / 5 : 0, r.x = Math.max(Math.min(r.x, e + i), n + i)), this._drag.stage.current = r, this.animate(r.x));
  }, c.prototype.onDragEnd = function (t) {
    var e = this.difference(this._drag.pointer, this.pointer(t)),
        n = this._drag.stage.current,
        i = 0 < e.x ^ this.settings.rtl ? "left" : "right";
    l(o).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== e.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(n.x, 0 !== e.x ? i : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = i, (3 < Math.abs(e.x) || 300 < new Date().getTime() - this._drag.time) && this._drag.target.one("click.owl.core", function () {
      return !1;
    })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"));
  }, c.prototype.closest = function (n, i) {
    var o = -1,
        r = this.width(),
        s = this.coordinates();
    return this.settings.freeDrag || l.each(s, l.proxy(function (t, e) {
      return "left" === i && e - 30 < n && n < e + 30 ? o = t : "right" === i && e - r - 30 < n && n < e - r + 30 ? o = t + 1 : this.op(n, "<", e) && this.op(n, ">", s[t + 1] !== a ? s[t + 1] : e - r) && (o = "left" === i ? t + 1 : t), -1 === o;
    }, this)), this.settings.loop || (this.op(n, ">", s[this.minimum()]) ? o = n = this.minimum() : this.op(n, "<", s[this.maximum()]) && (o = n = this.maximum())), o;
  }, c.prototype.animate = function (t) {
    var e = 0 < this.speed();
    this.is("animating") && this.onTransitionEnd(), e && (this.enter("animating"), this.trigger("translate")), l.support.transform3d && l.support.transition ? this.$stage.css({
      transform: "translate3d(" + t + "px,0px,0px)",
      transition: this.speed() / 1e3 + "s" + (this.settings.slideTransition ? " " + this.settings.slideTransition : "")
    }) : e ? this.$stage.animate({
      left: t + "px"
    }, this.speed(), this.settings.fallbackEasing, l.proxy(this.onTransitionEnd, this)) : this.$stage.css({
      left: t + "px"
    });
  }, c.prototype.is = function (t) {
    return this._states.current[t] && 0 < this._states.current[t];
  }, c.prototype.current = function (t) {
    return t === a ? this._current : 0 === this._items.length ? a : (t = this.normalize(t), this._current !== t && ((e = this.trigger("change", {
      property: {
        name: "position",
        value: t
      }
    })).data !== a && (t = this.normalize(e.data)), this._current = t, this.invalidate("position"), this.trigger("changed", {
      property: {
        name: "position",
        value: this._current
      }
    })), this._current);
    var e;
  }, c.prototype.invalidate = function (t) {
    return "string" === l.type(t) && (this._invalidated[t] = !0, this.is("valid") && this.leave("valid")), l.map(this._invalidated, function (t, e) {
      return e;
    });
  }, c.prototype.reset = function (t) {
    (t = this.normalize(t)) !== a && (this._speed = 0, this._current = t, this.suppress(["translate", "translated"]), this.animate(this.coordinates(t)), this.release(["translate", "translated"]));
  }, c.prototype.normalize = function (t, e) {
    var n = this._items.length,
        i = e ? 0 : this._clones.length;
    return !this.isNumeric(t) || n < 1 ? t = a : (t < 0 || n + i <= t) && (t = ((t - i / 2) % n + n) % n + i / 2), t;
  }, c.prototype.relative = function (t) {
    return t -= this._clones.length / 2, this.normalize(t, !0);
  }, c.prototype.maximum = function (t) {
    var e,
        n,
        i,
        o = this.settings,
        r = this._coordinates.length;
    if (o.loop) r = this._clones.length / 2 + this._items.length - 1;else if (o.autoWidth || o.merge) {
      if (e = this._items.length) for (n = this._items[--e].width(), i = this.$element.width(); e-- && !(i < (n += this._items[e].width() + this.settings.margin));) {
        ;
      }
      r = e + 1;
    } else r = o.center ? this._items.length - 1 : this._items.length - o.items;
    return t && (r -= this._clones.length / 2), Math.max(r, 0);
  }, c.prototype.minimum = function (t) {
    return t ? 0 : this._clones.length / 2;
  }, c.prototype.items = function (t) {
    return t === a ? this._items.slice() : (t = this.normalize(t, !0), this._items[t]);
  }, c.prototype.mergers = function (t) {
    return t === a ? this._mergers.slice() : (t = this.normalize(t, !0), this._mergers[t]);
  }, c.prototype.clones = function (n) {
    function i(t) {
      return t % 2 == 0 ? o + t / 2 : e - (t + 1) / 2;
    }

    var e = this._clones.length / 2,
        o = e + this._items.length;
    return n === a ? l.map(this._clones, function (t, e) {
      return i(e);
    }) : l.map(this._clones, function (t, e) {
      return t === n ? i(e) : null;
    });
  }, c.prototype.speed = function (t) {
    return t !== a && (this._speed = t), this._speed;
  }, c.prototype.coordinates = function (t) {
    var e,
        n = 1,
        i = t - 1;
    return t === a ? l.map(this._coordinates, l.proxy(function (t, e) {
      return this.coordinates(e);
    }, this)) : (this.settings.center ? (this.settings.rtl && (n = -1, i = t + 1), e = this._coordinates[t], e += (this.width() - e + (this._coordinates[i] || 0)) / 2 * n) : e = this._coordinates[i] || 0, e = Math.ceil(e));
  }, c.prototype.duration = function (t, e, n) {
    return 0 === n ? 0 : Math.min(Math.max(Math.abs(e - t), 1), 6) * Math.abs(n || this.settings.smartSpeed);
  }, c.prototype.to = function (t, e) {
    var n = this.current(),
        i = null,
        o = t - this.relative(n),
        r = (0 < o) - (o < 0),
        s = this._items.length,
        a = this.minimum(),
        l = this.maximum();
    this.settings.loop ? (!this.settings.rewind && Math.abs(o) > s / 2 && (o += -1 * r * s), (i = (((t = n + o) - a) % s + s) % s + a) !== t && i - o <= l && 0 < i - o && (n = i - o, t = i, this.reset(n))) : t = this.settings.rewind ? (t % (l += 1) + l) % l : Math.max(a, Math.min(l, t)), this.speed(this.duration(n, t, e)), this.current(t), this.isVisible() && this.update();
  }, c.prototype.next = function (t) {
    t = t || !1, this.to(this.relative(this.current()) + 1, t);
  }, c.prototype.prev = function (t) {
    t = t || !1, this.to(this.relative(this.current()) - 1, t);
  }, c.prototype.onTransitionEnd = function (t) {
    if (t !== a && (t.stopPropagation(), (t.target || t.srcElement || t.originalTarget) !== this.$stage.get(0))) return !1;
    this.leave("animating"), this.trigger("translated");
  }, c.prototype.viewport = function () {
    var t;
    return this.options.responsiveBaseElement !== n ? t = l(this.options.responsiveBaseElement).width() : n.innerWidth ? t = n.innerWidth : o.documentElement && o.documentElement.clientWidth ? t = o.documentElement.clientWidth : console.warn("Can not detect viewport width."), t;
  }, c.prototype.replace = function (t) {
    this.$stage.empty(), this._items = [], t = t && (t instanceof jQuery ? t : l(t)), this.settings.nestedItemSelector && (t = t.find("." + this.settings.nestedItemSelector)), t.filter(function () {
      return 1 === this.nodeType;
    }).each(l.proxy(function (t, e) {
      e = this.prepare(e), this.$stage.append(e), this._items.push(e), this._mergers.push(+e.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1);
    }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items");
  }, c.prototype.add = function (t, e) {
    var n = this.relative(this._current);
    e = e === a ? this._items.length : this.normalize(e, !0), t = t instanceof jQuery ? t : l(t), this.trigger("add", {
      content: t,
      position: e
    }), t = this.prepare(t), 0 === this._items.length || e === this._items.length ? (0 === this._items.length && this.$stage.append(t), 0 !== this._items.length && this._items[e - 1].after(t), this._items.push(t), this._mergers.push(+t.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[e].before(t), this._items.splice(e, 0, t), this._mergers.splice(e, 0, +t.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[n] && this.reset(this._items[n].index()), this.invalidate("items"), this.trigger("added", {
      content: t,
      position: e
    });
  }, c.prototype.remove = function (t) {
    (t = this.normalize(t, !0)) !== a && (this.trigger("remove", {
      content: this._items[t],
      position: t
    }), this._items[t].remove(), this._items.splice(t, 1), this._mergers.splice(t, 1), this.invalidate("items"), this.trigger("removed", {
      content: null,
      position: t
    }));
  }, c.prototype.preloadAutoWidthImages = function (t) {
    t.each(l.proxy(function (t, e) {
      this.enter("pre-loading"), e = l(e), l(new Image()).one("load", l.proxy(function (t) {
        e.attr("src", t.target.src), e.css("opacity", 1), this.leave("pre-loading"), this.is("pre-loading") || this.is("initializing") || this.refresh();
      }, this)).attr("src", e.attr("src") || e.attr("data-src") || e.attr("data-src-retina"));
    }, this));
  }, c.prototype.destroy = function () {
    for (var t in this.$element.off(".owl.core"), this.$stage.off(".owl.core"), l(o).off(".owl.core"), !1 !== this.settings.responsive && (n.clearTimeout(this.resizeTimer), this.off(n, "resize", this._handlers.onThrottledResize)), this._plugins) {
      this._plugins[t].destroy();
    }

    this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$stage.remove(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel");
  }, c.prototype.op = function (t, e, n) {
    var i = this.settings.rtl;

    switch (e) {
      case "<":
        return i ? n < t : t < n;

      case ">":
        return i ? t < n : n < t;

      case ">=":
        return i ? t <= n : n <= t;

      case "<=":
        return i ? n <= t : t <= n;
    }
  }, c.prototype.on = function (t, e, n, i) {
    t.addEventListener ? t.addEventListener(e, n, i) : t.attachEvent && t.attachEvent("on" + e, n);
  }, c.prototype.off = function (t, e, n, i) {
    t.removeEventListener ? t.removeEventListener(e, n, i) : t.detachEvent && t.detachEvent("on" + e, n);
  }, c.prototype.trigger = function (t, e, n, i, o) {
    var r = {
      item: {
        count: this._items.length,
        index: this.current()
      }
    },
        s = l.camelCase(l.grep(["on", t, n], function (t) {
      return t;
    }).join("-").toLowerCase()),
        a = l.Event([t, "owl", n || "carousel"].join(".").toLowerCase(), l.extend({
      relatedTarget: this
    }, r, e));
    return this._supress[t] || (l.each(this._plugins, function (t, e) {
      e.onTrigger && e.onTrigger(a);
    }), this.register({
      type: c.Type.Event,
      name: t
    }), this.$element.trigger(a), this.settings && "function" == typeof this.settings[s] && this.settings[s].call(this, a)), a;
  }, c.prototype.enter = function (t) {
    l.each([t].concat(this._states.tags[t] || []), l.proxy(function (t, e) {
      this._states.current[e] === a && (this._states.current[e] = 0), this._states.current[e]++;
    }, this));
  }, c.prototype.leave = function (t) {
    l.each([t].concat(this._states.tags[t] || []), l.proxy(function (t, e) {
      this._states.current[e]--;
    }, this));
  }, c.prototype.register = function (n) {
    var e;
    n.type === c.Type.Event ? (l.event.special[n.name] || (l.event.special[n.name] = {}), l.event.special[n.name].owl || (e = l.event.special[n.name]._default, l.event.special[n.name]._default = function (t) {
      return !e || !e.apply || t.namespace && -1 !== t.namespace.indexOf("owl") ? t.namespace && -1 < t.namespace.indexOf("owl") : e.apply(this, arguments);
    }, l.event.special[n.name].owl = !0)) : n.type === c.Type.State && (this._states.tags[n.name] ? this._states.tags[n.name] = this._states.tags[n.name].concat(n.tags) : this._states.tags[n.name] = n.tags, this._states.tags[n.name] = l.grep(this._states.tags[n.name], l.proxy(function (t, e) {
      return l.inArray(t, this._states.tags[n.name]) === e;
    }, this)));
  }, c.prototype.suppress = function (t) {
    l.each(t, l.proxy(function (t, e) {
      this._supress[e] = !0;
    }, this));
  }, c.prototype.release = function (t) {
    l.each(t, l.proxy(function (t, e) {
      delete this._supress[e];
    }, this));
  }, c.prototype.pointer = function (t) {
    var e = {
      x: null,
      y: null
    };
    return (t = (t = t.originalEvent || t || n.event).touches && t.touches.length ? t.touches[0] : t.changedTouches && t.changedTouches.length ? t.changedTouches[0] : t).pageX ? (e.x = t.pageX, e.y = t.pageY) : (e.x = t.clientX, e.y = t.clientY), e;
  }, c.prototype.isNumeric = function (t) {
    return !isNaN(parseFloat(t));
  }, c.prototype.difference = function (t, e) {
    return {
      x: t.x - e.x,
      y: t.y - e.y
    };
  }, l.fn.owlCarousel = function (e) {
    var i = Array.prototype.slice.call(arguments, 1);
    return this.each(function () {
      var t = l(this),
          n = t.data("owl.carousel");
      n || (n = new c(this, "object" == _typeof2(e) && e), t.data("owl.carousel", n), l.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function (t, e) {
        n.register({
          type: c.Type.Event,
          name: e
        }), n.$element.on(e + ".owl.carousel.core", l.proxy(function (t) {
          t.namespace && t.relatedTarget !== this && (this.suppress([e]), n[e].apply(this, [].slice.call(arguments, 1)), this.release([e]));
        }, n));
      })), "string" == typeof e && "_" !== e.charAt(0) && n[e].apply(n, i);
    });
  }, l.fn.owlCarousel.Constructor = c;
}(window.Zepto || __webpack_provided_window_dot_jQuery, window, document), function (e, n) {
  var i = function i(t) {
    this._core = t, this._interval = null, this._visible = null, this._handlers = {
      "initialized.owl.carousel": e.proxy(function (t) {
        t.namespace && this._core.settings.autoRefresh && this.watch();
      }, this)
    }, this._core.options = e.extend({}, i.Defaults, this._core.options), this._core.$element.on(this._handlers);
  };

  i.Defaults = {
    autoRefresh: !0,
    autoRefreshInterval: 500
  }, i.prototype.watch = function () {
    this._interval || (this._visible = this._core.isVisible(), this._interval = n.setInterval(e.proxy(this.refresh, this), this._core.settings.autoRefreshInterval));
  }, i.prototype.refresh = function () {
    this._core.isVisible() !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh());
  }, i.prototype.destroy = function () {
    var t, e;

    for (t in n.clearInterval(this._interval), this._handlers) {
      this._core.$element.off(t, this._handlers[t]);
    }

    for (e in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[e] && (this[e] = null);
    }
  }, e.fn.owlCarousel.Constructor.Plugins.AutoRefresh = i;
}(window.Zepto || __webpack_provided_window_dot_jQuery, window, document), function (a, r) {
  var e = function e(t) {
    this._core = t, this._loaded = [], this._handlers = {
      "initialized.owl.carousel change.owl.carousel resized.owl.carousel": a.proxy(function (t) {
        if (t.namespace && this._core.settings && this._core.settings.lazyLoad && (t.property && "position" == t.property.name || "initialized" == t.type)) {
          var e = this._core.settings,
              n = e.center && Math.ceil(e.items / 2) || e.items,
              i = e.center && -1 * n || 0,
              o = (t.property && void 0 !== t.property.value ? t.property.value : this._core.current()) + i,
              r = this._core.clones().length,
              s = a.proxy(function (t, e) {
            this.load(e);
          }, this);

          for (0 < e.lazyLoadEager && (n += e.lazyLoadEager, e.loop && (o -= e.lazyLoadEager, n++)); i++ < n;) {
            this.load(r / 2 + this._core.relative(o)), r && a.each(this._core.clones(this._core.relative(o)), s), o++;
          }
        }
      }, this)
    }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers);
  };

  e.Defaults = {
    lazyLoad: !1,
    lazyLoadEager: 0
  }, e.prototype.load = function (t) {
    var e = this._core.$stage.children().eq(t),
        n = e && e.find(".owl-lazy");

    !n || -1 < a.inArray(e.get(0), this._loaded) || (n.each(a.proxy(function (t, e) {
      var n,
          i = a(e),
          o = 1 < r.devicePixelRatio && i.attr("data-src-retina") || i.attr("data-src") || i.attr("data-srcset");
      this._core.trigger("load", {
        element: i,
        url: o
      }, "lazy"), i.is("img") ? i.one("load.owl.lazy", a.proxy(function () {
        i.css("opacity", 1), this._core.trigger("loaded", {
          element: i,
          url: o
        }, "lazy");
      }, this)).attr("src", o) : i.is("source") ? i.one("load.owl.lazy", a.proxy(function () {
        this._core.trigger("loaded", {
          element: i,
          url: o
        }, "lazy");
      }, this)).attr("srcset", o) : ((n = new Image()).onload = a.proxy(function () {
        i.css({
          "background-image": 'url("' + o + '")',
          opacity: "1"
        }), this._core.trigger("loaded", {
          element: i,
          url: o
        }, "lazy");
      }, this), n.src = o);
    }, this)), this._loaded.push(e.get(0)));
  }, e.prototype.destroy = function () {
    var t, e;

    for (t in this.handlers) {
      this._core.$element.off(t, this.handlers[t]);
    }

    for (e in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[e] && (this[e] = null);
    }
  }, a.fn.owlCarousel.Constructor.Plugins.Lazy = e;
}(window.Zepto || __webpack_provided_window_dot_jQuery, window, document), function (s, n) {
  var i = function i(t) {
    this._core = t, this._previousHeight = null, this._handlers = {
      "initialized.owl.carousel refreshed.owl.carousel": s.proxy(function (t) {
        t.namespace && this._core.settings.autoHeight && this.update();
      }, this),
      "changed.owl.carousel": s.proxy(function (t) {
        t.namespace && this._core.settings.autoHeight && "position" === t.property.name && this.update();
      }, this),
      "loaded.owl.lazy": s.proxy(function (t) {
        t.namespace && this._core.settings.autoHeight && t.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update();
      }, this)
    }, this._core.options = s.extend({}, i.Defaults, this._core.options), this._core.$element.on(this._handlers), this._intervalId = null;
    var e = this;
    s(n).on("load", function () {
      e._core.settings.autoHeight && e.update();
    }), s(n).resize(function () {
      e._core.settings.autoHeight && (null != e._intervalId && clearTimeout(e._intervalId), e._intervalId = setTimeout(function () {
        e.update();
      }, 250));
    });
  };

  i.Defaults = {
    autoHeight: !1,
    autoHeightClass: "owl-height"
  }, i.prototype.update = function () {
    var t = this._core._current,
        e = t + this._core.settings.items,
        n = this._core.settings.lazyLoad,
        i = this._core.$stage.children().toArray().slice(t, e),
        o = [],
        r = 0;

    s.each(i, function (t, e) {
      o.push(s(e).height());
    }), (r = Math.max.apply(null, o)) <= 1 && n && this._previousHeight && (r = this._previousHeight), this._previousHeight = r, this._core.$stage.parent().height(r).addClass(this._core.settings.autoHeightClass);
  }, i.prototype.destroy = function () {
    var t, e;

    for (t in this._handlers) {
      this._core.$element.off(t, this._handlers[t]);
    }

    for (e in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[e] && (this[e] = null);
    }
  }, s.fn.owlCarousel.Constructor.Plugins.AutoHeight = i;
}(window.Zepto || __webpack_provided_window_dot_jQuery, window, document), function (s) {
  var e = function e(t) {
    this.core = t, this.core.options = s.extend({}, e.Defaults, this.core.options), this.swapping = !0, this.previous = void 0, this.next = void 0, this.handlers = {
      "change.owl.carousel": s.proxy(function (t) {
        t.namespace && "position" == t.property.name && (this.previous = this.core.current(), this.next = t.property.value);
      }, this),
      "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": s.proxy(function (t) {
        t.namespace && (this.swapping = "translated" == t.type);
      }, this),
      "translate.owl.carousel": s.proxy(function (t) {
        t.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap();
      }, this)
    }, this.core.$element.on(this.handlers);
  };

  e.Defaults = {
    animateOut: !1,
    animateIn: !1
  }, e.prototype.swap = function () {
    var t, e, n, i, o, r;
    1 === this.core.settings.items && s.support.animation && s.support.transition && (this.core.speed(0), e = s.proxy(this.clear, this), n = this.core.$stage.children().eq(this.previous), i = this.core.$stage.children().eq(this.next), o = this.core.settings.animateIn, r = this.core.settings.animateOut, this.core.current() !== this.previous && (r && (t = this.core.coordinates(this.previous) - this.core.coordinates(this.next), n.one(s.support.animation.end, e).css({
      left: t + "px"
    }).addClass("animated owl-animated-out").addClass(r)), o && i.one(s.support.animation.end, e).addClass("animated owl-animated-in").addClass(o)));
  }, e.prototype.clear = function (t) {
    s(t.target).css({
      left: ""
    }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd();
  }, e.prototype.destroy = function () {
    var t, e;

    for (t in this.handlers) {
      this.core.$element.off(t, this.handlers[t]);
    }

    for (e in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[e] && (this[e] = null);
    }
  }, s.fn.owlCarousel.Constructor.Plugins.Animate = e;
}(window.Zepto || __webpack_provided_window_dot_jQuery, (window, document)), function (i, o, e) {
  var n = function n(t) {
    this._core = t, this._call = null, this._time = 0, this._timeout = 0, this._paused = !0, this._handlers = {
      "changed.owl.carousel": i.proxy(function (t) {
        t.namespace && "settings" === t.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : t.namespace && "position" === t.property.name && this._paused && (this._time = 0);
      }, this),
      "initialized.owl.carousel": i.proxy(function (t) {
        t.namespace && this._core.settings.autoplay && this.play();
      }, this),
      "play.owl.autoplay": i.proxy(function (t, e, n) {
        t.namespace && this.play(e, n);
      }, this),
      "stop.owl.autoplay": i.proxy(function (t) {
        t.namespace && this.stop();
      }, this),
      "mouseover.owl.autoplay": i.proxy(function () {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause();
      }, this),
      "mouseleave.owl.autoplay": i.proxy(function () {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play();
      }, this),
      "touchstart.owl.core": i.proxy(function () {
        this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause();
      }, this),
      "touchend.owl.core": i.proxy(function () {
        this._core.settings.autoplayHoverPause && this.play();
      }, this)
    }, this._core.$element.on(this._handlers), this._core.options = i.extend({}, n.Defaults, this._core.options);
  };

  n.Defaults = {
    autoplay: !1,
    autoplayTimeout: 5e3,
    autoplayHoverPause: !1,
    autoplaySpeed: !1
  }, n.prototype._next = function (t) {
    this._call = o.setTimeout(i.proxy(this._next, this, t), this._timeout * (Math.round(this.read() / this._timeout) + 1) - this.read()), this._core.is("interacting") || e.hidden || this._core.next(t || this._core.settings.autoplaySpeed);
  }, n.prototype.read = function () {
    return new Date().getTime() - this._time;
  }, n.prototype.play = function (t, e) {
    var n;
    this._core.is("rotating") || this._core.enter("rotating"), t = t || this._core.settings.autoplayTimeout, n = Math.min(this._time % (this._timeout || t), t), this._paused ? (this._time = this.read(), this._paused = !1) : o.clearTimeout(this._call), this._time += this.read() % t - n, this._timeout = t, this._call = o.setTimeout(i.proxy(this._next, this, e), t - n);
  }, n.prototype.stop = function () {
    this._core.is("rotating") && (this._time = 0, this._paused = !0, o.clearTimeout(this._call), this._core.leave("rotating"));
  }, n.prototype.pause = function () {
    this._core.is("rotating") && !this._paused && (this._time = this.read(), this._paused = !0, o.clearTimeout(this._call));
  }, n.prototype.destroy = function () {
    var t, e;

    for (t in this.stop(), this._handlers) {
      this._core.$element.off(t, this._handlers[t]);
    }

    for (e in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[e] && (this[e] = null);
    }
  }, i.fn.owlCarousel.Constructor.Plugins.autoplay = n;
}(window.Zepto || __webpack_provided_window_dot_jQuery, window, document), function (r) {
  "use strict";

  var e = function e(t) {
    this._core = t, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
      next: this._core.next,
      prev: this._core.prev,
      to: this._core.to
    }, this._handlers = {
      "prepared.owl.carousel": r.proxy(function (t) {
        t.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + r(t.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>");
      }, this),
      "added.owl.carousel": r.proxy(function (t) {
        t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 0, this._templates.pop());
      }, this),
      "remove.owl.carousel": r.proxy(function (t) {
        t.namespace && this._core.settings.dotsData && this._templates.splice(t.position, 1);
      }, this),
      "changed.owl.carousel": r.proxy(function (t) {
        t.namespace && "position" == t.property.name && this.draw();
      }, this),
      "initialized.owl.carousel": r.proxy(function (t) {
        t.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"));
      }, this),
      "refreshed.owl.carousel": r.proxy(function (t) {
        t.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"));
      }, this)
    }, this._core.options = r.extend({}, e.Defaults, this._core.options), this.$element.on(this._handlers);
  };

  e.Defaults = {
    nav: !1,
    navText: ['<span aria-label="Previous">&#x2039;</span>', '<span aria-label="Next">&#x203a;</span>'],
    navSpeed: !1,
    navElement: 'button type="button" role="presentation"',
    navContainer: !1,
    navContainerClass: "owl-nav",
    navClass: ["owl-prev", "owl-next"],
    slideBy: 1,
    dotClass: "owl-dot",
    dotsClass: "owl-dots",
    dots: !0,
    dotsEach: !1,
    dotsData: !1,
    dotsSpeed: !1,
    dotsContainer: !1
  }, e.prototype.initialize = function () {
    var t,
        n = this._core.settings;

    for (t in this._controls.$relative = (n.navContainer ? r(n.navContainer) : r("<div>").addClass(n.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = r("<" + n.navElement + ">").addClass(n.navClass[0]).html(n.navText[0]).prependTo(this._controls.$relative).on("click", r.proxy(function (t) {
      this.prev(n.navSpeed);
    }, this)), this._controls.$next = r("<" + n.navElement + ">").addClass(n.navClass[1]).html(n.navText[1]).appendTo(this._controls.$relative).on("click", r.proxy(function (t) {
      this.next(n.navSpeed);
    }, this)), n.dotsData || (this._templates = [r('<button role="button">').addClass(n.dotClass).append(r("<span>")).prop("outerHTML")]), this._controls.$absolute = (n.dotsContainer ? r(n.dotsContainer) : r("<div>").addClass(n.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "button", r.proxy(function (t) {
      var e = r(t.target).parent().is(this._controls.$absolute) ? r(t.target).index() : r(t.target).parent().index();
      t.preventDefault(), this.to(e, n.dotsSpeed);
    }, this)), this._overrides) {
      this._core[t] = r.proxy(this[t], this);
    }
  }, e.prototype.destroy = function () {
    var t,
        e,
        n,
        i,
        o = this._core.settings;

    for (t in this._handlers) {
      this.$element.off(t, this._handlers[t]);
    }

    for (e in this._controls) {
      "$relative" === e && o.navContainer ? this._controls[e].html("") : this._controls[e].remove();
    }

    for (i in this.overides) {
      this._core[i] = this._overrides[i];
    }

    for (n in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[n] && (this[n] = null);
    }
  }, e.prototype.update = function () {
    var t,
        e,
        n = this._core.clones().length / 2,
        i = n + this._core.items().length,
        o = this._core.maximum(!0),
        r = this._core.settings,
        s = r.center || r.autoWidth || r.dotsData ? 1 : r.dotsEach || r.items;

    if ("page" !== r.slideBy && (r.slideBy = Math.min(r.slideBy, r.items)), r.dots || "page" == r.slideBy) for (this._pages = [], t = n, e = 0; t < i; t++) {
      if (s <= e || 0 === e) {
        if (this._pages.push({
          start: Math.min(o, t - n),
          end: t - n + s - 1
        }), Math.min(o, t - n) === o) break;
        e = 0, 0;
      }

      e += this._core.mergers(this._core.relative(t));
    }
  }, e.prototype.draw = function () {
    var t,
        e = this._core.settings,
        n = this._core.items().length <= e.items,
        i = this._core.relative(this._core.current()),
        o = e.loop || e.rewind;

    this._controls.$relative.toggleClass("disabled", !e.nav || n), e.nav && (this._controls.$previous.toggleClass("disabled", !o && i <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !o && i >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !e.dots || n), e.dots && (t = this._pages.length - this._controls.$absolute.children().length, e.dotsData && 0 != t ? this._controls.$absolute.html(this._templates.join("")) : 0 < t ? this._controls.$absolute.append(new Array(1 + t).join(this._templates[0])) : t < 0 && this._controls.$absolute.children().slice(t).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(r.inArray(this.current(), this._pages)).addClass("active"));
  }, e.prototype.onTrigger = function (t) {
    var e = this._core.settings;
    t.page = {
      index: r.inArray(this.current(), this._pages),
      count: this._pages.length,
      size: e && (e.center || e.autoWidth || e.dotsData ? 1 : e.dotsEach || e.items)
    };
  }, e.prototype.current = function () {
    var n = this._core.relative(this._core.current());

    return r.grep(this._pages, r.proxy(function (t, e) {
      return t.start <= n && t.end >= n;
    }, this)).pop();
  }, e.prototype.getPosition = function (t) {
    var e,
        n,
        i = this._core.settings;
    return "page" == i.slideBy ? (e = r.inArray(this.current(), this._pages), n = this._pages.length, t ? ++e : --e, e = this._pages[(e % n + n) % n].start) : (e = this._core.relative(this._core.current()), n = this._core.items().length, t ? e += i.slideBy : e -= i.slideBy), e;
  }, e.prototype.next = function (t) {
    r.proxy(this._overrides.to, this._core)(this.getPosition(!0), t);
  }, e.prototype.prev = function (t) {
    r.proxy(this._overrides.to, this._core)(this.getPosition(!1), t);
  }, e.prototype.to = function (t, e, n) {
    var i;
    !n && this._pages.length ? (i = this._pages.length, r.proxy(this._overrides.to, this._core)(this._pages[(t % i + i) % i].start, e)) : r.proxy(this._overrides.to, this._core)(t, e);
  }, r.fn.owlCarousel.Constructor.Plugins.Navigation = e;
}(window.Zepto || __webpack_provided_window_dot_jQuery, (window, document)), function (i, o) {
  "use strict";

  var e = function e(t) {
    this._core = t, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
      "initialized.owl.carousel": i.proxy(function (t) {
        t.namespace && "URLHash" === this._core.settings.startPosition && i(o).trigger("hashchange.owl.navigation");
      }, this),
      "prepared.owl.carousel": i.proxy(function (t) {
        if (t.namespace) {
          var e = i(t.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
          if (!e) return;
          this._hashes[e] = t.content;
        }
      }, this),
      "changed.owl.carousel": i.proxy(function (t) {
        if (t.namespace && "position" === t.property.name) {
          var n = this._core.items(this._core.relative(this._core.current())),
              e = i.map(this._hashes, function (t, e) {
            return t === n ? e : null;
          }).join();

          if (!e || o.location.hash.slice(1) === e) return;
          o.location.hash = e;
        }
      }, this)
    }, this._core.options = i.extend({}, e.Defaults, this._core.options), this.$element.on(this._handlers), i(o).on("hashchange.owl.navigation", i.proxy(function (t) {
      var e = o.location.hash.substring(1),
          n = this._core.$stage.children(),
          i = this._hashes[e] && n.index(this._hashes[e]);

      void 0 !== i && i !== this._core.current() && this._core.to(this._core.relative(i), !1, !0);
    }, this));
  };

  e.Defaults = {
    URLhashListener: !1
  }, e.prototype.destroy = function () {
    var t, e;

    for (t in i(o).off("hashchange.owl.navigation"), this._handlers) {
      this._core.$element.off(t, this._handlers[t]);
    }

    for (e in Object.getOwnPropertyNames(this)) {
      "function" != typeof this[e] && (this[e] = null);
    }
  }, i.fn.owlCarousel.Constructor.Plugins.Hash = e;
}(window.Zepto || __webpack_provided_window_dot_jQuery, window, document), function (o, r) {
  var s = o("<support>").get(0).style,
      a = "Webkit Moz O ms".split(" "),
      t = {
    transition: {
      end: {
        WebkitTransition: "webkitTransitionEnd",
        MozTransition: "transitionend",
        OTransition: "oTransitionEnd",
        transition: "transitionend"
      }
    },
    animation: {
      end: {
        WebkitAnimation: "webkitAnimationEnd",
        MozAnimation: "animationend",
        OAnimation: "oAnimationEnd",
        animation: "animationend"
      }
    }
  },
      e = function e() {
    return !!l("transform");
  },
      n = function n() {
    return !!l("perspective");
  },
      i = function i() {
    return !!l("animation");
  };

  function l(t, n) {
    var i = !1,
        e = t.charAt(0).toUpperCase() + t.slice(1);
    return o.each((t + " " + a.join(e + " ") + e).split(" "), function (t, e) {
      if (s[e] !== r) return i = !n || e, !1;
    }), i;
  }

  function c(t) {
    return l(t, !0);
  }

  !function () {
    return !!l("transition");
  }() || (o.support.transition = new String(c("transition")), o.support.transition.end = t.transition.end[o.support.transition]), i() && (o.support.animation = new String(c("animation")), o.support.animation.end = t.animation.end[o.support.animation]), e() && (o.support.transform = new String(c("transform")), o.support.transform3d = n());
}(window.Zepto || __webpack_provided_window_dot_jQuery, (window, void document)), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_0__], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function (t) {
  var e, n, i, o, p, g, m, v, s, a, l, c, u, r, h, d, f, y, _, b, w, C, x, E, T, S, A, k, D, I, N, O, P, L, j, M, H, $, q, z, R, F, W;

  (H = t)[W = "mmenu"] && "6.1.8" < H[W].version || (H[W] = function (t, e, n) {
    return this.$menu = t, this._api = ["bind", "getInstance", "initPanels", "openPanel", "closePanel", "closeAllPanels", "setSelected"], this.opts = e, this.conf = n, this.vars = {}, this.cbck = {}, this.mtch = {}, "function" == typeof this.___deprecated && this.___deprecated(), this._initAddons(), this._initExtensions(), this._initMenu(), this._initPanels(), this._initOpened(), this._initAnchors(), this._initMatchMedia(), "function" == typeof this.___debug && this.___debug(), this;
  }, H[W].version = "6.1.8", H[W].addons = {}, H[W].uniqueId = 0, H[W].defaults = {
    extensions: [],
    initMenu: function initMenu() {},
    initPanels: function initPanels() {},
    navbar: {
      add: !0,
      title: "Menu",
      titleLink: "parent"
    },
    onClick: {
      setSelected: !0
    },
    slidingSubmenus: !0
  }, H[W].configuration = {
    classNames: {
      divider: "Divider",
      inset: "Inset",
      nolistview: "NoListview",
      nopanel: "NoPanel",
      panel: "Panel",
      selected: "Selected",
      spacer: "Spacer",
      vertical: "Vertical"
    },
    clone: !1,
    openingInterval: 25,
    panelNodetype: "ul, ol, div",
    transitionDuration: 400
  }, H[W].prototype = {
    getInstance: function getInstance() {
      return this;
    },
    initPanels: function initPanels(t) {
      this._initPanels(t);
    },
    openPanel: function openPanel(t, e) {
      if (this.trigger("openPanel:before", t), t && t.length && (t.is("." + $.panel) || (t = t.closest("." + $.panel)), t.is("." + $.panel))) {
        var n = this;
        if ("boolean" != typeof e && (e = !0), t.hasClass($.vertical)) t.add(t.parents("." + $.vertical)).removeClass($.hidden).parent("li").addClass($.opened), this.openPanel(t.parents("." + $.panel).not("." + $.vertical).first()), this.trigger("openPanel:start", t), this.trigger("openPanel:finish", t);else {
          if (t.hasClass($.opened)) return;
          var i = this.$pnls.children("." + $.panel),
              o = i.filter("." + $.opened);
          if (!H[W].support.csstransitions) return o.addClass($.hidden).removeClass($.opened), t.removeClass($.hidden).addClass($.opened), this.trigger("openPanel:start", t), void this.trigger("openPanel:finish", t);
          i.not(t).removeClass($.subopened);

          for (var r = t.data(q.parent); r;) {
            (r = r.closest("." + $.panel)).is("." + $.vertical) || r.addClass($.subopened), r = r.data(q.parent);
          }

          i.removeClass($.highest).not(o).not(t).addClass($.hidden), t.removeClass($.hidden), this.openPanelStart = function () {
            o.removeClass($.opened), t.addClass($.opened), t.hasClass($.subopened) ? (o.addClass($.highest), t.removeClass($.subopened)) : (o.addClass($.subopened), t.addClass($.highest)), this.trigger("openPanel:start", t);
          }, this.openPanelFinish = function () {
            o.removeClass($.highest).addClass($.hidden), t.removeClass($.highest), this.trigger("openPanel:finish", t);
          }, e && !t.hasClass($.noanimation) ? setTimeout(function () {
            n.__transitionend(t, function () {
              n.openPanelFinish.call(n);
            }, n.conf.transitionDuration), n.openPanelStart.call(n);
          }, n.conf.openingInterval) : (this.openPanelStart.call(this), this.openPanelFinish.call(this));
        }
        this.trigger("openPanel:after", t);
      }
    },
    closePanel: function closePanel(t) {
      this.trigger("closePanel:before", t);
      var e = t.parent();
      e.hasClass($.vertical) && (e.removeClass($.opened), this.trigger("closePanel", t)), this.trigger("closePanel:after", t);
    },
    closeAllPanels: function closeAllPanels(t) {
      this.trigger("closeAllPanels:before"), this.$pnls.find("." + $.listview).children().removeClass($.selected).filter("." + $.vertical).removeClass($.opened);
      var e = this.$pnls.children("." + $.panel),
          n = t && t.length ? t : e.first();
      this.$pnls.children("." + $.panel).not(n).removeClass($.subopened).removeClass($.opened).removeClass($.highest).addClass($.hidden), this.openPanel(n, !1), this.trigger("closeAllPanels:after");
    },
    togglePanel: function togglePanel(t) {
      var e = t.parent();
      e.hasClass($.vertical) && this[e.hasClass($.opened) ? "closePanel" : "openPanel"](t);
    },
    setSelected: function setSelected(t) {
      this.trigger("setSelected:before", t), this.$menu.find("." + $.listview).children("." + $.selected).removeClass($.selected), t.addClass($.selected), this.trigger("setSelected:after", t);
    },
    bind: function bind(t, e) {
      this.cbck[t] = this.cbck[t] || [], this.cbck[t].push(e);
    },
    trigger: function trigger() {
      var t = Array.prototype.slice.call(arguments),
          e = t.shift();
      if (this.cbck[e]) for (var n = 0, i = this.cbck[e].length; n < i; n++) {
        this.cbck[e][n].apply(this, t);
      }
    },
    matchMedia: function matchMedia(t, e, n) {
      var i = {
        yes: e,
        no: n
      };
      this.mtch[t] = this.mtch[t] || [], this.mtch[t].push(i);
    },
    _initAddons: function _initAddons() {
      var t;

      for (t in this.trigger("initAddons:before"), H[W].addons) {
        H[W].addons[t].add.call(this), H[W].addons[t].add = function () {};
      }

      for (t in H[W].addons) {
        H[W].addons[t].setup.call(this);
      }

      this.trigger("initAddons:after");
    },
    _initExtensions: function _initExtensions() {
      this.trigger("initExtensions:before");
      var e = this;

      for (var t in this.opts.extensions.constructor === Array && (this.opts.extensions = {
        all: this.opts.extensions
      }), this.opts.extensions) {
        this.opts.extensions[t] = this.opts.extensions[t].length ? "mm-" + this.opts.extensions[t].join(" mm-") : "", this.opts.extensions[t] && function (t) {
          e.matchMedia(t, function () {
            this.$menu.addClass(this.opts.extensions[t]);
          }, function () {
            this.$menu.removeClass(this.opts.extensions[t]);
          });
        }(t);
      }

      this.trigger("initExtensions:after");
    },
    _initMenu: function _initMenu() {
      this.trigger("initMenu:before"), this.conf.clone && (this.$orig = this.$menu, this.$menu = this.$orig.clone(), this.$menu.add(this.$menu.find("[id]")).filter("[id]").each(function () {
        H(this).attr("id", $.mm(H(this).attr("id")));
      })), this.opts.initMenu.call(this, this.$menu, this.$orig), this.$menu.attr("id", this.$menu.attr("id") || this.__getUniqueId()), this.$pnls = H('<div class="' + $.panels + '" />').append(this.$menu.children(this.conf.panelNodetype)).prependTo(this.$menu);
      var t = [$.menu];
      this.opts.slidingSubmenus || t.push($.vertical), this.$menu.addClass(t.join(" ")).parent().addClass($.wrapper), this.trigger("initMenu:after");
    },
    _initPanels: function _initPanels(t) {
      this.trigger("initPanels:before", t), t = t || this.$pnls.children(this.conf.panelNodetype);

      var n = H(),
          i = this,
          o = function o(t) {
        t.filter(this.conf.panelNodetype).each(function () {
          var t,
              e = i._initPanel(H(this));

          e && (i._initNavbar(e), i._initListview(e), n = n.add(e), (t = e.children("." + $.listview).children("li").children(i.conf.panelNodeType).add(e.children("." + i.conf.classNames.panel))).length && o.call(i, t));
        });
      };

      o.call(this, t), this.opts.initPanels.call(this, n), this.trigger("initPanels:after", n);
    },
    _initPanel: function _initPanel(t) {
      if (this.trigger("initPanel:before", t), t.hasClass($.panel)) return t;
      if (this.__refactorClass(t, this.conf.classNames.panel, "panel"), this.__refactorClass(t, this.conf.classNames.nopanel, "nopanel"), this.__refactorClass(t, this.conf.classNames.vertical, "vertical"), this.__refactorClass(t, this.conf.classNames.inset, "inset"), t.filter("." + $.inset).addClass($.nopanel), t.hasClass($.nopanel)) return !1;
      var e = t.hasClass($.vertical) || !this.opts.slidingSubmenus;
      t.removeClass($.vertical);

      var n = t.attr("id") || this.__getUniqueId();

      t.removeAttr("id"), t.is("ul, ol") && (t.wrap("<div />"), t = t.parent()), t.addClass($.panel + " " + $.hidden).attr("id", n);
      var i = t.parent("li");
      return e ? t.add(i).addClass($.vertical) : t.appendTo(this.$pnls), i.length && (i.data(q.child, t), t.data(q.parent, i)), this.trigger("initPanel:after", t), t;
    },
    _initNavbar: function _initNavbar(t) {
      if (this.trigger("initNavbar:before", t), !t.children("." + $.navbar).length) {
        var e = t.data(q.parent),
            n = H('<div class="' + $.navbar + '" />'),
            i = H[W].i18n(this.opts.navbar.title),
            o = "";

        if (e && e.length) {
          if (e.hasClass($.vertical)) return;
          var r,
              s = (e = (r = (r = e.parent().is("." + $.listview) ? e.children("a, span").not("." + $.next) : e.closest("." + $.panel).find('a[href="#' + t.attr("id") + '"]')).first()).closest("." + $.panel)).attr("id"),
              i = r.text();

          switch (this.opts.navbar.titleLink) {
            case "anchor":
              o = r.attr("href");
              break;

            case "parent":
              o = "#" + s;
          }

          n.append('<a class="' + $.btn + " " + $.prev + '" href="#' + s + '" />');
        } else if (!this.opts.navbar.title) return;

        this.opts.navbar.add && t.addClass($.hasnavbar), n.append('<a class="' + $.title + '"' + (o.length ? ' href="' + o + '"' : "") + ">" + i + "</a>").prependTo(t), this.trigger("initNavbar:after", t);
      }
    },
    _initListview: function _initListview(t) {
      this.trigger("initListview:before", t);

      var e = this.__childAddBack(t, "ul, ol");

      this.__refactorClass(e, this.conf.classNames.nolistview, "nolistview"), e.filter("." + this.conf.classNames.inset).addClass($.nolistview);
      var n = e.not("." + $.nolistview).addClass($.listview).children();
      this.__refactorClass(n, this.conf.classNames.selected, "selected"), this.__refactorClass(n, this.conf.classNames.divider, "divider"), this.__refactorClass(n, this.conf.classNames.spacer, "spacer");
      var i,
          o,
          r = t.data(q.parent);
      r && r.parent().is("." + $.listview) && !r.children("." + $.next).length && (i = r.children("a, span").first(), o = H('<a class="' + $.next + '" href="#' + t.attr("id") + '" />').insertBefore(i), i.is("span") && o.addClass($.fullsubopen)), this.trigger("initListview:after", t);
    },
    _initOpened: function _initOpened() {
      this.trigger("initOpened:before");
      var t = this.$pnls.find("." + $.listview).children("." + $.selected).removeClass($.selected).last().addClass($.selected),
          e = t.length ? t.closest("." + $.panel) : this.$pnls.children("." + $.panel).first();
      this.openPanel(e, !1), this.trigger("initOpened:after");
    },
    _initAnchors: function _initAnchors() {
      var l = this;
      R.$body.on(z.click + "-oncanvas", "a[href]", function (t) {
        var e = H(this),
            n = !1,
            i = l.$menu.find(e).length;

        for (var o in H[W].addons) {
          if (H[W].addons[o].clickAnchor.call(l, e, i)) {
            n = !0;
            break;
          }
        }

        var r,
            s = e.attr("href");
        if (!n && i && 1 < s.length && "#" == s.slice(0, 1)) try {
          var a = H(s, l.$menu);
          a.is("." + $.panel) && (n = !0, l[e.parent().hasClass($.vertical) ? "togglePanel" : "openPanel"](a));
        } catch (t) {}
        n && t.preventDefault(), n || !i || !e.is("." + $.listview + " > li > a") || e.is('[rel="external"]') || e.is('[target="_blank"]') || (l.__valueOrFn(l.opts.onClick.setSelected, e) && l.setSelected(H(t.target).parent()), (r = l.__valueOrFn(l.opts.onClick.preventDefault, e, "#" == s.slice(0, 1))) && t.preventDefault(), l.__valueOrFn(l.opts.onClick.close, e, r) && l.opts.offCanvas && "function" == typeof l.close && l.close());
      });
    },
    _initMatchMedia: function _initMatchMedia() {
      var e = this;
      this._fireMatchMedia(), R.$wndw.on(z.resize, function (t) {
        e._fireMatchMedia();
      });
    },
    _fireMatchMedia: function _fireMatchMedia() {
      for (var t in this.mtch) {
        for (var e = window.matchMedia && window.matchMedia(t).matches ? "yes" : "no", n = 0; n < this.mtch[t].length; n++) {
          this.mtch[t][n][e].call(this);
        }
      }
    },
    _getOriginalMenuId: function _getOriginalMenuId() {
      var t = this.$menu.attr("id");
      return this.conf.clone && t && t.length && (t = $.umm(t)), t;
    },
    __api: function __api() {
      var n = this,
          i = {};
      return H.each(this._api, function (t) {
        var e = this;

        i[e] = function () {
          var t = n[e].apply(n, arguments);
          return void 0 === t ? i : t;
        };
      }), i;
    },
    __valueOrFn: function __valueOrFn(t, e, n) {
      return "function" == typeof t ? t.call(e[0]) : void 0 === t && void 0 !== n ? n : t;
    },
    __refactorClass: function __refactorClass(t, e, n) {
      return t.filter("." + e).removeClass(e).addClass($[n]);
    },
    __findAddBack: function __findAddBack(t, e) {
      return t.find(e).add(t.filter(e));
    },
    __childAddBack: function __childAddBack(t, e) {
      return t.children(e).add(t.filter(e));
    },
    __filterListItems: function __filterListItems(t) {
      return t.not("." + $.divider).not("." + $.hidden);
    },
    __filterListItemAnchors: function __filterListItemAnchors(t) {
      return this.__filterListItems(t).children("a").not("." + $.next);
    },
    __transitionend: function __transitionend(e, n, t) {
      function i(t) {
        void 0 !== t && t.target != e[0] || (o || (e.off(z.transitionend), e.off(z.webkitTransitionEnd), n.call(e[0])), o = !0);
      }

      var o = !1;
      e.on(z.transitionend, i), e.on(z.webkitTransitionEnd, i), setTimeout(i, 1.1 * t);
    },
    __getUniqueId: function __getUniqueId() {
      return $.mm(H[W].uniqueId++);
    }
  }, H.fn[W] = function (n, i) {
    H[W].glbl || (R = {
      $wndw: H(window),
      $docu: H(document),
      $html: H("html"),
      $body: H("body")
    }, $ = {}, q = {}, z = {}, H.each([$, q, z], function (t, i) {
      i.add = function (t) {
        for (var e = 0, n = (t = t.split(" ")).length; e < n; e++) {
          i[t[e]] = i.mm(t[e]);
        }
      };
    }), $.mm = function (t) {
      return "mm-" + t;
    }, $.add("wrapper menu panels panel nopanel highest opened subopened navbar hasnavbar title btn prev next listview nolistview inset vertical selected divider spacer hidden fullsubopen noanimation"), $.umm = function (t) {
      return "mm-" == t.slice(0, 3) && (t = t.slice(3)), t;
    }, q.mm = function (t) {
      return "mm-" + t;
    }, q.add("parent child"), z.mm = function (t) {
      return t + ".mm";
    }, z.add("transitionend webkitTransitionEnd click scroll resize keydown mousedown mouseup touchstart touchmove touchend orientationchange"), H[W]._c = $, H[W]._d = q, H[W]._e = z, H[W].glbl = R), n = H.extend(!0, {}, H[W].defaults, n), i = H.extend(!0, {}, H[W].configuration, i);
    var o = H();
    return this.each(function () {
      var t,
          e = H(this);
      e.data(W) || ((t = new H[W](e, n, i)).$menu.data(W, t.__api()), o = o.add(t.$menu));
    }), o;
  }, H[W].i18n = (F = {}, function (t) {
    switch (_typeof2(t)) {
      case "object":
        return H.extend(F, t), F;

      case "string":
        return F[t] || t;

      case "undefined":
      default:
        return F;
    }
  }), H[W].support = {
    touch: "ontouchstart" in window || navigator.msMaxTouchPoints || !1,
    csstransitions: "undefined" == typeof Modernizr || void 0 === Modernizr.csstransitions || Modernizr.csstransitions,
    csstransforms: "undefined" == typeof Modernizr || void 0 === Modernizr.csstransforms || Modernizr.csstransforms,
    csstransforms3d: "undefined" == typeof Modernizr || void 0 === Modernizr.csstransforms3d || Modernizr.csstransforms3d
  }), M = "offCanvas", (I = t)[j = "mmenu"].addons[M] = {
    setup: function setup() {
      var i, t, o, r;
      this.opts[M] && (t = (i = this).opts[M], o = this.conf[M], L = I[j].glbl, this._api = I.merge(this._api, ["open", "close", "setPage"]), "object" != _typeof2(t) && (t = {}), "top" != t.position && "bottom" != t.position || (t.zposition = "front"), t = this.opts[M] = I.extend(!0, {}, I[j].defaults[M], t), "string" != typeof o.pageSelector && (o.pageSelector = "> " + o.pageNodetype), this.vars.opened = !1, r = [N.offcanvas], "left" != t.position && r.push(N.mm(t.position)), "back" != t.zposition && r.push(N.mm(t.zposition)), I[j].support.csstransforms || r.push(N["no-csstransforms"]), I[j].support.csstransforms3d || r.push(N["no-csstransforms3d"]), this.bind("initMenu:after", function () {
        var t = this;
        this.setPage(L.$page), this._initBlocker(), this["_initWindow_" + M](), this.$menu.addClass(r.join(" ")).parent("." + N.wrapper).removeClass(N.wrapper), this.$menu[o.menuInsertMethod](o.menuInsertSelector);
        var e,
            n = window.location.hash;
        !n || (e = this._getOriginalMenuId()) && e == n.slice(1) && setTimeout(function () {
          t.open();
        }, 1e3);
      }), this.bind("initExtensions:after", function () {
        for (var e = [N.mm("widescreen"), N.mm("iconbar")], t = 0; t < e.length; t++) {
          for (var n in this.opts.extensions) {
            if (-1 < this.opts.extensions[n].indexOf(e[t])) {
              !function (t) {
                i.matchMedia(n, function () {
                  L.$html.addClass(e[t]);
                }, function () {
                  L.$html.removeClass(e[t]);
                });
              }(t);
              break;
            }
          }
        }
      }), this.bind("open:start:sr-aria", function () {
        this.__sr_aria(this.$menu, "hidden", !1);
      }), this.bind("close:finish:sr-aria", function () {
        this.__sr_aria(this.$menu, "hidden", !0);
      }), this.bind("initMenu:after:sr-aria", function () {
        this.__sr_aria(this.$menu, "hidden", !0);
      }));
    },
    add: function add() {
      N = I[j]._c, O = I[j]._d, P = I[j]._e, N.add("offcanvas slideout blocking modal background opening blocker page no-csstransforms3d"), O.add("style");
    },
    clickAnchor: function clickAnchor(t, e) {
      var n = this;

      if (this.opts[M]) {
        var i = this._getOriginalMenuId();

        if (i && t.is('[href="#' + i + '"]')) {
          if (e) return !0;
          var o = t.closest("." + N.menu);

          if (o.length) {
            var r = o.data("mmenu");
            if (r && r.close) return r.close(), n.__transitionend(o, function () {
              n.open();
            }, n.conf.transitionDuration), !0;
          }

          return this.open(), !0;
        }

        if (L.$page) return (i = L.$page.first().attr("id")) && t.is('[href="#' + i + '"]') ? (this.close(), !0) : void 0;
      }
    }
  }, I[j].defaults[M] = {
    position: "left",
    zposition: "back",
    blockUI: !0,
    moveBackground: !0
  }, I[j].configuration[M] = {
    pageNodetype: "div",
    pageSelector: null,
    noPageSelector: [],
    wrapPageIfNeeded: !0,
    menuInsertMethod: "prependTo",
    menuInsertSelector: "body"
  }, I[j].prototype.open = function () {
    var t;
    this.trigger("open:before"), this.vars.opened || ((t = this)._openSetup(), setTimeout(function () {
      t._openFinish();
    }, this.conf.openingInterval), this.trigger("open:after"));
  }, I[j].prototype._openSetup = function () {
    var t = this,
        e = this.opts[M];
    this.closeAllOthers(), L.$page.each(function () {
      I(this).data(O.style, I(this).attr("style") || "");
    }), L.$wndw.trigger(P.resize + "-" + M, [!0]);
    var n = [N.opened];
    e.blockUI && n.push(N.blocking), "modal" == e.blockUI && n.push(N.modal), e.moveBackground && n.push(N.background), "left" != e.position && n.push(N.mm(this.opts[M].position)), "back" != e.zposition && n.push(N.mm(this.opts[M].zposition)), L.$html.addClass(n.join(" ")), setTimeout(function () {
      t.vars.opened = !0;
    }, this.conf.openingInterval), this.$menu.addClass(N.opened);
  }, I[j].prototype._openFinish = function () {
    var t = this;
    this.__transitionend(L.$page.first(), function () {
      t.trigger("open:finish");
    }, this.conf.transitionDuration), this.trigger("open:start"), L.$html.addClass(N.opening);
  }, I[j].prototype.close = function () {
    var e;
    this.trigger("close:before"), this.vars.opened && ((e = this).__transitionend(L.$page.first(), function () {
      e.$menu.removeClass(N.opened);
      var t = [N.opened, N.blocking, N.modal, N.background, N.mm(e.opts[M].position), N.mm(e.opts[M].zposition)];
      L.$html.removeClass(t.join(" ")), L.$page.each(function () {
        I(this).attr("style", I(this).data(O.style));
      }), e.vars.opened = !1, e.trigger("close:finish");
    }, this.conf.transitionDuration), this.trigger("close:start"), L.$html.removeClass(N.opening), this.trigger("close:after"));
  }, I[j].prototype.closeAllOthers = function () {
    L.$body.find("." + N.menu + "." + N.offcanvas).not(this.$menu).each(function () {
      var t = I(this).data(j);
      t && t.close && t.close();
    });
  }, I[j].prototype.setPage = function (t) {
    this.trigger("setPage:before", t);
    var e = this,
        n = this.conf[M];
    t && t.length || (t = L.$body.find(n.pageSelector), n.noPageSelector.length && (t = t.not(n.noPageSelector.join(", "))), 1 < t.length && n.wrapPageIfNeeded && (t = t.wrapAll("<" + this.conf[M].pageNodetype + " />").parent())), t.each(function () {
      I(this).attr("id", I(this).attr("id") || e.__getUniqueId());
    }), t.addClass(N.page + " " + N.slideout), L.$page = t, this.trigger("setPage:after", t);
  }, I[j].prototype["_initWindow_" + M] = function () {
    L.$wndw.off(P.keydown + "-" + M).on(P.keydown + "-" + M, function (t) {
      if (L.$html.hasClass(N.opened) && 9 == t.keyCode) return t.preventDefault(), !1;
    });
    var i = 0;
    L.$wndw.off(P.resize + "-" + M).on(P.resize + "-" + M, function (t, e) {
      var n;
      1 == L.$page.length && (e || L.$html.hasClass(N.opened)) && (n = L.$wndw.height(), !e && n == i || (i = n, L.$page.css("minHeight", n)));
    });
  }, I[j].prototype._initBlocker = function () {
    var e = this;
    this.opts[M].blockUI && (L.$blck || (L.$blck = I('<div id="' + N.blocker + '" class="' + N.slideout + '" />')), L.$blck.appendTo(L.$body).off(P.touchstart + "-" + M + " " + P.touchmove + "-" + M).on(P.touchstart + "-" + M + " " + P.touchmove + "-" + M, function (t) {
      t.preventDefault(), t.stopPropagation(), L.$blck.trigger(P.mousedown + "-" + M);
    }).off(P.mousedown + "-" + M).on(P.mousedown + "-" + M, function (t) {
      t.preventDefault(), L.$html.hasClass(N.modal) || (e.closeAllOthers(), e.close());
    }));
  }, D = "scrollBugFix", (E = t)[k = "mmenu"].addons[D] = {
    setup: function setup() {
      var t = this.opts[D];
      this.conf[D], A = E[k].glbl, E[k].support.touch && this.opts.offCanvas && this.opts.offCanvas.blockUI && ("boolean" == typeof t && (t = {
        fix: t
      }), "object" != _typeof2(t) && (t = {}), (t = this.opts[D] = E.extend(!0, {}, E[k].defaults[D], t)).fix && (this.bind("open:start", function () {
        this.$pnls.children("." + T.opened).scrollTop(0);
      }), this.bind("initMenu:after", function () {
        this["_initWindow_" + D]();
      })));
    },
    add: function add() {
      T = E[k]._c, E[k]._d, S = E[k]._e;
    },
    clickAnchor: function clickAnchor(t, e) {}
  }, E[k].defaults[D] = {
    fix: !0
  }, E[k].prototype["_initWindow_" + D] = function () {
    var t = this;
    A.$docu.off(S.touchmove + "-" + D).on(S.touchmove + "-" + D, function (t) {
      A.$html.hasClass(T.opened) && t.preventDefault();
    });
    var e = !1;
    A.$body.off(S.touchstart + "-" + D).on(S.touchstart + "-" + D, "." + T.panels + "> ." + T.panel, function (t) {
      A.$html.hasClass(T.opened) && (e || (e = !0, 0 === t.currentTarget.scrollTop ? t.currentTarget.scrollTop = 1 : t.currentTarget.scrollHeight === t.currentTarget.scrollTop + t.currentTarget.offsetHeight && --t.currentTarget.scrollTop, e = !1));
    }).off(S.touchmove + "-" + D).on(S.touchmove + "-" + D, "." + T.panels + "> ." + T.panel, function (t) {
      A.$html.hasClass(T.opened) && E(this)[0].scrollHeight > E(this).innerHeight() && t.stopPropagation();
    }), A.$wndw.off(S.orientationchange + "-" + D).on(S.orientationchange + "-" + D, function () {
      t.$pnls.children("." + T.opened).scrollTop(0).css({
        "-webkit-overflow-scrolling": "auto"
      }).css({
        "-webkit-overflow-scrolling": "touch"
      });
    });
  }, x = "screenReader", (_ = t)[C = "mmenu"].addons[x] = {
    setup: function setup() {
      var r = this,
          t = this.opts[x],
          s = this.conf[x];
      _[C].glbl, "boolean" == typeof t && (t = {
        aria: t,
        text: t
      }), "object" != _typeof2(t) && (t = {}), (t = this.opts[x] = _.extend(!0, {}, _[C].defaults[x], t)).aria && (this.bind("initAddons:after", function () {
        this.bind("initMenu:after", function () {
          this.trigger("initMenu:after:sr-aria");
        }), this.bind("initNavbar:after", function () {
          this.trigger("initNavbar:after:sr-aria", arguments[0]);
        }), this.bind("openPanel:start", function () {
          this.trigger("openPanel:start:sr-aria", arguments[0]);
        }), this.bind("close:start", function () {
          this.trigger("close:start:sr-aria");
        }), this.bind("close:finish", function () {
          this.trigger("close:finish:sr-aria");
        }), this.bind("open:start", function () {
          this.trigger("open:start:sr-aria");
        }), this.bind("open:finish", function () {
          this.trigger("open:finish:sr-aria");
        });
      }), this.bind("updateListview", function () {
        this.$pnls.find("." + b.listview).children().each(function () {
          r.__sr_aria(_(this), "hidden", _(this).is("." + b.hidden));
        });
      }), this.bind("openPanel:start", function (t) {
        var e = this.$menu.find("." + b.panel).not(t).not(t.parents("." + b.panel)),
            n = t.add(t.find("." + b.vertical + "." + b.opened).children("." + b.panel));
        this.__sr_aria(e, "hidden", !0), this.__sr_aria(n, "hidden", !1);
      }), this.bind("closePanel", function (t) {
        this.__sr_aria(t, "hidden", !0);
      }), this.bind("initPanels:after", function (t) {
        var e = t.find("." + b.prev + ", ." + b.next).each(function () {
          r.__sr_aria(_(this), "owns", _(this).attr("href").replace("#", ""));
        });

        this.__sr_aria(e, "haspopup", !0);
      }), this.bind("initNavbar:after", function (t) {
        var e = t.children("." + b.navbar);

        this.__sr_aria(e, "hidden", !t.hasClass(b.hasnavbar));
      }), t.text && (this.bind("initlistview:after", function (t) {
        var e = t.find("." + b.listview).find("." + b.fullsubopen).parent().children("span");

        this.__sr_aria(e, "hidden", !0);
      }), "parent" == this.opts.navbar.titleLink && this.bind("initNavbar:after", function (t) {
        var e = t.children("." + b.navbar),
            n = !!e.children("." + b.prev).length;

        this.__sr_aria(e.children("." + b.title), "hidden", n);
      }))), t.text && (this.bind("initAddons:after", function () {
        this.bind("setPage:after", function () {
          this.trigger("setPage:after:sr-text", arguments[0]);
        });
      }), this.bind("initNavbar:after", function (t) {
        var e = t.children("." + b.navbar),
            n = e.children("." + b.title).text(),
            i = _[C].i18n(s.text.closeSubmenu);

        n && (i += " (" + n + ")"), e.children("." + b.prev).html(this.__sr_text(i));
      }), this.bind("initListview:after", function (t) {
        var e,
            n,
            i,
            o = t.data(w.parent);
        o && o.length && (n = (e = o.children("." + b.next)).nextAll("span, a").first().text(), i = _[C].i18n(s.text[e.parent().is("." + b.vertical) ? "toggleSubmenu" : "openSubmenu"]), n && (i += " (" + n + ")"), e.html(r.__sr_text(i)));
      }));
    },
    add: function add() {
      b = _[C]._c, w = _[C]._d, _[C]._e, b.add("sronly");
    },
    clickAnchor: function clickAnchor(t, e) {}
  }, _[C].defaults[x] = {
    aria: !0,
    text: !0
  }, _[C].configuration[x] = {
    text: {
      closeMenu: "Close menu",
      closeSubmenu: "Close submenu",
      openSubmenu: "Open submenu",
      toggleSubmenu: "Toggle submenu"
    }
  }, _[C].prototype.__sr_aria = function (t, e, n) {
    t.prop("aria-" + e, n)[n ? "attr" : "removeAttr"]("aria-" + e, n);
  }, _[C].prototype.__sr_text = function (t) {
    return '<span class="' + b.sronly + '">' + t + "</span>";
  }, y = "autoHeight", (r = t)[f = "mmenu"].addons[y] = {
    setup: function setup() {
      var t,
          o = this.opts[y];
      this.conf[y], r[f].glbl, "boolean" == typeof o && o && (o = {
        height: "auto"
      }), "string" == typeof o && (o = {
        height: o
      }), "object" != _typeof2(o) && (o = {}), "auto" != (o = this.opts[y] = r.extend(!0, {}, r[f].defaults[y], o)).height && "highest" != o.height || (this.bind("initMenu:after", function () {
        this.$menu.addClass(h.autoheight);
      }), t = function t(_t2) {
        var e, n, i;
        this.opts.offCanvas && !this.vars.opened || (e = Math.max(parseInt(this.$pnls.css("top"), 10), 0) || 0, n = Math.max(parseInt(this.$pnls.css("bottom"), 10), 0) || 0, i = 0, this.$menu.addClass(h.measureheight), "auto" == o.height ? ((_t2 = _t2 || this.$pnls.children("." + h.opened)).is("." + h.vertical) && (_t2 = _t2.parents("." + h.panel).not("." + h.vertical)), _t2.length || (_t2 = this.$pnls.children("." + h.panel)), i = _t2.first().outerHeight()) : "highest" == o.height && this.$pnls.children().each(function () {
          var t = r(this);
          t.is("." + h.vertical) && (t = t.parents("." + h.panel).not("." + h.vertical).first()), i = Math.max(i, t.outerHeight());
        }), this.$menu.height(i + e + n).removeClass(h.measureheight));
      }, this.opts.offCanvas && this.bind("open:start", t), "highest" == o.height && this.bind("initPanels:after", t), "auto" == o.height && (this.bind("updateListview", t), this.bind("openPanel:start", t), this.bind("closePanel", t)));
    },
    add: function add() {
      h = r[f]._c, r[f]._d, d = r[f]._e, h.add("autoheight measureheight"), d.add("resize");
    },
    clickAnchor: function clickAnchor(t, e) {}
  }, r[f].defaults[y] = {
    height: "default"
  }, u = "fixedElements", (s = t)[c = "mmenu"].addons[u] = {
    setup: function setup() {
      var r;
      this.opts.offCanvas && (this.opts[u], r = this.conf[u], l = s[c].glbl, this.bind("setPage:after", function (t) {
        var e = this.conf.classNames[u].fixed,
            n = t.find("." + e);
        this.__refactorClass(n, e, "slideout"), n[r.elemInsertMethod](r.elemInsertSelector);
        var i = this.conf.classNames[u].sticky,
            o = t.find("." + i);
        this.__refactorClass(o, i, "sticky"), (o = t.find("." + a.sticky)).length && (this.bind("open:before", function () {
          var t = l.$wndw.scrollTop() + r.sticky.offset;
          o.each(function () {
            s(this).css("top", parseInt(s(this).css("top"), 10) + t);
          });
        }), this.bind("close:finish", function () {
          o.css("top", "");
        }));
      }));
    },
    add: function add() {
      a = s[c]._c, s[c]._d, s[c]._e, a.add("sticky");
    },
    clickAnchor: function clickAnchor(t, e) {}
  }, s[c].configuration[u] = {
    sticky: {
      offset: 0
    },
    elemInsertMethod: "appendTo",
    elemInsertSelector: "body"
  }, s[c].configuration.classNames[u] = {
    fixed: "Fixed",
    sticky: "Sticky"
  }, v = "navbars", (p = t)[m = "mmenu"].addons[v] = {
    setup: function setup() {
      var c,
          u,
          h = this,
          d = this.opts[v],
          f = this.conf[v];
      p[m].glbl, void 0 !== d && (d instanceof Array || (d = [d]), c = {}, u = {}, d.length && (p.each(d, function (t) {
        var e = d[t];
        "boolean" == typeof e && e && (e = {}), "object" != _typeof2(e) && (e = {}), void 0 === e.content && (e.content = ["prev", "title"]), e.content instanceof Array || (e.content = [e.content]), e = p.extend(!0, {}, h.opts.navbar, e);
        var n = p('<div class="' + g.navbar + '" />'),
            i = e.height;
        "number" != typeof i && (i = 1), i = Math.min(4, Math.max(1, i)), n.addClass(g.navbar + "-size-" + i);
        var o = e.position;
        "bottom" != o && (o = "top"), c[o] || (c[o] = 0), c[o] += i, u[o] || (u[o] = p('<div class="' + g.navbars + "-" + o + '" />')), u[o].append(n);

        for (var r = 0, s = 0, a = e.content.length; s < a; s++) {
          var l = p[m].addons[v][e.content[s]] || !1;
          l ? r += l.call(h, n, e, f) : ((l = e.content[s]) instanceof p || (l = p(e.content[s])), n.append(l));
        }

        1 < (r += Math.ceil(n.children().not("." + g.btn).length / i)) && n.addClass(g.navbar + "-content-" + r), n.children("." + g.btn).length && n.addClass(g.hasbtns);
      }), this.bind("initMenu:after", function () {
        for (var t in c) {
          this.$menu.addClass(g.hasnavbar + "-" + t + "-" + c[t]), this.$menu["bottom" == t ? "append" : "prepend"](u[t]);
        }
      })));
    },
    add: function add() {
      g = p[m]._c, p[m]._d, p[m]._e, g.add("navbars close hasbtns");
    },
    clickAnchor: function clickAnchor(t, e) {}
  }, p[m].configuration[v] = {
    breadcrumbSeparator: "/"
  }, p[m].configuration.classNames[v] = {}, (e = t)[o = "mmenu"].addons.rtl = {
    setup: function setup() {
      var t = this.opts.rtl;
      this.conf.rtl, i = e[o].glbl, "object" != _typeof2(t) && (t = {
        use: t
      }), "boolean" != typeof (t = this.opts.rtl = e.extend(!0, {}, e[o].defaults.rtl, t)).use && (t.use = "rtl" == (i.$html.attr("dir") || "").toLowerCase()), t.use && this.bind("initMenu:after", function () {
        this.$menu.addClass(n.rtl);
      });
    },
    add: function add() {
      n = e[o]._c, e[o]._d, e[o]._e, n.add("rtl");
    },
    clickAnchor: function clickAnchor(t, e) {}
  }, e[o].defaults.rtl = {
    use: "detect"
  };
}), function (t) {
  "use strict";

   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_0__], __WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(function (o) {
  "use strict";

  var r = [],
      e = [],
      i = {
    precision: 100,
    elapse: !1,
    defer: !1
  };
  e.push(/^[0-9]*$/.source), e.push(/([0-9]{1,2}\/){2}[0-9]{4}( [0-9]{1,2}(:[0-9]{2}){2})?/.source), e.push(/[0-9]{4}([\/\-][0-9]{1,2}){2}( [0-9]{1,2}(:[0-9]{2}){2})?/.source), e = new RegExp(e.join("|"));
  var d = {
    Y: "years",
    m: "months",
    n: "daysToMonth",
    d: "daysToWeek",
    w: "weeks",
    W: "weeksToMonth",
    H: "hours",
    M: "minutes",
    S: "seconds",
    D: "totalDays",
    I: "totalHours",
    N: "totalMinutes",
    T: "totalSeconds"
  };

  function n(h) {
    return function (t) {
      var e,
          n,
          i = t.match(/%(-|!)?[A-Z]{1}(:[^;]+;)?/gi);
      if (i) for (var o = 0, r = i.length; o < r; ++o) {
        var s = i[o].match(/%(-|!)?([a-zA-Z]{1})(:[^;]+;)?/),
            a = (e = s[0], n = e.toString().replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1"), new RegExp(n)),
            l = s[1] || "",
            c = s[3] || "",
            u = null,
            s = s[2];
        d.hasOwnProperty(s) && (u = d[s], u = Number(h[u])), null !== u && ("!" === l && (u = function (t, e) {
          var n = "s",
              i = "";
          t && (t = t.replace(/(:|;|\s)/gi, "").split(/\,/), n = 1 === t.length ? t[0] : (i = t[0], t[1]));
          return 1 < Math.abs(e) ? n : i;
        }(c, u)), "" === l && u < 10 && (u = "0" + u.toString()), t = t.replace(a, u.toString()));
      }
      return t = t.replace(/%%/, "%");
    };
  }

  function s(t, e, n) {
    this.el = t, this.$el = o(t), this.interval = null, this.offset = {}, this.options = o.extend({}, i), this.firstTick = !0, this.instanceNumber = r.length, r.push(this), this.$el.data("countdown-instance", this.instanceNumber), n && ("function" == typeof n ? (this.$el.on("update.countdown", n), this.$el.on("stoped.countdown", n), this.$el.on("finish.countdown", n)) : this.options = o.extend({}, i, n)), this.setFinalDate(e), !1 === this.options.defer && this.start();
  }

  o.extend(s.prototype, {
    start: function start() {
      null !== this.interval && clearInterval(this.interval);
      var t = this;
      this.update(), this.interval = setInterval(function () {
        t.update.call(t);
      }, this.options.precision);
    },
    stop: function stop() {
      clearInterval(this.interval), this.interval = null, this.dispatchEvent("stoped");
    },
    toggle: function toggle() {
      this.interval ? this.stop() : this.start();
    },
    pause: function pause() {
      this.stop();
    },
    resume: function resume() {
      this.start();
    },
    remove: function remove() {
      this.stop.call(this), r[this.instanceNumber] = null, delete this.$el.data().countdownInstance;
    },
    setFinalDate: function setFinalDate(t) {
      this.finalDate = function (t) {
        if (t instanceof Date) return t;
        if (String(t).match(e)) return String(t).match(/^[0-9]*$/) && (t = Number(t)), String(t).match(/\-/) && (t = String(t).replace(/\-/g, "/")), new Date(t);
        throw new Error("Couldn't cast `" + t + "` to a date object.");
      }(t);
    },
    update: function update() {
      var t, e;
      0 !== this.$el.closest("html").length ? (t = new Date(), e = this.finalDate.getTime() - t.getTime(), e = Math.ceil(e / 1e3), e = !this.options.elapse && e < 0 ? 0 : Math.abs(e), this.totalSecsLeft === e || this.firstTick ? this.firstTick = !1 : (this.totalSecsLeft = e, this.elapsed = t >= this.finalDate, this.offset = {
        seconds: this.totalSecsLeft % 60,
        minutes: Math.floor(this.totalSecsLeft / 60) % 60,
        hours: Math.floor(this.totalSecsLeft / 60 / 60) % 24,
        days: Math.floor(this.totalSecsLeft / 60 / 60 / 24) % 7,
        daysToWeek: Math.floor(this.totalSecsLeft / 60 / 60 / 24) % 7,
        daysToMonth: Math.floor(this.totalSecsLeft / 60 / 60 / 24 % 30.4368),
        weeks: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 7),
        weeksToMonth: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 7) % 4,
        months: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 30.4368),
        years: Math.abs(this.finalDate.getFullYear() - t.getFullYear()),
        totalDays: Math.floor(this.totalSecsLeft / 60 / 60 / 24),
        totalHours: Math.floor(this.totalSecsLeft / 60 / 60),
        totalMinutes: Math.floor(this.totalSecsLeft / 60),
        totalSeconds: this.totalSecsLeft
      }, this.options.elapse || 0 !== this.totalSecsLeft ? this.dispatchEvent("update") : (this.stop(), this.dispatchEvent("finish")))) : this.remove();
    },
    dispatchEvent: function dispatchEvent(t) {
      var e = o.Event(t + ".countdown");
      e.finalDate = this.finalDate, e.elapsed = this.elapsed, e.offset = o.extend({}, this.offset), e.strftime = n(this.offset), this.$el.trigger(e);
    }
  }), o.fn.countdown = function () {
    var i = Array.prototype.slice.call(arguments, 0);
    return this.each(function () {
      var t,
          e,
          n = o(this).data("countdown-instance");
      void 0 !== n ? (t = r[n], e = i[0], s.prototype.hasOwnProperty(e) ? t[e].apply(t, i.slice(1)) : null === String(e).match(/^[$A-Z_][0-9A-Z_$]*$/i) ? (t.setFinalDate.call(t, e), t.start()) : o.error("Method %s does not exist on jQuery.countdown".replace(/\%s/gi, e))) : new s(this, i[0], i[1]);
    });
  };
}), function (t) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_0__], __WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(function (u) {
  function t() {}

  function h(t, e) {
    g.ev.on("mfp" + t + b, e);
  }

  function d(t, e, n, i) {
    var o = document.createElement("div");
    return o.className = "mfp-" + t, n && (o.innerHTML = n), i ? e && e.appendChild(o) : (o = u(o), e && o.appendTo(e)), o;
  }

  function f(t, e) {
    g.ev.triggerHandler("mfp" + t, e), g.st.callbacks && (t = t.charAt(0).toLowerCase() + t.slice(1), g.st.callbacks[t] && g.st.callbacks[t].apply(g, u.isArray(e) ? e : [e]));
  }

  function p(t) {
    return t === e && g.currTemplate.closeBtn || (g.currTemplate.closeBtn = u(g.st.closeMarkup.replace("%title%", g.st.tClose)), e = t), g.currTemplate.closeBtn;
  }

  function r() {
    u.magnificPopup.instance || ((g = new t()).init(), u.magnificPopup.instance = g);
  }

  var g,
      i,
      m,
      o,
      v,
      e,
      l = "Close",
      c = "BeforeClose",
      y = "MarkupParse",
      _ = "Open",
      b = ".mfp",
      w = "mfp-ready",
      n = "mfp-removing",
      s = "mfp-prevent-close",
      a = !!__webpack_provided_window_dot_jQuery,
      C = u(window);
  t.prototype = {
    constructor: t,
    init: function init() {
      var t = navigator.appVersion;
      g.isLowIE = g.isIE8 = document.all && !document.addEventListener, g.isAndroid = /android/gi.test(t), g.isIOS = /iphone|ipad|ipod/gi.test(t), g.supportsTransition = function () {
        var t = document.createElement("p").style,
            e = ["ms", "O", "Moz", "Webkit"];
        if (void 0 !== t.transition) return !0;

        for (; e.length;) {
          if (e.pop() + "Transition" in t) return !0;
        }

        return !1;
      }(), g.probablyMobile = g.isAndroid || g.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), m = u(document), g.popupsCache = {};
    },
    open: function open(t) {
      if (!1 === t.isObj) {
        g.items = t.items.toArray(), g.index = 0;

        for (var e, n = t.items, i = 0; i < n.length; i++) {
          if ((e = n[i]).parsed && (e = e.el[0]), e === t.el[0]) {
            g.index = i;
            break;
          }
        }
      } else g.items = u.isArray(t.items) ? t.items : [t.items], g.index = t.index || 0;

      if (!g.isOpen) {
        g.types = [], v = "", t.mainEl && t.mainEl.length ? g.ev = t.mainEl.eq(0) : g.ev = m, t.key ? (g.popupsCache[t.key] || (g.popupsCache[t.key] = {}), g.currTemplate = g.popupsCache[t.key]) : g.currTemplate = {}, g.st = u.extend(!0, {}, u.magnificPopup.defaults, t), g.fixedContentPos = "auto" === g.st.fixedContentPos ? !g.probablyMobile : g.st.fixedContentPos, g.st.modal && (g.st.closeOnContentClick = !1, g.st.closeOnBgClick = !1, g.st.showCloseBtn = !1, g.st.enableEscapeKey = !1), g.bgOverlay || (g.bgOverlay = d("bg").on("click" + b, function () {
          g.close();
        }), g.wrap = d("wrap").attr("tabindex", -1).on("click" + b, function (t) {
          g._checkIfClose(t.target) && g.close();
        }), g.container = d("container", g.wrap)), g.contentContainer = d("content"), g.st.preloader && (g.preloader = d("preloader", g.container, g.st.tLoading));
        var o = u.magnificPopup.modules;

        for (i = 0; i < o.length; i++) {
          var r = (r = o[i]).charAt(0).toUpperCase() + r.slice(1);
          g["init" + r].call(g);
        }

        f("BeforeOpen"), g.st.showCloseBtn && (g.st.closeBtnInside ? (h(y, function (t, e, n, i) {
          n.close_replaceWith = p(i.type);
        }), v += " mfp-close-btn-in") : g.wrap.append(p())), g.st.alignTop && (v += " mfp-align-top"), g.fixedContentPos ? g.wrap.css({
          overflow: g.st.overflowY,
          overflowX: "hidden",
          overflowY: g.st.overflowY
        }) : g.wrap.css({
          top: C.scrollTop(),
          position: "absolute"
        }), !1 !== g.st.fixedBgPos && ("auto" !== g.st.fixedBgPos || g.fixedContentPos) || g.bgOverlay.css({
          height: m.height(),
          position: "absolute"
        }), g.st.enableEscapeKey && m.on("keyup" + b, function (t) {
          27 === t.keyCode && g.close();
        }), C.on("resize" + b, function () {
          g.updateSize();
        }), g.st.closeOnContentClick || (v += " mfp-auto-cursor"), v && g.wrap.addClass(v);
        var s,
            a = g.wH = C.height(),
            l = {};
        g.fixedContentPos && (!g._hasScrollBar(a) || (s = g._getScrollbarSize()) && (l.marginRight = s)), g.fixedContentPos && (g.isIE7 ? u("body, html").css("overflow", "hidden") : l.overflow = "hidden");
        var c = g.st.mainClass;
        return g.isIE7 && (c += " mfp-ie7"), c && g._addClassToMFP(c), g.updateItemHTML(), f("BuildControls"), u("html").css(l), g.bgOverlay.add(g.wrap).prependTo(g.st.prependTo || u(document.body)), g._lastFocusedEl = document.activeElement, setTimeout(function () {
          g.content ? (g._addClassToMFP(w), g._setFocus()) : g.bgOverlay.addClass(w), m.on("focusin" + b, g._onFocusIn);
        }, 16), g.isOpen = !0, g.updateSize(a), f(_), t;
      }

      g.updateItemHTML();
    },
    close: function close() {
      g.isOpen && (f(c), g.isOpen = !1, g.st.removalDelay && !g.isLowIE && g.supportsTransition ? (g._addClassToMFP(n), setTimeout(function () {
        g._close();
      }, g.st.removalDelay)) : g._close());
    },
    _close: function _close() {
      f(l);
      var t,
          e = n + " " + w + " ";
      g.bgOverlay.detach(), g.wrap.detach(), g.container.empty(), g.st.mainClass && (e += g.st.mainClass + " "), g._removeClassFromMFP(e), g.fixedContentPos && (t = {
        marginRight: ""
      }, g.isIE7 ? u("body, html").css("overflow", "") : t.overflow = "", u("html").css(t)), m.off("keyup.mfp focusin" + b), g.ev.off(b), g.wrap.attr("class", "mfp-wrap").removeAttr("style"), g.bgOverlay.attr("class", "mfp-bg"), g.container.attr("class", "mfp-container"), !g.st.showCloseBtn || g.st.closeBtnInside && !0 !== g.currTemplate[g.currItem.type] || g.currTemplate.closeBtn && g.currTemplate.closeBtn.detach(), g.st.autoFocusLast && g._lastFocusedEl && u(g._lastFocusedEl).focus(), g.currItem = null, g.content = null, g.currTemplate = null, g.prevHeight = 0, f("AfterClose");
    },
    updateSize: function updateSize(t) {
      var e, n;
      g.isIOS ? (e = document.documentElement.clientWidth / window.innerWidth, n = window.innerHeight * e, g.wrap.css("height", n), g.wH = n) : g.wH = t || C.height(), g.fixedContentPos || g.wrap.css("height", g.wH), f("Resize");
    },
    updateItemHTML: function updateItemHTML() {
      var t = g.items[g.index];
      g.contentContainer.detach(), g.content && g.content.detach(), t.parsed || (t = g.parseEl(g.index));
      var e,
          n = t.type;
      f("BeforeChange", [g.currItem ? g.currItem.type : "", n]), g.currItem = t, g.currTemplate[n] || (e = !!g.st[n] && g.st[n].markup, f("FirstMarkupParse", e), g.currTemplate[n] = !e || u(e)), o && o !== t.type && g.container.removeClass("mfp-" + o + "-holder");
      var i = g["get" + n.charAt(0).toUpperCase() + n.slice(1)](t, g.currTemplate[n]);
      g.appendContent(i, n), t.preloaded = !0, f("Change", t), o = t.type, g.container.prepend(g.contentContainer), f("AfterChange");
    },
    appendContent: function appendContent(t, e) {
      (g.content = t) ? g.st.showCloseBtn && g.st.closeBtnInside && !0 === g.currTemplate[e] ? g.content.find(".mfp-close").length || g.content.append(p()) : g.content = t : g.content = "", f("BeforeAppend"), g.container.addClass("mfp-" + e + "-holder"), g.contentContainer.append(g.content);
    },
    parseEl: function parseEl(t) {
      var e,
          n = g.items[t];

      if ((n = n.tagName ? {
        el: u(n)
      } : (e = n.type, {
        data: n,
        src: n.src
      })).el) {
        for (var i = g.types, o = 0; o < i.length; o++) {
          if (n.el.hasClass("mfp-" + i[o])) {
            e = i[o];
            break;
          }
        }

        n.src = n.el.attr("data-mfp-src"), n.src || (n.src = n.el.attr("href"));
      }

      return n.type = e || g.st.type || "inline", n.index = t, n.parsed = !0, g.items[t] = n, f("ElementParse", n), g.items[t];
    },
    addGroup: function addGroup(e, n) {
      function t(t) {
        t.mfpEl = this, g._openClick(t, e, n);
      }

      var i = "click.magnificPopup";
      (n = n || {}).mainEl = e, n.items ? (n.isObj = !0, e.off(i).on(i, t)) : (n.isObj = !1, n.delegate ? e.off(i).on(i, n.delegate, t) : (n.items = e).off(i).on(i, t));
    },
    _openClick: function _openClick(t, e, n) {
      if ((void 0 !== n.midClick ? n.midClick : u.magnificPopup.defaults.midClick) || !(2 === t.which || t.ctrlKey || t.metaKey || t.altKey || t.shiftKey)) {
        var i = void 0 !== n.disableOn ? n.disableOn : u.magnificPopup.defaults.disableOn;
        if (i) if (u.isFunction(i)) {
          if (!i.call(g)) return !0;
        } else if (C.width() < i) return !0;
        t.type && (t.preventDefault(), g.isOpen && t.stopPropagation()), n.el = u(t.mfpEl), n.delegate && (n.items = e.find(n.delegate)), g.open(n);
      }
    },
    updateStatus: function updateStatus(t, e) {
      var n;
      g.preloader && (i !== t && g.container.removeClass("mfp-s-" + i), e || "loading" !== t || (e = g.st.tLoading), f("UpdateStatus", n = {
        status: t,
        text: e
      }), t = n.status, e = n.text, g.preloader.html(e), g.preloader.find("a").on("click", function (t) {
        t.stopImmediatePropagation();
      }), g.container.addClass("mfp-s-" + t), i = t);
    },
    _checkIfClose: function _checkIfClose(t) {
      if (!u(t).hasClass(s)) {
        var e = g.st.closeOnContentClick,
            n = g.st.closeOnBgClick;
        if (e && n) return !0;
        if (!g.content || u(t).hasClass("mfp-close") || g.preloader && t === g.preloader[0]) return !0;

        if (t === g.content[0] || u.contains(g.content[0], t)) {
          if (e) return !0;
        } else if (n && u.contains(document, t)) return !0;

        return !1;
      }
    },
    _addClassToMFP: function _addClassToMFP(t) {
      g.bgOverlay.addClass(t), g.wrap.addClass(t);
    },
    _removeClassFromMFP: function _removeClassFromMFP(t) {
      this.bgOverlay.removeClass(t), g.wrap.removeClass(t);
    },
    _hasScrollBar: function _hasScrollBar(t) {
      return (g.isIE7 ? m.height() : document.body.scrollHeight) > (t || C.height());
    },
    _setFocus: function _setFocus() {
      (g.st.focus ? g.content.find(g.st.focus).eq(0) : g.wrap).focus();
    },
    _onFocusIn: function _onFocusIn(t) {
      if (t.target !== g.wrap[0] && !u.contains(g.wrap[0], t.target)) return g._setFocus(), !1;
    },
    _parseMarkup: function _parseMarkup(o, t, e) {
      var r;
      e.data && (t = u.extend(e.data, t)), f(y, [o, t, e]), u.each(t, function (t, e) {
        return void 0 === e || !1 === e || void (1 < (r = t.split("_")).length ? 0 < (n = o.find(b + "-" + r[0])).length && ("replaceWith" === (i = r[1]) ? n[0] !== e[0] && n.replaceWith(e) : "img" === i ? n.is("img") ? n.attr("src", e) : n.replaceWith(u("<img>").attr("src", e).attr("class", n.attr("class"))) : n.attr(r[1], e)) : o.find(b + "-" + t).html(e));
        var n, i;
      });
    },
    _getScrollbarSize: function _getScrollbarSize() {
      var t;
      return void 0 === g.scrollbarSize && ((t = document.createElement("div")).style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(t), g.scrollbarSize = t.offsetWidth - t.clientWidth, document.body.removeChild(t)), g.scrollbarSize;
    }
  }, u.magnificPopup = {
    instance: null,
    proto: t.prototype,
    modules: [],
    open: function open(t, e) {
      return r(), (t = t ? u.extend(!0, {}, t) : {}).isObj = !0, t.index = e || 0, this.instance.open(t);
    },
    close: function close() {
      return u.magnificPopup.instance && u.magnificPopup.instance.close();
    },
    registerModule: function registerModule(t, e) {
      e.options && (u.magnificPopup.defaults[t] = e.options), u.extend(this.proto, e.proto), this.modules.push(t);
    },
    defaults: {
      disableOn: 0,
      key: null,
      midClick: !1,
      mainClass: "",
      preloader: !0,
      focus: "",
      closeOnContentClick: !1,
      closeOnBgClick: !0,
      closeBtnInside: !0,
      showCloseBtn: !0,
      enableEscapeKey: !0,
      modal: !1,
      alignTop: !1,
      removalDelay: 0,
      prependTo: null,
      fixedContentPos: "auto",
      fixedBgPos: "auto",
      overflowY: "auto",
      closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',
      tClose: "Close (Esc)",
      tLoading: "Loading...",
      autoFocusLast: !0
    }
  }, u.fn.magnificPopup = function (t) {
    r();
    var e,
        n,
        i,
        o = u(this);
    return "string" == typeof t ? "open" === t ? (e = a ? o.data("magnificPopup") : o[0].magnificPopup, n = parseInt(arguments[1], 10) || 0, i = e.items ? e.items[n] : (i = o, e.delegate && (i = i.find(e.delegate)), i.eq(n)), g._openClick({
      mfpEl: i
    }, o, e)) : g.isOpen && g[t].apply(g, Array.prototype.slice.call(arguments, 1)) : (t = u.extend(!0, {}, t), a ? o.data("magnificPopup", t) : o[0].magnificPopup = t, g.addGroup(o, t)), o;
  };

  function x() {
    S && (T.after(S.addClass(E)).detach(), S = null);
  }

  var E,
      T,
      S,
      A = "inline";
  u.magnificPopup.registerModule(A, {
    options: {
      hiddenClass: "hide",
      markup: "",
      tNotFound: "Content not found"
    },
    proto: {
      initInline: function initInline() {
        g.types.push(A), h(l + "." + A, function () {
          x();
        });
      },
      getInline: function getInline(t, e) {
        if (x(), t.src) {
          var n,
              i = g.st.inline,
              o = u(t.src);
          return o.length ? ((n = o[0].parentNode) && n.tagName && (T || (E = i.hiddenClass, T = d(E), E = "mfp-" + E), S = o.after(T).detach().removeClass(E)), g.updateStatus("ready")) : (g.updateStatus("error", i.tNotFound), o = u("<div>")), t.inlineElement = o;
        }

        return g.updateStatus("ready"), g._parseMarkup(e, {}, t), e;
      }
    }
  });

  function k() {
    I && u(document.body).removeClass(I);
  }

  function D() {
    k(), g.req && g.req.abort();
  }

  var I,
      N = "ajax";
  u.magnificPopup.registerModule(N, {
    options: {
      settings: null,
      cursor: "mfp-ajax-cur",
      tError: '<a href="%url%">The content</a> could not be loaded.'
    },
    proto: {
      initAjax: function initAjax() {
        g.types.push(N), I = g.st.ajax.cursor, h(l + "." + N, D), h("BeforeChange." + N, D);
      },
      getAjax: function getAjax(o) {
        I && u(document.body).addClass(I), g.updateStatus("loading");
        var t = u.extend({
          url: o.src,
          success: function success(t, e, n) {
            var i = {
              data: t,
              xhr: n
            };
            f("ParseAjax", i), g.appendContent(u(i.data), N), o.finished = !0, k(), g._setFocus(), setTimeout(function () {
              g.wrap.addClass(w);
            }, 16), g.updateStatus("ready"), f("AjaxContentAdded");
          },
          error: function error() {
            k(), o.finished = o.loadError = !0, g.updateStatus("error", g.st.ajax.tError.replace("%url%", o.src));
          }
        }, g.st.ajax.settings);
        return g.req = u.ajax(t), "";
      }
    }
  });
  var O;
  u.magnificPopup.registerModule("image", {
    options: {
      markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
      cursor: "mfp-zoom-out-cur",
      titleSrc: "title",
      verticalFit: !0,
      tError: '<a href="%url%">The image</a> could not be loaded.'
    },
    proto: {
      initImage: function initImage() {
        var t = g.st.image,
            e = ".image";
        g.types.push("image"), h(_ + e, function () {
          "image" === g.currItem.type && t.cursor && u(document.body).addClass(t.cursor);
        }), h(l + e, function () {
          t.cursor && u(document.body).removeClass(t.cursor), C.off("resize" + b);
        }), h("Resize" + e, g.resizeImage), g.isLowIE && h("AfterChange", g.resizeImage);
      },
      resizeImage: function resizeImage() {
        var t,
            e = g.currItem;
        e && e.img && g.st.image.verticalFit && (t = 0, g.isLowIE && (t = parseInt(e.img.css("padding-top"), 10) + parseInt(e.img.css("padding-bottom"), 10)), e.img.css("max-height", g.wH - t));
      },
      _onImageHasSize: function _onImageHasSize(t) {
        t.img && (t.hasSize = !0, O && clearInterval(O), t.isCheckingImgSize = !1, f("ImageHasSize", t), t.imgHidden && (g.content && g.content.removeClass("mfp-loading"), t.imgHidden = !1));
      },
      findImageSize: function findImageSize(e) {
        var n = 0,
            i = e.img[0],
            o = function o(t) {
          O && clearInterval(O), O = setInterval(function () {
            0 < i.naturalWidth ? g._onImageHasSize(e) : (200 < n && clearInterval(O), 3 === ++n ? o(10) : 40 === n ? o(50) : 100 === n && o(500));
          }, t);
        };

        o(1);
      },
      getImage: function getImage(t, e) {
        var n,
            i = 0,
            o = function o() {
          t && (t.img[0].complete ? (t.img.off(".mfploader"), t === g.currItem && (g._onImageHasSize(t), g.updateStatus("ready")), t.hasSize = !0, t.loaded = !0, f("ImageLoadComplete")) : ++i < 200 ? setTimeout(o, 100) : r());
        },
            r = function r() {
          t && (t.img.off(".mfploader"), t === g.currItem && (g._onImageHasSize(t), g.updateStatus("error", s.tError.replace("%url%", t.src))), t.hasSize = !0, t.loaded = !0, t.loadError = !0);
        },
            s = g.st.image,
            a = e.find(".mfp-img");

        return a.length && ((n = document.createElement("img")).className = "mfp-img", t.el && t.el.find("img").length && (n.alt = t.el.find("img").attr("alt")), t.img = u(n).on("load.mfploader", o).on("error.mfploader", r), n.src = t.src, a.is("img") && (t.img = t.img.clone()), 0 < (n = t.img[0]).naturalWidth ? t.hasSize = !0 : n.width || (t.hasSize = !1)), g._parseMarkup(e, {
          title: function (t) {
            if (t.data && void 0 !== t.data.title) return t.data.title;
            var e = g.st.image.titleSrc;

            if (e) {
              if (u.isFunction(e)) return e.call(g, t);
              if (t.el) return t.el.attr(e) || "";
            }

            return "";
          }(t),
          img_replaceWith: t.img
        }, t), g.resizeImage(), t.hasSize ? (O && clearInterval(O), t.loadError ? (e.addClass("mfp-loading"), g.updateStatus("error", s.tError.replace("%url%", t.src))) : (e.removeClass("mfp-loading"), g.updateStatus("ready"))) : (g.updateStatus("loading"), t.loading = !0, t.hasSize || (t.imgHidden = !0, e.addClass("mfp-loading"), g.findImageSize(t))), e;
      }
    }
  });
  var P;
  u.magnificPopup.registerModule("zoom", {
    options: {
      enabled: !1,
      easing: "ease-in-out",
      duration: 300,
      opener: function opener(t) {
        return t.is("img") ? t : t.find("img");
      }
    },
    proto: {
      initZoom: function initZoom() {
        var t,
            e,
            n,
            i,
            o,
            r,
            s = g.st.zoom,
            a = ".zoom";
        s.enabled && g.supportsTransition && (e = s.duration, n = function n(t) {
          var e = t.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
              n = "all " + s.duration / 1e3 + "s " + s.easing,
              i = {
            position: "fixed",
            zIndex: 9999,
            left: 0,
            top: 0,
            "-webkit-backface-visibility": "hidden"
          },
              o = "transition";
          return i["-webkit-" + o] = i["-moz-" + o] = i["-o-" + o] = i[o] = n, e.css(i), e;
        }, i = function i() {
          g.content.css("visibility", "visible");
        }, h("BuildControls" + a, function () {
          if (g._allowZoom()) {
            if (clearTimeout(o), g.content.css("visibility", "hidden"), !(t = g._getItemToZoom())) return void i();
            (r = n(t)).css(g._getOffset()), g.wrap.append(r), o = setTimeout(function () {
              r.css(g._getOffset(!0)), o = setTimeout(function () {
                i(), setTimeout(function () {
                  r.remove(), t = r = null, f("ZoomAnimationEnded");
                }, 16);
              }, e);
            }, 16);
          }
        }), h(c + a, function () {
          if (g._allowZoom()) {
            if (clearTimeout(o), g.st.removalDelay = e, !t) {
              if (!(t = g._getItemToZoom())) return;
              r = n(t);
            }

            r.css(g._getOffset(!0)), g.wrap.append(r), g.content.css("visibility", "hidden"), setTimeout(function () {
              r.css(g._getOffset());
            }, 16);
          }
        }), h(l + a, function () {
          g._allowZoom() && (i(), r && r.remove(), t = null);
        }));
      },
      _allowZoom: function _allowZoom() {
        return "image" === g.currItem.type;
      },
      _getItemToZoom: function _getItemToZoom() {
        return !!g.currItem.hasSize && g.currItem.img;
      },
      _getOffset: function _getOffset(t) {
        var e = t ? g.currItem.img : g.st.zoom.opener(g.currItem.el || g.currItem),
            n = e.offset(),
            i = parseInt(e.css("padding-top"), 10),
            o = parseInt(e.css("padding-bottom"), 10);
        n.top -= u(window).scrollTop() - i;
        var r = {
          width: e.width(),
          height: (a ? e.innerHeight() : e[0].offsetHeight) - o - i
        };
        return void 0 === P && (P = void 0 !== document.createElement("p").style.MozTransform), P ? r["-moz-transform"] = r.transform = "translate(" + n.left + "px," + n.top + "px)" : (r.left = n.left, r.top = n.top), r;
      }
    }
  });

  function L(t) {
    var e;
    !g.currTemplate[j] || (e = g.currTemplate[j].find("iframe")).length && (t || (e[0].src = "//about:blank"), g.isIE8 && e.css("display", t ? "block" : "none"));
  }

  var j = "iframe";
  u.magnificPopup.registerModule(j, {
    options: {
      markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
      srcAction: "iframe_src",
      patterns: {
        youtube: {
          index: "youtube.com",
          id: "v=",
          src: "//www.youtube.com/embed/%id%?autoplay=1"
        },
        vimeo: {
          index: "vimeo.com/",
          id: "/",
          src: "//player.vimeo.com/video/%id%?autoplay=1"
        },
        gmaps: {
          index: "//maps.google.",
          src: "%id%&output=embed"
        }
      }
    },
    proto: {
      initIframe: function initIframe() {
        g.types.push(j), h("BeforeChange", function (t, e, n) {
          e !== n && (e === j ? L() : n === j && L(!0));
        }), h(l + "." + j, function () {
          L();
        });
      },
      getIframe: function getIframe(t, e) {
        var n = t.src,
            i = g.st.iframe;
        u.each(i.patterns, function () {
          if (-1 < n.indexOf(this.index)) return this.id && (n = "string" == typeof this.id ? n.substr(n.lastIndexOf(this.id) + this.id.length, n.length) : this.id.call(this, n)), n = this.src.replace("%id%", n), !1;
        });
        var o = {};
        return i.srcAction && (o[i.srcAction] = n), g._parseMarkup(e, o, t), g.updateStatus("ready"), e;
      }
    }
  });

  function M(t) {
    var e = g.items.length;
    return e - 1 < t ? t - e : t < 0 ? e + t : t;
  }

  function H(t, e, n) {
    return t.replace(/%curr%/gi, e + 1).replace(/%total%/gi, n);
  }

  u.magnificPopup.registerModule("gallery", {
    options: {
      enabled: !1,
      arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
      preload: [0, 2],
      navigateByImgClick: !0,
      arrows: !0,
      tPrev: "Previous (Left arrow key)",
      tNext: "Next (Right arrow key)",
      tCounter: "%curr% of %total%"
    },
    proto: {
      initGallery: function initGallery() {
        var r = g.st.gallery,
            t = ".mfp-gallery";
        if (g.direction = !0, !r || !r.enabled) return !1;
        v += " mfp-gallery", h(_ + t, function () {
          r.navigateByImgClick && g.wrap.on("click" + t, ".mfp-img", function () {
            if (1 < g.items.length) return g.next(), !1;
          }), m.on("keydown" + t, function (t) {
            37 === t.keyCode ? g.prev() : 39 === t.keyCode && g.next();
          });
        }), h("UpdateStatus" + t, function (t, e) {
          e.text && (e.text = H(e.text, g.currItem.index, g.items.length));
        }), h(y + t, function (t, e, n, i) {
          var o = g.items.length;
          n.counter = 1 < o ? H(r.tCounter, i.index, o) : "";
        }), h("BuildControls" + t, function () {
          var t, e, n;
          1 < g.items.length && r.arrows && !g.arrowLeft && (t = r.arrowMarkup, e = g.arrowLeft = u(t.replace(/%title%/gi, r.tPrev).replace(/%dir%/gi, "left")).addClass(s), n = g.arrowRight = u(t.replace(/%title%/gi, r.tNext).replace(/%dir%/gi, "right")).addClass(s), e.click(function () {
            g.prev();
          }), n.click(function () {
            g.next();
          }), g.container.append(e.add(n)));
        }), h("Change" + t, function () {
          g._preloadTimeout && clearTimeout(g._preloadTimeout), g._preloadTimeout = setTimeout(function () {
            g.preloadNearbyImages(), g._preloadTimeout = null;
          }, 16);
        }), h(l + t, function () {
          m.off(t), g.wrap.off("click" + t), g.arrowRight = g.arrowLeft = null;
        });
      },
      next: function next() {
        g.direction = !0, g.index = M(g.index + 1), g.updateItemHTML();
      },
      prev: function prev() {
        g.direction = !1, g.index = M(g.index - 1), g.updateItemHTML();
      },
      goTo: function goTo(t) {
        g.direction = t >= g.index, g.index = t, g.updateItemHTML();
      },
      preloadNearbyImages: function preloadNearbyImages() {
        for (var t = g.st.gallery.preload, e = Math.min(t[0], g.items.length), n = Math.min(t[1], g.items.length), i = 1; i <= (g.direction ? n : e); i++) {
          g._preloadItem(g.index + i);
        }

        for (i = 1; i <= (g.direction ? e : n); i++) {
          g._preloadItem(g.index - i);
        }
      },
      _preloadItem: function _preloadItem(t) {
        var e;
        t = M(t), g.items[t].preloaded || ((e = g.items[t]).parsed || (e = g.parseEl(t)), f("LazyLoad", e), "image" === e.type && (e.img = u('<img class="mfp-img" />').on("load.mfploader", function () {
          e.hasSize = !0;
        }).on("error.mfploader", function () {
          e.hasSize = !0, e.loadError = !0, f("LazyLoadError", e);
        }).attr("src", e.src)), e.preloaded = !0);
      }
    }
  });
  var $ = "retina";
  u.magnificPopup.registerModule($, {
    options: {
      replaceSrc: function replaceSrc(t) {
        return t.src.replace(/\.\w+$/, function (t) {
          return "@2x" + t;
        });
      },
      ratio: 1
    },
    proto: {
      initRetina: function initRetina() {
        var n, i;
        1 < window.devicePixelRatio && (n = g.st.retina, i = n.ratio, 1 < (i = isNaN(i) ? i() : i) && (h("ImageHasSize." + $, function (t, e) {
          e.img.css({
            "max-width": e.img[0].naturalWidth / i,
            width: "100%"
          });
        }), h("ElementParse." + $, function (t, e) {
          e.src = n.replaceSrc(e, i);
        })));
      }
    }
  }), r();
}), function () {
  function e(t, e) {
    return function () {
      return t.apply(e, arguments);
    };
  }

  var i,
      t,
      n,
      l,
      o,
      s = [].indexOf || function (t) {
    for (var e = 0, n = this.length; e < n; e++) {
      if (e in this && this[e] === t) return e;
    }

    return -1;
  };

  function r() {}

  function a() {
    this.keys = [], this.values = [];
  }

  function c() {
    "undefined" != typeof console && null !== console && console.warn("MutationObserver is not supported by your browser."), "undefined" != typeof console && null !== console && console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.");
  }

  function u(t) {
    null == t && (t = {}), this.scrollCallback = e(this.scrollCallback, this), this.scrollHandler = e(this.scrollHandler, this), this.resetAnimation = e(this.resetAnimation, this), this.start = e(this.start, this), this.scrolled = !0, this.config = this.util().extend(t, this.defaults), null != t.scrollContainer && (this.config.scrollContainer = document.querySelector(t.scrollContainer)), this.animationNameCache = new n(), this.wowEvent = this.util().createEvent(this.config.boxClass);
  }

  r.prototype.extend = function (t, e) {
    var n, i;

    for (n in e) {
      i = e[n], null == t[n] && (t[n] = i);
    }

    return t;
  }, r.prototype.isMobile = function (t) {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(t);
  }, r.prototype.createEvent = function (t, e, n, i) {
    var o;
    return null == e && (e = !1), null == n && (n = !1), null == i && (i = null), null != document.createEvent ? (o = document.createEvent("CustomEvent")).initCustomEvent(t, e, n, i) : null != document.createEventObject ? (o = document.createEventObject()).eventType = t : o.eventName = t, o;
  }, r.prototype.emitEvent = function (t, e) {
    return null != t.dispatchEvent ? t.dispatchEvent(e) : e in (null != t) ? t[e]() : "on" + e in (null != t) ? t["on" + e]() : void 0;
  }, r.prototype.addEvent = function (t, e, n) {
    return null != t.addEventListener ? t.addEventListener(e, n, !1) : null != t.attachEvent ? t.attachEvent("on" + e, n) : t[e] = n;
  }, r.prototype.removeEvent = function (t, e, n) {
    return null != t.removeEventListener ? t.removeEventListener(e, n, !1) : null != t.detachEvent ? t.detachEvent("on" + e, n) : delete t[e];
  }, r.prototype.innerHeight = function () {
    return "innerHeight" in window ? window.innerHeight : document.documentElement.clientHeight;
  }, t = r, n = this.WeakMap || this.MozWeakMap || (a.prototype.get = function (t) {
    for (var e, n = this.keys, i = e = 0, o = n.length; e < o; i = ++e) {
      if (n[i] === t) return this.values[i];
    }
  }, a.prototype.set = function (t, e) {
    for (var n, i = this.keys, o = n = 0, r = i.length; n < r; o = ++n) {
      if (i[o] === t) return void (this.values[o] = e);
    }

    return this.keys.push(t), this.values.push(e);
  }, a), i = this.MutationObserver || this.WebkitMutationObserver || this.MozMutationObserver || (c.notSupported = !0, c.prototype.observe = function () {}, c), l = this.getComputedStyle || function (n, t) {
    return this.getPropertyValue = function (t) {
      var e;
      return "float" === t && (t = "styleFloat"), o.test(t) && t.replace(o, function (t, e) {
        return e.toUpperCase();
      }), (null != (e = n.currentStyle) ? e[t] : void 0) || null;
    }, this;
  }, o = /(\-([a-z]){1})/g, this.WOW = (u.prototype.defaults = {
    boxClass: "wow",
    animateClass: "animated",
    offset: 0,
    mobile: !0,
    live: !0,
    callback: null,
    scrollContainer: null
  }, u.prototype.init = function () {
    var t;
    return this.element = window.document.documentElement, "interactive" === (t = document.readyState) || "complete" === t ? this.start() : this.util().addEvent(document, "DOMContentLoaded", this.start), this.finished = [];
  }, u.prototype.start = function () {
    var o, t, e, n, s;
    if (this.stopped = !1, this.boxes = function () {
      for (var t = this.element.querySelectorAll("." + this.config.boxClass), e = [], n = 0, i = t.length; n < i; n++) {
        o = t[n], e.push(o);
      }

      return e;
    }.call(this), this.all = function () {
      for (var t = this.boxes, e = [], n = 0, i = t.length; n < i; n++) {
        o = t[n], e.push(o);
      }

      return e;
    }.call(this), this.boxes.length) if (this.disabled()) this.resetStyle();else for (t = 0, e = (n = this.boxes).length; t < e; t++) {
      o = n[t], this.applyStyle(o, !0);
    }
    if (this.disabled() || (this.util().addEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().addEvent(window, "resize", this.scrollHandler), this.interval = setInterval(this.scrollCallback, 50)), this.config.live) return new i((s = this, function (t) {
      for (var o, r, e = [], n = 0, i = t.length; n < i; n++) {
        r = t[n], e.push(function () {
          for (var t = r.addedNodes || [], e = [], n = 0, i = t.length; n < i; n++) {
            o = t[n], e.push(this.doSync(o));
          }

          return e;
        }.call(s));
      }

      return e;
    })).observe(document.body, {
      childList: !0,
      subtree: !0
    });
  }, u.prototype.stop = function () {
    if (this.stopped = !0, this.util().removeEvent(this.config.scrollContainer || window, "scroll", this.scrollHandler), this.util().removeEvent(window, "resize", this.scrollHandler), null != this.interval) return clearInterval(this.interval);
  }, u.prototype.sync = function (t) {
    if (i.notSupported) return this.doSync(this.element);
  }, u.prototype.doSync = function (t) {
    var e, n, i, o, r;

    if (null == t && (t = this.element), 1 === t.nodeType) {
      for (r = [], n = 0, i = (o = (t = t.parentNode || t).querySelectorAll("." + this.config.boxClass)).length; n < i; n++) {
        e = o[n], s.call(this.all, e) < 0 ? (this.boxes.push(e), this.all.push(e), this.stopped || this.disabled() ? this.resetStyle() : this.applyStyle(e, !0), r.push(this.scrolled = !0)) : r.push(void 0);
      }

      return r;
    }
  }, u.prototype.show = function (t) {
    return this.applyStyle(t), t.className = t.className + " " + this.config.animateClass, null != this.config.callback && this.config.callback(t), this.util().emitEvent(t, this.wowEvent), this.util().addEvent(t, "animationend", this.resetAnimation), this.util().addEvent(t, "oanimationend", this.resetAnimation), this.util().addEvent(t, "webkitAnimationEnd", this.resetAnimation), this.util().addEvent(t, "MSAnimationEnd", this.resetAnimation), t;
  }, u.prototype.applyStyle = function (t, e) {
    var n,
        i = t.getAttribute("data-wow-duration"),
        o = t.getAttribute("data-wow-delay"),
        r = t.getAttribute("data-wow-iteration");
    return this.animate((n = this, function () {
      return n.customStyle(t, e, i, o, r);
    }));
  }, u.prototype.animate = "requestAnimationFrame" in window ? function (t) {
    return window.requestAnimationFrame(t);
  } : function (t) {
    return t();
  }, u.prototype.resetStyle = function () {
    for (var t, e = this.boxes, n = [], i = 0, o = e.length; i < o; i++) {
      t = e[i], n.push(t.style.visibility = "visible");
    }

    return n;
  }, u.prototype.resetAnimation = function (t) {
    var e;
    if (0 <= t.type.toLowerCase().indexOf("animationend")) return (e = t.target || t.srcElement).className = e.className.replace(this.config.animateClass, "").trim();
  }, u.prototype.customStyle = function (t, e, n, i, o) {
    return e && this.cacheAnimationName(t), t.style.visibility = e ? "hidden" : "visible", n && this.vendorSet(t.style, {
      animationDuration: n
    }), i && this.vendorSet(t.style, {
      animationDelay: i
    }), o && this.vendorSet(t.style, {
      animationIterationCount: o
    }), this.vendorSet(t.style, {
      animationName: e ? "none" : this.cachedAnimationName(t)
    }), t;
  }, u.prototype.vendors = ["moz", "webkit"], u.prototype.vendorSet = function (o, t) {
    var r,
        s,
        a,
        e = [];

    for (r in t) {
      s = t[r], o["" + r] = s, e.push(function () {
        for (var t = this.vendors, e = [], n = 0, i = t.length; n < i; n++) {
          a = t[n], e.push(o["" + a + r.charAt(0).toUpperCase() + r.substr(1)] = s);
        }

        return e;
      }.call(this));
    }

    return e;
  }, u.prototype.vendorCSS = function (t, e) {
    for (var n, i = l(t), o = i.getPropertyCSSValue(e), r = this.vendors, s = 0, a = r.length; s < a; s++) {
      n = r[s], o = o || i.getPropertyCSSValue("-" + n + "-" + e);
    }

    return o;
  }, u.prototype.animationName = function (e) {
    var n;

    try {
      n = this.vendorCSS(e, "animation-name").cssText;
    } catch (t) {
      n = l(e).getPropertyValue("animation-name");
    }

    return "none" === n ? "" : n;
  }, u.prototype.cacheAnimationName = function (t) {
    return this.animationNameCache.set(t, this.animationName(t));
  }, u.prototype.cachedAnimationName = function (t) {
    return this.animationNameCache.get(t);
  }, u.prototype.scrollHandler = function () {
    return this.scrolled = !0;
  }, u.prototype.scrollCallback = function () {
    var o;
    if (this.scrolled && (this.scrolled = !1, this.boxes = function () {
      for (var t = this.boxes, e = [], n = 0, i = t.length; n < i; n++) {
        (o = t[n]) && (this.isVisible(o) ? this.show(o) : e.push(o));
      }

      return e;
    }.call(this), !this.boxes.length && !this.config.live)) return this.stop();
  }, u.prototype.offsetTop = function (t) {
    for (var e; void 0 === t.offsetTop;) {
      t = t.parentNode;
    }

    for (e = t.offsetTop; t = t.offsetParent;) {
      e += t.offsetTop;
    }

    return e;
  }, u.prototype.isVisible = function (t) {
    var e = t.getAttribute("data-wow-offset") || this.config.offset,
        n = this.config.scrollContainer && this.config.scrollContainer.scrollTop || window.pageYOffset,
        i = n + Math.min(this.element.clientHeight, this.util().innerHeight()) - e,
        o = this.offsetTop(t),
        r = o + t.clientHeight;
    return o <= i && n <= r;
  }, u.prototype.util = function () {
    return null != this._util ? this._util : this._util = new t();
  }, u.prototype.disabled = function () {
    return !this.config.mobile && this.util().isMobile(navigator.userAgent);
  }, u);
}.call(this), function (r) {
  r.fn.footerReveal = function (t) {
    var e = r(this),
        n = e.prev(),
        i = r(window),
        o = r.extend({
      shadow: !0,
      shadowOpacity: .8,
      zIndex: -100
    }, t);
    r.extend(!0, {}, o, t);
    return e.outerHeight() <= i.outerHeight() && e.offset().top >= i.outerHeight() && (e.css({
      "z-index": o.zIndex,
      position: "fixed",
      bottom: 0
    }), o.shadow && n.css({
      "-moz-box-shadow": "0 20px 30px -20px rgba(0,0,0," + o.shadowOpacity + ")",
      "-webkit-box-shadow": "0 20px 30px -20px rgba(0,0,0," + o.shadowOpacity + ")",
      "box-shadow": "0 20px 30px -20px rgba(0,0,0," + o.shadowOpacity + ")"
    }), i.on("load resize footerRevealResize", function () {
      e.css({
        width: n.outerWidth()
      }), n.css({
        "margin-bottom": e.outerHeight()
      });
    })), this;
  };
}(jQuery), function (t, e) {
  "object" === ( false ? undefined : _typeof(exports)) && "undefined" != typeof module ? module.exports = e() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function () {
  "use strict";

  function o(t, e) {
    var n,
        i = "LazyLoad::Initialized",
        o = new t(e);

    try {
      n = new CustomEvent(i, {
        detail: {
          instance: o
        }
      });
    } catch (t) {
      (n = document.createEvent("CustomEvent")).initCustomEvent(i, !1, !1, {
        instance: o
      });
    }

    window.dispatchEvent(n);
  }

  var t = "undefined" != typeof window,
      r = t && !("onscroll" in window) || "undefined" != typeof navigator && /(gle|ing|ro)bot|crawl|spider/i.test(navigator.userAgent),
      e = t && "IntersectionObserver" in window,
      c = t && "classList" in document.createElement("p"),
      n = {
    elements_selector: "img",
    container: r || t ? document : null,
    threshold: 300,
    thresholds: null,
    data_src: "src",
    data_srcset: "srcset",
    data_sizes: "sizes",
    data_bg: "bg",
    class_loading: "loading",
    class_loaded: "loaded",
    class_error: "error",
    load_delay: 0,
    auto_unobserve: !0,
    callback_enter: null,
    callback_exit: null,
    callback_reveal: null,
    callback_loaded: null,
    callback_error: null,
    callback_finish: null,
    use_native: !1
  };

  function h(t, e) {
    return t.getAttribute("data-" + e);
  }

  function s(t, e, n) {
    var i = "data-" + e;
    null !== n ? t.setAttribute(i, n) : t.removeAttribute(i);
  }

  function a(t) {
    return "true" === h(t, S);
  }

  function l(t, e) {
    return s(t, A, e), 0;
  }

  function u(t) {
    return h(t, A);
  }

  function d(t, e) {
    t && t(e);
  }

  function f(t, e) {
    t._loadingCount += e, 0 === t._elements.length && 0 === t._loadingCount && d(t._settings.callback_finish);
  }

  function i(t) {
    for (var e, n = [], i = 0; e = t.children[i]; i += 1) {
      "SOURCE" === e.tagName && n.push(e);
    }

    return n;
  }

  function p(t, e, n) {
    n && t.setAttribute(e, n);
  }

  function g(t, e) {
    p(t, "sizes", h(t, e.data_sizes)), p(t, "srcset", h(t, e.data_srcset)), p(t, "src", h(t, e.data_src));
  }

  function m(t, e) {
    var n,
        i,
        o,
        r,
        s,
        a,
        l = e._settings,
        c = t.tagName,
        u = k[c];
    if (u) return u(t, l), f(e, 1), e._elements = (n = e._elements, i = t, n.filter(function (t) {
      return t !== i;
    })), 0;
    s = h(o = t, (r = l).data_src), a = h(o, r.data_bg), s && (o.style.backgroundImage = 'url("'.concat(s, '")')), a && (o.style.backgroundImage = a);
  }

  function v(t, e) {
    c ? t.classList.add(e) : t.className += (t.className ? " " : "") + e;
  }

  function y(t, e, n) {
    t.addEventListener(e, n);
  }

  function _(t, e, n) {
    t.removeEventListener(e, n);
  }

  function b(t, e, n) {
    _(t, "load", e), _(t, D, e), _(t, "error", n);
  }

  function w(t, e, n) {
    var i,
        o,
        r = n._settings,
        s = e ? r.class_loaded : r.class_error,
        a = e ? r.callback_loaded : r.callback_error,
        l = t.target;
    i = l, o = r.class_loading, c ? i.classList.remove(o) : i.className = i.className.replace(new RegExp("(^|\\s+)" + o + "(\\s+|$)"), " ").replace(/^\s+/, "").replace(/\s+$/, ""), v(l, s), d(a, l), f(n, -1);
  }

  function C(n, i) {
    function o(t) {
      w(t, !0, i), b(n, o, s);
    }

    var t,
        e,
        r,
        s = function t(e) {
      w(e, !1, i), b(n, o, t);
    };

    r = s, y(t = n, "load", e = o), y(t, D, e), y(t, "error", r);
  }

  function x(a) {
    return e && (a._observer = new IntersectionObserver(function (t) {
      t.forEach(function (t) {
        return (s = t).isIntersecting || 0 < s.intersectionRatio ? (i = t.target, r = (o = a)._settings, d(r.callback_enter, i), void (r.load_delay ? P : N)(i, o)) : (e = t.target, n = a._settings, d(n.callback_exit, e), void (n.load_delay && O(e)));
        var e, n, i, o, r, s;
      });
    }, {
      root: (t = a._settings).container === document ? null : t.container,
      rootMargin: t.thresholds || t.threshold + "px"
    }), !0);
    var t;
  }

  function E(t, e) {
    return n = t || (i = e).container.querySelectorAll(i.elements_selector), Array.prototype.slice.call(n).filter(function (t) {
      return !a(t);
    });
    var n, i;
  }

  function T(t, e) {
    this._settings = _extends({}, n, t), this._loadingCount = 0, x(this), this.update(e);
  }

  var S = "was-processed",
      A = "ll-timeout",
      k = {
    IMG: function IMG(t, e) {
      var n = t.parentNode;
      n && "PICTURE" === n.tagName && i(n).forEach(function (t) {
        g(t, e);
      }), g(t, e);
    },
    IFRAME: function IFRAME(t, e) {
      p(t, "src", h(t, e.data_src));
    },
    VIDEO: function VIDEO(t, e) {
      i(t).forEach(function (t) {
        p(t, "src", h(t, e.data_src));
      }), p(t, "src", h(t, e.data_src)), t.load();
    }
  },
      D = "loadeddata",
      I = ["IMG", "IFRAME", "VIDEO"],
      N = function N(t, e) {
    var n = e._observer;
    L(t, e), n && e._settings.auto_unobserve && n.unobserve(t);
  },
      O = function O(t) {
    var e = u(t);
    e && (clearTimeout(e), l(t, null));
  },
      P = function P(t, e) {
    var n = e._settings.load_delay,
        i = u(t);
    i || (i = setTimeout(function () {
      N(t, e), O(t);
    }, n), l(t, i));
  },
      L = function L(t, e, n) {
    var i = e._settings;
    !n && a(t) || (-1 < I.indexOf(t.tagName) && (C(t, e), v(t, i.class_loading)), m(t, e), s(t, S, "true"), d(i.callback_reveal, t), d(i.callback_set, t));
  },
      j = ["IMG", "IFRAME"];

  return T.prototype = {
    update: function update(t) {
      var e,
          n = this,
          i = this._settings;
      this._elements = E(t, i), !r && this._observer ? (i.use_native && "loading" in HTMLImageElement.prototype && ((e = this)._elements.forEach(function (t) {
        -1 !== j.indexOf(t.tagName) && (t.setAttribute("loading", "lazy"), L(t, e));
      }), this._elements = E(t, i)), this._elements.forEach(function (t) {
        n._observer.observe(t);
      })) : this.loadAll();
    },
    destroy: function destroy() {
      var e = this;
      this._observer && (this._elements.forEach(function (t) {
        e._observer.unobserve(t);
      }), this._observer = null), this._elements = null, this._settings = null;
    },
    load: function load(t, e) {
      L(t, this, e);
    },
    loadAll: function loadAll() {
      var e = this;

      this._elements.forEach(function (t) {
        N(t, e);
      });
    }
  }, t && function (t, e) {
    if (e) if (e.length) for (var n, i = 0; n = e[i]; i += 1) {
      o(t, n);
    } else o(t, e);
  }(T, window.lazyLoadOptions), T;
}), function (t, e) {
  "object" == ( false ? undefined : _typeof2(exports)) && "undefined" != typeof module ? e(exports, __WEBPACK_LOCAL_MODULE_0__) :  true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [exports, __WEBPACK_LOCAL_MODULE_0__], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function (t, p) {
  "use strict";

  function i(t, e) {
    for (var n = 0; n < e.length; n++) {
      var i = e[n];
      i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
    }
  }

  function s(t, e, n) {
    return e && i(t.prototype, e), n && i(t, n), t;
  }

  function e(e, t) {
    var n,
        i = Object.keys(e);
    return Object.getOwnPropertySymbols && (n = Object.getOwnPropertySymbols(e), t && (n = n.filter(function (t) {
      return Object.getOwnPropertyDescriptor(e, t).enumerable;
    })), i.push.apply(i, n)), i;
  }

  function l(o) {
    for (var t = 1; t < arguments.length; t++) {
      var r = null != arguments[t] ? arguments[t] : {};
      t % 2 ? e(Object(r), !0).forEach(function (t) {
        var e, n, i;
        e = o, i = r[n = t], n in e ? Object.defineProperty(e, n, {
          value: i,
          enumerable: !0,
          configurable: !0,
          writable: !0
        }) : e[n] = i;
      }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(o, Object.getOwnPropertyDescriptors(r)) : e(Object(r)).forEach(function (t) {
        Object.defineProperty(o, t, Object.getOwnPropertyDescriptor(r, t));
      });
    }

    return o;
  }

  p = p && p.hasOwnProperty("default") ? p.default : p;
  var n = "transitionend";

  function o(t) {
    var e = this,
        n = !1;
    return p(this).one(g.TRANSITION_END, function () {
      n = !0;
    }), setTimeout(function () {
      n || g.triggerTransitionEnd(e);
    }, t), this;
  }

  var g = {
    TRANSITION_END: "bsTransitionEnd",
    getUID: function getUID(t) {
      for (; t += ~~(1e6 * Math.random()), document.getElementById(t);) {
        ;
      }

      return t;
    },
    getSelectorFromElement: function getSelectorFromElement(t) {
      var e,
          n = t.getAttribute("data-target");
      n && "#" !== n || (n = (e = t.getAttribute("href")) && "#" !== e ? e.trim() : "");

      try {
        return document.querySelector(n) ? n : null;
      } catch (t) {
        return null;
      }
    },
    getTransitionDurationFromElement: function getTransitionDurationFromElement(t) {
      if (!t) return 0;
      var e = p(t).css("transition-duration"),
          n = p(t).css("transition-delay"),
          i = parseFloat(e),
          o = parseFloat(n);
      return i || o ? (e = e.split(",")[0], n = n.split(",")[0], 1e3 * (parseFloat(e) + parseFloat(n))) : 0;
    },
    reflow: function reflow(t) {
      return t.offsetHeight;
    },
    triggerTransitionEnd: function triggerTransitionEnd(t) {
      p(t).trigger(n);
    },
    supportsTransitionEnd: function supportsTransitionEnd() {
      return Boolean(n);
    },
    isElement: function isElement(t) {
      return (t[0] || t).nodeType;
    },
    typeCheckConfig: function typeCheckConfig(t, e, n) {
      for (var i in n) {
        if (Object.prototype.hasOwnProperty.call(n, i)) {
          var o = n[i],
              r = e[i],
              s = r && g.isElement(r) ? "element" : (a = r, {}.toString.call(a).match(/\s([a-z]+)/i)[1].toLowerCase());
          if (!new RegExp(o).test(s)) throw new Error(t.toUpperCase() + ': Option "' + i + '" provided type "' + s + '" but expected type "' + o + '".');
        }
      }

      var a;
    },
    findShadowRoot: function findShadowRoot(t) {
      if (!document.documentElement.attachShadow) return null;
      if ("function" != typeof t.getRootNode) return t instanceof ShadowRoot ? t : t.parentNode ? g.findShadowRoot(t.parentNode) : null;
      var e = t.getRootNode();
      return e instanceof ShadowRoot ? e : null;
    },
    jQueryDetection: function jQueryDetection() {
      if (void 0 === p) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
      var t = p.fn.jquery.split(" ")[0].split(".");
      if (t[0] < 2 && t[1] < 9 || 1 === t[0] && 9 === t[1] && t[2] < 1 || 4 <= t[0]) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0");
    }
  };
  g.jQueryDetection(), p.fn.emulateTransitionEnd = o, p.event.special[g.TRANSITION_END] = {
    bindType: n,
    delegateType: n,
    handle: function handle(t) {
      if (p(t.target).is(this)) return t.handleObj.handler.apply(this, arguments);
    }
  };

  var r = "alert",
      a = "bs.alert",
      c = "." + a,
      u = p.fn[r],
      h = {
    CLOSE: "close" + c,
    CLOSED: "closed" + c,
    CLICK_DATA_API: "click" + c + ".data-api"
  },
      d = "alert",
      f = "fade",
      m = "show",
      v = function () {
    function i(t) {
      this._element = t;
    }

    var t = i.prototype;
    return t.close = function (t) {
      var e = this._element;
      t && (e = this._getRootElement(t)), this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e);
    }, t.dispose = function () {
      p.removeData(this._element, a), this._element = null;
    }, t._getRootElement = function (t) {
      var e = g.getSelectorFromElement(t),
          n = !1;
      return e && (n = document.querySelector(e)), n = n || p(t).closest("." + d)[0];
    }, t._triggerCloseEvent = function (t) {
      var e = p.Event(h.CLOSE);
      return p(t).trigger(e), e;
    }, t._removeElement = function (e) {
      var t,
          n = this;
      p(e).removeClass(m), p(e).hasClass(f) ? (t = g.getTransitionDurationFromElement(e), p(e).one(g.TRANSITION_END, function (t) {
        return n._destroyElement(e, t);
      }).emulateTransitionEnd(t)) : this._destroyElement(e);
    }, t._destroyElement = function (t) {
      p(t).detach().trigger(h.CLOSED).remove();
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = p(this),
            e = t.data(a);
        e || (e = new i(this), t.data(a, e)), "close" === n && e[n](this);
      });
    }, i._handleDismiss = function (e) {
      return function (t) {
        t && t.preventDefault(), e.close(this);
      };
    }, s(i, null, [{
      key: "VERSION",
      get: function get() {
        return "4.4.1";
      }
    }]), i;
  }();

  p(document).on(h.CLICK_DATA_API, '[data-dismiss="alert"]', v._handleDismiss(new v())), p.fn[r] = v._jQueryInterface, p.fn[r].Constructor = v, p.fn[r].noConflict = function () {
    return p.fn[r] = u, v._jQueryInterface;
  };

  var y = "button",
      _ = "bs.button",
      b = "." + _,
      w = ".data-api",
      C = p.fn[y],
      x = "active",
      E = "btn",
      T = "focus",
      S = '[data-toggle^="button"]',
      A = '[data-toggle="buttons"]',
      k = '[data-toggle="button"]',
      D = '[data-toggle="buttons"] .btn',
      I = 'input:not([type="hidden"])',
      N = ".active",
      O = ".btn",
      P = {
    CLICK_DATA_API: "click" + b + w,
    FOCUS_BLUR_DATA_API: "focus" + b + w + " blur" + b + w,
    LOAD_DATA_API: "load" + b + w
  },
      L = function () {
    function n(t) {
      this._element = t;
    }

    var t = n.prototype;
    return t.toggle = function () {
      var t,
          e,
          n = !0,
          i = !0,
          o = p(this._element).closest(A)[0];
      !o || (t = this._element.querySelector(I)) && ("radio" === t.type ? t.checked && this._element.classList.contains(x) ? n = !1 : (e = o.querySelector(N)) && p(e).removeClass(x) : ("checkbox" !== t.type || "LABEL" === this._element.tagName && t.checked === this._element.classList.contains(x)) && (n = !1), n && (t.checked = !this._element.classList.contains(x), p(t).trigger("change")), t.focus(), i = !1), this._element.hasAttribute("disabled") || this._element.classList.contains("disabled") || (i && this._element.setAttribute("aria-pressed", !this._element.classList.contains(x)), n && p(this._element).toggleClass(x));
    }, t.dispose = function () {
      p.removeData(this._element, _), this._element = null;
    }, n._jQueryInterface = function (e) {
      return this.each(function () {
        var t = p(this).data(_);
        t || (t = new n(this), p(this).data(_, t)), "toggle" === e && t[e]();
      });
    }, s(n, null, [{
      key: "VERSION",
      get: function get() {
        return "4.4.1";
      }
    }]), n;
  }();

  p(document).on(P.CLICK_DATA_API, S, function (t) {
    var e = t.target;
    if (p(e).hasClass(E) || (e = p(e).closest(O)[0]), !e || e.hasAttribute("disabled") || e.classList.contains("disabled")) t.preventDefault();else {
      var n = e.querySelector(I);
      if (n && (n.hasAttribute("disabled") || n.classList.contains("disabled"))) return void t.preventDefault();

      L._jQueryInterface.call(p(e), "toggle");
    }
  }).on(P.FOCUS_BLUR_DATA_API, S, function (t) {
    var e = p(t.target).closest(O)[0];
    p(e).toggleClass(T, /^focus(in)?$/.test(t.type));
  }), p(window).on(P.LOAD_DATA_API, function () {
    for (var t = [].slice.call(document.querySelectorAll(D)), e = 0, n = t.length; e < n; e++) {
      var i = t[e],
          o = i.querySelector(I);
      o.checked || o.hasAttribute("checked") ? i.classList.add(x) : i.classList.remove(x);
    }

    for (var r = 0, s = (t = [].slice.call(document.querySelectorAll(k))).length; r < s; r++) {
      var a = t[r];
      "true" === a.getAttribute("aria-pressed") ? a.classList.add(x) : a.classList.remove(x);
    }
  }), p.fn[y] = L._jQueryInterface, p.fn[y].Constructor = L, p.fn[y].noConflict = function () {
    return p.fn[y] = C, L._jQueryInterface;
  };

  var j = "carousel",
      M = "bs.carousel",
      H = "." + M,
      $ = ".data-api",
      q = p.fn[j],
      z = {
    interval: 5e3,
    keyboard: !0,
    slide: !1,
    pause: "hover",
    wrap: !0,
    touch: !0
  },
      R = {
    interval: "(number|boolean)",
    keyboard: "boolean",
    slide: "(boolean|string)",
    pause: "(string|boolean)",
    wrap: "boolean",
    touch: "boolean"
  },
      F = "next",
      W = "prev",
      B = "left",
      U = "right",
      Q = {
    SLIDE: "slide" + H,
    SLID: "slid" + H,
    KEYDOWN: "keydown" + H,
    MOUSEENTER: "mouseenter" + H,
    MOUSELEAVE: "mouseleave" + H,
    TOUCHSTART: "touchstart" + H,
    TOUCHMOVE: "touchmove" + H,
    TOUCHEND: "touchend" + H,
    POINTERDOWN: "pointerdown" + H,
    POINTERUP: "pointerup" + H,
    DRAG_START: "dragstart" + H,
    LOAD_DATA_API: "load" + H + $,
    CLICK_DATA_API: "click" + H + $
  },
      K = "carousel",
      V = "active",
      Y = "slide",
      X = "carousel-item-right",
      Z = "carousel-item-left",
      G = "carousel-item-next",
      J = "carousel-item-prev",
      tt = "pointer-event",
      et = ".active",
      nt = ".active.carousel-item",
      it = ".carousel-item",
      ot = ".carousel-item img",
      rt = ".carousel-item-next, .carousel-item-prev",
      st = ".carousel-indicators",
      at = "[data-slide], [data-slide-to]",
      lt = '[data-ride="carousel"]',
      ct = {
    TOUCH: "touch",
    PEN: "pen"
  },
      ut = function () {
    function r(t, e) {
      this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this.touchStartX = 0, this.touchDeltaX = 0, this._config = this._getConfig(e), this._element = t, this._indicatorsElement = this._element.querySelector(st), this._touchSupported = "ontouchstart" in document.documentElement || 0 < navigator.maxTouchPoints, this._pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent), this._addEventListeners();
    }

    var t = r.prototype;
    return t.next = function () {
      this._isSliding || this._slide(F);
    }, t.nextWhenVisible = function () {
      !document.hidden && p(this._element).is(":visible") && "hidden" !== p(this._element).css("visibility") && this.next();
    }, t.prev = function () {
      this._isSliding || this._slide(W);
    }, t.pause = function (t) {
      t || (this._isPaused = !0), this._element.querySelector(rt) && (g.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null;
    }, t.cycle = function (t) {
      t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval));
    }, t.to = function (t) {
      var e = this;
      this._activeElement = this._element.querySelector(nt);

      var n = this._getItemIndex(this._activeElement);

      if (!(t > this._items.length - 1 || t < 0)) if (this._isSliding) p(this._element).one(Q.SLID, function () {
        return e.to(t);
      });else {
        if (n === t) return this.pause(), void this.cycle();
        var i = n < t ? F : W;

        this._slide(i, this._items[t]);
      }
    }, t.dispose = function () {
      p(this._element).off(H), p.removeData(this._element, M), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null;
    }, t._getConfig = function (t) {
      return t = l({}, z, {}, t), g.typeCheckConfig(j, t, R), t;
    }, t._handleSwipe = function () {
      var t,
          e = Math.abs(this.touchDeltaX);
      e <= 40 || (t = e / this.touchDeltaX, (this.touchDeltaX = 0) < t && this.prev(), t < 0 && this.next());
    }, t._addEventListeners = function () {
      var e = this;
      this._config.keyboard && p(this._element).on(Q.KEYDOWN, function (t) {
        return e._keydown(t);
      }), "hover" === this._config.pause && p(this._element).on(Q.MOUSEENTER, function (t) {
        return e.pause(t);
      }).on(Q.MOUSELEAVE, function (t) {
        return e.cycle(t);
      }), this._config.touch && this._addTouchEventListeners();
    }, t._addTouchEventListeners = function () {
      var t,
          e,
          n = this;
      this._touchSupported && (t = function t(_t3) {
        n._pointerEvent && ct[_t3.originalEvent.pointerType.toUpperCase()] ? n.touchStartX = _t3.originalEvent.clientX : n._pointerEvent || (n.touchStartX = _t3.originalEvent.touches[0].clientX);
      }, e = function e(t) {
        n._pointerEvent && ct[t.originalEvent.pointerType.toUpperCase()] && (n.touchDeltaX = t.originalEvent.clientX - n.touchStartX), n._handleSwipe(), "hover" === n._config.pause && (n.pause(), n.touchTimeout && clearTimeout(n.touchTimeout), n.touchTimeout = setTimeout(function (t) {
          return n.cycle(t);
        }, 500 + n._config.interval));
      }, p(this._element.querySelectorAll(ot)).on(Q.DRAG_START, function (t) {
        return t.preventDefault();
      }), this._pointerEvent ? (p(this._element).on(Q.POINTERDOWN, t), p(this._element).on(Q.POINTERUP, e), this._element.classList.add(tt)) : (p(this._element).on(Q.TOUCHSTART, t), p(this._element).on(Q.TOUCHMOVE, function (t) {
        var e;
        (e = t).originalEvent.touches && 1 < e.originalEvent.touches.length ? n.touchDeltaX = 0 : n.touchDeltaX = e.originalEvent.touches[0].clientX - n.touchStartX;
      }), p(this._element).on(Q.TOUCHEND, e)));
    }, t._keydown = function (t) {
      if (!/input|textarea/i.test(t.target.tagName)) switch (t.which) {
        case 37:
          t.preventDefault(), this.prev();
          break;

        case 39:
          t.preventDefault(), this.next();
      }
    }, t._getItemIndex = function (t) {
      return this._items = t && t.parentNode ? [].slice.call(t.parentNode.querySelectorAll(it)) : [], this._items.indexOf(t);
    }, t._getItemByDirection = function (t, e) {
      var n = t === F,
          i = t === W,
          o = this._getItemIndex(e),
          r = this._items.length - 1;

      if ((i && 0 === o || n && o === r) && !this._config.wrap) return e;
      var s = (o + (t === W ? -1 : 1)) % this._items.length;
      return -1 == s ? this._items[this._items.length - 1] : this._items[s];
    }, t._triggerSlideEvent = function (t, e) {
      var n = this._getItemIndex(t),
          i = this._getItemIndex(this._element.querySelector(nt)),
          o = p.Event(Q.SLIDE, {
        relatedTarget: t,
        direction: e,
        from: i,
        to: n
      });

      return p(this._element).trigger(o), o;
    }, t._setActiveIndicatorElement = function (t) {
      var e, n;
      this._indicatorsElement && (e = [].slice.call(this._indicatorsElement.querySelectorAll(et)), p(e).removeClass(V), (n = this._indicatorsElement.children[this._getItemIndex(t)]) && p(n).addClass(V));
    }, t._slide = function (t, e) {
      var n,
          i,
          o,
          r,
          s,
          a = this,
          l = this._element.querySelector(nt),
          c = this._getItemIndex(l),
          u = e || l && this._getItemByDirection(t, l),
          h = this._getItemIndex(u),
          d = Boolean(this._interval),
          f = t === F ? (n = Z, i = G, B) : (n = X, i = J, U);

      u && p(u).hasClass(V) ? this._isSliding = !1 : this._triggerSlideEvent(u, f).isDefaultPrevented() || l && u && (this._isSliding = !0, d && this.pause(), this._setActiveIndicatorElement(u), o = p.Event(Q.SLID, {
        relatedTarget: u,
        direction: f,
        from: c,
        to: h
      }), p(this._element).hasClass(Y) ? (p(u).addClass(i), g.reflow(u), p(l).addClass(n), p(u).addClass(n), (r = parseInt(u.getAttribute("data-interval"), 10)) ? (this._config.defaultInterval = this._config.defaultInterval || this._config.interval, this._config.interval = r) : this._config.interval = this._config.defaultInterval || this._config.interval, s = g.getTransitionDurationFromElement(l), p(l).one(g.TRANSITION_END, function () {
        p(u).removeClass(n + " " + i).addClass(V), p(l).removeClass(V + " " + i + " " + n), a._isSliding = !1, setTimeout(function () {
          return p(a._element).trigger(o);
        }, 0);
      }).emulateTransitionEnd(s)) : (p(l).removeClass(V), p(u).addClass(V), this._isSliding = !1, p(this._element).trigger(o)), d && this.cycle());
    }, r._jQueryInterface = function (i) {
      return this.each(function () {
        var t = p(this).data(M),
            e = l({}, z, {}, p(this).data());
        "object" == _typeof2(i) && (e = l({}, e, {}, i));
        var n = "string" == typeof i ? i : e.slide;
        if (t || (t = new r(this, e), p(this).data(M, t)), "number" == typeof i) t.to(i);else if ("string" == typeof n) {
          if (void 0 === t[n]) throw new TypeError('No method named "' + n + '"');
          t[n]();
        } else e.interval && e.ride && (t.pause(), t.cycle());
      });
    }, r._dataApiClickHandler = function (t) {
      var e,
          n,
          i,
          o = g.getSelectorFromElement(this);
      !o || (e = p(o)[0]) && p(e).hasClass(K) && (n = l({}, p(e).data(), {}, p(this).data()), (i = this.getAttribute("data-slide-to")) && (n.interval = !1), r._jQueryInterface.call(p(e), n), i && p(e).data(M).to(i), t.preventDefault());
    }, s(r, null, [{
      key: "VERSION",
      get: function get() {
        return "4.4.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return z;
      }
    }]), r;
  }();

  p(document).on(Q.CLICK_DATA_API, at, ut._dataApiClickHandler), p(window).on(Q.LOAD_DATA_API, function () {
    for (var t = [].slice.call(document.querySelectorAll(lt)), e = 0, n = t.length; e < n; e++) {
      var i = p(t[e]);

      ut._jQueryInterface.call(i, i.data());
    }
  }), p.fn[j] = ut._jQueryInterface, p.fn[j].Constructor = ut, p.fn[j].noConflict = function () {
    return p.fn[j] = q, ut._jQueryInterface;
  };

  var ht = "collapse",
      dt = "bs.collapse",
      ft = "." + dt,
      pt = p.fn[ht],
      gt = {
    toggle: !0,
    parent: ""
  },
      mt = {
    toggle: "boolean",
    parent: "(string|element)"
  },
      vt = {
    SHOW: "show" + ft,
    SHOWN: "shown" + ft,
    HIDE: "hide" + ft,
    HIDDEN: "hidden" + ft,
    CLICK_DATA_API: "click" + ft + ".data-api"
  },
      yt = "show",
      _t = "collapse",
      bt = "collapsing",
      wt = "collapsed",
      Ct = "width",
      xt = "height",
      Et = ".show, .collapsing",
      Tt = '[data-toggle="collapse"]',
      St = function () {
    function a(e, t) {
      this._isTransitioning = !1, this._element = e, this._config = this._getConfig(t), this._triggerArray = [].slice.call(document.querySelectorAll('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'));

      for (var n = [].slice.call(document.querySelectorAll(Tt)), i = 0, o = n.length; i < o; i++) {
        var r = n[i],
            s = g.getSelectorFromElement(r),
            a = [].slice.call(document.querySelectorAll(s)).filter(function (t) {
          return t === e;
        });
        null !== s && 0 < a.length && (this._selector = s, this._triggerArray.push(r));
      }

      this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle();
    }

    var t = a.prototype;
    return t.toggle = function () {
      p(this._element).hasClass(yt) ? this.hide() : this.show();
    }, t.show = function () {
      var t,
          e,
          n,
          i,
          o,
          r,
          s = this;
      this._isTransitioning || p(this._element).hasClass(yt) || (this._parent && 0 === (t = [].slice.call(this._parent.querySelectorAll(Et)).filter(function (t) {
        return "string" == typeof s._config.parent ? t.getAttribute("data-parent") === s._config.parent : t.classList.contains(_t);
      })).length && (t = null), t && (e = p(t).not(this._selector).data(dt)) && e._isTransitioning || (n = p.Event(vt.SHOW), p(this._element).trigger(n), n.isDefaultPrevented() || (t && (a._jQueryInterface.call(p(t).not(this._selector), "hide"), e || p(t).data(dt, null)), i = this._getDimension(), p(this._element).removeClass(_t).addClass(bt), this._element.style[i] = 0, this._triggerArray.length && p(this._triggerArray).removeClass(wt).attr("aria-expanded", !0), this.setTransitioning(!0), o = "scroll" + (i[0].toUpperCase() + i.slice(1)), r = g.getTransitionDurationFromElement(this._element), p(this._element).one(g.TRANSITION_END, function () {
        p(s._element).removeClass(bt).addClass(_t).addClass(yt), s._element.style[i] = "", s.setTransitioning(!1), p(s._element).trigger(vt.SHOWN);
      }).emulateTransitionEnd(r), this._element.style[i] = this._element[o] + "px")));
    }, t.hide = function () {
      var t = this;

      if (!this._isTransitioning && p(this._element).hasClass(yt)) {
        var e = p.Event(vt.HIDE);

        if (p(this._element).trigger(e), !e.isDefaultPrevented()) {
          var n = this._getDimension();

          this._element.style[n] = this._element.getBoundingClientRect()[n] + "px", g.reflow(this._element), p(this._element).addClass(bt).removeClass(_t).removeClass(yt);
          var i = this._triggerArray.length;
          if (0 < i) for (var o = 0; o < i; o++) {
            var r = this._triggerArray[o],
                s = g.getSelectorFromElement(r);
            null !== s && (p([].slice.call(document.querySelectorAll(s))).hasClass(yt) || p(r).addClass(wt).attr("aria-expanded", !1));
          }
          this.setTransitioning(!0);
          this._element.style[n] = "";
          var a = g.getTransitionDurationFromElement(this._element);
          p(this._element).one(g.TRANSITION_END, function () {
            t.setTransitioning(!1), p(t._element).removeClass(bt).addClass(_t).trigger(vt.HIDDEN);
          }).emulateTransitionEnd(a);
        }
      }
    }, t.setTransitioning = function (t) {
      this._isTransitioning = t;
    }, t.dispose = function () {
      p.removeData(this._element, dt), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null;
    }, t._getConfig = function (t) {
      return (t = l({}, gt, {}, t)).toggle = Boolean(t.toggle), g.typeCheckConfig(ht, t, mt), t;
    }, t._getDimension = function () {
      return p(this._element).hasClass(Ct) ? Ct : xt;
    }, t._getParent = function () {
      var t,
          n = this;
      g.isElement(this._config.parent) ? (t = this._config.parent, void 0 !== this._config.parent.jquery && (t = this._config.parent[0])) : t = document.querySelector(this._config.parent);
      var e = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]',
          i = [].slice.call(t.querySelectorAll(e));
      return p(i).each(function (t, e) {
        n._addAriaAndCollapsedClass(a._getTargetFromElement(e), [e]);
      }), t;
    }, t._addAriaAndCollapsedClass = function (t, e) {
      var n = p(t).hasClass(yt);
      e.length && p(e).toggleClass(wt, !n).attr("aria-expanded", n);
    }, a._getTargetFromElement = function (t) {
      var e = g.getSelectorFromElement(t);
      return e ? document.querySelector(e) : null;
    }, a._jQueryInterface = function (i) {
      return this.each(function () {
        var t = p(this),
            e = t.data(dt),
            n = l({}, gt, {}, t.data(), {}, "object" == _typeof2(i) && i ? i : {});

        if (!e && n.toggle && /show|hide/.test(i) && (n.toggle = !1), e || (e = new a(this, n), t.data(dt, e)), "string" == typeof i) {
          if (void 0 === e[i]) throw new TypeError('No method named "' + i + '"');
          e[i]();
        }
      });
    }, s(a, null, [{
      key: "VERSION",
      get: function get() {
        return "4.4.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return gt;
      }
    }]), a;
  }();

  p(document).on(vt.CLICK_DATA_API, Tt, function (t) {
    "A" === t.currentTarget.tagName && t.preventDefault();
    var n = p(this),
        e = g.getSelectorFromElement(this),
        i = [].slice.call(document.querySelectorAll(e));
    p(i).each(function () {
      var t = p(this),
          e = t.data(dt) ? "toggle" : n.data();

      St._jQueryInterface.call(t, e);
    });
  }), p.fn[ht] = St._jQueryInterface, p.fn[ht].Constructor = St, p.fn[ht].noConflict = function () {
    return p.fn[ht] = pt, St._jQueryInterface;
  };

  var At = "undefined" != typeof window && "undefined" != typeof document && "undefined" != typeof navigator,
      kt = function () {
    for (var t = ["Edge", "Trident", "Firefox"], e = 0; e < t.length; e += 1) {
      if (At && 0 <= navigator.userAgent.indexOf(t[e])) return 1;
    }

    return 0;
  }();

  var Dt = At && window.Promise ? function (t) {
    var e = !1;
    return function () {
      e || (e = !0, window.Promise.resolve().then(function () {
        e = !1, t();
      }));
    };
  } : function (t) {
    var e = !1;
    return function () {
      e || (e = !0, setTimeout(function () {
        e = !1, t();
      }, kt));
    };
  };

  function It(t) {
    return t && "[object Function]" === {}.toString.call(t);
  }

  function Nt(t, e) {
    if (1 !== t.nodeType) return [];
    var n = t.ownerDocument.defaultView.getComputedStyle(t, null);
    return e ? n[e] : n;
  }

  function Ot(t) {
    return "HTML" === t.nodeName ? t : t.parentNode || t.host;
  }

  function Pt(t) {
    if (!t) return document.body;

    switch (t.nodeName) {
      case "HTML":
      case "BODY":
        return t.ownerDocument.body;

      case "#document":
        return t.body;
    }

    var e = Nt(t),
        n = e.overflow,
        i = e.overflowX,
        o = e.overflowY;
    return /(auto|scroll|overlay)/.test(n + o + i) ? t : Pt(Ot(t));
  }

  function Lt(t) {
    return t && t.referenceNode ? t.referenceNode : t;
  }

  var jt = At && !(!window.MSInputMethodContext || !document.documentMode),
      Mt = At && /MSIE 10/.test(navigator.userAgent);

  function Ht(t) {
    return 11 === t ? jt : 10 !== t && jt || Mt;
  }

  function $t(t) {
    if (!t) return document.documentElement;

    for (var e = Ht(10) ? document.body : null, n = t.offsetParent || null; n === e && t.nextElementSibling;) {
      n = (t = t.nextElementSibling).offsetParent;
    }

    var i = n && n.nodeName;
    return i && "BODY" !== i && "HTML" !== i ? -1 !== ["TH", "TD", "TABLE"].indexOf(n.nodeName) && "static" === Nt(n, "position") ? $t(n) : n : t ? t.ownerDocument.documentElement : document.documentElement;
  }

  function qt(t) {
    return null !== t.parentNode ? qt(t.parentNode) : t;
  }

  function zt(t, e) {
    if (!(t && t.nodeType && e && e.nodeType)) return document.documentElement;
    var n = t.compareDocumentPosition(e) & Node.DOCUMENT_POSITION_FOLLOWING,
        i = n ? t : e,
        o = n ? e : t,
        r = document.createRange();
    r.setStart(i, 0), r.setEnd(o, 0);
    var s,
        a,
        l = r.commonAncestorContainer;
    if (t !== l && e !== l || i.contains(o)) return "BODY" === (a = (s = l).nodeName) || "HTML" !== a && $t(s.firstElementChild) !== s ? $t(l) : l;
    var c = qt(t);
    return c.host ? zt(c.host, e) : zt(t, qt(e).host);
  }

  function Rt(t, e) {
    var n = "top" === (1 < arguments.length && void 0 !== e ? e : "top") ? "scrollTop" : "scrollLeft",
        i = t.nodeName;
    if ("BODY" !== i && "HTML" !== i) return t[n];
    var o = t.ownerDocument.documentElement;
    return (t.ownerDocument.scrollingElement || o)[n];
  }

  function Ft(t, e) {
    var n = "x" === e ? "Left" : "Top",
        i = "Left" == n ? "Right" : "Bottom";
    return parseFloat(t["border" + n + "Width"], 10) + parseFloat(t["border" + i + "Width"], 10);
  }

  function Wt(t, e, n, i) {
    return Math.max(e["offset" + t], e["scroll" + t], n["client" + t], n["offset" + t], n["scroll" + t], Ht(10) ? parseInt(n["offset" + t]) + parseInt(i["margin" + ("Height" === t ? "Top" : "Left")]) + parseInt(i["margin" + ("Height" === t ? "Bottom" : "Right")]) : 0);
  }

  function Bt(t) {
    var e = t.body,
        n = t.documentElement,
        i = Ht(10) && getComputedStyle(n);
    return {
      height: Wt("Height", e, n, i),
      width: Wt("Width", e, n, i)
    };
  }

  var Ut = function Ut(t, e) {
    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
  },
      Qt = function Qt(t, e, n) {
    return e && Kt(t.prototype, e), n && Kt(t, n), t;
  };

  function Kt(t, e) {
    for (var n = 0; n < e.length; n++) {
      var i = e[n];
      i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
    }
  }

  function Vt(t, e, n) {
    return e in t ? Object.defineProperty(t, e, {
      value: n,
      enumerable: !0,
      configurable: !0,
      writable: !0
    }) : t[e] = n, t;
  }

  var Yt = Object.assign || function (t) {
    for (var e = 1; e < arguments.length; e++) {
      var n = arguments[e];

      for (var i in n) {
        Object.prototype.hasOwnProperty.call(n, i) && (t[i] = n[i]);
      }
    }

    return t;
  };

  function Xt(t) {
    return Yt({}, t, {
      right: t.left + t.width,
      bottom: t.top + t.height
    });
  }

  function Zt(t) {
    var e,
        n,
        i = {};

    try {
      Ht(10) ? (i = t.getBoundingClientRect(), e = Rt(t, "top"), n = Rt(t, "left"), i.top += e, i.left += n, i.bottom += e, i.right += n) : i = t.getBoundingClientRect();
    } catch (t) {}

    var o,
        r = {
      left: i.left,
      top: i.top,
      width: i.right - i.left,
      height: i.bottom - i.top
    },
        s = "HTML" === t.nodeName ? Bt(t.ownerDocument) : {},
        a = s.width || t.clientWidth || r.width,
        l = s.height || t.clientHeight || r.height,
        c = t.offsetWidth - a,
        u = t.offsetHeight - l;
    return (c || u) && (c -= Ft(o = Nt(t), "x"), u -= Ft(o, "y"), r.width -= c, r.height -= u), Xt(r);
  }

  function Gt(t, e, n) {
    var i = 2 < arguments.length && void 0 !== n && n,
        o = Ht(10),
        r = "HTML" === e.nodeName,
        s = Zt(t),
        a = Zt(e),
        l = Pt(t),
        c = Nt(e),
        u = parseFloat(c.borderTopWidth, 10),
        h = parseFloat(c.borderLeftWidth, 10);
    i && r && (a.top = Math.max(a.top, 0), a.left = Math.max(a.left, 0));
    var d,
        f,
        p = Xt({
      top: s.top - a.top - u,
      left: s.left - a.left - h,
      width: s.width,
      height: s.height
    });
    return p.marginTop = 0, p.marginLeft = 0, !o && r && (d = parseFloat(c.marginTop, 10), f = parseFloat(c.marginLeft, 10), p.top -= u - d, p.bottom -= u - d, p.left -= h - f, p.right -= h - f, p.marginTop = d, p.marginLeft = f), (o && !i ? e.contains(l) : e === l && "BODY" !== l.nodeName) && (p = function (t, e, n) {
      var i = 2 < arguments.length && void 0 !== n && n,
          o = Rt(e, "top"),
          r = Rt(e, "left"),
          s = i ? -1 : 1;
      return t.top += o * s, t.bottom += o * s, t.left += r * s, t.right += r * s, t;
    }(p, e)), p;
  }

  function Jt(t) {
    if (!t || !t.parentElement || Ht()) return document.documentElement;

    for (var e = t.parentElement; e && "none" === Nt(e, "transform");) {
      e = e.parentElement;
    }

    return e || document.documentElement;
  }

  function te(t, e, n, i, o) {
    var r,
        s,
        a,
        l,
        c,
        u = 4 < arguments.length && void 0 !== o && o,
        h = {
      top: 0,
      left: 0
    },
        d = u ? Jt(t) : zt(t, Lt(e));
    "viewport" === i ? h = function (t, e) {
      var n = 1 < arguments.length && void 0 !== e && e,
          i = t.ownerDocument.documentElement,
          o = Gt(t, i),
          r = Math.max(i.clientWidth, window.innerWidth || 0),
          s = Math.max(i.clientHeight, window.innerHeight || 0),
          a = n ? 0 : Rt(i),
          l = n ? 0 : Rt(i, "left");
      return Xt({
        top: a - o.top + o.marginTop,
        left: l - o.left + o.marginLeft,
        width: r,
        height: s
      });
    }(d, u) : (r = void 0, "scrollParent" === i ? "BODY" === (r = Pt(Ot(e))).nodeName && (r = t.ownerDocument.documentElement) : r = "window" === i ? t.ownerDocument.documentElement : i, s = Gt(r, d, u), "HTML" !== r.nodeName || function t(e) {
      var n = e.nodeName;
      if ("BODY" === n || "HTML" === n) return !1;
      if ("fixed" === Nt(e, "position")) return !0;
      var i = Ot(e);
      return !!i && t(i);
    }(d) ? h = s : (l = (a = Bt(t.ownerDocument)).height, c = a.width, h.top += s.top - s.marginTop, h.bottom = l + s.top, h.left += s.left - s.marginLeft, h.right = c + s.left));
    var f = "number" == typeof (n = n || 0);
    return h.left += f ? n : n.left || 0, h.top += f ? n : n.top || 0, h.right -= f ? n : n.right || 0, h.bottom -= f ? n : n.bottom || 0, h;
  }

  function ee(t, e, i, n, o, r) {
    var s = 5 < arguments.length && void 0 !== r ? r : 0;
    if (-1 === t.indexOf("auto")) return t;
    var a = te(i, n, s, o),
        l = {
      top: {
        width: a.width,
        height: e.top - a.top
      },
      right: {
        width: a.right - e.right,
        height: a.height
      },
      bottom: {
        width: a.width,
        height: a.bottom - e.bottom
      },
      left: {
        width: e.left - a.left,
        height: a.height
      }
    },
        c = Object.keys(l).map(function (t) {
      return Yt({
        key: t
      }, l[t], {
        area: (e = l[t]).width * e.height
      });
      var e;
    }).sort(function (t, e) {
      return e.area - t.area;
    }),
        u = c.filter(function (t) {
      var e = t.width,
          n = t.height;
      return e >= i.clientWidth && n >= i.clientHeight;
    }),
        h = 0 < u.length ? u[0].key : c[0].key,
        d = t.split("-")[1];
    return h + (d ? "-" + d : "");
  }

  function ne(t, e, n, i) {
    var o = 3 < arguments.length && void 0 !== i ? i : null;
    return Gt(n, o ? Jt(e) : zt(e, Lt(n)), o);
  }

  function ie(t) {
    var e = t.ownerDocument.defaultView.getComputedStyle(t),
        n = parseFloat(e.marginTop || 0) + parseFloat(e.marginBottom || 0),
        i = parseFloat(e.marginLeft || 0) + parseFloat(e.marginRight || 0);
    return {
      width: t.offsetWidth + i,
      height: t.offsetHeight + n
    };
  }

  function oe(t) {
    var e = {
      left: "right",
      right: "left",
      bottom: "top",
      top: "bottom"
    };
    return t.replace(/left|right|bottom|top/g, function (t) {
      return e[t];
    });
  }

  function re(t, e, n) {
    n = n.split("-")[0];
    var i = ie(t),
        o = {
      width: i.width,
      height: i.height
    },
        r = -1 !== ["right", "left"].indexOf(n),
        s = r ? "top" : "left",
        a = r ? "left" : "top",
        l = r ? "height" : "width",
        c = r ? "width" : "height";
    return o[s] = e[s] + e[l] / 2 - i[l] / 2, o[a] = n === a ? e[a] - i[c] : e[oe(a)], o;
  }

  function se(t, e) {
    return Array.prototype.find ? t.find(e) : t.filter(e)[0];
  }

  function ae(t, n, e) {
    return (void 0 === e ? t : t.slice(0, function (t, e, n) {
      if (Array.prototype.findIndex) return t.findIndex(function (t) {
        return t[e] === n;
      });
      var i = se(t, function (t) {
        return t[e] === n;
      });
      return t.indexOf(i);
    }(t, "name", e))).forEach(function (t) {
      t.function && console.warn("`modifier.function` is deprecated, use `modifier.fn`!");
      var e = t.function || t.fn;
      t.enabled && It(e) && (n.offsets.popper = Xt(n.offsets.popper), n.offsets.reference = Xt(n.offsets.reference), n = e(n, t));
    }), n;
  }

  function le(t, n) {
    return t.some(function (t) {
      var e = t.name;
      return t.enabled && e === n;
    });
  }

  function ce(t) {
    for (var e = [!1, "ms", "Webkit", "Moz", "O"], n = t.charAt(0).toUpperCase() + t.slice(1), i = 0; i < e.length; i++) {
      var o = e[i],
          r = o ? "" + o + n : t;
      if (void 0 !== document.body.style[r]) return r;
    }

    return null;
  }

  function ue(t) {
    var e = t.ownerDocument;
    return e ? e.defaultView : window;
  }

  function he(t, e, n, i) {
    n.updateBound = i, ue(t).addEventListener("resize", n.updateBound, {
      passive: !0
    });
    var o = Pt(t);
    return function t(e, n, i, o) {
      var r = "BODY" === e.nodeName,
          s = r ? e.ownerDocument.defaultView : e;
      s.addEventListener(n, i, {
        passive: !0
      }), r || t(Pt(s.parentNode), n, i, o), o.push(s);
    }(o, "scroll", n.updateBound, n.scrollParents), n.scrollElement = o, n.eventsEnabled = !0, n;
  }

  function de() {
    var t, e;
    this.state.eventsEnabled && (cancelAnimationFrame(this.scheduleUpdate), this.state = (t = this.reference, e = this.state, ue(t).removeEventListener("resize", e.updateBound), e.scrollParents.forEach(function (t) {
      t.removeEventListener("scroll", e.updateBound);
    }), e.updateBound = null, e.scrollParents = [], e.scrollElement = null, e.eventsEnabled = !1, e));
  }

  function fe(t) {
    return "" !== t && !isNaN(parseFloat(t)) && isFinite(t);
  }

  function pe(n, i) {
    Object.keys(i).forEach(function (t) {
      var e = "";
      -1 !== ["width", "height", "top", "right", "bottom", "left"].indexOf(t) && fe(i[t]) && (e = "px"), n.style[t] = i[t] + e;
    });
  }

  function ge(t, e) {
    function n(t) {
      return t;
    }

    var i = t.offsets,
        o = i.popper,
        r = i.reference,
        s = Math.round,
        a = Math.floor,
        l = s(r.width),
        c = s(o.width),
        u = -1 !== ["left", "right"].indexOf(t.placement),
        h = -1 !== t.placement.indexOf("-"),
        d = e ? u || h || l % 2 == c % 2 ? s : a : n,
        f = e ? s : n;
    return {
      left: d(l % 2 == 1 && c % 2 == 1 && !h && e ? o.left - 1 : o.left),
      top: f(o.top),
      bottom: f(o.bottom),
      right: d(o.right)
    };
  }

  var me = At && /Firefox/i.test(navigator.userAgent);

  function ve(t, e, n) {
    var i,
        o,
        r = se(t, function (t) {
      return t.name === e;
    }),
        s = !!r && t.some(function (t) {
      return t.name === n && t.enabled && t.order < r.order;
    });
    return s || (i = "`" + e + "`", o = "`" + n + "`", console.warn(o + " modifier is required by " + i + " modifier in order to work, be sure to include it before " + i + "!")), s;
  }

  var ye = ["auto-start", "auto", "auto-end", "top-start", "top", "top-end", "right-start", "right", "right-end", "bottom-end", "bottom", "bottom-start", "left-end", "left", "left-start"],
      _e = ye.slice(3);

  function be(t, e) {
    var n = 1 < arguments.length && void 0 !== e && e,
        i = _e.indexOf(t),
        o = _e.slice(i + 1).concat(_e.slice(0, i));

    return n ? o.reverse() : o;
  }

  var we = "flip",
      Ce = "clockwise",
      xe = "counterclockwise";

  function Ee(t, o, r, e) {
    var s = [0, 0],
        a = -1 !== ["right", "left"].indexOf(e),
        n = t.split(/(\+|\-)/).map(function (t) {
      return t.trim();
    }),
        i = n.indexOf(se(n, function (t) {
      return -1 !== t.search(/,|\s/);
    }));
    n[i] && -1 === n[i].indexOf(",") && console.warn("Offsets separated by white space(s) are deprecated, use a comma (,) instead.");
    var l = /\s*,\s*|\s+/;
    return (-1 !== i ? [n.slice(0, i).concat([n[i].split(l)[0]]), [n[i].split(l)[1]].concat(n.slice(i + 1))] : [n]).map(function (t, e) {
      var n = (1 === e ? !a : a) ? "height" : "width",
          i = !1;
      return t.reduce(function (t, e) {
        return "" === t[t.length - 1] && -1 !== ["+", "-"].indexOf(e) ? (t[t.length - 1] = e, i = !0, t) : i ? (t[t.length - 1] += e, i = !1, t) : t.concat(e);
      }, []).map(function (t) {
        return function (t, e, n, i) {
          var o = t.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
              r = +o[1],
              s = o[2];
          if (!r) return t;
          if (0 !== s.indexOf("%")) return "vh" !== s && "vw" !== s ? r : ("vh" === s ? Math.max(document.documentElement.clientHeight, window.innerHeight || 0) : Math.max(document.documentElement.clientWidth, window.innerWidth || 0)) / 100 * r;
          var a = void 0;

          switch (s) {
            case "%p":
              a = n;
              break;

            case "%":
            case "%r":
            default:
              a = i;
          }

          return Xt(a)[e] / 100 * r;
        }(t, n, o, r);
      });
    }).forEach(function (n, i) {
      n.forEach(function (t, e) {
        fe(t) && (s[i] += t * ("-" === n[e - 1] ? -1 : 1));
      });
    }), s;
  }

  var Te = {
    placement: "bottom",
    positionFixed: !1,
    eventsEnabled: !0,
    removeOnDestroy: !1,
    onCreate: function onCreate() {},
    onUpdate: function onUpdate() {},
    modifiers: {
      shift: {
        order: 100,
        enabled: !0,
        fn: function fn(t) {
          var e,
              n,
              i,
              o,
              r,
              s,
              a,
              l = t.placement,
              c = l.split("-")[0],
              u = l.split("-")[1];
          return u && (n = (e = t.offsets).reference, i = e.popper, s = (o = -1 !== ["bottom", "top"].indexOf(c)) ? "width" : "height", a = {
            start: Vt({}, r = o ? "left" : "top", n[r]),
            end: Vt({}, r, n[r] + n[s] - i[s])
          }, t.offsets.popper = Yt({}, i, a[u])), t;
        }
      },
      offset: {
        order: 200,
        enabled: !0,
        fn: function fn(t, e) {
          var n = e.offset,
              i = t.placement,
              o = t.offsets,
              r = o.popper,
              s = o.reference,
              a = i.split("-")[0],
              l = void 0,
              l = fe(+n) ? [+n, 0] : Ee(n, r, s, a);
          return "left" === a ? (r.top += l[0], r.left -= l[1]) : "right" === a ? (r.top += l[0], r.left += l[1]) : "top" === a ? (r.left += l[0], r.top -= l[1]) : "bottom" === a && (r.left += l[0], r.top += l[1]), t.popper = r, t;
        },
        offset: 0
      },
      preventOverflow: {
        order: 300,
        enabled: !0,
        fn: function fn(t, i) {
          var e = i.boundariesElement || $t(t.instance.popper);
          t.instance.reference === e && (e = $t(e));
          var n = ce("transform"),
              o = t.instance.popper.style,
              r = o.top,
              s = o.left,
              a = o[n];
          o.top = "", o.left = "", o[n] = "";
          var l = te(t.instance.popper, t.instance.reference, i.padding, e, t.positionFixed);
          o.top = r, o.left = s, o[n] = a, i.boundaries = l;
          var c = i.priority,
              u = t.offsets.popper,
              h = {
            primary: function primary(t) {
              var e = u[t];
              return u[t] < l[t] && !i.escapeWithReference && (e = Math.max(u[t], l[t])), Vt({}, t, e);
            },
            secondary: function secondary(t) {
              var e = "right" === t ? "left" : "top",
                  n = u[e];
              return u[t] > l[t] && !i.escapeWithReference && (n = Math.min(u[e], l[t] - ("right" === t ? u.width : u.height))), Vt({}, e, n);
            }
          };
          return c.forEach(function (t) {
            var e = -1 !== ["left", "top"].indexOf(t) ? "primary" : "secondary";
            u = Yt({}, u, h[e](t));
          }), t.offsets.popper = u, t;
        },
        priority: ["left", "right", "top", "bottom"],
        padding: 5,
        boundariesElement: "scrollParent"
      },
      keepTogether: {
        order: 400,
        enabled: !0,
        fn: function fn(t) {
          var e = t.offsets,
              n = e.popper,
              i = e.reference,
              o = t.placement.split("-")[0],
              r = Math.floor,
              s = -1 !== ["top", "bottom"].indexOf(o),
              a = s ? "right" : "bottom",
              l = s ? "left" : "top",
              c = s ? "width" : "height";
          return n[a] < r(i[l]) && (t.offsets.popper[l] = r(i[l]) - n[c]), n[l] > r(i[a]) && (t.offsets.popper[l] = r(i[a])), t;
        }
      },
      arrow: {
        order: 500,
        enabled: !0,
        fn: function fn(t, e) {
          var n;
          if (!ve(t.instance.modifiers, "arrow", "keepTogether")) return t;
          var i = e.element;

          if ("string" == typeof i) {
            if (!(i = t.instance.popper.querySelector(i))) return t;
          } else if (!t.instance.popper.contains(i)) return console.warn("WARNING: `arrow.element` must be child of its popper element!"), t;

          var o = t.placement.split("-")[0],
              r = t.offsets,
              s = r.popper,
              a = r.reference,
              l = -1 !== ["left", "right"].indexOf(o),
              c = l ? "height" : "width",
              u = l ? "Top" : "Left",
              h = u.toLowerCase(),
              d = l ? "left" : "top",
              f = l ? "bottom" : "right",
              p = ie(i)[c];
          a[f] - p < s[h] && (t.offsets.popper[h] -= s[h] - (a[f] - p)), a[h] + p > s[f] && (t.offsets.popper[h] += a[h] + p - s[f]), t.offsets.popper = Xt(t.offsets.popper);

          var g = a[h] + a[c] / 2 - p / 2,
              m = Nt(t.instance.popper),
              v = parseFloat(m["margin" + u], 10),
              y = parseFloat(m["border" + u + "Width"], 10),
              _ = g - t.offsets.popper[h] - v - y,
              _ = Math.max(Math.min(s[c] - p, _), 0);

          return t.arrowElement = i, t.offsets.arrow = (Vt(n = {}, h, Math.round(_)), Vt(n, d, ""), n), t;
        },
        element: "[x-arrow]"
      },
      flip: {
        order: 600,
        enabled: !0,
        fn: function fn(m, v) {
          if (le(m.instance.modifiers, "inner")) return m;
          if (m.flipped && m.placement === m.originalPlacement) return m;
          var y = te(m.instance.popper, m.instance.reference, v.padding, v.boundariesElement, m.positionFixed),
              _ = m.placement.split("-")[0],
              b = oe(_),
              w = m.placement.split("-")[1] || "",
              C = [];

          switch (v.behavior) {
            case we:
              C = [_, b];
              break;

            case Ce:
              C = be(_);
              break;

            case xe:
              C = be(_, !0);
              break;

            default:
              C = v.behavior;
          }

          return C.forEach(function (t, e) {
            if (_ !== t || C.length === e + 1) return m;
            _ = m.placement.split("-")[0], b = oe(_);
            var n,
                i = m.offsets.popper,
                o = m.offsets.reference,
                r = Math.floor,
                s = "left" === _ && r(i.right) > r(o.left) || "right" === _ && r(i.left) < r(o.right) || "top" === _ && r(i.bottom) > r(o.top) || "bottom" === _ && r(i.top) < r(o.bottom),
                a = r(i.left) < r(y.left),
                l = r(i.right) > r(y.right),
                c = r(i.top) < r(y.top),
                u = r(i.bottom) > r(y.bottom),
                h = "left" === _ && a || "right" === _ && l || "top" === _ && c || "bottom" === _ && u,
                d = -1 !== ["top", "bottom"].indexOf(_),
                f = !!v.flipVariations && (d && "start" === w && a || d && "end" === w && l || !d && "start" === w && c || !d && "end" === w && u),
                p = !!v.flipVariationsByContent && (d && "start" === w && l || d && "end" === w && a || !d && "start" === w && u || !d && "end" === w && c),
                g = f || p;
            (s || h || g) && (m.flipped = !0, (s || h) && (_ = C[e + 1]), g && (w = "end" === (n = w) ? "start" : "start" === n ? "end" : n), m.placement = _ + (w ? "-" + w : ""), m.offsets.popper = Yt({}, m.offsets.popper, re(m.instance.popper, m.offsets.reference, m.placement)), m = ae(m.instance.modifiers, m, "flip"));
          }), m;
        },
        behavior: "flip",
        padding: 5,
        boundariesElement: "viewport",
        flipVariations: !1,
        flipVariationsByContent: !1
      },
      inner: {
        order: 700,
        enabled: !1,
        fn: function fn(t) {
          var e = t.placement,
              n = e.split("-")[0],
              i = t.offsets,
              o = i.popper,
              r = i.reference,
              s = -1 !== ["left", "right"].indexOf(n),
              a = -1 === ["top", "left"].indexOf(n);
          return o[s ? "left" : "top"] = r[n] - (a ? o[s ? "width" : "height"] : 0), t.placement = oe(e), t.offsets.popper = Xt(o), t;
        }
      },
      hide: {
        order: 800,
        enabled: !0,
        fn: function fn(t) {
          if (!ve(t.instance.modifiers, "hide", "preventOverflow")) return t;
          var e = t.offsets.reference,
              n = se(t.instance.modifiers, function (t) {
            return "preventOverflow" === t.name;
          }).boundaries;

          if (e.bottom < n.top || e.left > n.right || e.top > n.bottom || e.right < n.left) {
            if (!0 === t.hide) return t;
            t.hide = !0, t.attributes["x-out-of-boundaries"] = "";
          } else {
            if (!1 === t.hide) return t;
            t.hide = !1, t.attributes["x-out-of-boundaries"] = !1;
          }

          return t;
        }
      },
      computeStyle: {
        order: 850,
        enabled: !0,
        fn: function fn(t, e) {
          var n = e.x,
              i = e.y,
              o = t.offsets.popper,
              r = se(t.instance.modifiers, function (t) {
            return "applyStyle" === t.name;
          }).gpuAcceleration;
          void 0 !== r && console.warn("WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!");
          var s,
              a,
              l = void 0 !== r ? r : e.gpuAcceleration,
              c = $t(t.instance.popper),
              u = Zt(c),
              h = {
            position: o.position
          },
              d = ge(t, window.devicePixelRatio < 2 || !me),
              f = "bottom" === n ? "top" : "bottom",
              p = "right" === i ? "left" : "right",
              g = ce("transform"),
              m = void 0,
              v = void 0,
              v = "bottom" == f ? "HTML" === c.nodeName ? -c.clientHeight + d.bottom : -u.height + d.bottom : d.top,
              m = "right" == p ? "HTML" === c.nodeName ? -c.clientWidth + d.right : -u.width + d.right : d.left;
          l && g ? (h[g] = "translate3d(" + m + "px, " + v + "px, 0)", h[f] = 0, h[p] = 0, h.willChange = "transform") : (s = "bottom" == f ? -1 : 1, a = "right" == p ? -1 : 1, h[f] = v * s, h[p] = m * a, h.willChange = f + ", " + p);
          var y = {
            "x-placement": t.placement
          };
          return t.attributes = Yt({}, y, t.attributes), t.styles = Yt({}, h, t.styles), t.arrowStyles = Yt({}, t.offsets.arrow, t.arrowStyles), t;
        },
        gpuAcceleration: !0,
        x: "bottom",
        y: "right"
      },
      applyStyle: {
        order: 900,
        enabled: !0,
        fn: function fn(t) {
          var e, n;
          return pe(t.instance.popper, t.styles), e = t.instance.popper, n = t.attributes, Object.keys(n).forEach(function (t) {
            !1 !== n[t] ? e.setAttribute(t, n[t]) : e.removeAttribute(t);
          }), t.arrowElement && Object.keys(t.arrowStyles).length && pe(t.arrowElement, t.arrowStyles), t;
        },
        onLoad: function onLoad(t, e, n, i, o) {
          var r = ne(o, e, t, n.positionFixed),
              s = ee(n.placement, r, e, t, n.modifiers.flip.boundariesElement, n.modifiers.flip.padding);
          return e.setAttribute("x-placement", s), pe(e, {
            position: n.positionFixed ? "fixed" : "absolute"
          }), n;
        },
        gpuAcceleration: void 0
      }
    }
  },
      Se = (Qt(Ae, [{
    key: "update",
    value: function value() {
      return function () {
        var t;
        this.state.isDestroyed || ((t = {
          instance: this,
          styles: {},
          arrowStyles: {},
          attributes: {},
          flipped: !1,
          offsets: {}
        }).offsets.reference = ne(this.state, this.popper, this.reference, this.options.positionFixed), t.placement = ee(this.options.placement, t.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), t.originalPlacement = t.placement, t.positionFixed = this.options.positionFixed, t.offsets.popper = re(this.popper, t.offsets.reference, t.placement), t.offsets.popper.position = this.options.positionFixed ? "fixed" : "absolute", t = ae(this.modifiers, t), this.state.isCreated ? this.options.onUpdate(t) : (this.state.isCreated = !0, this.options.onCreate(t)));
      }.call(this);
    }
  }, {
    key: "destroy",
    value: function value() {
      return function () {
        return this.state.isDestroyed = !0, le(this.modifiers, "applyStyle") && (this.popper.removeAttribute("x-placement"), this.popper.style.position = "", this.popper.style.top = "", this.popper.style.left = "", this.popper.style.right = "", this.popper.style.bottom = "", this.popper.style.willChange = "", this.popper.style[ce("transform")] = ""), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this;
      }.call(this);
    }
  }, {
    key: "enableEventListeners",
    value: function value() {
      return function () {
        this.state.eventsEnabled || (this.state = he(this.reference, this.options, this.state, this.scheduleUpdate));
      }.call(this);
    }
  }, {
    key: "disableEventListeners",
    value: function value() {
      return de.call(this);
    }
  }]), Ae);

  function Ae(t, e) {
    var n = this,
        i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
    Ut(this, Ae), this.scheduleUpdate = function () {
      return requestAnimationFrame(n.update);
    }, this.update = Dt(this.update.bind(this)), this.options = Yt({}, Ae.Defaults, i), this.state = {
      isDestroyed: !1,
      isCreated: !1,
      scrollParents: []
    }, this.reference = t && t.jquery ? t[0] : t, this.popper = e && e.jquery ? e[0] : e, this.options.modifiers = {}, Object.keys(Yt({}, Ae.Defaults.modifiers, i.modifiers)).forEach(function (t) {
      n.options.modifiers[t] = Yt({}, Ae.Defaults.modifiers[t] || {}, i.modifiers ? i.modifiers[t] : {});
    }), this.modifiers = Object.keys(this.options.modifiers).map(function (t) {
      return Yt({
        name: t
      }, n.options.modifiers[t]);
    }).sort(function (t, e) {
      return t.order - e.order;
    }), this.modifiers.forEach(function (t) {
      t.enabled && It(t.onLoad) && t.onLoad(n.reference, n.popper, n.options, t, n.state);
    }), this.update();
    var o = this.options.eventsEnabled;
    o && this.enableEventListeners(), this.state.eventsEnabled = o;
  }

  Se.Utils = ("undefined" != typeof window ? window : global).PopperUtils, Se.placements = ye, Se.Defaults = Te;

  var ke = "dropdown",
      De = "bs.dropdown",
      Ie = "." + De,
      Ne = ".data-api",
      Oe = p.fn[ke],
      Pe = new RegExp("38|40|27"),
      Le = {
    HIDE: "hide" + Ie,
    HIDDEN: "hidden" + Ie,
    SHOW: "show" + Ie,
    SHOWN: "shown" + Ie,
    CLICK: "click" + Ie,
    CLICK_DATA_API: "click" + Ie + Ne,
    KEYDOWN_DATA_API: "keydown" + Ie + Ne,
    KEYUP_DATA_API: "keyup" + Ie + Ne
  },
      je = "disabled",
      Me = "show",
      He = "dropup",
      $e = "dropright",
      qe = "dropleft",
      ze = "dropdown-menu-right",
      Re = "position-static",
      Fe = '[data-toggle="dropdown"]',
      We = ".dropdown form",
      Be = ".dropdown-menu",
      Ue = ".navbar-nav",
      Qe = ".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)",
      Ke = "top-start",
      Ve = "top-end",
      Ye = "bottom-start",
      Xe = "bottom-end",
      Ze = "right-start",
      Ge = "left-start",
      Je = {
    offset: 0,
    flip: !0,
    boundary: "scrollParent",
    reference: "toggle",
    display: "dynamic",
    popperConfig: null
  },
      tn = {
    offset: "(number|string|function)",
    flip: "boolean",
    boundary: "(string|element)",
    reference: "(string|element)",
    display: "string",
    popperConfig: "(null|object)"
  },
      en = function () {
    function c(t, e) {
      this._element = t, this._popper = null, this._config = this._getConfig(e), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners();
    }

    var t = c.prototype;
    return t.toggle = function () {
      var t;
      this._element.disabled || p(this._element).hasClass(je) || (t = p(this._menu).hasClass(Me), c._clearMenus(), t || this.show(!0));
    }, t.show = function (t) {
      if (void 0 === t && (t = !1), !(this._element.disabled || p(this._element).hasClass(je) || p(this._menu).hasClass(Me))) {
        var e = {
          relatedTarget: this._element
        },
            n = p.Event(Le.SHOW, e),
            i = c._getParentFromElement(this._element);

        if (p(i).trigger(n), !n.isDefaultPrevented()) {
          if (!this._inNavbar && t) {
            if (void 0 === Se) throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org/)");
            var o = this._element;
            "parent" === this._config.reference ? o = i : g.isElement(this._config.reference) && (o = this._config.reference, void 0 !== this._config.reference.jquery && (o = this._config.reference[0])), "scrollParent" !== this._config.boundary && p(i).addClass(Re), this._popper = new Se(o, this._menu, this._getPopperConfig());
          }

          "ontouchstart" in document.documentElement && 0 === p(i).closest(Ue).length && p(document.body).children().on("mouseover", null, p.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), p(this._menu).toggleClass(Me), p(i).toggleClass(Me).trigger(p.Event(Le.SHOWN, e));
        }
      }
    }, t.hide = function () {
      var t, e, n;
      this._element.disabled || p(this._element).hasClass(je) || !p(this._menu).hasClass(Me) || (t = {
        relatedTarget: this._element
      }, e = p.Event(Le.HIDE, t), n = c._getParentFromElement(this._element), p(n).trigger(e), e.isDefaultPrevented() || (this._popper && this._popper.destroy(), p(this._menu).toggleClass(Me), p(n).toggleClass(Me).trigger(p.Event(Le.HIDDEN, t))));
    }, t.dispose = function () {
      p.removeData(this._element, De), p(this._element).off(Ie), this._element = null, (this._menu = null) !== this._popper && (this._popper.destroy(), this._popper = null);
    }, t.update = function () {
      this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate();
    }, t._addEventListeners = function () {
      var e = this;
      p(this._element).on(Le.CLICK, function (t) {
        t.preventDefault(), t.stopPropagation(), e.toggle();
      });
    }, t._getConfig = function (t) {
      return t = l({}, this.constructor.Default, {}, p(this._element).data(), {}, t), g.typeCheckConfig(ke, t, this.constructor.DefaultType), t;
    }, t._getMenuElement = function () {
      var t;
      return this._menu || (t = c._getParentFromElement(this._element)) && (this._menu = t.querySelector(Be)), this._menu;
    }, t._getPlacement = function () {
      var t = p(this._element.parentNode),
          e = Ye;
      return t.hasClass(He) ? (e = Ke, p(this._menu).hasClass(ze) && (e = Ve)) : t.hasClass($e) ? e = Ze : t.hasClass(qe) ? e = Ge : p(this._menu).hasClass(ze) && (e = Xe), e;
    }, t._detectNavbar = function () {
      return 0 < p(this._element).closest(".navbar").length;
    }, t._getOffset = function () {
      var e = this,
          t = {};
      return "function" == typeof this._config.offset ? t.fn = function (t) {
        return t.offsets = l({}, t.offsets, {}, e._config.offset(t.offsets, e._element) || {}), t;
      } : t.offset = this._config.offset, t;
    }, t._getPopperConfig = function () {
      var t = {
        placement: this._getPlacement(),
        modifiers: {
          offset: this._getOffset(),
          flip: {
            enabled: this._config.flip
          },
          preventOverflow: {
            boundariesElement: this._config.boundary
          }
        }
      };
      return "static" === this._config.display && (t.modifiers.applyStyle = {
        enabled: !1
      }), l({}, t, {}, this._config.popperConfig);
    }, c._jQueryInterface = function (e) {
      return this.each(function () {
        var t = p(this).data(De);

        if (t || (t = new c(this, "object" == _typeof2(e) ? e : null), p(this).data(De, t)), "string" == typeof e) {
          if (void 0 === t[e]) throw new TypeError('No method named "' + e + '"');
          t[e]();
        }
      });
    }, c._clearMenus = function (t) {
      if (!t || 3 !== t.which && ("keyup" !== t.type || 9 === t.which)) for (var e = [].slice.call(document.querySelectorAll(Fe)), n = 0, i = e.length; n < i; n++) {
        var o,
            r,
            s = c._getParentFromElement(e[n]),
            a = p(e[n]).data(De),
            l = {
          relatedTarget: e[n]
        };

        t && "click" === t.type && (l.clickEvent = t), a && (o = a._menu, p(s).hasClass(Me) && (t && ("click" === t.type && /input|textarea/i.test(t.target.tagName) || "keyup" === t.type && 9 === t.which) && p.contains(s, t.target) || (r = p.Event(Le.HIDE, l), p(s).trigger(r), r.isDefaultPrevented() || ("ontouchstart" in document.documentElement && p(document.body).children().off("mouseover", null, p.noop), e[n].setAttribute("aria-expanded", "false"), a._popper && a._popper.destroy(), p(o).removeClass(Me), p(s).removeClass(Me).trigger(p.Event(Le.HIDDEN, l))))));
      }
    }, c._getParentFromElement = function (t) {
      var e,
          n = g.getSelectorFromElement(t);
      return n && (e = document.querySelector(n)), e || t.parentNode;
    }, c._dataApiKeydownHandler = function (t) {
      if ((/input|textarea/i.test(t.target.tagName) ? !(32 === t.which || 27 !== t.which && (40 !== t.which && 38 !== t.which || p(t.target).closest(Be).length)) : Pe.test(t.which)) && (t.preventDefault(), t.stopPropagation(), !this.disabled && !p(this).hasClass(je))) {
        var e,
            n = c._getParentFromElement(this),
            i = p(n).hasClass(Me);

        if (i || 27 !== t.which) {
          if (!i || i && (27 === t.which || 32 === t.which)) return 27 === t.which && (e = n.querySelector(Fe), p(e).trigger("focus")), void p(this).trigger("click");
          var o,
              r = [].slice.call(n.querySelectorAll(Qe)).filter(function (t) {
            return p(t).is(":visible");
          });
          0 !== r.length && (o = r.indexOf(t.target), 38 === t.which && 0 < o && o--, 40 === t.which && o < r.length - 1 && o++, o < 0 && (o = 0), r[o].focus());
        }
      }
    }, s(c, null, [{
      key: "VERSION",
      get: function get() {
        return "4.4.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return Je;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return tn;
      }
    }]), c;
  }();

  p(document).on(Le.KEYDOWN_DATA_API, Fe, en._dataApiKeydownHandler).on(Le.KEYDOWN_DATA_API, Be, en._dataApiKeydownHandler).on(Le.CLICK_DATA_API + " " + Le.KEYUP_DATA_API, en._clearMenus).on(Le.CLICK_DATA_API, Fe, function (t) {
    t.preventDefault(), t.stopPropagation(), en._jQueryInterface.call(p(this), "toggle");
  }).on(Le.CLICK_DATA_API, We, function (t) {
    t.stopPropagation();
  }), p.fn[ke] = en._jQueryInterface, p.fn[ke].Constructor = en, p.fn[ke].noConflict = function () {
    return p.fn[ke] = Oe, en._jQueryInterface;
  };

  var nn = "modal",
      on = "bs.modal",
      rn = "." + on,
      sn = p.fn[nn],
      an = {
    backdrop: !0,
    keyboard: !0,
    focus: !0,
    show: !0
  },
      ln = {
    backdrop: "(boolean|string)",
    keyboard: "boolean",
    focus: "boolean",
    show: "boolean"
  },
      cn = {
    HIDE: "hide" + rn,
    HIDE_PREVENTED: "hidePrevented" + rn,
    HIDDEN: "hidden" + rn,
    SHOW: "show" + rn,
    SHOWN: "shown" + rn,
    FOCUSIN: "focusin" + rn,
    RESIZE: "resize" + rn,
    CLICK_DISMISS: "click.dismiss" + rn,
    KEYDOWN_DISMISS: "keydown.dismiss" + rn,
    MOUSEUP_DISMISS: "mouseup.dismiss" + rn,
    MOUSEDOWN_DISMISS: "mousedown.dismiss" + rn,
    CLICK_DATA_API: "click" + rn + ".data-api"
  },
      un = "modal-dialog-scrollable",
      hn = "modal-scrollbar-measure",
      dn = "modal-backdrop",
      fn = "modal-open",
      pn = "fade",
      gn = "show",
      mn = "modal-static",
      vn = ".modal-dialog",
      yn = ".modal-body",
      _n = '[data-toggle="modal"]',
      bn = '[data-dismiss="modal"]',
      wn = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
      Cn = ".sticky-top",
      xn = function () {
    function o(t, e) {
      this._config = this._getConfig(e), this._element = t, this._dialog = t.querySelector(vn), this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._isTransitioning = !1, this._scrollbarWidth = 0;
    }

    var t = o.prototype;
    return t.toggle = function (t) {
      return this._isShown ? this.hide() : this.show(t);
    }, t.show = function (t) {
      var e,
          n = this;
      this._isShown || this._isTransitioning || (p(this._element).hasClass(pn) && (this._isTransitioning = !0), e = p.Event(cn.SHOW, {
        relatedTarget: t
      }), p(this._element).trigger(e), this._isShown || e.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), this._setEscapeEvent(), this._setResizeEvent(), p(this._element).on(cn.CLICK_DISMISS, bn, function (t) {
        return n.hide(t);
      }), p(this._dialog).on(cn.MOUSEDOWN_DISMISS, function () {
        p(n._element).one(cn.MOUSEUP_DISMISS, function (t) {
          p(t.target).is(n._element) && (n._ignoreBackdropClick = !0);
        });
      }), this._showBackdrop(function () {
        return n._showElement(t);
      })));
    }, t.hide = function (t) {
      var e,
          n,
          i,
          o = this;
      t && t.preventDefault(), this._isShown && !this._isTransitioning && (e = p.Event(cn.HIDE), p(this._element).trigger(e), this._isShown && !e.isDefaultPrevented() && (this._isShown = !1, (n = p(this._element).hasClass(pn)) && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), p(document).off(cn.FOCUSIN), p(this._element).removeClass(gn), p(this._element).off(cn.CLICK_DISMISS), p(this._dialog).off(cn.MOUSEDOWN_DISMISS), n ? (i = g.getTransitionDurationFromElement(this._element), p(this._element).one(g.TRANSITION_END, function (t) {
        return o._hideModal(t);
      }).emulateTransitionEnd(i)) : this._hideModal()));
    }, t.dispose = function () {
      [window, this._element, this._dialog].forEach(function (t) {
        return p(t).off(rn);
      }), p(document).off(cn.FOCUSIN), p.removeData(this._element, on), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._isTransitioning = null, this._scrollbarWidth = null;
    }, t.handleUpdate = function () {
      this._adjustDialog();
    }, t._getConfig = function (t) {
      return t = l({}, an, {}, t), g.typeCheckConfig(nn, t, ln), t;
    }, t._triggerBackdropTransition = function () {
      var t = this;

      if ("static" === this._config.backdrop) {
        var e = p.Event(cn.HIDE_PREVENTED);
        if (p(this._element).trigger(e), e.defaultPrevented) return;

        this._element.classList.add(mn);

        var n = g.getTransitionDurationFromElement(this._element);
        p(this._element).one(g.TRANSITION_END, function () {
          t._element.classList.remove(mn);
        }).emulateTransitionEnd(n), this._element.focus();
      } else this.hide();
    }, t._showElement = function (t) {
      var e = this,
          n = p(this._element).hasClass(pn),
          i = this._dialog ? this._dialog.querySelector(yn) : null;
      this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.setAttribute("aria-modal", !0), p(this._dialog).hasClass(un) && i ? i.scrollTop = 0 : this._element.scrollTop = 0, n && g.reflow(this._element), p(this._element).addClass(gn), this._config.focus && this._enforceFocus();

      function o() {
        e._config.focus && e._element.focus(), e._isTransitioning = !1, p(e._element).trigger(s);
      }

      var r,
          s = p.Event(cn.SHOWN, {
        relatedTarget: t
      });
      n ? (r = g.getTransitionDurationFromElement(this._dialog), p(this._dialog).one(g.TRANSITION_END, o).emulateTransitionEnd(r)) : o();
    }, t._enforceFocus = function () {
      var e = this;
      p(document).off(cn.FOCUSIN).on(cn.FOCUSIN, function (t) {
        document !== t.target && e._element !== t.target && 0 === p(e._element).has(t.target).length && e._element.focus();
      });
    }, t._setEscapeEvent = function () {
      var e = this;
      this._isShown && this._config.keyboard ? p(this._element).on(cn.KEYDOWN_DISMISS, function (t) {
        27 === t.which && e._triggerBackdropTransition();
      }) : this._isShown || p(this._element).off(cn.KEYDOWN_DISMISS);
    }, t._setResizeEvent = function () {
      var e = this;
      this._isShown ? p(window).on(cn.RESIZE, function (t) {
        return e.handleUpdate(t);
      }) : p(window).off(cn.RESIZE);
    }, t._hideModal = function () {
      var t = this;
      this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._element.removeAttribute("aria-modal"), this._isTransitioning = !1, this._showBackdrop(function () {
        p(document.body).removeClass(fn), t._resetAdjustments(), t._resetScrollbar(), p(t._element).trigger(cn.HIDDEN);
      });
    }, t._removeBackdrop = function () {
      this._backdrop && (p(this._backdrop).remove(), this._backdrop = null);
    }, t._showBackdrop = function (t) {
      var e,
          n,
          i = this,
          o = p(this._element).hasClass(pn) ? pn : "";

      if (this._isShown && this._config.backdrop) {
        if (this._backdrop = document.createElement("div"), this._backdrop.className = dn, o && this._backdrop.classList.add(o), p(this._backdrop).appendTo(document.body), p(this._element).on(cn.CLICK_DISMISS, function (t) {
          i._ignoreBackdropClick ? i._ignoreBackdropClick = !1 : t.target === t.currentTarget && i._triggerBackdropTransition();
        }), o && g.reflow(this._backdrop), p(this._backdrop).addClass(gn), !t) return;
        if (!o) return void t();
        var r = g.getTransitionDurationFromElement(this._backdrop);
        p(this._backdrop).one(g.TRANSITION_END, t).emulateTransitionEnd(r);
      } else {
        !this._isShown && this._backdrop ? (p(this._backdrop).removeClass(gn), e = function e() {
          i._removeBackdrop(), t && t();
        }, p(this._element).hasClass(pn) ? (n = g.getTransitionDurationFromElement(this._backdrop), p(this._backdrop).one(g.TRANSITION_END, e).emulateTransitionEnd(n)) : e()) : t && t();
      }
    }, t._adjustDialog = function () {
      var t = this._element.scrollHeight > document.documentElement.clientHeight;
      !this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px");
    }, t._resetAdjustments = function () {
      this._element.style.paddingLeft = "", this._element.style.paddingRight = "";
    }, t._checkScrollbar = function () {
      var t = document.body.getBoundingClientRect();
      this._isBodyOverflowing = t.left + t.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth();
    }, t._setScrollbar = function () {
      var t,
          e,
          n,
          i,
          o = this;
      this._isBodyOverflowing && (t = [].slice.call(document.querySelectorAll(wn)), e = [].slice.call(document.querySelectorAll(Cn)), p(t).each(function (t, e) {
        var n = e.style.paddingRight,
            i = p(e).css("padding-right");
        p(e).data("padding-right", n).css("padding-right", parseFloat(i) + o._scrollbarWidth + "px");
      }), p(e).each(function (t, e) {
        var n = e.style.marginRight,
            i = p(e).css("margin-right");
        p(e).data("margin-right", n).css("margin-right", parseFloat(i) - o._scrollbarWidth + "px");
      }), n = document.body.style.paddingRight, i = p(document.body).css("padding-right"), p(document.body).data("padding-right", n).css("padding-right", parseFloat(i) + this._scrollbarWidth + "px")), p(document.body).addClass(fn);
    }, t._resetScrollbar = function () {
      var t = [].slice.call(document.querySelectorAll(wn));
      p(t).each(function (t, e) {
        var n = p(e).data("padding-right");
        p(e).removeData("padding-right"), e.style.paddingRight = n || "";
      });
      var e = [].slice.call(document.querySelectorAll("" + Cn));
      p(e).each(function (t, e) {
        var n = p(e).data("margin-right");
        void 0 !== n && p(e).css("margin-right", n).removeData("margin-right");
      });
      var n = p(document.body).data("padding-right");
      p(document.body).removeData("padding-right"), document.body.style.paddingRight = n || "";
    }, t._getScrollbarWidth = function () {
      var t = document.createElement("div");
      t.className = hn, document.body.appendChild(t);
      var e = t.getBoundingClientRect().width - t.clientWidth;
      return document.body.removeChild(t), e;
    }, o._jQueryInterface = function (n, i) {
      return this.each(function () {
        var t = p(this).data(on),
            e = l({}, an, {}, p(this).data(), {}, "object" == _typeof2(n) && n ? n : {});

        if (t || (t = new o(this, e), p(this).data(on, t)), "string" == typeof n) {
          if (void 0 === t[n]) throw new TypeError('No method named "' + n + '"');
          t[n](i);
        } else e.show && t.show(i);
      });
    }, s(o, null, [{
      key: "VERSION",
      get: function get() {
        return "4.4.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return an;
      }
    }]), o;
  }();

  p(document).on(cn.CLICK_DATA_API, _n, function (t) {
    var e,
        n = this,
        i = g.getSelectorFromElement(this);
    i && (e = document.querySelector(i));
    var o = p(e).data(on) ? "toggle" : l({}, p(e).data(), {}, p(this).data());
    "A" !== this.tagName && "AREA" !== this.tagName || t.preventDefault();
    var r = p(e).one(cn.SHOW, function (t) {
      t.isDefaultPrevented() || r.one(cn.HIDDEN, function () {
        p(n).is(":visible") && n.focus();
      });
    });

    xn._jQueryInterface.call(p(e), o, this);
  }), p.fn[nn] = xn._jQueryInterface, p.fn[nn].Constructor = xn, p.fn[nn].noConflict = function () {
    return p.fn[nn] = sn, xn._jQueryInterface;
  };
  var En = ["background", "cite", "href", "itemtype", "longdesc", "poster", "src", "xlink:href"],
      Tn = {
    "*": ["class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i],
    a: ["target", "href", "title", "rel"],
    area: [],
    b: [],
    br: [],
    col: [],
    code: [],
    div: [],
    em: [],
    hr: [],
    h1: [],
    h2: [],
    h3: [],
    h4: [],
    h5: [],
    h6: [],
    i: [],
    img: ["src", "alt", "title", "width", "height"],
    li: [],
    ol: [],
    p: [],
    pre: [],
    s: [],
    small: [],
    span: [],
    sub: [],
    sup: [],
    strong: [],
    u: [],
    ul: []
  },
      Sn = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi,
      An = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i;

  function kn(t, r, e) {
    if (0 === t.length) return t;
    if (e && "function" == typeof e) return e(t);

    for (var n = new window.DOMParser().parseFromString(t, "text/html"), s = Object.keys(r), a = [].slice.call(n.body.querySelectorAll("*")), i = function i(t) {
      var e = a[t],
          n = e.nodeName.toLowerCase();
      if (-1 === s.indexOf(e.nodeName.toLowerCase())) return e.parentNode.removeChild(e), "continue";
      var i = [].slice.call(e.attributes),
          o = [].concat(r["*"] || [], r[n] || []);
      i.forEach(function (t) {
        !function (t, e) {
          var n = t.nodeName.toLowerCase();
          if (-1 !== e.indexOf(n)) return -1 === En.indexOf(n) || Boolean(t.nodeValue.match(Sn) || t.nodeValue.match(An));

          for (var i = e.filter(function (t) {
            return t instanceof RegExp;
          }), o = 0, r = i.length; o < r; o++) {
            if (n.match(i[o])) return 1;
          }
        }(t, o) && e.removeAttribute(t.nodeName);
      });
    }, o = 0, l = a.length; o < l; o++) {
      i(o);
    }

    return n.body.innerHTML;
  }

  var Dn = "tooltip",
      In = "bs.tooltip",
      Nn = "." + In,
      On = p.fn[Dn],
      Pn = "bs-tooltip",
      Ln = new RegExp("(^|\\s)" + Pn + "\\S+", "g"),
      jn = ["sanitize", "whiteList", "sanitizeFn"],
      Mn = {
    animation: "boolean",
    template: "string",
    title: "(string|element|function)",
    trigger: "string",
    delay: "(number|object)",
    html: "boolean",
    selector: "(string|boolean)",
    placement: "(string|function)",
    offset: "(number|string|function)",
    container: "(string|element|boolean)",
    fallbackPlacement: "(string|array)",
    boundary: "(string|element)",
    sanitize: "boolean",
    sanitizeFn: "(null|function)",
    whiteList: "object",
    popperConfig: "(null|object)"
  },
      Hn = {
    AUTO: "auto",
    TOP: "top",
    RIGHT: "right",
    BOTTOM: "bottom",
    LEFT: "left"
  },
      $n = {
    animation: !0,
    template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: "hover focus",
    title: "",
    delay: 0,
    html: !1,
    selector: !1,
    placement: "top",
    offset: 0,
    container: !1,
    fallbackPlacement: "flip",
    boundary: "scrollParent",
    sanitize: !0,
    sanitizeFn: null,
    whiteList: Tn,
    popperConfig: null
  },
      qn = "show",
      zn = "out",
      Rn = {
    HIDE: "hide" + Nn,
    HIDDEN: "hidden" + Nn,
    SHOW: "show" + Nn,
    SHOWN: "shown" + Nn,
    INSERTED: "inserted" + Nn,
    CLICK: "click" + Nn,
    FOCUSIN: "focusin" + Nn,
    FOCUSOUT: "focusout" + Nn,
    MOUSEENTER: "mouseenter" + Nn,
    MOUSELEAVE: "mouseleave" + Nn
  },
      Fn = "fade",
      Wn = "show",
      Bn = ".tooltip-inner",
      Un = ".arrow",
      Qn = "hover",
      Kn = "focus",
      Vn = "click",
      Yn = "manual",
      Xn = function () {
    function i(t, e) {
      if (void 0 === Se) throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org/)");
      this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = t, this.config = this._getConfig(e), this.tip = null, this._setListeners();
    }

    var t = i.prototype;
    return t.enable = function () {
      this._isEnabled = !0;
    }, t.disable = function () {
      this._isEnabled = !1;
    }, t.toggleEnabled = function () {
      this._isEnabled = !this._isEnabled;
    }, t.toggle = function (t) {
      if (this._isEnabled) if (t) {
        var e = this.constructor.DATA_KEY,
            n = p(t.currentTarget).data(e);
        n || (n = new this.constructor(t.currentTarget, this._getDelegateConfig()), p(t.currentTarget).data(e, n)), n._activeTrigger.click = !n._activeTrigger.click, n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n);
      } else {
        if (p(this.getTipElement()).hasClass(Wn)) return void this._leave(null, this);

        this._enter(null, this);
      }
    }, t.dispose = function () {
      clearTimeout(this._timeout), p.removeData(this.element, this.constructor.DATA_KEY), p(this.element).off(this.constructor.EVENT_KEY), p(this.element).closest(".modal").off("hide.bs.modal", this._hideModalHandler), this.tip && p(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, this._activeTrigger = null, this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null;
    }, t.show = function () {
      var e = this;
      if ("none" === p(this.element).css("display")) throw new Error("Please use show on visible elements");
      var t = p.Event(this.constructor.Event.SHOW);

      if (this.isWithContent() && this._isEnabled) {
        p(this.element).trigger(t);
        var n = g.findShadowRoot(this.element),
            i = p.contains(null !== n ? n : this.element.ownerDocument.documentElement, this.element);
        if (t.isDefaultPrevented() || !i) return;
        var o = this.getTipElement(),
            r = g.getUID(this.constructor.NAME);
        o.setAttribute("id", r), this.element.setAttribute("aria-describedby", r), this.setContent(), this.config.animation && p(o).addClass(Fn);

        var s = "function" == typeof this.config.placement ? this.config.placement.call(this, o, this.element) : this.config.placement,
            a = this._getAttachment(s);

        this.addAttachmentClass(a);

        var l = this._getContainer();

        p(o).data(this.constructor.DATA_KEY, this), p.contains(this.element.ownerDocument.documentElement, this.tip) || p(o).appendTo(l), p(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new Se(this.element, o, this._getPopperConfig(a)), p(o).addClass(Wn), "ontouchstart" in document.documentElement && p(document.body).children().on("mouseover", null, p.noop);

        var c,
            u = function u() {
          e.config.animation && e._fixTransition();
          var t = e._hoverState;
          e._hoverState = null, p(e.element).trigger(e.constructor.Event.SHOWN), t === zn && e._leave(null, e);
        };

        p(this.tip).hasClass(Fn) ? (c = g.getTransitionDurationFromElement(this.tip), p(this.tip).one(g.TRANSITION_END, u).emulateTransitionEnd(c)) : u();
      }
    }, t.hide = function (t) {
      function e() {
        i._hoverState !== qn && o.parentNode && o.parentNode.removeChild(o), i._cleanTipClass(), i.element.removeAttribute("aria-describedby"), p(i.element).trigger(i.constructor.Event.HIDDEN), null !== i._popper && i._popper.destroy(), t && t();
      }

      var n,
          i = this,
          o = this.getTipElement(),
          r = p.Event(this.constructor.Event.HIDE);
      p(this.element).trigger(r), r.isDefaultPrevented() || (p(o).removeClass(Wn), "ontouchstart" in document.documentElement && p(document.body).children().off("mouseover", null, p.noop), this._activeTrigger[Vn] = !1, this._activeTrigger[Kn] = !1, this._activeTrigger[Qn] = !1, p(this.tip).hasClass(Fn) ? (n = g.getTransitionDurationFromElement(o), p(o).one(g.TRANSITION_END, e).emulateTransitionEnd(n)) : e(), this._hoverState = "");
    }, t.update = function () {
      null !== this._popper && this._popper.scheduleUpdate();
    }, t.isWithContent = function () {
      return Boolean(this.getTitle());
    }, t.addAttachmentClass = function (t) {
      p(this.getTipElement()).addClass(Pn + "-" + t);
    }, t.getTipElement = function () {
      return this.tip = this.tip || p(this.config.template)[0], this.tip;
    }, t.setContent = function () {
      var t = this.getTipElement();
      this.setElementContent(p(t.querySelectorAll(Bn)), this.getTitle()), p(t).removeClass(Fn + " " + Wn);
    }, t.setElementContent = function (t, e) {
      "object" != _typeof2(e) || !e.nodeType && !e.jquery ? this.config.html ? (this.config.sanitize && (e = kn(e, this.config.whiteList, this.config.sanitizeFn)), t.html(e)) : t.text(e) : this.config.html ? p(e).parent().is(t) || t.empty().append(e) : t.text(p(e).text());
    }, t.getTitle = function () {
      return this.element.getAttribute("data-original-title") || ("function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title);
    }, t._getPopperConfig = function (t) {
      var e = this;
      return l({}, {
        placement: t,
        modifiers: {
          offset: this._getOffset(),
          flip: {
            behavior: this.config.fallbackPlacement
          },
          arrow: {
            element: Un
          },
          preventOverflow: {
            boundariesElement: this.config.boundary
          }
        },
        onCreate: function onCreate(t) {
          t.originalPlacement !== t.placement && e._handlePopperPlacementChange(t);
        },
        onUpdate: function onUpdate(t) {
          return e._handlePopperPlacementChange(t);
        }
      }, {}, this.config.popperConfig);
    }, t._getOffset = function () {
      var e = this,
          t = {};
      return "function" == typeof this.config.offset ? t.fn = function (t) {
        return t.offsets = l({}, t.offsets, {}, e.config.offset(t.offsets, e.element) || {}), t;
      } : t.offset = this.config.offset, t;
    }, t._getContainer = function () {
      return !1 === this.config.container ? document.body : g.isElement(this.config.container) ? p(this.config.container) : p(document).find(this.config.container);
    }, t._getAttachment = function (t) {
      return Hn[t.toUpperCase()];
    }, t._setListeners = function () {
      var i = this;
      this.config.trigger.split(" ").forEach(function (t) {
        var e, n;
        "click" === t ? p(i.element).on(i.constructor.Event.CLICK, i.config.selector, function (t) {
          return i.toggle(t);
        }) : t !== Yn && (e = t === Qn ? i.constructor.Event.MOUSEENTER : i.constructor.Event.FOCUSIN, n = t === Qn ? i.constructor.Event.MOUSELEAVE : i.constructor.Event.FOCUSOUT, p(i.element).on(e, i.config.selector, function (t) {
          return i._enter(t);
        }).on(n, i.config.selector, function (t) {
          return i._leave(t);
        }));
      }), this._hideModalHandler = function () {
        i.element && i.hide();
      }, p(this.element).closest(".modal").on("hide.bs.modal", this._hideModalHandler), this.config.selector ? this.config = l({}, this.config, {
        trigger: "manual",
        selector: ""
      }) : this._fixTitle();
    }, t._fixTitle = function () {
      var t = _typeof2(this.element.getAttribute("data-original-title"));

      !this.element.getAttribute("title") && "string" == t || (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""));
    }, t._enter = function (t, e) {
      var n = this.constructor.DATA_KEY;
      (e = e || p(t.currentTarget).data(n)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), p(t.currentTarget).data(n, e)), t && (e._activeTrigger["focusin" === t.type ? Kn : Qn] = !0), p(e.getTipElement()).hasClass(Wn) || e._hoverState === qn ? e._hoverState = qn : (clearTimeout(e._timeout), e._hoverState = qn, e.config.delay && e.config.delay.show ? e._timeout = setTimeout(function () {
        e._hoverState === qn && e.show();
      }, e.config.delay.show) : e.show());
    }, t._leave = function (t, e) {
      var n = this.constructor.DATA_KEY;
      (e = e || p(t.currentTarget).data(n)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), p(t.currentTarget).data(n, e)), t && (e._activeTrigger["focusout" === t.type ? Kn : Qn] = !1), e._isWithActiveTrigger() || (clearTimeout(e._timeout), e._hoverState = zn, e.config.delay && e.config.delay.hide ? e._timeout = setTimeout(function () {
        e._hoverState === zn && e.hide();
      }, e.config.delay.hide) : e.hide());
    }, t._isWithActiveTrigger = function () {
      for (var t in this._activeTrigger) {
        if (this._activeTrigger[t]) return !0;
      }

      return !1;
    }, t._getConfig = function (t) {
      var e = p(this.element).data();
      return Object.keys(e).forEach(function (t) {
        -1 !== jn.indexOf(t) && delete e[t];
      }), "number" == typeof (t = l({}, this.constructor.Default, {}, e, {}, "object" == _typeof2(t) && t ? t : {})).delay && (t.delay = {
        show: t.delay,
        hide: t.delay
      }), "number" == typeof t.title && (t.title = t.title.toString()), "number" == typeof t.content && (t.content = t.content.toString()), g.typeCheckConfig(Dn, t, this.constructor.DefaultType), t.sanitize && (t.template = kn(t.template, t.whiteList, t.sanitizeFn)), t;
    }, t._getDelegateConfig = function () {
      var t = {};
      if (this.config) for (var e in this.config) {
        this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
      }
      return t;
    }, t._cleanTipClass = function () {
      var t = p(this.getTipElement()),
          e = t.attr("class").match(Ln);
      null !== e && e.length && t.removeClass(e.join(""));
    }, t._handlePopperPlacementChange = function (t) {
      var e = t.instance;
      this.tip = e.popper, this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(t.placement));
    }, t._fixTransition = function () {
      var t = this.getTipElement(),
          e = this.config.animation;
      null === t.getAttribute("x-placement") && (p(t).removeClass(Fn), this.config.animation = !1, this.hide(), this.show(), this.config.animation = e);
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = p(this).data(In),
            e = "object" == _typeof2(n) && n;

        if ((t || !/dispose|hide/.test(n)) && (t || (t = new i(this, e), p(this).data(In, t)), "string" == typeof n)) {
          if (void 0 === t[n]) throw new TypeError('No method named "' + n + '"');
          t[n]();
        }
      });
    }, s(i, null, [{
      key: "VERSION",
      get: function get() {
        return "4.4.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return $n;
      }
    }, {
      key: "NAME",
      get: function get() {
        return Dn;
      }
    }, {
      key: "DATA_KEY",
      get: function get() {
        return In;
      }
    }, {
      key: "Event",
      get: function get() {
        return Rn;
      }
    }, {
      key: "EVENT_KEY",
      get: function get() {
        return Nn;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return Mn;
      }
    }]), i;
  }();

  p.fn[Dn] = Xn._jQueryInterface, p.fn[Dn].Constructor = Xn, p.fn[Dn].noConflict = function () {
    return p.fn[Dn] = On, Xn._jQueryInterface;
  };

  var Zn = "popover",
      Gn = "bs.popover",
      Jn = "." + Gn,
      ti = p.fn[Zn],
      ei = "bs-popover",
      ni = new RegExp("(^|\\s)" + ei + "\\S+", "g"),
      ii = l({}, Xn.Default, {
    placement: "right",
    trigger: "click",
    content: "",
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
  }),
      oi = l({}, Xn.DefaultType, {
    content: "(string|element|function)"
  }),
      ri = "fade",
      si = "show",
      ai = ".popover-header",
      li = ".popover-body",
      ci = {
    HIDE: "hide" + Jn,
    HIDDEN: "hidden" + Jn,
    SHOW: "show" + Jn,
    SHOWN: "shown" + Jn,
    INSERTED: "inserted" + Jn,
    CLICK: "click" + Jn,
    FOCUSIN: "focusin" + Jn,
    FOCUSOUT: "focusout" + Jn,
    MOUSEENTER: "mouseenter" + Jn,
    MOUSELEAVE: "mouseleave" + Jn
  },
      ui = function (t) {
    var e, n;

    function i() {
      return t.apply(this, arguments) || this;
    }

    n = t, (e = i).prototype = Object.create(n.prototype), (e.prototype.constructor = e).__proto__ = n;
    var o = i.prototype;
    return o.isWithContent = function () {
      return this.getTitle() || this._getContent();
    }, o.addAttachmentClass = function (t) {
      p(this.getTipElement()).addClass(ei + "-" + t);
    }, o.getTipElement = function () {
      return this.tip = this.tip || p(this.config.template)[0], this.tip;
    }, o.setContent = function () {
      var t = p(this.getTipElement());
      this.setElementContent(t.find(ai), this.getTitle());

      var e = this._getContent();

      "function" == typeof e && (e = e.call(this.element)), this.setElementContent(t.find(li), e), t.removeClass(ri + " " + si);
    }, o._getContent = function () {
      return this.element.getAttribute("data-content") || this.config.content;
    }, o._cleanTipClass = function () {
      var t = p(this.getTipElement()),
          e = t.attr("class").match(ni);
      null !== e && 0 < e.length && t.removeClass(e.join(""));
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = p(this).data(Gn),
            e = "object" == _typeof2(n) ? n : null;

        if ((t || !/dispose|hide/.test(n)) && (t || (t = new i(this, e), p(this).data(Gn, t)), "string" == typeof n)) {
          if (void 0 === t[n]) throw new TypeError('No method named "' + n + '"');
          t[n]();
        }
      });
    }, s(i, null, [{
      key: "VERSION",
      get: function get() {
        return "4.4.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return ii;
      }
    }, {
      key: "NAME",
      get: function get() {
        return Zn;
      }
    }, {
      key: "DATA_KEY",
      get: function get() {
        return Gn;
      }
    }, {
      key: "Event",
      get: function get() {
        return ci;
      }
    }, {
      key: "EVENT_KEY",
      get: function get() {
        return Jn;
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return oi;
      }
    }]), i;
  }(Xn);

  p.fn[Zn] = ui._jQueryInterface, p.fn[Zn].Constructor = ui, p.fn[Zn].noConflict = function () {
    return p.fn[Zn] = ti, ui._jQueryInterface;
  };

  var hi = "scrollspy",
      di = "bs.scrollspy",
      fi = "." + di,
      pi = p.fn[hi],
      gi = {
    offset: 10,
    method: "auto",
    target: ""
  },
      mi = {
    offset: "number",
    method: "string",
    target: "(string|element)"
  },
      vi = {
    ACTIVATE: "activate" + fi,
    SCROLL: "scroll" + fi,
    LOAD_DATA_API: "load" + fi + ".data-api"
  },
      yi = "dropdown-item",
      _i = "active",
      bi = '[data-spy="scroll"]',
      wi = ".nav, .list-group",
      Ci = ".nav-link",
      xi = ".nav-item",
      Ei = ".list-group-item",
      Ti = ".dropdown",
      Si = ".dropdown-item",
      Ai = ".dropdown-toggle",
      ki = "offset",
      Di = "position",
      Ii = function () {
    function n(t, e) {
      var n = this;
      this._element = t, this._scrollElement = "BODY" === t.tagName ? window : t, this._config = this._getConfig(e), this._selector = this._config.target + " " + Ci + "," + this._config.target + " " + Ei + "," + this._config.target + " " + Si, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, p(this._scrollElement).on(vi.SCROLL, function (t) {
        return n._process(t);
      }), this.refresh(), this._process();
    }

    var t = n.prototype;
    return t.refresh = function () {
      var e = this,
          t = this._scrollElement === this._scrollElement.window ? ki : Di,
          o = "auto" === this._config.method ? t : this._config.method,
          r = o === Di ? this._getScrollTop() : 0;
      this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), [].slice.call(document.querySelectorAll(this._selector)).map(function (t) {
        var e,
            n = g.getSelectorFromElement(t);

        if (n && (e = document.querySelector(n)), e) {
          var i = e.getBoundingClientRect();
          if (i.width || i.height) return [p(e)[o]().top + r, n];
        }

        return null;
      }).filter(function (t) {
        return t;
      }).sort(function (t, e) {
        return t[0] - e[0];
      }).forEach(function (t) {
        e._offsets.push(t[0]), e._targets.push(t[1]);
      });
    }, t.dispose = function () {
      p.removeData(this._element, di), p(this._scrollElement).off(fi), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null;
    }, t._getConfig = function (t) {
      var e;
      return "string" != typeof (t = l({}, gi, {}, "object" == _typeof2(t) && t ? t : {})).target && ((e = p(t.target).attr("id")) || (e = g.getUID(hi), p(t.target).attr("id", e)), t.target = "#" + e), g.typeCheckConfig(hi, t, mi), t;
    }, t._getScrollTop = function () {
      return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
    }, t._getScrollHeight = function () {
      return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
    }, t._getOffsetHeight = function () {
      return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
    }, t._process = function () {
      var t = this._getScrollTop() + this._config.offset,
          e = this._getScrollHeight(),
          n = this._config.offset + e - this._getOffsetHeight();

      if (this._scrollHeight !== e && this.refresh(), n <= t) {
        var i = this._targets[this._targets.length - 1];
        this._activeTarget !== i && this._activate(i);
      } else {
        if (this._activeTarget && t < this._offsets[0] && 0 < this._offsets[0]) return this._activeTarget = null, void this._clear();

        for (var o = this._offsets.length; o--;) {
          this._activeTarget !== this._targets[o] && t >= this._offsets[o] && (void 0 === this._offsets[o + 1] || t < this._offsets[o + 1]) && this._activate(this._targets[o]);
        }
      }
    }, t._activate = function (e) {
      this._activeTarget = e, this._clear();

      var t = this._selector.split(",").map(function (t) {
        return t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]';
      }),
          n = p([].slice.call(document.querySelectorAll(t.join(","))));

      n.hasClass(yi) ? (n.closest(Ti).find(Ai).addClass(_i), n.addClass(_i)) : (n.addClass(_i), n.parents(wi).prev(Ci + ", " + Ei).addClass(_i), n.parents(wi).prev(xi).children(Ci).addClass(_i)), p(this._scrollElement).trigger(vi.ACTIVATE, {
        relatedTarget: e
      });
    }, t._clear = function () {
      [].slice.call(document.querySelectorAll(this._selector)).filter(function (t) {
        return t.classList.contains(_i);
      }).forEach(function (t) {
        return t.classList.remove(_i);
      });
    }, n._jQueryInterface = function (e) {
      return this.each(function () {
        var t = p(this).data(di);

        if (t || (t = new n(this, "object" == _typeof2(e) && e), p(this).data(di, t)), "string" == typeof e) {
          if (void 0 === t[e]) throw new TypeError('No method named "' + e + '"');
          t[e]();
        }
      });
    }, s(n, null, [{
      key: "VERSION",
      get: function get() {
        return "4.4.1";
      }
    }, {
      key: "Default",
      get: function get() {
        return gi;
      }
    }]), n;
  }();

  p(window).on(vi.LOAD_DATA_API, function () {
    for (var t = [].slice.call(document.querySelectorAll(bi)), e = t.length; e--;) {
      var n = p(t[e]);

      Ii._jQueryInterface.call(n, n.data());
    }
  }), p.fn[hi] = Ii._jQueryInterface, p.fn[hi].Constructor = Ii, p.fn[hi].noConflict = function () {
    return p.fn[hi] = pi, Ii._jQueryInterface;
  };

  var Ni = "bs.tab",
      Oi = "." + Ni,
      Pi = p.fn.tab,
      Li = {
    HIDE: "hide" + Oi,
    HIDDEN: "hidden" + Oi,
    SHOW: "show" + Oi,
    SHOWN: "shown" + Oi,
    CLICK_DATA_API: "click" + Oi + ".data-api"
  },
      ji = "dropdown-menu",
      Mi = "active",
      Hi = "disabled",
      $i = "fade",
      qi = "show",
      zi = ".dropdown",
      Ri = ".nav, .list-group",
      Fi = ".active",
      Wi = "> li > .active",
      Bi = '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
      Ui = ".dropdown-toggle",
      Qi = "> .dropdown-menu .active",
      Ki = function () {
    function i(t) {
      this._element = t;
    }

    var t = i.prototype;
    return t.show = function () {
      var t,
          e,
          n,
          i,
          o,
          r,
          s,
          a,
          l = this;
      this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && p(this._element).hasClass(Mi) || p(this._element).hasClass(Hi) || (e = p(this._element).closest(Ri)[0], n = g.getSelectorFromElement(this._element), e && (i = "UL" === e.nodeName || "OL" === e.nodeName ? Wi : Fi, o = (o = p.makeArray(p(e).find(i)))[o.length - 1]), r = p.Event(Li.HIDE, {
        relatedTarget: this._element
      }), s = p.Event(Li.SHOW, {
        relatedTarget: o
      }), o && p(o).trigger(r), p(this._element).trigger(s), s.isDefaultPrevented() || r.isDefaultPrevented() || (n && (t = document.querySelector(n)), this._activate(this._element, e), a = function a() {
        var t = p.Event(Li.HIDDEN, {
          relatedTarget: l._element
        }),
            e = p.Event(Li.SHOWN, {
          relatedTarget: o
        });
        p(o).trigger(t), p(l._element).trigger(e);
      }, t ? this._activate(t, t.parentNode, a) : a()));
    }, t.dispose = function () {
      p.removeData(this._element, Ni), this._element = null;
    }, t._activate = function (t, e, n) {
      function i() {
        return r._transitionComplete(t, s, n);
      }

      var o,
          r = this,
          s = (!e || "UL" !== e.nodeName && "OL" !== e.nodeName ? p(e).children(Fi) : p(e).find(Wi))[0],
          a = n && s && p(s).hasClass($i);
      s && a ? (o = g.getTransitionDurationFromElement(s), p(s).removeClass(qi).one(g.TRANSITION_END, i).emulateTransitionEnd(o)) : i();
    }, t._transitionComplete = function (t, e, n) {
      var i, o, r;
      e && (p(e).removeClass(Mi), (i = p(e.parentNode).find(Qi)[0]) && p(i).removeClass(Mi), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !1)), p(t).addClass(Mi), "tab" === t.getAttribute("role") && t.setAttribute("aria-selected", !0), g.reflow(t), t.classList.contains($i) && t.classList.add(qi), t.parentNode && p(t.parentNode).hasClass(ji) && ((o = p(t).closest(zi)[0]) && (r = [].slice.call(o.querySelectorAll(Ui)), p(r).addClass(Mi)), t.setAttribute("aria-expanded", !0)), n && n();
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = p(this),
            e = t.data(Ni);

        if (e || (e = new i(this), t.data(Ni, e)), "string" == typeof n) {
          if (void 0 === e[n]) throw new TypeError('No method named "' + n + '"');
          e[n]();
        }
      });
    }, s(i, null, [{
      key: "VERSION",
      get: function get() {
        return "4.4.1";
      }
    }]), i;
  }();

  p(document).on(Li.CLICK_DATA_API, Bi, function (t) {
    t.preventDefault(), Ki._jQueryInterface.call(p(this), "show");
  }), p.fn.tab = Ki._jQueryInterface, p.fn.tab.Constructor = Ki, p.fn.tab.noConflict = function () {
    return p.fn.tab = Pi, Ki._jQueryInterface;
  };

  var Vi = "toast",
      Yi = "bs.toast",
      Xi = "." + Yi,
      Zi = p.fn[Vi],
      Gi = {
    CLICK_DISMISS: "click.dismiss" + Xi,
    HIDE: "hide" + Xi,
    HIDDEN: "hidden" + Xi,
    SHOW: "show" + Xi,
    SHOWN: "shown" + Xi
  },
      Ji = "fade",
      to = "hide",
      eo = "show",
      no = "showing",
      io = {
    animation: "boolean",
    autohide: "boolean",
    delay: "number"
  },
      oo = {
    animation: !0,
    autohide: !0,
    delay: 500
  },
      ro = '[data-dismiss="toast"]',
      so = function () {
    function i(t, e) {
      this._element = t, this._config = this._getConfig(e), this._timeout = null, this._setListeners();
    }

    var t = i.prototype;
    return t.show = function () {
      var t,
          e,
          n = this,
          i = p.Event(Gi.SHOW);
      p(this._element).trigger(i), i.isDefaultPrevented() || (this._config.animation && this._element.classList.add(Ji), t = function t() {
        n._element.classList.remove(no), n._element.classList.add(eo), p(n._element).trigger(Gi.SHOWN), n._config.autohide && (n._timeout = setTimeout(function () {
          n.hide();
        }, n._config.delay));
      }, this._element.classList.remove(to), g.reflow(this._element), this._element.classList.add(no), this._config.animation ? (e = g.getTransitionDurationFromElement(this._element), p(this._element).one(g.TRANSITION_END, t).emulateTransitionEnd(e)) : t());
    }, t.hide = function () {
      var t;
      this._element.classList.contains(eo) && (t = p.Event(Gi.HIDE), p(this._element).trigger(t), t.isDefaultPrevented() || this._close());
    }, t.dispose = function () {
      clearTimeout(this._timeout), this._timeout = null, this._element.classList.contains(eo) && this._element.classList.remove(eo), p(this._element).off(Gi.CLICK_DISMISS), p.removeData(this._element, Yi), this._element = null, this._config = null;
    }, t._getConfig = function (t) {
      return t = l({}, oo, {}, p(this._element).data(), {}, "object" == _typeof2(t) && t ? t : {}), g.typeCheckConfig(Vi, t, this.constructor.DefaultType), t;
    }, t._setListeners = function () {
      var t = this;
      p(this._element).on(Gi.CLICK_DISMISS, ro, function () {
        return t.hide();
      });
    }, t._close = function () {
      function t() {
        n._element.classList.add(to), p(n._element).trigger(Gi.HIDDEN);
      }

      var e,
          n = this;
      this._element.classList.remove(eo), this._config.animation ? (e = g.getTransitionDurationFromElement(this._element), p(this._element).one(g.TRANSITION_END, t).emulateTransitionEnd(e)) : t();
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = p(this),
            e = t.data(Yi);

        if (e || (e = new i(this, "object" == _typeof2(n) && n), t.data(Yi, e)), "string" == typeof n) {
          if (void 0 === e[n]) throw new TypeError('No method named "' + n + '"');
          e[n](this);
        }
      });
    }, s(i, null, [{
      key: "VERSION",
      get: function get() {
        return "4.4.1";
      }
    }, {
      key: "DefaultType",
      get: function get() {
        return io;
      }
    }, {
      key: "Default",
      get: function get() {
        return oo;
      }
    }]), i;
  }();

  p.fn[Vi] = so._jQueryInterface, p.fn[Vi].Constructor = so, p.fn[Vi].noConflict = function () {
    return p.fn[Vi] = Zi, so._jQueryInterface;
  }, t.Alert = v, t.Button = L, t.Carousel = ut, t.Collapse = St, t.Dropdown = en, t.Modal = xn, t.Popover = ui, t.Scrollspy = Ii, t.Tab = Ki, t.Toast = so, t.Tooltip = Xn, t.Util = g, Object.defineProperty(t, "__esModule", {
    value: !0
  });
}), function (t) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_0__], __WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(function (r, i) {
  var n = "plugin_hideShowPassword",
      o = ["show", "innerToggle"],
      e = {
    show: "infer",
    innerToggle: !1,
    enable: function () {
      var t = document.body,
          e = document.createElement("input"),
          n = !0,
          e = (t = t || document.createElement("body")).appendChild(e);

      try {
        e.setAttribute("type", "text");
      } catch (t) {
        n = !1;
      }

      return t.removeChild(e), n;
    }(),
    triggerOnToggle: !1,
    className: "hideShowPassword-field",
    initEvent: "hideShowPasswordInit",
    changeEvent: "passwordVisibilityChange",
    props: {
      autocapitalize: "off",
      autocomplete: "off",
      autocorrect: "off",
      spellcheck: "false"
    },
    toggle: {
      element: '<button type="button">',
      className: "hideShowPassword-toggle",
      touchSupport: "undefined" != typeof Modernizr && Modernizr.touchevents,
      attachToEvent: "click.hideShowPassword",
      attachToTouchEvent: "touchstart.hideShowPassword mousedown.hideShowPassword",
      attachToKeyEvent: "keyup",
      attachToKeyCodes: !0,
      styles: {
        position: "absolute"
      },
      touchStyles: {
        pointerEvents: "none"
      },
      position: "infer",
      verticalAlign: "middle",
      offset: 0,
      attr: {
        role: "button",
        "aria-label": "Show Password",
        title: "Show Password",
        tabIndex: 0
      }
    },
    wrapper: {
      element: "<div>",
      className: "hideShowPassword-wrapper",
      enforceWidth: !1,
      styles: {
        position: "relative"
      },
      inheritStyles: ["display", "verticalAlign", "marginTop", "marginRight", "marginBottom", "marginLeft"],
      innerElementStyles: {
        marginTop: 0,
        marginRight: 0,
        marginBottom: 0,
        marginLeft: 0
      }
    },
    states: {
      shown: {
        className: "hideShowPassword-shown",
        changeEvent: "passwordShown",
        props: {
          type: "text"
        },
        toggle: {
          className: "hideShowPassword-toggle-hide",
          content: "Hide",
          attr: {
            "aria-pressed": "true",
            title: "Hide Password"
          }
        }
      },
      hidden: {
        className: "hideShowPassword-hidden",
        changeEvent: "passwordHidden",
        props: {
          type: "password"
        },
        toggle: {
          className: "hideShowPassword-toggle-show",
          content: "Show",
          attr: {
            "aria-pressed": "false",
            title: "Show Password"
          }
        }
      }
    }
  };

  function s(t, e) {
    this.element = r(t), this.wrapperElement = r(), this.toggleElement = r(), this.init(e);
  }

  s.prototype = {
    init: function init(t) {
      this.update(t, e) && (this.element.addClass(this.options.className), this.options.innerToggle && (this.wrapElement(this.options.wrapper), this.initToggle(this.options.toggle), "string" == typeof this.options.innerToggle && (this.toggleElement.hide(), this.element.one(this.options.innerToggle, r.proxy(function () {
        this.toggleElement.show();
      }, this)))), this.element.trigger(this.options.initEvent, [this]));
    },
    update: function update(t, e) {
      return this.options = this.prepareOptions(t, e), this.updateElement() && this.element.trigger(this.options.changeEvent, [this]).trigger(this.state().changeEvent, [this]), this.options.enable;
    },
    toggle: function toggle(t) {
      return t = t || "toggle", this.update({
        show: t
      });
    },
    prepareOptions: function prepareOptions(t, e) {
      var n,
          i = t || {},
          o = [];

      if (e = e || this.options, t = r.extend(!0, {}, e, t), i.hasOwnProperty("wrapper") && i.wrapper.hasOwnProperty("inheritStyles") && (t.wrapper.inheritStyles = i.wrapper.inheritStyles), t.enable && ("toggle" === t.show ? t.show = this.isType("hidden", t.states) : "infer" === t.show && (t.show = this.isType("shown", t.states)), "infer" === t.toggle.position && (t.toggle.position = "rtl" === this.element.css("text-direction") ? "left" : "right"), !r.isArray(t.toggle.attachToKeyCodes))) {
        if (!0 === t.toggle.attachToKeyCodes) switch ((n = r(t.toggle.element)).prop("tagName").toLowerCase()) {
          case "button":
          case "input":
            break;

          case "a":
            if (n.filter("[href]").length) {
              o.push(32);
              break;
            }

          default:
            o.push(32, 13);
        }
        t.toggle.attachToKeyCodes = o;
      }

      return t;
    },
    updateElement: function updateElement() {
      return !(!this.options.enable || this.isType()) && (this.element.prop(r.extend({}, this.options.props, this.state().props)).addClass(this.state().className).removeClass(this.otherState().className), this.options.triggerOnToggle && this.element.trigger(this.options.triggerOnToggle, [this]), this.updateToggle(), !0);
    },
    isType: function isType(t, e) {
      return (e = e || this.options.states)[t = t || this.state(i, i, e).props.type] && (t = e[t].props.type), this.element.prop("type") === t;
    },
    state: function state(t, e, n) {
      return n = n || this.options.states, t === i && (t = this.options.show), "boolean" == typeof t && (t = t ? "shown" : "hidden"), e && (t = "shown" === t ? "hidden" : "shown"), n[t];
    },
    otherState: function otherState(t) {
      return this.state(t, !0);
    },
    wrapElement: function wrapElement(n) {
      var t,
          e = n.enforceWidth;
      return this.wrapperElement.length || (t = this.element.outerWidth(), r.each(n.inheritStyles, r.proxy(function (t, e) {
        n.styles[e] = this.element.css(e);
      }, this)), this.element.css(n.innerElementStyles).wrap(r(n.element).addClass(n.className).css(n.styles)), this.wrapperElement = this.element.parent(), !0 === e && (e = this.wrapperElement.outerWidth() !== t && t), !1 !== e && this.wrapperElement.css("width", e)), this.wrapperElement;
    },
    initToggle: function initToggle(t) {
      return this.toggleElement.length || (this.toggleElement = r(t.element).attr(t.attr).addClass(t.className).css(t.styles).appendTo(this.wrapperElement), this.updateToggle(), this.positionToggle(t.position, t.verticalAlign, t.offset), t.touchSupport ? (this.toggleElement.css(t.touchStyles), this.element.on(t.attachToTouchEvent, r.proxy(this.toggleTouchEvent, this))) : this.toggleElement.on(t.attachToEvent, r.proxy(this.toggleEvent, this)), t.attachToKeyCodes.length && this.toggleElement.on(t.attachToKeyEvent, r.proxy(this.toggleKeyEvent, this))), this.toggleElement;
    },
    positionToggle: function positionToggle(t, e, n) {
      var i = {};

      switch (i[t] = n, e) {
        case "top":
        case "bottom":
          i[e] = n;
          break;

        case "middle":
          i.top = "50%", i.marginTop = this.toggleElement.outerHeight() / -2;
      }

      return this.toggleElement.css(i);
    },
    updateToggle: function updateToggle(t, e) {
      var n, i;
      return this.toggleElement.length && (n = "padding-" + this.options.toggle.position, t = t || this.state().toggle, e = e || this.otherState().toggle, this.toggleElement.attr(t.attr).addClass(t.className).removeClass(e.className).html(t.content), i = this.toggleElement.outerWidth() + 2 * this.options.toggle.offset, this.element.css(n) !== i && this.element.css(n, i)), this.toggleElement;
    },
    toggleEvent: function toggleEvent(t) {
      t.preventDefault(), this.toggle();
    },
    toggleKeyEvent: function toggleKeyEvent(n) {
      r.each(this.options.toggle.attachToKeyCodes, r.proxy(function (t, e) {
        if (n.which === e) return this.toggleEvent(n), !1;
      }, this));
    },
    toggleTouchEvent: function toggleTouchEvent(t) {
      var e,
          n,
          i,
          o = this.toggleElement.offset().left;
      o && (e = t.pageX || t.originalEvent.pageX, i = "left" === this.options.toggle.position ? (n = e, o += this.toggleElement.outerWidth()) : (n = o, e), n <= i && this.toggleEvent(t));
    }
  }, r.fn.hideShowPassword = function () {
    var i = {};
    return r.each(arguments, function (t, e) {
      var n = {};
      if ("object" == _typeof2(e)) n = e;else {
        if (!o[t]) return !1;
        n[o[t]] = e;
      }
      r.extend(!0, i, n);
    }), this.each(function () {
      var t = r(this),
          e = t.data(n);
      e ? e.update(i) : t.data(n, new s(this, i));
    });
  }, r.each({
    show: !0,
    hide: !1,
    toggle: "toggle"
  }, function (t, n) {
    r.fn[t + "Password"] = function (t, e) {
      return this.hideShowPassword(n, t, e);
    };
  });
}), function (r) {
  r.fn.niceSelect = function (t) {
    if ("string" == typeof t) return "update" == t ? this.each(function () {
      var t = r(this),
          e = r(this).next(".nice-select"),
          n = e.hasClass("open");
      e.length && (e.remove(), i(t), n && t.next().trigger("click"));
    }) : "destroy" == t ? (this.each(function () {
      var t = r(this),
          e = r(this).next(".nice-select");
      e.length && (e.remove(), t.css("display", ""));
    }), 0 == r(".nice-select").length && r(document).off(".nice_select")) : console.log('Method "' + t + '" does not exist.'), this;

    function i(t) {
      t.after(r("<div></div>").addClass("nice-select").addClass(t.attr("class") || "").addClass(t.attr("disabled") ? "disabled" : "").attr("tabindex", t.attr("disabled") ? null : "0").html('<span class="current"></span><ul class="list"></ul>'));
      var i = t.next(),
          e = t.find("option"),
          n = t.find("option:selected");
      i.find(".current").html(n.data("display") || n.text()), e.each(function (t) {
        var e = r(this),
            n = e.data("display");
        i.find("ul").append(r("<li></li>").attr("data-value", e.val()).attr("data-display", n || null).addClass("option" + (e.is(":selected") ? " selected" : "") + (e.is(":disabled") ? " disabled" : "")).html(e.text()));
      });
    }

    this.hide(), this.each(function () {
      var t = r(this);
      t.next().hasClass("nice-select") || i(t);
    }), r(document).off(".nice_select"), r(document).on("click.nice_select", ".nice-select", function (t) {
      var e = r(this);
      r(".nice-select").not(e).removeClass("open"), e.toggleClass("open"), e.hasClass("open") ? (e.find(".option"), e.find(".focus").removeClass("focus"), e.find(".selected").addClass("focus")) : e.focus();
    }), r(document).on("click.nice_select", function (t) {
      0 === r(t.target).closest(".nice-select").length && r(".nice-select").removeClass("open").find(".option");
    }), r(document).on("click.nice_select", ".nice-select .option:not(.disabled)", function (t) {
      var e = r(this),
          n = e.closest(".nice-select");
      n.find(".selected").removeClass("selected"), e.addClass("selected");
      var i = e.data("display") || e.text();
      n.find(".current").text(i), n.prev("select").val(e.data("value")).trigger("change");
    }), r(document).on("keydown.nice_select", ".nice-select", function (t) {
      var e,
          n,
          i = r(this),
          o = r(i.find(".focus") || i.find(".list .option.selected"));
      if (32 == t.keyCode || 13 == t.keyCode) return i.hasClass("open") ? o.trigger("click") : i.trigger("click"), !1;
      if (40 == t.keyCode) return i.hasClass("open") ? 0 < (e = o.nextAll(".option:not(.disabled)").first()).length && (i.find(".focus").removeClass("focus"), e.addClass("focus")) : i.trigger("click"), !1;
      if (38 == t.keyCode) return i.hasClass("open") ? 0 < (n = o.prevAll(".option:not(.disabled)").first()).length && (i.find(".focus").removeClass("focus"), n.addClass("focus")) : i.trigger("click"), !1;
      if (27 == t.keyCode) i.hasClass("open") && i.trigger("click");else if (9 == t.keyCode && i.hasClass("open")) return !1;
    });
    var e = document.createElement("a").style;
    return e.cssText = "pointer-events:auto", "auto" !== e.pointerEvents && r("html").addClass("no-csspointerevents"), this;
  };
}(jQuery);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack-stream/node_modules/webpack/buildin/module.js */ "./node_modules/webpack-stream/node_modules/webpack/buildin/module.js")(module), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! ./../../../node_modules/webpack-stream/node_modules/webpack/buildin/global.js */ "./node_modules/webpack-stream/node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./js/libs/oldFiles/isotope.js":
/*!*************************************!*\
  !*** ./js/libs/oldFiles/isotope.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __WEBPACK_LOCAL_MODULE_1__, __WEBPACK_LOCAL_MODULE_1__factory, __WEBPACK_LOCAL_MODULE_1__module;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_2__;var __WEBPACK_LOCAL_MODULE_3__, __WEBPACK_LOCAL_MODULE_3__factory, __WEBPACK_LOCAL_MODULE_3__module;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_4__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_LOCAL_MODULE_5__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_LOCAL_MODULE_6__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_LOCAL_MODULE_7__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_LOCAL_MODULE_8__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_LOCAL_MODULE_9__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_LOCAL_MODULE_10__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_LOCAL_MODULE_11__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_LOCAL_MODULE_12__;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*!
 * Isotope PACKAGED v3.0.3
 *
 * Licensed GPLv3 for open source use
 * or Isotope Commercial License for commercial use
 *
 * http://isotope.metafizzy.co
 * Copyright 2017 Metafizzy
 */
!function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (i) {
    return e(t, i);
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(window, function (t, e) {
  "use strict";

  function i(i, s, a) {
    function u(t, e, n) {
      var o,
          s = "$()." + i + '("' + e + '")';
      return t.each(function (t, u) {
        var h = a.data(u, i);
        if (!h) return void r(i + " not initialized. Cannot call methods, i.e. " + s);
        var d = h[e];
        if (!d || "_" == e.charAt(0)) return void r(s + " is not a valid method");
        var l = d.apply(h, n);
        o = void 0 === o ? l : o;
      }), void 0 !== o ? o : t;
    }

    function h(t, e) {
      t.each(function (t, n) {
        var o = a.data(n, i);
        o ? (o.option(e), o._init()) : (o = new s(n, e), a.data(n, i, o));
      });
    }

    a = a || e || t.jQuery, a && (s.prototype.option || (s.prototype.option = function (t) {
      a.isPlainObject(t) && (this.options = a.extend(!0, this.options, t));
    }), a.fn[i] = function (t) {
      if ("string" == typeof t) {
        var e = o.call(arguments, 1);
        return u(this, t, e);
      }

      return h(this, t), this;
    }, n(a));
  }

  function n(t) {
    !t || t && t.bridget || (t.bridget = i);
  }

  var o = Array.prototype.slice,
      s = t.console,
      r = "undefined" == typeof s ? function () {} : function (t) {
    s.error(t);
  };
  return n(e || t.jQuery), i;
}), function (t, e) {
   true ? !(__WEBPACK_LOCAL_MODULE_1__factory = (e), (__WEBPACK_LOCAL_MODULE_1__module = { id: "ev-emitter/ev-emitter", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_1__ = (typeof __WEBPACK_LOCAL_MODULE_1__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_1__factory.call(__WEBPACK_LOCAL_MODULE_1__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_1__module.exports, __WEBPACK_LOCAL_MODULE_1__module)) : __WEBPACK_LOCAL_MODULE_1__factory), (__WEBPACK_LOCAL_MODULE_1__module.loaded = true), __WEBPACK_LOCAL_MODULE_1__ === undefined && (__WEBPACK_LOCAL_MODULE_1__ = __WEBPACK_LOCAL_MODULE_1__module.exports)) : undefined;
}("undefined" != typeof window ? window : this, function () {
  function t() {}

  var e = t.prototype;
  return e.on = function (t, e) {
    if (t && e) {
      var i = this._events = this._events || {},
          n = i[t] = i[t] || [];
      return n.indexOf(e) == -1 && n.push(e), this;
    }
  }, e.once = function (t, e) {
    if (t && e) {
      this.on(t, e);
      var i = this._onceEvents = this._onceEvents || {},
          n = i[t] = i[t] || {};
      return n[e] = !0, this;
    }
  }, e.off = function (t, e) {
    var i = this._events && this._events[t];

    if (i && i.length) {
      var n = i.indexOf(e);
      return n != -1 && i.splice(n, 1), this;
    }
  }, e.emitEvent = function (t, e) {
    var i = this._events && this._events[t];

    if (i && i.length) {
      var n = 0,
          o = i[n];
      e = e || [];

      for (var s = this._onceEvents && this._onceEvents[t]; o;) {
        var r = s && s[o];
        r && (this.off(t, o), delete s[o]), o.apply(this, e), n += r ? 0 : 1, o = i[n];
      }

      return this;
    }
  }, t;
}), function (t, e) {
  "use strict";

   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_LOCAL_MODULE_2__ = ((function () {
    return e();
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__))) : undefined;
}(window, function () {
  "use strict";

  function t(t) {
    var e = parseFloat(t),
        i = t.indexOf("%") == -1 && !isNaN(e);
    return i && e;
  }

  function e() {}

  function i() {
    for (var t = {
      width: 0,
      height: 0,
      innerWidth: 0,
      innerHeight: 0,
      outerWidth: 0,
      outerHeight: 0
    }, e = 0; e < h; e++) {
      var i = u[e];
      t[i] = 0;
    }

    return t;
  }

  function n(t) {
    var e = getComputedStyle(t);
    return e || a("Style returned " + e + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), e;
  }

  function o() {
    if (!d) {
      d = !0;
      var e = document.createElement("div");
      e.style.width = "200px", e.style.padding = "1px 2px 3px 4px", e.style.borderStyle = "solid", e.style.borderWidth = "1px 2px 3px 4px", e.style.boxSizing = "border-box";
      var i = document.body || document.documentElement;
      i.appendChild(e);
      var o = n(e);
      s.isBoxSizeOuter = r = 200 == t(o.width), i.removeChild(e);
    }
  }

  function s(e) {
    if (o(), "string" == typeof e && (e = document.querySelector(e)), e && "object" == _typeof(e) && e.nodeType) {
      var s = n(e);
      if ("none" == s.display) return i();
      var a = {};
      a.width = e.offsetWidth, a.height = e.offsetHeight;

      for (var d = a.isBorderBox = "border-box" == s.boxSizing, l = 0; l < h; l++) {
        var f = u[l],
            m = s[f],
            c = parseFloat(m);
        a[f] = isNaN(c) ? 0 : c;
      }

      var p = a.paddingLeft + a.paddingRight,
          y = a.paddingTop + a.paddingBottom,
          g = a.marginLeft + a.marginRight,
          v = a.marginTop + a.marginBottom,
          _ = a.borderLeftWidth + a.borderRightWidth,
          I = a.borderTopWidth + a.borderBottomWidth,
          z = d && r,
          S = t(s.width);

      S !== !1 && (a.width = S + (z ? 0 : p + _));
      var x = t(s.height);
      return x !== !1 && (a.height = x + (z ? 0 : y + I)), a.innerWidth = a.width - (p + _), a.innerHeight = a.height - (y + I), a.outerWidth = a.width + g, a.outerHeight = a.height + v, a;
    }
  }

  var r,
      a = "undefined" == typeof console ? e : function (t) {
    console.error(t);
  },
      u = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
      h = u.length,
      d = !1;
  return s;
}), function (t, e) {
  "use strict";

   true ? !(__WEBPACK_LOCAL_MODULE_3__factory = (e), (__WEBPACK_LOCAL_MODULE_3__module = { id: "desandro-matches-selector/matches-selector", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_3__ = (typeof __WEBPACK_LOCAL_MODULE_3__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_3__factory.call(__WEBPACK_LOCAL_MODULE_3__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_3__module.exports, __WEBPACK_LOCAL_MODULE_3__module)) : __WEBPACK_LOCAL_MODULE_3__factory), (__WEBPACK_LOCAL_MODULE_3__module.loaded = true), __WEBPACK_LOCAL_MODULE_3__ === undefined && (__WEBPACK_LOCAL_MODULE_3__ = __WEBPACK_LOCAL_MODULE_3__module.exports)) : undefined;
}(window, function () {
  "use strict";

  var t = function () {
    var t = window.Element.prototype;
    if (t.matches) return "matches";
    if (t.matchesSelector) return "matchesSelector";

    for (var e = ["webkit", "moz", "ms", "o"], i = 0; i < e.length; i++) {
      var n = e[i],
          o = n + "MatchesSelector";
      if (t[o]) return o;
    }
  }();

  return function (e, i) {
    return e[t](i);
  };
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_3__], __WEBPACK_LOCAL_MODULE_4__ = ((function (i) {
    return e(t, i);
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__))) : undefined;
}(window, function (t, e) {
  var i = {};
  i.extend = function (t, e) {
    for (var i in e) {
      t[i] = e[i];
    }

    return t;
  }, i.modulo = function (t, e) {
    return (t % e + e) % e;
  }, i.makeArray = function (t) {
    var e = [];
    if (Array.isArray(t)) e = t;else if (t && "object" == _typeof(t) && "number" == typeof t.length) for (var i = 0; i < t.length; i++) {
      e.push(t[i]);
    } else e.push(t);
    return e;
  }, i.removeFrom = function (t, e) {
    var i = t.indexOf(e);
    i != -1 && t.splice(i, 1);
  }, i.getParent = function (t, i) {
    for (; t != document.body;) {
      if (t = t.parentNode, e(t, i)) return t;
    }
  }, i.getQueryElement = function (t) {
    return "string" == typeof t ? document.querySelector(t) : t;
  }, i.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t);
  }, i.filterFindElements = function (t, n) {
    t = i.makeArray(t);
    var o = [];
    return t.forEach(function (t) {
      if (t instanceof HTMLElement) {
        if (!n) return void o.push(t);
        e(t, n) && o.push(t);

        for (var i = t.querySelectorAll(n), s = 0; s < i.length; s++) {
          o.push(i[s]);
        }
      }
    }), o;
  }, i.debounceMethod = function (t, e, i) {
    var n = t.prototype[e],
        o = e + "Timeout";

    t.prototype[e] = function () {
      var t = this[o];
      t && clearTimeout(t);
      var e = arguments,
          s = this;
      this[o] = setTimeout(function () {
        n.apply(s, e), delete s[o];
      }, i || 100);
    };
  }, i.docReady = function (t) {
    var e = document.readyState;
    "complete" == e || "interactive" == e ? setTimeout(t) : document.addEventListener("DOMContentLoaded", t);
  }, i.toDashed = function (t) {
    return t.replace(/(.)([A-Z])/g, function (t, e, i) {
      return e + "-" + i;
    }).toLowerCase();
  };
  var n = t.console;
  return i.htmlInit = function (e, o) {
    i.docReady(function () {
      var s = i.toDashed(o),
          r = "data-" + s,
          a = document.querySelectorAll("[" + r + "]"),
          u = document.querySelectorAll(".js-" + s),
          h = i.makeArray(a).concat(i.makeArray(u)),
          d = r + "-options",
          l = t.jQuery;
      h.forEach(function (t) {
        var i,
            s = t.getAttribute(r) || t.getAttribute(d);

        try {
          i = s && JSON.parse(s);
        } catch (a) {
          return void (n && n.error("Error parsing " + r + " on " + t.className + ": " + a));
        }

        var u = new e(t, i);
        l && l.data(t, o, u);
      });
    });
  }, i;
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_1__, __WEBPACK_LOCAL_MODULE_2__], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_LOCAL_MODULE_5__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__)) : undefined;
}(window, function (t, e) {
  "use strict";

  function i(t) {
    for (var e in t) {
      return !1;
    }

    return e = null, !0;
  }

  function n(t, e) {
    t && (this.element = t, this.layout = e, this.position = {
      x: 0,
      y: 0
    }, this._create());
  }

  function o(t) {
    return t.replace(/([A-Z])/g, function (t) {
      return "-" + t.toLowerCase();
    });
  }

  var s = document.documentElement.style,
      r = "string" == typeof s.transition ? "transition" : "WebkitTransition",
      a = "string" == typeof s.transform ? "transform" : "WebkitTransform",
      u = {
    WebkitTransition: "webkitTransitionEnd",
    transition: "transitionend"
  }[r],
      h = {
    transform: a,
    transition: r,
    transitionDuration: r + "Duration",
    transitionProperty: r + "Property",
    transitionDelay: r + "Delay"
  },
      d = n.prototype = Object.create(t.prototype);
  d.constructor = n, d._create = function () {
    this._transn = {
      ingProperties: {},
      clean: {},
      onEnd: {}
    }, this.css({
      position: "absolute"
    });
  }, d.handleEvent = function (t) {
    var e = "on" + t.type;
    this[e] && this[e](t);
  }, d.getSize = function () {
    this.size = e(this.element);
  }, d.css = function (t) {
    var e = this.element.style;

    for (var i in t) {
      var n = h[i] || i;
      e[n] = t[i];
    }
  }, d.getPosition = function () {
    var t = getComputedStyle(this.element),
        e = this.layout._getOption("originLeft"),
        i = this.layout._getOption("originTop"),
        n = t[e ? "left" : "right"],
        o = t[i ? "top" : "bottom"],
        s = this.layout.size,
        r = n.indexOf("%") != -1 ? parseFloat(n) / 100 * s.width : parseInt(n, 10),
        a = o.indexOf("%") != -1 ? parseFloat(o) / 100 * s.height : parseInt(o, 10);

    r = isNaN(r) ? 0 : r, a = isNaN(a) ? 0 : a, r -= e ? s.paddingLeft : s.paddingRight, a -= i ? s.paddingTop : s.paddingBottom, this.position.x = r, this.position.y = a;
  }, d.layoutPosition = function () {
    var t = this.layout.size,
        e = {},
        i = this.layout._getOption("originLeft"),
        n = this.layout._getOption("originTop"),
        o = i ? "paddingLeft" : "paddingRight",
        s = i ? "left" : "right",
        r = i ? "right" : "left",
        a = this.position.x + t[o];

    e[s] = this.getXValue(a), e[r] = "";
    var u = n ? "paddingTop" : "paddingBottom",
        h = n ? "top" : "bottom",
        d = n ? "bottom" : "top",
        l = this.position.y + t[u];
    e[h] = this.getYValue(l), e[d] = "", this.css(e), this.emitEvent("layout", [this]);
  }, d.getXValue = function (t) {
    var e = this.layout._getOption("horizontal");

    return this.layout.options.percentPosition && !e ? t / this.layout.size.width * 100 + "%" : t + "px";
  }, d.getYValue = function (t) {
    var e = this.layout._getOption("horizontal");

    return this.layout.options.percentPosition && e ? t / this.layout.size.height * 100 + "%" : t + "px";
  }, d._transitionTo = function (t, e) {
    this.getPosition();
    var i = this.position.x,
        n = this.position.y,
        o = parseInt(t, 10),
        s = parseInt(e, 10),
        r = o === this.position.x && s === this.position.y;
    if (this.setPosition(t, e), r && !this.isTransitioning) return void this.layoutPosition();
    var a = t - i,
        u = e - n,
        h = {};
    h.transform = this.getTranslate(a, u), this.transition({
      to: h,
      onTransitionEnd: {
        transform: this.layoutPosition
      },
      isCleaning: !0
    });
  }, d.getTranslate = function (t, e) {
    var i = this.layout._getOption("originLeft"),
        n = this.layout._getOption("originTop");

    return t = i ? t : -t, e = n ? e : -e, "translate3d(" + t + "px, " + e + "px, 0)";
  }, d.goTo = function (t, e) {
    this.setPosition(t, e), this.layoutPosition();
  }, d.moveTo = d._transitionTo, d.setPosition = function (t, e) {
    this.position.x = parseInt(t, 10), this.position.y = parseInt(e, 10);
  }, d._nonTransition = function (t) {
    this.css(t.to), t.isCleaning && this._removeStyles(t.to);

    for (var e in t.onTransitionEnd) {
      t.onTransitionEnd[e].call(this);
    }
  }, d.transition = function (t) {
    if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(t);
    var e = this._transn;

    for (var i in t.onTransitionEnd) {
      e.onEnd[i] = t.onTransitionEnd[i];
    }

    for (i in t.to) {
      e.ingProperties[i] = !0, t.isCleaning && (e.clean[i] = !0);
    }

    if (t.from) {
      this.css(t.from);
      var n = this.element.offsetHeight;
      n = null;
    }

    this.enableTransition(t.to), this.css(t.to), this.isTransitioning = !0;
  };
  var l = "opacity," + o(a);
  d.enableTransition = function () {
    if (!this.isTransitioning) {
      var t = this.layout.options.transitionDuration;
      t = "number" == typeof t ? t + "ms" : t, this.css({
        transitionProperty: l,
        transitionDuration: t,
        transitionDelay: this.staggerDelay || 0
      }), this.element.addEventListener(u, this, !1);
    }
  }, d.onwebkitTransitionEnd = function (t) {
    this.ontransitionend(t);
  }, d.onotransitionend = function (t) {
    this.ontransitionend(t);
  };
  var f = {
    "-webkit-transform": "transform"
  };
  d.ontransitionend = function (t) {
    if (t.target === this.element) {
      var e = this._transn,
          n = f[t.propertyName] || t.propertyName;

      if (delete e.ingProperties[n], i(e.ingProperties) && this.disableTransition(), n in e.clean && (this.element.style[t.propertyName] = "", delete e.clean[n]), n in e.onEnd) {
        var o = e.onEnd[n];
        o.call(this), delete e.onEnd[n];
      }

      this.emitEvent("transitionEnd", [this]);
    }
  }, d.disableTransition = function () {
    this.removeTransitionStyles(), this.element.removeEventListener(u, this, !1), this.isTransitioning = !1;
  }, d._removeStyles = function (t) {
    var e = {};

    for (var i in t) {
      e[i] = "";
    }

    this.css(e);
  };
  var m = {
    transitionProperty: "",
    transitionDuration: "",
    transitionDelay: ""
  };
  return d.removeTransitionStyles = function () {
    this.css(m);
  }, d.stagger = function (t) {
    t = isNaN(t) ? 0 : t, this.staggerDelay = t + "ms";
  }, d.removeElem = function () {
    this.element.parentNode.removeChild(this.element), this.css({
      display: ""
    }), this.emitEvent("remove", [this]);
  }, d.remove = function () {
    return r && parseFloat(this.layout.options.transitionDuration) ? (this.once("transitionEnd", function () {
      this.removeElem();
    }), void this.hide()) : void this.removeElem();
  }, d.reveal = function () {
    delete this.isHidden, this.css({
      display: ""
    });
    var t = this.layout.options,
        e = {},
        i = this.getHideRevealTransitionEndProperty("visibleStyle");
    e[i] = this.onRevealTransitionEnd, this.transition({
      from: t.hiddenStyle,
      to: t.visibleStyle,
      isCleaning: !0,
      onTransitionEnd: e
    });
  }, d.onRevealTransitionEnd = function () {
    this.isHidden || this.emitEvent("reveal");
  }, d.getHideRevealTransitionEndProperty = function (t) {
    var e = this.layout.options[t];
    if (e.opacity) return "opacity";

    for (var i in e) {
      return i;
    }
  }, d.hide = function () {
    this.isHidden = !0, this.css({
      display: ""
    });
    var t = this.layout.options,
        e = {},
        i = this.getHideRevealTransitionEndProperty("hiddenStyle");
    e[i] = this.onHideTransitionEnd, this.transition({
      from: t.visibleStyle,
      to: t.hiddenStyle,
      isCleaning: !0,
      onTransitionEnd: e
    });
  }, d.onHideTransitionEnd = function () {
    this.isHidden && (this.css({
      display: "none"
    }), this.emitEvent("hide"));
  }, d.destroy = function () {
    this.css({
      position: "",
      left: "",
      right: "",
      top: "",
      bottom: "",
      transition: "",
      transform: ""
    });
  }, n;
}), function (t, e) {
  "use strict";

   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_1__, __WEBPACK_LOCAL_MODULE_2__, __WEBPACK_LOCAL_MODULE_4__, __WEBPACK_LOCAL_MODULE_5__], __WEBPACK_LOCAL_MODULE_6__ = ((function (i, n, o, s) {
    return e(t, i, n, o, s);
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__))) : undefined;
}(window, function (t, e, i, n, o) {
  "use strict";

  function s(t, e) {
    var i = n.getQueryElement(t);
    if (!i) return void (u && u.error("Bad element for " + this.constructor.namespace + ": " + (i || t)));
    this.element = i, h && (this.$element = h(this.element)), this.options = n.extend({}, this.constructor.defaults), this.option(e);
    var o = ++l;
    this.element.outlayerGUID = o, f[o] = this, this._create();

    var s = this._getOption("initLayout");

    s && this.layout();
  }

  function r(t) {
    function e() {
      t.apply(this, arguments);
    }

    return e.prototype = Object.create(t.prototype), e.prototype.constructor = e, e;
  }

  function a(t) {
    if ("number" == typeof t) return t;
    var e = t.match(/(^\d*\.?\d*)(\w*)/),
        i = e && e[1],
        n = e && e[2];
    if (!i.length) return 0;
    i = parseFloat(i);
    var o = c[n] || 1;
    return i * o;
  }

  var u = t.console,
      h = t.jQuery,
      d = function d() {},
      l = 0,
      f = {};

  s.namespace = "outlayer", s.Item = o, s.defaults = {
    containerStyle: {
      position: "relative"
    },
    initLayout: !0,
    originLeft: !0,
    originTop: !0,
    resize: !0,
    resizeContainer: !0,
    transitionDuration: "0.4s",
    hiddenStyle: {
      opacity: 0,
      transform: "scale(0.001)"
    },
    visibleStyle: {
      opacity: 1,
      transform: "scale(1)"
    }
  };
  var m = s.prototype;
  n.extend(m, e.prototype), m.option = function (t) {
    n.extend(this.options, t);
  }, m._getOption = function (t) {
    var e = this.constructor.compatOptions[t];
    return e && void 0 !== this.options[e] ? this.options[e] : this.options[t];
  }, s.compatOptions = {
    initLayout: "isInitLayout",
    horizontal: "isHorizontal",
    layoutInstant: "isLayoutInstant",
    originLeft: "isOriginLeft",
    originTop: "isOriginTop",
    resize: "isResizeBound",
    resizeContainer: "isResizingContainer"
  }, m._create = function () {
    this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), n.extend(this.element.style, this.options.containerStyle);

    var t = this._getOption("resize");

    t && this.bindResize();
  }, m.reloadItems = function () {
    this.items = this._itemize(this.element.children);
  }, m._itemize = function (t) {
    for (var e = this._filterFindItemElements(t), i = this.constructor.Item, n = [], o = 0; o < e.length; o++) {
      var s = e[o],
          r = new i(s, this);
      n.push(r);
    }

    return n;
  }, m._filterFindItemElements = function (t) {
    return n.filterFindElements(t, this.options.itemSelector);
  }, m.getItemElements = function () {
    return this.items.map(function (t) {
      return t.element;
    });
  }, m.layout = function () {
    this._resetLayout(), this._manageStamps();

    var t = this._getOption("layoutInstant"),
        e = void 0 !== t ? t : !this._isLayoutInited;

    this.layoutItems(this.items, e), this._isLayoutInited = !0;
  }, m._init = m.layout, m._resetLayout = function () {
    this.getSize();
  }, m.getSize = function () {
    this.size = i(this.element);
  }, m._getMeasurement = function (t, e) {
    var n,
        o = this.options[t];
    o ? ("string" == typeof o ? n = this.element.querySelector(o) : o instanceof HTMLElement && (n = o), this[t] = n ? i(n)[e] : o) : this[t] = 0;
  }, m.layoutItems = function (t, e) {
    t = this._getItemsForLayout(t), this._layoutItems(t, e), this._postLayout();
  }, m._getItemsForLayout = function (t) {
    return t.filter(function (t) {
      return !t.isIgnored;
    });
  }, m._layoutItems = function (t, e) {
    if (this._emitCompleteOnItems("layout", t), t && t.length) {
      var i = [];
      t.forEach(function (t) {
        var n = this._getItemLayoutPosition(t);

        n.item = t, n.isInstant = e || t.isLayoutInstant, i.push(n);
      }, this), this._processLayoutQueue(i);
    }
  }, m._getItemLayoutPosition = function () {
    return {
      x: 0,
      y: 0
    };
  }, m._processLayoutQueue = function (t) {
    this.updateStagger(), t.forEach(function (t, e) {
      this._positionItem(t.item, t.x, t.y, t.isInstant, e);
    }, this);
  }, m.updateStagger = function () {
    var t = this.options.stagger;
    return null === t || void 0 === t ? void (this.stagger = 0) : (this.stagger = a(t), this.stagger);
  }, m._positionItem = function (t, e, i, n, o) {
    n ? t.goTo(e, i) : (t.stagger(o * this.stagger), t.moveTo(e, i));
  }, m._postLayout = function () {
    this.resizeContainer();
  }, m.resizeContainer = function () {
    var t = this._getOption("resizeContainer");

    if (t) {
      var e = this._getContainerSize();

      e && (this._setContainerMeasure(e.width, !0), this._setContainerMeasure(e.height, !1));
    }
  }, m._getContainerSize = d, m._setContainerMeasure = function (t, e) {
    if (void 0 !== t) {
      var i = this.size;
      i.isBorderBox && (t += e ? i.paddingLeft + i.paddingRight + i.borderLeftWidth + i.borderRightWidth : i.paddingBottom + i.paddingTop + i.borderTopWidth + i.borderBottomWidth), t = Math.max(t, 0), this.element.style[e ? "width" : "height"] = t + "px";
    }
  }, m._emitCompleteOnItems = function (t, e) {
    function i() {
      o.dispatchEvent(t + "Complete", null, [e]);
    }

    function n() {
      r++, r == s && i();
    }

    var o = this,
        s = e.length;
    if (!e || !s) return void i();
    var r = 0;
    e.forEach(function (e) {
      e.once(t, n);
    });
  }, m.dispatchEvent = function (t, e, i) {
    var n = e ? [e].concat(i) : i;
    if (this.emitEvent(t, n), h) if (this.$element = this.$element || h(this.element), e) {
      var o = h.Event(e);
      o.type = t, this.$element.trigger(o, i);
    } else this.$element.trigger(t, i);
  }, m.ignore = function (t) {
    var e = this.getItem(t);
    e && (e.isIgnored = !0);
  }, m.unignore = function (t) {
    var e = this.getItem(t);
    e && delete e.isIgnored;
  }, m.stamp = function (t) {
    t = this._find(t), t && (this.stamps = this.stamps.concat(t), t.forEach(this.ignore, this));
  }, m.unstamp = function (t) {
    t = this._find(t), t && t.forEach(function (t) {
      n.removeFrom(this.stamps, t), this.unignore(t);
    }, this);
  }, m._find = function (t) {
    if (t) return "string" == typeof t && (t = this.element.querySelectorAll(t)), t = n.makeArray(t);
  }, m._manageStamps = function () {
    this.stamps && this.stamps.length && (this._getBoundingRect(), this.stamps.forEach(this._manageStamp, this));
  }, m._getBoundingRect = function () {
    var t = this.element.getBoundingClientRect(),
        e = this.size;
    this._boundingRect = {
      left: t.left + e.paddingLeft + e.borderLeftWidth,
      top: t.top + e.paddingTop + e.borderTopWidth,
      right: t.right - (e.paddingRight + e.borderRightWidth),
      bottom: t.bottom - (e.paddingBottom + e.borderBottomWidth)
    };
  }, m._manageStamp = d, m._getElementOffset = function (t) {
    var e = t.getBoundingClientRect(),
        n = this._boundingRect,
        o = i(t),
        s = {
      left: e.left - n.left - o.marginLeft,
      top: e.top - n.top - o.marginTop,
      right: n.right - e.right - o.marginRight,
      bottom: n.bottom - e.bottom - o.marginBottom
    };
    return s;
  }, m.handleEvent = n.handleEvent, m.bindResize = function () {
    t.addEventListener("resize", this), this.isResizeBound = !0;
  }, m.unbindResize = function () {
    t.removeEventListener("resize", this), this.isResizeBound = !1;
  }, m.onresize = function () {
    this.resize();
  }, n.debounceMethod(s, "onresize", 100), m.resize = function () {
    this.isResizeBound && this.needsResizeLayout() && this.layout();
  }, m.needsResizeLayout = function () {
    var t = i(this.element),
        e = this.size && t;
    return e && t.innerWidth !== this.size.innerWidth;
  }, m.addItems = function (t) {
    var e = this._itemize(t);

    return e.length && (this.items = this.items.concat(e)), e;
  }, m.appended = function (t) {
    var e = this.addItems(t);
    e.length && (this.layoutItems(e, !0), this.reveal(e));
  }, m.prepended = function (t) {
    var e = this._itemize(t);

    if (e.length) {
      var i = this.items.slice(0);
      this.items = e.concat(i), this._resetLayout(), this._manageStamps(), this.layoutItems(e, !0), this.reveal(e), this.layoutItems(i);
    }
  }, m.reveal = function (t) {
    if (this._emitCompleteOnItems("reveal", t), t && t.length) {
      var e = this.updateStagger();
      t.forEach(function (t, i) {
        t.stagger(i * e), t.reveal();
      });
    }
  }, m.hide = function (t) {
    if (this._emitCompleteOnItems("hide", t), t && t.length) {
      var e = this.updateStagger();
      t.forEach(function (t, i) {
        t.stagger(i * e), t.hide();
      });
    }
  }, m.revealItemElements = function (t) {
    var e = this.getItems(t);
    this.reveal(e);
  }, m.hideItemElements = function (t) {
    var e = this.getItems(t);
    this.hide(e);
  }, m.getItem = function (t) {
    for (var e = 0; e < this.items.length; e++) {
      var i = this.items[e];
      if (i.element == t) return i;
    }
  }, m.getItems = function (t) {
    t = n.makeArray(t);
    var e = [];
    return t.forEach(function (t) {
      var i = this.getItem(t);
      i && e.push(i);
    }, this), e;
  }, m.remove = function (t) {
    var e = this.getItems(t);
    this._emitCompleteOnItems("remove", e), e && e.length && e.forEach(function (t) {
      t.remove(), n.removeFrom(this.items, t);
    }, this);
  }, m.destroy = function () {
    var t = this.element.style;
    t.height = "", t.position = "", t.width = "", this.items.forEach(function (t) {
      t.destroy();
    }), this.unbindResize();
    var e = this.element.outlayerGUID;
    delete f[e], delete this.element.outlayerGUID, h && h.removeData(this.element, this.constructor.namespace);
  }, s.data = function (t) {
    t = n.getQueryElement(t);
    var e = t && t.outlayerGUID;
    return e && f[e];
  }, s.create = function (t, e) {
    var i = r(s);
    return i.defaults = n.extend({}, s.defaults), n.extend(i.defaults, e), i.compatOptions = n.extend({}, s.compatOptions), i.namespace = t, i.data = s.data, i.Item = r(o), n.htmlInit(i, t), h && h.bridget && h.bridget(t, i), i;
  };
  var c = {
    ms: 1,
    s: 1e3
  };
  return s.Item = o, s;
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_6__], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_LOCAL_MODULE_7__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__)) : undefined;
}(window, function (t) {
  "use strict";

  function e() {
    t.Item.apply(this, arguments);
  }

  var i = e.prototype = Object.create(t.Item.prototype),
      n = i._create;
  i._create = function () {
    this.id = this.layout.itemGUID++, n.call(this), this.sortData = {};
  }, i.updateSortData = function () {
    if (!this.isIgnored) {
      this.sortData.id = this.id, this.sortData["original-order"] = this.id, this.sortData.random = Math.random();
      var t = this.layout.options.getSortData,
          e = this.layout._sorters;

      for (var i in t) {
        var n = e[i];
        this.sortData[i] = n(this.element, this);
      }
    }
  };
  var o = i.destroy;
  return i.destroy = function () {
    o.apply(this, arguments), this.css({
      display: ""
    });
  }, e;
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_2__, __WEBPACK_LOCAL_MODULE_6__], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_LOCAL_MODULE_8__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__)) : undefined;
}(window, function (t, e) {
  "use strict";

  function i(t) {
    this.isotope = t, t && (this.options = t.options[this.namespace], this.element = t.element, this.items = t.filteredItems, this.size = t.size);
  }

  var n = i.prototype,
      o = ["_resetLayout", "_getItemLayoutPosition", "_manageStamp", "_getContainerSize", "_getElementOffset", "needsResizeLayout", "_getOption"];
  return o.forEach(function (t) {
    n[t] = function () {
      return e.prototype[t].apply(this.isotope, arguments);
    };
  }), n.needsVerticalResizeLayout = function () {
    var e = t(this.isotope.element),
        i = this.isotope.size && e;
    return i && e.innerHeight != this.isotope.size.innerHeight;
  }, n._getMeasurement = function () {
    this.isotope._getMeasurement.apply(this, arguments);
  }, n.getColumnWidth = function () {
    this.getSegmentSize("column", "Width");
  }, n.getRowHeight = function () {
    this.getSegmentSize("row", "Height");
  }, n.getSegmentSize = function (t, e) {
    var i = t + e,
        n = "outer" + e;

    if (this._getMeasurement(i, n), !this[i]) {
      var o = this.getFirstItemSize();
      this[i] = o && o[n] || this.isotope.size["inner" + e];
    }
  }, n.getFirstItemSize = function () {
    var e = this.isotope.filteredItems[0];
    return e && e.element && t(e.element);
  }, n.layout = function () {
    this.isotope.layout.apply(this.isotope, arguments);
  }, n.getSize = function () {
    this.isotope.getSize(), this.size = this.isotope.size;
  }, i.modes = {}, i.create = function (t, e) {
    function o() {
      i.apply(this, arguments);
    }

    return o.prototype = Object.create(n), o.prototype.constructor = o, e && (o.options = e), o.prototype.namespace = t, i.modes[t] = o, o;
  }, i;
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_6__, __WEBPACK_LOCAL_MODULE_2__], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_LOCAL_MODULE_9__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__)) : undefined;
}(window, function (t, e) {
  var i = t.create("masonry");
  return i.compatOptions.fitWidth = "isFitWidth", i.prototype._resetLayout = function () {
    this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns(), this.colYs = [];

    for (var t = 0; t < this.cols; t++) {
      this.colYs.push(0);
    }

    this.maxY = 0;
  }, i.prototype.measureColumns = function () {
    if (this.getContainerWidth(), !this.columnWidth) {
      var t = this.items[0],
          i = t && t.element;
      this.columnWidth = i && e(i).outerWidth || this.containerWidth;
    }

    var n = this.columnWidth += this.gutter,
        o = this.containerWidth + this.gutter,
        s = o / n,
        r = n - o % n,
        a = r && r < 1 ? "round" : "floor";
    s = Math[a](s), this.cols = Math.max(s, 1);
  }, i.prototype.getContainerWidth = function () {
    var t = this._getOption("fitWidth"),
        i = t ? this.element.parentNode : this.element,
        n = e(i);

    this.containerWidth = n && n.innerWidth;
  }, i.prototype._getItemLayoutPosition = function (t) {
    t.getSize();
    var e = t.size.outerWidth % this.columnWidth,
        i = e && e < 1 ? "round" : "ceil",
        n = Math[i](t.size.outerWidth / this.columnWidth);
    n = Math.min(n, this.cols);

    for (var o = this._getColGroup(n), s = Math.min.apply(Math, o), r = o.indexOf(s), a = {
      x: this.columnWidth * r,
      y: s
    }, u = s + t.size.outerHeight, h = this.cols + 1 - o.length, d = 0; d < h; d++) {
      this.colYs[r + d] = u;
    }

    return a;
  }, i.prototype._getColGroup = function (t) {
    if (t < 2) return this.colYs;

    for (var e = [], i = this.cols + 1 - t, n = 0; n < i; n++) {
      var o = this.colYs.slice(n, n + t);
      e[n] = Math.max.apply(Math, o);
    }

    return e;
  }, i.prototype._manageStamp = function (t) {
    var i = e(t),
        n = this._getElementOffset(t),
        o = this._getOption("originLeft"),
        s = o ? n.left : n.right,
        r = s + i.outerWidth,
        a = Math.floor(s / this.columnWidth);

    a = Math.max(0, a);
    var u = Math.floor(r / this.columnWidth);
    u -= r % this.columnWidth ? 0 : 1, u = Math.min(this.cols - 1, u);

    for (var h = this._getOption("originTop"), d = (h ? n.top : n.bottom) + i.outerHeight, l = a; l <= u; l++) {
      this.colYs[l] = Math.max(d, this.colYs[l]);
    }
  }, i.prototype._getContainerSize = function () {
    this.maxY = Math.max.apply(Math, this.colYs);
    var t = {
      height: this.maxY
    };
    return this._getOption("fitWidth") && (t.width = this._getContainerFitWidth()), t;
  }, i.prototype._getContainerFitWidth = function () {
    for (var t = 0, e = this.cols; --e && 0 === this.colYs[e];) {
      t++;
    }

    return (this.cols - t) * this.columnWidth - this.gutter;
  }, i.prototype.needsResizeLayout = function () {
    var t = this.containerWidth;
    return this.getContainerWidth(), t != this.containerWidth;
  }, i;
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_8__, __WEBPACK_LOCAL_MODULE_9__], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_LOCAL_MODULE_10__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__)) : undefined;
}(window, function (t, e) {
  "use strict";

  var i = t.create("masonry"),
      n = i.prototype,
      o = {
    _getElementOffset: !0,
    layout: !0,
    _getMeasurement: !0
  };

  for (var s in e.prototype) {
    o[s] || (n[s] = e.prototype[s]);
  }

  var r = n.measureColumns;

  n.measureColumns = function () {
    this.items = this.isotope.filteredItems, r.call(this);
  };

  var a = n._getOption;
  return n._getOption = function (t) {
    return "fitWidth" == t ? void 0 !== this.options.isFitWidth ? this.options.isFitWidth : this.options.fitWidth : a.apply(this.isotope, arguments);
  }, i;
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_8__], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_LOCAL_MODULE_11__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__)) : undefined;
}(window, function (t) {
  "use strict";

  var e = t.create("fitRows"),
      i = e.prototype;
  return i._resetLayout = function () {
    this.x = 0, this.y = 0, this.maxY = 0, this._getMeasurement("gutter", "outerWidth");
  }, i._getItemLayoutPosition = function (t) {
    t.getSize();
    var e = t.size.outerWidth + this.gutter,
        i = this.isotope.size.innerWidth + this.gutter;
    0 !== this.x && e + this.x > i && (this.x = 0, this.y = this.maxY);
    var n = {
      x: this.x,
      y: this.y
    };
    return this.maxY = Math.max(this.maxY, this.y + t.size.outerHeight), this.x += e, n;
  }, i._getContainerSize = function () {
    return {
      height: this.maxY
    };
  }, e;
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_8__], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_LOCAL_MODULE_12__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__)) : undefined;
}(window, function (t) {
  "use strict";

  var e = t.create("vertical", {
    horizontalAlignment: 0
  }),
      i = e.prototype;
  return i._resetLayout = function () {
    this.y = 0;
  }, i._getItemLayoutPosition = function (t) {
    t.getSize();
    var e = (this.isotope.size.innerWidth - t.size.outerWidth) * this.options.horizontalAlignment,
        i = this.y;
    return this.y += t.size.outerHeight, {
      x: e,
      y: i
    };
  }, i._getContainerSize = function () {
    return {
      height: this.y
    };
  }, e;
}), function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_6__, __WEBPACK_LOCAL_MODULE_2__, __WEBPACK_LOCAL_MODULE_3__, __WEBPACK_LOCAL_MODULE_4__, __WEBPACK_LOCAL_MODULE_7__, __WEBPACK_LOCAL_MODULE_8__, __WEBPACK_LOCAL_MODULE_10__, __WEBPACK_LOCAL_MODULE_11__, __WEBPACK_LOCAL_MODULE_12__], __WEBPACK_AMD_DEFINE_RESULT__ = (function (i, n, o, s, r, a) {
    return e(t, i, n, o, s, r, a);
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(window, function (t, e, i, n, o, s, r) {
  function a(t, e) {
    return function (i, n) {
      for (var o = 0; o < t.length; o++) {
        var s = t[o],
            r = i.sortData[s],
            a = n.sortData[s];

        if (r > a || r < a) {
          var u = void 0 !== e[s] ? e[s] : e,
              h = u ? 1 : -1;
          return (r > a ? 1 : -1) * h;
        }
      }

      return 0;
    };
  }

  var u = t.jQuery,
      h = String.prototype.trim ? function (t) {
    return t.trim();
  } : function (t) {
    return t.replace(/^\s+|\s+$/g, "");
  },
      d = e.create("isotope", {
    layoutMode: "masonry",
    isJQueryFiltering: !0,
    sortAscending: !0
  });
  d.Item = s, d.LayoutMode = r;
  var l = d.prototype;
  l._create = function () {
    this.itemGUID = 0, this._sorters = {}, this._getSorters(), e.prototype._create.call(this), this.modes = {}, this.filteredItems = this.items, this.sortHistory = ["original-order"];

    for (var t in r.modes) {
      this._initLayoutMode(t);
    }
  }, l.reloadItems = function () {
    this.itemGUID = 0, e.prototype.reloadItems.call(this);
  }, l._itemize = function () {
    for (var t = e.prototype._itemize.apply(this, arguments), i = 0; i < t.length; i++) {
      var n = t[i];
      n.id = this.itemGUID++;
    }

    return this._updateItemsSortData(t), t;
  }, l._initLayoutMode = function (t) {
    var e = r.modes[t],
        i = this.options[t] || {};
    this.options[t] = e.options ? o.extend(e.options, i) : i, this.modes[t] = new e(this);
  }, l.layout = function () {
    return !this._isLayoutInited && this._getOption("initLayout") ? void this.arrange() : void this._layout();
  }, l._layout = function () {
    var t = this._getIsInstant();

    this._resetLayout(), this._manageStamps(), this.layoutItems(this.filteredItems, t), this._isLayoutInited = !0;
  }, l.arrange = function (t) {
    this.option(t), this._getIsInstant();

    var e = this._filter(this.items);

    this.filteredItems = e.matches, this._bindArrangeComplete(), this._isInstant ? this._noTransition(this._hideReveal, [e]) : this._hideReveal(e), this._sort(), this._layout();
  }, l._init = l.arrange, l._hideReveal = function (t) {
    this.reveal(t.needReveal), this.hide(t.needHide);
  }, l._getIsInstant = function () {
    var t = this._getOption("layoutInstant"),
        e = void 0 !== t ? t : !this._isLayoutInited;

    return this._isInstant = e, e;
  }, l._bindArrangeComplete = function () {
    function t() {
      e && i && n && o.dispatchEvent("arrangeComplete", null, [o.filteredItems]);
    }

    var e,
        i,
        n,
        o = this;
    this.once("layoutComplete", function () {
      e = !0, t();
    }), this.once("hideComplete", function () {
      i = !0, t();
    }), this.once("revealComplete", function () {
      n = !0, t();
    });
  }, l._filter = function (t) {
    var e = this.options.filter;
    e = e || "*";

    for (var i = [], n = [], o = [], s = this._getFilterTest(e), r = 0; r < t.length; r++) {
      var a = t[r];

      if (!a.isIgnored) {
        var u = s(a);
        u && i.push(a), u && a.isHidden ? n.push(a) : u || a.isHidden || o.push(a);
      }
    }

    return {
      matches: i,
      needReveal: n,
      needHide: o
    };
  }, l._getFilterTest = function (t) {
    return u && this.options.isJQueryFiltering ? function (e) {
      return u(e.element).is(t);
    } : "function" == typeof t ? function (e) {
      return t(e.element);
    } : function (e) {
      return n(e.element, t);
    };
  }, l.updateSortData = function (t) {
    var e;
    t ? (t = o.makeArray(t), e = this.getItems(t)) : e = this.items, this._getSorters(), this._updateItemsSortData(e);
  }, l._getSorters = function () {
    var t = this.options.getSortData;

    for (var e in t) {
      var i = t[e];
      this._sorters[e] = f(i);
    }
  }, l._updateItemsSortData = function (t) {
    for (var e = t && t.length, i = 0; e && i < e; i++) {
      var n = t[i];
      n.updateSortData();
    }
  };

  var f = function () {
    function t(t) {
      if ("string" != typeof t) return t;
      var i = h(t).split(" "),
          n = i[0],
          o = n.match(/^\[(.+)\]$/),
          s = o && o[1],
          r = e(s, n),
          a = d.sortDataParsers[i[1]];
      return t = a ? function (t) {
        return t && a(r(t));
      } : function (t) {
        return t && r(t);
      };
    }

    function e(t, e) {
      return t ? function (e) {
        return e.getAttribute(t);
      } : function (t) {
        var i = t.querySelector(e);
        return i && i.textContent;
      };
    }

    return t;
  }();

  d.sortDataParsers = {
    parseInt: function (_parseInt) {
      function parseInt(_x) {
        return _parseInt.apply(this, arguments);
      }

      parseInt.toString = function () {
        return _parseInt.toString();
      };

      return parseInt;
    }(function (t) {
      return parseInt(t, 10);
    }),
    parseFloat: function (_parseFloat) {
      function parseFloat(_x2) {
        return _parseFloat.apply(this, arguments);
      }

      parseFloat.toString = function () {
        return _parseFloat.toString();
      };

      return parseFloat;
    }(function (t) {
      return parseFloat(t);
    })
  }, l._sort = function () {
    if (this.options.sortBy) {
      var t = o.makeArray(this.options.sortBy);
      this._getIsSameSortBy(t) || (this.sortHistory = t.concat(this.sortHistory));
      var e = a(this.sortHistory, this.options.sortAscending);
      this.filteredItems.sort(e);
    }
  }, l._getIsSameSortBy = function (t) {
    for (var e = 0; e < t.length; e++) {
      if (t[e] != this.sortHistory[e]) return !1;
    }

    return !0;
  }, l._mode = function () {
    var t = this.options.layoutMode,
        e = this.modes[t];
    if (!e) throw new Error("No layout mode: " + t);
    return e.options = this.options[t], e;
  }, l._resetLayout = function () {
    e.prototype._resetLayout.call(this), this._mode()._resetLayout();
  }, l._getItemLayoutPosition = function (t) {
    return this._mode()._getItemLayoutPosition(t);
  }, l._manageStamp = function (t) {
    this._mode()._manageStamp(t);
  }, l._getContainerSize = function () {
    return this._mode()._getContainerSize();
  }, l.needsResizeLayout = function () {
    return this._mode().needsResizeLayout();
  }, l.appended = function (t) {
    var e = this.addItems(t);

    if (e.length) {
      var i = this._filterRevealAdded(e);

      this.filteredItems = this.filteredItems.concat(i);
    }
  }, l.prepended = function (t) {
    var e = this._itemize(t);

    if (e.length) {
      this._resetLayout(), this._manageStamps();

      var i = this._filterRevealAdded(e);

      this.layoutItems(this.filteredItems), this.filteredItems = i.concat(this.filteredItems), this.items = e.concat(this.items);
    }
  }, l._filterRevealAdded = function (t) {
    var e = this._filter(t);

    return this.hide(e.needHide), this.reveal(e.matches), this.layoutItems(e.matches, !0), e.matches;
  }, l.insert = function (t) {
    var e = this.addItems(t);

    if (e.length) {
      var i,
          n,
          o = e.length;

      for (i = 0; i < o; i++) {
        n = e[i], this.element.appendChild(n.element);
      }

      var s = this._filter(e).matches;

      for (i = 0; i < o; i++) {
        e[i].isLayoutInstant = !0;
      }

      for (this.arrange(), i = 0; i < o; i++) {
        delete e[i].isLayoutInstant;
      }

      this.reveal(s);
    }
  };
  var m = l.remove;
  return l.remove = function (t) {
    t = o.makeArray(t);
    var e = this.getItems(t);
    m.call(this, t);

    for (var i = e && e.length, n = 0; i && n < i; n++) {
      var s = e[n];
      o.removeFrom(this.filteredItems, s);
    }
  }, l.shuffle = function () {
    for (var t = 0; t < this.items.length; t++) {
      var e = this.items[t];
      e.sortData.random = Math.random();
    }

    this.options.sortBy = "random", this._sort(), this._layout();
  }, l._noTransition = function (t, e) {
    var i = this.options.transitionDuration;
    this.options.transitionDuration = 0;
    var n = t.apply(this, e);
    return this.options.transitionDuration = i, n;
  }, l.getFilteredItemElements = function () {
    return this.filteredItems.map(function (t) {
      return t.element;
    });
  }, d;
});

/***/ }),

/***/ "./js/libs/oldFiles/jquery-ui-touch-punch.js":
/*!***************************************************!*\
  !*** ./js/libs/oldFiles/jquery-ui-touch-punch.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {/*!
 * jQuery UI Touch Punch 0.2.3
 *
 * Copyright 2011–2014, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
!function (a) {
  function f(a, b) {
    if (!(a.originalEvent.touches.length > 1)) {
      a.preventDefault();
      var c = a.originalEvent.changedTouches[0],
          d = document.createEvent("MouseEvents");
      d.initMouseEvent(b, !0, !0, window, 1, c.screenX, c.screenY, c.clientX, c.clientY, !1, !1, !1, !1, 0, null), a.target.dispatchEvent(d);
    }
  }

  if (a.support.touch = "ontouchend" in document, a.support.touch) {
    var e,
        b = a.ui.mouse.prototype,
        c = b._mouseInit,
        d = b._mouseDestroy;
    b._touchStart = function (a) {
      var b = this;
      !e && b._mouseCapture(a.originalEvent.changedTouches[0]) && (e = !0, b._touchMoved = !1, f(a, "mouseover"), f(a, "mousemove"), f(a, "mousedown"));
    }, b._touchMove = function (a) {
      e && (this._touchMoved = !0, f(a, "mousemove"));
    }, b._touchEnd = function (a) {
      e && (f(a, "mouseup"), f(a, "mouseout"), this._touchMoved || f(a, "click"), e = !1);
    }, b._mouseInit = function () {
      var b = this;
      b.element.bind({
        touchstart: a.proxy(b, "_touchStart"),
        touchmove: a.proxy(b, "_touchMove"),
        touchend: a.proxy(b, "_touchEnd")
      }), c.call(b);
    }, b._mouseDestroy = function () {
      var b = this;
      b.element.unbind({
        touchstart: a.proxy(b, "_touchStart"),
        touchmove: a.proxy(b, "_touchMove"),
        touchend: a.proxy(b, "_touchEnd")
      }), d.call(b);
    };
  }
}(jQuery);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/libs/oldFiles/jquery-ui.js":
/*!***************************************!*\
  !*** ./js/libs/oldFiles/jquery-ui.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! jQuery UI - v1.11.4 - 2015-12-03
* http://jqueryui.com
* Includes: core.js, widget.js, mouse.js, button.js, slider.js
* Copyright jQuery Foundation and other contributors; Licensed MIT */
(function (e) {
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
})(function (e) {
  function t(t, s) {
    var n,
        a,
        o,
        r = t.nodeName.toLowerCase();
    return "area" === r ? (n = t.parentNode, a = n.name, t.href && a && "map" === n.nodeName.toLowerCase() ? (o = e("img[usemap='#" + a + "']")[0], !!o && i(o)) : !1) : (/^(input|select|textarea|button|object)$/.test(r) ? !t.disabled : "a" === r ? t.href || s : s) && i(t);
  }

  function i(t) {
    return e.expr.filters.visible(t) && !e(t).parents().addBack().filter(function () {
      return "hidden" === e.css(this, "visibility");
    }).length;
  }

  e.ui = e.ui || {}, e.extend(e.ui, {
    version: "1.11.4",
    keyCode: {
      BACKSPACE: 8,
      COMMA: 188,
      DELETE: 46,
      DOWN: 40,
      END: 35,
      ENTER: 13,
      ESCAPE: 27,
      HOME: 36,
      LEFT: 37,
      PAGE_DOWN: 34,
      PAGE_UP: 33,
      PERIOD: 190,
      RIGHT: 39,
      SPACE: 32,
      TAB: 9,
      UP: 38
    }
  }), e.fn.extend({
    scrollParent: function scrollParent(t) {
      var i = this.css("position"),
          s = "absolute" === i,
          n = t ? /(auto|scroll|hidden)/ : /(auto|scroll)/,
          a = this.parents().filter(function () {
        var t = e(this);
        return s && "static" === t.css("position") ? !1 : n.test(t.css("overflow") + t.css("overflow-y") + t.css("overflow-x"));
      }).eq(0);
      return "fixed" !== i && a.length ? a : e(this[0].ownerDocument || document);
    },
    uniqueId: function () {
      var e = 0;
      return function () {
        return this.each(function () {
          this.id || (this.id = "ui-id-" + ++e);
        });
      };
    }(),
    removeUniqueId: function removeUniqueId() {
      return this.each(function () {
        /^ui-id-\d+$/.test(this.id) && e(this).removeAttr("id");
      });
    }
  }), e.extend(e.expr[":"], {
    data: e.expr.createPseudo ? e.expr.createPseudo(function (t) {
      return function (i) {
        return !!e.data(i, t);
      };
    }) : function (t, i, s) {
      return !!e.data(t, s[3]);
    },
    focusable: function focusable(i) {
      return t(i, !isNaN(e.attr(i, "tabindex")));
    },
    tabbable: function tabbable(i) {
      var s = e.attr(i, "tabindex"),
          n = isNaN(s);
      return (n || s >= 0) && t(i, !n);
    }
  }), e("<a>").outerWidth(1).jquery || e.each(["Width", "Height"], function (t, i) {
    function s(t, i, s, a) {
      return e.each(n, function () {
        i -= parseFloat(e.css(t, "padding" + this)) || 0, s && (i -= parseFloat(e.css(t, "border" + this + "Width")) || 0), a && (i -= parseFloat(e.css(t, "margin" + this)) || 0);
      }), i;
    }

    var n = "Width" === i ? ["Left", "Right"] : ["Top", "Bottom"],
        a = i.toLowerCase(),
        o = {
      innerWidth: e.fn.innerWidth,
      innerHeight: e.fn.innerHeight,
      outerWidth: e.fn.outerWidth,
      outerHeight: e.fn.outerHeight
    };
    e.fn["inner" + i] = function (t) {
      return void 0 === t ? o["inner" + i].call(this) : this.each(function () {
        e(this).css(a, s(this, t) + "px");
      });
    }, e.fn["outer" + i] = function (t, n) {
      return "number" != typeof t ? o["outer" + i].call(this, t) : this.each(function () {
        e(this).css(a, s(this, t, !0, n) + "px");
      });
    };
  }), e.fn.addBack || (e.fn.addBack = function (e) {
    return this.add(null == e ? this.prevObject : this.prevObject.filter(e));
  }), e("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (e.fn.removeData = function (t) {
    return function (i) {
      return arguments.length ? t.call(this, e.camelCase(i)) : t.call(this);
    };
  }(e.fn.removeData)), e.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), e.fn.extend({
    focus: function (t) {
      return function (i, s) {
        return "number" == typeof i ? this.each(function () {
          var t = this;
          setTimeout(function () {
            e(t).focus(), s && s.call(t);
          }, i);
        }) : t.apply(this, arguments);
      };
    }(e.fn.focus),
    disableSelection: function () {
      var e = "onselectstart" in document.createElement("div") ? "selectstart" : "mousedown";
      return function () {
        return this.bind(e + ".ui-disableSelection", function (e) {
          e.preventDefault();
        });
      };
    }(),
    enableSelection: function enableSelection() {
      return this.unbind(".ui-disableSelection");
    },
    zIndex: function zIndex(t) {
      if (void 0 !== t) return this.css("zIndex", t);
      if (this.length) for (var i, s, n = e(this[0]); n.length && n[0] !== document;) {
        if (i = n.css("position"), ("absolute" === i || "relative" === i || "fixed" === i) && (s = parseInt(n.css("zIndex"), 10), !isNaN(s) && 0 !== s)) return s;
        n = n.parent();
      }
      return 0;
    }
  }), e.ui.plugin = {
    add: function add(t, i, s) {
      var n,
          a = e.ui[t].prototype;

      for (n in s) {
        a.plugins[n] = a.plugins[n] || [], a.plugins[n].push([i, s[n]]);
      }
    },
    call: function call(e, t, i, s) {
      var n,
          a = e.plugins[t];
      if (a && (s || e.element[0].parentNode && 11 !== e.element[0].parentNode.nodeType)) for (n = 0; a.length > n; n++) {
        e.options[a[n][0]] && a[n][1].apply(e.element, i);
      }
    }
  };
  var s = 0,
      n = Array.prototype.slice;
  e.cleanData = function (t) {
    return function (i) {
      var s, n, a;

      for (a = 0; null != (n = i[a]); a++) {
        try {
          s = e._data(n, "events"), s && s.remove && e(n).triggerHandler("remove");
        } catch (o) {}
      }

      t(i);
    };
  }(e.cleanData), e.widget = function (t, i, s) {
    var n,
        a,
        o,
        r,
        h = {},
        l = t.split(".")[0];
    return t = t.split(".")[1], n = l + "-" + t, s || (s = i, i = e.Widget), e.expr[":"][n.toLowerCase()] = function (t) {
      return !!e.data(t, n);
    }, e[l] = e[l] || {}, a = e[l][t], o = e[l][t] = function (e, t) {
      return this._createWidget ? (arguments.length && this._createWidget(e, t), void 0) : new o(e, t);
    }, e.extend(o, a, {
      version: s.version,
      _proto: e.extend({}, s),
      _childConstructors: []
    }), r = new i(), r.options = e.widget.extend({}, r.options), e.each(s, function (t, s) {
      return e.isFunction(s) ? (h[t] = function () {
        var e = function e() {
          return i.prototype[t].apply(this, arguments);
        },
            n = function n(e) {
          return i.prototype[t].apply(this, e);
        };

        return function () {
          var t,
              i = this._super,
              a = this._superApply;
          return this._super = e, this._superApply = n, t = s.apply(this, arguments), this._super = i, this._superApply = a, t;
        };
      }(), void 0) : (h[t] = s, void 0);
    }), o.prototype = e.widget.extend(r, {
      widgetEventPrefix: a ? r.widgetEventPrefix || t : t
    }, h, {
      constructor: o,
      namespace: l,
      widgetName: t,
      widgetFullName: n
    }), a ? (e.each(a._childConstructors, function (t, i) {
      var s = i.prototype;
      e.widget(s.namespace + "." + s.widgetName, o, i._proto);
    }), delete a._childConstructors) : i._childConstructors.push(o), e.widget.bridge(t, o), o;
  }, e.widget.extend = function (t) {
    for (var i, s, a = n.call(arguments, 1), o = 0, r = a.length; r > o; o++) {
      for (i in a[o]) {
        s = a[o][i], a[o].hasOwnProperty(i) && void 0 !== s && (t[i] = e.isPlainObject(s) ? e.isPlainObject(t[i]) ? e.widget.extend({}, t[i], s) : e.widget.extend({}, s) : s);
      }
    }

    return t;
  }, e.widget.bridge = function (t, i) {
    var s = i.prototype.widgetFullName || t;

    e.fn[t] = function (a) {
      var o = "string" == typeof a,
          r = n.call(arguments, 1),
          h = this;
      return o ? this.each(function () {
        var i,
            n = e.data(this, s);
        return "instance" === a ? (h = n, !1) : n ? e.isFunction(n[a]) && "_" !== a.charAt(0) ? (i = n[a].apply(n, r), i !== n && void 0 !== i ? (h = i && i.jquery ? h.pushStack(i.get()) : i, !1) : void 0) : e.error("no such method '" + a + "' for " + t + " widget instance") : e.error("cannot call methods on " + t + " prior to initialization; " + "attempted to call method '" + a + "'");
      }) : (r.length && (a = e.widget.extend.apply(null, [a].concat(r))), this.each(function () {
        var t = e.data(this, s);
        t ? (t.option(a || {}), t._init && t._init()) : e.data(this, s, new i(a, this));
      })), h;
    };
  }, e.Widget = function () {}, e.Widget._childConstructors = [], e.Widget.prototype = {
    widgetName: "widget",
    widgetEventPrefix: "",
    defaultElement: "<div>",
    options: {
      disabled: !1,
      create: null
    },
    _createWidget: function _createWidget(t, i) {
      i = e(i || this.defaultElement || this)[0], this.element = e(i), this.uuid = s++, this.eventNamespace = "." + this.widgetName + this.uuid, this.bindings = e(), this.hoverable = e(), this.focusable = e(), i !== this && (e.data(i, this.widgetFullName, this), this._on(!0, this.element, {
        remove: function remove(e) {
          e.target === i && this.destroy();
        }
      }), this.document = e(i.style ? i.ownerDocument : i.document || i), this.window = e(this.document[0].defaultView || this.document[0].parentWindow)), this.options = e.widget.extend({}, this.options, this._getCreateOptions(), t), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init();
    },
    _getCreateOptions: e.noop,
    _getCreateEventData: e.noop,
    _create: e.noop,
    _init: e.noop,
    destroy: function destroy() {
      this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled " + "ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus");
    },
    _destroy: e.noop,
    widget: function widget() {
      return this.element;
    },
    option: function option(t, i) {
      var s,
          n,
          a,
          o = t;
      if (0 === arguments.length) return e.widget.extend({}, this.options);
      if ("string" == typeof t) if (o = {}, s = t.split("."), t = s.shift(), s.length) {
        for (n = o[t] = e.widget.extend({}, this.options[t]), a = 0; s.length - 1 > a; a++) {
          n[s[a]] = n[s[a]] || {}, n = n[s[a]];
        }

        if (t = s.pop(), 1 === arguments.length) return void 0 === n[t] ? null : n[t];
        n[t] = i;
      } else {
        if (1 === arguments.length) return void 0 === this.options[t] ? null : this.options[t];
        o[t] = i;
      }
      return this._setOptions(o), this;
    },
    _setOptions: function _setOptions(e) {
      var t;

      for (t in e) {
        this._setOption(t, e[t]);
      }

      return this;
    },
    _setOption: function _setOption(e, t) {
      return this.options[e] = t, "disabled" === e && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!t), t && (this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus"))), this;
    },
    enable: function enable() {
      return this._setOptions({
        disabled: !1
      });
    },
    disable: function disable() {
      return this._setOptions({
        disabled: !0
      });
    },
    _on: function _on(t, i, s) {
      var n,
          a = this;
      "boolean" != typeof t && (s = i, i = t, t = !1), s ? (i = n = e(i), this.bindings = this.bindings.add(i)) : (s = i, i = this.element, n = this.widget()), e.each(s, function (s, o) {
        function r() {
          return t || a.options.disabled !== !0 && !e(this).hasClass("ui-state-disabled") ? ("string" == typeof o ? a[o] : o).apply(a, arguments) : void 0;
        }

        "string" != typeof o && (r.guid = o.guid = o.guid || r.guid || e.guid++);
        var h = s.match(/^([\w:-]*)\s*(.*)$/),
            l = h[1] + a.eventNamespace,
            u = h[2];
        u ? n.delegate(u, l, r) : i.bind(l, r);
      });
    },
    _off: function _off(t, i) {
      i = (i || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, t.unbind(i).undelegate(i), this.bindings = e(this.bindings.not(t).get()), this.focusable = e(this.focusable.not(t).get()), this.hoverable = e(this.hoverable.not(t).get());
    },
    _delay: function _delay(e, t) {
      function i() {
        return ("string" == typeof e ? s[e] : e).apply(s, arguments);
      }

      var s = this;
      return setTimeout(i, t || 0);
    },
    _hoverable: function _hoverable(t) {
      this.hoverable = this.hoverable.add(t), this._on(t, {
        mouseenter: function mouseenter(t) {
          e(t.currentTarget).addClass("ui-state-hover");
        },
        mouseleave: function mouseleave(t) {
          e(t.currentTarget).removeClass("ui-state-hover");
        }
      });
    },
    _focusable: function _focusable(t) {
      this.focusable = this.focusable.add(t), this._on(t, {
        focusin: function focusin(t) {
          e(t.currentTarget).addClass("ui-state-focus");
        },
        focusout: function focusout(t) {
          e(t.currentTarget).removeClass("ui-state-focus");
        }
      });
    },
    _trigger: function _trigger(t, i, s) {
      var n,
          a,
          o = this.options[t];
      if (s = s || {}, i = e.Event(i), i.type = (t === this.widgetEventPrefix ? t : this.widgetEventPrefix + t).toLowerCase(), i.target = this.element[0], a = i.originalEvent) for (n in a) {
        n in i || (i[n] = a[n]);
      }
      return this.element.trigger(i, s), !(e.isFunction(o) && o.apply(this.element[0], [i].concat(s)) === !1 || i.isDefaultPrevented());
    }
  }, e.each({
    show: "fadeIn",
    hide: "fadeOut"
  }, function (t, i) {
    e.Widget.prototype["_" + t] = function (s, n, a) {
      "string" == typeof n && (n = {
        effect: n
      });
      var o,
          r = n ? n === !0 || "number" == typeof n ? i : n.effect || i : t;
      n = n || {}, "number" == typeof n && (n = {
        duration: n
      }), o = !e.isEmptyObject(n), n.complete = a, n.delay && s.delay(n.delay), o && e.effects && e.effects.effect[r] ? s[t](n) : r !== t && s[r] ? s[r](n.duration, n.easing, a) : s.queue(function (i) {
        e(this)[t](), a && a.call(s[0]), i();
      });
    };
  }), e.widget;
  var a = !1;
  e(document).mouseup(function () {
    a = !1;
  }), e.widget("ui.mouse", {
    version: "1.11.4",
    options: {
      cancel: "input,textarea,button,select,option",
      distance: 1,
      delay: 0
    },
    _mouseInit: function _mouseInit() {
      var t = this;
      this.element.bind("mousedown." + this.widgetName, function (e) {
        return t._mouseDown(e);
      }).bind("click." + this.widgetName, function (i) {
        return !0 === e.data(i.target, t.widgetName + ".preventClickEvent") ? (e.removeData(i.target, t.widgetName + ".preventClickEvent"), i.stopImmediatePropagation(), !1) : void 0;
      }), this.started = !1;
    },
    _mouseDestroy: function _mouseDestroy() {
      this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate);
    },
    _mouseDown: function _mouseDown(t) {
      if (!a) {
        this._mouseMoved = !1, this._mouseStarted && this._mouseUp(t), this._mouseDownEvent = t;
        var i = this,
            s = 1 === t.which,
            n = "string" == typeof this.options.cancel && t.target.nodeName ? e(t.target).closest(this.options.cancel).length : !1;
        return s && !n && this._mouseCapture(t) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function () {
          i.mouseDelayMet = !0;
        }, this.options.delay)), this._mouseDistanceMet(t) && this._mouseDelayMet(t) && (this._mouseStarted = this._mouseStart(t) !== !1, !this._mouseStarted) ? (t.preventDefault(), !0) : (!0 === e.data(t.target, this.widgetName + ".preventClickEvent") && e.removeData(t.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function (e) {
          return i._mouseMove(e);
        }, this._mouseUpDelegate = function (e) {
          return i._mouseUp(e);
        }, this.document.bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), t.preventDefault(), a = !0, !0)) : !0;
      }
    },
    _mouseMove: function _mouseMove(t) {
      if (this._mouseMoved) {
        if (e.ui.ie && (!document.documentMode || 9 > document.documentMode) && !t.button) return this._mouseUp(t);
        if (!t.which) return this._mouseUp(t);
      }

      return (t.which || t.button) && (this._mouseMoved = !0), this._mouseStarted ? (this._mouseDrag(t), t.preventDefault()) : (this._mouseDistanceMet(t) && this._mouseDelayMet(t) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, t) !== !1, this._mouseStarted ? this._mouseDrag(t) : this._mouseUp(t)), !this._mouseStarted);
    },
    _mouseUp: function _mouseUp(t) {
      return this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, t.target === this._mouseDownEvent.target && e.data(t.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(t)), a = !1, !1;
    },
    _mouseDistanceMet: function _mouseDistanceMet(e) {
      return Math.max(Math.abs(this._mouseDownEvent.pageX - e.pageX), Math.abs(this._mouseDownEvent.pageY - e.pageY)) >= this.options.distance;
    },
    _mouseDelayMet: function _mouseDelayMet() {
      return this.mouseDelayMet;
    },
    _mouseStart: function _mouseStart() {},
    _mouseDrag: function _mouseDrag() {},
    _mouseStop: function _mouseStop() {},
    _mouseCapture: function _mouseCapture() {
      return !0;
    }
  });

  var o,
      r = "ui-button ui-widget ui-state-default ui-corner-all",
      h = "ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only",
      l = function l() {
    var t = e(this);
    setTimeout(function () {
      t.find(":ui-button").button("refresh");
    }, 1);
  },
      u = function u(t) {
    var i = t.name,
        s = t.form,
        n = e([]);
    return i && (i = i.replace(/'/g, "\\'"), n = s ? e(s).find("[name='" + i + "'][type=radio]") : e("[name='" + i + "'][type=radio]", t.ownerDocument).filter(function () {
      return !this.form;
    })), n;
  };

  e.widget("ui.button", {
    version: "1.11.4",
    defaultElement: "<button>",
    options: {
      disabled: null,
      text: !0,
      label: null,
      icons: {
        primary: null,
        secondary: null
      }
    },
    _create: function _create() {
      this.element.closest("form").unbind("reset" + this.eventNamespace).bind("reset" + this.eventNamespace, l), "boolean" != typeof this.options.disabled ? this.options.disabled = !!this.element.prop("disabled") : this.element.prop("disabled", this.options.disabled), this._determineButtonType(), this.hasTitle = !!this.buttonElement.attr("title");
      var t = this,
          i = this.options,
          s = "checkbox" === this.type || "radio" === this.type,
          n = s ? "" : "ui-state-active";
      null === i.label && (i.label = "input" === this.type ? this.buttonElement.val() : this.buttonElement.html()), this._hoverable(this.buttonElement), this.buttonElement.addClass(r).attr("role", "button").bind("mouseenter" + this.eventNamespace, function () {
        i.disabled || this === o && e(this).addClass("ui-state-active");
      }).bind("mouseleave" + this.eventNamespace, function () {
        i.disabled || e(this).removeClass(n);
      }).bind("click" + this.eventNamespace, function (e) {
        i.disabled && (e.preventDefault(), e.stopImmediatePropagation());
      }), this._on({
        focus: function focus() {
          this.buttonElement.addClass("ui-state-focus");
        },
        blur: function blur() {
          this.buttonElement.removeClass("ui-state-focus");
        }
      }), s && this.element.bind("change" + this.eventNamespace, function () {
        t.refresh();
      }), "checkbox" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function () {
        return i.disabled ? !1 : void 0;
      }) : "radio" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function () {
        if (i.disabled) return !1;
        e(this).addClass("ui-state-active"), t.buttonElement.attr("aria-pressed", "true");
        var s = t.element[0];
        u(s).not(s).map(function () {
          return e(this).button("widget")[0];
        }).removeClass("ui-state-active").attr("aria-pressed", "false");
      }) : (this.buttonElement.bind("mousedown" + this.eventNamespace, function () {
        return i.disabled ? !1 : (e(this).addClass("ui-state-active"), o = this, t.document.one("mouseup", function () {
          o = null;
        }), void 0);
      }).bind("mouseup" + this.eventNamespace, function () {
        return i.disabled ? !1 : (e(this).removeClass("ui-state-active"), void 0);
      }).bind("keydown" + this.eventNamespace, function (t) {
        return i.disabled ? !1 : ((t.keyCode === e.ui.keyCode.SPACE || t.keyCode === e.ui.keyCode.ENTER) && e(this).addClass("ui-state-active"), void 0);
      }).bind("keyup" + this.eventNamespace + " blur" + this.eventNamespace, function () {
        e(this).removeClass("ui-state-active");
      }), this.buttonElement.is("a") && this.buttonElement.keyup(function (t) {
        t.keyCode === e.ui.keyCode.SPACE && e(this).click();
      })), this._setOption("disabled", i.disabled), this._resetButton();
    },
    _determineButtonType: function _determineButtonType() {
      var e, t, i;
      this.type = this.element.is("[type=checkbox]") ? "checkbox" : this.element.is("[type=radio]") ? "radio" : this.element.is("input") ? "input" : "button", "checkbox" === this.type || "radio" === this.type ? (e = this.element.parents().last(), t = "label[for='" + this.element.attr("id") + "']", this.buttonElement = e.find(t), this.buttonElement.length || (e = e.length ? e.siblings() : this.element.siblings(), this.buttonElement = e.filter(t), this.buttonElement.length || (this.buttonElement = e.find(t))), this.element.addClass("ui-helper-hidden-accessible"), i = this.element.is(":checked"), i && this.buttonElement.addClass("ui-state-active"), this.buttonElement.prop("aria-pressed", i)) : this.buttonElement = this.element;
    },
    widget: function widget() {
      return this.buttonElement;
    },
    _destroy: function _destroy() {
      this.element.removeClass("ui-helper-hidden-accessible"), this.buttonElement.removeClass(r + " ui-state-active " + h).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html()), this.hasTitle || this.buttonElement.removeAttr("title");
    },
    _setOption: function _setOption(e, t) {
      return this._super(e, t), "disabled" === e ? (this.widget().toggleClass("ui-state-disabled", !!t), this.element.prop("disabled", !!t), t && ("checkbox" === this.type || "radio" === this.type ? this.buttonElement.removeClass("ui-state-focus") : this.buttonElement.removeClass("ui-state-focus ui-state-active")), void 0) : (this._resetButton(), void 0);
    },
    refresh: function refresh() {
      var t = this.element.is("input, button") ? this.element.is(":disabled") : this.element.hasClass("ui-button-disabled");
      t !== this.options.disabled && this._setOption("disabled", t), "radio" === this.type ? u(this.element[0]).each(function () {
        e(this).is(":checked") ? e(this).button("widget").addClass("ui-state-active").attr("aria-pressed", "true") : e(this).button("widget").removeClass("ui-state-active").attr("aria-pressed", "false");
      }) : "checkbox" === this.type && (this.element.is(":checked") ? this.buttonElement.addClass("ui-state-active").attr("aria-pressed", "true") : this.buttonElement.removeClass("ui-state-active").attr("aria-pressed", "false"));
    },
    _resetButton: function _resetButton() {
      if ("input" === this.type) return this.options.label && this.element.val(this.options.label), void 0;
      var t = this.buttonElement.removeClass(h),
          i = e("<span></span>", this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(t.empty()).text(),
          s = this.options.icons,
          n = s.primary && s.secondary,
          a = [];
      s.primary || s.secondary ? (this.options.text && a.push("ui-button-text-icon" + (n ? "s" : s.primary ? "-primary" : "-secondary")), s.primary && t.prepend("<span class='ui-button-icon-primary ui-icon " + s.primary + "'></span>"), s.secondary && t.append("<span class='ui-button-icon-secondary ui-icon " + s.secondary + "'></span>"), this.options.text || (a.push(n ? "ui-button-icons-only" : "ui-button-icon-only"), this.hasTitle || t.attr("title", e.trim(i)))) : a.push("ui-button-text-only"), t.addClass(a.join(" "));
    }
  }), e.widget("ui.buttonset", {
    version: "1.11.4",
    options: {
      items: "button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(ui-button)"
    },
    _create: function _create() {
      this.element.addClass("ui-buttonset");
    },
    _init: function _init() {
      this.refresh();
    },
    _setOption: function _setOption(e, t) {
      "disabled" === e && this.buttons.button("option", e, t), this._super(e, t);
    },
    refresh: function refresh() {
      var t = "rtl" === this.element.css("direction"),
          i = this.element.find(this.options.items),
          s = i.filter(":ui-button");
      i.not(":ui-button").button(), s.button("refresh"), this.buttons = i.map(function () {
        return e(this).button("widget")[0];
      }).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(t ? "ui-corner-right" : "ui-corner-left").end().filter(":last").addClass(t ? "ui-corner-left" : "ui-corner-right").end().end();
    },
    _destroy: function _destroy() {
      this.element.removeClass("ui-buttonset"), this.buttons.map(function () {
        return e(this).button("widget")[0];
      }).removeClass("ui-corner-left ui-corner-right").end().button("destroy");
    }
  }), e.ui.button, e.widget("ui.slider", e.ui.mouse, {
    version: "1.11.4",
    widgetEventPrefix: "slide",
    options: {
      animate: !1,
      distance: 0,
      max: 100,
      min: 0,
      orientation: "horizontal",
      range: !1,
      step: 1,
      value: 0,
      values: null,
      change: null,
      slide: null,
      start: null,
      stop: null
    },
    numPages: 5,
    _create: function _create() {
      this._keySliding = !1, this._mouseSliding = !1, this._animateOff = !0, this._handleIndex = null, this._detectOrientation(), this._mouseInit(), this._calculateNewMax(), this.element.addClass("ui-slider ui-slider-" + this.orientation + " ui-widget" + " ui-widget-content" + " ui-corner-all"), this._refresh(), this._setOption("disabled", this.options.disabled), this._animateOff = !1;
    },
    _refresh: function _refresh() {
      this._createRange(), this._createHandles(), this._setupEvents(), this._refreshValue();
    },
    _createHandles: function _createHandles() {
      var t,
          i,
          s = this.options,
          n = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),
          a = "<span class='ui-slider-handle ui-state-default ui-corner-all' tabindex='0'></span>",
          o = [];

      for (i = s.values && s.values.length || 1, n.length > i && (n.slice(i).remove(), n = n.slice(0, i)), t = n.length; i > t; t++) {
        o.push(a);
      }

      this.handles = n.add(e(o.join("")).appendTo(this.element)), this.handle = this.handles.eq(0), this.handles.each(function (t) {
        e(this).data("ui-slider-handle-index", t);
      });
    },
    _createRange: function _createRange() {
      var t = this.options,
          i = "";
      t.range ? (t.range === !0 && (t.values ? t.values.length && 2 !== t.values.length ? t.values = [t.values[0], t.values[0]] : e.isArray(t.values) && (t.values = t.values.slice(0)) : t.values = [this._valueMin(), this._valueMin()]), this.range && this.range.length ? this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({
        left: "",
        bottom: ""
      }) : (this.range = e("<div></div>").appendTo(this.element), i = "ui-slider-range ui-widget-header ui-corner-all"), this.range.addClass(i + ("min" === t.range || "max" === t.range ? " ui-slider-range-" + t.range : ""))) : (this.range && this.range.remove(), this.range = null);
    },
    _setupEvents: function _setupEvents() {
      this._off(this.handles), this._on(this.handles, this._handleEvents), this._hoverable(this.handles), this._focusable(this.handles);
    },
    _destroy: function _destroy() {
      this.handles.remove(), this.range && this.range.remove(), this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"), this._mouseDestroy();
    },
    _mouseCapture: function _mouseCapture(t) {
      var i,
          s,
          n,
          a,
          o,
          r,
          h,
          l,
          u = this,
          d = this.options;
      return d.disabled ? !1 : (this.elementSize = {
        width: this.element.outerWidth(),
        height: this.element.outerHeight()
      }, this.elementOffset = this.element.offset(), i = {
        x: t.pageX,
        y: t.pageY
      }, s = this._normValueFromMouse(i), n = this._valueMax() - this._valueMin() + 1, this.handles.each(function (t) {
        var i = Math.abs(s - u.values(t));
        (n > i || n === i && (t === u._lastChangedValue || u.values(t) === d.min)) && (n = i, a = e(this), o = t);
      }), r = this._start(t, o), r === !1 ? !1 : (this._mouseSliding = !0, this._handleIndex = o, a.addClass("ui-state-active").focus(), h = a.offset(), l = !e(t.target).parents().addBack().is(".ui-slider-handle"), this._clickOffset = l ? {
        left: 0,
        top: 0
      } : {
        left: t.pageX - h.left - a.width() / 2,
        top: t.pageY - h.top - a.height() / 2 - (parseInt(a.css("borderTopWidth"), 10) || 0) - (parseInt(a.css("borderBottomWidth"), 10) || 0) + (parseInt(a.css("marginTop"), 10) || 0)
      }, this.handles.hasClass("ui-state-hover") || this._slide(t, o, s), this._animateOff = !0, !0));
    },
    _mouseStart: function _mouseStart() {
      return !0;
    },
    _mouseDrag: function _mouseDrag(e) {
      var t = {
        x: e.pageX,
        y: e.pageY
      },
          i = this._normValueFromMouse(t);

      return this._slide(e, this._handleIndex, i), !1;
    },
    _mouseStop: function _mouseStop(e) {
      return this.handles.removeClass("ui-state-active"), this._mouseSliding = !1, this._stop(e, this._handleIndex), this._change(e, this._handleIndex), this._handleIndex = null, this._clickOffset = null, this._animateOff = !1, !1;
    },
    _detectOrientation: function _detectOrientation() {
      this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal";
    },
    _normValueFromMouse: function _normValueFromMouse(e) {
      var t, i, s, n, a;
      return "horizontal" === this.orientation ? (t = this.elementSize.width, i = e.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (t = this.elementSize.height, i = e.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)), s = i / t, s > 1 && (s = 1), 0 > s && (s = 0), "vertical" === this.orientation && (s = 1 - s), n = this._valueMax() - this._valueMin(), a = this._valueMin() + s * n, this._trimAlignValue(a);
    },
    _start: function _start(e, t) {
      var i = {
        handle: this.handles[t],
        value: this.value()
      };
      return this.options.values && this.options.values.length && (i.value = this.values(t), i.values = this.values()), this._trigger("start", e, i);
    },
    _slide: function _slide(e, t, i) {
      var s, n, a;
      this.options.values && this.options.values.length ? (s = this.values(t ? 0 : 1), 2 === this.options.values.length && this.options.range === !0 && (0 === t && i > s || 1 === t && s > i) && (i = s), i !== this.values(t) && (n = this.values(), n[t] = i, a = this._trigger("slide", e, {
        handle: this.handles[t],
        value: i,
        values: n
      }), s = this.values(t ? 0 : 1), a !== !1 && this.values(t, i))) : i !== this.value() && (a = this._trigger("slide", e, {
        handle: this.handles[t],
        value: i
      }), a !== !1 && this.value(i));
    },
    _stop: function _stop(e, t) {
      var i = {
        handle: this.handles[t],
        value: this.value()
      };
      this.options.values && this.options.values.length && (i.value = this.values(t), i.values = this.values()), this._trigger("stop", e, i);
    },
    _change: function _change(e, t) {
      if (!this._keySliding && !this._mouseSliding) {
        var i = {
          handle: this.handles[t],
          value: this.value()
        };
        this.options.values && this.options.values.length && (i.value = this.values(t), i.values = this.values()), this._lastChangedValue = t, this._trigger("change", e, i);
      }
    },
    value: function value(e) {
      return arguments.length ? (this.options.value = this._trimAlignValue(e), this._refreshValue(), this._change(null, 0), void 0) : this._value();
    },
    values: function values(t, i) {
      var s, n, a;
      if (arguments.length > 1) return this.options.values[t] = this._trimAlignValue(i), this._refreshValue(), this._change(null, t), void 0;
      if (!arguments.length) return this._values();
      if (!e.isArray(arguments[0])) return this.options.values && this.options.values.length ? this._values(t) : this.value();

      for (s = this.options.values, n = arguments[0], a = 0; s.length > a; a += 1) {
        s[a] = this._trimAlignValue(n[a]), this._change(null, a);
      }

      this._refreshValue();
    },
    _setOption: function _setOption(t, i) {
      var s,
          n = 0;

      switch ("range" === t && this.options.range === !0 && ("min" === i ? (this.options.value = this._values(0), this.options.values = null) : "max" === i && (this.options.value = this._values(this.options.values.length - 1), this.options.values = null)), e.isArray(this.options.values) && (n = this.options.values.length), "disabled" === t && this.element.toggleClass("ui-state-disabled", !!i), this._super(t, i), t) {
        case "orientation":
          this._detectOrientation(), this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation), this._refreshValue(), this.handles.css("horizontal" === i ? "bottom" : "left", "");
          break;

        case "value":
          this._animateOff = !0, this._refreshValue(), this._change(null, 0), this._animateOff = !1;
          break;

        case "values":
          for (this._animateOff = !0, this._refreshValue(), s = 0; n > s; s += 1) {
            this._change(null, s);
          }

          this._animateOff = !1;
          break;

        case "step":
        case "min":
        case "max":
          this._animateOff = !0, this._calculateNewMax(), this._refreshValue(), this._animateOff = !1;
          break;

        case "range":
          this._animateOff = !0, this._refresh(), this._animateOff = !1;
      }
    },
    _value: function _value() {
      var e = this.options.value;
      return e = this._trimAlignValue(e);
    },
    _values: function _values(e) {
      var t, i, s;
      if (arguments.length) return t = this.options.values[e], t = this._trimAlignValue(t);

      if (this.options.values && this.options.values.length) {
        for (i = this.options.values.slice(), s = 0; i.length > s; s += 1) {
          i[s] = this._trimAlignValue(i[s]);
        }

        return i;
      }

      return [];
    },
    _trimAlignValue: function _trimAlignValue(e) {
      if (this._valueMin() >= e) return this._valueMin();
      if (e >= this._valueMax()) return this._valueMax();
      var t = this.options.step > 0 ? this.options.step : 1,
          i = (e - this._valueMin()) % t,
          s = e - i;
      return 2 * Math.abs(i) >= t && (s += i > 0 ? t : -t), parseFloat(s.toFixed(5));
    },
    _calculateNewMax: function _calculateNewMax() {
      var e = this.options.max,
          t = this._valueMin(),
          i = this.options.step,
          s = Math.floor(+(e - t).toFixed(this._precision()) / i) * i;

      e = s + t, this.max = parseFloat(e.toFixed(this._precision()));
    },
    _precision: function _precision() {
      var e = this._precisionOf(this.options.step);

      return null !== this.options.min && (e = Math.max(e, this._precisionOf(this.options.min))), e;
    },
    _precisionOf: function _precisionOf(e) {
      var t = "" + e,
          i = t.indexOf(".");
      return -1 === i ? 0 : t.length - i - 1;
    },
    _valueMin: function _valueMin() {
      return this.options.min;
    },
    _valueMax: function _valueMax() {
      return this.max;
    },
    _refreshValue: function _refreshValue() {
      var t,
          i,
          s,
          n,
          a,
          o = this.options.range,
          r = this.options,
          h = this,
          l = this._animateOff ? !1 : r.animate,
          u = {};
      this.options.values && this.options.values.length ? this.handles.each(function (s) {
        i = 100 * ((h.values(s) - h._valueMin()) / (h._valueMax() - h._valueMin())), u["horizontal" === h.orientation ? "left" : "bottom"] = i + "%", e(this).stop(1, 1)[l ? "animate" : "css"](u, r.animate), h.options.range === !0 && ("horizontal" === h.orientation ? (0 === s && h.range.stop(1, 1)[l ? "animate" : "css"]({
          left: i + "%"
        }, r.animate), 1 === s && h.range[l ? "animate" : "css"]({
          width: i - t + "%"
        }, {
          queue: !1,
          duration: r.animate
        })) : (0 === s && h.range.stop(1, 1)[l ? "animate" : "css"]({
          bottom: i + "%"
        }, r.animate), 1 === s && h.range[l ? "animate" : "css"]({
          height: i - t + "%"
        }, {
          queue: !1,
          duration: r.animate
        }))), t = i;
      }) : (s = this.value(), n = this._valueMin(), a = this._valueMax(), i = a !== n ? 100 * ((s - n) / (a - n)) : 0, u["horizontal" === this.orientation ? "left" : "bottom"] = i + "%", this.handle.stop(1, 1)[l ? "animate" : "css"](u, r.animate), "min" === o && "horizontal" === this.orientation && this.range.stop(1, 1)[l ? "animate" : "css"]({
        width: i + "%"
      }, r.animate), "max" === o && "horizontal" === this.orientation && this.range[l ? "animate" : "css"]({
        width: 100 - i + "%"
      }, {
        queue: !1,
        duration: r.animate
      }), "min" === o && "vertical" === this.orientation && this.range.stop(1, 1)[l ? "animate" : "css"]({
        height: i + "%"
      }, r.animate), "max" === o && "vertical" === this.orientation && this.range[l ? "animate" : "css"]({
        height: 100 - i + "%"
      }, {
        queue: !1,
        duration: r.animate
      }));
    },
    _handleEvents: {
      keydown: function keydown(t) {
        var i,
            s,
            n,
            a,
            o = e(t.target).data("ui-slider-handle-index");

        switch (t.keyCode) {
          case e.ui.keyCode.HOME:
          case e.ui.keyCode.END:
          case e.ui.keyCode.PAGE_UP:
          case e.ui.keyCode.PAGE_DOWN:
          case e.ui.keyCode.UP:
          case e.ui.keyCode.RIGHT:
          case e.ui.keyCode.DOWN:
          case e.ui.keyCode.LEFT:
            if (t.preventDefault(), !this._keySliding && (this._keySliding = !0, e(t.target).addClass("ui-state-active"), i = this._start(t, o), i === !1)) return;
        }

        switch (a = this.options.step, s = n = this.options.values && this.options.values.length ? this.values(o) : this.value(), t.keyCode) {
          case e.ui.keyCode.HOME:
            n = this._valueMin();
            break;

          case e.ui.keyCode.END:
            n = this._valueMax();
            break;

          case e.ui.keyCode.PAGE_UP:
            n = this._trimAlignValue(s + (this._valueMax() - this._valueMin()) / this.numPages);
            break;

          case e.ui.keyCode.PAGE_DOWN:
            n = this._trimAlignValue(s - (this._valueMax() - this._valueMin()) / this.numPages);
            break;

          case e.ui.keyCode.UP:
          case e.ui.keyCode.RIGHT:
            if (s === this._valueMax()) return;
            n = this._trimAlignValue(s + a);
            break;

          case e.ui.keyCode.DOWN:
          case e.ui.keyCode.LEFT:
            if (s === this._valueMin()) return;
            n = this._trimAlignValue(s - a);
        }

        this._slide(t, o, n);
      },
      keyup: function keyup(t) {
        var i = e(t.target).data("ui-slider-handle-index");
        this._keySliding && (this._keySliding = !1, this._stop(t, i), this._change(t, i), e(t.target).removeClass("ui-state-active"));
      }
    }
  });
});

/***/ }),

/***/ "./js/libs/oldFiles/lazyload.min.js":
/*!******************************************!*\
  !*** ./js/libs/oldFiles/lazyload.min.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof2(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _extends() {
  return (_extends = Object.assign || function (t) {
    for (var e = 1; e < arguments.length; e++) {
      var n = arguments[e];

      for (var o in n) {
        Object.prototype.hasOwnProperty.call(n, o) && (t[o] = n[o]);
      }
    }

    return t;
  }).apply(this, arguments);
}

function _typeof(t) {
  return (_typeof = "function" == typeof Symbol && "symbol" == _typeof2(Symbol.iterator) ? function (t) {
    return _typeof2(t);
  } : function (t) {
    return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : _typeof2(t);
  })(t);
}

!function (t, e) {
  "object" === ( false ? undefined : _typeof(exports)) && "undefined" != typeof module ? module.exports = e() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function () {
  "use strict";

  var t = "undefined" != typeof window,
      e = t && !("onscroll" in window) || "undefined" != typeof navigator && /(gle|ing|ro)bot|crawl|spider/i.test(navigator.userAgent),
      n = t && "IntersectionObserver" in window,
      o = t && "classList" in document.createElement("p"),
      r = {
    elements_selector: "img",
    container: e || t ? document : null,
    threshold: 300,
    thresholds: null,
    data_src: "src",
    data_srcset: "srcset",
    data_sizes: "sizes",
    data_bg: "bg",
    class_loading: "loading",
    class_loaded: "loaded",
    class_error: "error",
    load_delay: 0,
    auto_unobserve: !0,
    callback_enter: null,
    callback_exit: null,
    callback_reveal: null,
    callback_loaded: null,
    callback_error: null,
    callback_finish: null,
    use_native: !1
  },
      a = function a(t, e) {
    var n,
        o = new t(e);

    try {
      n = new CustomEvent("LazyLoad::Initialized", {
        detail: {
          instance: o
        }
      });
    } catch (t) {
      (n = document.createEvent("CustomEvent")).initCustomEvent("LazyLoad::Initialized", !1, !1, {
        instance: o
      });
    }

    window.dispatchEvent(n);
  };

  var i = function i(t, e) {
    return t.getAttribute("data-" + e);
  },
      s = function s(t, e, n) {
    var o = "data-" + e;
    null !== n ? t.setAttribute(o, n) : t.removeAttribute(o);
  },
      c = function c(t) {
    return "true" === i(t, "was-processed");
  },
      l = function l(t, e) {
    return s(t, "ll-timeout", e);
  },
      u = function u(t) {
    return i(t, "ll-timeout");
  },
      d = function d(t, e) {
    t && t(e);
  },
      f = function f(t, e) {
    t._loadingCount += e, 0 === t._elements.length && 0 === t._loadingCount && d(t._settings.callback_finish);
  },
      _ = function _(t) {
    for (var e, n = [], o = 0; e = t.children[o]; o += 1) {
      "SOURCE" === e.tagName && n.push(e);
    }

    return n;
  },
      v = function v(t, e, n) {
    n && t.setAttribute(e, n);
  },
      g = function g(t, e) {
    v(t, "sizes", i(t, e.data_sizes)), v(t, "srcset", i(t, e.data_srcset)), v(t, "src", i(t, e.data_src));
  },
      m = {
    IMG: function IMG(t, e) {
      var n = t.parentNode;
      n && "PICTURE" === n.tagName && _(n).forEach(function (t) {
        g(t, e);
      });
      g(t, e);
    },
    IFRAME: function IFRAME(t, e) {
      v(t, "src", i(t, e.data_src));
    },
    VIDEO: function VIDEO(t, e) {
      _(t).forEach(function (t) {
        v(t, "src", i(t, e.data_src));
      }), v(t, "src", i(t, e.data_src)), t.load();
    }
  },
      b = function b(t, e) {
    var n,
        o,
        r = e._settings,
        a = t.tagName,
        s = m[a];
    if (s) return s(t, r), f(e, 1), void (e._elements = (n = e._elements, o = t, n.filter(function (t) {
      return t !== o;
    })));
    !function (t, e) {
      var n = i(t, e.data_src),
          o = i(t, e.data_bg);
      n && (t.style.backgroundImage = 'url("'.concat(n, '")')), o && (t.style.backgroundImage = o);
    }(t, r);
  },
      h = function h(t, e) {
    o ? t.classList.add(e) : t.className += (t.className ? " " : "") + e;
  },
      p = function p(t, e, n) {
    t.addEventListener(e, n);
  },
      y = function y(t, e, n) {
    t.removeEventListener(e, n);
  },
      E = function E(t, e, n) {
    y(t, "load", e), y(t, "loadeddata", e), y(t, "error", n);
  },
      w = function w(t, e, n) {
    var r = n._settings,
        a = e ? r.class_loaded : r.class_error,
        i = e ? r.callback_loaded : r.callback_error,
        s = t.target;
    !function (t, e) {
      o ? t.classList.remove(e) : t.className = t.className.replace(new RegExp("(^|\\s+)" + e + "(\\s+|$)"), " ").replace(/^\s+/, "").replace(/\s+$/, "");
    }(s, r.class_loading), h(s, a), d(i, s), f(n, -1);
  },
      I = function I(t, e) {
    var n = function n(r) {
      w(r, !0, e), E(t, n, o);
    },
        o = function o(r) {
      w(r, !1, e), E(t, n, o);
    };

    !function (t, e, n) {
      p(t, "load", e), p(t, "loadeddata", e), p(t, "error", n);
    }(t, n, o);
  },
      k = ["IMG", "IFRAME", "VIDEO"],
      A = function A(t, e) {
    var n = e._observer;
    z(t, e), n && e._settings.auto_unobserve && n.unobserve(t);
  },
      L = function L(t) {
    var e = u(t);
    e && (clearTimeout(e), l(t, null));
  },
      x = function x(t, e) {
    var n = e._settings.load_delay,
        o = u(t);
    o || (o = setTimeout(function () {
      A(t, e), L(t);
    }, n), l(t, o));
  },
      z = function z(t, e, n) {
    var o = e._settings;
    !n && c(t) || (k.indexOf(t.tagName) > -1 && (I(t, e), h(t, o.class_loading)), b(t, e), function (t) {
      s(t, "was-processed", "true");
    }(t), d(o.callback_reveal, t), d(o.callback_set, t));
  },
      O = function O(t) {
    return !!n && (t._observer = new IntersectionObserver(function (e) {
      e.forEach(function (e) {
        return function (t) {
          return t.isIntersecting || t.intersectionRatio > 0;
        }(e) ? function (t, e) {
          var n = e._settings;
          d(n.callback_enter, t), n.load_delay ? x(t, e) : A(t, e);
        }(e.target, t) : function (t, e) {
          var n = e._settings;
          d(n.callback_exit, t), n.load_delay && L(t);
        }(e.target, t);
      });
    }, {
      root: (e = t._settings).container === document ? null : e.container,
      rootMargin: e.thresholds || e.threshold + "px"
    }), !0);
    var e;
  },
      N = ["IMG", "IFRAME"],
      C = function C(t, e) {
    return function (t) {
      return t.filter(function (t) {
        return !c(t);
      });
    }((n = t || function (t) {
      return t.container.querySelectorAll(t.elements_selector);
    }(e), Array.prototype.slice.call(n)));
    var n;
  },
      M = function M(t, e) {
    this._settings = function (t) {
      return _extends({}, r, t);
    }(t), this._loadingCount = 0, O(this), this.update(e);
  };

  return M.prototype = {
    update: function update(t) {
      var n,
          o = this,
          r = this._settings;
      (this._elements = C(t, r), !e && this._observer) ? (function (t) {
        return t.use_native && "loading" in HTMLImageElement.prototype;
      }(r) && ((n = this)._elements.forEach(function (t) {
        -1 !== N.indexOf(t.tagName) && (t.setAttribute("loading", "lazy"), z(t, n));
      }), this._elements = C(t, r)), this._elements.forEach(function (t) {
        o._observer.observe(t);
      })) : this.loadAll();
    },
    destroy: function destroy() {
      var t = this;
      this._observer && (this._elements.forEach(function (e) {
        t._observer.unobserve(e);
      }), this._observer = null), this._elements = null, this._settings = null;
    },
    load: function load(t, e) {
      z(t, this, e);
    },
    loadAll: function loadAll() {
      var t = this;

      this._elements.forEach(function (e) {
        A(e, t);
      });
    }
  }, t && function (t, e) {
    if (e) if (e.length) for (var n, o = 0; n = e[o]; o += 1) {
      a(t, n);
    } else a(t, e);
  }(M, window.lazyLoadOptions), M;
});

/***/ }),

/***/ "./js/libs/oldFiles/modernizr-3.6.0.min.js":
/*!*************************************************!*\
  !*** ./js/libs/oldFiles/modernizr-3.6.0.min.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*! modernizr 3.6.0 (Custom Build) | MIT */

/* https://modernizr.com/download/?-animation-cssanimations-csscolumns-customelements-flexbox-history-picture-pointerevents-postmessage-sizes-srcset-webgl-websockets-webworkers-addtest-atrule-domprefixes-hasevent-mq-prefixed-prefixedcss-prefixedcssvalue-prefixes-setclasses-shiv-testallprops-testprop-teststyles !*/
!function (e, t, n) {
  function r(e, t) {
    return _typeof(e) === t;
  }

  function o() {
    var e, t, n, o, i, a, s;

    for (var l in b) {
      if (b.hasOwnProperty(l)) {
        if (e = [], t = b[l], t.name && (e.push(t.name.toLowerCase()), t.options && t.options.aliases && t.options.aliases.length)) for (n = 0; n < t.options.aliases.length; n++) {
          e.push(t.options.aliases[n].toLowerCase());
        }

        for (o = r(t.fn, "function") ? t.fn() : t.fn, i = 0; i < e.length; i++) {
          a = e[i], s = a.split("."), 1 === s.length ? Modernizr[s[0]] = o : (!Modernizr[s[0]] || Modernizr[s[0]] instanceof Boolean || (Modernizr[s[0]] = new Boolean(Modernizr[s[0]])), Modernizr[s[0]][s[1]] = o), C.push((o ? "" : "no-") + s.join("-"));
        }
      }
    }
  }

  function i(e) {
    var t = _.className,
        n = Modernizr._config.classPrefix || "";

    if (T && (t = t.baseVal), Modernizr._config.enableJSClass) {
      var r = new RegExp("(^|\\s)" + n + "no-js(\\s|$)");
      t = t.replace(r, "$1" + n + "js$2");
    }

    Modernizr._config.enableClasses && (t += " " + n + e.join(" " + n), T ? _.className.baseVal = t : _.className = t);
  }

  function a(e, t) {
    if ("object" == _typeof(e)) for (var n in e) {
      z(e, n) && a(n, e[n]);
    } else {
      e = e.toLowerCase();
      var r = e.split("."),
          o = Modernizr[r[0]];
      if (2 == r.length && (o = o[r[1]]), "undefined" != typeof o) return Modernizr;
      t = "function" == typeof t ? t() : t, 1 == r.length ? Modernizr[r[0]] = t : (!Modernizr[r[0]] || Modernizr[r[0]] instanceof Boolean || (Modernizr[r[0]] = new Boolean(Modernizr[r[0]])), Modernizr[r[0]][r[1]] = t), i([(t && 0 != t ? "" : "no-") + r.join("-")]), Modernizr._trigger(e, t);
    }
    return Modernizr;
  }

  function s(e) {
    return e.replace(/([A-Z])/g, function (e, t) {
      return "-" + t.toLowerCase();
    }).replace(/^ms-/, "-ms-");
  }

  function l() {
    return "function" != typeof t.createElement ? t.createElement(arguments[0]) : T ? t.createElementNS.call(t, "http://www.w3.org/2000/svg", arguments[0]) : t.createElement.apply(t, arguments);
  }

  function u(e) {
    return e.replace(/([a-z])-([a-z])/g, function (e, t, n) {
      return t + n.toUpperCase();
    }).replace(/^-/, "");
  }

  function c(e, t) {
    return !!~("" + e).indexOf(t);
  }

  function f() {
    var e = t.body;
    return e || (e = l(T ? "svg" : "body"), e.fake = !0), e;
  }

  function d(e, n, r, o) {
    var i,
        a,
        s,
        u,
        c = "modernizr",
        d = l("div"),
        p = f();
    if (parseInt(r, 10)) for (; r--;) {
      s = l("div"), s.id = o ? o[r] : c + (r + 1), d.appendChild(s);
    }
    return i = l("style"), i.type = "text/css", i.id = "s" + c, (p.fake ? p : d).appendChild(i), p.appendChild(d), i.styleSheet ? i.styleSheet.cssText = e : i.appendChild(t.createTextNode(e)), d.id = c, p.fake && (p.style.background = "", p.style.overflow = "hidden", u = _.style.overflow, _.style.overflow = "hidden", _.appendChild(p)), a = n(d, e), p.fake ? (p.parentNode.removeChild(p), _.style.overflow = u, _.offsetHeight) : d.parentNode.removeChild(d), !!a;
  }

  function p(e, t) {
    return function () {
      return e.apply(t, arguments);
    };
  }

  function m(e, t, n) {
    var o;

    for (var i in e) {
      if (e[i] in t) return n === !1 ? e[i] : (o = t[e[i]], r(o, "function") ? p(o, n || t) : o);
    }

    return !1;
  }

  function h(t, n, r) {
    var o;

    if ("getComputedStyle" in e) {
      o = getComputedStyle.call(e, t, n);
      var i = e.console;
      if (null !== o) r && (o = o.getPropertyValue(r));else if (i) {
        var a = i.error ? "error" : "log";
        i[a].call(i, "getComputedStyle returning null, its possible modernizr test results are inaccurate");
      }
    } else o = !n && t.currentStyle && t.currentStyle[r];

    return o;
  }

  function v(t, r) {
    var o = t.length;

    if ("CSS" in e && "supports" in e.CSS) {
      for (; o--;) {
        if (e.CSS.supports(s(t[o]), r)) return !0;
      }

      return !1;
    }

    if ("CSSSupportsRule" in e) {
      for (var i = []; o--;) {
        i.push("(" + s(t[o]) + ":" + r + ")");
      }

      return i = i.join(" or "), d("@supports (" + i + ") { #modernizr { position: absolute; } }", function (e) {
        return "absolute" == h(e, null, "position");
      });
    }

    return n;
  }

  function g(e, t, o, i) {
    function a() {
      f && (delete R.style, delete R.modElem);
    }

    if (i = r(i, "undefined") ? !1 : i, !r(o, "undefined")) {
      var s = v(e, o);
      if (!r(s, "undefined")) return s;
    }

    for (var f, d, p, m, h, g = ["modernizr", "tspan", "samp"]; !R.style && g.length;) {
      f = !0, R.modElem = l(g.shift()), R.style = R.modElem.style;
    }

    for (p = e.length, d = 0; p > d; d++) {
      if (m = e[d], h = R.style[m], c(m, "-") && (m = u(m)), R.style[m] !== n) {
        if (i || r(o, "undefined")) return a(), "pfx" == t ? m : !0;

        try {
          R.style[m] = o;
        } catch (y) {}

        if (R.style[m] != h) return a(), "pfx" == t ? m : !0;
      }
    }

    return a(), !1;
  }

  function y(e, t, n, o, i) {
    var a = e.charAt(0).toUpperCase() + e.slice(1),
        s = (e + " " + B.join(a + " ") + a).split(" ");
    return r(t, "string") || r(t, "undefined") ? g(s, t, o, i) : (s = (e + " " + P.join(a + " ") + a).split(" "), m(s, t, n));
  }

  function A(e, t, r) {
    return y(e, n, n, t, r);
  }

  var C = [],
      b = [],
      S = {
    _version: "3.6.0",
    _config: {
      classPrefix: "",
      enableClasses: !0,
      enableJSClass: !0,
      usePrefixes: !0
    },
    _q: [],
    on: function on(e, t) {
      var n = this;
      setTimeout(function () {
        t(n[e]);
      }, 0);
    },
    addTest: function addTest(e, t, n) {
      b.push({
        name: e,
        fn: t,
        options: n
      });
    },
    addAsyncTest: function addAsyncTest(e) {
      b.push({
        name: null,
        fn: e
      });
    }
  },
      Modernizr = function Modernizr() {};

  Modernizr.prototype = S, Modernizr = new Modernizr();
  var w = !1;

  try {
    w = "WebSocket" in e && 2 === e.WebSocket.CLOSING;
  } catch (x) {}

  Modernizr.addTest("websockets", w), Modernizr.addTest("customelements", "customElements" in e), Modernizr.addTest("history", function () {
    var t = navigator.userAgent;
    return -1 === t.indexOf("Android 2.") && -1 === t.indexOf("Android 4.0") || -1 === t.indexOf("Mobile Safari") || -1 !== t.indexOf("Chrome") || -1 !== t.indexOf("Windows Phone") || "file:" === location.protocol ? e.history && "pushState" in e.history : !1;
  }), Modernizr.addTest("postmessage", "postMessage" in e), Modernizr.addTest("webworkers", "Worker" in e), Modernizr.addTest("picture", "HTMLPictureElement" in e);
  var E = S._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : ["", ""];
  S._prefixes = E;

  var _ = t.documentElement,
      T = "svg" === _.nodeName.toLowerCase();

  T || !function (e, t) {
    function n(e, t) {
      var n = e.createElement("p"),
          r = e.getElementsByTagName("head")[0] || e.documentElement;
      return n.innerHTML = "x<style>" + t + "</style>", r.insertBefore(n.lastChild, r.firstChild);
    }

    function r() {
      var e = A.elements;
      return "string" == typeof e ? e.split(" ") : e;
    }

    function o(e, t) {
      var n = A.elements;
      "string" != typeof n && (n = n.join(" ")), "string" != typeof e && (e = e.join(" ")), A.elements = n + " " + e, u(t);
    }

    function i(e) {
      var t = y[e[v]];
      return t || (t = {}, g++, e[v] = g, y[g] = t), t;
    }

    function a(e, n, r) {
      if (n || (n = t), f) return n.createElement(e);
      r || (r = i(n));
      var o;
      return o = r.cache[e] ? r.cache[e].cloneNode() : h.test(e) ? (r.cache[e] = r.createElem(e)).cloneNode() : r.createElem(e), !o.canHaveChildren || m.test(e) || o.tagUrn ? o : r.frag.appendChild(o);
    }

    function s(e, n) {
      if (e || (e = t), f) return e.createDocumentFragment();
      n = n || i(e);

      for (var o = n.frag.cloneNode(), a = 0, s = r(), l = s.length; l > a; a++) {
        o.createElement(s[a]);
      }

      return o;
    }

    function l(e, t) {
      t.cache || (t.cache = {}, t.createElem = e.createElement, t.createFrag = e.createDocumentFragment, t.frag = t.createFrag()), e.createElement = function (n) {
        return A.shivMethods ? a(n, e, t) : t.createElem(n);
      }, e.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + r().join().replace(/[\w\-:]+/g, function (e) {
        return t.createElem(e), t.frag.createElement(e), 'c("' + e + '")';
      }) + ");return n}")(A, t.frag);
    }

    function u(e) {
      e || (e = t);
      var r = i(e);
      return !A.shivCSS || c || r.hasCSS || (r.hasCSS = !!n(e, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), f || l(e, r), e;
    }

    var c,
        f,
        d = "3.7.3",
        p = e.html5 || {},
        m = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
        h = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
        v = "_html5shiv",
        g = 0,
        y = {};
    !function () {
      try {
        var e = t.createElement("a");
        e.innerHTML = "<xyz></xyz>", c = "hidden" in e, f = 1 == e.childNodes.length || function () {
          t.createElement("a");
          var e = t.createDocumentFragment();
          return "undefined" == typeof e.cloneNode || "undefined" == typeof e.createDocumentFragment || "undefined" == typeof e.createElement;
        }();
      } catch (n) {
        c = !0, f = !0;
      }
    }();
    var A = {
      elements: p.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output picture progress section summary template time video",
      version: d,
      shivCSS: p.shivCSS !== !1,
      supportsUnknownElements: f,
      shivMethods: p.shivMethods !== !1,
      type: "default",
      shivDocument: u,
      createElement: a,
      createDocumentFragment: s,
      addElements: o
    };
    e.html5 = A, u(t), "object" == ( false ? undefined : _typeof(module)) && module.exports && (module.exports = A);
  }("undefined" != typeof e ? e : this, t);
  var k = "Moz O ms Webkit",
      P = S._config.usePrefixes ? k.toLowerCase().split(" ") : [];
  S._domPrefixes = P;
  var z;
  !function () {
    var e = {}.hasOwnProperty;
    z = r(e, "undefined") || r(e.call, "undefined") ? function (e, t) {
      return t in e && r(e.constructor.prototype[t], "undefined");
    } : function (t, n) {
      return e.call(t, n);
    };
  }(), S._l = {}, S.on = function (e, t) {
    this._l[e] || (this._l[e] = []), this._l[e].push(t), Modernizr.hasOwnProperty(e) && setTimeout(function () {
      Modernizr._trigger(e, Modernizr[e]);
    }, 0);
  }, S._trigger = function (e, t) {
    if (this._l[e]) {
      var n = this._l[e];
      setTimeout(function () {
        var e, r;

        for (e = 0; e < n.length; e++) {
          (r = n[e])(t);
        }
      }, 0), delete this._l[e];
    }
  }, Modernizr._q.push(function () {
    S.addTest = a;
  });
  var B = S._config.usePrefixes ? k.split(" ") : [];
  S._cssomPrefixes = B;

  var N = function N(t) {
    var r,
        o = E.length,
        i = e.CSSRule;
    if ("undefined" == typeof i) return n;
    if (!t) return !1;
    if (t = t.replace(/^@/, ""), r = t.replace(/-/g, "_").toUpperCase() + "_RULE", r in i) return "@" + t;

    for (var a = 0; o > a; a++) {
      var s = E[a],
          l = s.toUpperCase() + "_" + r;
      if (l in i) return "@-" + s.toLowerCase() + "-" + t;
    }

    return !1;
  };

  S.atRule = N;

  var O = function () {
    function e(e, t) {
      var o;
      return e ? (t && "string" != typeof t || (t = l(t || "div")), e = "on" + e, o = e in t, !o && r && (t.setAttribute || (t = l("div")), t.setAttribute(e, ""), o = "function" == typeof t[e], t[e] !== n && (t[e] = n), t.removeAttribute(e)), o) : !1;
    }

    var r = !("onblur" in t.documentElement);
    return e;
  }();

  S.hasEvent = O, Modernizr.addTest("pointerevents", function () {
    var e = !1,
        t = P.length;

    for (e = Modernizr.hasEvent("pointerdown"); t-- && !e;) {
      O(P[t] + "pointerdown") && (e = !0);
    }

    return e;
  });

  var L = function L(e, t) {
    var n = !1,
        r = l("div"),
        o = r.style;

    if (e in o) {
      var i = P.length;

      for (o[e] = t, n = o[e]; i-- && !n;) {
        o[e] = "-" + P[i] + "-" + t, n = o[e];
      }
    }

    return "" === n && (n = !1), n;
  };

  S.prefixedCSSValue = L, Modernizr.addTest("webanimations", "animate" in l("div")), Modernizr.addTest("webgl", function () {
    var t = l("canvas"),
        n = "probablySupportsContext" in t ? "probablySupportsContext" : "supportsContext";
    return n in t ? t[n]("webgl") || t[n]("experimental-webgl") : "WebGLRenderingContext" in e;
  }), Modernizr.addAsyncTest(function () {
    var e,
        t,
        n,
        r = l("img"),
        o = ("sizes" in r);
    !o && "srcset" in r ? (t = "data:image/gif;base64,R0lGODlhAgABAPAAAP///wAAACH5BAAAAAAALAAAAAACAAEAAAICBAoAOw==", e = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==", n = function n() {
      a("sizes", 2 == r.width);
    }, r.onload = n, r.onerror = n, r.setAttribute("sizes", "9px"), r.srcset = e + " 1w," + t + " 8w", r.src = e) : a("sizes", o);
  }), Modernizr.addTest("srcset", "srcset" in l("img"));

  var j = function () {
    var t = e.matchMedia || e.msMatchMedia;
    return t ? function (e) {
      var n = t(e);
      return n && n.matches || !1;
    } : function (t) {
      var n = !1;
      return d("@media " + t + " { #modernizr { position: absolute; } }", function (t) {
        n = "absolute" == (e.getComputedStyle ? e.getComputedStyle(t, null) : t.currentStyle).position;
      }), n;
    };
  }();

  S.mq = j;
  var M = (S.testStyles = d, {
    elem: l("modernizr")
  });

  Modernizr._q.push(function () {
    delete M.elem;
  });

  var R = {
    style: M.elem.style
  };

  Modernizr._q.unshift(function () {
    delete R.style;
  });

  S.testProp = function (e, t, r) {
    return g([e], n, t, r);
  };

  S.testAllProps = y;

  var F = S.prefixed = function (e, t, n) {
    return 0 === e.indexOf("@") ? N(e) : (-1 != e.indexOf("-") && (e = u(e)), t ? y(e, t, n) : y(e, "pfx"));
  };

  S.prefixedCSS = function (e) {
    var t = F(e);
    return t && s(t);
  };

  S.testAllProps = A, Modernizr.addTest("flexbox", A("flexBasis", "1px", !0)), Modernizr.addTest("cssanimations", A("animationName", "a", !0)), function () {
    Modernizr.addTest("csscolumns", function () {
      var e = !1,
          t = A("columnCount");

      try {
        e = !!t, e && (e = new Boolean(e));
      } catch (n) {}

      return e;
    });

    for (var e, t, n = ["Width", "Span", "Fill", "Gap", "Rule", "RuleColor", "RuleStyle", "RuleWidth", "BreakBefore", "BreakAfter", "BreakInside"], r = 0; r < n.length; r++) {
      e = n[r].toLowerCase(), t = A("column" + n[r]), ("breakbefore" === e || "breakafter" === e || "breakinside" == e) && (t = t || A(n[r])), Modernizr.addTest("csscolumns." + e, t);
    }
  }(), o(), i(C), delete S.addTest, delete S.addAsyncTest;

  for (var D = 0; D < Modernizr._q.length; D++) {
    Modernizr._q[D]();
  }

  e.Modernizr = Modernizr;
}(window, document);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack-stream/node_modules/webpack/buildin/module.js */ "./node_modules/webpack-stream/node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "./js/libs/oldFiles/popper.js":
/*!************************************!*\
  !*** ./js/libs/oldFiles/popper.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*
// Copyright (C) Federico Zivolo 2019
// Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
*/
(function (e, t) {
  'object' == ( false ? undefined : _typeof(exports)) && 'undefined' != typeof module ? module.exports = t() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
})(this, function () {
  'use strict';

  function e(e) {
    return e && '[object Function]' === {}.toString.call(e);
  }

  function t(e, t) {
    if (1 !== e.nodeType) return [];
    var o = e.ownerDocument.defaultView,
        n = o.getComputedStyle(e, null);
    return t ? n[t] : n;
  }

  function o(e) {
    return 'HTML' === e.nodeName ? e : e.parentNode || e.host;
  }

  function n(e) {
    if (!e) return document.body;

    switch (e.nodeName) {
      case 'HTML':
      case 'BODY':
        return e.ownerDocument.body;

      case '#document':
        return e.body;
    }

    var i = t(e),
        r = i.overflow,
        p = i.overflowX,
        s = i.overflowY;
    return /(auto|scroll|overlay)/.test(r + s + p) ? e : n(o(e));
  }

  function r(e) {
    return 11 === e ? pe : 10 === e ? se : pe || se;
  }

  function p(e) {
    if (!e) return document.documentElement;

    for (var o = r(10) ? document.body : null, n = e.offsetParent || null; n === o && e.nextElementSibling;) {
      n = (e = e.nextElementSibling).offsetParent;
    }

    var i = n && n.nodeName;
    return i && 'BODY' !== i && 'HTML' !== i ? -1 !== ['TH', 'TD', 'TABLE'].indexOf(n.nodeName) && 'static' === t(n, 'position') ? p(n) : n : e ? e.ownerDocument.documentElement : document.documentElement;
  }

  function s(e) {
    var t = e.nodeName;
    return 'BODY' !== t && ('HTML' === t || p(e.firstElementChild) === e);
  }

  function d(e) {
    return null === e.parentNode ? e : d(e.parentNode);
  }

  function a(e, t) {
    if (!e || !e.nodeType || !t || !t.nodeType) return document.documentElement;
    var o = e.compareDocumentPosition(t) & Node.DOCUMENT_POSITION_FOLLOWING,
        n = o ? e : t,
        i = o ? t : e,
        r = document.createRange();
    r.setStart(n, 0), r.setEnd(i, 0);
    var l = r.commonAncestorContainer;
    if (e !== l && t !== l || n.contains(i)) return s(l) ? l : p(l);
    var f = d(e);
    return f.host ? a(f.host, t) : a(e, d(t).host);
  }

  function l(e) {
    var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : 'top',
        o = 'top' === t ? 'scrollTop' : 'scrollLeft',
        n = e.nodeName;

    if ('BODY' === n || 'HTML' === n) {
      var i = e.ownerDocument.documentElement,
          r = e.ownerDocument.scrollingElement || i;
      return r[o];
    }

    return e[o];
  }

  function f(e, t) {
    var o = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
        n = l(t, 'top'),
        i = l(t, 'left'),
        r = o ? -1 : 1;
    return e.top += n * r, e.bottom += n * r, e.left += i * r, e.right += i * r, e;
  }

  function m(e, t) {
    var o = 'x' === t ? 'Left' : 'Top',
        n = 'Left' == o ? 'Right' : 'Bottom';
    return parseFloat(e['border' + o + 'Width'], 10) + parseFloat(e['border' + n + 'Width'], 10);
  }

  function h(e, t, o, n) {
    return ee(t['offset' + e], t['scroll' + e], o['client' + e], o['offset' + e], o['scroll' + e], r(10) ? parseInt(o['offset' + e]) + parseInt(n['margin' + ('Height' === e ? 'Top' : 'Left')]) + parseInt(n['margin' + ('Height' === e ? 'Bottom' : 'Right')]) : 0);
  }

  function c(e) {
    var t = e.body,
        o = e.documentElement,
        n = r(10) && getComputedStyle(o);
    return {
      height: h('Height', t, o, n),
      width: h('Width', t, o, n)
    };
  }

  function g(e) {
    return fe({}, e, {
      right: e.left + e.width,
      bottom: e.top + e.height
    });
  }

  function u(e) {
    var o = {};

    try {
      if (r(10)) {
        o = e.getBoundingClientRect();
        var n = l(e, 'top'),
            i = l(e, 'left');
        o.top += n, o.left += i, o.bottom += n, o.right += i;
      } else o = e.getBoundingClientRect();
    } catch (t) {}

    var p = {
      left: o.left,
      top: o.top,
      width: o.right - o.left,
      height: o.bottom - o.top
    },
        s = 'HTML' === e.nodeName ? c(e.ownerDocument) : {},
        d = s.width || e.clientWidth || p.right - p.left,
        a = s.height || e.clientHeight || p.bottom - p.top,
        f = e.offsetWidth - d,
        h = e.offsetHeight - a;

    if (f || h) {
      var u = t(e);
      f -= m(u, 'x'), h -= m(u, 'y'), p.width -= f, p.height -= h;
    }

    return g(p);
  }

  function b(e, o) {
    var i = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
        p = r(10),
        s = 'HTML' === o.nodeName,
        d = u(e),
        a = u(o),
        l = n(e),
        m = t(o),
        h = parseFloat(m.borderTopWidth, 10),
        c = parseFloat(m.borderLeftWidth, 10);
    i && s && (a.top = ee(a.top, 0), a.left = ee(a.left, 0));
    var b = g({
      top: d.top - a.top - h,
      left: d.left - a.left - c,
      width: d.width,
      height: d.height
    });

    if (b.marginTop = 0, b.marginLeft = 0, !p && s) {
      var w = parseFloat(m.marginTop, 10),
          y = parseFloat(m.marginLeft, 10);
      b.top -= h - w, b.bottom -= h - w, b.left -= c - y, b.right -= c - y, b.marginTop = w, b.marginLeft = y;
    }

    return (p && !i ? o.contains(l) : o === l && 'BODY' !== l.nodeName) && (b = f(b, o)), b;
  }

  function w(e) {
    var t = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
        o = e.ownerDocument.documentElement,
        n = b(e, o),
        i = ee(o.clientWidth, window.innerWidth || 0),
        r = ee(o.clientHeight, window.innerHeight || 0),
        p = t ? 0 : l(o),
        s = t ? 0 : l(o, 'left'),
        d = {
      top: p - n.top + n.marginTop,
      left: s - n.left + n.marginLeft,
      width: i,
      height: r
    };
    return g(d);
  }

  function y(e) {
    var n = e.nodeName;
    if ('BODY' === n || 'HTML' === n) return !1;
    if ('fixed' === t(e, 'position')) return !0;
    var i = o(e);
    return !!i && y(i);
  }

  function E(e) {
    if (!e || !e.parentElement || r()) return document.documentElement;

    for (var o = e.parentElement; o && 'none' === t(o, 'transform');) {
      o = o.parentElement;
    }

    return o || document.documentElement;
  }

  function v(e, t, i, r) {
    var p = 4 < arguments.length && void 0 !== arguments[4] && arguments[4],
        s = {
      top: 0,
      left: 0
    },
        d = p ? E(e) : a(e, t);
    if ('viewport' === r) s = w(d, p);else {
      var l;
      'scrollParent' === r ? (l = n(o(t)), 'BODY' === l.nodeName && (l = e.ownerDocument.documentElement)) : 'window' === r ? l = e.ownerDocument.documentElement : l = r;
      var f = b(l, d, p);

      if ('HTML' === l.nodeName && !y(d)) {
        var m = c(e.ownerDocument),
            h = m.height,
            g = m.width;
        s.top += f.top - f.marginTop, s.bottom = h + f.top, s.left += f.left - f.marginLeft, s.right = g + f.left;
      } else s = f;
    }
    i = i || 0;
    var u = 'number' == typeof i;
    return s.left += u ? i : i.left || 0, s.top += u ? i : i.top || 0, s.right -= u ? i : i.right || 0, s.bottom -= u ? i : i.bottom || 0, s;
  }

  function x(e) {
    var t = e.width,
        o = e.height;
    return t * o;
  }

  function O(e, t, o, n, i) {
    var r = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
    if (-1 === e.indexOf('auto')) return e;
    var p = v(o, n, r, i),
        s = {
      top: {
        width: p.width,
        height: t.top - p.top
      },
      right: {
        width: p.right - t.right,
        height: p.height
      },
      bottom: {
        width: p.width,
        height: p.bottom - t.bottom
      },
      left: {
        width: t.left - p.left,
        height: p.height
      }
    },
        d = Object.keys(s).map(function (e) {
      return fe({
        key: e
      }, s[e], {
        area: x(s[e])
      });
    }).sort(function (e, t) {
      return t.area - e.area;
    }),
        a = d.filter(function (e) {
      var t = e.width,
          n = e.height;
      return t >= o.clientWidth && n >= o.clientHeight;
    }),
        l = 0 < a.length ? a[0].key : d[0].key,
        f = e.split('-')[1];
    return l + (f ? '-' + f : '');
  }

  function L(e, t, o) {
    var n = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : null,
        i = n ? E(t) : a(t, o);
    return b(o, i, n);
  }

  function S(e) {
    var t = e.ownerDocument.defaultView,
        o = t.getComputedStyle(e),
        n = parseFloat(o.marginTop || 0) + parseFloat(o.marginBottom || 0),
        i = parseFloat(o.marginLeft || 0) + parseFloat(o.marginRight || 0),
        r = {
      width: e.offsetWidth + i,
      height: e.offsetHeight + n
    };
    return r;
  }

  function T(e) {
    var t = {
      left: 'right',
      right: 'left',
      bottom: 'top',
      top: 'bottom'
    };
    return e.replace(/left|right|bottom|top/g, function (e) {
      return t[e];
    });
  }

  function D(e, t, o) {
    o = o.split('-')[0];
    var n = S(e),
        i = {
      width: n.width,
      height: n.height
    },
        r = -1 !== ['right', 'left'].indexOf(o),
        p = r ? 'top' : 'left',
        s = r ? 'left' : 'top',
        d = r ? 'height' : 'width',
        a = r ? 'width' : 'height';
    return i[p] = t[p] + t[d] / 2 - n[d] / 2, i[s] = o === s ? t[s] - n[a] : t[T(s)], i;
  }

  function C(e, t) {
    return Array.prototype.find ? e.find(t) : e.filter(t)[0];
  }

  function N(e, t, o) {
    if (Array.prototype.findIndex) return e.findIndex(function (e) {
      return e[t] === o;
    });
    var n = C(e, function (e) {
      return e[t] === o;
    });
    return e.indexOf(n);
  }

  function P(t, o, n) {
    var i = void 0 === n ? t : t.slice(0, N(t, 'name', n));
    return i.forEach(function (t) {
      t['function'] && console.warn('`modifier.function` is deprecated, use `modifier.fn`!');
      var n = t['function'] || t.fn;
      t.enabled && e(n) && (o.offsets.popper = g(o.offsets.popper), o.offsets.reference = g(o.offsets.reference), o = n(o, t));
    }), o;
  }

  function k() {
    if (!this.state.isDestroyed) {
      var e = {
        instance: this,
        styles: {},
        arrowStyles: {},
        attributes: {},
        flipped: !1,
        offsets: {}
      };
      e.offsets.reference = L(this.state, this.popper, this.reference, this.options.positionFixed), e.placement = O(this.options.placement, e.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), e.originalPlacement = e.placement, e.positionFixed = this.options.positionFixed, e.offsets.popper = D(this.popper, e.offsets.reference, e.placement), e.offsets.popper.position = this.options.positionFixed ? 'fixed' : 'absolute', e = P(this.modifiers, e), this.state.isCreated ? this.options.onUpdate(e) : (this.state.isCreated = !0, this.options.onCreate(e));
    }
  }

  function W(e, t) {
    return e.some(function (e) {
      var o = e.name,
          n = e.enabled;
      return n && o === t;
    });
  }

  function H(e) {
    for (var t = [!1, 'ms', 'Webkit', 'Moz', 'O'], o = e.charAt(0).toUpperCase() + e.slice(1), n = 0; n < t.length; n++) {
      var i = t[n],
          r = i ? '' + i + o : e;
      if ('undefined' != typeof document.body.style[r]) return r;
    }

    return null;
  }

  function B() {
    return this.state.isDestroyed = !0, W(this.modifiers, 'applyStyle') && (this.popper.removeAttribute('x-placement'), this.popper.style.position = '', this.popper.style.top = '', this.popper.style.left = '', this.popper.style.right = '', this.popper.style.bottom = '', this.popper.style.willChange = '', this.popper.style[H('transform')] = ''), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this;
  }

  function A(e) {
    var t = e.ownerDocument;
    return t ? t.defaultView : window;
  }

  function M(e, t, o, i) {
    var r = 'BODY' === e.nodeName,
        p = r ? e.ownerDocument.defaultView : e;
    p.addEventListener(t, o, {
      passive: !0
    }), r || M(n(p.parentNode), t, o, i), i.push(p);
  }

  function F(e, t, o, i) {
    o.updateBound = i, A(e).addEventListener('resize', o.updateBound, {
      passive: !0
    });
    var r = n(e);
    return M(r, 'scroll', o.updateBound, o.scrollParents), o.scrollElement = r, o.eventsEnabled = !0, o;
  }

  function I() {
    this.state.eventsEnabled || (this.state = F(this.reference, this.options, this.state, this.scheduleUpdate));
  }

  function R(e, t) {
    return A(e).removeEventListener('resize', t.updateBound), t.scrollParents.forEach(function (e) {
      e.removeEventListener('scroll', t.updateBound);
    }), t.updateBound = null, t.scrollParents = [], t.scrollElement = null, t.eventsEnabled = !1, t;
  }

  function U() {
    this.state.eventsEnabled && (cancelAnimationFrame(this.scheduleUpdate), this.state = R(this.reference, this.state));
  }

  function Y(e) {
    return '' !== e && !isNaN(parseFloat(e)) && isFinite(e);
  }

  function j(e, t) {
    Object.keys(t).forEach(function (o) {
      var n = '';
      -1 !== ['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(o) && Y(t[o]) && (n = 'px'), e.style[o] = t[o] + n;
    });
  }

  function V(e, t) {
    Object.keys(t).forEach(function (o) {
      var n = t[o];
      !1 === n ? e.removeAttribute(o) : e.setAttribute(o, t[o]);
    });
  }

  function q(e, t) {
    var o = e.offsets,
        n = o.popper,
        i = o.reference,
        r = $,
        p = function p(e) {
      return e;
    },
        s = r(i.width),
        d = r(n.width),
        a = -1 !== ['left', 'right'].indexOf(e.placement),
        l = -1 !== e.placement.indexOf('-'),
        f = t ? a || l || s % 2 == d % 2 ? r : Z : p,
        m = t ? r : p;

    return {
      left: f(1 == s % 2 && 1 == d % 2 && !l && t ? n.left - 1 : n.left),
      top: m(n.top),
      bottom: m(n.bottom),
      right: f(n.right)
    };
  }

  function K(e, t, o) {
    var n = C(e, function (e) {
      var o = e.name;
      return o === t;
    }),
        i = !!n && e.some(function (e) {
      return e.name === o && e.enabled && e.order < n.order;
    });

    if (!i) {
      var r = '`' + t + '`';
      console.warn('`' + o + '`' + ' modifier is required by ' + r + ' modifier in order to work, be sure to include it before ' + r + '!');
    }

    return i;
  }

  function z(e) {
    return 'end' === e ? 'start' : 'start' === e ? 'end' : e;
  }

  function G(e) {
    var t = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
        o = ce.indexOf(e),
        n = ce.slice(o + 1).concat(ce.slice(0, o));
    return t ? n.reverse() : n;
  }

  function _(e, t, o, n) {
    var i = e.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
        r = +i[1],
        p = i[2];
    if (!r) return e;

    if (0 === p.indexOf('%')) {
      var s;

      switch (p) {
        case '%p':
          s = o;
          break;

        case '%':
        case '%r':
        default:
          s = n;
      }

      var d = g(s);
      return d[t] / 100 * r;
    }

    if ('vh' === p || 'vw' === p) {
      var a;
      return a = 'vh' === p ? ee(document.documentElement.clientHeight, window.innerHeight || 0) : ee(document.documentElement.clientWidth, window.innerWidth || 0), a / 100 * r;
    }

    return r;
  }

  function X(e, t, o, n) {
    var i = [0, 0],
        r = -1 !== ['right', 'left'].indexOf(n),
        p = e.split(/(\+|\-)/).map(function (e) {
      return e.trim();
    }),
        s = p.indexOf(C(p, function (e) {
      return -1 !== e.search(/,|\s/);
    }));
    p[s] && -1 === p[s].indexOf(',') && console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');
    var d = /\s*,\s*|\s+/,
        a = -1 === s ? [p] : [p.slice(0, s).concat([p[s].split(d)[0]]), [p[s].split(d)[1]].concat(p.slice(s + 1))];
    return a = a.map(function (e, n) {
      var i = (1 === n ? !r : r) ? 'height' : 'width',
          p = !1;
      return e.reduce(function (e, t) {
        return '' === e[e.length - 1] && -1 !== ['+', '-'].indexOf(t) ? (e[e.length - 1] = t, p = !0, e) : p ? (e[e.length - 1] += t, p = !1, e) : e.concat(t);
      }, []).map(function (e) {
        return _(e, i, t, o);
      });
    }), a.forEach(function (e, t) {
      e.forEach(function (o, n) {
        Y(o) && (i[t] += o * ('-' === e[n - 1] ? -1 : 1));
      });
    }), i;
  }

  function J(e, t) {
    var o,
        n = t.offset,
        i = e.placement,
        r = e.offsets,
        p = r.popper,
        s = r.reference,
        d = i.split('-')[0];
    return o = Y(+n) ? [+n, 0] : X(n, p, s, d), 'left' === d ? (p.top += o[0], p.left -= o[1]) : 'right' === d ? (p.top += o[0], p.left += o[1]) : 'top' === d ? (p.left += o[0], p.top -= o[1]) : 'bottom' === d && (p.left += o[0], p.top += o[1]), e.popper = p, e;
  }

  for (var Q = Math.min, Z = Math.floor, $ = Math.round, ee = Math.max, te = 'undefined' != typeof window && 'undefined' != typeof document, oe = ['Edge', 'Trident', 'Firefox'], ne = 0, ie = 0; ie < oe.length; ie += 1) {
    if (te && 0 <= navigator.userAgent.indexOf(oe[ie])) {
      ne = 1;
      break;
    }
  }

  var i = te && window.Promise,
      re = i ? function (e) {
    var t = !1;
    return function () {
      t || (t = !0, window.Promise.resolve().then(function () {
        t = !1, e();
      }));
    };
  } : function (e) {
    var t = !1;
    return function () {
      t || (t = !0, setTimeout(function () {
        t = !1, e();
      }, ne));
    };
  },
      pe = te && !!(window.MSInputMethodContext && document.documentMode),
      se = te && /MSIE 10/.test(navigator.userAgent),
      de = function de(e, t) {
    if (!(e instanceof t)) throw new TypeError('Cannot call a class as a function');
  },
      ae = function () {
    function e(e, t) {
      for (var o, n = 0; n < t.length; n++) {
        o = t[n], o.enumerable = o.enumerable || !1, o.configurable = !0, 'value' in o && (o.writable = !0), Object.defineProperty(e, o.key, o);
      }
    }

    return function (t, o, n) {
      return o && e(t.prototype, o), n && e(t, n), t;
    };
  }(),
      le = function le(e, t, o) {
    return t in e ? Object.defineProperty(e, t, {
      value: o,
      enumerable: !0,
      configurable: !0,
      writable: !0
    }) : e[t] = o, e;
  },
      fe = Object.assign || function (e) {
    for (var t, o = 1; o < arguments.length; o++) {
      for (var n in t = arguments[o], t) {
        Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
      }
    }

    return e;
  },
      me = te && /Firefox/i.test(navigator.userAgent),
      he = ['auto-start', 'auto', 'auto-end', 'top-start', 'top', 'top-end', 'right-start', 'right', 'right-end', 'bottom-end', 'bottom', 'bottom-start', 'left-end', 'left', 'left-start'],
      ce = he.slice(3),
      ge = {
    FLIP: 'flip',
    CLOCKWISE: 'clockwise',
    COUNTERCLOCKWISE: 'counterclockwise'
  },
      ue = function () {
    function t(o, n) {
      var i = this,
          r = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
      de(this, t), this.scheduleUpdate = function () {
        return requestAnimationFrame(i.update);
      }, this.update = re(this.update.bind(this)), this.options = fe({}, t.Defaults, r), this.state = {
        isDestroyed: !1,
        isCreated: !1,
        scrollParents: []
      }, this.reference = o && o.jquery ? o[0] : o, this.popper = n && n.jquery ? n[0] : n, this.options.modifiers = {}, Object.keys(fe({}, t.Defaults.modifiers, r.modifiers)).forEach(function (e) {
        i.options.modifiers[e] = fe({}, t.Defaults.modifiers[e] || {}, r.modifiers ? r.modifiers[e] : {});
      }), this.modifiers = Object.keys(this.options.modifiers).map(function (e) {
        return fe({
          name: e
        }, i.options.modifiers[e]);
      }).sort(function (e, t) {
        return e.order - t.order;
      }), this.modifiers.forEach(function (t) {
        t.enabled && e(t.onLoad) && t.onLoad(i.reference, i.popper, i.options, t, i.state);
      }), this.update();
      var p = this.options.eventsEnabled;
      p && this.enableEventListeners(), this.state.eventsEnabled = p;
    }

    return ae(t, [{
      key: 'update',
      value: function value() {
        return k.call(this);
      }
    }, {
      key: 'destroy',
      value: function value() {
        return B.call(this);
      }
    }, {
      key: 'enableEventListeners',
      value: function value() {
        return I.call(this);
      }
    }, {
      key: 'disableEventListeners',
      value: function value() {
        return U.call(this);
      }
    }]), t;
  }();

  return ue.Utils = ('undefined' == typeof window ? global : window).PopperUtils, ue.placements = he, ue.Defaults = {
    placement: 'bottom',
    positionFixed: !1,
    eventsEnabled: !0,
    removeOnDestroy: !1,
    onCreate: function onCreate() {},
    onUpdate: function onUpdate() {},
    modifiers: {
      shift: {
        order: 100,
        enabled: !0,
        fn: function fn(e) {
          var t = e.placement,
              o = t.split('-')[0],
              n = t.split('-')[1];

          if (n) {
            var i = e.offsets,
                r = i.reference,
                p = i.popper,
                s = -1 !== ['bottom', 'top'].indexOf(o),
                d = s ? 'left' : 'top',
                a = s ? 'width' : 'height',
                l = {
              start: le({}, d, r[d]),
              end: le({}, d, r[d] + r[a] - p[a])
            };
            e.offsets.popper = fe({}, p, l[n]);
          }

          return e;
        }
      },
      offset: {
        order: 200,
        enabled: !0,
        fn: J,
        offset: 0
      },
      preventOverflow: {
        order: 300,
        enabled: !0,
        fn: function fn(e, t) {
          var o = t.boundariesElement || p(e.instance.popper);
          e.instance.reference === o && (o = p(o));
          var n = H('transform'),
              i = e.instance.popper.style,
              r = i.top,
              s = i.left,
              d = i[n];
          i.top = '', i.left = '', i[n] = '';
          var a = v(e.instance.popper, e.instance.reference, t.padding, o, e.positionFixed);
          i.top = r, i.left = s, i[n] = d, t.boundaries = a;
          var l = t.priority,
              f = e.offsets.popper,
              m = {
            primary: function primary(e) {
              var o = f[e];
              return f[e] < a[e] && !t.escapeWithReference && (o = ee(f[e], a[e])), le({}, e, o);
            },
            secondary: function secondary(e) {
              var o = 'right' === e ? 'left' : 'top',
                  n = f[o];
              return f[e] > a[e] && !t.escapeWithReference && (n = Q(f[o], a[e] - ('right' === e ? f.width : f.height))), le({}, o, n);
            }
          };
          return l.forEach(function (e) {
            var t = -1 === ['left', 'top'].indexOf(e) ? 'secondary' : 'primary';
            f = fe({}, f, m[t](e));
          }), e.offsets.popper = f, e;
        },
        priority: ['left', 'right', 'top', 'bottom'],
        padding: 5,
        boundariesElement: 'scrollParent'
      },
      keepTogether: {
        order: 400,
        enabled: !0,
        fn: function fn(e) {
          var t = e.offsets,
              o = t.popper,
              n = t.reference,
              i = e.placement.split('-')[0],
              r = Z,
              p = -1 !== ['top', 'bottom'].indexOf(i),
              s = p ? 'right' : 'bottom',
              d = p ? 'left' : 'top',
              a = p ? 'width' : 'height';
          return o[s] < r(n[d]) && (e.offsets.popper[d] = r(n[d]) - o[a]), o[d] > r(n[s]) && (e.offsets.popper[d] = r(n[s])), e;
        }
      },
      arrow: {
        order: 500,
        enabled: !0,
        fn: function fn(e, o) {
          var n;
          if (!K(e.instance.modifiers, 'arrow', 'keepTogether')) return e;
          var i = o.element;

          if ('string' == typeof i) {
            if (i = e.instance.popper.querySelector(i), !i) return e;
          } else if (!e.instance.popper.contains(i)) return console.warn('WARNING: `arrow.element` must be child of its popper element!'), e;

          var r = e.placement.split('-')[0],
              p = e.offsets,
              s = p.popper,
              d = p.reference,
              a = -1 !== ['left', 'right'].indexOf(r),
              l = a ? 'height' : 'width',
              f = a ? 'Top' : 'Left',
              m = f.toLowerCase(),
              h = a ? 'left' : 'top',
              c = a ? 'bottom' : 'right',
              u = S(i)[l];
          d[c] - u < s[m] && (e.offsets.popper[m] -= s[m] - (d[c] - u)), d[m] + u > s[c] && (e.offsets.popper[m] += d[m] + u - s[c]), e.offsets.popper = g(e.offsets.popper);
          var b = d[m] + d[l] / 2 - u / 2,
              w = t(e.instance.popper),
              y = parseFloat(w['margin' + f], 10),
              E = parseFloat(w['border' + f + 'Width'], 10),
              v = b - e.offsets.popper[m] - y - E;
          return v = ee(Q(s[l] - u, v), 0), e.arrowElement = i, e.offsets.arrow = (n = {}, le(n, m, $(v)), le(n, h, ''), n), e;
        },
        element: '[x-arrow]'
      },
      flip: {
        order: 600,
        enabled: !0,
        fn: function fn(e, t) {
          if (W(e.instance.modifiers, 'inner')) return e;
          if (e.flipped && e.placement === e.originalPlacement) return e;
          var o = v(e.instance.popper, e.instance.reference, t.padding, t.boundariesElement, e.positionFixed),
              n = e.placement.split('-')[0],
              i = T(n),
              r = e.placement.split('-')[1] || '',
              p = [];

          switch (t.behavior) {
            case ge.FLIP:
              p = [n, i];
              break;

            case ge.CLOCKWISE:
              p = G(n);
              break;

            case ge.COUNTERCLOCKWISE:
              p = G(n, !0);
              break;

            default:
              p = t.behavior;
          }

          return p.forEach(function (s, d) {
            if (n !== s || p.length === d + 1) return e;
            n = e.placement.split('-')[0], i = T(n);
            var a = e.offsets.popper,
                l = e.offsets.reference,
                f = Z,
                m = 'left' === n && f(a.right) > f(l.left) || 'right' === n && f(a.left) < f(l.right) || 'top' === n && f(a.bottom) > f(l.top) || 'bottom' === n && f(a.top) < f(l.bottom),
                h = f(a.left) < f(o.left),
                c = f(a.right) > f(o.right),
                g = f(a.top) < f(o.top),
                u = f(a.bottom) > f(o.bottom),
                b = 'left' === n && h || 'right' === n && c || 'top' === n && g || 'bottom' === n && u,
                w = -1 !== ['top', 'bottom'].indexOf(n),
                y = !!t.flipVariations && (w && 'start' === r && h || w && 'end' === r && c || !w && 'start' === r && g || !w && 'end' === r && u);
            (m || b || y) && (e.flipped = !0, (m || b) && (n = p[d + 1]), y && (r = z(r)), e.placement = n + (r ? '-' + r : ''), e.offsets.popper = fe({}, e.offsets.popper, D(e.instance.popper, e.offsets.reference, e.placement)), e = P(e.instance.modifiers, e, 'flip'));
          }), e;
        },
        behavior: 'flip',
        padding: 5,
        boundariesElement: 'viewport'
      },
      inner: {
        order: 700,
        enabled: !1,
        fn: function fn(e) {
          var t = e.placement,
              o = t.split('-')[0],
              n = e.offsets,
              i = n.popper,
              r = n.reference,
              p = -1 !== ['left', 'right'].indexOf(o),
              s = -1 === ['top', 'left'].indexOf(o);
          return i[p ? 'left' : 'top'] = r[o] - (s ? i[p ? 'width' : 'height'] : 0), e.placement = T(t), e.offsets.popper = g(i), e;
        }
      },
      hide: {
        order: 800,
        enabled: !0,
        fn: function fn(e) {
          if (!K(e.instance.modifiers, 'hide', 'preventOverflow')) return e;
          var t = e.offsets.reference,
              o = C(e.instance.modifiers, function (e) {
            return 'preventOverflow' === e.name;
          }).boundaries;

          if (t.bottom < o.top || t.left > o.right || t.top > o.bottom || t.right < o.left) {
            if (!0 === e.hide) return e;
            e.hide = !0, e.attributes['x-out-of-boundaries'] = '';
          } else {
            if (!1 === e.hide) return e;
            e.hide = !1, e.attributes['x-out-of-boundaries'] = !1;
          }

          return e;
        }
      },
      computeStyle: {
        order: 850,
        enabled: !0,
        fn: function fn(e, t) {
          var o = t.x,
              n = t.y,
              i = e.offsets.popper,
              r = C(e.instance.modifiers, function (e) {
            return 'applyStyle' === e.name;
          }).gpuAcceleration;
          void 0 !== r && console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');
          var s,
              d,
              a = void 0 === r ? t.gpuAcceleration : r,
              l = p(e.instance.popper),
              f = u(l),
              m = {
            position: i.position
          },
              h = q(e, 2 > window.devicePixelRatio || !me),
              c = 'bottom' === o ? 'top' : 'bottom',
              g = 'right' === n ? 'left' : 'right',
              b = H('transform');
          if (d = 'bottom' == c ? 'HTML' === l.nodeName ? -l.clientHeight + h.bottom : -f.height + h.bottom : h.top, s = 'right' == g ? 'HTML' === l.nodeName ? -l.clientWidth + h.right : -f.width + h.right : h.left, a && b) m[b] = 'translate3d(' + s + 'px, ' + d + 'px, 0)', m[c] = 0, m[g] = 0, m.willChange = 'transform';else {
            var w = 'bottom' == c ? -1 : 1,
                y = 'right' == g ? -1 : 1;
            m[c] = d * w, m[g] = s * y, m.willChange = c + ', ' + g;
          }
          var E = {
            "x-placement": e.placement
          };
          return e.attributes = fe({}, E, e.attributes), e.styles = fe({}, m, e.styles), e.arrowStyles = fe({}, e.offsets.arrow, e.arrowStyles), e;
        },
        gpuAcceleration: !0,
        x: 'bottom',
        y: 'right'
      },
      applyStyle: {
        order: 900,
        enabled: !0,
        fn: function fn(e) {
          return j(e.instance.popper, e.styles), V(e.instance.popper, e.attributes), e.arrowElement && Object.keys(e.arrowStyles).length && j(e.arrowElement, e.arrowStyles), e;
        },
        onLoad: function onLoad(e, t, o, n, i) {
          var r = L(i, t, e, o.positionFixed),
              p = O(o.placement, r, t, e, o.modifiers.flip.boundariesElement, o.modifiers.flip.padding);
          return t.setAttribute('x-placement', p), j(t, {
            position: o.positionFixed ? 'fixed' : 'absolute'
          }), o;
        },
        gpuAcceleration: void 0
      }
    }
  }, ue;
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack-stream/node_modules/webpack/buildin/global.js */ "./node_modules/webpack-stream/node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./js/libs/oldFiles/smoothscroll.js":
/*!******************************************!*\
  !*** ./js/libs/oldFiles/smoothscroll.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

// smoothscroll.js in here.

/*!
 * Smooth Scroll - v1.4.13 - 2013-11-02
 * https://github.com/kswedberg/jquery-smooth-scroll
 * Copyright (c) 2013 Karl Swedberg
 * Licensed MIT (https://github.com/kswedberg/jquery-smooth-scroll/blob/master/LICENSE-MIT)
 */
(function (t) {
  function e(t) {
    return t.replace(/(:|\.)/g, "\\$1");
  }

  var l = "1.4.13",
      o = {},
      s = {
    exclude: [],
    excludeWithin: [],
    offset: 0,
    direction: "top",
    scrollElement: null,
    scrollTarget: null,
    beforeScroll: function beforeScroll() {},
    afterScroll: function afterScroll() {},
    easing: "swing",
    speed: 400,
    autoCoefficent: 2,
    preventDefault: !0
  },
      n = function n(e) {
    var l = [],
        o = !1,
        s = e.dir && "left" == e.dir ? "scrollLeft" : "scrollTop";
    return this.each(function () {
      if (this != document && this != window) {
        var e = t(this);
        e[s]() > 0 ? l.push(this) : (e[s](1), o = e[s]() > 0, o && l.push(this), e[s](0));
      }
    }), l.length || this.each(function () {
      "BODY" === this.nodeName && (l = [this]);
    }), "first" === e.el && l.length > 1 && (l = [l[0]]), l;
  };

  t.fn.extend({
    scrollable: function scrollable(t) {
      var e = n.call(this, {
        dir: t
      });
      return this.pushStack(e);
    },
    firstScrollable: function firstScrollable(t) {
      var e = n.call(this, {
        el: "first",
        dir: t
      });
      return this.pushStack(e);
    },
    smoothScroll: function smoothScroll(l, o) {
      if (l = l || {}, "options" === l) return o ? this.each(function () {
        var e = t(this),
            l = t.extend(e.data("ssOpts") || {}, o);
        t(this).data("ssOpts", l);
      }) : this.first().data("ssOpts");
      var s = t.extend({}, t.fn.smoothScroll.defaults, l),
          n = t.smoothScroll.filterPath(location.pathname);
      return this.unbind("click.smoothscroll").bind("click.smoothscroll", function (l) {
        var o = this,
            r = t(this),
            i = t.extend({}, s, r.data("ssOpts") || {}),
            c = s.exclude,
            a = i.excludeWithin,
            f = 0,
            h = 0,
            u = !0,
            d = {},
            p = location.hostname === o.hostname || !o.hostname,
            m = i.scrollTarget || (t.smoothScroll.filterPath(o.pathname) || n) === n,
            S = e(o.hash);

        if (i.scrollTarget || p && m && S) {
          for (; u && c.length > f;) {
            r.is(e(c[f++])) && (u = !1);
          }

          for (; u && a.length > h;) {
            r.closest(a[h++]).length && (u = !1);
          }
        } else u = !1;

        u && (i.preventDefault && l.preventDefault(), t.extend(d, i, {
          scrollTarget: i.scrollTarget || S,
          link: o
        }), t.smoothScroll(d));
      }), this;
    }
  }), t.smoothScroll = function (e, l) {
    if ("options" === e && "object" == _typeof(l)) return t.extend(o, l);
    var s,
        n,
        r,
        i,
        c = 0,
        a = "offset",
        f = "scrollTop",
        h = {},
        u = {};
    "number" == typeof e ? (s = t.extend({
      link: null
    }, t.fn.smoothScroll.defaults, o), r = e) : (s = t.extend({
      link: null
    }, t.fn.smoothScroll.defaults, e || {}, o), s.scrollElement && (a = "position", "static" == s.scrollElement.css("position") && s.scrollElement.css("position", "relative"))), f = "left" == s.direction ? "scrollLeft" : f, s.scrollElement ? (n = s.scrollElement, /^(?:HTML|BODY)$/.test(n[0].nodeName) || (c = n[f]())) : n = t("html, body").firstScrollable(s.direction), s.beforeScroll.call(n, s), r = "number" == typeof e ? e : l || t(s.scrollTarget)[a]() && t(s.scrollTarget)[a]()[s.direction] || 0, h[f] = r + c + s.offset, i = s.speed, "auto" === i && (i = h[f] || n.scrollTop(), i /= s.autoCoefficent), u = {
      duration: i,
      easing: s.easing,
      complete: function complete() {
        s.afterScroll.call(s.link, s);
      }
    }, s.step && (u.step = s.step), n.length ? n.stop().animate(h, u) : s.afterScroll.call(s.link, s);
  }, t.smoothScroll.version = l, t.smoothScroll.filterPath = function (t) {
    return t.replace(/^\//, "").replace(/(?:index|default).[a-zA-Z]{3,4}$/, "").replace(/\/$/, "");
  }, t.fn.smoothScroll.defaults = s;
})(jQuery);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/libs/oldFiles/waypoints.js":
/*!***************************************!*\
  !*** ./js/libs/oldFiles/waypoints.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(__webpack_provided_window_dot_jQuery) {/*!
Waypoints - 4.0.1
Copyright © 2011-2016 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blob/master/licenses.txt
*/
!function () {
  "use strict";

  function t(o) {
    if (!o) throw new Error("No options passed to Waypoint constructor");
    if (!o.element) throw new Error("No element option passed to Waypoint constructor");
    if (!o.handler) throw new Error("No handler option passed to Waypoint constructor");
    this.key = "waypoint-" + e, this.options = t.Adapter.extend({}, t.defaults, o), this.element = this.options.element, this.adapter = new t.Adapter(this.element), this.callback = o.handler, this.axis = this.options.horizontal ? "horizontal" : "vertical", this.enabled = this.options.enabled, this.triggerPoint = null, this.group = t.Group.findOrCreate({
      name: this.options.group,
      axis: this.axis
    }), this.context = t.Context.findOrCreateByElement(this.options.context), t.offsetAliases[this.options.offset] && (this.options.offset = t.offsetAliases[this.options.offset]), this.group.add(this), this.context.add(this), i[this.key] = this, e += 1;
  }

  var e = 0,
      i = {};
  t.prototype.queueTrigger = function (t) {
    this.group.queueTrigger(this, t);
  }, t.prototype.trigger = function (t) {
    this.enabled && this.callback && this.callback.apply(this, t);
  }, t.prototype.destroy = function () {
    this.context.remove(this), this.group.remove(this), delete i[this.key];
  }, t.prototype.disable = function () {
    return this.enabled = !1, this;
  }, t.prototype.enable = function () {
    return this.context.refresh(), this.enabled = !0, this;
  }, t.prototype.next = function () {
    return this.group.next(this);
  }, t.prototype.previous = function () {
    return this.group.previous(this);
  }, t.invokeAll = function (t) {
    var e = [];

    for (var o in i) {
      e.push(i[o]);
    }

    for (var n = 0, r = e.length; r > n; n++) {
      e[n][t]();
    }
  }, t.destroyAll = function () {
    t.invokeAll("destroy");
  }, t.disableAll = function () {
    t.invokeAll("disable");
  }, t.enableAll = function () {
    t.Context.refreshAll();

    for (var e in i) {
      i[e].enabled = !0;
    }

    return this;
  }, t.refreshAll = function () {
    t.Context.refreshAll();
  }, t.viewportHeight = function () {
    return window.innerHeight || document.documentElement.clientHeight;
  }, t.viewportWidth = function () {
    return document.documentElement.clientWidth;
  }, t.adapters = [], t.defaults = {
    context: window,
    continuous: !0,
    enabled: !0,
    group: "default",
    horizontal: !1,
    offset: 0
  }, t.offsetAliases = {
    "bottom-in-view": function bottomInView() {
      return this.context.innerHeight() - this.adapter.outerHeight();
    },
    "right-in-view": function rightInView() {
      return this.context.innerWidth() - this.adapter.outerWidth();
    }
  }, window.Waypoint = t;
}(), function () {
  "use strict";

  function t(t) {
    window.setTimeout(t, 1e3 / 60);
  }

  function e(t) {
    this.element = t, this.Adapter = n.Adapter, this.adapter = new this.Adapter(t), this.key = "waypoint-context-" + i, this.didScroll = !1, this.didResize = !1, this.oldScroll = {
      x: this.adapter.scrollLeft(),
      y: this.adapter.scrollTop()
    }, this.waypoints = {
      vertical: {},
      horizontal: {}
    }, t.waypointContextKey = this.key, o[t.waypointContextKey] = this, i += 1, n.windowContext || (n.windowContext = !0, n.windowContext = new e(window)), this.createThrottledScrollHandler(), this.createThrottledResizeHandler();
  }

  var i = 0,
      o = {},
      n = window.Waypoint,
      r = window.onload;
  e.prototype.add = function (t) {
    var e = t.options.horizontal ? "horizontal" : "vertical";
    this.waypoints[e][t.key] = t, this.refresh();
  }, e.prototype.checkEmpty = function () {
    var t = this.Adapter.isEmptyObject(this.waypoints.horizontal),
        e = this.Adapter.isEmptyObject(this.waypoints.vertical),
        i = this.element == this.element.window;
    t && e && !i && (this.adapter.off(".waypoints"), delete o[this.key]);
  }, e.prototype.createThrottledResizeHandler = function () {
    function t() {
      e.handleResize(), e.didResize = !1;
    }

    var e = this;
    this.adapter.on("resize.waypoints", function () {
      e.didResize || (e.didResize = !0, n.requestAnimationFrame(t));
    });
  }, e.prototype.createThrottledScrollHandler = function () {
    function t() {
      e.handleScroll(), e.didScroll = !1;
    }

    var e = this;
    this.adapter.on("scroll.waypoints", function () {
      (!e.didScroll || n.isTouch) && (e.didScroll = !0, n.requestAnimationFrame(t));
    });
  }, e.prototype.handleResize = function () {
    n.Context.refreshAll();
  }, e.prototype.handleScroll = function () {
    var t = {},
        e = {
      horizontal: {
        newScroll: this.adapter.scrollLeft(),
        oldScroll: this.oldScroll.x,
        forward: "right",
        backward: "left"
      },
      vertical: {
        newScroll: this.adapter.scrollTop(),
        oldScroll: this.oldScroll.y,
        forward: "down",
        backward: "up"
      }
    };

    for (var i in e) {
      var o = e[i],
          n = o.newScroll > o.oldScroll,
          r = n ? o.forward : o.backward;

      for (var s in this.waypoints[i]) {
        var a = this.waypoints[i][s];

        if (null !== a.triggerPoint) {
          var l = o.oldScroll < a.triggerPoint,
              h = o.newScroll >= a.triggerPoint,
              p = l && h,
              u = !l && !h;
          (p || u) && (a.queueTrigger(r), t[a.group.id] = a.group);
        }
      }
    }

    for (var c in t) {
      t[c].flushTriggers();
    }

    this.oldScroll = {
      x: e.horizontal.newScroll,
      y: e.vertical.newScroll
    };
  }, e.prototype.innerHeight = function () {
    return this.element == this.element.window ? n.viewportHeight() : this.adapter.innerHeight();
  }, e.prototype.remove = function (t) {
    delete this.waypoints[t.axis][t.key], this.checkEmpty();
  }, e.prototype.innerWidth = function () {
    return this.element == this.element.window ? n.viewportWidth() : this.adapter.innerWidth();
  }, e.prototype.destroy = function () {
    var t = [];

    for (var e in this.waypoints) {
      for (var i in this.waypoints[e]) {
        t.push(this.waypoints[e][i]);
      }
    }

    for (var o = 0, n = t.length; n > o; o++) {
      t[o].destroy();
    }
  }, e.prototype.refresh = function () {
    var t,
        e = this.element == this.element.window,
        i = e ? void 0 : this.adapter.offset(),
        o = {};
    this.handleScroll(), t = {
      horizontal: {
        contextOffset: e ? 0 : i.left,
        contextScroll: e ? 0 : this.oldScroll.x,
        contextDimension: this.innerWidth(),
        oldScroll: this.oldScroll.x,
        forward: "right",
        backward: "left",
        offsetProp: "left"
      },
      vertical: {
        contextOffset: e ? 0 : i.top,
        contextScroll: e ? 0 : this.oldScroll.y,
        contextDimension: this.innerHeight(),
        oldScroll: this.oldScroll.y,
        forward: "down",
        backward: "up",
        offsetProp: "top"
      }
    };

    for (var r in t) {
      var s = t[r];

      for (var a in this.waypoints[r]) {
        var l,
            h,
            p,
            u,
            c,
            d = this.waypoints[r][a],
            f = d.options.offset,
            w = d.triggerPoint,
            y = 0,
            g = null == w;
        d.element !== d.element.window && (y = d.adapter.offset()[s.offsetProp]), "function" == typeof f ? f = f.apply(d) : "string" == typeof f && (f = parseFloat(f), d.options.offset.indexOf("%") > -1 && (f = Math.ceil(s.contextDimension * f / 100))), l = s.contextScroll - s.contextOffset, d.triggerPoint = Math.floor(y + l - f), h = w < s.oldScroll, p = d.triggerPoint >= s.oldScroll, u = h && p, c = !h && !p, !g && u ? (d.queueTrigger(s.backward), o[d.group.id] = d.group) : !g && c ? (d.queueTrigger(s.forward), o[d.group.id] = d.group) : g && s.oldScroll >= d.triggerPoint && (d.queueTrigger(s.forward), o[d.group.id] = d.group);
      }
    }

    return n.requestAnimationFrame(function () {
      for (var t in o) {
        o[t].flushTriggers();
      }
    }), this;
  }, e.findOrCreateByElement = function (t) {
    return e.findByElement(t) || new e(t);
  }, e.refreshAll = function () {
    for (var t in o) {
      o[t].refresh();
    }
  }, e.findByElement = function (t) {
    return o[t.waypointContextKey];
  }, window.onload = function () {
    r && r(), e.refreshAll();
  }, n.requestAnimationFrame = function (e) {
    var i = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || t;
    i.call(window, e);
  }, n.Context = e;
}(), function () {
  "use strict";

  function t(t, e) {
    return t.triggerPoint - e.triggerPoint;
  }

  function e(t, e) {
    return e.triggerPoint - t.triggerPoint;
  }

  function i(t) {
    this.name = t.name, this.axis = t.axis, this.id = this.name + "-" + this.axis, this.waypoints = [], this.clearTriggerQueues(), o[this.axis][this.name] = this;
  }

  var o = {
    vertical: {},
    horizontal: {}
  },
      n = window.Waypoint;
  i.prototype.add = function (t) {
    this.waypoints.push(t);
  }, i.prototype.clearTriggerQueues = function () {
    this.triggerQueues = {
      up: [],
      down: [],
      left: [],
      right: []
    };
  }, i.prototype.flushTriggers = function () {
    for (var i in this.triggerQueues) {
      var o = this.triggerQueues[i],
          n = "up" === i || "left" === i;
      o.sort(n ? e : t);

      for (var r = 0, s = o.length; s > r; r += 1) {
        var a = o[r];
        (a.options.continuous || r === o.length - 1) && a.trigger([i]);
      }
    }

    this.clearTriggerQueues();
  }, i.prototype.next = function (e) {
    this.waypoints.sort(t);
    var i = n.Adapter.inArray(e, this.waypoints),
        o = i === this.waypoints.length - 1;
    return o ? null : this.waypoints[i + 1];
  }, i.prototype.previous = function (e) {
    this.waypoints.sort(t);
    var i = n.Adapter.inArray(e, this.waypoints);
    return i ? this.waypoints[i - 1] : null;
  }, i.prototype.queueTrigger = function (t, e) {
    this.triggerQueues[e].push(t);
  }, i.prototype.remove = function (t) {
    var e = n.Adapter.inArray(t, this.waypoints);
    e > -1 && this.waypoints.splice(e, 1);
  }, i.prototype.first = function () {
    return this.waypoints[0];
  }, i.prototype.last = function () {
    return this.waypoints[this.waypoints.length - 1];
  }, i.findOrCreate = function (t) {
    return o[t.axis][t.name] || new i(t);
  }, n.Group = i;
}(), function () {
  "use strict";

  function t(t) {
    this.$element = e(t);
  }

  var e = __webpack_provided_window_dot_jQuery,
      i = window.Waypoint;
  e.each(["innerHeight", "innerWidth", "off", "offset", "on", "outerHeight", "outerWidth", "scrollLeft", "scrollTop"], function (e, i) {
    t.prototype[i] = function () {
      var t = Array.prototype.slice.call(arguments);
      return this.$element[i].apply(this.$element, t);
    };
  }), e.each(["extend", "inArray", "isEmptyObject"], function (i, o) {
    t[o] = e[o];
  }), i.adapters.push({
    name: "jquery",
    Adapter: t
  }), i.Adapter = t;
}(), function () {
  "use strict";

  function t(t) {
    return function () {
      var i = [],
          o = arguments[0];
      return t.isFunction(arguments[0]) && (o = t.extend({}, arguments[1]), o.handler = arguments[0]), this.each(function () {
        var n = t.extend({}, o, {
          element: this
        });
        "string" == typeof n.context && (n.context = t(this).closest(n.context)[0]), i.push(new e(n));
      }), i;
    };
  }

  var e = window.Waypoint;
  __webpack_provided_window_dot_jQuery && (__webpack_provided_window_dot_jQuery.fn.waypoint = t(__webpack_provided_window_dot_jQuery)), window.Zepto && (window.Zepto.fn.waypoint = t(window.Zepto));
}();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./js/libs/plugins.js":
/*!****************************!*\
  !*** ./js/libs/plugins.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _oldFiles_modernizr_3_6_0_min__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./oldFiles/modernizr-3.6.0.min */ "./js/libs/oldFiles/modernizr-3.6.0.min.js");
/* harmony import */ var _oldFiles_modernizr_3_6_0_min__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_oldFiles_modernizr_3_6_0_min__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _oldFiles_popper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./oldFiles/popper */ "./js/libs/oldFiles/popper.js");
/* harmony import */ var _oldFiles_popper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_oldFiles_popper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _oldFiles_common_scripts_min__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./oldFiles/common_scripts.min */ "./js/libs/oldFiles/common_scripts.min.js");
/* harmony import */ var _oldFiles_common_scripts_min__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_oldFiles_common_scripts_min__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _node_modules_jquery_countdown_dist_jquery_countdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../node_modules/jquery-countdown/dist/jquery.countdown */ "./node_modules/jquery-countdown/dist/jquery.countdown.js");
/* harmony import */ var _node_modules_jquery_countdown_dist_jquery_countdown__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_node_modules_jquery_countdown_dist_jquery_countdown__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _node_modules_imagesloaded_imagesloaded__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../node_modules/imagesloaded/imagesloaded */ "./node_modules/imagesloaded/imagesloaded.js");
/* harmony import */ var _node_modules_imagesloaded_imagesloaded__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_node_modules_imagesloaded_imagesloaded__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _oldFiles_isotope__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./oldFiles/isotope */ "./js/libs/oldFiles/isotope.js");
/* harmony import */ var _oldFiles_isotope__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_oldFiles_isotope__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _oldFiles_jquery_ui__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./oldFiles/jquery-ui */ "./js/libs/oldFiles/jquery-ui.js");
/* harmony import */ var _oldFiles_jquery_ui__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_oldFiles_jquery_ui__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _oldFiles_jquery_ui_touch_punch__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./oldFiles/jquery-ui-touch-punch */ "./js/libs/oldFiles/jquery-ui-touch-punch.js");
/* harmony import */ var _oldFiles_jquery_ui_touch_punch__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_oldFiles_jquery_ui_touch_punch__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _oldFiles_waypoints__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./oldFiles/waypoints */ "./js/libs/oldFiles/waypoints.js");
/* harmony import */ var _oldFiles_waypoints__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_oldFiles_waypoints__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _oldFiles_smoothscroll__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./oldFiles/smoothscroll */ "./js/libs/oldFiles/smoothscroll.js");
/* harmony import */ var _oldFiles_smoothscroll__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_oldFiles_smoothscroll__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _spinner__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./spinner */ "./js/libs/spinner.js");
/* harmony import */ var _spinner__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_spinner__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _oldFiles_lazyload_min__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./oldFiles/lazyload.min */ "./js/libs/oldFiles/lazyload.min.js");
/* harmony import */ var _oldFiles_lazyload_min__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_oldFiles_lazyload_min__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _oldFiles_ResizeSensor_min__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./oldFiles/ResizeSensor.min */ "./js/libs/oldFiles/ResizeSensor.min.js");
/* harmony import */ var _oldFiles_ResizeSensor_min__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_oldFiles_ResizeSensor_min__WEBPACK_IMPORTED_MODULE_13__);
//JQuery

global.jQuery = jquery__WEBPACK_IMPORTED_MODULE_0___default.a;
global.$ = jquery__WEBPACK_IMPORTED_MODULE_0___default.a; //old plugin













 //Wow js plugin

window.WOW = __webpack_require__(/*! ../../node_modules/wowjs/dist/wow.min */ "./node_modules/wowjs/dist/wow.min.js").WOW;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack-stream/node_modules/webpack/buildin/global.js */ "./node_modules/webpack-stream/node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./js/libs/spinner.js":
/*!****************************!*\
  !*** ./js/libs/spinner.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($, jQuery) {/*
 * Spinner functions.
 */
$(document).ready(function () {
  /**
   * Create jQuery function.
   */
  (function ($) {
    /*
     * Adding spinner into element.
     * @param spinnerId{String} - id of spinner
     */
    $.fn.spinnerAdd = function (spinnerId) {
      if (!spinnerId) spinnerId = 'js_spinner';
      $(this).append('<div id="' + spinnerId + '" class="loaderWrapper"><div class="loader"></div></div>');
    };
    /*
     * Remove spinner.
     * @param{String} spinnerId - id of spinner
     */


    $.spinnerRemove = function (spinnerId) {
      if (!spinnerId) spinnerId = 'js_spinner';
      $('#' + spinnerId + '.loaderWrapper').remove();
    };
    /*
     * Adding spinner into body.
     * @param{String} spinnerId - id of spinner
     */


    $.spinnerAdd = function (spinnerId) {
      if (!spinnerId) spinnerId = 'js_spinner';
      $('body').append('<div id="' + spinnerId + '" class="loaderWrapper"><div class="loader"></div></div>');
    };
  })(jQuery);
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./node_modules/ev-emitter/ev-emitter.js":
/*!***********************************************!*\
  !*** ./node_modules/ev-emitter/ev-emitter.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;/**
 * EvEmitter v1.0.3
 * Lil' event emitter
 * MIT License
 */

/* jshint unused: true, undef: true, strict: true */

( function( global, factory ) {
  // universal module definition
  /* jshint strict: false */ /* globals define, module, window */
  if ( true ) {
    // AMD - RequireJS
    !(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}

}( typeof window != 'undefined' ? window : this, function() {

"use strict";

function EvEmitter() {}

var proto = EvEmitter.prototype;

proto.on = function( eventName, listener ) {
  if ( !eventName || !listener ) {
    return;
  }
  // set events hash
  var events = this._events = this._events || {};
  // set listeners array
  var listeners = events[ eventName ] = events[ eventName ] || [];
  // only add once
  if ( listeners.indexOf( listener ) == -1 ) {
    listeners.push( listener );
  }

  return this;
};

proto.once = function( eventName, listener ) {
  if ( !eventName || !listener ) {
    return;
  }
  // add event
  this.on( eventName, listener );
  // set once flag
  // set onceEvents hash
  var onceEvents = this._onceEvents = this._onceEvents || {};
  // set onceListeners object
  var onceListeners = onceEvents[ eventName ] = onceEvents[ eventName ] || {};
  // set flag
  onceListeners[ listener ] = true;

  return this;
};

proto.off = function( eventName, listener ) {
  var listeners = this._events && this._events[ eventName ];
  if ( !listeners || !listeners.length ) {
    return;
  }
  var index = listeners.indexOf( listener );
  if ( index != -1 ) {
    listeners.splice( index, 1 );
  }

  return this;
};

proto.emitEvent = function( eventName, args ) {
  var listeners = this._events && this._events[ eventName ];
  if ( !listeners || !listeners.length ) {
    return;
  }
  var i = 0;
  var listener = listeners[i];
  args = args || [];
  // once stuff
  var onceListeners = this._onceEvents && this._onceEvents[ eventName ];

  while ( listener ) {
    var isOnce = onceListeners && onceListeners[ listener ];
    if ( isOnce ) {
      // remove listener
      // remove before trigger to prevent recursion
      this.off( eventName, listener );
      // unset once flag
      delete onceListeners[ listener ];
    }
    // trigger listener
    listener.apply( this, args );
    // get next listener
    i += isOnce ? 0 : 1;
    listener = listeners[i];
  }

  return this;
};

return EvEmitter;

}));


/***/ }),

/***/ "./node_modules/imagesloaded/imagesloaded.js":
/*!***************************************************!*\
  !*** ./node_modules/imagesloaded/imagesloaded.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * imagesLoaded v4.1.1
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */

( function( window, factory ) { 'use strict';
  // universal module definition

  /*global define: false, module: false, require: false */

  if ( true ) {
    // AMD
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [
      __webpack_require__(/*! ev-emitter/ev-emitter */ "./node_modules/ev-emitter/ev-emitter.js")
    ], __WEBPACK_AMD_DEFINE_RESULT__ = (function( EvEmitter ) {
      return factory( window, EvEmitter );
    }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}

})( window,

// --------------------------  factory -------------------------- //

function factory( window, EvEmitter ) {

'use strict';

var $ = window.jQuery;
var console = window.console;

// -------------------------- helpers -------------------------- //

// extend objects
function extend( a, b ) {
  for ( var prop in b ) {
    a[ prop ] = b[ prop ];
  }
  return a;
}

// turn element or nodeList into an array
function makeArray( obj ) {
  var ary = [];
  if ( Array.isArray( obj ) ) {
    // use object if already an array
    ary = obj;
  } else if ( typeof obj.length == 'number' ) {
    // convert nodeList to array
    for ( var i=0; i < obj.length; i++ ) {
      ary.push( obj[i] );
    }
  } else {
    // array of single index
    ary.push( obj );
  }
  return ary;
}

// -------------------------- imagesLoaded -------------------------- //

/**
 * @param {Array, Element, NodeList, String} elem
 * @param {Object or Function} options - if function, use as callback
 * @param {Function} onAlways - callback function
 */
function ImagesLoaded( elem, options, onAlways ) {
  // coerce ImagesLoaded() without new, to be new ImagesLoaded()
  if ( !( this instanceof ImagesLoaded ) ) {
    return new ImagesLoaded( elem, options, onAlways );
  }
  // use elem as selector string
  if ( typeof elem == 'string' ) {
    elem = document.querySelectorAll( elem );
  }

  this.elements = makeArray( elem );
  this.options = extend( {}, this.options );

  if ( typeof options == 'function' ) {
    onAlways = options;
  } else {
    extend( this.options, options );
  }

  if ( onAlways ) {
    this.on( 'always', onAlways );
  }

  this.getImages();

  if ( $ ) {
    // add jQuery Deferred object
    this.jqDeferred = new $.Deferred();
  }

  // HACK check async to allow time to bind listeners
  setTimeout( function() {
    this.check();
  }.bind( this ));
}

ImagesLoaded.prototype = Object.create( EvEmitter.prototype );

ImagesLoaded.prototype.options = {};

ImagesLoaded.prototype.getImages = function() {
  this.images = [];

  // filter & find items if we have an item selector
  this.elements.forEach( this.addElementImages, this );
};

/**
 * @param {Node} element
 */
ImagesLoaded.prototype.addElementImages = function( elem ) {
  // filter siblings
  if ( elem.nodeName == 'IMG' ) {
    this.addImage( elem );
  }
  // get background image on element
  if ( this.options.background === true ) {
    this.addElementBackgroundImages( elem );
  }

  // find children
  // no non-element nodes, #143
  var nodeType = elem.nodeType;
  if ( !nodeType || !elementNodeTypes[ nodeType ] ) {
    return;
  }
  var childImgs = elem.querySelectorAll('img');
  // concat childElems to filterFound array
  for ( var i=0; i < childImgs.length; i++ ) {
    var img = childImgs[i];
    this.addImage( img );
  }

  // get child background images
  if ( typeof this.options.background == 'string' ) {
    var children = elem.querySelectorAll( this.options.background );
    for ( i=0; i < children.length; i++ ) {
      var child = children[i];
      this.addElementBackgroundImages( child );
    }
  }
};

var elementNodeTypes = {
  1: true,
  9: true,
  11: true
};

ImagesLoaded.prototype.addElementBackgroundImages = function( elem ) {
  var style = getComputedStyle( elem );
  if ( !style ) {
    // Firefox returns null if in a hidden iframe https://bugzil.la/548397
    return;
  }
  // get url inside url("...")
  var reURL = /url\((['"])?(.*?)\1\)/gi;
  var matches = reURL.exec( style.backgroundImage );
  while ( matches !== null ) {
    var url = matches && matches[2];
    if ( url ) {
      this.addBackground( url, elem );
    }
    matches = reURL.exec( style.backgroundImage );
  }
};

/**
 * @param {Image} img
 */
ImagesLoaded.prototype.addImage = function( img ) {
  var loadingImage = new LoadingImage( img );
  this.images.push( loadingImage );
};

ImagesLoaded.prototype.addBackground = function( url, elem ) {
  var background = new Background( url, elem );
  this.images.push( background );
};

ImagesLoaded.prototype.check = function() {
  var _this = this;
  this.progressedCount = 0;
  this.hasAnyBroken = false;
  // complete if no images
  if ( !this.images.length ) {
    this.complete();
    return;
  }

  function onProgress( image, elem, message ) {
    // HACK - Chrome triggers event before object properties have changed. #83
    setTimeout( function() {
      _this.progress( image, elem, message );
    });
  }

  this.images.forEach( function( loadingImage ) {
    loadingImage.once( 'progress', onProgress );
    loadingImage.check();
  });
};

ImagesLoaded.prototype.progress = function( image, elem, message ) {
  this.progressedCount++;
  this.hasAnyBroken = this.hasAnyBroken || !image.isLoaded;
  // progress event
  this.emitEvent( 'progress', [ this, image, elem ] );
  if ( this.jqDeferred && this.jqDeferred.notify ) {
    this.jqDeferred.notify( this, image );
  }
  // check if completed
  if ( this.progressedCount == this.images.length ) {
    this.complete();
  }

  if ( this.options.debug && console ) {
    console.log( 'progress: ' + message, image, elem );
  }
};

ImagesLoaded.prototype.complete = function() {
  var eventName = this.hasAnyBroken ? 'fail' : 'done';
  this.isComplete = true;
  this.emitEvent( eventName, [ this ] );
  this.emitEvent( 'always', [ this ] );
  if ( this.jqDeferred ) {
    var jqMethod = this.hasAnyBroken ? 'reject' : 'resolve';
    this.jqDeferred[ jqMethod ]( this );
  }
};

// --------------------------  -------------------------- //

function LoadingImage( img ) {
  this.img = img;
}

LoadingImage.prototype = Object.create( EvEmitter.prototype );

LoadingImage.prototype.check = function() {
  // If complete is true and browser supports natural sizes,
  // try to check for image status manually.
  var isComplete = this.getIsImageComplete();
  if ( isComplete ) {
    // report based on naturalWidth
    this.confirm( this.img.naturalWidth !== 0, 'naturalWidth' );
    return;
  }

  // If none of the checks above matched, simulate loading on detached element.
  this.proxyImage = new Image();
  this.proxyImage.addEventListener( 'load', this );
  this.proxyImage.addEventListener( 'error', this );
  // bind to image as well for Firefox. #191
  this.img.addEventListener( 'load', this );
  this.img.addEventListener( 'error', this );
  this.proxyImage.src = this.img.src;
};

LoadingImage.prototype.getIsImageComplete = function() {
  return this.img.complete && this.img.naturalWidth !== undefined;
};

LoadingImage.prototype.confirm = function( isLoaded, message ) {
  this.isLoaded = isLoaded;
  this.emitEvent( 'progress', [ this, this.img, message ] );
};

// ----- events ----- //

// trigger specified handler for event type
LoadingImage.prototype.handleEvent = function( event ) {
  var method = 'on' + event.type;
  if ( this[ method ] ) {
    this[ method ]( event );
  }
};

LoadingImage.prototype.onload = function() {
  this.confirm( true, 'onload' );
  this.unbindEvents();
};

LoadingImage.prototype.onerror = function() {
  this.confirm( false, 'onerror' );
  this.unbindEvents();
};

LoadingImage.prototype.unbindEvents = function() {
  this.proxyImage.removeEventListener( 'load', this );
  this.proxyImage.removeEventListener( 'error', this );
  this.img.removeEventListener( 'load', this );
  this.img.removeEventListener( 'error', this );
};

// -------------------------- Background -------------------------- //

function Background( url, element ) {
  this.url = url;
  this.element = element;
  this.img = new Image();
}

// inherit LoadingImage prototype
Background.prototype = Object.create( LoadingImage.prototype );

Background.prototype.check = function() {
  this.img.addEventListener( 'load', this );
  this.img.addEventListener( 'error', this );
  this.img.src = this.url;
  // check if image is already complete
  var isComplete = this.getIsImageComplete();
  if ( isComplete ) {
    this.confirm( this.img.naturalWidth !== 0, 'naturalWidth' );
    this.unbindEvents();
  }
};

Background.prototype.unbindEvents = function() {
  this.img.removeEventListener( 'load', this );
  this.img.removeEventListener( 'error', this );
};

Background.prototype.confirm = function( isLoaded, message ) {
  this.isLoaded = isLoaded;
  this.emitEvent( 'progress', [ this, this.element, message ] );
};

// -------------------------- jQuery -------------------------- //

ImagesLoaded.makeJQueryPlugin = function( jQuery ) {
  jQuery = jQuery || window.jQuery;
  if ( !jQuery ) {
    return;
  }
  // set local variable
  $ = jQuery;
  // $().imagesLoaded()
  $.fn.imagesLoaded = function( options, callback ) {
    var instance = new ImagesLoaded( this, options, callback );
    return instance.jqDeferred.promise( $(this) );
  };
};
// try making plugin
ImagesLoaded.makeJQueryPlugin();

// --------------------------  -------------------------- //

return ImagesLoaded;

});


/***/ }),

/***/ "./node_modules/jquery-countdown/dist/jquery.countdown.js":
/*!****************************************************************!*\
  !*** ./node_modules/jquery-countdown/dist/jquery.countdown.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * The Final Countdown for jQuery v2.2.0 (http://hilios.github.io/jQuery.countdown/)
 * Copyright (c) 2016 Edson Hilios
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
(function(factory) {
    "use strict";
    if (true) {
        !(__WEBPACK_AMD_DEFINE_ARRAY__ = [ __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js") ], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
    } else {}
})(function($) {
    "use strict";
    var instances = [], matchers = [], defaultOptions = {
        precision: 100,
        elapse: false,
        defer: false
    };
    matchers.push(/^[0-9]*$/.source);
    matchers.push(/([0-9]{1,2}\/){2}[0-9]{4}( [0-9]{1,2}(:[0-9]{2}){2})?/.source);
    matchers.push(/[0-9]{4}([\/\-][0-9]{1,2}){2}( [0-9]{1,2}(:[0-9]{2}){2})?/.source);
    matchers = new RegExp(matchers.join("|"));
    function parseDateString(dateString) {
        if (dateString instanceof Date) {
            return dateString;
        }
        if (String(dateString).match(matchers)) {
            if (String(dateString).match(/^[0-9]*$/)) {
                dateString = Number(dateString);
            }
            if (String(dateString).match(/\-/)) {
                dateString = String(dateString).replace(/\-/g, "/");
            }
            return new Date(dateString);
        } else {
            throw new Error("Couldn't cast `" + dateString + "` to a date object.");
        }
    }
    var DIRECTIVE_KEY_MAP = {
        Y: "years",
        m: "months",
        n: "daysToMonth",
        d: "daysToWeek",
        w: "weeks",
        W: "weeksToMonth",
        H: "hours",
        M: "minutes",
        S: "seconds",
        D: "totalDays",
        I: "totalHours",
        N: "totalMinutes",
        T: "totalSeconds"
    };
    function escapedRegExp(str) {
        var sanitize = str.toString().replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
        return new RegExp(sanitize);
    }
    function strftime(offsetObject) {
        return function(format) {
            var directives = format.match(/%(-|!)?[A-Z]{1}(:[^;]+;)?/gi);
            if (directives) {
                for (var i = 0, len = directives.length; i < len; ++i) {
                    var directive = directives[i].match(/%(-|!)?([a-zA-Z]{1})(:[^;]+;)?/), regexp = escapedRegExp(directive[0]), modifier = directive[1] || "", plural = directive[3] || "", value = null;
                    directive = directive[2];
                    if (DIRECTIVE_KEY_MAP.hasOwnProperty(directive)) {
                        value = DIRECTIVE_KEY_MAP[directive];
                        value = Number(offsetObject[value]);
                    }
                    if (value !== null) {
                        if (modifier === "!") {
                            value = pluralize(plural, value);
                        }
                        if (modifier === "") {
                            if (value < 10) {
                                value = "0" + value.toString();
                            }
                        }
                        format = format.replace(regexp, value.toString());
                    }
                }
            }
            format = format.replace(/%%/, "%");
            return format;
        };
    }
    function pluralize(format, count) {
        var plural = "s", singular = "";
        if (format) {
            format = format.replace(/(:|;|\s)/gi, "").split(/\,/);
            if (format.length === 1) {
                plural = format[0];
            } else {
                singular = format[0];
                plural = format[1];
            }
        }
        if (Math.abs(count) > 1) {
            return plural;
        } else {
            return singular;
        }
    }
    var Countdown = function(el, finalDate, options) {
        this.el = el;
        this.$el = $(el);
        this.interval = null;
        this.offset = {};
        this.options = $.extend({}, defaultOptions);
        this.instanceNumber = instances.length;
        instances.push(this);
        this.$el.data("countdown-instance", this.instanceNumber);
        if (options) {
            if (typeof options === "function") {
                this.$el.on("update.countdown", options);
                this.$el.on("stoped.countdown", options);
                this.$el.on("finish.countdown", options);
            } else {
                this.options = $.extend({}, defaultOptions, options);
            }
        }
        this.setFinalDate(finalDate);
        if (this.options.defer === false) {
            this.start();
        }
    };
    $.extend(Countdown.prototype, {
        start: function() {
            if (this.interval !== null) {
                clearInterval(this.interval);
            }
            var self = this;
            this.update();
            this.interval = setInterval(function() {
                self.update.call(self);
            }, this.options.precision);
        },
        stop: function() {
            clearInterval(this.interval);
            this.interval = null;
            this.dispatchEvent("stoped");
        },
        toggle: function() {
            if (this.interval) {
                this.stop();
            } else {
                this.start();
            }
        },
        pause: function() {
            this.stop();
        },
        resume: function() {
            this.start();
        },
        remove: function() {
            this.stop.call(this);
            instances[this.instanceNumber] = null;
            delete this.$el.data().countdownInstance;
        },
        setFinalDate: function(value) {
            this.finalDate = parseDateString(value);
        },
        update: function() {
            if (this.$el.closest("html").length === 0) {
                this.remove();
                return;
            }
            var hasEventsAttached = $._data(this.el, "events") !== undefined, now = new Date(), newTotalSecsLeft;
            newTotalSecsLeft = this.finalDate.getTime() - now.getTime();
            newTotalSecsLeft = Math.ceil(newTotalSecsLeft / 1e3);
            newTotalSecsLeft = !this.options.elapse && newTotalSecsLeft < 0 ? 0 : Math.abs(newTotalSecsLeft);
            if (this.totalSecsLeft === newTotalSecsLeft || !hasEventsAttached) {
                return;
            } else {
                this.totalSecsLeft = newTotalSecsLeft;
            }
            this.elapsed = now >= this.finalDate;
            this.offset = {
                seconds: this.totalSecsLeft % 60,
                minutes: Math.floor(this.totalSecsLeft / 60) % 60,
                hours: Math.floor(this.totalSecsLeft / 60 / 60) % 24,
                days: Math.floor(this.totalSecsLeft / 60 / 60 / 24) % 7,
                daysToWeek: Math.floor(this.totalSecsLeft / 60 / 60 / 24) % 7,
                daysToMonth: Math.floor(this.totalSecsLeft / 60 / 60 / 24 % 30.4368),
                weeks: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 7),
                weeksToMonth: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 7) % 4,
                months: Math.floor(this.totalSecsLeft / 60 / 60 / 24 / 30.4368),
                years: Math.abs(this.finalDate.getFullYear() - now.getFullYear()),
                totalDays: Math.floor(this.totalSecsLeft / 60 / 60 / 24),
                totalHours: Math.floor(this.totalSecsLeft / 60 / 60),
                totalMinutes: Math.floor(this.totalSecsLeft / 60),
                totalSeconds: this.totalSecsLeft
            };
            if (!this.options.elapse && this.totalSecsLeft === 0) {
                this.stop();
                this.dispatchEvent("finish");
            } else {
                this.dispatchEvent("update");
            }
        },
        dispatchEvent: function(eventName) {
            var event = $.Event(eventName + ".countdown");
            event.finalDate = this.finalDate;
            event.elapsed = this.elapsed;
            event.offset = $.extend({}, this.offset);
            event.strftime = strftime(this.offset);
            this.$el.trigger(event);
        }
    });
    $.fn.countdown = function() {
        var argumentsArray = Array.prototype.slice.call(arguments, 0);
        return this.each(function() {
            var instanceNumber = $(this).data("countdown-instance");
            if (instanceNumber !== undefined) {
                var instance = instances[instanceNumber], method = argumentsArray[0];
                if (Countdown.prototype.hasOwnProperty(method)) {
                    instance[method].apply(instance, argumentsArray.slice(1));
                } else if (String(method).match(/^[$A-Z_][0-9A-Z_$]*$/i) === null) {
                    instance.setFinalDate.call(instance, method);
                    instance.start();
                } else {
                    $.error("Method %s does not exist on jQuery.countdown".replace(/\%s/gi, method));
                }
            } else {
                new Countdown(this, argumentsArray[0], argumentsArray[1]);
            }
        });
    };
});

/***/ }),

/***/ "./node_modules/jquery/dist/jquery.js":
/*!********************************************!*\
  !*** ./node_modules/jquery/dist/jquery.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * jQuery JavaScript Library v3.5.1
 * https://jquery.com/
 *
 * Includes Sizzle.js
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2020-05-04T22:49Z
 */
( function( global, factory ) {

	"use strict";

	if (  true && typeof module.exports === "object" ) {

		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Edge <= 12 - 13+, Firefox <=18 - 45+, IE 10 - 11, Safari 5.1 - 9+, iOS 6 - 9.1
// throw exceptions when non-strict code (e.g., ASP.NET 4.5) accesses strict mode
// arguments.callee.caller (trac-13335). But as of jQuery 3.0 (2016), strict mode should be common
// enough that all such attempts are guarded in a try block.
"use strict";

var arr = [];

var getProto = Object.getPrototypeOf;

var slice = arr.slice;

var flat = arr.flat ? function( array ) {
	return arr.flat.call( array );
} : function( array ) {
	return arr.concat.apply( [], array );
};


var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var fnToString = hasOwn.toString;

var ObjectFunctionString = fnToString.call( Object );

var support = {};

var isFunction = function isFunction( obj ) {

      // Support: Chrome <=57, Firefox <=52
      // In some browsers, typeof returns "function" for HTML <object> elements
      // (i.e., `typeof document.createElement( "object" ) === "function"`).
      // We don't want to classify *any* DOM node as a function.
      return typeof obj === "function" && typeof obj.nodeType !== "number";
  };


var isWindow = function isWindow( obj ) {
		return obj != null && obj === obj.window;
	};


var document = window.document;



	var preservedScriptAttributes = {
		type: true,
		src: true,
		nonce: true,
		noModule: true
	};

	function DOMEval( code, node, doc ) {
		doc = doc || document;

		var i, val,
			script = doc.createElement( "script" );

		script.text = code;
		if ( node ) {
			for ( i in preservedScriptAttributes ) {

				// Support: Firefox 64+, Edge 18+
				// Some browsers don't support the "nonce" property on scripts.
				// On the other hand, just using `getAttribute` is not enough as
				// the `nonce` attribute is reset to an empty string whenever it
				// becomes browsing-context connected.
				// See https://github.com/whatwg/html/issues/2369
				// See https://html.spec.whatwg.org/#nonce-attributes
				// The `node.getAttribute` check was added for the sake of
				// `jQuery.globalEval` so that it can fake a nonce-containing node
				// via an object.
				val = node[ i ] || node.getAttribute && node.getAttribute( i );
				if ( val ) {
					script.setAttribute( i, val );
				}
			}
		}
		doc.head.appendChild( script ).parentNode.removeChild( script );
	}


function toType( obj ) {
	if ( obj == null ) {
		return obj + "";
	}

	// Support: Android <=2.3 only (functionish RegExp)
	return typeof obj === "object" || typeof obj === "function" ?
		class2type[ toString.call( obj ) ] || "object" :
		typeof obj;
}
/* global Symbol */
// Defining this global in .eslintrc.json would create a danger of using the global
// unguarded in another place, it seems safer to define global only for this module



var
	version = "3.5.1",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {

		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	};

jQuery.fn = jQuery.prototype = {

	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {

		// Return all the elements in a clean array
		if ( num == null ) {
			return slice.call( this );
		}

		// Return just the one element from the set
		return num < 0 ? this[ num + this.length ] : this[ num ];
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	each: function( callback ) {
		return jQuery.each( this, callback );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map( this, function( elem, i ) {
			return callback.call( elem, i, elem );
		} ) );
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	even: function() {
		return this.pushStack( jQuery.grep( this, function( _elem, i ) {
			return ( i + 1 ) % 2;
		} ) );
	},

	odd: function() {
		return this.pushStack( jQuery.grep( this, function( _elem, i ) {
			return i % 2;
		} ) );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor();
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[ 0 ] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !isFunction( target ) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {

		// Only deal with non-null/undefined values
		if ( ( options = arguments[ i ] ) != null ) {

			// Extend the base object
			for ( name in options ) {
				copy = options[ name ];

				// Prevent Object.prototype pollution
				// Prevent never-ending loop
				if ( name === "__proto__" || target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
					( copyIsArray = Array.isArray( copy ) ) ) ) {
					src = target[ name ];

					// Ensure proper type for the source value
					if ( copyIsArray && !Array.isArray( src ) ) {
						clone = [];
					} else if ( !copyIsArray && !jQuery.isPlainObject( src ) ) {
						clone = {};
					} else {
						clone = src;
					}
					copyIsArray = false;

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend( {

	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isPlainObject: function( obj ) {
		var proto, Ctor;

		// Detect obvious negatives
		// Use toString instead of jQuery.type to catch host objects
		if ( !obj || toString.call( obj ) !== "[object Object]" ) {
			return false;
		}

		proto = getProto( obj );

		// Objects with no prototype (e.g., `Object.create( null )`) are plain
		if ( !proto ) {
			return true;
		}

		// Objects with prototype are plain iff they were constructed by a global Object function
		Ctor = hasOwn.call( proto, "constructor" ) && proto.constructor;
		return typeof Ctor === "function" && fnToString.call( Ctor ) === ObjectFunctionString;
	},

	isEmptyObject: function( obj ) {
		var name;

		for ( name in obj ) {
			return false;
		}
		return true;
	},

	// Evaluates a script in a provided context; falls back to the global one
	// if not specified.
	globalEval: function( code, options, doc ) {
		DOMEval( code, { nonce: options && options.nonce }, doc );
	},

	each: function( obj, callback ) {
		var length, i = 0;

		if ( isArrayLike( obj ) ) {
			length = obj.length;
			for ( ; i < length; i++ ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		} else {
			for ( i in obj ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		}

		return obj;
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArrayLike( Object( arr ) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	// Support: Android <=4.0 only, PhantomJS 1 only
	// push.apply(_, arraylike) throws on ancient WebKit
	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var length, value,
			i = 0,
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArrayLike( elems ) ) {
			length = elems.length;
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return flat( ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
} );

if ( typeof Symbol === "function" ) {
	jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}

// Populate the class2type map
jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
function( _i, name ) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
} );

function isArrayLike( obj ) {

	// Support: real iOS 8.2 only (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = !!obj && "length" in obj && obj.length,
		type = toType( obj );

	if ( isFunction( obj ) || isWindow( obj ) ) {
		return false;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.3.5
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://js.foundation/
 *
 * Date: 2020-03-14
 */
( function( window ) {
var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	nonnativeSelectorCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// Instance methods
	hasOwn = ( {} ).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	pushNative = arr.push,
	push = arr.push,
	slice = arr.slice,

	// Use a stripped-down indexOf as it's faster than native
	// https://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[ i ] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|" +
		"ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",

	// https://www.w3.org/TR/css-syntax-3/#ident-token-diagram
	identifier = "(?:\\\\[\\da-fA-F]{1,6}" + whitespace +
		"?|\\\\[^\\r\\n\\f]|[\\w-]|[^\0-\\x7f])+",

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +

		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +

		// "Attribute values must be CSS identifiers [capture 5]
		// or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" +
		whitespace + "*\\]",

	pseudos = ":(" + identifier + ")(?:\\((" +

		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +

		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +

		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" +
		whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace +
		"*" ),
	rdescend = new RegExp( whitespace + "|>" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + identifier + ")" ),
		"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
		"TAG": new RegExp( "^(" + identifier + "|[*])" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" +
			whitespace + "*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" +
			whitespace + "*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),

		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace +
			"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + whitespace +
			"*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rhtml = /HTML$/i,
	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,

	// CSS escapes
	// http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\[\\da-fA-F]{1,6}" + whitespace + "?|\\\\([^\\r\\n\\f])", "g" ),
	funescape = function( escape, nonHex ) {
		var high = "0x" + escape.slice( 1 ) - 0x10000;

		return nonHex ?

			// Strip the backslash prefix from a non-hex escape sequence
			nonHex :

			// Replace a hexadecimal escape sequence with the encoded Unicode code point
			// Support: IE <=11+
			// For values outside the Basic Multilingual Plane (BMP), manually construct a
			// surrogate pair
			high < 0 ?
				String.fromCharCode( high + 0x10000 ) :
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// CSS string/identifier serialization
	// https://drafts.csswg.org/cssom/#common-serializing-idioms
	rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
	fcssescape = function( ch, asCodePoint ) {
		if ( asCodePoint ) {

			// U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
			if ( ch === "\0" ) {
				return "\uFFFD";
			}

			// Control characters and (dependent upon position) numbers get escaped as code points
			return ch.slice( 0, -1 ) + "\\" +
				ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
		}

		// Other potentially-special ASCII characters get backslash-escaped
		return "\\" + ch;
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	},

	inDisabledFieldset = addCombinator(
		function( elem ) {
			return elem.disabled === true && elem.nodeName.toLowerCase() === "fieldset";
		},
		{ dir: "parentNode", next: "legend" }
	);

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		( arr = slice.call( preferredDoc.childNodes ) ),
		preferredDoc.childNodes
	);

	// Support: Android<4.0
	// Detect silently failing push.apply
	// eslint-disable-next-line no-unused-expressions
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			pushNative.apply( target, slice.call( els ) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;

			// Can't trust NodeList.length
			while ( ( target[ j++ ] = els[ i++ ] ) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var m, i, elem, nid, match, groups, newSelector,
		newContext = context && context.ownerDocument,

		// nodeType defaults to 9, since context defaults to document
		nodeType = context ? context.nodeType : 9;

	results = results || [];

	// Return early from calls with invalid selector or context
	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	// Try to shortcut find operations (as opposed to filters) in HTML documents
	if ( !seed ) {
		setDocument( context );
		context = context || document;

		if ( documentIsHTML ) {

			// If the selector is sufficiently simple, try using a "get*By*" DOM method
			// (excepting DocumentFragment context, where the methods don't exist)
			if ( nodeType !== 11 && ( match = rquickExpr.exec( selector ) ) ) {

				// ID selector
				if ( ( m = match[ 1 ] ) ) {

					// Document context
					if ( nodeType === 9 ) {
						if ( ( elem = context.getElementById( m ) ) ) {

							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( elem.id === m ) {
								results.push( elem );
								return results;
							}
						} else {
							return results;
						}

					// Element context
					} else {

						// Support: IE, Opera, Webkit
						// TODO: identify versions
						// getElementById can match elements by name instead of ID
						if ( newContext && ( elem = newContext.getElementById( m ) ) &&
							contains( context, elem ) &&
							elem.id === m ) {

							results.push( elem );
							return results;
						}
					}

				// Type selector
				} else if ( match[ 2 ] ) {
					push.apply( results, context.getElementsByTagName( selector ) );
					return results;

				// Class selector
				} else if ( ( m = match[ 3 ] ) && support.getElementsByClassName &&
					context.getElementsByClassName ) {

					push.apply( results, context.getElementsByClassName( m ) );
					return results;
				}
			}

			// Take advantage of querySelectorAll
			if ( support.qsa &&
				!nonnativeSelectorCache[ selector + " " ] &&
				( !rbuggyQSA || !rbuggyQSA.test( selector ) ) &&

				// Support: IE 8 only
				// Exclude object elements
				( nodeType !== 1 || context.nodeName.toLowerCase() !== "object" ) ) {

				newSelector = selector;
				newContext = context;

				// qSA considers elements outside a scoping root when evaluating child or
				// descendant combinators, which is not what we want.
				// In such cases, we work around the behavior by prefixing every selector in the
				// list with an ID selector referencing the scope context.
				// The technique has to be used as well when a leading combinator is used
				// as such selectors are not recognized by querySelectorAll.
				// Thanks to Andrew Dupont for this technique.
				if ( nodeType === 1 &&
					( rdescend.test( selector ) || rcombinators.test( selector ) ) ) {

					// Expand context for sibling selectors
					newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
						context;

					// We can use :scope instead of the ID hack if the browser
					// supports it & if we're not changing the context.
					if ( newContext !== context || !support.scope ) {

						// Capture the context ID, setting it first if necessary
						if ( ( nid = context.getAttribute( "id" ) ) ) {
							nid = nid.replace( rcssescape, fcssescape );
						} else {
							context.setAttribute( "id", ( nid = expando ) );
						}
					}

					// Prefix every selector in the list
					groups = tokenize( selector );
					i = groups.length;
					while ( i-- ) {
						groups[ i ] = ( nid ? "#" + nid : ":scope" ) + " " +
							toSelector( groups[ i ] );
					}
					newSelector = groups.join( "," );
				}

				try {
					push.apply( results,
						newContext.querySelectorAll( newSelector )
					);
					return results;
				} catch ( qsaError ) {
					nonnativeSelectorCache( selector, true );
				} finally {
					if ( nid === expando ) {
						context.removeAttribute( "id" );
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {function(string, object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {

		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {

			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return ( cache[ key + " " ] = value );
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created element and returns a boolean result
 */
function assert( fn ) {
	var el = document.createElement( "fieldset" );

	try {
		return !!fn( el );
	} catch ( e ) {
		return false;
	} finally {

		// Remove from its parent by default
		if ( el.parentNode ) {
			el.parentNode.removeChild( el );
		}

		// release memory in IE
		el = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split( "|" ),
		i = arr.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[ i ] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			a.sourceIndex - b.sourceIndex;

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( ( cur = cur.nextSibling ) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return ( name === "input" || name === "button" ) && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for :enabled/:disabled
 * @param {Boolean} disabled true for :disabled; false for :enabled
 */
function createDisabledPseudo( disabled ) {

	// Known :disabled false positives: fieldset[disabled] > legend:nth-of-type(n+2) :can-disable
	return function( elem ) {

		// Only certain elements can match :enabled or :disabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-enabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-disabled
		if ( "form" in elem ) {

			// Check for inherited disabledness on relevant non-disabled elements:
			// * listed form-associated elements in a disabled fieldset
			//   https://html.spec.whatwg.org/multipage/forms.html#category-listed
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-fe-disabled
			// * option elements in a disabled optgroup
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-option-disabled
			// All such elements have a "form" property.
			if ( elem.parentNode && elem.disabled === false ) {

				// Option elements defer to a parent optgroup if present
				if ( "label" in elem ) {
					if ( "label" in elem.parentNode ) {
						return elem.parentNode.disabled === disabled;
					} else {
						return elem.disabled === disabled;
					}
				}

				// Support: IE 6 - 11
				// Use the isDisabled shortcut property to check for disabled fieldset ancestors
				return elem.isDisabled === disabled ||

					// Where there is no isDisabled, check manually
					/* jshint -W018 */
					elem.isDisabled !== !disabled &&
					inDisabledFieldset( elem ) === disabled;
			}

			return elem.disabled === disabled;

		// Try to winnow out elements that can't be disabled before trusting the disabled property.
		// Some victims get caught in our net (label, legend, menu, track), but it shouldn't
		// even exist on them, let alone have a boolean value.
		} else if ( "label" in elem ) {
			return elem.disabled === disabled;
		}

		// Remaining elements are neither :enabled nor :disabled
		return false;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction( function( argument ) {
		argument = +argument;
		return markFunction( function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ ( j = matchIndexes[ i ] ) ] ) {
					seed[ j ] = !( matches[ j ] = seed[ j ] );
				}
			}
		} );
	} );
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	var namespace = elem.namespaceURI,
		docElem = ( elem.ownerDocument || elem ).documentElement;

	// Support: IE <=8
	// Assume HTML when documentElement doesn't yet exist, such as inside loading iframes
	// https://bugs.jquery.com/ticket/4833
	return !rhtml.test( namespace || docElem && docElem.nodeName || "HTML" );
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, subWindow,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// Return early if doc is invalid or already selected
	// Support: IE 11+, Edge 17 - 18+
	// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
	// two documents; shallow comparisons work.
	// eslint-disable-next-line eqeqeq
	if ( doc == document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Update global variables
	document = doc;
	docElem = document.documentElement;
	documentIsHTML = !isXML( document );

	// Support: IE 9 - 11+, Edge 12 - 18+
	// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
	// Support: IE 11+, Edge 17 - 18+
	// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
	// two documents; shallow comparisons work.
	// eslint-disable-next-line eqeqeq
	if ( preferredDoc != document &&
		( subWindow = document.defaultView ) && subWindow.top !== subWindow ) {

		// Support: IE 11, Edge
		if ( subWindow.addEventListener ) {
			subWindow.addEventListener( "unload", unloadHandler, false );

		// Support: IE 9 - 10 only
		} else if ( subWindow.attachEvent ) {
			subWindow.attachEvent( "onunload", unloadHandler );
		}
	}

	// Support: IE 8 - 11+, Edge 12 - 18+, Chrome <=16 - 25 only, Firefox <=3.6 - 31 only,
	// Safari 4 - 5 only, Opera <=11.6 - 12.x only
	// IE/Edge & older browsers don't support the :scope pseudo-class.
	// Support: Safari 6.0 only
	// Safari 6.0 supports :scope but it's an alias of :root there.
	support.scope = assert( function( el ) {
		docElem.appendChild( el ).appendChild( document.createElement( "div" ) );
		return typeof el.querySelectorAll !== "undefined" &&
			!el.querySelectorAll( ":scope fieldset div" ).length;
	} );

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert( function( el ) {
		el.className = "i";
		return !el.getAttribute( "className" );
	} );

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert( function( el ) {
		el.appendChild( document.createComment( "" ) );
		return !el.getElementsByTagName( "*" ).length;
	} );

	// Support: IE<9
	support.getElementsByClassName = rnative.test( document.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programmatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert( function( el ) {
		docElem.appendChild( el ).id = expando;
		return !document.getElementsByName || !document.getElementsByName( expando ).length;
	} );

	// ID filter and find
	if ( support.getById ) {
		Expr.filter[ "ID" ] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute( "id" ) === attrId;
			};
		};
		Expr.find[ "ID" ] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var elem = context.getElementById( id );
				return elem ? [ elem ] : [];
			}
		};
	} else {
		Expr.filter[ "ID" ] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" &&
					elem.getAttributeNode( "id" );
				return node && node.value === attrId;
			};
		};

		// Support: IE 6 - 7 only
		// getElementById is not reliable as a find shortcut
		Expr.find[ "ID" ] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var node, i, elems,
					elem = context.getElementById( id );

				if ( elem ) {

					// Verify the id attribute
					node = elem.getAttributeNode( "id" );
					if ( node && node.value === id ) {
						return [ elem ];
					}

					// Fall back on getElementsByName
					elems = context.getElementsByName( id );
					i = 0;
					while ( ( elem = elems[ i++ ] ) ) {
						node = elem.getAttributeNode( "id" );
						if ( node && node.value === id ) {
							return [ elem ];
						}
					}
				}

				return [];
			}
		};
	}

	// Tag
	Expr.find[ "TAG" ] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,

				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( ( elem = results[ i++ ] ) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find[ "CLASS" ] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See https://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( ( support.qsa = rnative.test( document.querySelectorAll ) ) ) {

		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert( function( el ) {

			var input;

			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// https://bugs.jquery.com/ticket/12359
			docElem.appendChild( el ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\r\\' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// https://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( el.querySelectorAll( "[msallowcapture^='']" ).length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !el.querySelectorAll( "[selected]" ).length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
			if ( !el.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push( "~=" );
			}

			// Support: IE 11+, Edge 15 - 18+
			// IE 11/Edge don't find elements on a `[name='']` query in some cases.
			// Adding a temporary attribute to the document before the selection works
			// around the issue.
			// Interestingly, IE 10 & older don't seem to have the issue.
			input = document.createElement( "input" );
			input.setAttribute( "name", "" );
			el.appendChild( input );
			if ( !el.querySelectorAll( "[name='']" ).length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*name" + whitespace + "*=" +
					whitespace + "*(?:''|\"\")" );
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !el.querySelectorAll( ":checked" ).length ) {
				rbuggyQSA.push( ":checked" );
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibling-combinator selector` fails
			if ( !el.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push( ".#.+[+~]" );
			}

			// Support: Firefox <=3.6 - 5 only
			// Old Firefox doesn't throw on a badly-escaped identifier.
			el.querySelectorAll( "\\\f" );
			rbuggyQSA.push( "[\\r\\n\\f]" );
		} );

		assert( function( el ) {
			el.innerHTML = "<a href='' disabled='disabled'></a>" +
				"<select disabled='disabled'><option/></select>";

			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = document.createElement( "input" );
			input.setAttribute( "type", "hidden" );
			el.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( el.querySelectorAll( "[name=d]" ).length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( el.querySelectorAll( ":enabled" ).length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Support: IE9-11+
			// IE's :disabled selector does not pick up the children of disabled fieldsets
			docElem.appendChild( el ).disabled = true;
			if ( el.querySelectorAll( ":disabled" ).length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Support: Opera 10 - 11 only
			// Opera 10-11 does not throw on post-comma invalid pseudos
			el.querySelectorAll( "*,:x" );
			rbuggyQSA.push( ",.*:" );
		} );
	}

	if ( ( support.matchesSelector = rnative.test( ( matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector ) ) ) ) {

		assert( function( el ) {

			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( el, "*" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( el, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		} );
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join( "|" ) );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join( "|" ) );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully self-exclusive
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			) );
		} :
		function( a, b ) {
			if ( b ) {
				while ( ( b = b.parentNode ) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		// Support: IE 11+, Edge 17 - 18+
		// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
		// two documents; shallow comparisons work.
		// eslint-disable-next-line eqeqeq
		compare = ( a.ownerDocument || a ) == ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			( !support.sortDetached && b.compareDocumentPosition( a ) === compare ) ) {

			// Choose the first element that is related to our preferred document
			// Support: IE 11+, Edge 17 - 18+
			// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
			// two documents; shallow comparisons work.
			// eslint-disable-next-line eqeqeq
			if ( a == document || a.ownerDocument == preferredDoc &&
				contains( preferredDoc, a ) ) {
				return -1;
			}

			// Support: IE 11+, Edge 17 - 18+
			// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
			// two documents; shallow comparisons work.
			// eslint-disable-next-line eqeqeq
			if ( b == document || b.ownerDocument == preferredDoc &&
				contains( preferredDoc, b ) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {

		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {

			// Support: IE 11+, Edge 17 - 18+
			// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
			// two documents; shallow comparisons work.
			/* eslint-disable eqeqeq */
			return a == document ? -1 :
				b == document ? 1 :
				/* eslint-enable eqeqeq */
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( ( cur = cur.parentNode ) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( ( cur = cur.parentNode ) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[ i ] === bp[ i ] ) {
			i++;
		}

		return i ?

			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[ i ], bp[ i ] ) :

			// Otherwise nodes in our document sort first
			// Support: IE 11+, Edge 17 - 18+
			// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
			// two documents; shallow comparisons work.
			/* eslint-disable eqeqeq */
			ap[ i ] == preferredDoc ? -1 :
			bp[ i ] == preferredDoc ? 1 :
			/* eslint-enable eqeqeq */
			0;
	};

	return document;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	setDocument( elem );

	if ( support.matchesSelector && documentIsHTML &&
		!nonnativeSelectorCache[ expr + " " ] &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||

				// As well, disconnected nodes are said to be in a document
				// fragment in IE 9
				elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch ( e ) {
			nonnativeSelectorCache( expr, true );
		}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {

	// Set document vars if needed
	// Support: IE 11+, Edge 17 - 18+
	// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
	// two documents; shallow comparisons work.
	// eslint-disable-next-line eqeqeq
	if ( ( context.ownerDocument || context ) != document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {

	// Set document vars if needed
	// Support: IE 11+, Edge 17 - 18+
	// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
	// two documents; shallow comparisons work.
	// eslint-disable-next-line eqeqeq
	if ( ( elem.ownerDocument || elem ) != document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],

		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			( val = elem.getAttributeNode( name ) ) && val.specified ?
				val.value :
				null;
};

Sizzle.escape = function( sel ) {
	return ( sel + "" ).replace( rcssescape, fcssescape );
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( ( elem = results[ i++ ] ) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {

		// If no nodeType, this is expected to be an array
		while ( ( node = elem[ i++ ] ) ) {

			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {

		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {

			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}

	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[ 1 ] = match[ 1 ].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[ 3 ] = ( match[ 3 ] || match[ 4 ] ||
				match[ 5 ] || "" ).replace( runescape, funescape );

			if ( match[ 2 ] === "~=" ) {
				match[ 3 ] = " " + match[ 3 ] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {

			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[ 1 ] = match[ 1 ].toLowerCase();

			if ( match[ 1 ].slice( 0, 3 ) === "nth" ) {

				// nth-* requires argument
				if ( !match[ 3 ] ) {
					Sizzle.error( match[ 0 ] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[ 4 ] = +( match[ 4 ] ?
					match[ 5 ] + ( match[ 6 ] || 1 ) :
					2 * ( match[ 3 ] === "even" || match[ 3 ] === "odd" ) );
				match[ 5 ] = +( ( match[ 7 ] + match[ 8 ] ) || match[ 3 ] === "odd" );

				// other types prohibit arguments
			} else if ( match[ 3 ] ) {
				Sizzle.error( match[ 0 ] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[ 6 ] && match[ 2 ];

			if ( matchExpr[ "CHILD" ].test( match[ 0 ] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[ 3 ] ) {
				match[ 2 ] = match[ 4 ] || match[ 5 ] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&

				// Get excess from tokenize (recursively)
				( excess = tokenize( unquoted, true ) ) &&

				// advance to the next closing parenthesis
				( excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length ) ) {

				// excess is a negative index
				match[ 0 ] = match[ 0 ].slice( 0, excess );
				match[ 2 ] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() {
					return true;
				} :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				( pattern = new RegExp( "(^|" + whitespace +
					")" + className + "(" + whitespace + "|$)" ) ) && classCache(
						className, function( elem ) {
							return pattern.test(
								typeof elem.className === "string" && elem.className ||
								typeof elem.getAttribute !== "undefined" &&
									elem.getAttribute( "class" ) ||
								""
							);
				} );
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				/* eslint-disable max-len */

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
				/* eslint-enable max-len */

			};
		},

		"CHILD": function( type, what, _argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, _context, xml ) {
					var cache, uniqueCache, outerCache, node, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType,
						diff = false;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( ( node = node[ dir ] ) ) {
									if ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) {

										return false;
									}
								}

								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {

							// Seek `elem` from a previously-cached index

							// ...in a gzip-friendly way
							node = parent;
							outerCache = node[ expando ] || ( node[ expando ] = {} );

							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ node.uniqueID ] ||
								( outerCache[ node.uniqueID ] = {} );

							cache = uniqueCache[ type ] || [];
							nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
							diff = nodeIndex && cache[ 2 ];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( ( node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								( diff = nodeIndex = 0 ) || start.pop() ) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						} else {

							// Use previously-cached element index if available
							if ( useCache ) {

								// ...in a gzip-friendly way
								node = elem;
								outerCache = node[ expando ] || ( node[ expando ] = {} );

								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
									( outerCache[ node.uniqueID ] = {} );

								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex;
							}

							// xml :nth-child(...)
							// or :nth-last-child(...) or :nth(-last)?-of-type(...)
							if ( diff === false ) {

								// Use the same loop as above to seek `elem` from the start
								while ( ( node = ++nodeIndex && node && node[ dir ] ||
									( diff = nodeIndex = 0 ) || start.pop() ) ) {

									if ( ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) &&
										++diff ) {

										// Cache the index of each encountered element
										if ( useCache ) {
											outerCache = node[ expando ] ||
												( node[ expando ] = {} );

											// Support: IE <9 only
											// Defend against cloned attroperties (jQuery gh-1709)
											uniqueCache = outerCache[ node.uniqueID ] ||
												( outerCache[ node.uniqueID ] = {} );

											uniqueCache[ type ] = [ dirruns, diff ];
										}

										if ( node === elem ) {
											break;
										}
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {

			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction( function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[ i ] );
							seed[ idx ] = !( matches[ idx ] = matched[ i ] );
						}
					} ) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {

		// Potentially complex pseudos
		"not": markFunction( function( selector ) {

			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction( function( seed, matches, _context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( ( elem = unmatched[ i ] ) ) {
							seed[ i ] = !( matches[ i ] = elem );
						}
					}
				} ) :
				function( elem, _context, xml ) {
					input[ 0 ] = elem;
					matcher( input, null, xml, results );

					// Don't keep the element (issue #299)
					input[ 0 ] = null;
					return !results.pop();
				};
		} ),

		"has": markFunction( function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		} ),

		"contains": markFunction( function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || getText( elem ) ).indexOf( text ) > -1;
			};
		} ),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {

			// lang value must be a valid identifier
			if ( !ridentifier.test( lang || "" ) ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( ( elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute( "xml:lang" ) || elem.getAttribute( "lang" ) ) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( ( elem = elem.parentNode ) && elem.nodeType === 1 );
				return false;
			};
		} ),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement &&
				( !document.hasFocus || document.hasFocus() ) &&
				!!( elem.type || elem.href || ~elem.tabIndex );
		},

		// Boolean properties
		"enabled": createDisabledPseudo( false ),
		"disabled": createDisabledPseudo( true ),

		"checked": function( elem ) {

			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return ( nodeName === "input" && !!elem.checked ) ||
				( nodeName === "option" && !!elem.selected );
		},

		"selected": function( elem ) {

			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				// eslint-disable-next-line no-unused-expressions
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {

			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos[ "empty" ]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( ( attr = elem.getAttribute( "type" ) ) == null ||
					attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo( function() {
			return [ 0 ];
		} ),

		"last": createPositionalPseudo( function( _matchIndexes, length ) {
			return [ length - 1 ];
		} ),

		"eq": createPositionalPseudo( function( _matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		} ),

		"even": createPositionalPseudo( function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		} ),

		"odd": createPositionalPseudo( function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		} ),

		"lt": createPositionalPseudo( function( matchIndexes, length, argument ) {
			var i = argument < 0 ?
				argument + length :
				argument > length ?
					length :
					argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		} ),

		"gt": createPositionalPseudo( function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		} )
	}
};

Expr.pseudos[ "nth" ] = Expr.pseudos[ "eq" ];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || ( match = rcomma.exec( soFar ) ) ) {
			if ( match ) {

				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[ 0 ].length ) || soFar;
			}
			groups.push( ( tokens = [] ) );
		}

		matched = false;

		// Combinators
		if ( ( match = rcombinators.exec( soFar ) ) ) {
			matched = match.shift();
			tokens.push( {
				value: matched,

				// Cast descendant combinators to space
				type: match[ 0 ].replace( rtrim, " " )
			} );
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( ( match = matchExpr[ type ].exec( soFar ) ) && ( !preFilters[ type ] ||
				( match = preFilters[ type ]( match ) ) ) ) {
				matched = match.shift();
				tokens.push( {
					value: matched,
					type: type,
					matches: match
				} );
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :

			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[ i ].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		skip = combinator.next,
		key = skip || dir,
		checkNonElements = base && key === "parentNode",
		doneName = done++;

	return combinator.first ?

		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( ( elem = elem[ dir ] ) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
			return false;
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, uniqueCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
			if ( xml ) {
				while ( ( elem = elem[ dir ] ) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( ( elem = elem[ dir ] ) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || ( elem[ expando ] = {} );

						// Support: IE <9 only
						// Defend against cloned attroperties (jQuery gh-1709)
						uniqueCache = outerCache[ elem.uniqueID ] ||
							( outerCache[ elem.uniqueID ] = {} );

						if ( skip && skip === elem.nodeName.toLowerCase() ) {
							elem = elem[ dir ] || elem;
						} else if ( ( oldCache = uniqueCache[ key ] ) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return ( newCache[ 2 ] = oldCache[ 2 ] );
						} else {

							// Reuse newcache so results back-propagate to previous elements
							uniqueCache[ key ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( ( newCache[ 2 ] = matcher( elem, context, xml ) ) ) {
								return true;
							}
						}
					}
				}
			}
			return false;
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[ i ]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[ 0 ];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[ i ], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( ( elem = unmatched[ i ] ) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction( function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts(
				selector || "*",
				context.nodeType ? [ context ] : context,
				[]
			),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?

				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( ( elem = temp[ i ] ) ) {
					matcherOut[ postMap[ i ] ] = !( matcherIn[ postMap[ i ] ] = elem );
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {

					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( ( elem = matcherOut[ i ] ) ) {

							// Restore matcherIn since elem is not yet a final match
							temp.push( ( matcherIn[ i ] = elem ) );
						}
					}
					postFinder( null, ( matcherOut = [] ), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( ( elem = matcherOut[ i ] ) &&
						( temp = postFinder ? indexOf( seed, elem ) : preMap[ i ] ) > -1 ) {

						seed[ temp ] = !( results[ temp ] = elem );
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	} );
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[ 0 ].type ],
		implicitRelative = leadingRelative || Expr.relative[ " " ],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				( checkContext = context ).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );

			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( ( matcher = Expr.relative[ tokens[ i ].type ] ) ) {
			matchers = [ addCombinator( elementMatcher( matchers ), matcher ) ];
		} else {
			matcher = Expr.filter[ tokens[ i ].type ].apply( null, tokens[ i ].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {

				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[ j ].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(

					// If the preceding token was a descendant combinator, insert an implicit any-element `*`
					tokens
						.slice( 0, i - 1 )
						.concat( { value: tokens[ i - 2 ].type === " " ? "*" : "" } )
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( ( tokens = tokens.slice( j ) ) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,

				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find[ "TAG" ]( "*", outermost ),

				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = ( dirruns += contextBackup == null ? 1 : Math.random() || 0.1 ),
				len = elems.length;

			if ( outermost ) {

				// Support: IE 11+, Edge 17 - 18+
				// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
				// two documents; shallow comparisons work.
				// eslint-disable-next-line eqeqeq
				outermostContext = context == document || context || outermost;
			}

			// Add elements passing elementMatchers directly to results
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && ( elem = elems[ i ] ) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;

					// Support: IE 11+, Edge 17 - 18+
					// IE/Edge sometimes throw a "Permission denied" error when strict-comparing
					// two documents; shallow comparisons work.
					// eslint-disable-next-line eqeqeq
					if ( !context && elem.ownerDocument != document ) {
						setDocument( elem );
						xml = !documentIsHTML;
					}
					while ( ( matcher = elementMatchers[ j++ ] ) ) {
						if ( matcher( elem, context || document, xml ) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {

					// They will have gone through all possible matchers
					if ( ( elem = !matcher && elem ) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// `i` is now the count of elements visited above, and adding it to `matchedCount`
			// makes the latter nonnegative.
			matchedCount += i;

			// Apply set filters to unmatched elements
			// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
			// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
			// no element matchers and no seed.
			// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
			// case, which will result in a "00" `matchedCount` that differs from `i` but is also
			// numerically zero.
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( ( matcher = setMatchers[ j++ ] ) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {

					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !( unmatched[ i ] || setMatched[ i ] ) ) {
								setMatched[ i ] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {

		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[ i ] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache(
			selector,
			matcherFromGroupMatchers( elementMatchers, setMatchers )
		);

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( ( selector = compiled.selector || selector ) );

	results = results || [];

	// Try to minimize operations if there is only one selector in the list and no seed
	// (the latter of which guarantees us context)
	if ( match.length === 1 ) {

		// Reduce context if the leading compound selector is an ID
		tokens = match[ 0 ] = match[ 0 ].slice( 0 );
		if ( tokens.length > 2 && ( token = tokens[ 0 ] ).type === "ID" &&
			context.nodeType === 9 && documentIsHTML && Expr.relative[ tokens[ 1 ].type ] ) {

			context = ( Expr.find[ "ID" ]( token.matches[ 0 ]
				.replace( runescape, funescape ), context ) || [] )[ 0 ];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr[ "needsContext" ].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[ i ];

			// Abort if we hit a combinator
			if ( Expr.relative[ ( type = token.type ) ] ) {
				break;
			}
			if ( ( find = Expr.find[ type ] ) ) {

				// Search, expanding context for leading sibling combinators
				if ( ( seed = find(
					token.matches[ 0 ].replace( runescape, funescape ),
					rsibling.test( tokens[ 0 ].type ) && testContext( context.parentNode ) ||
						context
				) ) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split( "" ).sort( sortOrder ).join( "" ) === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert( function( el ) {

	// Should return 1, but returns 4 (following)
	return el.compareDocumentPosition( document.createElement( "fieldset" ) ) & 1;
} );

// Support: IE<8
// Prevent attribute/property "interpolation"
// https://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert( function( el ) {
	el.innerHTML = "<a href='#'></a>";
	return el.firstChild.getAttribute( "href" ) === "#";
} ) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	} );
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert( function( el ) {
	el.innerHTML = "<input/>";
	el.firstChild.setAttribute( "value", "" );
	return el.firstChild.getAttribute( "value" ) === "";
} ) ) {
	addHandle( "value", function( elem, _name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	} );
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert( function( el ) {
	return el.getAttribute( "disabled" ) == null;
} ) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
				( val = elem.getAttributeNode( name ) ) && val.specified ?
					val.value :
					null;
		}
	} );
}

return Sizzle;

} )( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;

// Deprecated
jQuery.expr[ ":" ] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;
jQuery.escapeSelector = Sizzle.escape;




var dir = function( elem, dir, until ) {
	var matched = [],
		truncate = until !== undefined;

	while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
		if ( elem.nodeType === 1 ) {
			if ( truncate && jQuery( elem ).is( until ) ) {
				break;
			}
			matched.push( elem );
		}
	}
	return matched;
};


var siblings = function( n, elem ) {
	var matched = [];

	for ( ; n; n = n.nextSibling ) {
		if ( n.nodeType === 1 && n !== elem ) {
			matched.push( n );
		}
	}

	return matched;
};


var rneedsContext = jQuery.expr.match.needsContext;



function nodeName( elem, name ) {

  return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();

};
var rsingleTag = ( /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i );



// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			return !!qualifier.call( elem, i, elem ) !== not;
		} );
	}

	// Single element
	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		} );
	}

	// Arraylike of elements (jQuery, arguments, Array)
	if ( typeof qualifier !== "string" ) {
		return jQuery.grep( elements, function( elem ) {
			return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
		} );
	}

	// Filtered directly for both simple and complex selectors
	return jQuery.filter( qualifier, elements, not );
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	if ( elems.length === 1 && elem.nodeType === 1 ) {
		return jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [];
	}

	return jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
		return elem.nodeType === 1;
	} ) );
};

jQuery.fn.extend( {
	find: function( selector ) {
		var i, ret,
			len = this.length,
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter( function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			} ) );
		}

		ret = this.pushStack( [] );

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		return len > 1 ? jQuery.uniqueSort( ret ) : ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow( this, selector || [], false ) );
	},
	not: function( selector ) {
		return this.pushStack( winnow( this, selector || [], true ) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
} );


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	// Shortcut simple #id case for speed
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,

	init = jQuery.fn.init = function( selector, context, root ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Method init() accepts an alternate rootjQuery
		// so migrate can support jQuery.sub (gh-2101)
		root = root || rootjQuery;

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[ 0 ] === "<" &&
				selector[ selector.length - 1 ] === ">" &&
				selector.length >= 3 ) {

				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && ( match[ 1 ] || !context ) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[ 1 ] ) {
					context = context instanceof jQuery ? context[ 0 ] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[ 1 ],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {

							// Properties of context are called as methods if possible
							if ( isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[ 2 ] );

					if ( elem ) {

						// Inject the element directly into the jQuery object
						this[ 0 ] = elem;
						this.length = 1;
					}
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || root ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this[ 0 ] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( isFunction( selector ) ) {
			return root.ready !== undefined ?
				root.ready( selector ) :

				// Execute immediately if ready is not present
				selector( jQuery );
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,

	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.fn.extend( {
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter( function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[ i ] ) ) {
					return true;
				}
			}
		} );
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			targets = typeof selectors !== "string" && jQuery( selectors );

		// Positional selectors never match, since there's no _selection_ context
		if ( !rneedsContext.test( selectors ) ) {
			for ( ; i < l; i++ ) {
				for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {

					// Always skip document fragments
					if ( cur.nodeType < 11 && ( targets ?
						targets.index( cur ) > -1 :

						// Don't pass non-elements to Sizzle
						cur.nodeType === 1 &&
							jQuery.find.matchesSelector( cur, selectors ) ) ) {

						matched.push( cur );
						break;
					}
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.uniqueSort(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter( selector )
		);
	}
} );

function sibling( cur, dir ) {
	while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each( {
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, _i, until ) {
		return dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, _i, until ) {
		return dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, _i, until ) {
		return dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return siblings( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return siblings( elem.firstChild );
	},
	contents: function( elem ) {
		if ( elem.contentDocument != null &&

			// Support: IE 11+
			// <object> elements with no `data` attribute has an object
			// `contentDocument` with a `null` prototype.
			getProto( elem.contentDocument ) ) {

			return elem.contentDocument;
		}

		// Support: IE 9 - 11 only, iOS 7 only, Android Browser <=4.3 only
		// Treat the template element as a regular one in browsers that
		// don't support it.
		if ( nodeName( elem, "template" ) ) {
			elem = elem.content || elem;
		}

		return jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {

			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.uniqueSort( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
} );
var rnothtmlwhite = ( /[^\x20\t\r\n\f]+/g );



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
	var object = {};
	jQuery.each( options.match( rnothtmlwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	} );
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		createOptions( options ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,

		// Last fire value for non-forgettable lists
		memory,

		// Flag to know if list was already fired
		fired,

		// Flag to prevent firing
		locked,

		// Actual callback list
		list = [],

		// Queue of execution data for repeatable lists
		queue = [],

		// Index of currently firing callback (modified by add/remove as needed)
		firingIndex = -1,

		// Fire callbacks
		fire = function() {

			// Enforce single-firing
			locked = locked || options.once;

			// Execute callbacks for all pending executions,
			// respecting firingIndex overrides and runtime changes
			fired = firing = true;
			for ( ; queue.length; firingIndex = -1 ) {
				memory = queue.shift();
				while ( ++firingIndex < list.length ) {

					// Run callback and check for early termination
					if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
						options.stopOnFalse ) {

						// Jump to end and forget the data so .add doesn't re-fire
						firingIndex = list.length;
						memory = false;
					}
				}
			}

			// Forget the data if we're done with it
			if ( !options.memory ) {
				memory = false;
			}

			firing = false;

			// Clean up if we're done firing for good
			if ( locked ) {

				// Keep an empty list if we have data for future add calls
				if ( memory ) {
					list = [];

				// Otherwise, this object is spent
				} else {
					list = "";
				}
			}
		},

		// Actual Callbacks object
		self = {

			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {

					// If we have memory from a past run, we should fire after adding
					if ( memory && !firing ) {
						firingIndex = list.length - 1;
						queue.push( memory );
					}

					( function add( args ) {
						jQuery.each( args, function( _, arg ) {
							if ( isFunction( arg ) ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && toType( arg ) !== "string" ) {

								// Inspect recursively
								add( arg );
							}
						} );
					} )( arguments );

					if ( memory && !firing ) {
						fire();
					}
				}
				return this;
			},

			// Remove a callback from the list
			remove: function() {
				jQuery.each( arguments, function( _, arg ) {
					var index;
					while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
						list.splice( index, 1 );

						// Handle firing indexes
						if ( index <= firingIndex ) {
							firingIndex--;
						}
					}
				} );
				return this;
			},

			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ?
					jQuery.inArray( fn, list ) > -1 :
					list.length > 0;
			},

			// Remove all callbacks from the list
			empty: function() {
				if ( list ) {
					list = [];
				}
				return this;
			},

			// Disable .fire and .add
			// Abort any current/pending executions
			// Clear all callbacks and values
			disable: function() {
				locked = queue = [];
				list = memory = "";
				return this;
			},
			disabled: function() {
				return !list;
			},

			// Disable .fire
			// Also disable .add unless we have memory (since it would have no effect)
			// Abort any pending executions
			lock: function() {
				locked = queue = [];
				if ( !memory && !firing ) {
					list = memory = "";
				}
				return this;
			},
			locked: function() {
				return !!locked;
			},

			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( !locked ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					queue.push( args );
					if ( !firing ) {
						fire();
					}
				}
				return this;
			},

			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},

			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


function Identity( v ) {
	return v;
}
function Thrower( ex ) {
	throw ex;
}

function adoptValue( value, resolve, reject, noValue ) {
	var method;

	try {

		// Check for promise aspect first to privilege synchronous behavior
		if ( value && isFunction( ( method = value.promise ) ) ) {
			method.call( value ).done( resolve ).fail( reject );

		// Other thenables
		} else if ( value && isFunction( ( method = value.then ) ) ) {
			method.call( value, resolve, reject );

		// Other non-thenables
		} else {

			// Control `resolve` arguments by letting Array#slice cast boolean `noValue` to integer:
			// * false: [ value ].slice( 0 ) => resolve( value )
			// * true: [ value ].slice( 1 ) => resolve()
			resolve.apply( undefined, [ value ].slice( noValue ) );
		}

	// For Promises/A+, convert exceptions into rejections
	// Since jQuery.when doesn't unwrap thenables, we can skip the extra checks appearing in
	// Deferred#then to conditionally suppress rejection.
	} catch ( value ) {

		// Support: Android 4.0 only
		// Strict mode functions invoked without .call/.apply get global-object context
		reject.apply( undefined, [ value ] );
	}
}

jQuery.extend( {

	Deferred: function( func ) {
		var tuples = [

				// action, add listener, callbacks,
				// ... .then handlers, argument index, [final state]
				[ "notify", "progress", jQuery.Callbacks( "memory" ),
					jQuery.Callbacks( "memory" ), 2 ],
				[ "resolve", "done", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 0, "resolved" ],
				[ "reject", "fail", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 1, "rejected" ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				"catch": function( fn ) {
					return promise.then( null, fn );
				},

				// Keep pipe for back-compat
				pipe: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;

					return jQuery.Deferred( function( newDefer ) {
						jQuery.each( tuples, function( _i, tuple ) {

							// Map tuples (progress, done, fail) to arguments (done, fail, progress)
							var fn = isFunction( fns[ tuple[ 4 ] ] ) && fns[ tuple[ 4 ] ];

							// deferred.progress(function() { bind to newDefer or newDefer.notify })
							// deferred.done(function() { bind to newDefer or newDefer.resolve })
							// deferred.fail(function() { bind to newDefer or newDefer.reject })
							deferred[ tuple[ 1 ] ]( function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && isFunction( returned.promise ) ) {
									returned.promise()
										.progress( newDefer.notify )
										.done( newDefer.resolve )
										.fail( newDefer.reject );
								} else {
									newDefer[ tuple[ 0 ] + "With" ](
										this,
										fn ? [ returned ] : arguments
									);
								}
							} );
						} );
						fns = null;
					} ).promise();
				},
				then: function( onFulfilled, onRejected, onProgress ) {
					var maxDepth = 0;
					function resolve( depth, deferred, handler, special ) {
						return function() {
							var that = this,
								args = arguments,
								mightThrow = function() {
									var returned, then;

									// Support: Promises/A+ section 2.3.3.3.3
									// https://promisesaplus.com/#point-59
									// Ignore double-resolution attempts
									if ( depth < maxDepth ) {
										return;
									}

									returned = handler.apply( that, args );

									// Support: Promises/A+ section 2.3.1
									// https://promisesaplus.com/#point-48
									if ( returned === deferred.promise() ) {
										throw new TypeError( "Thenable self-resolution" );
									}

									// Support: Promises/A+ sections 2.3.3.1, 3.5
									// https://promisesaplus.com/#point-54
									// https://promisesaplus.com/#point-75
									// Retrieve `then` only once
									then = returned &&

										// Support: Promises/A+ section 2.3.4
										// https://promisesaplus.com/#point-64
										// Only check objects and functions for thenability
										( typeof returned === "object" ||
											typeof returned === "function" ) &&
										returned.then;

									// Handle a returned thenable
									if ( isFunction( then ) ) {

										// Special processors (notify) just wait for resolution
										if ( special ) {
											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special )
											);

										// Normal processors (resolve) also hook into progress
										} else {

											// ...and disregard older resolution values
											maxDepth++;

											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special ),
												resolve( maxDepth, deferred, Identity,
													deferred.notifyWith )
											);
										}

									// Handle all other returned values
									} else {

										// Only substitute handlers pass on context
										// and multiple values (non-spec behavior)
										if ( handler !== Identity ) {
											that = undefined;
											args = [ returned ];
										}

										// Process the value(s)
										// Default process is resolve
										( special || deferred.resolveWith )( that, args );
									}
								},

								// Only normal processors (resolve) catch and reject exceptions
								process = special ?
									mightThrow :
									function() {
										try {
											mightThrow();
										} catch ( e ) {

											if ( jQuery.Deferred.exceptionHook ) {
												jQuery.Deferred.exceptionHook( e,
													process.stackTrace );
											}

											// Support: Promises/A+ section 2.3.3.3.4.1
											// https://promisesaplus.com/#point-61
											// Ignore post-resolution exceptions
											if ( depth + 1 >= maxDepth ) {

												// Only substitute handlers pass on context
												// and multiple values (non-spec behavior)
												if ( handler !== Thrower ) {
													that = undefined;
													args = [ e ];
												}

												deferred.rejectWith( that, args );
											}
										}
									};

							// Support: Promises/A+ section 2.3.3.3.1
							// https://promisesaplus.com/#point-57
							// Re-resolve promises immediately to dodge false rejection from
							// subsequent errors
							if ( depth ) {
								process();
							} else {

								// Call an optional hook to record the stack, in case of exception
								// since it's otherwise lost when execution goes async
								if ( jQuery.Deferred.getStackHook ) {
									process.stackTrace = jQuery.Deferred.getStackHook();
								}
								window.setTimeout( process );
							}
						};
					}

					return jQuery.Deferred( function( newDefer ) {

						// progress_handlers.add( ... )
						tuples[ 0 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onProgress ) ?
									onProgress :
									Identity,
								newDefer.notifyWith
							)
						);

						// fulfilled_handlers.add( ... )
						tuples[ 1 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onFulfilled ) ?
									onFulfilled :
									Identity
							)
						);

						// rejected_handlers.add( ... )
						tuples[ 2 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								isFunction( onRejected ) ?
									onRejected :
									Thrower
							)
						);
					} ).promise();
				},

				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 5 ];

			// promise.progress = list.add
			// promise.done = list.add
			// promise.fail = list.add
			promise[ tuple[ 1 ] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(
					function() {

						// state = "resolved" (i.e., fulfilled)
						// state = "rejected"
						state = stateString;
					},

					// rejected_callbacks.disable
					// fulfilled_callbacks.disable
					tuples[ 3 - i ][ 2 ].disable,

					// rejected_handlers.disable
					// fulfilled_handlers.disable
					tuples[ 3 - i ][ 3 ].disable,

					// progress_callbacks.lock
					tuples[ 0 ][ 2 ].lock,

					// progress_handlers.lock
					tuples[ 0 ][ 3 ].lock
				);
			}

			// progress_handlers.fire
			// fulfilled_handlers.fire
			// rejected_handlers.fire
			list.add( tuple[ 3 ].fire );

			// deferred.notify = function() { deferred.notifyWith(...) }
			// deferred.resolve = function() { deferred.resolveWith(...) }
			// deferred.reject = function() { deferred.rejectWith(...) }
			deferred[ tuple[ 0 ] ] = function() {
				deferred[ tuple[ 0 ] + "With" ]( this === deferred ? undefined : this, arguments );
				return this;
			};

			// deferred.notifyWith = list.fireWith
			// deferred.resolveWith = list.fireWith
			// deferred.rejectWith = list.fireWith
			deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
		} );

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( singleValue ) {
		var

			// count of uncompleted subordinates
			remaining = arguments.length,

			// count of unprocessed arguments
			i = remaining,

			// subordinate fulfillment data
			resolveContexts = Array( i ),
			resolveValues = slice.call( arguments ),

			// the master Deferred
			master = jQuery.Deferred(),

			// subordinate callback factory
			updateFunc = function( i ) {
				return function( value ) {
					resolveContexts[ i ] = this;
					resolveValues[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( !( --remaining ) ) {
						master.resolveWith( resolveContexts, resolveValues );
					}
				};
			};

		// Single- and empty arguments are adopted like Promise.resolve
		if ( remaining <= 1 ) {
			adoptValue( singleValue, master.done( updateFunc( i ) ).resolve, master.reject,
				!remaining );

			// Use .then() to unwrap secondary thenables (cf. gh-3000)
			if ( master.state() === "pending" ||
				isFunction( resolveValues[ i ] && resolveValues[ i ].then ) ) {

				return master.then();
			}
		}

		// Multiple arguments are aggregated like Promise.all array elements
		while ( i-- ) {
			adoptValue( resolveValues[ i ], updateFunc( i ), master.reject );
		}

		return master.promise();
	}
} );


// These usually indicate a programmer mistake during development,
// warn about them ASAP rather than swallowing them by default.
var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;

jQuery.Deferred.exceptionHook = function( error, stack ) {

	// Support: IE 8 - 9 only
	// Console exists when dev tools are open, which can happen at any time
	if ( window.console && window.console.warn && error && rerrorNames.test( error.name ) ) {
		window.console.warn( "jQuery.Deferred exception: " + error.message, error.stack, stack );
	}
};




jQuery.readyException = function( error ) {
	window.setTimeout( function() {
		throw error;
	} );
};




// The deferred used on DOM ready
var readyList = jQuery.Deferred();

jQuery.fn.ready = function( fn ) {

	readyList
		.then( fn )

		// Wrap jQuery.readyException in a function so that the lookup
		// happens at the time of error handling instead of callback
		// registration.
		.catch( function( error ) {
			jQuery.readyException( error );
		} );

	return this;
};

jQuery.extend( {

	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );
	}
} );

jQuery.ready.then = readyList.then;

// The ready event handler and self cleanup method
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed );
	window.removeEventListener( "load", completed );
	jQuery.ready();
}

// Catch cases where $(document).ready() is called
// after the browser event has already occurred.
// Support: IE <=9 - 10 only
// Older IE sometimes signals "interactive" too soon
if ( document.readyState === "complete" ||
	( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {

	// Handle it asynchronously to allow scripts the opportunity to delay ready
	window.setTimeout( jQuery.ready );

} else {

	// Use the handy event callback
	document.addEventListener( "DOMContentLoaded", completed );

	// A fallback to window.onload, that will always work
	window.addEventListener( "load", completed );
}




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( toType( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			access( elems, fn, i, key[ i ], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {

			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, _key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn(
					elems[ i ], key, raw ?
					value :
					value.call( elems[ i ], i, fn( elems[ i ], key ) )
				);
			}
		}
	}

	if ( chainable ) {
		return elems;
	}

	// Gets
	if ( bulk ) {
		return fn.call( elems );
	}

	return len ? fn( elems[ 0 ], key ) : emptyGet;
};


// Matches dashed string for camelizing
var rmsPrefix = /^-ms-/,
	rdashAlpha = /-([a-z])/g;

// Used by camelCase as callback to replace()
function fcamelCase( _all, letter ) {
	return letter.toUpperCase();
}

// Convert dashed to camelCase; used by the css and data modules
// Support: IE <=9 - 11, Edge 12 - 15
// Microsoft forgot to hump their vendor prefix (#9572)
function camelCase( string ) {
	return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
}
var acceptData = function( owner ) {

	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};




function Data() {
	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;

Data.prototype = {

	cache: function( owner ) {

		// Check if the owner object already has a cache
		var value = owner[ this.expando ];

		// If not, create one
		if ( !value ) {
			value = {};

			// We can accept data for non-element nodes in modern browsers,
			// but we should not, see #8335.
			// Always return an empty object.
			if ( acceptData( owner ) ) {

				// If it is a node unlikely to be stringify-ed or looped over
				// use plain assignment
				if ( owner.nodeType ) {
					owner[ this.expando ] = value;

				// Otherwise secure it in a non-enumerable property
				// configurable must be true to allow the property to be
				// deleted when data is removed
				} else {
					Object.defineProperty( owner, this.expando, {
						value: value,
						configurable: true
					} );
				}
			}
		}

		return value;
	},
	set: function( owner, data, value ) {
		var prop,
			cache = this.cache( owner );

		// Handle: [ owner, key, value ] args
		// Always use camelCase key (gh-2257)
		if ( typeof data === "string" ) {
			cache[ camelCase( data ) ] = value;

		// Handle: [ owner, { properties } ] args
		} else {

			// Copy the properties one-by-one to the cache object
			for ( prop in data ) {
				cache[ camelCase( prop ) ] = data[ prop ];
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		return key === undefined ?
			this.cache( owner ) :

			// Always use camelCase key (gh-2257)
			owner[ this.expando ] && owner[ this.expando ][ camelCase( key ) ];
	},
	access: function( owner, key, value ) {

		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				( ( key && typeof key === "string" ) && value === undefined ) ) {

			return this.get( owner, key );
		}

		// When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i,
			cache = owner[ this.expando ];

		if ( cache === undefined ) {
			return;
		}

		if ( key !== undefined ) {

			// Support array or space separated string of keys
			if ( Array.isArray( key ) ) {

				// If key is an array of keys...
				// We always set camelCase keys, so remove that.
				key = key.map( camelCase );
			} else {
				key = camelCase( key );

				// If a key with the spaces exists, use it.
				// Otherwise, create an array by matching non-whitespace
				key = key in cache ?
					[ key ] :
					( key.match( rnothtmlwhite ) || [] );
			}

			i = key.length;

			while ( i-- ) {
				delete cache[ key[ i ] ];
			}
		}

		// Remove the expando if there's no more data
		if ( key === undefined || jQuery.isEmptyObject( cache ) ) {

			// Support: Chrome <=35 - 45
			// Webkit & Blink performance suffers when deleting properties
			// from DOM nodes, so set to undefined instead
			// https://bugs.chromium.org/p/chromium/issues/detail?id=378607 (bug restricted)
			if ( owner.nodeType ) {
				owner[ this.expando ] = undefined;
			} else {
				delete owner[ this.expando ];
			}
		}
	},
	hasData: function( owner ) {
		var cache = owner[ this.expando ];
		return cache !== undefined && !jQuery.isEmptyObject( cache );
	}
};
var dataPriv = new Data();

var dataUser = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /[A-Z]/g;

function getData( data ) {
	if ( data === "true" ) {
		return true;
	}

	if ( data === "false" ) {
		return false;
	}

	if ( data === "null" ) {
		return null;
	}

	// Only convert to a number if it doesn't change the string
	if ( data === +data + "" ) {
		return +data;
	}

	if ( rbrace.test( data ) ) {
		return JSON.parse( data );
	}

	return data;
}

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = getData( data );
			} catch ( e ) {}

			// Make sure we set the data so it isn't changed later
			dataUser.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend( {
	hasData: function( elem ) {
		return dataUser.hasData( elem ) || dataPriv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return dataUser.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		dataUser.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to dataPriv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return dataPriv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		dataPriv.remove( elem, name );
	}
} );

jQuery.fn.extend( {
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = dataUser.get( elem );

				if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE 11 only
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = camelCase( name.slice( 5 ) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					dataPriv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each( function() {
				dataUser.set( this, key );
			} );
		}

		return access( this, function( value ) {
			var data;

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {

				// Attempt to get data from the cache
				// The key will always be camelCased in Data
				data = dataUser.get( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			this.each( function() {

				// We always store the camelCased key
				dataUser.set( this, key, value );
			} );
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each( function() {
			dataUser.remove( this, key );
		} );
	}
} );


jQuery.extend( {
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = dataPriv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || Array.isArray( data ) ) {
					queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
			empty: jQuery.Callbacks( "once memory" ).add( function() {
				dataPriv.remove( elem, [ type + "queue", key ] );
			} )
		} );
	}
} );

jQuery.fn.extend( {
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[ 0 ], type );
		}

		return data === undefined ?
			this :
			this.each( function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			} );
	},
	dequeue: function( type ) {
		return this.each( function() {
			jQuery.dequeue( this, type );
		} );
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},

	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
} );
var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var documentElement = document.documentElement;



	var isAttached = function( elem ) {
			return jQuery.contains( elem.ownerDocument, elem );
		},
		composed = { composed: true };

	// Support: IE 9 - 11+, Edge 12 - 18+, iOS 10.0 - 10.2 only
	// Check attachment across shadow DOM boundaries when possible (gh-3504)
	// Support: iOS 10.0-10.2 only
	// Early iOS 10 versions support `attachShadow` but not `getRootNode`,
	// leading to errors. We need to check for `getRootNode`.
	if ( documentElement.getRootNode ) {
		isAttached = function( elem ) {
			return jQuery.contains( elem.ownerDocument, elem ) ||
				elem.getRootNode( composed ) === elem.ownerDocument;
		};
	}
var isHiddenWithinTree = function( elem, el ) {

		// isHiddenWithinTree might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;

		// Inline style trumps all
		return elem.style.display === "none" ||
			elem.style.display === "" &&

			// Otherwise, check computed style
			// Support: Firefox <=43 - 45
			// Disconnected elements can have computed display: none, so first confirm that elem is
			// in the document.
			isAttached( elem ) &&

			jQuery.css( elem, "display" ) === "none";
	};



function adjustCSS( elem, prop, valueParts, tween ) {
	var adjusted, scale,
		maxIterations = 20,
		currentValue = tween ?
			function() {
				return tween.cur();
			} :
			function() {
				return jQuery.css( elem, prop, "" );
			},
		initial = currentValue(),
		unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

		// Starting value computation is required for potential unit mismatches
		initialInUnit = elem.nodeType &&
			( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
			rcssNum.exec( jQuery.css( elem, prop ) );

	if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {

		// Support: Firefox <=54
		// Halve the iteration target value to prevent interference from CSS upper bounds (gh-2144)
		initial = initial / 2;

		// Trust units reported by jQuery.css
		unit = unit || initialInUnit[ 3 ];

		// Iteratively approximate from a nonzero starting point
		initialInUnit = +initial || 1;

		while ( maxIterations-- ) {

			// Evaluate and update our best guess (doubling guesses that zero out).
			// Finish if the scale equals or crosses 1 (making the old*new product non-positive).
			jQuery.style( elem, prop, initialInUnit + unit );
			if ( ( 1 - scale ) * ( 1 - ( scale = currentValue() / initial || 0.5 ) ) <= 0 ) {
				maxIterations = 0;
			}
			initialInUnit = initialInUnit / scale;

		}

		initialInUnit = initialInUnit * 2;
		jQuery.style( elem, prop, initialInUnit + unit );

		// Make sure we update the tween properties later on
		valueParts = valueParts || [];
	}

	if ( valueParts ) {
		initialInUnit = +initialInUnit || +initial || 0;

		// Apply relative offset (+=/-=) if specified
		adjusted = valueParts[ 1 ] ?
			initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
			+valueParts[ 2 ];
		if ( tween ) {
			tween.unit = unit;
			tween.start = initialInUnit;
			tween.end = adjusted;
		}
	}
	return adjusted;
}


var defaultDisplayMap = {};

function getDefaultDisplay( elem ) {
	var temp,
		doc = elem.ownerDocument,
		nodeName = elem.nodeName,
		display = defaultDisplayMap[ nodeName ];

	if ( display ) {
		return display;
	}

	temp = doc.body.appendChild( doc.createElement( nodeName ) );
	display = jQuery.css( temp, "display" );

	temp.parentNode.removeChild( temp );

	if ( display === "none" ) {
		display = "block";
	}
	defaultDisplayMap[ nodeName ] = display;

	return display;
}

function showHide( elements, show ) {
	var display, elem,
		values = [],
		index = 0,
		length = elements.length;

	// Determine new display value for elements that need to change
	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		display = elem.style.display;
		if ( show ) {

			// Since we force visibility upon cascade-hidden elements, an immediate (and slow)
			// check is required in this first loop unless we have a nonempty display value (either
			// inline or about-to-be-restored)
			if ( display === "none" ) {
				values[ index ] = dataPriv.get( elem, "display" ) || null;
				if ( !values[ index ] ) {
					elem.style.display = "";
				}
			}
			if ( elem.style.display === "" && isHiddenWithinTree( elem ) ) {
				values[ index ] = getDefaultDisplay( elem );
			}
		} else {
			if ( display !== "none" ) {
				values[ index ] = "none";

				// Remember what we're overwriting
				dataPriv.set( elem, "display", display );
			}
		}
	}

	// Set the display of the elements in a second loop to avoid constant reflow
	for ( index = 0; index < length; index++ ) {
		if ( values[ index ] != null ) {
			elements[ index ].style.display = values[ index ];
		}
	}

	return elements;
}

jQuery.fn.extend( {
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each( function() {
			if ( isHiddenWithinTree( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		} );
	}
} );
var rcheckableType = ( /^(?:checkbox|radio)$/i );

var rtagName = ( /<([a-z][^\/\0>\x20\t\r\n\f]*)/i );

var rscriptType = ( /^$|^module$|\/(?:java|ecma)script/i );



( function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Android 4.0 - 4.3 only
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Android <=4.1 only
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE <=11 only
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;

	// Support: IE <=9 only
	// IE <=9 replaces <option> tags with their contents when inserted outside of
	// the select element.
	div.innerHTML = "<option></option>";
	support.option = !!div.lastChild;
} )();


// We have to close these tags to support XHTML (#13200)
var wrapMap = {

	// XHTML parsers do not magically insert elements in the
	// same way that tag soup parsers do. So we cannot shorten
	// this by omitting <tbody> or other required elements.
	thead: [ 1, "<table>", "</table>" ],
	col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
	tr: [ 2, "<table><tbody>", "</tbody></table>" ],
	td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

	_default: [ 0, "", "" ]
};

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;

// Support: IE <=9 only
if ( !support.option ) {
	wrapMap.optgroup = wrapMap.option = [ 1, "<select multiple='multiple'>", "</select>" ];
}


function getAll( context, tag ) {

	// Support: IE <=9 - 11 only
	// Use typeof to avoid zero-argument method invocation on host objects (#15151)
	var ret;

	if ( typeof context.getElementsByTagName !== "undefined" ) {
		ret = context.getElementsByTagName( tag || "*" );

	} else if ( typeof context.querySelectorAll !== "undefined" ) {
		ret = context.querySelectorAll( tag || "*" );

	} else {
		ret = [];
	}

	if ( tag === undefined || tag && nodeName( context, tag ) ) {
		return jQuery.merge( [ context ], ret );
	}

	return ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		dataPriv.set(
			elems[ i ],
			"globalEval",
			!refElements || dataPriv.get( refElements[ i ], "globalEval" )
		);
	}
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
	var elem, tmp, tag, wrap, attached, j,
		fragment = context.createDocumentFragment(),
		nodes = [],
		i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		elem = elems[ i ];

		if ( elem || elem === 0 ) {

			// Add nodes directly
			if ( toType( elem ) === "object" ) {

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

			// Convert non-html into a text node
			} else if ( !rhtml.test( elem ) ) {
				nodes.push( context.createTextNode( elem ) );

			// Convert html into DOM nodes
			} else {
				tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

				// Deserialize a standard representation
				tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
				wrap = wrapMap[ tag ] || wrapMap._default;
				tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

				// Descend through wrappers to the right content
				j = wrap[ 0 ];
				while ( j-- ) {
					tmp = tmp.lastChild;
				}

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, tmp.childNodes );

				// Remember the top-level container
				tmp = fragment.firstChild;

				// Ensure the created nodes are orphaned (#12392)
				tmp.textContent = "";
			}
		}
	}

	// Remove wrapper from fragment
	fragment.textContent = "";

	i = 0;
	while ( ( elem = nodes[ i++ ] ) ) {

		// Skip elements already in the context collection (trac-4087)
		if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
			if ( ignored ) {
				ignored.push( elem );
			}
			continue;
		}

		attached = isAttached( elem );

		// Append to fragment
		tmp = getAll( fragment.appendChild( elem ), "script" );

		// Preserve script evaluation history
		if ( attached ) {
			setGlobalEval( tmp );
		}

		// Capture executables
		if ( scripts ) {
			j = 0;
			while ( ( elem = tmp[ j++ ] ) ) {
				if ( rscriptType.test( elem.type || "" ) ) {
					scripts.push( elem );
				}
			}
		}
	}

	return fragment;
}


var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

// Support: IE <=9 - 11+
// focus() and blur() are asynchronous, except when they are no-op.
// So expect focus to be synchronous when the element is already active,
// and blur to be synchronous when the element is not already active.
// (focus and blur are always synchronous in other supported browsers,
// this just defines when we can count on it).
function expectSync( elem, type ) {
	return ( elem === safeActiveElement() ) === ( type === "focus" );
}

// Support: IE <=9 only
// Accessing document.activeElement can throw unexpectedly
// https://bugs.jquery.com/ticket/13393
function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
	var origFn, type;

	// Types can be a map of types/handlers
	if ( typeof types === "object" ) {

		// ( types-Object, selector, data )
		if ( typeof selector !== "string" ) {

			// ( types-Object, data )
			data = data || selector;
			selector = undefined;
		}
		for ( type in types ) {
			on( elem, type, selector, data, types[ type ], one );
		}
		return elem;
	}

	if ( data == null && fn == null ) {

		// ( types, fn )
		fn = selector;
		data = selector = undefined;
	} else if ( fn == null ) {
		if ( typeof selector === "string" ) {

			// ( types, selector, fn )
			fn = data;
			data = undefined;
		} else {

			// ( types, data, fn )
			fn = data;
			data = selector;
			selector = undefined;
		}
	}
	if ( fn === false ) {
		fn = returnFalse;
	} else if ( !fn ) {
		return elem;
	}

	if ( one === 1 ) {
		origFn = fn;
		fn = function( event ) {

			// Can use an empty set, since event contains the info
			jQuery().off( event );
			return origFn.apply( this, arguments );
		};

		// Use same guid so caller can remove using origFn
		fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
	}
	return elem.each( function() {
		jQuery.event.add( this, types, fn, data, selector );
	} );
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.get( elem );

		// Only attach events to objects that accept data
		if ( !acceptData( elem ) ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Ensure that invalid selectors throw exceptions at attach time
		// Evaluate against documentElement in case elem is a non-element node (e.g., document)
		if ( selector ) {
			jQuery.find.matchesSelector( documentElement, selector );
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !( events = elemData.events ) ) {
			events = elemData.events = Object.create( null );
		}
		if ( !( eventHandle = elemData.handle ) ) {
			eventHandle = elemData.handle = function( e ) {

				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend( {
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join( "." )
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !( handlers = events[ type ] ) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup ||
					special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

		if ( !elemData || !( events = elemData.events ) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[ 2 ] &&
				new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector ||
						selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown ||
					special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove data and the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			dataPriv.remove( elem, "handle events" );
		}
	},

	dispatch: function( nativeEvent ) {

		var i, j, ret, matched, handleObj, handlerQueue,
			args = new Array( arguments.length ),

			// Make a writable jQuery.Event from the native event object
			event = jQuery.event.fix( nativeEvent ),

			handlers = (
					dataPriv.get( this, "events" ) || Object.create( null )
				)[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[ 0 ] = event;

		for ( i = 1; i < arguments.length; i++ ) {
			args[ i ] = arguments[ i ];
		}

		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( ( handleObj = matched.handlers[ j++ ] ) &&
				!event.isImmediatePropagationStopped() ) {

				// If the event is namespaced, then each handler is only invoked if it is
				// specially universal or its namespaces are a superset of the event's.
				if ( !event.rnamespace || handleObj.namespace === false ||
					event.rnamespace.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
						handleObj.handler ).apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( ( event.result = ret ) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, handleObj, sel, matchedHandlers, matchedSelectors,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		if ( delegateCount &&

			// Support: IE <=9
			// Black-hole SVG <use> instance trees (trac-13180)
			cur.nodeType &&

			// Support: Firefox <=42
			// Suppress spec-violating clicks indicating a non-primary pointer button (trac-3861)
			// https://www.w3.org/TR/DOM-Level-3-Events/#event-type-click
			// Support: IE 11 only
			// ...but not arrow key "clicks" of radio inputs, which can have `button` -1 (gh-2343)
			!( event.type === "click" && event.button >= 1 ) ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && !( event.type === "click" && cur.disabled === true ) ) {
					matchedHandlers = [];
					matchedSelectors = {};
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matchedSelectors[ sel ] === undefined ) {
							matchedSelectors[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) > -1 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matchedSelectors[ sel ] ) {
							matchedHandlers.push( handleObj );
						}
					}
					if ( matchedHandlers.length ) {
						handlerQueue.push( { elem: cur, handlers: matchedHandlers } );
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		cur = this;
		if ( delegateCount < handlers.length ) {
			handlerQueue.push( { elem: cur, handlers: handlers.slice( delegateCount ) } );
		}

		return handlerQueue;
	},

	addProp: function( name, hook ) {
		Object.defineProperty( jQuery.Event.prototype, name, {
			enumerable: true,
			configurable: true,

			get: isFunction( hook ) ?
				function() {
					if ( this.originalEvent ) {
							return hook( this.originalEvent );
					}
				} :
				function() {
					if ( this.originalEvent ) {
							return this.originalEvent[ name ];
					}
				},

			set: function( value ) {
				Object.defineProperty( this, name, {
					enumerable: true,
					configurable: true,
					writable: true,
					value: value
				} );
			}
		} );
	},

	fix: function( originalEvent ) {
		return originalEvent[ jQuery.expando ] ?
			originalEvent :
			new jQuery.Event( originalEvent );
	},

	special: {
		load: {

			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		click: {

			// Utilize native event to ensure correct state for checkable inputs
			setup: function( data ) {

				// For mutual compressibility with _default, replace `this` access with a local var.
				// `|| data` is dead code meant only to preserve the variable through minification.
				var el = this || data;

				// Claim the first handler
				if ( rcheckableType.test( el.type ) &&
					el.click && nodeName( el, "input" ) ) {

					// dataPriv.set( el, "click", ... )
					leverageNative( el, "click", returnTrue );
				}

				// Return false to allow normal processing in the caller
				return false;
			},
			trigger: function( data ) {

				// For mutual compressibility with _default, replace `this` access with a local var.
				// `|| data` is dead code meant only to preserve the variable through minification.
				var el = this || data;

				// Force setup before triggering a click
				if ( rcheckableType.test( el.type ) &&
					el.click && nodeName( el, "input" ) ) {

					leverageNative( el, "click" );
				}

				// Return non-false to allow normal event-path propagation
				return true;
			},

			// For cross-browser consistency, suppress native .click() on links
			// Also prevent it if we're currently inside a leveraged native-event stack
			_default: function( event ) {
				var target = event.target;
				return rcheckableType.test( target.type ) &&
					target.click && nodeName( target, "input" ) &&
					dataPriv.get( target, "click" ) ||
					nodeName( target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	}
};

// Ensure the presence of an event listener that handles manually-triggered
// synthetic events by interrupting progress until reinvoked in response to
// *native* events that it fires directly, ensuring that state changes have
// already occurred before other listeners are invoked.
function leverageNative( el, type, expectSync ) {

	// Missing expectSync indicates a trigger call, which must force setup through jQuery.event.add
	if ( !expectSync ) {
		if ( dataPriv.get( el, type ) === undefined ) {
			jQuery.event.add( el, type, returnTrue );
		}
		return;
	}

	// Register the controller as a special universal handler for all event namespaces
	dataPriv.set( el, type, false );
	jQuery.event.add( el, type, {
		namespace: false,
		handler: function( event ) {
			var notAsync, result,
				saved = dataPriv.get( this, type );

			if ( ( event.isTrigger & 1 ) && this[ type ] ) {

				// Interrupt processing of the outer synthetic .trigger()ed event
				// Saved data should be false in such cases, but might be a leftover capture object
				// from an async native handler (gh-4350)
				if ( !saved.length ) {

					// Store arguments for use when handling the inner native event
					// There will always be at least one argument (an event object), so this array
					// will not be confused with a leftover capture object.
					saved = slice.call( arguments );
					dataPriv.set( this, type, saved );

					// Trigger the native event and capture its result
					// Support: IE <=9 - 11+
					// focus() and blur() are asynchronous
					notAsync = expectSync( this, type );
					this[ type ]();
					result = dataPriv.get( this, type );
					if ( saved !== result || notAsync ) {
						dataPriv.set( this, type, false );
					} else {
						result = {};
					}
					if ( saved !== result ) {

						// Cancel the outer synthetic event
						event.stopImmediatePropagation();
						event.preventDefault();
						return result.value;
					}

				// If this is an inner synthetic event for an event with a bubbling surrogate
				// (focus or blur), assume that the surrogate already propagated from triggering the
				// native event and prevent that from happening again here.
				// This technically gets the ordering wrong w.r.t. to `.trigger()` (in which the
				// bubbling surrogate propagates *after* the non-bubbling base), but that seems
				// less bad than duplication.
				} else if ( ( jQuery.event.special[ type ] || {} ).delegateType ) {
					event.stopPropagation();
				}

			// If this is a native event triggered above, everything is now in order
			// Fire an inner synthetic event with the original arguments
			} else if ( saved.length ) {

				// ...and capture the result
				dataPriv.set( this, type, {
					value: jQuery.event.trigger(

						// Support: IE <=9 - 11+
						// Extend with the prototype to reset the above stopImmediatePropagation()
						jQuery.extend( saved[ 0 ], jQuery.Event.prototype ),
						saved.slice( 1 ),
						this
					)
				} );

				// Abort handling of the native event
				event.stopImmediatePropagation();
			}
		}
	} );
}

jQuery.removeEvent = function( elem, type, handle ) {

	// This "if" is needed for plain objects
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle );
	}
};

jQuery.Event = function( src, props ) {

	// Allow instantiation without the 'new' keyword
	if ( !( this instanceof jQuery.Event ) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&

				// Support: Android <=2.3 only
				src.returnValue === false ?
			returnTrue :
			returnFalse;

		// Create target properties
		// Support: Safari <=6 - 7 only
		// Target should not be a text node (#504, #13143)
		this.target = ( src.target && src.target.nodeType === 3 ) ?
			src.target.parentNode :
			src.target;

		this.currentTarget = src.currentTarget;
		this.relatedTarget = src.relatedTarget;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || Date.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// https://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	constructor: jQuery.Event,
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,
	isSimulated: false,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && !this.isSimulated ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Includes all common event props including KeyEvent and MouseEvent specific props
jQuery.each( {
	altKey: true,
	bubbles: true,
	cancelable: true,
	changedTouches: true,
	ctrlKey: true,
	detail: true,
	eventPhase: true,
	metaKey: true,
	pageX: true,
	pageY: true,
	shiftKey: true,
	view: true,
	"char": true,
	code: true,
	charCode: true,
	key: true,
	keyCode: true,
	button: true,
	buttons: true,
	clientX: true,
	clientY: true,
	offsetX: true,
	offsetY: true,
	pointerId: true,
	pointerType: true,
	screenX: true,
	screenY: true,
	targetTouches: true,
	toElement: true,
	touches: true,

	which: function( event ) {
		var button = event.button;

		// Add which for key events
		if ( event.which == null && rkeyEvent.test( event.type ) ) {
			return event.charCode != null ? event.charCode : event.keyCode;
		}

		// Add which for click: 1 === left; 2 === middle; 3 === right
		if ( !event.which && button !== undefined && rmouseEvent.test( event.type ) ) {
			if ( button & 1 ) {
				return 1;
			}

			if ( button & 2 ) {
				return 3;
			}

			if ( button & 4 ) {
				return 2;
			}

			return 0;
		}

		return event.which;
	}
}, jQuery.event.addProp );

jQuery.each( { focus: "focusin", blur: "focusout" }, function( type, delegateType ) {
	jQuery.event.special[ type ] = {

		// Utilize native event if possible so blur/focus sequence is correct
		setup: function() {

			// Claim the first handler
			// dataPriv.set( this, "focus", ... )
			// dataPriv.set( this, "blur", ... )
			leverageNative( this, type, expectSync );

			// Return false to allow normal processing in the caller
			return false;
		},
		trigger: function() {

			// Force setup before trigger
			leverageNative( this, type );

			// Return non-false to allow normal event-path propagation
			return true;
		},

		delegateType: delegateType
	};
} );

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari 7 only
// Safari sends mouseenter too often; see:
// https://bugs.chromium.org/p/chromium/issues/detail?id=470258
// for the description of the bug (it existed in older Chrome versions as well).
jQuery.each( {
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mouseenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
} );

jQuery.fn.extend( {

	on: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn );
	},
	one: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {

			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ?
					handleObj.origType + "." + handleObj.namespace :
					handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {

			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {

			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each( function() {
			jQuery.event.remove( this, types, fn, selector );
		} );
	}
} );


var

	// Support: IE <=10 - 11, Edge 12 - 13 only
	// In IE/Edge using regex groups here causes severe slowdowns.
	// See https://connect.microsoft.com/IE/feedback/details/1736512/
	rnoInnerhtml = /<script|<style|<link/i,

	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

// Prefer a tbody over its parent table for containing new rows
function manipulationTarget( elem, content ) {
	if ( nodeName( elem, "table" ) &&
		nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ) {

		return jQuery( elem ).children( "tbody" )[ 0 ] || elem;
	}

	return elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	if ( ( elem.type || "" ).slice( 0, 5 ) === "true/" ) {
		elem.type = elem.type.slice( 5 );
	} else {
		elem.removeAttribute( "type" );
	}

	return elem;
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( dataPriv.hasData( src ) ) {
		pdataOld = dataPriv.get( src );
		events = pdataOld.events;

		if ( events ) {
			dataPriv.remove( dest, "handle events" );

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( dataUser.hasData( src ) ) {
		udataOld = dataUser.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		dataUser.set( dest, udataCur );
	}
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

function domManip( collection, args, callback, ignored ) {

	// Flatten any nested arrays
	args = flat( args );

	var fragment, first, scripts, hasScripts, node, doc,
		i = 0,
		l = collection.length,
		iNoClone = l - 1,
		value = args[ 0 ],
		valueIsFunction = isFunction( value );

	// We can't cloneNode fragments that contain checked, in WebKit
	if ( valueIsFunction ||
			( l > 1 && typeof value === "string" &&
				!support.checkClone && rchecked.test( value ) ) ) {
		return collection.each( function( index ) {
			var self = collection.eq( index );
			if ( valueIsFunction ) {
				args[ 0 ] = value.call( this, index, self.html() );
			}
			domManip( self, args, callback, ignored );
		} );
	}

	if ( l ) {
		fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
		first = fragment.firstChild;

		if ( fragment.childNodes.length === 1 ) {
			fragment = first;
		}

		// Require either new content or an interest in ignored elements to invoke the callback
		if ( first || ignored ) {
			scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
			hasScripts = scripts.length;

			// Use the original fragment for the last item
			// instead of the first because it can end up
			// being emptied incorrectly in certain situations (#8070).
			for ( ; i < l; i++ ) {
				node = fragment;

				if ( i !== iNoClone ) {
					node = jQuery.clone( node, true, true );

					// Keep references to cloned scripts for later restoration
					if ( hasScripts ) {

						// Support: Android <=4.0 only, PhantomJS 1 only
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( scripts, getAll( node, "script" ) );
					}
				}

				callback.call( collection[ i ], node, i );
			}

			if ( hasScripts ) {
				doc = scripts[ scripts.length - 1 ].ownerDocument;

				// Reenable scripts
				jQuery.map( scripts, restoreScript );

				// Evaluate executable scripts on first document insertion
				for ( i = 0; i < hasScripts; i++ ) {
					node = scripts[ i ];
					if ( rscriptType.test( node.type || "" ) &&
						!dataPriv.access( node, "globalEval" ) &&
						jQuery.contains( doc, node ) ) {

						if ( node.src && ( node.type || "" ).toLowerCase()  !== "module" ) {

							// Optional AJAX dependency, but won't run scripts if not present
							if ( jQuery._evalUrl && !node.noModule ) {
								jQuery._evalUrl( node.src, {
									nonce: node.nonce || node.getAttribute( "nonce" )
								}, doc );
							}
						} else {
							DOMEval( node.textContent.replace( rcleanScript, "" ), node, doc );
						}
					}
				}
			}
		}
	}

	return collection;
}

function remove( elem, selector, keepData ) {
	var node,
		nodes = selector ? jQuery.filter( selector, elem ) : elem,
		i = 0;

	for ( ; ( node = nodes[ i ] ) != null; i++ ) {
		if ( !keepData && node.nodeType === 1 ) {
			jQuery.cleanData( getAll( node ) );
		}

		if ( node.parentNode ) {
			if ( keepData && isAttached( node ) ) {
				setGlobalEval( getAll( node, "script" ) );
			}
			node.parentNode.removeChild( node );
		}
	}

	return elem;
}

jQuery.extend( {
	htmlPrefilter: function( html ) {
		return html;
	},

	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = isAttached( elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: https://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	cleanData: function( elems ) {
		var data, elem, type,
			special = jQuery.event.special,
			i = 0;

		for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
			if ( acceptData( elem ) ) {
				if ( ( data = elem[ dataPriv.expando ] ) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataPriv.expando ] = undefined;
				}
				if ( elem[ dataUser.expando ] ) {

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataUser.expando ] = undefined;
				}
			}
		}
	}
} );

jQuery.fn.extend( {
	detach: function( selector ) {
		return remove( this, selector, true );
	},

	remove: function( selector ) {
		return remove( this, selector );
	},

	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each( function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				} );
		}, null, value, arguments.length );
	},

	append: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		} );
	},

	prepend: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		} );
	},

	before: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		} );
	},

	after: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		} );
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; ( elem = this[ i ] ) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map( function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		} );
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = jQuery.htmlPrefilter( value );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch ( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var ignored = [];

		// Make the changes, replacing each non-ignored context element with the new content
		return domManip( this, arguments, function( elem ) {
			var parent = this.parentNode;

			if ( jQuery.inArray( this, ignored ) < 0 ) {
				jQuery.cleanData( getAll( this ) );
				if ( parent ) {
					parent.replaceChild( elem, this );
				}
			}

		// Force callback invocation
		}, ignored );
	}
} );

jQuery.each( {
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: Android <=4.0 only, PhantomJS 1 only
			// .get() because push.apply(_, arraylike) throws on ancient WebKit
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
} );
var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {

		// Support: IE <=11 only, Firefox <=30 (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		var view = elem.ownerDocument.defaultView;

		if ( !view || !view.opener ) {
			view = window;
		}

		return view.getComputedStyle( elem );
	};

var swap = function( elem, options, callback ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.call( elem );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};


var rboxStyle = new RegExp( cssExpand.join( "|" ), "i" );



( function() {

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computeStyleTests() {

		// This is a singleton, we need to execute it only once
		if ( !div ) {
			return;
		}

		container.style.cssText = "position:absolute;left:-11111px;width:60px;" +
			"margin-top:1px;padding:0;border:0";
		div.style.cssText =
			"position:relative;display:block;box-sizing:border-box;overflow:scroll;" +
			"margin:auto;border:1px;padding:1px;" +
			"width:60%;top:1%";
		documentElement.appendChild( container ).appendChild( div );

		var divStyle = window.getComputedStyle( div );
		pixelPositionVal = divStyle.top !== "1%";

		// Support: Android 4.0 - 4.3 only, Firefox <=3 - 44
		reliableMarginLeftVal = roundPixelMeasures( divStyle.marginLeft ) === 12;

		// Support: Android 4.0 - 4.3 only, Safari <=9.1 - 10.1, iOS <=7.0 - 9.3
		// Some styles come back with percentage values, even though they shouldn't
		div.style.right = "60%";
		pixelBoxStylesVal = roundPixelMeasures( divStyle.right ) === 36;

		// Support: IE 9 - 11 only
		// Detect misreporting of content dimensions for box-sizing:border-box elements
		boxSizingReliableVal = roundPixelMeasures( divStyle.width ) === 36;

		// Support: IE 9 only
		// Detect overflow:scroll screwiness (gh-3699)
		// Support: Chrome <=64
		// Don't get tricked when zoom affects offsetWidth (gh-4029)
		div.style.position = "absolute";
		scrollboxSizeVal = roundPixelMeasures( div.offsetWidth / 3 ) === 12;

		documentElement.removeChild( container );

		// Nullify the div so it wouldn't be stored in the memory and
		// it will also be a sign that checks already performed
		div = null;
	}

	function roundPixelMeasures( measure ) {
		return Math.round( parseFloat( measure ) );
	}

	var pixelPositionVal, boxSizingReliableVal, scrollboxSizeVal, pixelBoxStylesVal,
		reliableTrDimensionsVal, reliableMarginLeftVal,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	// Finish early in limited (non-browser) environments
	if ( !div.style ) {
		return;
	}

	// Support: IE <=9 - 11 only
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	jQuery.extend( support, {
		boxSizingReliable: function() {
			computeStyleTests();
			return boxSizingReliableVal;
		},
		pixelBoxStyles: function() {
			computeStyleTests();
			return pixelBoxStylesVal;
		},
		pixelPosition: function() {
			computeStyleTests();
			return pixelPositionVal;
		},
		reliableMarginLeft: function() {
			computeStyleTests();
			return reliableMarginLeftVal;
		},
		scrollboxSize: function() {
			computeStyleTests();
			return scrollboxSizeVal;
		},

		// Support: IE 9 - 11+, Edge 15 - 18+
		// IE/Edge misreport `getComputedStyle` of table rows with width/height
		// set in CSS while `offset*` properties report correct values.
		// Behavior in IE 9 is more subtle than in newer versions & it passes
		// some versions of this test; make sure not to make it pass there!
		reliableTrDimensions: function() {
			var table, tr, trChild, trStyle;
			if ( reliableTrDimensionsVal == null ) {
				table = document.createElement( "table" );
				tr = document.createElement( "tr" );
				trChild = document.createElement( "div" );

				table.style.cssText = "position:absolute;left:-11111px";
				tr.style.height = "1px";
				trChild.style.height = "9px";

				documentElement
					.appendChild( table )
					.appendChild( tr )
					.appendChild( trChild );

				trStyle = window.getComputedStyle( tr );
				reliableTrDimensionsVal = parseInt( trStyle.height ) > 3;

				documentElement.removeChild( table );
			}
			return reliableTrDimensionsVal;
		}
	} );
} )();


function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,

		// Support: Firefox 51+
		// Retrieving style before computed somehow
		// fixes an issue with getting wrong values
		// on detached elements
		style = elem.style;

	computed = computed || getStyles( elem );

	// getPropertyValue is needed for:
	//   .css('filter') (IE 9 only, #12537)
	//   .css('--customProperty) (#3144)
	if ( computed ) {
		ret = computed.getPropertyValue( name ) || computed[ name ];

		if ( ret === "" && !isAttached( elem ) ) {
			ret = jQuery.style( elem, name );
		}

		// A tribute to the "awesome hack by Dean Edwards"
		// Android Browser returns percentage for some values,
		// but width seems to be reliably pixels.
		// This is against the CSSOM draft spec:
		// https://drafts.csswg.org/cssom/#resolved-values
		if ( !support.pixelBoxStyles() && rnumnonpx.test( ret ) && rboxStyle.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?

		// Support: IE <=9 - 11 only
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {

	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {

				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return ( this.get = hookFn ).apply( this, arguments );
		}
	};
}


var cssPrefixes = [ "Webkit", "Moz", "ms" ],
	emptyStyle = document.createElement( "div" ).style,
	vendorProps = {};

// Return a vendor-prefixed property or undefined
function vendorPropName( name ) {

	// Check for vendor prefixed names
	var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in emptyStyle ) {
			return name;
		}
	}
}

// Return a potentially-mapped jQuery.cssProps or vendor prefixed property
function finalPropName( name ) {
	var final = jQuery.cssProps[ name ] || vendorProps[ name ];

	if ( final ) {
		return final;
	}
	if ( name in emptyStyle ) {
		return name;
	}
	return vendorProps[ name ] = vendorPropName( name ) || name;
}


var

	// Swappable if display is none or starts with table
	// except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rcustomProp = /^--/,
	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	};

function setPositiveNumber( _elem, value, subtract ) {

	// Any relative (+/-) values have already been
	// normalized at this point
	var matches = rcssNum.exec( value );
	return matches ?

		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
		value;
}

function boxModelAdjustment( elem, dimension, box, isBorderBox, styles, computedVal ) {
	var i = dimension === "width" ? 1 : 0,
		extra = 0,
		delta = 0;

	// Adjustment may not be necessary
	if ( box === ( isBorderBox ? "border" : "content" ) ) {
		return 0;
	}

	for ( ; i < 4; i += 2 ) {

		// Both box models exclude margin
		if ( box === "margin" ) {
			delta += jQuery.css( elem, box + cssExpand[ i ], true, styles );
		}

		// If we get here with a content-box, we're seeking "padding" or "border" or "margin"
		if ( !isBorderBox ) {

			// Add padding
			delta += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// For "border" or "margin", add border
			if ( box !== "padding" ) {
				delta += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );

			// But still keep track of it otherwise
			} else {
				extra += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}

		// If we get here with a border-box (content + padding + border), we're seeking "content" or
		// "padding" or "margin"
		} else {

			// For "content", subtract padding
			if ( box === "content" ) {
				delta -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// For "content" or "padding", subtract border
			if ( box !== "margin" ) {
				delta -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	// Account for positive content-box scroll gutter when requested by providing computedVal
	if ( !isBorderBox && computedVal >= 0 ) {

		// offsetWidth/offsetHeight is a rounded sum of content, padding, scroll gutter, and border
		// Assuming integer scroll gutter, subtract the rest and round down
		delta += Math.max( 0, Math.ceil(
			elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
			computedVal -
			delta -
			extra -
			0.5

		// If offsetWidth/offsetHeight is unknown, then we can't determine content-box scroll gutter
		// Use an explicit zero to avoid NaN (gh-3964)
		) ) || 0;
	}

	return delta;
}

function getWidthOrHeight( elem, dimension, extra ) {

	// Start with computed style
	var styles = getStyles( elem ),

		// To avoid forcing a reflow, only fetch boxSizing if we need it (gh-4322).
		// Fake content-box until we know it's needed to know the true value.
		boxSizingNeeded = !support.boxSizingReliable() || extra,
		isBorderBox = boxSizingNeeded &&
			jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
		valueIsBorderBox = isBorderBox,

		val = curCSS( elem, dimension, styles ),
		offsetProp = "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 );

	// Support: Firefox <=54
	// Return a confounding non-pixel value or feign ignorance, as appropriate.
	if ( rnumnonpx.test( val ) ) {
		if ( !extra ) {
			return val;
		}
		val = "auto";
	}


	// Support: IE 9 - 11 only
	// Use offsetWidth/offsetHeight for when box sizing is unreliable.
	// In those cases, the computed value can be trusted to be border-box.
	if ( ( !support.boxSizingReliable() && isBorderBox ||

		// Support: IE 10 - 11+, Edge 15 - 18+
		// IE/Edge misreport `getComputedStyle` of table rows with width/height
		// set in CSS while `offset*` properties report correct values.
		// Interestingly, in some cases IE 9 doesn't suffer from this issue.
		!support.reliableTrDimensions() && nodeName( elem, "tr" ) ||

		// Fall back to offsetWidth/offsetHeight when value is "auto"
		// This happens for inline elements with no explicit setting (gh-3571)
		val === "auto" ||

		// Support: Android <=4.1 - 4.3 only
		// Also use offsetWidth/offsetHeight for misreported inline dimensions (gh-3602)
		!parseFloat( val ) && jQuery.css( elem, "display", false, styles ) === "inline" ) &&

		// Make sure the element is visible & connected
		elem.getClientRects().length ) {

		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

		// Where available, offsetWidth/offsetHeight approximate border box dimensions.
		// Where not available (e.g., SVG), assume unreliable box-sizing and interpret the
		// retrieved value as a content box dimension.
		valueIsBorderBox = offsetProp in elem;
		if ( valueIsBorderBox ) {
			val = elem[ offsetProp ];
		}
	}

	// Normalize "" and auto
	val = parseFloat( val ) || 0;

	// Adjust for the element's box model
	return ( val +
		boxModelAdjustment(
			elem,
			dimension,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles,

			// Provide the current computed size to request scroll gutter calculation (gh-3589)
			val
		)
	) + "px";
}

jQuery.extend( {

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"animationIterationCount": true,
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"gridArea": true,
		"gridColumn": true,
		"gridColumnEnd": true,
		"gridColumnStart": true,
		"gridRow": true,
		"gridRowEnd": true,
		"gridRowStart": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = camelCase( name ),
			isCustomProp = rcustomProp.test( name ),
			style = elem.style;

		// Make sure that we're working with the right name. We don't
		// want to query the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
				value = adjustCSS( elem, name, ret );

				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add the unit (except for certain CSS properties)
			// The isCustomProp check can be removed in jQuery 4.0 when we only auto-append
			// "px" to a few hardcoded values.
			if ( type === "number" && !isCustomProp ) {
				value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
			}

			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !( "set" in hooks ) ||
				( value = hooks.set( elem, value, extra ) ) !== undefined ) {

				if ( isCustomProp ) {
					style.setProperty( name, value );
				} else {
					style[ name ] = value;
				}
			}

		} else {

			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks &&
				( ret = hooks.get( elem, false, extra ) ) !== undefined ) {

				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = camelCase( name ),
			isCustomProp = rcustomProp.test( name );

		// Make sure that we're working with the right name. We don't
		// want to modify the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || isFinite( num ) ? num || 0 : val;
		}

		return val;
	}
} );

jQuery.each( [ "height", "width" ], function( _i, dimension ) {
	jQuery.cssHooks[ dimension ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&

					// Support: Safari 8+
					// Table columns in Safari have non-zero offsetWidth & zero
					// getBoundingClientRect().width unless display is changed.
					// Support: IE <=11 only
					// Running getBoundingClientRect on a disconnected node
					// in IE throws an error.
					( !elem.getClientRects().length || !elem.getBoundingClientRect().width ) ?
						swap( elem, cssShow, function() {
							return getWidthOrHeight( elem, dimension, extra );
						} ) :
						getWidthOrHeight( elem, dimension, extra );
			}
		},

		set: function( elem, value, extra ) {
			var matches,
				styles = getStyles( elem ),

				// Only read styles.position if the test has a chance to fail
				// to avoid forcing a reflow.
				scrollboxSizeBuggy = !support.scrollboxSize() &&
					styles.position === "absolute",

				// To avoid forcing a reflow, only fetch boxSizing if we need it (gh-3991)
				boxSizingNeeded = scrollboxSizeBuggy || extra,
				isBorderBox = boxSizingNeeded &&
					jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
				subtract = extra ?
					boxModelAdjustment(
						elem,
						dimension,
						extra,
						isBorderBox,
						styles
					) :
					0;

			// Account for unreliable border-box dimensions by comparing offset* to computed and
			// faking a content-box to get border and padding (gh-3699)
			if ( isBorderBox && scrollboxSizeBuggy ) {
				subtract -= Math.ceil(
					elem[ "offset" + dimension[ 0 ].toUpperCase() + dimension.slice( 1 ) ] -
					parseFloat( styles[ dimension ] ) -
					boxModelAdjustment( elem, dimension, "border", false, styles ) -
					0.5
				);
			}

			// Convert to pixels if value adjustment is needed
			if ( subtract && ( matches = rcssNum.exec( value ) ) &&
				( matches[ 3 ] || "px" ) !== "px" ) {

				elem.style[ dimension ] = value;
				value = jQuery.css( elem, dimension );
			}

			return setPositiveNumber( elem, value, subtract );
		}
	};
} );

jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
	function( elem, computed ) {
		if ( computed ) {
			return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
				elem.getBoundingClientRect().left -
					swap( elem, { marginLeft: 0 }, function() {
						return elem.getBoundingClientRect().left;
					} )
				) + "px";
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each( {
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split( " " ) : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( prefix !== "margin" ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
} );

jQuery.fn.extend( {
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( Array.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	}
} );


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || jQuery.easing._default;
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			// Use a property on the element directly when it is not a DOM element,
			// or when there is no matching style property that exists.
			if ( tween.elem.nodeType !== 1 ||
				tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );

			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {

			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.nodeType === 1 && (
					jQuery.cssHooks[ tween.prop ] ||
					tween.elem.style[ finalPropName( tween.prop ) ] != null ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE <=9 only
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	},
	_default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, inProgress,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rrun = /queueHooks$/;

function schedule() {
	if ( inProgress ) {
		if ( document.hidden === false && window.requestAnimationFrame ) {
			window.requestAnimationFrame( schedule );
		} else {
			window.setTimeout( schedule, jQuery.fx.interval );
		}

		jQuery.fx.tick();
	}
}

// Animations created synchronously will run synchronously
function createFxNow() {
	window.setTimeout( function() {
		fxNow = undefined;
	} );
	return ( fxNow = Date.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
		isBox = "width" in props || "height" in props,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHiddenWithinTree( elem ),
		dataShow = dataPriv.get( elem, "fxshow" );

	// Queue-skipping animations hijack the fx hooks
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always( function() {

			// Ensure the complete handler is called before this completes
			anim.always( function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			} );
		} );
	}

	// Detect show/hide animations
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.test( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// Pretend to be hidden if this is a "show" and
				// there is still data from a stopped show/hide
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;

				// Ignore all other no-op show/hide data
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
		}
	}

	// Bail out if this is a no-op like .hide().hide()
	propTween = !jQuery.isEmptyObject( props );
	if ( !propTween && jQuery.isEmptyObject( orig ) ) {
		return;
	}

	// Restrict "overflow" and "display" styles during box animations
	if ( isBox && elem.nodeType === 1 ) {

		// Support: IE <=9 - 11, Edge 12 - 15
		// Record all 3 overflow attributes because IE does not infer the shorthand
		// from identically-valued overflowX and overflowY and Edge just mirrors
		// the overflowX value there.
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Identify a display type, preferring old show/hide data over the CSS cascade
		restoreDisplay = dataShow && dataShow.display;
		if ( restoreDisplay == null ) {
			restoreDisplay = dataPriv.get( elem, "display" );
		}
		display = jQuery.css( elem, "display" );
		if ( display === "none" ) {
			if ( restoreDisplay ) {
				display = restoreDisplay;
			} else {

				// Get nonempty value(s) by temporarily forcing visibility
				showHide( [ elem ], true );
				restoreDisplay = elem.style.display || restoreDisplay;
				display = jQuery.css( elem, "display" );
				showHide( [ elem ] );
			}
		}

		// Animate inline elements as inline-block
		if ( display === "inline" || display === "inline-block" && restoreDisplay != null ) {
			if ( jQuery.css( elem, "float" ) === "none" ) {

				// Restore the original display value at the end of pure show/hide animations
				if ( !propTween ) {
					anim.done( function() {
						style.display = restoreDisplay;
					} );
					if ( restoreDisplay == null ) {
						display = style.display;
						restoreDisplay = display === "none" ? "" : display;
					}
				}
				style.display = "inline-block";
			}
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always( function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		} );
	}

	// Implement show/hide animations
	propTween = false;
	for ( prop in orig ) {

		// General show/hide setup for this element animation
		if ( !propTween ) {
			if ( dataShow ) {
				if ( "hidden" in dataShow ) {
					hidden = dataShow.hidden;
				}
			} else {
				dataShow = dataPriv.access( elem, "fxshow", { display: restoreDisplay } );
			}

			// Store hidden/visible for toggle so `.stop().toggle()` "reverses"
			if ( toggle ) {
				dataShow.hidden = !hidden;
			}

			// Show elements before animating them
			if ( hidden ) {
				showHide( [ elem ], true );
			}

			/* eslint-disable no-loop-func */

			anim.done( function() {

			/* eslint-enable no-loop-func */

				// The final step of a "hide" animation is actually hiding the element
				if ( !hidden ) {
					showHide( [ elem ] );
				}
				dataPriv.remove( elem, "fxshow" );
				for ( prop in orig ) {
					jQuery.style( elem, prop, orig[ prop ] );
				}
			} );
		}

		// Per-property setup
		propTween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );
		if ( !( prop in dataShow ) ) {
			dataShow[ prop ] = propTween.start;
			if ( hidden ) {
				propTween.end = propTween.start;
				propTween.start = 0;
			}
		}
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( Array.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = Animation.prefilters.length,
		deferred = jQuery.Deferred().always( function() {

			// Don't match elem in the :animated selector
			delete tick.elem;
		} ),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),

				// Support: Android 2.3 only
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ] );

			// If there's more to do, yield
			if ( percent < 1 && length ) {
				return remaining;
			}

			// If this was an empty animation, synthesize a final progress notification
			if ( !length ) {
				deferred.notifyWith( elem, [ animation, 1, 0 ] );
			}

			// Resolve the animation and report its conclusion
			deferred.resolveWith( elem, [ animation ] );
			return false;
		},
		animation = deferred.promise( {
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, {
				specialEasing: {},
				easing: jQuery.easing._default
			}, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,

					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.notifyWith( elem, [ animation, 1, 0 ] );
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		} ),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length; index++ ) {
		result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			if ( isFunction( result.stop ) ) {
				jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
					result.stop.bind( result );
			}
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	// Attach callbacks from options
	animation
		.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		} )
	);

	return animation;
}

jQuery.Animation = jQuery.extend( Animation, {

	tweeners: {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value );
			adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
			return tween;
		} ]
	},

	tweener: function( props, callback ) {
		if ( isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.match( rnothtmlwhite );
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length; index++ ) {
			prop = props[ index ];
			Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
			Animation.tweeners[ prop ].unshift( callback );
		}
	},

	prefilters: [ defaultPrefilter ],

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			Animation.prefilters.unshift( callback );
		} else {
			Animation.prefilters.push( callback );
		}
	}
} );

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !isFunction( easing ) && easing
	};

	// Go to the end state if fx are off
	if ( jQuery.fx.off ) {
		opt.duration = 0;

	} else {
		if ( typeof opt.duration !== "number" ) {
			if ( opt.duration in jQuery.fx.speeds ) {
				opt.duration = jQuery.fx.speeds[ opt.duration ];

			} else {
				opt.duration = jQuery.fx.speeds._default;
			}
		}
	}

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend( {
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHiddenWithinTree ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate( { opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {

				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || dataPriv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue ) {
			this.queue( type || "fx", [] );
		}

		return this.each( function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = dataPriv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this &&
					( type == null || timers[ index ].queue === type ) ) {

					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		} );
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each( function() {
			var index,
				data = dataPriv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		} );
	}
} );

jQuery.each( [ "toggle", "show", "hide" ], function( _i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
} );

// Generate shortcuts for custom animations
jQuery.each( {
	slideDown: genFx( "show" ),
	slideUp: genFx( "hide" ),
	slideToggle: genFx( "toggle" ),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
} );

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = Date.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];

		// Run the timer and safely remove it when done (allowing for external removal)
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	jQuery.fx.start();
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
	if ( inProgress ) {
		return;
	}

	inProgress = true;
	schedule();
};

jQuery.fx.stop = function() {
	inProgress = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,

	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// https://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = window.setTimeout( next, time );
		hooks.stop = function() {
			window.clearTimeout( timeout );
		};
	} );
};


( function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: Android <=4.3 only
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE <=11 only
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: IE <=11 only
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
} )();


var boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend( {
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each( function() {
			jQuery.removeAttr( this, name );
		} );
	}
} );

jQuery.extend( {
	attr: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set attributes on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === "undefined" ) {
			return jQuery.prop( elem, name, value );
		}

		// Attribute hooks are determined by the lowercase version
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			hooks = jQuery.attrHooks[ name.toLowerCase() ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
		}

		if ( value !== undefined ) {
			if ( value === null ) {
				jQuery.removeAttr( elem, name );
				return;
			}

			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			elem.setAttribute( name, value + "" );
			return value;
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		ret = jQuery.find.attr( elem, name );

		// Non-existent attributes return null, we normalize to undefined
		return ret == null ? undefined : ret;
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	},

	removeAttr: function( elem, value ) {
		var name,
			i = 0,

			// Attribute names can contain non-HTML whitespace characters
			// https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
			attrNames = value && value.match( rnothtmlwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( ( name = attrNames[ i++ ] ) ) {
				elem.removeAttribute( name );
			}
		}
	}
} );

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {

			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};

jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( _i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle,
			lowercaseName = name.toLowerCase();

		if ( !isXML ) {

			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ lowercaseName ];
			attrHandle[ lowercaseName ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				lowercaseName :
				null;
			attrHandle[ lowercaseName ] = handle;
		}
		return ret;
	};
} );




var rfocusable = /^(?:input|select|textarea|button)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend( {
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each( function() {
			delete this[ jQuery.propFix[ name ] || name ];
		} );
	}
} );

jQuery.extend( {
	prop: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			return ( elem[ name ] = value );
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		return elem[ name ];
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {

				// Support: IE <=9 - 11 only
				// elem.tabIndex doesn't always return the
				// correct value when it hasn't been explicitly set
				// https://web.archive.org/web/20141116233347/http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				if ( tabindex ) {
					return parseInt( tabindex, 10 );
				}

				if (
					rfocusable.test( elem.nodeName ) ||
					rclickable.test( elem.nodeName ) &&
					elem.href
				) {
					return 0;
				}

				return -1;
			}
		}
	},

	propFix: {
		"for": "htmlFor",
		"class": "className"
	}
} );

// Support: IE <=11 only
// Accessing the selectedIndex property
// forces the browser to respect setting selected
// on the option
// The getter ensures a default option is selected
// when in an optgroup
// eslint rule "no-unused-expressions" is disabled for this code
// since it considers such accessions noop
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		},
		set: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent ) {
				parent.selectedIndex;

				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
		}
	};
}

jQuery.each( [
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
} );




	// Strip and collapse whitespace according to HTML spec
	// https://infra.spec.whatwg.org/#strip-and-collapse-ascii-whitespace
	function stripAndCollapse( value ) {
		var tokens = value.match( rnothtmlwhite ) || [];
		return tokens.join( " " );
	}


function getClass( elem ) {
	return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

function classesToArray( value ) {
	if ( Array.isArray( value ) ) {
		return value;
	}
	if ( typeof value === "string" ) {
		return value.match( rnothtmlwhite ) || [];
	}
	return [];
}

jQuery.fn.extend( {
	addClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		classes = classesToArray( value );

		if ( classes.length ) {
			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( !arguments.length ) {
			return this.attr( "class", "" );
		}

		classes = classesToArray( value );

		if ( classes.length ) {
			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );

				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {

						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value,
			isValidValue = type === "string" || Array.isArray( value );

		if ( typeof stateVal === "boolean" && isValidValue ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( isFunction( value ) ) {
			return this.each( function( i ) {
				jQuery( this ).toggleClass(
					value.call( this, i, getClass( this ), stateVal ),
					stateVal
				);
			} );
		}

		return this.each( function() {
			var className, i, self, classNames;

			if ( isValidValue ) {

				// Toggle individual class names
				i = 0;
				self = jQuery( this );
				classNames = classesToArray( value );

				while ( ( className = classNames[ i++ ] ) ) {

					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( value === undefined || type === "boolean" ) {
				className = getClass( this );
				if ( className ) {

					// Store className if set
					dataPriv.set( this, "__className__", className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				if ( this.setAttribute ) {
					this.setAttribute( "class",
						className || value === false ?
						"" :
						dataPriv.get( this, "__className__" ) || ""
					);
				}
			}
		} );
	},

	hasClass: function( selector ) {
		var className, elem,
			i = 0;

		className = " " + selector + " ";
		while ( ( elem = this[ i++ ] ) ) {
			if ( elem.nodeType === 1 &&
				( " " + stripAndCollapse( getClass( elem ) ) + " " ).indexOf( className ) > -1 ) {
					return true;
			}
		}

		return false;
	}
} );




var rreturn = /\r/g;

jQuery.fn.extend( {
	val: function( value ) {
		var hooks, ret, valueIsFunction,
			elem = this[ 0 ];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] ||
					jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks &&
					"get" in hooks &&
					( ret = hooks.get( elem, "value" ) ) !== undefined
				) {
					return ret;
				}

				ret = elem.value;

				// Handle most common string cases
				if ( typeof ret === "string" ) {
					return ret.replace( rreturn, "" );
				}

				// Handle cases where value is null/undef or number
				return ret == null ? "" : ret;
			}

			return;
		}

		valueIsFunction = isFunction( value );

		return this.each( function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( valueIsFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( Array.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				} );
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		} );
	}
} );

jQuery.extend( {
	valHooks: {
		option: {
			get: function( elem ) {

				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :

					// Support: IE <=10 - 11 only
					// option.text throws exceptions (#14686, #14858)
					// Strip and collapse whitespace
					// https://html.spec.whatwg.org/#strip-and-collapse-whitespace
					stripAndCollapse( jQuery.text( elem ) );
			}
		},
		select: {
			get: function( elem ) {
				var value, option, i,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one",
					values = one ? null : [],
					max = one ? index + 1 : options.length;

				if ( index < 0 ) {
					i = max;

				} else {
					i = one ? index : 0;
				}

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// Support: IE <=9 only
					// IE8-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&

							// Don't return options that are disabled or in a disabled optgroup
							!option.disabled &&
							( !option.parentNode.disabled ||
								!nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];

					/* eslint-disable no-cond-assign */

					if ( option.selected =
						jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
					) {
						optionSet = true;
					}

					/* eslint-enable no-cond-assign */
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
} );

// Radios and checkboxes getter/setter
jQuery.each( [ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( Array.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute( "value" ) === null ? "on" : elem.value;
		};
	}
} );




// Return jQuery for attributes-only inclusion


support.focusin = "onfocusin" in window;


var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	stopPropagationCallback = function( e ) {
		e.stopPropagation();
	};

jQuery.extend( jQuery.event, {

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special, lastElement,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];

		cur = lastElement = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf( "." ) > -1 ) {

			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split( "." );
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf( ":" ) < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join( "." );
		event.rnamespace = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === ( elem.ownerDocument || document ) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {
			lastElement = cur;
			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = (
					dataPriv.get( cur, "events" ) || Object.create( null )
				)[ event.type ] &&
				dataPriv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( ( !special._default ||
				special._default.apply( eventPath.pop(), data ) === false ) &&
				acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && isFunction( elem[ type ] ) && !isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;

					if ( event.isPropagationStopped() ) {
						lastElement.addEventListener( type, stopPropagationCallback );
					}

					elem[ type ]();

					if ( event.isPropagationStopped() ) {
						lastElement.removeEventListener( type, stopPropagationCallback );
					}

					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	// Piggyback on a donor event to simulate a different one
	// Used only for `focus(in | out)` events
	simulate: function( type, elem, event ) {
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true
			}
		);

		jQuery.event.trigger( e, null, elem );
	}

} );

jQuery.fn.extend( {

	trigger: function( type, data ) {
		return this.each( function() {
			jQuery.event.trigger( type, data, this );
		} );
	},
	triggerHandler: function( type, data ) {
		var elem = this[ 0 ];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
} );


// Support: Firefox <=44
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome <=48 - 49, Safari <=9.0 - 9.1
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://bugs.chromium.org/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
	jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
			jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
		};

		jQuery.event.special[ fix ] = {
			setup: function() {

				// Handle: regular nodes (via `this.ownerDocument`), window
				// (via `this.document`) & document (via `this`).
				var doc = this.ownerDocument || this.document || this,
					attaches = dataPriv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this.document || this,
					attaches = dataPriv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					dataPriv.remove( doc, fix );

				} else {
					dataPriv.access( doc, fix, attaches );
				}
			}
		};
	} );
}
var location = window.location;

var nonce = { guid: Date.now() };

var rquery = ( /\?/ );



// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE 9 - 11 only
	// IE throws on parseFromString with invalid input.
	try {
		xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( Array.isArray( obj ) ) {

		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {

				// Treat each array item as a scalar.
				add( prefix, v );

			} else {

				// Item is non-scalar (array or object), encode its numeric index.
				buildParams(
					prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
					v,
					traditional,
					add
				);
			}
		} );

	} else if ( !traditional && toType( obj ) === "object" ) {

		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {

		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, valueOrFunction ) {

			// If value is a function, invoke it and use its return value
			var value = isFunction( valueOrFunction ) ?
				valueOrFunction() :
				valueOrFunction;

			s[ s.length ] = encodeURIComponent( key ) + "=" +
				encodeURIComponent( value == null ? "" : value );
		};

	if ( a == null ) {
		return "";
	}

	// If an array was passed in, assume that it is an array of form elements.
	if ( Array.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {

		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		} );

	} else {

		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" );
};

jQuery.fn.extend( {
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map( function() {

			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		} )
		.filter( function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		} )
		.map( function( _i, elem ) {
			var val = jQuery( this ).val();

			if ( val == null ) {
				return null;
			}

			if ( Array.isArray( val ) ) {
				return jQuery.map( val, function( val ) {
					return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
				} );
			}

			return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		} ).get();
	}
} );


var
	r20 = /%20/g,
	rhash = /#.*$/,
	rantiCache = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,

	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Anchor tag for parsing the document origin
	originAnchor = document.createElement( "a" );
	originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnothtmlwhite ) || [];

		if ( isFunction( func ) ) {

			// For each dataType in the dataTypeExpression
			while ( ( dataType = dataTypes[ i++ ] ) ) {

				// Prepend if requested
				if ( dataType[ 0 ] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );

				// Otherwise append
				} else {
					( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" &&
				!seekingTransport && !inspected[ dataTypeOrTransport ] ) {

				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		} );
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {

		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}

		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},

		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

			// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {

								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s.throws ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return {
								state: "parsererror",
								error: conv ? e : "No conversion from " + prev + " to " + current
							};
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend( {

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: location.href,
		type: "GET",
		isLocal: rlocalProtocol.test( location.protocol ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",

		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /\bxml\b/,
			html: /\bhtml/,
			json: /\bjson\b/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": JSON.parse,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,

			// URL without anti-cache param
			cacheURL,

			// Response headers
			responseHeadersString,
			responseHeaders,

			// timeout handle
			timeoutTimer,

			// Url cleanup var
			urlAnchor,

			// Request state (becomes false upon send and true upon completion)
			completed,

			// To know if global events are to be dispatched
			fireGlobals,

			// Loop variable
			i,

			// uncached part of the url
			uncached,

			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),

			// Callbacks context
			callbackContext = s.context || s,

			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context &&
				( callbackContext.nodeType || callbackContext.jquery ) ?
					jQuery( callbackContext ) :
					jQuery.event,

			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks( "once memory" ),

			// Status-dependent callbacks
			statusCode = s.statusCode || {},

			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},

			// Default abort message
			strAbort = "canceled",

			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( completed ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
								responseHeaders[ match[ 1 ].toLowerCase() + " " ] =
									( responseHeaders[ match[ 1 ].toLowerCase() + " " ] || [] )
										.concat( match[ 2 ] );
							}
						}
						match = responseHeaders[ key.toLowerCase() + " " ];
					}
					return match == null ? null : match.join( ", " );
				},

				// Raw string
				getAllResponseHeaders: function() {
					return completed ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					if ( completed == null ) {
						name = requestHeadersNames[ name.toLowerCase() ] =
							requestHeadersNames[ name.toLowerCase() ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( completed == null ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( completed ) {

							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						} else {

							// Lazy-add the new callbacks in a way that preserves old ones
							for ( code in map ) {
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR );

		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || location.href ) + "" )
			.replace( rprotocol, location.protocol + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = ( s.dataType || "*" ).toLowerCase().match( rnothtmlwhite ) || [ "" ];

		// A cross-domain request is in order when the origin doesn't match the current origin.
		if ( s.crossDomain == null ) {
			urlAnchor = document.createElement( "a" );

			// Support: IE <=8 - 11, Edge 12 - 15
			// IE throws exception on accessing the href property if url is malformed,
			// e.g. http://example.com:80x/
			try {
				urlAnchor.href = s.url;

				// Support: IE <=8 - 11 only
				// Anchor's host property isn't correctly set when s.url is relative
				urlAnchor.href = urlAnchor.href;
				s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
					urlAnchor.protocol + "//" + urlAnchor.host;
			} catch ( e ) {

				// If there is an error parsing the URL, assume it is crossDomain,
				// it can be rejected by the transport if it is invalid
				s.crossDomain = true;
			}
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( completed ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger( "ajaxStart" );
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		// Remove hash to simplify url manipulation
		cacheURL = s.url.replace( rhash, "" );

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// Remember the hash so we can put it back
			uncached = s.url.slice( cacheURL.length );

			// If data is available and should be processed, append data to url
			if ( s.data && ( s.processData || typeof s.data === "string" ) ) {
				cacheURL += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data;

				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add or update anti-cache param if needed
			if ( s.cache === false ) {
				cacheURL = cacheURL.replace( rantiCache, "$1" );
				uncached = ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + ( nonce.guid++ ) +
					uncached;
			}

			// Put hash and anti-cache on the URL that will be requested (gh-1732)
			s.url = cacheURL + uncached;

		// Change '%20' to '+' if this is encoded form body content (gh-2658)
		} else if ( s.data && s.processData &&
			( s.contentType || "" ).indexOf( "application/x-www-form-urlencoded" ) === 0 ) {
			s.data = s.data.replace( r20, "+" );
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
				s.accepts[ s.dataTypes[ 0 ] ] +
					( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend &&
			( s.beforeSend.call( callbackContext, jqXHR, s ) === false || completed ) ) {

			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		completeDeferred.add( s.complete );
		jqXHR.done( s.success );
		jqXHR.fail( s.error );

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}

			// If request was aborted inside ajaxSend, stop there
			if ( completed ) {
				return jqXHR;
			}

			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = window.setTimeout( function() {
					jqXHR.abort( "timeout" );
				}, s.timeout );
			}

			try {
				completed = false;
				transport.send( requestHeaders, done );
			} catch ( e ) {

				// Rethrow post-completion exceptions
				if ( completed ) {
					throw e;
				}

				// Propagate others as results
				done( -1, e );
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Ignore repeat invocations
			if ( completed ) {
				return;
			}

			completed = true;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				window.clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Use a noop converter for missing script
			if ( !isSuccess && jQuery.inArray( "script", s.dataTypes ) > -1 ) {
				s.converters[ "text script" ] = function() {};
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader( "Last-Modified" );
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader( "etag" );
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {

				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );

				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger( "ajaxStop" );
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
} );

jQuery.each( [ "get", "post" ], function( _i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {

		// Shift arguments if data argument was omitted
		if ( isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		// The url can be an options object (which then must have .url)
		return jQuery.ajax( jQuery.extend( {
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		}, jQuery.isPlainObject( url ) && url ) );
	};
} );

jQuery.ajaxPrefilter( function( s ) {
	var i;
	for ( i in s.headers ) {
		if ( i.toLowerCase() === "content-type" ) {
			s.contentType = s.headers[ i ] || "";
		}
	}
} );


jQuery._evalUrl = function( url, options, doc ) {
	return jQuery.ajax( {
		url: url,

		// Make this explicit, since user can override this through ajaxSetup (#11264)
		type: "GET",
		dataType: "script",
		cache: true,
		async: false,
		global: false,

		// Only evaluate the response if it is successful (gh-4126)
		// dataFilter is not invoked for failure responses, so using it instead
		// of the default converter is kludgy but it works.
		converters: {
			"text script": function() {}
		},
		dataFilter: function( response ) {
			jQuery.globalEval( response, options, doc );
		}
	} );
};


jQuery.fn.extend( {
	wrapAll: function( html ) {
		var wrap;

		if ( this[ 0 ] ) {
			if ( isFunction( html ) ) {
				html = html.call( this[ 0 ] );
			}

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map( function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			} ).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( isFunction( html ) ) {
			return this.each( function( i ) {
				jQuery( this ).wrapInner( html.call( this, i ) );
			} );
		}

		return this.each( function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		} );
	},

	wrap: function( html ) {
		var htmlIsFunction = isFunction( html );

		return this.each( function( i ) {
			jQuery( this ).wrapAll( htmlIsFunction ? html.call( this, i ) : html );
		} );
	},

	unwrap: function( selector ) {
		this.parent( selector ).not( "body" ).each( function() {
			jQuery( this ).replaceWith( this.childNodes );
		} );
		return this;
	}
} );


jQuery.expr.pseudos.hidden = function( elem ) {
	return !jQuery.expr.pseudos.visible( elem );
};
jQuery.expr.pseudos.visible = function( elem ) {
	return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
};




jQuery.ajaxSettings.xhr = function() {
	try {
		return new window.XMLHttpRequest();
	} catch ( e ) {}
};

var xhrSuccessStatus = {

		// File protocol always yields status code 0, assume 200
		0: 200,

		// Support: IE <=9 only
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport( function( options ) {
	var callback, errorCallback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr();

				xhr.open(
					options.type,
					options.url,
					options.async,
					options.username,
					options.password
				);

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
					headers[ "X-Requested-With" ] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							callback = errorCallback = xhr.onload =
								xhr.onerror = xhr.onabort = xhr.ontimeout =
									xhr.onreadystatechange = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {

								// Support: IE <=9 only
								// On a manual native abort, IE9 throws
								// errors on any property access that is not readyState
								if ( typeof xhr.status !== "number" ) {
									complete( 0, "error" );
								} else {
									complete(

										// File: protocol always yields status 0; see #8605, #14207
										xhr.status,
										xhr.statusText
									);
								}
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,

									// Support: IE <=9 only
									// IE9 has no XHR2 but throws on binary (trac-11426)
									// For XHR2 non-text, let the caller handle it (gh-2498)
									( xhr.responseType || "text" ) !== "text"  ||
									typeof xhr.responseText !== "string" ?
										{ binary: xhr.response } :
										{ text: xhr.responseText },
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				errorCallback = xhr.onerror = xhr.ontimeout = callback( "error" );

				// Support: IE 9 only
				// Use onreadystatechange to replace onabort
				// to handle uncaught aborts
				if ( xhr.onabort !== undefined ) {
					xhr.onabort = errorCallback;
				} else {
					xhr.onreadystatechange = function() {

						// Check readyState before timeout as it changes
						if ( xhr.readyState === 4 ) {

							// Allow onerror to be called first,
							// but that will not handle a native abort
							// Also, save errorCallback to a variable
							// as xhr.onerror cannot be accessed
							window.setTimeout( function() {
								if ( callback ) {
									errorCallback();
								}
							} );
						}
					};
				}

				// Create the abort callback
				callback = callback( "abort" );

				try {

					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {

					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




// Prevent auto-execution of scripts when no explicit dataType was provided (See gh-2432)
jQuery.ajaxPrefilter( function( s ) {
	if ( s.crossDomain ) {
		s.contents.script = false;
	}
} );

// Install script dataType
jQuery.ajaxSetup( {
	accepts: {
		script: "text/javascript, application/javascript, " +
			"application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /\b(?:java|ecma)script\b/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
} );

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
} );

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {

	// This transport only deals with cross domain or forced-by-attrs requests
	if ( s.crossDomain || s.scriptAttrs ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery( "<script>" )
					.attr( s.scriptAttrs || {} )
					.prop( { charset: s.scriptCharset, src: s.url } )
					.on( "load error", callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					} );

				// Use native DOM manipulation to avoid our domManip AJAX trickery
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup( {
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce.guid++ ) );
		this[ callback ] = true;
		return callback;
	}
} );

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" &&
				( s.contentType || "" )
					.indexOf( "application/x-www-form-urlencoded" ) === 0 &&
				rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters[ "script json" ] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// Force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always( function() {

			// If previous value didn't exist - remove it
			if ( overwritten === undefined ) {
				jQuery( window ).removeProp( callbackName );

			// Otherwise restore preexisting value
			} else {
				window[ callbackName ] = overwritten;
			}

			// Save back as free
			if ( s[ callbackName ] ) {

				// Make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// Save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		} );

		// Delegate to script
		return "script";
	}
} );




// Support: Safari 8 only
// In Safari 8 documents created via document.implementation.createHTMLDocument
// collapse sibling forms: the second one becomes a child of the first one.
// Because of that, this security measure has to be disabled in Safari 8.
// https://bugs.webkit.org/show_bug.cgi?id=137337
support.createHTMLDocument = ( function() {
	var body = document.implementation.createHTMLDocument( "" ).body;
	body.innerHTML = "<form></form><form></form>";
	return body.childNodes.length === 2;
} )();


// Argument "data" should be string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( typeof data !== "string" ) {
		return [];
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}

	var base, parsed, scripts;

	if ( !context ) {

		// Stop scripts or inline event handlers from being executed immediately
		// by using document.implementation
		if ( support.createHTMLDocument ) {
			context = document.implementation.createHTMLDocument( "" );

			// Set the base href for the created document
			// so any parsed elements with URLs
			// are based on the document's URL (gh-2965)
			base = context.createElement( "base" );
			base.href = document.location.href;
			context.head.appendChild( base );
		} else {
			context = document;
		}
	}

	parsed = rsingleTag.exec( data );
	scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[ 1 ] ) ];
	}

	parsed = buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	var selector, type, response,
		self = this,
		off = url.indexOf( " " );

	if ( off > -1 ) {
		selector = stripAndCollapse( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax( {
			url: url,

			// If "type" variable is undefined, then "GET" method will be used.
			// Make value of this field explicit since
			// user can override it through ajaxSetup method
			type: type || "GET",
			dataType: "html",
			data: params
		} ).done( function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		// If the request succeeds, this function gets "data", "status", "jqXHR"
		// but they are ignored because response was set above.
		// If it fails, this function gets "jqXHR", "status", "error"
		} ).always( callback && function( jqXHR, status ) {
			self.each( function() {
				callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
			} );
		} );
	}

	return this;
};




jQuery.expr.pseudos.animated = function( elem ) {
	return jQuery.grep( jQuery.timers, function( fn ) {
		return elem === fn.elem;
	} ).length;
};




jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( isFunction( options ) ) {

			// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
			options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			if ( typeof props.top === "number" ) {
				props.top += "px";
			}
			if ( typeof props.left === "number" ) {
				props.left += "px";
			}
			curElem.css( props );
		}
	}
};

jQuery.fn.extend( {

	// offset() relates an element's border box to the document origin
	offset: function( options ) {

		// Preserve chaining for setter
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each( function( i ) {
					jQuery.offset.setOffset( this, options, i );
				} );
		}

		var rect, win,
			elem = this[ 0 ];

		if ( !elem ) {
			return;
		}

		// Return zeros for disconnected and hidden (display: none) elements (gh-2310)
		// Support: IE <=11 only
		// Running getBoundingClientRect on a
		// disconnected node in IE throws an error
		if ( !elem.getClientRects().length ) {
			return { top: 0, left: 0 };
		}

		// Get document-relative position by adding viewport scroll to viewport-relative gBCR
		rect = elem.getBoundingClientRect();
		win = elem.ownerDocument.defaultView;
		return {
			top: rect.top + win.pageYOffset,
			left: rect.left + win.pageXOffset
		};
	},

	// position() relates an element's margin box to its offset parent's padding box
	// This corresponds to the behavior of CSS absolute positioning
	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset, doc,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// position:fixed elements are offset from the viewport, which itself always has zero offset
		if ( jQuery.css( elem, "position" ) === "fixed" ) {

			// Assume position:fixed implies availability of getBoundingClientRect
			offset = elem.getBoundingClientRect();

		} else {
			offset = this.offset();

			// Account for the *real* offset parent, which can be the document or its root element
			// when a statically positioned element is identified
			doc = elem.ownerDocument;
			offsetParent = elem.offsetParent || doc.documentElement;
			while ( offsetParent &&
				( offsetParent === doc.body || offsetParent === doc.documentElement ) &&
				jQuery.css( offsetParent, "position" ) === "static" ) {

				offsetParent = offsetParent.parentNode;
			}
			if ( offsetParent && offsetParent !== elem && offsetParent.nodeType === 1 ) {

				// Incorporate borders into its offset, since they are outside its content origin
				parentOffset = jQuery( offsetParent ).offset();
				parentOffset.top += jQuery.css( offsetParent, "borderTopWidth", true );
				parentOffset.left += jQuery.css( offsetParent, "borderLeftWidth", true );
			}
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	// This method will return documentElement in the following cases:
	// 1) For the element inside the iframe without offsetParent, this method will return
	//    documentElement of the parent window
	// 2) For the hidden or detached element
	// 3) For body or html element, i.e. in case of the html node - it will return itself
	//
	// but those exceptions were never presented as a real life use-cases
	// and might be considered as more preferable results.
	//
	// This logic, however, is not guaranteed and can change at any point in the future
	offsetParent: function() {
		return this.map( function() {
			var offsetParent = this.offsetParent;

			while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || documentElement;
		} );
	}
} );

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {

			// Coalesce documents and windows
			var win;
			if ( isWindow( elem ) ) {
				win = elem;
			} else if ( elem.nodeType === 9 ) {
				win = elem.defaultView;
			}

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : win.pageXOffset,
					top ? val : win.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length );
	};
} );

// Support: Safari <=7 - 9.1, Chrome <=37 - 49
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://bugs.chromium.org/p/chromium/issues/detail?id=589347
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( _i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );

				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
} );


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
		function( defaultExtra, funcName ) {

		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( isWindow( elem ) ) {

					// $( window ).outerWidth/Height return w/h including scrollbars (gh-1729)
					return funcName.indexOf( "outer" ) === 0 ?
						elem[ "inner" + name ] :
						elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?

					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable );
		};
	} );
} );


jQuery.each( [
	"ajaxStart",
	"ajaxStop",
	"ajaxComplete",
	"ajaxError",
	"ajaxSuccess",
	"ajaxSend"
], function( _i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
} );




jQuery.fn.extend( {

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {

		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ?
			this.off( selector, "**" ) :
			this.off( types, selector || "**", fn );
	},

	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	}
} );

jQuery.each( ( "blur focus focusin focusout resize scroll click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup contextmenu" ).split( " " ),
	function( _i, name ) {

		// Handle event binding
		jQuery.fn[ name ] = function( data, fn ) {
			return arguments.length > 0 ?
				this.on( name, null, data, fn ) :
				this.trigger( name );
		};
	} );




// Support: Android <=4.0 only
// Make sure we trim BOM and NBSP
var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

// Bind a function to a context, optionally partially applying any
// arguments.
// jQuery.proxy is deprecated to promote standards (specifically Function#bind)
// However, it is not slated for removal any time soon
jQuery.proxy = function( fn, context ) {
	var tmp, args, proxy;

	if ( typeof context === "string" ) {
		tmp = fn[ context ];
		context = fn;
		fn = tmp;
	}

	// Quick check to determine if target is callable, in the spec
	// this throws a TypeError, but we will just return undefined.
	if ( !isFunction( fn ) ) {
		return undefined;
	}

	// Simulated bind
	args = slice.call( arguments, 2 );
	proxy = function() {
		return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
	};

	// Set the guid of unique handler to the same of original handler, so it can be removed
	proxy.guid = fn.guid = fn.guid || jQuery.guid++;

	return proxy;
};

jQuery.holdReady = function( hold ) {
	if ( hold ) {
		jQuery.readyWait++;
	} else {
		jQuery.ready( true );
	}
};
jQuery.isArray = Array.isArray;
jQuery.parseJSON = JSON.parse;
jQuery.nodeName = nodeName;
jQuery.isFunction = isFunction;
jQuery.isWindow = isWindow;
jQuery.camelCase = camelCase;
jQuery.type = toType;

jQuery.now = Date.now;

jQuery.isNumeric = function( obj ) {

	// As of jQuery 3.0, isNumeric is limited to
	// strings and numbers (primitives or objects)
	// that can be coerced to finite numbers (gh-2662)
	var type = jQuery.type( obj );
	return ( type === "number" || type === "string" ) &&

		// parseFloat NaNs numeric-cast false positives ("")
		// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
		// subtraction forces infinities to NaN
		!isNaN( obj - parseFloat( obj ) );
};

jQuery.trim = function( text ) {
	return text == null ?
		"" :
		( text + "" ).replace( rtrim, "" );
};



// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( true ) {
	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function() {
		return jQuery;
	}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
}




var

	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( typeof noGlobal === "undefined" ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;
} );


/***/ }),

/***/ "./node_modules/webpack-stream/node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./node_modules/webpack-stream/node_modules/webpack/buildin/module.js":
/*!***********************************!*\
  !*** (webpack)/buildin/module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function(module) {
	if (!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ "./node_modules/wowjs/dist/wow.min.js":
/*!********************************************!*\
  !*** ./node_modules/wowjs/dist/wow.min.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*! WOW - v1.1.2 - 2016-04-08
* Copyright (c) 2016 Matthieu Aussaguel;*/(function(){var a,b,c,d,e,f=function(a,b){return function(){return a.apply(b,arguments)}},g=[].indexOf||function(a){for(var b=0,c=this.length;c>b;b++)if(b in this&&this[b]===a)return b;return-1};b=function(){function a(){}return a.prototype.extend=function(a,b){var c,d;for(c in b)d=b[c],null==a[c]&&(a[c]=d);return a},a.prototype.isMobile=function(a){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(a)},a.prototype.createEvent=function(a,b,c,d){var e;return null==b&&(b=!1),null==c&&(c=!1),null==d&&(d=null),null!=document.createEvent?(e=document.createEvent("CustomEvent"),e.initCustomEvent(a,b,c,d)):null!=document.createEventObject?(e=document.createEventObject(),e.eventType=a):e.eventName=a,e},a.prototype.emitEvent=function(a,b){return null!=a.dispatchEvent?a.dispatchEvent(b):b in(null!=a)?a[b]():"on"+b in(null!=a)?a["on"+b]():void 0},a.prototype.addEvent=function(a,b,c){return null!=a.addEventListener?a.addEventListener(b,c,!1):null!=a.attachEvent?a.attachEvent("on"+b,c):a[b]=c},a.prototype.removeEvent=function(a,b,c){return null!=a.removeEventListener?a.removeEventListener(b,c,!1):null!=a.detachEvent?a.detachEvent("on"+b,c):delete a[b]},a.prototype.innerHeight=function(){return"innerHeight"in window?window.innerHeight:document.documentElement.clientHeight},a}(),c=this.WeakMap||this.MozWeakMap||(c=function(){function a(){this.keys=[],this.values=[]}return a.prototype.get=function(a){var b,c,d,e,f;for(f=this.keys,b=d=0,e=f.length;e>d;b=++d)if(c=f[b],c===a)return this.values[b]},a.prototype.set=function(a,b){var c,d,e,f,g;for(g=this.keys,c=e=0,f=g.length;f>e;c=++e)if(d=g[c],d===a)return void(this.values[c]=b);return this.keys.push(a),this.values.push(b)},a}()),a=this.MutationObserver||this.WebkitMutationObserver||this.MozMutationObserver||(a=function(){function a(){"undefined"!=typeof console&&null!==console&&console.warn("MutationObserver is not supported by your browser."),"undefined"!=typeof console&&null!==console&&console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")}return a.notSupported=!0,a.prototype.observe=function(){},a}()),d=this.getComputedStyle||function(a,b){return this.getPropertyValue=function(b){var c;return"float"===b&&(b="styleFloat"),e.test(b)&&b.replace(e,function(a,b){return b.toUpperCase()}),(null!=(c=a.currentStyle)?c[b]:void 0)||null},this},e=/(\-([a-z]){1})/g,this.WOW=function(){function e(a){null==a&&(a={}),this.scrollCallback=f(this.scrollCallback,this),this.scrollHandler=f(this.scrollHandler,this),this.resetAnimation=f(this.resetAnimation,this),this.start=f(this.start,this),this.scrolled=!0,this.config=this.util().extend(a,this.defaults),null!=a.scrollContainer&&(this.config.scrollContainer=document.querySelector(a.scrollContainer)),this.animationNameCache=new c,this.wowEvent=this.util().createEvent(this.config.boxClass)}return e.prototype.defaults={boxClass:"wow",animateClass:"animated",offset:0,mobile:!0,live:!0,callback:null,scrollContainer:null},e.prototype.init=function(){var a;return this.element=window.document.documentElement,"interactive"===(a=document.readyState)||"complete"===a?this.start():this.util().addEvent(document,"DOMContentLoaded",this.start),this.finished=[]},e.prototype.start=function(){var b,c,d,e;if(this.stopped=!1,this.boxes=function(){var a,c,d,e;for(d=this.element.querySelectorAll("."+this.config.boxClass),e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.all=function(){var a,c,d,e;for(d=this.boxes,e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.boxes.length)if(this.disabled())this.resetStyle();else for(e=this.boxes,c=0,d=e.length;d>c;c++)b=e[c],this.applyStyle(b,!0);return this.disabled()||(this.util().addEvent(this.config.scrollContainer||window,"scroll",this.scrollHandler),this.util().addEvent(window,"resize",this.scrollHandler),this.interval=setInterval(this.scrollCallback,50)),this.config.live?new a(function(a){return function(b){var c,d,e,f,g;for(g=[],c=0,d=b.length;d>c;c++)f=b[c],g.push(function(){var a,b,c,d;for(c=f.addedNodes||[],d=[],a=0,b=c.length;b>a;a++)e=c[a],d.push(this.doSync(e));return d}.call(a));return g}}(this)).observe(document.body,{childList:!0,subtree:!0}):void 0},e.prototype.stop=function(){return this.stopped=!0,this.util().removeEvent(this.config.scrollContainer||window,"scroll",this.scrollHandler),this.util().removeEvent(window,"resize",this.scrollHandler),null!=this.interval?clearInterval(this.interval):void 0},e.prototype.sync=function(b){return a.notSupported?this.doSync(this.element):void 0},e.prototype.doSync=function(a){var b,c,d,e,f;if(null==a&&(a=this.element),1===a.nodeType){for(a=a.parentNode||a,e=a.querySelectorAll("."+this.config.boxClass),f=[],c=0,d=e.length;d>c;c++)b=e[c],g.call(this.all,b)<0?(this.boxes.push(b),this.all.push(b),this.stopped||this.disabled()?this.resetStyle():this.applyStyle(b,!0),f.push(this.scrolled=!0)):f.push(void 0);return f}},e.prototype.show=function(a){return this.applyStyle(a),a.className=a.className+" "+this.config.animateClass,null!=this.config.callback&&this.config.callback(a),this.util().emitEvent(a,this.wowEvent),this.util().addEvent(a,"animationend",this.resetAnimation),this.util().addEvent(a,"oanimationend",this.resetAnimation),this.util().addEvent(a,"webkitAnimationEnd",this.resetAnimation),this.util().addEvent(a,"MSAnimationEnd",this.resetAnimation),a},e.prototype.applyStyle=function(a,b){var c,d,e;return d=a.getAttribute("data-wow-duration"),c=a.getAttribute("data-wow-delay"),e=a.getAttribute("data-wow-iteration"),this.animate(function(f){return function(){return f.customStyle(a,b,d,c,e)}}(this))},e.prototype.animate=function(){return"requestAnimationFrame"in window?function(a){return window.requestAnimationFrame(a)}:function(a){return a()}}(),e.prototype.resetStyle=function(){var a,b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],e.push(a.style.visibility="visible");return e},e.prototype.resetAnimation=function(a){var b;return a.type.toLowerCase().indexOf("animationend")>=0?(b=a.target||a.srcElement,b.className=b.className.replace(this.config.animateClass,"").trim()):void 0},e.prototype.customStyle=function(a,b,c,d,e){return b&&this.cacheAnimationName(a),a.style.visibility=b?"hidden":"visible",c&&this.vendorSet(a.style,{animationDuration:c}),d&&this.vendorSet(a.style,{animationDelay:d}),e&&this.vendorSet(a.style,{animationIterationCount:e}),this.vendorSet(a.style,{animationName:b?"none":this.cachedAnimationName(a)}),a},e.prototype.vendors=["moz","webkit"],e.prototype.vendorSet=function(a,b){var c,d,e,f;d=[];for(c in b)e=b[c],a[""+c]=e,d.push(function(){var b,d,g,h;for(g=this.vendors,h=[],b=0,d=g.length;d>b;b++)f=g[b],h.push(a[""+f+c.charAt(0).toUpperCase()+c.substr(1)]=e);return h}.call(this));return d},e.prototype.vendorCSS=function(a,b){var c,e,f,g,h,i;for(h=d(a),g=h.getPropertyCSSValue(b),f=this.vendors,c=0,e=f.length;e>c;c++)i=f[c],g=g||h.getPropertyCSSValue("-"+i+"-"+b);return g},e.prototype.animationName=function(a){var b;try{b=this.vendorCSS(a,"animation-name").cssText}catch(c){b=d(a).getPropertyValue("animation-name")}return"none"===b?"":b},e.prototype.cacheAnimationName=function(a){return this.animationNameCache.set(a,this.animationName(a))},e.prototype.cachedAnimationName=function(a){return this.animationNameCache.get(a)},e.prototype.scrollHandler=function(){return this.scrolled=!0},e.prototype.scrollCallback=function(){var a;return!this.scrolled||(this.scrolled=!1,this.boxes=function(){var b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],a&&(this.isVisible(a)?this.show(a):e.push(a));return e}.call(this),this.boxes.length||this.config.live)?void 0:this.stop()},e.prototype.offsetTop=function(a){for(var b;void 0===a.offsetTop;)a=a.parentNode;for(b=a.offsetTop;a=a.offsetParent;)b+=a.offsetTop;return b},e.prototype.isVisible=function(a){var b,c,d,e,f;return c=a.getAttribute("data-wow-offset")||this.config.offset,f=this.config.scrollContainer&&this.config.scrollContainer.scrollTop||window.pageYOffset,e=f+Math.min(this.element.clientHeight,this.util().innerHeight())-c,d=this.offsetTop(a),b=d+a.clientHeight,e>=d&&b>=f},e.prototype.util=function(){return null!=this._util?this._util:this._util=new b},e.prototype.disabled=function(){return!this.config.mobile&&this.util().isMobile(navigator.userAgent)},e}()}).call(this);

/***/ })

/******/ });
//# sourceMappingURL=libs.js.map