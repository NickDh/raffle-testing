#!/usr/bin/env bash

# Task for cancel old processing orders
command="/usr/local/bin/php /srv/app/artisan project:cancel-old-processing-orders >> /dev/null 2>&1"
job="11 * * * * $command"
cat <(fgrep -i -v "$command" <(crontab -l)) <(echo "$job") | crontab -

# Task for unlock held tickets
command="/usr/local/bin/php /srv/app/artisan resolve-old-held-tickets >> /dev/null 2>&1"
job="53 */2 * * * $command"
cat <(fgrep -i -v "$command" <(crontab -l)) <(echo "$job") | crontab -

#Task for renew certificates
command="/usr/bin/certbot renew --quiet"
job="15 3 * * * $command"
cat <(fgrep -i -v "$command" <(crontab -l)) <(echo "$job") | crontab -
