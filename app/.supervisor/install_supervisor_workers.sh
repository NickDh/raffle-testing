#!/usr/bin/env bash

# Load configs
supervisorctl reread
supervisorctl update

# Start workers
supervisorctl start laravel-worker:*
supervisorctl start clear-cart-worker:*
supervisorctl start competition-worker:*
supervisorctl start sms-worker:*