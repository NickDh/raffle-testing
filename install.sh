#!/usr/bin/env bash

# Import scripts
. ./.bin/utils.sh

# Update project URL
echo "New domain installing"
echo "Please, enter a new domain name:"
read domain

if [ "$domain" = "" ]
then
  error_exit "No domain defined";
fi

basicName="conf_template"
basicConfigPath="server/.nginx/config/local"

# Create new config
cp "$basicConfigPath/$basicName" "$basicConfigPath/$domain.conf"
# Update domain name
sed -i "s/{{domain_name}}/$domain/g" "$basicConfigPath/$domain.conf"
sed -i "s/{{domain_name}}/https:\/\/$domain/" "app/.env.example"

echo "$domain installed"

# Run containers
echo "Docker containers starting"
sudo docker-compose -f docker-compose.yml -f local.yml up --build -d

# Prepare back-end container
echo "Back-end container preparing"
sudo docker-compose -f docker-compose.yml -f local.yml run php-fpm .bin/install.sh

# Install certificates
echo "Install SSL certificates"
sudo docker-compose -f docker-compose.yml -f local.yml run server certbot --nginx --redirect --domains "$domain"

